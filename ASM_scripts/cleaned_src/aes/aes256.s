	.file	"aes256.c"
	.text
	.globl	gf_alog
	.type	gf_alog, @function
gf_alog:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movb	$1, -2(%rbp)
	jmp	.L2
.L4:
	movzbl	-2(%rbp), %eax
	movb	%al, -1(%rbp)
	salb	-2(%rbp)
	movzbl	-1(%rbp), %eax
	testb	%al, %al
	jns	.L3
	xorb	$27, -2(%rbp)
.L3:
	movzbl	-1(%rbp), %eax
	xorb	%al, -2(%rbp)
.L2:
	cmpb	$0, -20(%rbp)
	setne	%al
	subb	$1, -20(%rbp)
	testb	%al, %al
	jne	.L4
	movzbl	-2(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	gf_alog, .-gf_alog
	.globl	gf_log
	.type	gf_log, @function
gf_log:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movb	$1, -3(%rbp)
	movb	$0, -2(%rbp)
.L10:
	movzbl	-3(%rbp), %eax
	cmpb	-20(%rbp), %al
	je	.L12
.L7:
	movzbl	-3(%rbp), %eax
	movb	%al, -1(%rbp)
	salb	-3(%rbp)
	movzbl	-1(%rbp), %eax
	testb	%al, %al
	jns	.L9
	xorb	$27, -3(%rbp)
.L9:
	movzbl	-1(%rbp), %eax
	xorb	%al, -3(%rbp)
	addb	$1, -2(%rbp)
	cmpb	$0, -2(%rbp)
	jne	.L10
	jmp	.L8
.L12:
	nop
.L8:
	movzbl	-2(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	gf_log, .-gf_log
	.globl	gf_mulinv
	.type	gf_mulinv, @function
gf_mulinv:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	movl	%edi, %eax
	movb	%al, -4(%rbp)
	cmpb	$0, -4(%rbp)
	je	.L14
	movzbl	-4(%rbp), %eax
	movl	%eax, %edi
	call	gf_log
	notl	%eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	gf_alog
	jmp	.L15
.L14:
	movl	$0, %eax
.L15:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	gf_mulinv, .-gf_mulinv
	.globl	rj_sbox
	.type	rj_sbox, @function
rj_sbox:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movzbl	-20(%rbp), %eax
	movl	%eax, %edi
	call	gf_mulinv
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	xorl	$99, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	rj_sbox, .-rj_sbox
	.globl	rj_sbox_inv
	.type	rj_sbox_inv, @function
rj_sbox_inv:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movzbl	-20(%rbp), %eax
	xorl	$99, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	sall	$2, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$6, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	sall	$3, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$5, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	movl	%eax, %edi
	call	gf_mulinv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	rj_sbox_inv, .-rj_sbox_inv
	.globl	rj_xtime
	.type	rj_xtime, @function
rj_xtime:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -4(%rbp)
	movzbl	-4(%rbp), %eax
	testb	%al, %al
	jns	.L22
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	xorl	$27, %eax
	jmp	.L23
.L22:
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
.L23:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	rj_xtime, .-rj_xtime
	.globl	aes_subBytes
	.type	aes_subBytes, @function
aes_subBytes:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -24(%rbp)
	movl	$16, %ebx
	jmp	.L26
.L27:
	movzbl	%bl, %edx
	movq	-24(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %edx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movb	%al, (%r12)
.L26:
	testb	%bl, %bl
	setne	%al
	subl	$1, %ebx
	testb	%al, %al
	jne	.L27
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	aes_subBytes, .-aes_subBytes
	.globl	aes_subBytes_inv
	.type	aes_subBytes_inv, @function
aes_subBytes_inv:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -24(%rbp)
	movl	$16, %ebx
	jmp	.L29
.L30:
	movzbl	%bl, %edx
	movq	-24(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %edx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox_inv
	movb	%al, (%r12)
.L29:
	testb	%bl, %bl
	setne	%al
	subl	$1, %ebx
	testb	%al, %al
	jne	.L30
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	aes_subBytes_inv, .-aes_subBytes_inv
	.globl	aes_addRoundKey
	.type	aes_addRoundKey, @function
aes_addRoundKey:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movl	$16, %ebx
	jmp	.L32
.L33:
	movzbl	%bl, %edx
	movq	-16(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-16(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %esi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
.L32:
	testb	%bl, %bl
	setne	%al
	subl	$1, %ebx
	testb	%al, %al
	jne	.L33
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	aes_addRoundKey, .-aes_addRoundKey
	.globl	aes_addRoundKey_cpy
	.type	aes_addRoundKey_cpy, @function
aes_addRoundKey_cpy:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movl	$16, %ebx
	jmp	.L35
.L36:
	movzbl	%bl, %edx
	movq	-16(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-16(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %esi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %edx
	movq	-32(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	addl	$16, %eax
	movslq	%eax, %rdx
	movq	-32(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	addl	$16, %eax
	movslq	%eax, %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
.L35:
	testb	%bl, %bl
	setne	%al
	subl	$1, %ebx
	testb	%al, %al
	jne	.L36
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	aes_addRoundKey_cpy, .-aes_addRoundKey_cpy
	.globl	aes_shiftRows
	.type	aes_shiftRows, @function
aes_shiftRows:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rax
	movzbl	1(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	5(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	9(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	13(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$13, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	10(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	2(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$2, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	3(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	15(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	15(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	11(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	7(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$7, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	14(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	14(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	6(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$6, %rax
	movb	%bl, (%rax)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
	.size	aes_shiftRows, .-aes_shiftRows
	.globl	aes_shiftRows_inv
	.type	aes_shiftRows_inv, @function
aes_shiftRows_inv:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rax
	movzbl	1(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	13(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	9(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	5(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$5, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	2(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	10(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$10, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	3(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	7(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	11(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	15(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$15, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	6(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	14(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$14, %rax
	movb	%bl, (%rax)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	aes_shiftRows_inv, .-aes_shiftRows_inv
	.globl	aes_mixColumns
	.type	aes_mixColumns, @function
aes_mixColumns:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -48(%rbp)
	movl	$0, %ebx
	jmp	.L40
.L41:
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r15d
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -49(%rbp)
	movl	%r15d, %eax
	xorl	%r14d, %eax
	xorl	%r13d, %eax
	movzbl	-49(%rbp), %edx
	xorl	%eax, %edx
	movl	%edx, %r12d
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -50(%rbp)
	movl	%r15d, %eax
	xorl	%r14d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-50(%rbp), %al
	movq	-64(%rbp), %rdx
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -51(%rbp)
	movl	%r14d, %eax
	xorl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-51(%rbp), %al
	movq	-72(%rbp), %rdx
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	leaq	(%rdx,%rax), %r14
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -52(%rbp)
	movzbl	-49(%rbp), %eax
	xorl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-52(%rbp), %al
	movb	%al, (%r14)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	leaq	(%rdx,%rax), %r13
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	-49(%rbp), %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorl	%r14d, %eax
	movb	%al, 0(%r13)
	addl	$4, %ebx
.L40:
	cmpb	$15, %bl
	jbe	.L41
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	aes_mixColumns, .-aes_mixColumns
	.globl	aes_mixColumns_inv
	.type	aes_mixColumns_inv, @function
aes_mixColumns_inv:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -48(%rbp)
	movl	$0, %ebx
	jmp	.L43
.L44:
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r12d
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r15d
	movl	%r14d, %eax
	xorl	%r13d, %eax
	xorl	%r12d, %eax
	movl	%eax, %edx
	xorl	%r15d, %edx
	movb	%dl, -49(%rbp)
	movzbl	-49(%rbp), %eax
	movl	%eax, %edi
	call	rj_xtime
	movb	%al, -50(%rbp)
	movzbl	-50(%rbp), %eax
	xorl	%r14d, %eax
	xorl	%r12d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	-49(%rbp), %edx
	xorl	%eax, %edx
	movb	%dl, -51(%rbp)
	movzbl	-50(%rbp), %eax
	xorl	%r13d, %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	-49(%rbp), %edx
	xorl	%eax, %edx
	movb	%dl, -52(%rbp)
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -53(%rbp)
	movl	%r14d, %eax
	xorl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-51(%rbp), %al
	xorb	-53(%rbp), %al
	movq	-64(%rbp), %rdx
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -54(%rbp)
	movl	%r13d, %eax
	xorl	%r12d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-52(%rbp), %al
	xorb	-54(%rbp), %al
	movq	-72(%rbp), %rdx
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	leaq	(%rdx,%rax), %r13
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -55(%rbp)
	movl	%r12d, %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-51(%rbp), %al
	xorb	-55(%rbp), %al
	movb	%al, 0(%r13)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movl	%r15d, %eax
	xorl	%r14d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-52(%rbp), %al
	xorl	%r13d, %eax
	movb	%al, (%r12)
	addl	$4, %ebx
.L43:
	cmpb	$15, %bl
	jbe	.L44
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	aes_mixColumns_inv, .-aes_mixColumns_inv
	.globl	aes_expandEncKey
	.type	aes_expandEncKey, @function
aes_expandEncKey:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movzbl	(%rax), %ebx
	movq	-24(%rbp), %rax
	addq	$29, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movq	-32(%rbp), %rdx
	movzbl	(%rdx), %edx
	xorl	%edx, %eax
	movl	%ebx, %edx
	xorl	%eax, %edx
	movq	-24(%rbp), %rax
	movb	%dl, (%rax)
	movq	-24(%rbp), %rax
	leaq	1(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$30, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-24(%rbp), %rax
	leaq	2(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$2, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$31, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-24(%rbp), %rax
	leaq	3(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movl	%eax, %edx
	shrb	$7, %dl
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	leal	0(,%rax,8), %edx
	addl	%edx, %eax
	xorl	%ecx, %eax
	movl	%eax, %edx
	movq	-32(%rbp), %rax
	movb	%dl, (%rax)
	movl	$4, %ebx
	jmp	.L46
.L47:
	movzbl	%bl, %edx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-4(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	1(%rax), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-3(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	2(%rax), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-2(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	3(%rax), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-1(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	addl	$4, %ebx
.L46:
	cmpb	$15, %bl
	jbe	.L47
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-24(%rbp), %rax
	leaq	17(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$17, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$13, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-24(%rbp), %rax
	leaq	18(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$18, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$14, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-24(%rbp), %rax
	leaq	19(%rax), %rbx
	movq	-24(%rbp), %rax
	addq	$19, %rax
	movzbl	(%rax), %r12d
	movq	-24(%rbp), %rax
	addq	$15, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movl	$20, %ebx
	jmp	.L48
.L49:
	movzbl	%bl, %edx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-4(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	1(%rax), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-3(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	2(%rax), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-2(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	3(%rax), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-1(%rax), %rsi
	movq	-24(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	addl	$4, %ebx
.L48:
	cmpb	$31, %bl
	jbe	.L49
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	aes_expandEncKey, .-aes_expandEncKey
	.globl	aes_expandDecKey
	.type	aes_expandDecKey, @function
aes_expandDecKey:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movb	$28, -17(%rbp)
	jmp	.L51
.L52:
	movzbl	-17(%rbp), %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	subb	$4, -17(%rbp)
.L51:
	cmpb	$16, -17(%rbp)
	ja	.L52
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	17(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$17, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$13, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	18(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$18, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$14, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	19(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$19, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$15, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movb	$12, -17(%rbp)
	jmp	.L53
.L54:
	movzbl	-17(%rbp), %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	subb	$4, -17(%rbp)
.L53:
	cmpb	$0, -17(%rbp)
	jne	.L54
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	shrb	%al
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L55
	movl	$-115, %eax
	jmp	.L56
.L55:
	movl	$0, %eax
.L56:
	xorl	%edx, %eax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	movq	-40(%rbp), %rax
	movzbl	(%rax), %ebx
	movq	-40(%rbp), %rax
	addq	$29, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movq	-48(%rbp), %rdx
	movzbl	(%rdx), %edx
	xorl	%edx, %eax
	movl	%ebx, %edx
	xorl	%eax, %edx
	movq	-40(%rbp), %rax
	movb	%dl, (%rax)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$30, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	2(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$2, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$31, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	aes_expandDecKey, .-aes_expandDecKey
	.globl	aes256_init
	.type	aes256_init, @function
aes256_init:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movb	$1, -9(%rbp)
	movl	$0, %ebx
	jmp	.L58
.L59:
	movzbl	%bl, %edi
	movzbl	%bl, %eax
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rdx
	addq	%rcx, %rdx
	movzbl	(%rdx), %esi
	movq	-32(%rbp), %rcx
	movslq	%eax, %rdx
	movb	%sil, 64(%rcx,%rdx)
	movq	-32(%rbp), %rdx
	cltq
	movzbl	64(%rdx,%rax), %ecx
	movq	-32(%rbp), %rdx
	movslq	%edi, %rax
	movb	%cl, 32(%rdx,%rax)
	addl	$1, %ebx
.L58:
	cmpb	$31, %bl
	jbe	.L59
	movl	$8, %ebx
	jmp	.L60
.L61:
	movq	-32(%rbp), %rax
	leaq	64(%rax), %rdx
	leaq	-9(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	aes_expandEncKey
.L60:
	subl	$1, %ebx
	testb	%bl, %bl
	jne	.L61
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	aes256_init, .-aes256_init
	.globl	aes256_done
	.type	aes256_done, @function
aes256_done:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movl	$0, %ebx
	jmp	.L63
.L64:
	movzbl	%bl, %edi
	movzbl	%bl, %eax
	movzbl	%bl, %edx
	movq	-16(%rbp), %rsi
	movslq	%edx, %rcx
	movb	$0, 64(%rsi,%rcx)
	movq	-16(%rbp), %rcx
	movslq	%edx, %rdx
	movzbl	64(%rcx,%rdx), %esi
	movq	-16(%rbp), %rcx
	movslq	%eax, %rdx
	movb	%sil, 32(%rcx,%rdx)
	movq	-16(%rbp), %rdx
	cltq
	movzbl	32(%rdx,%rax), %ecx
	movq	-16(%rbp), %rdx
	movslq	%edi, %rax
	movb	%cl, (%rdx,%rax)
	addl	$1, %ebx
.L63:
	cmpb	$31, %bl
	jbe	.L64
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
	.size	aes256_done, .-aes256_done
	.globl	aes256_encrypt_ecb
	.type	aes256_encrypt_ecb, @function
aes256_encrypt_ecb:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rdx
	movq	-24(%rbp), %rax
	leaq	32(%rax), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey_cpy
	movb	$1, -1(%rbp)
	movb	$1, -2(%rbp)
	jmp	.L66
.L69:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_mixColumns
	movzbl	-1(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L67
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	jmp	.L68
.L67:
	movq	-24(%rbp), %rax
	leaq	-2(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandEncKey
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
.L68:
	addb	$1, -1(%rbp)
.L66:
	cmpb	$13, -1(%rbp)
	jbe	.L69
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows
	movq	-24(%rbp), %rax
	leaq	-2(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandEncKey
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	aes256_encrypt_ecb, .-aes256_encrypt_ecb
	.globl	aes256_decrypt_ecb
	.type	aes256_decrypt_ecb, @function
aes256_decrypt_ecb:
.LFB21:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rdx
	movq	-24(%rbp), %rax
	leaq	64(%rax), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey_cpy
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows_inv
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes_inv
	movb	$14, -1(%rbp)
	movb	$-128, -2(%rbp)
	jmp	.L71
.L74:
	movzbl	-1(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L72
	movq	-24(%rbp), %rax
	leaq	-2(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandDecKey
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	jmp	.L73
.L72:
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
.L73:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_mixColumns_inv
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows_inv
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes_inv
.L71:
	subb	$1, -1(%rbp)
	cmpb	$0, -1(%rbp)
	jne	.L74
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21:
	.size	aes256_decrypt_ecb, .-aes256_decrypt_ecb
	.section	.rodata
.LC0:
	.string	"txt: "
.LC1:
	.string	"%02x "
.LC2:
	.string	"key: "
.LC3:
	.string	"---"
.LC4:
	.string	"enc: "
	.align 8
.LC5:
	.string	"tst: 8e a2 b7 ca 51 67 45 bf ea fc 49 90 4b 49 60 89"
.LC6:
	.string	"dec: "
	.text
	.globl	main
	.type	main, @function
main:
.LFB22:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movl	%edi, -180(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movb	$0, -161(%rbp)
	jmp	.L76
.L77:
	movzbl	-161(%rbp), %ecx
	movzbl	-161(%rbp), %edx
	movl	%edx, %eax
	sall	$4, %eax
	addl	%eax, %edx
	movslq	%ecx, %rax
	movb	%dl, -160(%rbp,%rax)
	addb	$1, -161(%rbp)
.L76:
	cmpb	$15, -161(%rbp)
	jbe	.L77
	movb	$0, -161(%rbp)
	jmp	.L78
.L79:
	movzbl	-161(%rbp), %eax
	cltq
	movzbl	-161(%rbp), %edx
	movb	%dl, -144(%rbp,%rax)
	addb	$1, -161(%rbp)
.L78:
	cmpb	$31, -161(%rbp)
	jbe	.L79
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movb	$0, -161(%rbp)
	jmp	.L80
.L81:
	movzbl	-161(%rbp), %eax
	cltq
	movzbl	-160(%rbp,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addb	$1, -161(%rbp)
.L80:
	cmpb	$15, -161(%rbp)
	jbe	.L81
	movl	$10, %edi
	call	putchar
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movb	$0, -161(%rbp)
	jmp	.L82
.L83:
	movzbl	-161(%rbp), %eax
	cltq
	movzbl	-144(%rbp,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addb	$1, -161(%rbp)
.L82:
	cmpb	$31, -161(%rbp)
	jbe	.L83
	movl	$10, %edi
	call	putchar
	movl	$.LC3, %edi
	call	puts
	leaq	-144(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_init
	leaq	-160(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_encrypt_ecb
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movb	$0, -161(%rbp)
	jmp	.L84
.L85:
	movzbl	-161(%rbp), %eax
	cltq
	movzbl	-160(%rbp,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addb	$1, -161(%rbp)
.L84:
	cmpb	$15, -161(%rbp)
	jbe	.L85
	movl	$10, %edi
	call	putchar
	movl	$.LC5, %edi
	call	puts
	leaq	-144(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_init
	leaq	-160(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_decrypt_ecb
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	movb	$0, -161(%rbp)
	jmp	.L86
.L87:
	movzbl	-161(%rbp), %eax
	cltq
	movzbl	-160(%rbp,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addb	$1, -161(%rbp)
.L86:
	cmpb	$15, -161(%rbp)
	jbe	.L87
	movl	$10, %edi
	call	putchar
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	call	aes256_done
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L89
	call	__stack_chk_fail
.L89:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.7.3-1ubuntu1) 4.7.3"
	.section	.note.GNU-stack,"",@progbits
