	.file	"aes256_jflaccolades.c"
/******************************************************* */
/*  A ajouter au début                                   */
/* Définition des diverses variables globales auxiliaires*/
/* var_hack: donne le tour dans lequel l'attaque doit    */
/*           avoir lieu                                  */
/* .LC00 : chaine de caractère affichée quand on fait un */
/*         saut                                          */
/*********************************************************/
	.globl	var_hack
	.data
	.align 4
	.type	var_hack, @object
	.size	var_hack, 4
var_hack:
	.long	1

.LC00:
	.string "Jumping\n"
.LC01:
	.string "HACKING\n"
/*********************************************************/
/*  Fin de la définition des variables                   */
/*********************************************************/
	.section	.rodata
.LC0:
	.string	"%s"
.LC1:
	.string	"%02x "
	.text
	.globl	DUMP
	.type	DUMP, @function
DUMP:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movq	%rdx, -40(%rbp)
	movl	%ecx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L2:
	movl	-4(%rbp), %eax
	cmpl	-32(%rbp), %eax
	jl	.L3
	movl	$10, %edi
	call	putchar
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	DUMP, .-DUMP
	.globl	gf_alog
	.type	gf_alog, @function
gf_alog:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movb	$1, -2(%rbp)
	jmp	.L5
.L7:
	movzbl	-2(%rbp), %eax
	movb	%al, -1(%rbp)
	salb	-2(%rbp)
	movzbl	-1(%rbp), %eax
	testb	%al, %al
	jns	.L6
	xorb	$27, -2(%rbp)
.L6:
	movzbl	-1(%rbp), %eax
	xorb	%al, -2(%rbp)
.L5:
	movzbl	-20(%rbp), %eax
	leal	-1(%rax), %edx
	movb	%dl, -20(%rbp)
	testb	%al, %al
	jne	.L7
	movzbl	-2(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	gf_alog, .-gf_alog
	.globl	gf_log
	.type	gf_log, @function
gf_log:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movb	$1, -3(%rbp)
	movb	$0, -2(%rbp)
.L13:
	movzbl	-3(%rbp), %eax
	cmpb	-20(%rbp), %al
	jne	.L10
	jmp	.L11
.L10:
	movzbl	-3(%rbp), %eax
	movb	%al, -1(%rbp)
	salb	-3(%rbp)
	movzbl	-1(%rbp), %eax
	testb	%al, %al
	jns	.L12
	xorb	$27, -3(%rbp)
.L12:
	movzbl	-1(%rbp), %eax
	xorb	%al, -3(%rbp)
	addb	$1, -2(%rbp)
	cmpb	$0, -2(%rbp)
	jne	.L13
.L11:
	movzbl	-2(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	gf_log, .-gf_log
	.globl	gf_mulinv
	.type	gf_mulinv, @function
gf_mulinv:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	movl	%edi, %eax
	movb	%al, -4(%rbp)
	cmpb	$0, -4(%rbp)
	je	.L16
	movzbl	-4(%rbp), %eax
	movl	%eax, %edi
	call	gf_log
	notl	%eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	gf_alog
	jmp	.L17
.L16:
	movl	$0, %eax
.L17:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	gf_mulinv, .-gf_mulinv
	.globl	rj_sbox
	.type	rj_sbox, @function
rj_sbox:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movzbl	-20(%rbp), %eax
	movl	%eax, %edi
	call	gf_mulinv
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	xorl	$99, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	rj_sbox, .-rj_sbox
	.globl	rj_sbox_inv
	.type	rj_sbox_inv, @function
rj_sbox_inv:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movzbl	-20(%rbp), %eax
	xorl	$99, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	sall	$2, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$6, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	sall	$3, %eax
	movl	%eax, %edx
	movzbl	-2(%rbp), %eax
	shrb	$5, %al
	orl	%edx, %eax
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	xorb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	movl	%eax, %edi
	call	gf_mulinv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	rj_sbox_inv, .-rj_sbox_inv
	.globl	rj_xtime
	.type	rj_xtime, @function
rj_xtime:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -4(%rbp)
	movzbl	-4(%rbp), %eax
	testb	%al, %al
	jns	.L24
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	xorl	$27, %eax
	jmp	.L25
.L24:
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
.L25:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	rj_xtime, .-rj_xtime
	.globl	aes_subBytes
	.type	aes_subBytes, @function
aes_subBytes:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movl	$16, %ebx
	jmp	.L28
.L29:
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movb	%al, (%r12)
.L28:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	testb	%al, %al
	jne	.L29
	movl	$1, -20(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	aes_subBytes, .-aes_subBytes
	.globl	aes_subBytes_inv
	.type	aes_subBytes_inv, @function
aes_subBytes_inv:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movl	$16, %ebx
	jmp	.L31
.L32:
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox_inv
	movb	%al, (%r12)
.L31:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	testb	%al, %al
	jne	.L32
	movl	$1, -20(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	aes_subBytes_inv, .-aes_subBytes_inv
	.globl	aes_addRoundKey
	.type	aes_addRoundKey, @function
aes_addRoundKey:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movl	$16, %ebx
	jmp	.L34
.L35:
	movzbl	%bl, %edx
	movq	-32(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-32(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %esi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
.L34:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	testb	%al, %al
	jne	.L35
	movl	$1, -12(%rbp)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	aes_addRoundKey, .-aes_addRoundKey
	.globl	aes_addRoundKey_cpy
	.type	aes_addRoundKey_cpy, @function
aes_addRoundKey_cpy:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movl	$16, %ebx
	jmp	.L37
.L38:
	movzbl	%bl, %edx
	movq	-32(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-32(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %esi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %edx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	addl	$16, %eax
	movslq	%eax, %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	addl	$16, %eax
	movslq	%eax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
.L37:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	testb	%al, %al
	jne	.L38
	movl	$1, -12(%rbp)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
	.size	aes_addRoundKey_cpy, .-aes_addRoundKey_cpy
	.globl	aes_shiftRows
	.type	aes_shiftRows, @function
aes_shiftRows:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rax
	movzbl	1(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	5(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	9(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	13(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$13, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	10(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	2(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$2, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	3(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	15(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	15(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	11(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	7(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$7, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	14(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	14(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	6(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$6, %rax
	movb	%bl, (%rax)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	aes_shiftRows, .-aes_shiftRows
	.globl	aes_shiftRows_inv
	.type	aes_shiftRows_inv, @function
aes_shiftRows_inv:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rax
	movzbl	1(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	13(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	9(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	5(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$5, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	2(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	10(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$10, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	3(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	7(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	11(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	15(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$15, %rax
	movb	%bl, (%rax)
	movq	-16(%rbp), %rax
	movzbl	6(%rax), %ebx
	movq	-16(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-16(%rbp), %rax
	movzbl	14(%rax), %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	addq	$14, %rax
	movb	%bl, (%rax)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	aes_shiftRows_inv, .-aes_shiftRows_inv
	.globl	aes_mixColumns
	.type	aes_mixColumns, @function
aes_mixColumns:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movl	$0, %ebx
	jmp	.L42
.L43:
	movzbl	%bl, %edx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r15d
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movl	%eax, %ecx
	movl	%r15d, %eax
	xorl	%r14d, %eax
	xorl	%r13d, %eax
	movl	%eax, %r12d
	movb	%cl, -65(%rbp)
	xorb	-65(%rbp), %r12b
	movzbl	%bl, %edx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %rsi
	movq	%rsi, -80(%rbp)
	movzbl	%bl, %edx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edi
	movb	%dil, -66(%rbp)
	movl	%r15d, %esi
	xorl	%r14d, %esi
	movl	%esi, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-66(%rbp), %al
	movq	-80(%rbp), %rsi
	movb	%al, (%rsi)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %rsi
	movq	%rsi, -80(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edi
	movb	%dil, -66(%rbp)
	xorl	%r13d, %r14d
	movl	%r14d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-66(%rbp), %al
	movq	-80(%rbp), %rsi
	movb	%al, (%rsi)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %r14
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edi
	movb	%dil, -80(%rbp)
	xorb	-65(%rbp), %r13b
	movl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-80(%rbp), %al
	movb	%al, (%r14)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %r13
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	-65(%rbp), %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorl	%r14d, %eax
	movb	%al, 0(%r13)
	addl	$4, %ebx
.L42:
	cmpb	$15, %bl
	jbe	.L43
	movl	$1, -44(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	aes_mixColumns, .-aes_mixColumns
	.globl	aes_mixColumns_inv
	.type	aes_mixColumns_inv, @function
aes_mixColumns_inv:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movl	$0, %ebx
	jmp	.L45
.L46:
	movzbl	%bl, %edx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r12d
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r15d
	movl	%r14d, %eax
	xorl	%r13d, %eax
	xorl	%r12d, %eax
	xorl	%r15d, %eax
	movb	%al, -65(%rbp)
	movzbl	-65(%rbp), %eax
	movl	%eax, %edi
	call	rj_xtime
	movl	%eax, %ecx
	movb	%cl, -80(%rbp)
	xorl	%r14d, %ecx
	movl	%ecx, %eax
	xorl	%r12d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movl	%eax, %esi
	xorb	-65(%rbp), %sil
	movb	%sil, -66(%rbp)
	movzbl	-80(%rbp), %ecx
	xorl	%r13d, %ecx
	movl	%ecx, %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movl	%eax, %ecx
	xorb	-65(%rbp), %cl
	movb	%cl, -65(%rbp)
	movzbl	%bl, %edx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movzbl	%bl, %edx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movb	%dl, -67(%rbp)
	movl	%r14d, %eax
	xorl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-66(%rbp), %al
	xorb	-67(%rbp), %al
	movq	-80(%rbp), %rdi
	movb	%al, (%rdi)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movb	%dl, -67(%rbp)
	movl	%r13d, %eax
	xorl	%r12d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-65(%rbp), %al
	xorb	-67(%rbp), %al
	movq	-80(%rbp), %rdi
	movb	%al, (%rdi)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %r13
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -80(%rbp)
	movl	%r12d, %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-66(%rbp), %al
	xorb	-80(%rbp), %al
	movb	%al, 0(%r13)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movl	%r15d, %eax
	xorl	%r14d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-65(%rbp), %al
	xorl	%r13d, %eax
	movb	%al, (%r12)
	addl	$4, %ebx
.L45:
	cmpb	$15, %bl
	jbe	.L46
	movl	$1, -44(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	aes_mixColumns_inv, .-aes_mixColumns_inv
	.globl	aes_expandEncKey
	.type	aes_expandEncKey, @function
aes_expandEncKey:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rax
	movzbl	(%rax), %ebx
	movq	-40(%rbp), %rax
	addq	$29, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movq	-48(%rbp), %rdx
	movzbl	(%rdx), %edx
	xorl	%edx, %eax
	movl	%ebx, %edx
	xorl	%eax, %edx
	movq	-40(%rbp), %rax
	movb	%dl, (%rax)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$30, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	2(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$2, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$31, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, %ecx
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	shrb	$7, %al
	movl	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	leal	0(,%rax,8), %edx
	addl	%edx, %eax
	xorl	%ecx, %eax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	movl	$4, %ebx
	jmp	.L48
.L49:
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	addl	$4, %ebx
.L48:
	cmpb	$15, %bl
	jbe	.L49
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	17(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$17, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$13, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	18(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$18, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$14, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	19(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$19, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$15, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movl	$20, %ebx
	jmp	.L50
.L51:
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	addl	$4, %ebx
.L50:
	cmpb	$31, %bl
	jbe	.L51
	movl	$1, -20(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	aes_expandEncKey, .-aes_expandEncKey
	.globl	aes_expandDecKey
	.type	aes_expandDecKey, @function
aes_expandDecKey:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movb	$28, -17(%rbp)
	jmp	.L53
.L54:
	movzbl	-17(%rbp), %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	subb	$4, -17(%rbp)
.L53:
	cmpb	$16, -17(%rbp)
	ja	.L54
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	17(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$17, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$13, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	18(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$18, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$14, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	19(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$19, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$15, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movb	$12, -17(%rbp)
	jmp	.L55
.L56:
	movzbl	-17(%rbp), %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-17(%rbp), %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-17(%rbp), %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	subb	$4, -17(%rbp)
.L55:
	cmpb	$0, -17(%rbp)
	jne	.L56
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	shrb	%al
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L57
	movl	$-115, %eax
	jmp	.L58
.L57:
	movl	$0, %eax
.L58:
	xorl	%edx, %eax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	movq	-40(%rbp), %rax
	movzbl	(%rax), %ebx
	movq	-40(%rbp), %rax
	addq	$29, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movq	-48(%rbp), %rdx
	movzbl	(%rdx), %edx
	xorl	%edx, %eax
	xorl	%eax, %ebx
	movl	%ebx, %edx
	movq	-40(%rbp), %rax
	movb	%dl, (%rax)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$30, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	2(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$2, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$31, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	aes_expandDecKey, .-aes_expandDecKey
	.globl	aes256_init
	.type	aes256_init, @function
aes256_init:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movb	$1, -13(%rbp)
	movl	$0, %ebx
	jmp	.L60
.L61:
	movzbl	%bl, %edi
	movzbl	%bl, %eax
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rdx
	addq	%rcx, %rdx
	movzbl	(%rdx), %esi
	movq	-32(%rbp), %rcx
	movslq	%eax, %rdx
	movb	%sil, 64(%rcx,%rdx)
	movq	-32(%rbp), %rdx
	cltq
	movzbl	64(%rdx,%rax), %ecx
	movq	-32(%rbp), %rdx
	movslq	%edi, %rax
	movb	%cl, 32(%rdx,%rax)
	movl	%ebx, %eax
	leal	1(%rax), %ebx
.L60:
	cmpb	$31, %bl
	jbe	.L61
	movl	$8, %ebx
	jmp	.L62
.L63:
	movq	-32(%rbp), %rax
	leaq	64(%rax), %rdx
	leaq	-13(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	aes_expandEncKey
.L62:
	subl	$1, %ebx
	testb	%bl, %bl
	jne	.L63
	movl	$1, -12(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
	.size	aes256_init, .-aes256_init
	.globl	aes256_done
	.type	aes256_done, @function
aes256_done:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, -32(%rbp)
	movl	$0, %ebx
	jmp	.L65
.L66:
	movzbl	%bl, %edi
	movzbl	%bl, %eax
	movzbl	%bl, %edx
	movq	-32(%rbp), %rsi
	movslq	%edx, %rcx
	movb	$0, 64(%rsi,%rcx)
	movq	-32(%rbp), %rcx
	movslq	%edx, %rdx
	movzbl	64(%rcx,%rdx), %esi
	movq	-32(%rbp), %rcx
	movslq	%eax, %rdx
	movb	%sil, 32(%rcx,%rdx)
	movq	-32(%rbp), %rdx
	cltq
	movzbl	32(%rdx,%rax), %ecx
	movq	-32(%rbp), %rdx
	movslq	%edi, %rax
	movb	%cl, (%rdx,%rax)
	movl	%ebx, %eax
	leal	1(%rax), %ebx
.L65:
	cmpb	$31, %bl
	jbe	.L66
	movl	$1, -12(%rbp)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	aes256_done, .-aes256_done
	.globl	aes256_encrypt_ecb
	.type	aes256_encrypt_ecb, @function
aes256_encrypt_ecb:
.LFB21:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rdx
	movq	-24(%rbp), %rax
	leaq	32(%rax), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey_cpy
	movb	$1, -5(%rbp)
	movb	$1, -6(%rbp)
	jmp	.L68
.L71:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_mixColumns
	movzbl	-5(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L69
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	jmp	.L70
.L69:
	movq	-24(%rbp), %rax
	leaq	-6(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandEncKey
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
.L70:
	movl	$1, -4(%rbp)
	addb	$1, -5(%rbp)
.L68:
	cmpb	$13, -5(%rbp)
	jbe	.L71
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows
	movq	-24(%rbp), %rax
	leaq	-6(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandEncKey
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21:
	.size	aes256_encrypt_ecb, .-aes256_encrypt_ecb
	.globl	aes256_decrypt_ecb
	.type	aes256_decrypt_ecb, @function
aes256_decrypt_ecb:
.LFB22:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rdx
	movq	-24(%rbp), %rax
	leaq	64(%rax), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey_cpy
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows_inv
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes_inv
	movb	$14, -1(%rbp)
	movb	$-128, -2(%rbp)
	jmp	.L73
.L76:
	movzbl	-1(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L74
	movq	-24(%rbp), %rax
	leaq	-2(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandDecKey
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	jmp	.L75
.L74:
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
.L75:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_mixColumns_inv
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows_inv
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes_inv
.L73:
	subb	$1, -1(%rbp)
	cmpb	$0, -1(%rbp)
	jne	.L76
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	aes256_decrypt_ecb, .-aes256_decrypt_ecb
	.section	.rodata
.LC2:
	.string	"txt: "
.LC3:
	.string	"key: "
.LC4:
	.string	"---"
.LC5:
	.string	"enc: "
	.align 8
.LC6:
	.string	"tst: 8e a2 b7 ca 51 67 45 bf ea fc 49 90 4b 49 60 89"
.LC7:
	.string	"dec: "
	.text
	.globl	main
	.type	main, @function
main:
.LFB23:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -24
	movl	%edi, -196(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -181(%rbp)
	jmp	.L78
.L79:
	movzbl	-181(%rbp), %ecx
	movzbl	-181(%rbp), %edx
	movl	%edx, %eax
	sall	$4, %eax
	addl	%eax, %edx
	movslq	%ecx, %rax
	movb	%dl, -176(%rbp,%rax)
	movzbl	-181(%rbp), %eax
	addl	$1, %eax
	movb	%al, -181(%rbp)
.L78:
	cmpb	$15, -181(%rbp)
	jbe	.L79
	movb	$0, -181(%rbp)
	jmp	.L80
.L81:
	movzbl	-181(%rbp), %eax
	cltq
	movzbl	-181(%rbp), %edx
	movb	%dl, -160(%rbp,%rax)
	movzbl	-181(%rbp), %eax
	addl	$1, %eax
	movb	%al, -181(%rbp)
.L80:
	cmpb	$31, -181(%rbp)
	jbe	.L81
	movl	$1, -180(%rbp)
	movzbl	-181(%rbp), %eax
	leaq	-176(%rbp), %rdx
	movl	$16, %ecx
	movl	%eax, %esi
	movl	$.LC2, %edi
	call	DUMP
	movzbl	-181(%rbp), %eax
	leaq	-160(%rbp), %rdx
	movl	$32, %ecx
	movl	%eax, %esi
	movl	$.LC3, %edi
	call	DUMP
	movl	$.LC4, %edi
	call	puts
	leaq	-160(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_init
	leaq	-176(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_encrypt_ecb
	movzbl	-181(%rbp), %eax
	leaq	-176(%rbp), %rdx
	movl	$16, %ecx
	movl	%eax, %esi
	movl	$.LC5, %edi
	call	DUMP
	movl	$.LC6, %edi
	call	puts
	leaq	-160(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_init
	leaq	-176(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_decrypt_ecb
	movzbl	-181(%rbp), %eax
	leaq	-176(%rbp), %rdx
	movl	$16, %ecx
	movl	%eax, %esi
	movl	$.LC7, %edi
	call	DUMP
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	aes256_done
	movl	$0, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	je	.L83
	call	__stack_chk_fail
.L83:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.8.1-10ubuntu8) 4.8.1"
	.section	.note.GNU-stack,"",@progbits
