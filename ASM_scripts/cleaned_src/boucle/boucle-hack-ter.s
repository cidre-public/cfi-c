	.file	"boucle.c"
/******************************************************* */
/*  A ajouter au début                                   */
/* Définition des diverses variables globales auxiliaires*/
/* var_hack: donne le tour dans lequel l'attaque doit    */
/*           avoir lieu                                  */
/* .LC00 : chaine de caractère affichée quand on fait un */
/*         saut                                          */
/*********************************************************/
	.globl	var_hack
	.data
	.align 4
	.type	var_hack, @object
	.size	var_hack, 4
var_hack:
	.long	1

.LC00:
	.string "JUMPING\n"
/*********************************************************/
/*  Fin de la définition des variables                   */
/*********************************************************/
	.section	.rodata
.LC0:
	.string	"%i\t%i\n"
	.text
/*********************************************************/
/*  Différentes parties du hack                          */
/* .LHack1: Affiche JUMPING, rétablit le registre et fait*/
/*          le saut                                      */
/* .LHack2: Rétablit le registre et continue comme s'il  */
/*          n'y avait pas eu d'attaque                   */
/* .LHack3: Sauvegarde le registre eax, décrémente le    */
/*          compteur de saut, et envoie vers l'attaque   */
/*          si ce compteur est à 0                       */
/*********************************************************/
.LHack1:
	movl	$.LC00, %eax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	pop	%rax
	jmp	.JumpThere

.LHack2:
	pop	%rax
	jmp 	.Suite
	nop

.LHack3:
	push	%rax
	movl	var_hack(%rip), %eax
	subl	$1, %eax
	movl	%eax, var_hack(%rip)
	movl	var_hack(%rip), %eax
	testl	%eax, %eax
	jne	.LHack2
	jmp	.LHack1
/*********************************************************/
/*     FIN DU CODE AJOUTE                                */
/*   Il faut placer les attaques dans le code source     */
/*********************************************************/
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$0, -8(%rbp)
/********************************************************/
/*   Point d'entrée de l'attaque                        */
/********************************************************/
.JumpFromThisLine:
	jmp 	.LHack3
.Suite:
/********************************************************/
/*   Fin du point d'entrée                              */
/********************************************************/
	movl	$0, -4(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	movl	-4(%rbp), %eax
	movl	-8(%rbp), %edx
	addl	%edx, %eax
/********************************************************/
/*  Point de chute de l'attaque                         */
/*  Il est simplement réduit à l'étiquette              */
/********************************************************/
.JumpThere:
/********************************************************/
	addl	%eax, -8(%rbp)
	addl	$1, -4(%rbp)
.L2:
	cmpl	$9, -4(%rbp)
	jle	.L3
	movl	$.LC0, %eax
	movl	-8(%rbp), %edx
	movl	-4(%rbp), %ecx
	movl	%ecx, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
