	.file	"aes256_jflaccolades_CFI.c"
/******************************************************* */
/*  A ajouter au début                                   */
/* Définition des diverses variables globales auxiliaires*/
/* var_hack: donne le tour dans lequel l'attaque doit    */
/*           avoir lieu                                  */
/* .LC00 : chaine de caractère affichée quand on fait un */
/*         saut                                          */
/*********************************************************/
	.globl	var_hack
	.data
	.align 4
	.type	var_hack, @object
	.size	var_hack, 4
var_hack:
	.long	1

.LC00:
	.string "Jumping\n"
.LC01:
	.string "HACKING\n"
/*********************************************************/
/*  Fin de la définition des variables                   */
/*********************************************************/
	.section	.rodata
.LC0:
	.string	"%s"
.LC1:
	.string	"%02x "
	.text
	.globl	DUMP
	.type	DUMP, @function
DUMP:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movq	%rdx, -40(%rbp)
	movl	%ecx, -32(%rbp)
	movw	$13, -8(%rbp)
	cmpw	$13, -8(%rbp)
	jne	.L2
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L3
.L2:
	movl	$0, %eax
	call	killcard
.L3:
	movw	%ax, -8(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	cmpw	$14, -8(%rbp)
	jne	.L4
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L5
.L4:
	movl	$0, %eax
	call	killcard
.L5:
	movw	%ax, -8(%rbp)
	movw	$0, -10(%rbp)
	cmpw	$15, -8(%rbp)
	jne	.L6
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L7
.L6:
	movl	$0, %eax
	call	killcard
.L7:
	movw	%ax, -8(%rbp)
	movw	$1, -6(%rbp)
	cmpw	$16, -8(%rbp)
	jne	.L8
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L9
.L8:
	movl	$0, %eax
	call	killcard
.L9:
	movw	%ax, -8(%rbp)
	movl	$0, -4(%rbp)
	cmpw	$17, -8(%rbp)
	jne	.L10
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L11
.L10:
	movl	$0, %eax
	call	killcard
.L11:
	movw	%ax, -8(%rbp)
.L12:
	cmpw	$0, -10(%rbp)
	je	.L13
	cmpw	$5, -10(%rbp)
	je	.L13
	movl	$0, %eax
	call	killcard
	jmp	.L14
.L13:
	movl	$0, %eax
.L14:
	movw	%ax, -10(%rbp)
	movzwl	-10(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -10(%rbp)
	testw	%ax, %ax
	je	.L15
	movl	$0, %eax
	call	killcard
	jmp	.L16
.L15:
	movl	-4(%rbp), %eax
	cmpl	-32(%rbp), %eax
	setl	%al
	movzbl	%al, %eax
.L16:
	movw	%ax, -6(%rbp)
	cmpw	$0, -6(%rbp)
	jne	.L17
	nop
.L18:
	cmpw	$18, -8(%rbp)
	jne	.L27
	jmp	.L35
.L17:
	cmpw	$0, -6(%rbp)
	je	.L19
	cmpw	$1, -10(%rbp)
	jne	.L19
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L20
.L19:
	movl	$0, %eax
	call	killcard
.L20:
	movw	%ax, -10(%rbp)
	cmpw	$2, -10(%rbp)
	jne	.L21
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L22
.L21:
	movl	$0, %eax
	call	killcard
.L22:
	movw	%ax, -10(%rbp)
	movl	-4(%rbp), %eax
	movslq	%eax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	cmpw	$3, -10(%rbp)
	jne	.L23
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L24
.L23:
	movl	$0, %eax
	call	killcard
.L24:
	movw	%ax, -10(%rbp)
	addl	$1, -4(%rbp)
	cmpw	$4, -10(%rbp)
	jne	.L25
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L26
.L25:
	movl	$0, %eax
	call	killcard
.L26:
	movw	%ax, -10(%rbp)
	jmp	.L12
.L35:
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L28
.L27:
	movl	$0, %eax
	call	killcard
.L28:
	movw	%ax, -8(%rbp)
	cmpw	$1, -10(%rbp)
	jne	.L29
	cmpw	$0, -6(%rbp)
	je	.L30
.L29:
	movl	$0, %eax
	call	killcard
.L30:
	cmpw	$19, -8(%rbp)
	jne	.L31
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L32
.L31:
	movl	$0, %eax
	call	killcard
.L32:
	movw	%ax, -8(%rbp)
	movl	$10, %edi
	call	putchar
	cmpw	$20, -8(%rbp)
	jne	.L33
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L34
.L33:
	movl	$0, %eax
	call	killcard
.L34:
	movw	%ax, -8(%rbp)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	DUMP, .-DUMP
	.globl	gf_alog
	.type	gf_alog, @function
gf_alog:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movw	$13, -8(%rbp)
	cmpw	$13, -8(%rbp)
	jne	.L37
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L38
.L37:
	movl	$0, %eax
	call	killcard
.L38:
	movw	%ax, -8(%rbp)
	movb	$1, -14(%rbp)
	cmpw	$14, -8(%rbp)
	jne	.L39
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L40
.L39:
	movl	$0, %eax
	call	killcard
.L40:
	movw	%ax, -8(%rbp)
	cmpw	$15, -8(%rbp)
	jne	.L41
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L42
.L41:
	movl	$0, %eax
	call	killcard
.L42:
	movw	%ax, -8(%rbp)
	movw	$0, -12(%rbp)
	cmpw	$16, -8(%rbp)
	jne	.L43
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L44
.L43:
	movl	$0, %eax
	call	killcard
.L44:
	movw	%ax, -8(%rbp)
	movw	$1, -6(%rbp)
	cmpw	$17, -8(%rbp)
	jne	.L45
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L46
.L45:
	movl	$0, %eax
	call	killcard
.L46:
	movw	%ax, -8(%rbp)
.L47:
	cmpw	$0, -12(%rbp)
	je	.L48
	cmpw	$11, -12(%rbp)
	je	.L48
	movl	$0, %eax
	call	killcard
	jmp	.L49
.L48:
	movl	$0, %eax
.L49:
	movw	%ax, -12(%rbp)
	movzwl	-12(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -12(%rbp)
	testw	%ax, %ax
	je	.L50
	movl	$0, %eax
	call	killcard
	jmp	.L51
.L50:
	movzbl	-20(%rbp), %eax
	leal	-1(%rax), %edx
	movb	%dl, -20(%rbp)
	movzbl	%al, %eax
.L51:
	movw	%ax, -6(%rbp)
	cmpw	$0, -6(%rbp)
	jne	.L52
	nop
.L53:
	cmpw	$18, -8(%rbp)
	jne	.L82
	jmp	.L89
.L52:
	cmpw	$0, -6(%rbp)
	je	.L54
	cmpw	$1, -12(%rbp)
	jne	.L54
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L55
.L54:
	movl	$0, %eax
	call	killcard
.L55:
	movw	%ax, -12(%rbp)
	cmpw	$2, -12(%rbp)
	jne	.L56
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L57
.L56:
	movl	$0, %eax
	call	killcard
.L57:
	movw	%ax, -12(%rbp)
	movzbl	-14(%rbp), %eax
	movb	%al, -13(%rbp)
	cmpw	$3, -12(%rbp)
	jne	.L58
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L59
.L58:
	movl	$0, %eax
	call	killcard
.L59:
	movw	%ax, -12(%rbp)
	salb	-14(%rbp)
	cmpw	$4, -12(%rbp)
	jne	.L60
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L61
.L60:
	movl	$0, %eax
	call	killcard
.L61:
	movw	%ax, -12(%rbp)
	movw	$0, -10(%rbp)
	cmpw	$5, -12(%rbp)
	jne	.L62
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L63
.L62:
	movl	$0, %eax
	call	killcard
.L63:
	movw	%ax, -12(%rbp)
	movw	$0, -4(%rbp)
	cmpw	$6, -12(%rbp)
	jne	.L64
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L65
.L64:
	movl	$0, %eax
	call	killcard
.L65:
	movw	%ax, -12(%rbp)
	movw	$1, -2(%rbp)
	cmpw	$7, -12(%rbp)
	jne	.L66
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L67
.L66:
	movl	$0, %eax
	call	killcard
.L67:
	movw	%ax, -12(%rbp)
	movzbl	-13(%rbp), %eax
	andw	$128, %ax
	movw	%ax, -2(%rbp)
	cmpw	$0, -2(%rbp)
	je	.L68
	cmpw	$0, -10(%rbp)
	jne	.L69
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L70
.L69:
	movl	$0, %eax
	call	killcard
.L70:
	movw	%ax, -10(%rbp)
	xorb	$27, -14(%rbp)
	cmpw	$1, -10(%rbp)
	jne	.L71
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L72
.L71:
	movl	$0, %eax
	call	killcard
.L72:
	movw	%ax, -10(%rbp)
.L68:
	cmpw	$8, -12(%rbp)
	jne	.L73
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L74
.L73:
	movl	$0, %eax
	call	killcard
.L74:
	movw	%ax, -12(%rbp)
	cmpw	$2, -10(%rbp)
	jne	.L75
	cmpw	$0, -2(%rbp)
	jne	.L76
.L75:
	cmpw	$0, -10(%rbp)
	jne	.L77
	cmpw	$0, -2(%rbp)
	je	.L76
.L77:
	movl	$0, %eax
	call	killcard
.L76:
	cmpw	$9, -12(%rbp)
	jne	.L78
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L79
.L78:
	movl	$0, %eax
	call	killcard
.L79:
	movw	%ax, -12(%rbp)
	movzbl	-13(%rbp), %eax
	xorb	%al, -14(%rbp)
	cmpw	$10, -12(%rbp)
	jne	.L80
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L81
.L80:
	movl	$0, %eax
	call	killcard
.L81:
	movw	%ax, -12(%rbp)
	jmp	.L47
.L89:
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L83
.L82:
	movl	$0, %eax
	call	killcard
.L83:
	movw	%ax, -8(%rbp)
	cmpw	$1, -12(%rbp)
	jne	.L84
	cmpw	$0, -6(%rbp)
	je	.L85
.L84:
	movl	$0, %eax
	call	killcard
.L85:
	cmpw	$19, -8(%rbp)
	jne	.L86
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L87
.L86:
	movl	$0, %eax
	call	killcard
.L87:
	movw	%ax, -8(%rbp)
	movzbl	-14(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	gf_alog, .-gf_alog
	.globl	gf_log
	.type	gf_log, @function
gf_log:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movb	$1, -3(%rbp)
	movb	$0, -2(%rbp)
.L94:
	movzbl	-3(%rbp), %eax
	cmpb	-20(%rbp), %al
	jne	.L91
	jmp	.L92
.L91:
	movzbl	-3(%rbp), %eax
	movb	%al, -1(%rbp)
	salb	-3(%rbp)
	movzbl	-1(%rbp), %eax
	testb	%al, %al
	jns	.L93
	xorb	$27, -3(%rbp)
.L93:
	movzbl	-1(%rbp), %eax
	xorb	%al, -3(%rbp)
	addb	$1, -2(%rbp)
	cmpb	$0, -2(%rbp)
	jne	.L94
.L92:
	movzbl	-2(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	gf_log, .-gf_log
	.globl	gf_mulinv
	.type	gf_mulinv, @function
gf_mulinv:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movw	$13, -2(%rbp)
	cmpw	$13, -2(%rbp)
	jne	.L97
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L98
.L97:
	movl	$0, %eax
	call	killcard
.L98:
	movw	%ax, -2(%rbp)
	cmpb	$0, -20(%rbp)
	je	.L99
	movzbl	-20(%rbp), %eax
	movl	%eax, %edi
	call	gf_log
	notl	%eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	gf_alog
	jmp	.L100
.L99:
	movl	$0, %eax
.L100:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	gf_mulinv, .-gf_mulinv
	.globl	rj_sbox
	.type	rj_sbox, @function
rj_sbox:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movw	$13, -2(%rbp)
	cmpw	$13, -2(%rbp)
	jne	.L103
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L104
.L103:
	movl	$0, %eax
	call	killcard
.L104:
	movw	%ax, -2(%rbp)
	cmpw	$14, -2(%rbp)
	jne	.L105
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L106
.L105:
	movl	$0, %eax
	call	killcard
.L106:
	movw	%ax, -2(%rbp)
	movzbl	-20(%rbp), %eax
	movl	%eax, %edi
	call	gf_mulinv
	movb	%al, -4(%rbp)
	movzbl	-4(%rbp), %eax
	movb	%al, -3(%rbp)
	cmpw	$15, -2(%rbp)
	jne	.L107
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L108
.L107:
	movl	$0, %eax
	call	killcard
.L108:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	cmpw	$16, -2(%rbp)
	jne	.L109
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L110
.L109:
	movl	$0, %eax
	call	killcard
.L110:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	xorb	%al, -3(%rbp)
	cmpw	$17, -2(%rbp)
	jne	.L111
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L112
.L111:
	movl	$0, %eax
	call	killcard
.L112:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	cmpw	$18, -2(%rbp)
	jne	.L113
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L114
.L113:
	movl	$0, %eax
	call	killcard
.L114:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	xorb	%al, -3(%rbp)
	cmpw	$19, -2(%rbp)
	jne	.L115
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L116
.L115:
	movl	$0, %eax
	call	killcard
.L116:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	cmpw	$20, -2(%rbp)
	jne	.L117
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L118
.L117:
	movl	$0, %eax
	call	killcard
.L118:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	xorb	%al, -3(%rbp)
	cmpw	$21, -2(%rbp)
	jne	.L119
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L120
.L119:
	movl	$0, %eax
	call	killcard
.L120:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	cmpw	$22, -2(%rbp)
	jne	.L121
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L122
.L121:
	movl	$0, %eax
	call	killcard
.L122:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	xorb	%al, -3(%rbp)
	cmpw	$23, -2(%rbp)
	jne	.L123
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L124
.L123:
	movl	$0, %eax
	call	killcard
.L124:
	movw	%ax, -2(%rbp)
	movzbl	-3(%rbp), %eax
	xorl	$99, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	rj_sbox, .-rj_sbox
	.globl	rj_sbox_inv
	.type	rj_sbox_inv, @function
rj_sbox_inv:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movw	$13, -2(%rbp)
	cmpw	$13, -2(%rbp)
	jne	.L127
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L128
.L127:
	movl	$0, %eax
	call	killcard
.L128:
	movw	%ax, -2(%rbp)
	cmpw	$14, -2(%rbp)
	jne	.L129
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L130
.L129:
	movl	$0, %eax
	call	killcard
.L130:
	movw	%ax, -2(%rbp)
	movzbl	-20(%rbp), %eax
	xorl	$99, %eax
	movb	%al, -4(%rbp)
	cmpw	$15, -2(%rbp)
	jne	.L131
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L132
.L131:
	movl	$0, %eax
	call	killcard
.L132:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$7, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	movzbl	-4(%rbp), %eax
	movb	%al, -3(%rbp)
	cmpw	$16, -2(%rbp)
	jne	.L133
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L134
.L133:
	movl	$0, %eax
	call	killcard
.L134:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	sall	$2, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$6, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	cmpw	$17, -2(%rbp)
	jne	.L135
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L136
.L135:
	movl	$0, %eax
	call	killcard
.L136:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	xorb	%al, -3(%rbp)
	cmpw	$18, -2(%rbp)
	jne	.L137
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L138
.L137:
	movl	$0, %eax
	call	killcard
.L138:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	sall	$3, %eax
	movl	%eax, %edx
	movzbl	-4(%rbp), %eax
	shrb	$5, %al
	orl	%edx, %eax
	movb	%al, -4(%rbp)
	cmpw	$19, -2(%rbp)
	jne	.L139
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L140
.L139:
	movl	$0, %eax
	call	killcard
.L140:
	movw	%ax, -2(%rbp)
	movzbl	-4(%rbp), %eax
	xorb	%al, -3(%rbp)
	cmpw	$20, -2(%rbp)
	jne	.L141
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L142
.L141:
	movl	$0, %eax
	call	killcard
.L142:
	movw	%ax, -2(%rbp)
	movzbl	-3(%rbp), %eax
	movl	%eax, %edi
	call	gf_mulinv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	rj_sbox_inv, .-rj_sbox_inv
	.globl	rj_xtime
	.type	rj_xtime, @function
rj_xtime:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, %eax
	movb	%al, -20(%rbp)
	movw	$13, -2(%rbp)
	cmpw	$13, -2(%rbp)
	jne	.L145
	movzwl	-2(%rbp), %eax
	addl	$1, %eax
	jmp	.L146
.L145:
	movl	$0, %eax
	call	killcard
.L146:
	movw	%ax, -2(%rbp)
	movzbl	-20(%rbp), %eax
	testb	%al, %al
	jns	.L147
	movzbl	-20(%rbp), %eax
	addl	%eax, %eax
	xorl	$27, %eax
	jmp	.L148
.L147:
	movzbl	-20(%rbp), %eax
	addl	%eax, %eax
.L148:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	rj_xtime, .-rj_xtime
	.globl	aes_subBytes
	.type	aes_subBytes, @function
aes_subBytes:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L151
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L152
.L151:
	movl	$0, %eax
	call	killcard
.L152:
	movw	%ax, -24(%rbp)
	movl	$16, %ebx
	cmpw	$14, -24(%rbp)
	jne	.L153
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L154
.L153:
	movl	$0, %eax
	call	killcard
.L154:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$15, -24(%rbp)
	jne	.L155
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L156
.L155:
	movl	$0, %eax
	call	killcard
.L156:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$16, -24(%rbp)
	jne	.L157
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L158
.L157:
	movl	$0, %eax
	call	killcard
.L158:
	movw	%ax, -24(%rbp)
.L159:
	cmpw	$0, -26(%rbp)
	je	.L160
	cmpw	$4, -26(%rbp)
	je	.L160
	movl	$0, %eax
	call	killcard
	jmp	.L161
.L160:
	movl	$0, %eax
.L161:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L162
	movl	$0, %eax
	call	killcard
	jmp	.L163
.L162:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	movzbl	%al, %eax
.L163:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L164
	nop
.L165:
	cmpw	$17, -24(%rbp)
	jne	.L172
	jmp	.L180
.L164:
	cmpw	$0, -22(%rbp)
	je	.L166
	cmpw	$1, -26(%rbp)
	jne	.L166
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L167
.L166:
	movl	$0, %eax
	call	killcard
.L167:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L168
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L169
.L168:
	movl	$0, %eax
	call	killcard
.L169:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movb	%al, (%r12)
	cmpw	$3, -26(%rbp)
	jne	.L170
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L171
.L170:
	movl	$0, %eax
	call	killcard
.L171:
	movw	%ax, -26(%rbp)
	jmp	.L159
.L180:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L173
.L172:
	movl	$0, %eax
	call	killcard
.L173:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L174
	cmpw	$0, -22(%rbp)
	je	.L175
.L174:
	movl	$0, %eax
	call	killcard
.L175:
	cmpw	$18, -24(%rbp)
	jne	.L176
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L177
.L176:
	movl	$0, %eax
	call	killcard
.L177:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$19, -24(%rbp)
	jne	.L178
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L179
.L178:
	movl	$0, %eax
	call	killcard
.L179:
	movw	%ax, -24(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	aes_subBytes, .-aes_subBytes
	.globl	aes_subBytes_inv
	.type	aes_subBytes_inv, @function
aes_subBytes_inv:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L182
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L183
.L182:
	movl	$0, %eax
	call	killcard
.L183:
	movw	%ax, -24(%rbp)
	movl	$16, %ebx
	cmpw	$14, -24(%rbp)
	jne	.L184
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L185
.L184:
	movl	$0, %eax
	call	killcard
.L185:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$15, -24(%rbp)
	jne	.L186
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L187
.L186:
	movl	$0, %eax
	call	killcard
.L187:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$16, -24(%rbp)
	jne	.L188
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L189
.L188:
	movl	$0, %eax
	call	killcard
.L189:
	movw	%ax, -24(%rbp)
.L190:
	cmpw	$0, -26(%rbp)
	je	.L191
	cmpw	$4, -26(%rbp)
	je	.L191
	movl	$0, %eax
	call	killcard
	jmp	.L192
.L191:
	movl	$0, %eax
.L192:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L193
	movl	$0, %eax
	call	killcard
	jmp	.L194
.L193:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	movzbl	%al, %eax
.L194:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L195
	nop
.L196:
	cmpw	$17, -24(%rbp)
	jne	.L203
	jmp	.L211
.L195:
	cmpw	$0, -22(%rbp)
	je	.L197
	cmpw	$1, -26(%rbp)
	jne	.L197
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L198
.L197:
	movl	$0, %eax
	call	killcard
.L198:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L199
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L200
.L199:
	movl	$0, %eax
	call	killcard
.L200:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox_inv
	movb	%al, (%r12)
	cmpw	$3, -26(%rbp)
	jne	.L201
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L202
.L201:
	movl	$0, %eax
	call	killcard
.L202:
	movw	%ax, -26(%rbp)
	jmp	.L190
.L211:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L204
.L203:
	movl	$0, %eax
	call	killcard
.L204:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L205
	cmpw	$0, -22(%rbp)
	je	.L206
.L205:
	movl	$0, %eax
	call	killcard
.L206:
	cmpw	$18, -24(%rbp)
	jne	.L207
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L208
.L207:
	movl	$0, %eax
	call	killcard
.L208:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$19, -24(%rbp)
	jne	.L209
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L210
.L209:
	movl	$0, %eax
	call	killcard
.L210:
	movw	%ax, -24(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	aes_subBytes_inv, .-aes_subBytes_inv
	.globl	aes_addRoundKey
	.type	aes_addRoundKey, @function
aes_addRoundKey:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L213
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L214
.L213:
	movl	$0, %eax
	call	killcard
.L214:
	movw	%ax, -24(%rbp)
	movl	$16, %ebx
	cmpw	$14, -24(%rbp)
	jne	.L215
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L216
.L215:
	movl	$0, %eax
	call	killcard
.L216:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$15, -24(%rbp)
	jne	.L217
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L218
.L217:
	movl	$0, %eax
	call	killcard
.L218:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$16, -24(%rbp)
	jne	.L219
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L220
.L219:
	movl	$0, %eax
	call	killcard
.L220:
	movw	%ax, -24(%rbp)
.L221:
	cmpw	$0, -26(%rbp)
	je	.L222
	cmpw	$4, -26(%rbp)
	je	.L222
	movl	$0, %eax
	call	killcard
	jmp	.L223
.L222:
	movl	$0, %eax
.L223:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L224
	movl	$0, %eax
	call	killcard
	jmp	.L225
.L224:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	movzbl	%al, %eax
.L225:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L226
	nop
.L227:
	cmpw	$17, -24(%rbp)
	jne	.L234
	jmp	.L242
.L226:
	cmpw	$0, -22(%rbp)
	je	.L228
	cmpw	$1, -26(%rbp)
	jne	.L228
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L229
.L228:
	movl	$0, %eax
	call	killcard
.L229:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L230
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L231
.L230:
	movl	$0, %eax
	call	killcard
.L231:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %esi
	movq	-48(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$3, -26(%rbp)
	jne	.L232
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L233
.L232:
	movl	$0, %eax
	call	killcard
.L233:
	movw	%ax, -26(%rbp)
	jmp	.L221
.L242:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L235
.L234:
	movl	$0, %eax
	call	killcard
.L235:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L236
	cmpw	$0, -22(%rbp)
	je	.L237
.L236:
	movl	$0, %eax
	call	killcard
.L237:
	cmpw	$18, -24(%rbp)
	jne	.L238
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L239
.L238:
	movl	$0, %eax
	call	killcard
.L239:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$19, -24(%rbp)
	jne	.L240
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L241
.L240:
	movl	$0, %eax
	call	killcard
.L241:
	movw	%ax, -24(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	aes_addRoundKey, .-aes_addRoundKey
	.globl	aes_addRoundKey_cpy
	.type	aes_addRoundKey_cpy, @function
aes_addRoundKey_cpy:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L244
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L245
.L244:
	movl	$0, %eax
	call	killcard
.L245:
	movw	%ax, -24(%rbp)
	movl	$16, %ebx
	cmpw	$14, -24(%rbp)
	jne	.L246
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L247
.L246:
	movl	$0, %eax
	call	killcard
.L247:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$15, -24(%rbp)
	jne	.L248
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L249
.L248:
	movl	$0, %eax
	call	killcard
.L249:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$16, -24(%rbp)
	jne	.L250
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L251
.L250:
	movl	$0, %eax
	call	killcard
.L251:
	movw	%ax, -24(%rbp)
.L252:
	cmpw	$0, -26(%rbp)
	je	.L253
	cmpw	$6, -26(%rbp)
	je	.L253
	movl	$0, %eax
	call	killcard
	jmp	.L254
.L253:
	movl	$0, %eax
.L254:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L255
	movl	$0, %eax
	call	killcard
	jmp	.L256
.L255:
	movl	%ebx, %eax
	leal	-1(%rax), %ebx
	movzbl	%al, %eax
.L256:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L257
	nop
.L258:
	cmpw	$17, -24(%rbp)
	jne	.L269
	jmp	.L277
.L257:
	cmpw	$0, -22(%rbp)
	je	.L259
	cmpw	$1, -26(%rbp)
	jne	.L259
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L260
.L259:
	movl	$0, %eax
	call	killcard
.L260:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L261
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L262
.L261:
	movl	$0, %eax
	call	killcard
.L262:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %esi
	movq	-48(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$3, -26(%rbp)
	jne	.L263
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L264
.L263:
	movl	$0, %eax
	call	killcard
.L264:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-48(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$4, -26(%rbp)
	jne	.L265
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L266
.L265:
	movl	$0, %eax
	call	killcard
.L266:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	addl	$16, %eax
	movslq	%eax, %rdx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	addl	$16, %eax
	movslq	%eax, %rcx
	movq	-48(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$5, -26(%rbp)
	jne	.L267
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L268
.L267:
	movl	$0, %eax
	call	killcard
.L268:
	movw	%ax, -26(%rbp)
	jmp	.L252
.L277:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L270
.L269:
	movl	$0, %eax
	call	killcard
.L270:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L271
	cmpw	$0, -22(%rbp)
	je	.L272
.L271:
	movl	$0, %eax
	call	killcard
.L272:
	cmpw	$18, -24(%rbp)
	jne	.L273
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L274
.L273:
	movl	$0, %eax
	call	killcard
.L274:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$19, -24(%rbp)
	jne	.L275
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L276
.L275:
	movl	$0, %eax
	call	killcard
.L276:
	movw	%ax, -24(%rbp)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
	.size	aes_addRoundKey_cpy, .-aes_addRoundKey_cpy
	.globl	aes_shiftRows
	.type	aes_shiftRows, @function
aes_shiftRows:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movw	$13, -18(%rbp)
	cmpw	$13, -18(%rbp)
	jne	.L279
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L280
.L279:
	movl	$0, %eax
	call	killcard
.L280:
	movw	%ax, -18(%rbp)
	cmpw	$14, -18(%rbp)
	jne	.L281
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L282
.L281:
	movl	$0, %eax
	call	killcard
.L282:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	1(%rax), %ebx
	cmpw	$15, -18(%rbp)
	jne	.L283
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L284
.L283:
	movl	$0, %eax
	call	killcard
.L284:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	5(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$16, -18(%rbp)
	jne	.L285
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L286
.L285:
	movl	$0, %eax
	call	killcard
.L286:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	9(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$17, -18(%rbp)
	jne	.L287
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L288
.L287:
	movl	$0, %eax
	call	killcard
.L288:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	13(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$18, -18(%rbp)
	jne	.L289
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L290
.L289:
	movl	$0, %eax
	call	killcard
.L290:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$13, %rax
	movb	%bl, (%rax)
	cmpw	$19, -18(%rbp)
	jne	.L291
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L292
.L291:
	movl	$0, %eax
	call	killcard
.L292:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	10(%rax), %ebx
	cmpw	$20, -18(%rbp)
	jne	.L293
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L294
.L293:
	movl	$0, %eax
	call	killcard
.L294:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	2(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$21, -18(%rbp)
	jne	.L295
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L296
.L295:
	movl	$0, %eax
	call	killcard
.L296:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$2, %rax
	movb	%bl, (%rax)
	cmpw	$22, -18(%rbp)
	jne	.L297
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L298
.L297:
	movl	$0, %eax
	call	killcard
.L298:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	3(%rax), %ebx
	cmpw	$23, -18(%rbp)
	jne	.L299
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L300
.L299:
	movl	$0, %eax
	call	killcard
.L300:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	15(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$24, -18(%rbp)
	jne	.L301
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L302
.L301:
	movl	$0, %eax
	call	killcard
.L302:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	15(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	11(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$25, -18(%rbp)
	jne	.L303
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L304
.L303:
	movl	$0, %eax
	call	killcard
.L304:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	7(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$26, -18(%rbp)
	jne	.L305
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L306
.L305:
	movl	$0, %eax
	call	killcard
.L306:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$7, %rax
	movb	%bl, (%rax)
	cmpw	$27, -18(%rbp)
	jne	.L307
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L308
.L307:
	movl	$0, %eax
	call	killcard
.L308:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	14(%rax), %ebx
	cmpw	$28, -18(%rbp)
	jne	.L309
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L310
.L309:
	movl	$0, %eax
	call	killcard
.L310:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	14(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	6(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$29, -18(%rbp)
	jne	.L311
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L312
.L311:
	movl	$0, %eax
	call	killcard
.L312:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$6, %rax
	movb	%bl, (%rax)
	cmpw	$30, -18(%rbp)
	jne	.L313
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L314
.L313:
	movl	$0, %eax
	call	killcard
.L314:
	movw	%ax, -18(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	aes_shiftRows, .-aes_shiftRows
	.globl	aes_shiftRows_inv
	.type	aes_shiftRows_inv, @function
aes_shiftRows_inv:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movw	$13, -18(%rbp)
	cmpw	$13, -18(%rbp)
	jne	.L316
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L317
.L316:
	movl	$0, %eax
	call	killcard
.L317:
	movw	%ax, -18(%rbp)
	cmpw	$14, -18(%rbp)
	jne	.L318
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L319
.L318:
	movl	$0, %eax
	call	killcard
.L319:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	1(%rax), %ebx
	cmpw	$15, -18(%rbp)
	jne	.L320
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L321
.L320:
	movl	$0, %eax
	call	killcard
.L321:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	13(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$16, -18(%rbp)
	jne	.L322
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L323
.L322:
	movl	$0, %eax
	call	killcard
.L323:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	9(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$17, -18(%rbp)
	jne	.L324
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L325
.L324:
	movl	$0, %eax
	call	killcard
.L325:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	5(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$18, -18(%rbp)
	jne	.L326
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L327
.L326:
	movl	$0, %eax
	call	killcard
.L327:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$5, %rax
	movb	%bl, (%rax)
	cmpw	$19, -18(%rbp)
	jne	.L328
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L329
.L328:
	movl	$0, %eax
	call	killcard
.L329:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	2(%rax), %ebx
	cmpw	$20, -18(%rbp)
	jne	.L330
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L331
.L330:
	movl	$0, %eax
	call	killcard
.L331:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	10(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$21, -18(%rbp)
	jne	.L332
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L333
.L332:
	movl	$0, %eax
	call	killcard
.L333:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$10, %rax
	movb	%bl, (%rax)
	cmpw	$22, -18(%rbp)
	jne	.L334
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L335
.L334:
	movl	$0, %eax
	call	killcard
.L335:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	3(%rax), %ebx
	cmpw	$23, -18(%rbp)
	jne	.L336
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L337
.L336:
	movl	$0, %eax
	call	killcard
.L337:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	7(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$24, -18(%rbp)
	jne	.L338
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L339
.L338:
	movl	$0, %eax
	call	killcard
.L339:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	11(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$25, -18(%rbp)
	jne	.L340
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L341
.L340:
	movl	$0, %eax
	call	killcard
.L341:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	15(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$26, -18(%rbp)
	jne	.L342
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L343
.L342:
	movl	$0, %eax
	call	killcard
.L343:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$15, %rax
	movb	%bl, (%rax)
	cmpw	$27, -18(%rbp)
	jne	.L344
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L345
.L344:
	movl	$0, %eax
	call	killcard
.L345:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	movzbl	6(%rax), %ebx
	cmpw	$28, -18(%rbp)
	jne	.L346
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L347
.L346:
	movl	$0, %eax
	call	killcard
.L347:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-40(%rbp), %rax
	movzbl	14(%rax), %eax
	movb	%al, (%rdx)
	cmpw	$29, -18(%rbp)
	jne	.L348
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L349
.L348:
	movl	$0, %eax
	call	killcard
.L349:
	movw	%ax, -18(%rbp)
	movq	-40(%rbp), %rax
	addq	$14, %rax
	movb	%bl, (%rax)
	cmpw	$30, -18(%rbp)
	jne	.L350
	movzwl	-18(%rbp), %eax
	addl	$1, %eax
	jmp	.L351
.L350:
	movl	$0, %eax
	call	killcard
.L351:
	movw	%ax, -18(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	aes_shiftRows_inv, .-aes_shiftRows_inv
	.globl	aes_mixColumns
	.type	aes_mixColumns, @function
aes_mixColumns:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movw	$13, -56(%rbp)
	cmpw	$13, -56(%rbp)
	jne	.L353
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L354
.L353:
	movl	$0, %eax
	call	killcard
.L354:
	movw	%ax, -56(%rbp)
	cmpw	$14, -56(%rbp)
	jne	.L355
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L356
.L355:
	movl	$0, %eax
	call	killcard
.L356:
	movw	%ax, -56(%rbp)
	movw	$0, -58(%rbp)
	cmpw	$15, -56(%rbp)
	jne	.L357
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L358
.L357:
	movl	$0, %eax
	call	killcard
.L358:
	movw	%ax, -56(%rbp)
	movw	$1, -54(%rbp)
	cmpw	$16, -56(%rbp)
	jne	.L359
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L360
.L359:
	movl	$0, %eax
	call	killcard
.L360:
	movw	%ax, -56(%rbp)
	movl	$0, %ebx
	cmpw	$17, -56(%rbp)
	jne	.L361
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L362
.L361:
	movl	$0, %eax
	call	killcard
.L362:
	movw	%ax, -56(%rbp)
.L363:
	cmpw	$0, -58(%rbp)
	je	.L364
	cmpw	$13, -58(%rbp)
	je	.L364
	movl	$0, %eax
	call	killcard
	jmp	.L365
.L364:
	movl	$0, %eax
.L365:
	movw	%ax, -58(%rbp)
	movzwl	-58(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -58(%rbp)
	testw	%ax, %ax
	je	.L366
	movl	$0, %eax
	call	killcard
	jmp	.L367
.L366:
	cmpb	$15, %bl
	setbe	%al
	movzbl	%al, %eax
.L367:
	movw	%ax, -54(%rbp)
	cmpw	$0, -54(%rbp)
	jne	.L368
	nop
.L369:
	cmpw	$18, -56(%rbp)
	jne	.L394
	jmp	.L402
.L368:
	cmpw	$0, -54(%rbp)
	je	.L370
	cmpw	$1, -58(%rbp)
	jne	.L370
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L371
.L370:
	movl	$0, %eax
	call	killcard
.L371:
	movw	%ax, -58(%rbp)
	cmpw	$2, -58(%rbp)
	jne	.L372
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L373
.L372:
	movl	$0, %eax
	call	killcard
.L373:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %edx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r15d
	cmpw	$3, -58(%rbp)
	jne	.L374
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L375
.L374:
	movl	$0, %eax
	call	killcard
.L375:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	cmpw	$4, -58(%rbp)
	jne	.L376
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L377
.L376:
	movl	$0, %eax
	call	killcard
.L377:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	cmpw	$5, -58(%rbp)
	jne	.L378
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L379
.L378:
	movl	$0, %eax
	call	killcard
.L379:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -73(%rbp)
	cmpw	$6, -58(%rbp)
	jne	.L380
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L381
.L380:
	movl	$0, %eax
	call	killcard
.L381:
	movw	%ax, -58(%rbp)
	movl	%r15d, %eax
	xorl	%r14d, %eax
	xorl	%r13d, %eax
	xorb	-73(%rbp), %al
	movl	%eax, %r12d
	cmpw	$7, -58(%rbp)
	jne	.L382
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L383
.L382:
	movl	$0, %eax
	call	killcard
.L383:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %edx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movzbl	%bl, %edx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %esi
	movb	%sil, -74(%rbp)
	movl	%r15d, %eax
	xorl	%r14d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-74(%rbp), %al
	movq	-88(%rbp), %rcx
	movb	%al, (%rcx)
	cmpw	$8, -58(%rbp)
	jne	.L384
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L385
.L384:
	movl	$0, %eax
	call	killcard
.L385:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %esi
	movb	%sil, -74(%rbp)
	movl	%r14d, %eax
	xorl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-74(%rbp), %al
	movq	-88(%rbp), %rcx
	movb	%al, (%rcx)
	cmpw	$9, -58(%rbp)
	jne	.L386
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L387
.L386:
	movl	$0, %eax
	call	killcard
.L387:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %r14
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -88(%rbp)
	movl	%r13d, %eax
	xorb	-73(%rbp), %al
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorb	-88(%rbp), %al
	movb	%al, (%r14)
	cmpw	$10, -58(%rbp)
	jne	.L388
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L389
.L388:
	movl	$0, %eax
	call	killcard
.L389:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %r13
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	movzbl	-73(%rbp), %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorl	%r12d, %eax
	xorl	%r14d, %eax
	movb	%al, 0(%r13)
	cmpw	$11, -58(%rbp)
	jne	.L390
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L391
.L390:
	movl	$0, %eax
	call	killcard
.L391:
	movw	%ax, -58(%rbp)
	addl	$4, %ebx
	cmpw	$12, -58(%rbp)
	jne	.L392
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L393
.L392:
	movl	$0, %eax
	call	killcard
.L393:
	movw	%ax, -58(%rbp)
	jmp	.L363
.L402:
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L395
.L394:
	movl	$0, %eax
	call	killcard
.L395:
	movw	%ax, -56(%rbp)
	cmpw	$1, -58(%rbp)
	jne	.L396
	cmpw	$0, -54(%rbp)
	je	.L397
.L396:
	movl	$0, %eax
	call	killcard
.L397:
	cmpw	$19, -56(%rbp)
	jne	.L398
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L399
.L398:
	movl	$0, %eax
	call	killcard
.L399:
	movw	%ax, -56(%rbp)
	movl	$1, -52(%rbp)
	cmpw	$20, -56(%rbp)
	jne	.L400
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L401
.L400:
	movl	$0, %eax
	call	killcard
.L401:
	movw	%ax, -56(%rbp)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	aes_mixColumns, .-aes_mixColumns
	.globl	aes_mixColumns_inv
	.type	aes_mixColumns_inv, @function
aes_mixColumns_inv:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movw	$13, -56(%rbp)
	cmpw	$13, -56(%rbp)
	jne	.L404
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L405
.L404:
	movl	$0, %eax
	call	killcard
.L405:
	movw	%ax, -56(%rbp)
	cmpw	$14, -56(%rbp)
	jne	.L406
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L407
.L406:
	movl	$0, %eax
	call	killcard
.L407:
	movw	%ax, -56(%rbp)
	movw	$0, -58(%rbp)
	cmpw	$15, -56(%rbp)
	jne	.L408
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L409
.L408:
	movl	$0, %eax
	call	killcard
.L409:
	movw	%ax, -56(%rbp)
	movw	$1, -54(%rbp)
	cmpw	$16, -56(%rbp)
	jne	.L410
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L411
.L410:
	movl	$0, %eax
	call	killcard
.L411:
	movw	%ax, -56(%rbp)
	movl	$0, %ebx
	cmpw	$17, -56(%rbp)
	jne	.L412
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L413
.L412:
	movl	$0, %eax
	call	killcard
.L413:
	movw	%ax, -56(%rbp)
.L414:
	cmpw	$0, -58(%rbp)
	je	.L415
	cmpw	$16, -58(%rbp)
	je	.L415
	movl	$0, %eax
	call	killcard
	jmp	.L416
.L415:
	movl	$0, %eax
.L416:
	movw	%ax, -58(%rbp)
	movzwl	-58(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -58(%rbp)
	testw	%ax, %ax
	je	.L417
	movl	$0, %eax
	call	killcard
	jmp	.L418
.L417:
	cmpb	$15, %bl
	setbe	%al
	movzbl	%al, %eax
.L418:
	movw	%ax, -54(%rbp)
	cmpw	$0, -54(%rbp)
	jne	.L419
	nop
.L420:
	cmpw	$18, -56(%rbp)
	jne	.L451
	jmp	.L459
.L419:
	cmpw	$0, -54(%rbp)
	je	.L421
	cmpw	$1, -58(%rbp)
	jne	.L421
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L422
.L421:
	movl	$0, %eax
	call	killcard
.L422:
	movw	%ax, -58(%rbp)
	cmpw	$2, -58(%rbp)
	jne	.L423
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L424
.L423:
	movl	$0, %eax
	call	killcard
.L424:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %edx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r14d
	cmpw	$3, -58(%rbp)
	jne	.L425
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L426
.L425:
	movl	$0, %eax
	call	killcard
.L426:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	cmpw	$4, -58(%rbp)
	jne	.L427
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L428
.L427:
	movl	$0, %eax
	call	killcard
.L428:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r12d
	cmpw	$5, -58(%rbp)
	jne	.L429
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L430
.L429:
	movl	$0, %eax
	call	killcard
.L430:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r15d
	cmpw	$6, -58(%rbp)
	jne	.L431
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L432
.L431:
	movl	$0, %eax
	call	killcard
.L432:
	movw	%ax, -58(%rbp)
	movl	%r14d, %eax
	xorl	%r13d, %eax
	xorl	%r12d, %eax
	xorl	%r15d, %eax
	movb	%al, -73(%rbp)
	cmpw	$7, -58(%rbp)
	jne	.L433
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L434
.L433:
	movl	$0, %eax
	call	killcard
.L434:
	movw	%ax, -58(%rbp)
	movzbl	-73(%rbp), %eax
	movl	%eax, %edi
	call	rj_xtime
	movb	%al, -88(%rbp)
	cmpw	$8, -58(%rbp)
	jne	.L435
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L436
.L435:
	movl	$0, %eax
	call	killcard
.L436:
	movw	%ax, -58(%rbp)
	movzbl	-88(%rbp), %eax
	xorl	%r14d, %eax
	xorl	%r12d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-73(%rbp), %al
	movb	%al, -74(%rbp)
	cmpw	$9, -58(%rbp)
	jne	.L437
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L438
.L437:
	movl	$0, %eax
	call	killcard
.L438:
	movw	%ax, -58(%rbp)
	movzbl	-88(%rbp), %eax
	xorl	%r13d, %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-73(%rbp), %al
	movb	%al, -73(%rbp)
	cmpw	$10, -58(%rbp)
	jne	.L439
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L440
.L439:
	movl	$0, %eax
	call	killcard
.L440:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %edx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movzbl	%bl, %edx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %esi
	movb	%sil, -75(%rbp)
	movl	%r14d, %eax
	xorl	%r13d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-74(%rbp), %al
	xorb	-75(%rbp), %al
	movq	-88(%rbp), %rcx
	movb	%al, (%rcx)
	cmpw	$11, -58(%rbp)
	jne	.L441
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L442
.L441:
	movl	$0, %eax
	call	killcard
.L442:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %esi
	movb	%sil, -75(%rbp)
	movl	%r13d, %eax
	xorl	%r12d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-73(%rbp), %al
	xorb	-75(%rbp), %al
	movq	-88(%rbp), %rcx
	movb	%al, (%rcx)
	cmpw	$12, -58(%rbp)
	jne	.L443
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L444
.L443:
	movl	$0, %eax
	call	killcard
.L444:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %r13
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -88(%rbp)
	movl	%r12d, %eax
	xorl	%r15d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-74(%rbp), %al
	xorb	-88(%rbp), %al
	movb	%al, 0(%r13)
	cmpw	$13, -58(%rbp)
	jne	.L445
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L446
.L445:
	movl	$0, %eax
	call	killcard
.L446:
	movw	%ax, -58(%rbp)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %r12
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %r13d
	movl	%r15d, %eax
	xorl	%r14d, %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_xtime
	xorb	-73(%rbp), %al
	xorl	%r13d, %eax
	movb	%al, (%r12)
	cmpw	$14, -58(%rbp)
	jne	.L447
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L448
.L447:
	movl	$0, %eax
	call	killcard
.L448:
	movw	%ax, -58(%rbp)
	addl	$4, %ebx
	cmpw	$15, -58(%rbp)
	jne	.L449
	movzwl	-58(%rbp), %eax
	addl	$1, %eax
	jmp	.L450
.L449:
	movl	$0, %eax
	call	killcard
.L450:
	movw	%ax, -58(%rbp)
	jmp	.L414
.L459:
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L452
.L451:
	movl	$0, %eax
	call	killcard
.L452:
	movw	%ax, -56(%rbp)
	cmpw	$1, -58(%rbp)
	jne	.L453
	cmpw	$0, -54(%rbp)
	je	.L454
.L453:
	movl	$0, %eax
	call	killcard
.L454:
	cmpw	$19, -56(%rbp)
	jne	.L455
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L456
.L455:
	movl	$0, %eax
	call	killcard
.L456:
	movw	%ax, -56(%rbp)
	movl	$1, -52(%rbp)
	cmpw	$20, -56(%rbp)
	jne	.L457
	movzwl	-56(%rbp), %eax
	addl	$1, %eax
	jmp	.L458
.L457:
	movl	$0, %eax
	call	killcard
.L458:
	movw	%ax, -56(%rbp)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	aes_mixColumns_inv, .-aes_mixColumns_inv
	.globl	aes_expandEncKey
	.type	aes_expandEncKey, @function
aes_expandEncKey:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L461
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L462
.L461:
	movl	$0, %eax
	call	killcard
.L462:
	movw	%ax, -24(%rbp)
	cmpw	$14, -24(%rbp)
	jne	.L463
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L464
.L463:
	movl	$0, %eax
	call	killcard
.L464:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	movzbl	(%rax), %ebx
	movq	-40(%rbp), %rax
	addq	$29, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movq	-48(%rbp), %rdx
	movzbl	(%rdx), %edx
	xorl	%edx, %eax
	xorl	%eax, %ebx
	movl	%ebx, %edx
	movq	-40(%rbp), %rax
	movb	%dl, (%rax)
	cmpw	$15, -24(%rbp)
	jne	.L465
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L466
.L465:
	movl	$0, %eax
	call	killcard
.L466:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$30, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$16, -24(%rbp)
	jne	.L467
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L468
.L467:
	movl	$0, %eax
	call	killcard
.L468:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	2(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$2, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$31, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$17, -24(%rbp)
	jne	.L469
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L470
.L469:
	movl	$0, %eax
	call	killcard
.L470:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$18, -24(%rbp)
	jne	.L471
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L472
.L471:
	movl	$0, %eax
	call	killcard
.L472:
	movw	%ax, -24(%rbp)
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, %ecx
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	shrb	$7, %al
	movl	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	leal	0(,%rax,8), %edx
	addl	%edx, %eax
	xorl	%ecx, %eax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	cmpw	$19, -24(%rbp)
	jne	.L473
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L474
.L473:
	movl	$0, %eax
	call	killcard
.L474:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$20, -24(%rbp)
	jne	.L475
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L476
.L475:
	movl	$0, %eax
	call	killcard
.L476:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$21, -24(%rbp)
	jne	.L477
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L478
.L477:
	movl	$0, %eax
	call	killcard
.L478:
	movw	%ax, -24(%rbp)
	movl	$4, %ebx
	cmpw	$22, -24(%rbp)
	jne	.L479
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L480
.L479:
	movl	$0, %eax
	call	killcard
.L480:
	movw	%ax, -24(%rbp)
.L481:
	cmpw	$0, -26(%rbp)
	je	.L482
	cmpw	$8, -26(%rbp)
	je	.L482
	movl	$0, %eax
	call	killcard
	jmp	.L483
.L482:
	movl	$0, %eax
.L483:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L484
	movl	$0, %eax
	call	killcard
	jmp	.L485
.L484:
	cmpb	$15, %bl
	setbe	%al
	movzbl	%al, %eax
.L485:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L486
	nop
.L487:
	cmpw	$23, -24(%rbp)
	jne	.L502
	jmp	.L551
.L486:
	cmpw	$0, -22(%rbp)
	je	.L488
	cmpw	$1, -26(%rbp)
	jne	.L488
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L489
.L488:
	movl	$0, %eax
	call	killcard
.L489:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L490
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L491
.L490:
	movl	$0, %eax
	call	killcard
.L491:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$3, -26(%rbp)
	jne	.L492
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L493
.L492:
	movl	$0, %eax
	call	killcard
.L493:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$4, -26(%rbp)
	jne	.L494
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L495
.L494:
	movl	$0, %eax
	call	killcard
.L495:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$5, -26(%rbp)
	jne	.L496
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L497
.L496:
	movl	$0, %eax
	call	killcard
.L497:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$6, -26(%rbp)
	jne	.L498
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L499
.L498:
	movl	$0, %eax
	call	killcard
.L499:
	movw	%ax, -26(%rbp)
	addl	$4, %ebx
	cmpw	$7, -26(%rbp)
	jne	.L500
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L501
.L500:
	movl	$0, %eax
	call	killcard
.L501:
	movw	%ax, -26(%rbp)
	jmp	.L481
.L551:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L503
.L502:
	movl	$0, %eax
	call	killcard
.L503:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L504
	cmpw	$0, -22(%rbp)
	je	.L505
.L504:
	movl	$0, %eax
	call	killcard
.L505:
	cmpw	$24, -24(%rbp)
	jne	.L506
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L507
.L506:
	movl	$0, %eax
	call	killcard
.L507:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$25, -24(%rbp)
	jne	.L508
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L509
.L508:
	movl	$0, %eax
	call	killcard
.L509:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	17(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$17, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$13, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$26, -24(%rbp)
	jne	.L510
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L511
.L510:
	movl	$0, %eax
	call	killcard
.L511:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	18(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$18, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$14, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$27, -24(%rbp)
	jne	.L512
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L513
.L512:
	movl	$0, %eax
	call	killcard
.L513:
	movw	%ax, -24(%rbp)
	movq	-40(%rbp), %rax
	leaq	19(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$19, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$15, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$28, -24(%rbp)
	jne	.L514
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L515
.L514:
	movl	$0, %eax
	call	killcard
.L515:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$29, -24(%rbp)
	jne	.L516
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L517
.L516:
	movl	$0, %eax
	call	killcard
.L517:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$30, -24(%rbp)
	jne	.L518
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L519
.L518:
	movl	$0, %eax
	call	killcard
.L519:
	movw	%ax, -24(%rbp)
	movl	$20, %ebx
	cmpw	$31, -24(%rbp)
	jne	.L520
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L521
.L520:
	movl	$0, %eax
	call	killcard
.L521:
	movw	%ax, -24(%rbp)
.L522:
	cmpw	$0, -26(%rbp)
	je	.L523
	cmpw	$8, -26(%rbp)
	je	.L523
	movl	$0, %eax
	call	killcard
	jmp	.L524
.L523:
	movl	$0, %eax
.L524:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L525
	movl	$0, %eax
	call	killcard
	jmp	.L526
.L525:
	cmpb	$31, %bl
	setbe	%al
	movzbl	%al, %eax
.L526:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L527
	nop
.L528:
	cmpw	$32, -24(%rbp)
	jne	.L543
	jmp	.L552
.L527:
	cmpw	$0, -22(%rbp)
	je	.L529
	cmpw	$1, -26(%rbp)
	jne	.L529
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L530
.L529:
	movl	$0, %eax
	call	killcard
.L530:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L531
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L532
.L531:
	movl	$0, %eax
	call	killcard
.L532:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$3, -26(%rbp)
	jne	.L533
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L534
.L533:
	movl	$0, %eax
	call	killcard
.L534:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$4, -26(%rbp)
	jne	.L535
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L536
.L535:
	movl	$0, %eax
	call	killcard
.L536:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$5, -26(%rbp)
	jne	.L537
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L538
.L537:
	movl	$0, %eax
	call	killcard
.L538:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	%bl, %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	%bl, %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$6, -26(%rbp)
	jne	.L539
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L540
.L539:
	movl	$0, %eax
	call	killcard
.L540:
	movw	%ax, -26(%rbp)
	addl	$4, %ebx
	cmpw	$7, -26(%rbp)
	jne	.L541
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L542
.L541:
	movl	$0, %eax
	call	killcard
.L542:
	movw	%ax, -26(%rbp)
	jmp	.L522
.L552:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L544
.L543:
	movl	$0, %eax
	call	killcard
.L544:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L545
	cmpw	$0, -22(%rbp)
	je	.L546
.L545:
	movl	$0, %eax
	call	killcard
.L546:
	cmpw	$33, -24(%rbp)
	jne	.L547
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L548
.L547:
	movl	$0, %eax
	call	killcard
.L548:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$34, -24(%rbp)
	jne	.L549
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L550
.L549:
	movl	$0, %eax
	call	killcard
.L550:
	movw	%ax, -24(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	aes_expandEncKey, .-aes_expandEncKey
	.globl	aes_expandDecKey
	.type	aes_expandDecKey, @function
aes_expandDecKey:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movw	$13, -20(%rbp)
	cmpw	$13, -20(%rbp)
	jne	.L554
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L555
.L554:
	movl	$0, %eax
	call	killcard
.L555:
	movw	%ax, -20(%rbp)
	cmpw	$14, -20(%rbp)
	jne	.L556
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L557
.L556:
	movl	$0, %eax
	call	killcard
.L557:
	movw	%ax, -20(%rbp)
	movw	$0, -22(%rbp)
	cmpw	$15, -20(%rbp)
	jne	.L558
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L559
.L558:
	movl	$0, %eax
	call	killcard
.L559:
	movw	%ax, -20(%rbp)
	movw	$1, -18(%rbp)
	cmpw	$16, -20(%rbp)
	jne	.L560
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L561
.L560:
	movl	$0, %eax
	call	killcard
.L561:
	movw	%ax, -20(%rbp)
	movb	$28, -23(%rbp)
	cmpw	$17, -20(%rbp)
	jne	.L562
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L563
.L562:
	movl	$0, %eax
	call	killcard
.L563:
	movw	%ax, -20(%rbp)
.L564:
	cmpw	$0, -22(%rbp)
	je	.L565
	cmpw	$8, -22(%rbp)
	je	.L565
	movl	$0, %eax
	call	killcard
	jmp	.L566
.L565:
	movl	$0, %eax
.L566:
	movw	%ax, -22(%rbp)
	movzwl	-22(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -22(%rbp)
	testw	%ax, %ax
	je	.L567
	movl	$0, %eax
	call	killcard
	jmp	.L568
.L567:
	cmpb	$16, -23(%rbp)
	seta	%al
	movzbl	%al, %eax
.L568:
	movw	%ax, -18(%rbp)
	cmpw	$0, -18(%rbp)
	jne	.L569
	nop
.L570:
	cmpw	$18, -20(%rbp)
	jne	.L585
	jmp	.L644
.L569:
	cmpw	$0, -18(%rbp)
	je	.L571
	cmpw	$1, -22(%rbp)
	jne	.L571
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L572
.L571:
	movl	$0, %eax
	call	killcard
.L572:
	movw	%ax, -22(%rbp)
	cmpw	$2, -22(%rbp)
	jne	.L573
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L574
.L573:
	movl	$0, %eax
	call	killcard
.L574:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$3, -22(%rbp)
	jne	.L575
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L576
.L575:
	movl	$0, %eax
	call	killcard
.L576:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$4, -22(%rbp)
	jne	.L577
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L578
.L577:
	movl	$0, %eax
	call	killcard
.L578:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$5, -22(%rbp)
	jne	.L579
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L580
.L579:
	movl	$0, %eax
	call	killcard
.L580:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$6, -22(%rbp)
	jne	.L581
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L582
.L581:
	movl	$0, %eax
	call	killcard
.L582:
	movw	%ax, -22(%rbp)
	subb	$4, -23(%rbp)
	cmpw	$7, -22(%rbp)
	jne	.L583
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L584
.L583:
	movl	$0, %eax
	call	killcard
.L584:
	movw	%ax, -22(%rbp)
	jmp	.L564
.L644:
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L586
.L585:
	movl	$0, %eax
	call	killcard
.L586:
	movw	%ax, -20(%rbp)
	cmpw	$1, -22(%rbp)
	jne	.L587
	cmpw	$0, -18(%rbp)
	je	.L588
.L587:
	movl	$0, %eax
	call	killcard
.L588:
	cmpw	$19, -20(%rbp)
	jne	.L589
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L590
.L589:
	movl	$0, %eax
	call	killcard
.L590:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$20, -20(%rbp)
	jne	.L591
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L592
.L591:
	movl	$0, %eax
	call	killcard
.L592:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	17(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$17, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$13, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$21, -20(%rbp)
	jne	.L593
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L594
.L593:
	movl	$0, %eax
	call	killcard
.L594:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	18(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$18, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$14, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$22, -20(%rbp)
	jne	.L595
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L596
.L595:
	movl	$0, %eax
	call	killcard
.L596:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	19(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$19, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$15, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$23, -20(%rbp)
	jne	.L597
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L598
.L597:
	movl	$0, %eax
	call	killcard
.L598:
	movw	%ax, -20(%rbp)
	movw	$0, -22(%rbp)
	cmpw	$24, -20(%rbp)
	jne	.L599
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L600
.L599:
	movl	$0, %eax
	call	killcard
.L600:
	movw	%ax, -20(%rbp)
	movw	$1, -18(%rbp)
	cmpw	$25, -20(%rbp)
	jne	.L601
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L602
.L601:
	movl	$0, %eax
	call	killcard
.L602:
	movw	%ax, -20(%rbp)
	movb	$12, -23(%rbp)
	cmpw	$26, -20(%rbp)
	jne	.L603
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L604
.L603:
	movl	$0, %eax
	call	killcard
.L604:
	movw	%ax, -20(%rbp)
.L605:
	cmpw	$0, -22(%rbp)
	je	.L606
	cmpw	$8, -22(%rbp)
	je	.L606
	movl	$0, %eax
	call	killcard
	jmp	.L607
.L606:
	movl	$0, %eax
.L607:
	movw	%ax, -22(%rbp)
	movzwl	-22(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -22(%rbp)
	testw	%ax, %ax
	je	.L608
	movl	$0, %eax
	call	killcard
	jmp	.L609
.L608:
	cmpb	$0, -23(%rbp)
	setne	%al
	movzbl	%al, %eax
.L609:
	movw	%ax, -18(%rbp)
	cmpw	$0, -18(%rbp)
	jne	.L610
	nop
.L611:
	cmpw	$27, -20(%rbp)
	jne	.L626
	jmp	.L645
.L610:
	cmpw	$0, -18(%rbp)
	je	.L612
	cmpw	$1, -22(%rbp)
	jne	.L612
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L613
.L612:
	movl	$0, %eax
	call	killcard
.L613:
	movw	%ax, -22(%rbp)
	cmpw	$2, -22(%rbp)
	jne	.L614
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L615
.L614:
	movl	$0, %eax
	call	killcard
.L615:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %edx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %ecx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-4(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$3, -22(%rbp)
	jne	.L616
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L617
.L616:
	movl	$0, %eax
	call	killcard
.L617:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %eax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %eax
	leaq	1(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-3(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$4, -22(%rbp)
	jne	.L618
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L619
.L618:
	movl	$0, %eax
	call	killcard
.L619:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %eax
	leaq	2(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %eax
	leaq	2(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-2(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$5, -22(%rbp)
	jne	.L620
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L621
.L620:
	movl	$0, %eax
	call	killcard
.L621:
	movw	%ax, -22(%rbp)
	movzbl	-23(%rbp), %eax
	leaq	3(%rax), %rdx
	movq	-40(%rbp), %rax
	addq	%rax, %rdx
	movzbl	-23(%rbp), %eax
	leaq	3(%rax), %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %ecx
	movzbl	-23(%rbp), %eax
	leaq	-1(%rax), %rsi
	movq	-40(%rbp), %rax
	addq	%rsi, %rax
	movzbl	(%rax), %eax
	xorl	%ecx, %eax
	movb	%al, (%rdx)
	cmpw	$6, -22(%rbp)
	jne	.L622
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L623
.L622:
	movl	$0, %eax
	call	killcard
.L623:
	movw	%ax, -22(%rbp)
	subb	$4, -23(%rbp)
	cmpw	$7, -22(%rbp)
	jne	.L624
	movzwl	-22(%rbp), %eax
	addl	$1, %eax
	jmp	.L625
.L624:
	movl	$0, %eax
	call	killcard
.L625:
	movw	%ax, -22(%rbp)
	jmp	.L605
.L645:
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L627
.L626:
	movl	$0, %eax
	call	killcard
.L627:
	movw	%ax, -20(%rbp)
	cmpw	$1, -22(%rbp)
	jne	.L628
	cmpw	$0, -18(%rbp)
	je	.L629
.L628:
	movl	$0, %eax
	call	killcard
.L629:
	cmpw	$28, -20(%rbp)
	jne	.L630
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L631
.L630:
	movl	$0, %eax
	call	killcard
.L631:
	movw	%ax, -20(%rbp)
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	shrb	%al
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L632
	movl	$-115, %eax
	jmp	.L633
.L632:
	movl	$0, %eax
.L633:
	xorl	%edx, %eax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	cmpw	$29, -20(%rbp)
	jne	.L634
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L635
.L634:
	movl	$0, %eax
	call	killcard
.L635:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	movzbl	(%rax), %ebx
	movq	-40(%rbp), %rax
	addq	$29, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	movq	-48(%rbp), %rdx
	movzbl	(%rdx), %edx
	xorl	%edx, %eax
	xorl	%eax, %ebx
	movl	%ebx, %edx
	movq	-40(%rbp), %rax
	movb	%dl, (%rax)
	cmpw	$30, -20(%rbp)
	jne	.L636
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L637
.L636:
	movl	$0, %eax
	call	killcard
.L637:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	1(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$30, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$31, -20(%rbp)
	jne	.L638
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L639
.L638:
	movl	$0, %eax
	call	killcard
.L639:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	2(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$2, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$31, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$32, -20(%rbp)
	jne	.L640
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L641
.L640:
	movl	$0, %eax
	call	killcard
.L641:
	movw	%ax, -20(%rbp)
	movq	-40(%rbp), %rax
	leaq	3(%rax), %rbx
	movq	-40(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %r12d
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	rj_sbox
	xorl	%r12d, %eax
	movb	%al, (%rbx)
	cmpw	$33, -20(%rbp)
	jne	.L642
	movzwl	-20(%rbp), %eax
	addl	$1, %eax
	jmp	.L643
.L642:
	movl	$0, %eax
	call	killcard
.L643:
	movw	%ax, -20(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	aes_expandDecKey, .-aes_expandDecKey
	.globl	aes256_init
	.type	aes256_init, @function
aes256_init:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L647
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L648
.L647:
	movl	$0, %eax
	call	killcard
.L648:
	movw	%ax, -24(%rbp)
	movb	$1, -27(%rbp)
	cmpw	$14, -24(%rbp)
	jne	.L649
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L650
.L649:
	movl	$0, %eax
	call	killcard
.L650:
	movw	%ax, -24(%rbp)
	cmpw	$15, -24(%rbp)
	jne	.L651
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L652
.L651:
	movl	$0, %eax
	call	killcard
.L652:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$16, -24(%rbp)
	jne	.L653
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L654
.L653:
	movl	$0, %eax
	call	killcard
.L654:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$17, -24(%rbp)
	jne	.L655
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L656
.L655:
	movl	$0, %eax
	call	killcard
.L656:
	movw	%ax, -24(%rbp)
	movl	$0, %ebx
	cmpw	$18, -24(%rbp)
	jne	.L657
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L658
.L657:
	movl	$0, %eax
	call	killcard
.L658:
	movw	%ax, -24(%rbp)
.L659:
	cmpw	$0, -26(%rbp)
	je	.L660
	cmpw	$5, -26(%rbp)
	je	.L660
	movl	$0, %eax
	call	killcard
	jmp	.L661
.L660:
	movl	$0, %eax
.L661:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L662
	movl	$0, %eax
	call	killcard
	jmp	.L663
.L662:
	cmpb	$31, %bl
	setbe	%al
	movzbl	%al, %eax
.L663:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L664
	nop
.L665:
	cmpw	$19, -24(%rbp)
	jne	.L674
	jmp	.L709
.L664:
	cmpw	$0, -22(%rbp)
	je	.L666
	cmpw	$1, -26(%rbp)
	jne	.L666
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L667
.L666:
	movl	$0, %eax
	call	killcard
.L667:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L668
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L669
.L668:
	movl	$0, %eax
	call	killcard
.L669:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edi
	movzbl	%bl, %eax
	movzbl	%bl, %ecx
	movq	-48(%rbp), %rdx
	addq	%rcx, %rdx
	movzbl	(%rdx), %esi
	movq	-40(%rbp), %rcx
	movslq	%eax, %rdx
	movb	%sil, 64(%rcx,%rdx)
	movq	-40(%rbp), %rdx
	cltq
	movzbl	64(%rdx,%rax), %ecx
	movq	-40(%rbp), %rdx
	movslq	%edi, %rax
	movb	%cl, 32(%rdx,%rax)
	cmpw	$3, -26(%rbp)
	jne	.L670
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L671
.L670:
	movl	$0, %eax
	call	killcard
.L671:
	movw	%ax, -26(%rbp)
	movl	%ebx, %eax
	leal	1(%rax), %ebx
	cmpw	$4, -26(%rbp)
	jne	.L672
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L673
.L672:
	movl	$0, %eax
	call	killcard
.L673:
	movw	%ax, -26(%rbp)
	jmp	.L659
.L709:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L675
.L674:
	movl	$0, %eax
	call	killcard
.L675:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L676
	cmpw	$0, -22(%rbp)
	je	.L677
.L676:
	movl	$0, %eax
	call	killcard
.L677:
	cmpw	$20, -24(%rbp)
	jne	.L678
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L679
.L678:
	movl	$0, %eax
	call	killcard
.L679:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$21, -24(%rbp)
	jne	.L680
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L681
.L680:
	movl	$0, %eax
	call	killcard
.L681:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$22, -24(%rbp)
	jne	.L682
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L683
.L682:
	movl	$0, %eax
	call	killcard
.L683:
	movw	%ax, -24(%rbp)
	movl	$8, %ebx
	cmpw	$23, -24(%rbp)
	jne	.L684
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L685
.L684:
	movl	$0, %eax
	call	killcard
.L685:
	movw	%ax, -24(%rbp)
.L686:
	cmpw	$0, -26(%rbp)
	je	.L687
	cmpw	$5, -26(%rbp)
	je	.L687
	movl	$0, %eax
	call	killcard
	jmp	.L688
.L687:
	movl	$0, %eax
.L688:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L689
	movl	$0, %eax
	call	killcard
	jmp	.L690
.L689:
	subl	$1, %ebx
	movzbl	%bl, %eax
.L690:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L691
	nop
.L692:
	cmpw	$24, -24(%rbp)
	jne	.L701
	jmp	.L710
.L691:
	cmpw	$0, -22(%rbp)
	je	.L693
	cmpw	$1, -26(%rbp)
	jne	.L693
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L694
.L693:
	movl	$0, %eax
	call	killcard
.L694:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L695
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L696
.L695:
	movl	$0, %eax
	call	killcard
.L696:
	movw	%ax, -26(%rbp)
	movq	-40(%rbp), %rax
	leaq	64(%rax), %rdx
	leaq	-27(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	aes_expandEncKey
	cmpw	$3, -26(%rbp)
	jne	.L697
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L698
.L697:
	movl	$0, %eax
	call	killcard
.L698:
	movw	%ax, -26(%rbp)
	cmpw	$4, -26(%rbp)
	jne	.L699
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L700
.L699:
	movl	$0, %eax
	call	killcard
.L700:
	movw	%ax, -26(%rbp)
	jmp	.L686
.L710:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L702
.L701:
	movl	$0, %eax
	call	killcard
.L702:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L703
	cmpw	$0, -22(%rbp)
	je	.L704
.L703:
	movl	$0, %eax
	call	killcard
.L704:
	cmpw	$25, -24(%rbp)
	jne	.L705
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L706
.L705:
	movl	$0, %eax
	call	killcard
.L706:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$26, -24(%rbp)
	jne	.L707
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L708
.L707:
	movl	$0, %eax
	call	killcard
.L708:
	movw	%ax, -24(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
	.size	aes256_init, .-aes256_init
	.globl	aes256_done
	.type	aes256_done, @function
aes256_done:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movw	$13, -24(%rbp)
	cmpw	$13, -24(%rbp)
	jne	.L712
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L713
.L712:
	movl	$0, %eax
	call	killcard
.L713:
	movw	%ax, -24(%rbp)
	cmpw	$14, -24(%rbp)
	jne	.L714
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L715
.L714:
	movl	$0, %eax
	call	killcard
.L715:
	movw	%ax, -24(%rbp)
	movw	$0, -26(%rbp)
	cmpw	$15, -24(%rbp)
	jne	.L716
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L717
.L716:
	movl	$0, %eax
	call	killcard
.L717:
	movw	%ax, -24(%rbp)
	movw	$1, -22(%rbp)
	cmpw	$16, -24(%rbp)
	jne	.L718
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L719
.L718:
	movl	$0, %eax
	call	killcard
.L719:
	movw	%ax, -24(%rbp)
	movl	$0, %ebx
	cmpw	$17, -24(%rbp)
	jne	.L720
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L721
.L720:
	movl	$0, %eax
	call	killcard
.L721:
	movw	%ax, -24(%rbp)
.L722:
	cmpw	$0, -26(%rbp)
	je	.L723
	cmpw	$5, -26(%rbp)
	je	.L723
	movl	$0, %eax
	call	killcard
	jmp	.L724
.L723:
	movl	$0, %eax
.L724:
	movw	%ax, -26(%rbp)
	movzwl	-26(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -26(%rbp)
	testw	%ax, %ax
	je	.L725
	movl	$0, %eax
	call	killcard
	jmp	.L726
.L725:
	cmpb	$31, %bl
	setbe	%al
	movzbl	%al, %eax
.L726:
	movw	%ax, -22(%rbp)
	cmpw	$0, -22(%rbp)
	jne	.L727
	nop
.L728:
	cmpw	$18, -24(%rbp)
	jne	.L737
	jmp	.L745
.L727:
	cmpw	$0, -22(%rbp)
	je	.L729
	cmpw	$1, -26(%rbp)
	jne	.L729
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L730
.L729:
	movl	$0, %eax
	call	killcard
.L730:
	movw	%ax, -26(%rbp)
	cmpw	$2, -26(%rbp)
	jne	.L731
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L732
.L731:
	movl	$0, %eax
	call	killcard
.L732:
	movw	%ax, -26(%rbp)
	movzbl	%bl, %edi
	movzbl	%bl, %eax
	movzbl	%bl, %edx
	movq	-40(%rbp), %rsi
	movslq	%edx, %rcx
	movb	$0, 64(%rsi,%rcx)
	movq	-40(%rbp), %rcx
	movslq	%edx, %rdx
	movzbl	64(%rcx,%rdx), %esi
	movq	-40(%rbp), %rcx
	movslq	%eax, %rdx
	movb	%sil, 32(%rcx,%rdx)
	movq	-40(%rbp), %rdx
	cltq
	movzbl	32(%rdx,%rax), %ecx
	movq	-40(%rbp), %rdx
	movslq	%edi, %rax
	movb	%cl, (%rdx,%rax)
	cmpw	$3, -26(%rbp)
	jne	.L733
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L734
.L733:
	movl	$0, %eax
	call	killcard
.L734:
	movw	%ax, -26(%rbp)
	movl	%ebx, %eax
	leal	1(%rax), %ebx
	cmpw	$4, -26(%rbp)
	jne	.L735
	movzwl	-26(%rbp), %eax
	addl	$1, %eax
	jmp	.L736
.L735:
	movl	$0, %eax
	call	killcard
.L736:
	movw	%ax, -26(%rbp)
	jmp	.L722
.L745:
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L738
.L737:
	movl	$0, %eax
	call	killcard
.L738:
	movw	%ax, -24(%rbp)
	cmpw	$1, -26(%rbp)
	jne	.L739
	cmpw	$0, -22(%rbp)
	je	.L740
.L739:
	movl	$0, %eax
	call	killcard
.L740:
	cmpw	$19, -24(%rbp)
	jne	.L741
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L742
.L741:
	movl	$0, %eax
	call	killcard
.L742:
	movw	%ax, -24(%rbp)
	movl	$1, -20(%rbp)
	cmpw	$20, -24(%rbp)
	jne	.L743
	movzwl	-24(%rbp), %eax
	addl	$1, %eax
	jmp	.L744
.L743:
	movl	$0, %eax
	call	killcard
.L744:
	movw	%ax, -24(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	aes256_done, .-aes256_done
	.globl	aes256_encrypt_ecb
	.type	aes256_encrypt_ecb, @function
aes256_encrypt_ecb:
.LFB21:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movw	$13, -10(%rbp)
	cmpw	$13, -10(%rbp)
	jne	.L747
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L748
.L747:
	movl	$0, %eax
	call	killcard
.L748:
	movw	%ax, -10(%rbp)
	cmpw	$14, -10(%rbp)
	jne	.L749
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L750
.L749:
	movl	$0, %eax
	call	killcard
.L750:
	movw	%ax, -10(%rbp)
	movq	-40(%rbp), %rdx
	movq	-40(%rbp), %rax
	leaq	32(%rax), %rcx
	movq	-48(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey_cpy
	cmpw	$15, -10(%rbp)
	jne	.L751
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L752
.L751:
	movl	$0, %eax
	call	killcard
.L752:
	movw	%ax, -10(%rbp)
	movw	$0, -16(%rbp)
	cmpw	$16, -10(%rbp)
	jne	.L753
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L754
.L753:
	movl	$0, %eax
	call	killcard
.L754:
	movw	%ax, -10(%rbp)
	movw	$1, -8(%rbp)
	cmpw	$17, -10(%rbp)
	jne	.L755
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L756
.L755:
	movl	$0, %eax
	call	killcard
.L756:
	movw	%ax, -10(%rbp)
	movb	$1, -17(%rbp)
	movb	$1, -18(%rbp)
	cmpw	$18, -10(%rbp)
	jne	.L757
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L758
.L757:
	movl	$0, %eax
	call	killcard
.L758:
	movw	%ax, -10(%rbp)
.L759:
	cmpw	$0, -16(%rbp)
	je	.L760
	cmpw	$13, -16(%rbp)
	je	.L760
	movl	$0, %eax
	call	killcard
	jmp	.L761
.L760:
	movl	$0, %eax
.L761:
	movw	%ax, -16(%rbp)
	movzwl	-16(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -16(%rbp)
	testw	%ax, %ax
	je	.L762
	movl	$0, %eax
	call	killcard
	jmp	.L763
.L762:
	cmpb	$13, -17(%rbp)
	setbe	%al
	movzbl	%al, %eax
.L763:
	movw	%ax, -8(%rbp)
	cmpw	$0, -8(%rbp)
	jne	.L764
	nop
.L765:
	cmpw	$19, -10(%rbp)
	jne	.L805
	jmp	.L819
.L764:
	cmpw	$0, -8(%rbp)
	je	.L766
	cmpw	$1, -16(%rbp)
	jne	.L766
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L767
.L766:
	movl	$0, %eax
	call	killcard
.L767:
	movw	%ax, -16(%rbp)
	cmpw	$2, -16(%rbp)
	jne	.L768
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L769
.L768:
	movl	$0, %eax
	call	killcard
.L769:
	movw	%ax, -16(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes
	cmpw	$3, -16(%rbp)
	jne	.L770
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L771
.L770:
	movl	$0, %eax
	call	killcard
.L771:
	movw	%ax, -16(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows
	cmpw	$4, -16(%rbp)
	jne	.L772
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L773
.L772:
	movl	$0, %eax
	call	killcard
.L773:
	movw	%ax, -16(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	aes_mixColumns
	cmpw	$5, -16(%rbp)
	jne	.L774
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L775
.L774:
	movl	$0, %eax
	call	killcard
.L775:
	movw	%ax, -16(%rbp)
	movw	$0, -14(%rbp)
	cmpw	$6, -16(%rbp)
	jne	.L776
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L777
.L776:
	movl	$0, %eax
	call	killcard
.L777:
	movw	%ax, -16(%rbp)
	movw	$0, -12(%rbp)
	cmpw	$7, -16(%rbp)
	jne	.L778
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L779
.L778:
	movl	$0, %eax
	call	killcard
.L779:
	movw	%ax, -16(%rbp)
	movw	$1, -6(%rbp)
	cmpw	$8, -16(%rbp)
	jne	.L780
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L781
.L780:
	movl	$0, %eax
	call	killcard
.L781:
	movw	%ax, -16(%rbp)
	movzbl	-17(%rbp), %eax
	andl	$1, %eax
	movw	%ax, -6(%rbp)
	cmpw	$0, -6(%rbp)
	je	.L782
	cmpw	$0, -14(%rbp)
	jne	.L783
	movzwl	-14(%rbp), %eax
	addl	$1, %eax
	jmp	.L784
.L783:
	movl	$0, %eax
	call	killcard
.L784:
	movw	%ax, -14(%rbp)
	movq	-40(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	cmpw	$1, -14(%rbp)
	jne	.L785
	movzwl	-14(%rbp), %eax
	addl	$1, %eax
	jmp	.L786
.L785:
	movl	$0, %eax
	call	killcard
.L786:
	movw	%ax, -14(%rbp)
	jmp	.L787
.L782:
	cmpw	$0, -12(%rbp)
	jne	.L788
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L789
.L788:
	movl	$0, %eax
	call	killcard
.L789:
	movw	%ax, -12(%rbp)
	movq	-40(%rbp), %rax
	leaq	-18(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandEncKey
	cmpw	$1, -12(%rbp)
	jne	.L790
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L791
.L790:
	movl	$0, %eax
	call	killcard
.L791:
	movw	%ax, -12(%rbp)
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	cmpw	$2, -12(%rbp)
	jne	.L792
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L793
.L792:
	movl	$0, %eax
	call	killcard
.L793:
	movw	%ax, -12(%rbp)
.L787:
	cmpw	$9, -16(%rbp)
	jne	.L794
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L795
.L794:
	movl	$0, %eax
	call	killcard
.L795:
	movw	%ax, -16(%rbp)
	cmpw	$2, -14(%rbp)
	jne	.L796
	cmpw	$0, -12(%rbp)
	jne	.L796
	cmpw	$0, -6(%rbp)
	jne	.L797
.L796:
	cmpw	$3, -12(%rbp)
	jne	.L798
	cmpw	$0, -14(%rbp)
	jne	.L798
	cmpw	$0, -6(%rbp)
	je	.L797
.L798:
	movl	$0, %eax
	call	killcard
.L797:
	cmpw	$10, -16(%rbp)
	jne	.L799
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L800
.L799:
	movl	$0, %eax
	call	killcard
.L800:
	movw	%ax, -16(%rbp)
	movl	$1, -4(%rbp)
	cmpw	$11, -16(%rbp)
	jne	.L801
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L802
.L801:
	movl	$0, %eax
	call	killcard
.L802:
	movw	%ax, -16(%rbp)
	addb	$1, -17(%rbp)
	cmpw	$12, -16(%rbp)
	jne	.L803
	movzwl	-16(%rbp), %eax
	addl	$1, %eax
	jmp	.L804
.L803:
	movl	$0, %eax
	call	killcard
.L804:
	movw	%ax, -16(%rbp)
	jmp	.L759
.L819:
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L806
.L805:
	movl	$0, %eax
	call	killcard
.L806:
	movw	%ax, -10(%rbp)
	cmpw	$1, -16(%rbp)
	jne	.L807
	cmpw	$0, -8(%rbp)
	je	.L808
.L807:
	movl	$0, %eax
	call	killcard
.L808:
	cmpw	$20, -10(%rbp)
	jne	.L809
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L810
.L809:
	movl	$0, %eax
	call	killcard
.L810:
	movw	%ax, -10(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes
	cmpw	$21, -10(%rbp)
	jne	.L811
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L812
.L811:
	movl	$0, %eax
	call	killcard
.L812:
	movw	%ax, -10(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows
	cmpw	$22, -10(%rbp)
	jne	.L813
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L814
.L813:
	movl	$0, %eax
	call	killcard
.L814:
	movw	%ax, -10(%rbp)
	movq	-40(%rbp), %rax
	leaq	-18(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandEncKey
	cmpw	$23, -10(%rbp)
	jne	.L815
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L816
.L815:
	movl	$0, %eax
	call	killcard
.L816:
	movw	%ax, -10(%rbp)
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	cmpw	$24, -10(%rbp)
	jne	.L817
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L818
.L817:
	movl	$0, %eax
	call	killcard
.L818:
	movw	%ax, -10(%rbp)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21:
	.size	aes256_encrypt_ecb, .-aes256_encrypt_ecb
	.globl	aes256_decrypt_ecb
	.type	aes256_decrypt_ecb, @function
aes256_decrypt_ecb:
.LFB22:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movw	$13, -6(%rbp)
	cmpw	$13, -6(%rbp)
	jne	.L821
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L822
.L821:
	movl	$0, %eax
	call	killcard
.L822:
	movw	%ax, -6(%rbp)
	cmpw	$14, -6(%rbp)
	jne	.L823
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L824
.L823:
	movl	$0, %eax
	call	killcard
.L824:
	movw	%ax, -6(%rbp)
	movq	-24(%rbp), %rdx
	movq	-24(%rbp), %rax
	leaq	64(%rax), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey_cpy
	cmpw	$15, -6(%rbp)
	jne	.L825
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L826
.L825:
	movl	$0, %eax
	call	killcard
.L826:
	movw	%ax, -6(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows_inv
	cmpw	$16, -6(%rbp)
	jne	.L827
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L828
.L827:
	movl	$0, %eax
	call	killcard
.L828:
	movw	%ax, -6(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes_inv
	cmpw	$17, -6(%rbp)
	jne	.L829
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L830
.L829:
	movl	$0, %eax
	call	killcard
.L830:
	movw	%ax, -6(%rbp)
	movw	$0, -12(%rbp)
	cmpw	$18, -6(%rbp)
	jne	.L831
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L832
.L831:
	movl	$0, %eax
	call	killcard
.L832:
	movw	%ax, -6(%rbp)
	movw	$1, -4(%rbp)
	cmpw	$19, -6(%rbp)
	jne	.L833
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L834
.L833:
	movl	$0, %eax
	call	killcard
.L834:
	movw	%ax, -6(%rbp)
	movb	$14, -13(%rbp)
	movb	$-128, -14(%rbp)
	cmpw	$20, -6(%rbp)
	jne	.L835
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L836
.L835:
	movl	$0, %eax
	call	killcard
.L836:
	movw	%ax, -6(%rbp)
.L837:
	cmpw	$0, -12(%rbp)
	je	.L838
	cmpw	$12, -12(%rbp)
	je	.L838
	movl	$0, %eax
	call	killcard
	jmp	.L839
.L838:
	movl	$0, %eax
.L839:
	movw	%ax, -12(%rbp)
	movzwl	-12(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -12(%rbp)
	testw	%ax, %ax
	je	.L840
	movl	$0, %eax
	call	killcard
	jmp	.L841
.L840:
	subb	$1, -13(%rbp)
	movzbl	-13(%rbp), %eax
.L841:
	movw	%ax, -4(%rbp)
	cmpw	$0, -4(%rbp)
	jne	.L842
	nop
.L843:
	cmpw	$21, -6(%rbp)
	jne	.L881
	jmp	.L889
.L842:
	cmpw	$0, -4(%rbp)
	je	.L844
	cmpw	$1, -12(%rbp)
	jne	.L844
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L845
.L844:
	movl	$0, %eax
	call	killcard
.L845:
	movw	%ax, -12(%rbp)
	cmpw	$2, -12(%rbp)
	jne	.L846
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L847
.L846:
	movl	$0, %eax
	call	killcard
.L847:
	movw	%ax, -12(%rbp)
	movw	$0, -10(%rbp)
	cmpw	$3, -12(%rbp)
	jne	.L848
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L849
.L848:
	movl	$0, %eax
	call	killcard
.L849:
	movw	%ax, -12(%rbp)
	movw	$0, -8(%rbp)
	cmpw	$4, -12(%rbp)
	jne	.L850
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L851
.L850:
	movl	$0, %eax
	call	killcard
.L851:
	movw	%ax, -12(%rbp)
	movw	$1, -2(%rbp)
	cmpw	$5, -12(%rbp)
	jne	.L852
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L853
.L852:
	movl	$0, %eax
	call	killcard
.L853:
	movw	%ax, -12(%rbp)
	movzbl	-13(%rbp), %eax
	andl	$1, %eax
	movw	%ax, -2(%rbp)
	cmpw	$0, -2(%rbp)
	je	.L854
	cmpw	$0, -10(%rbp)
	jne	.L855
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L856
.L855:
	movl	$0, %eax
	call	killcard
.L856:
	movw	%ax, -10(%rbp)
	movq	-24(%rbp), %rax
	leaq	-14(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_expandDecKey
	cmpw	$1, -10(%rbp)
	jne	.L857
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L858
.L857:
	movl	$0, %eax
	call	killcard
.L858:
	movw	%ax, -10(%rbp)
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	cmpw	$2, -10(%rbp)
	jne	.L859
	movzwl	-10(%rbp), %eax
	addl	$1, %eax
	jmp	.L860
.L859:
	movl	$0, %eax
	call	killcard
.L860:
	movw	%ax, -10(%rbp)
	jmp	.L861
.L854:
	cmpw	$0, -8(%rbp)
	jne	.L862
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L863
.L862:
	movl	$0, %eax
	call	killcard
.L863:
	movw	%ax, -8(%rbp)
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	cmpw	$1, -8(%rbp)
	jne	.L864
	movzwl	-8(%rbp), %eax
	addl	$1, %eax
	jmp	.L865
.L864:
	movl	$0, %eax
	call	killcard
.L865:
	movw	%ax, -8(%rbp)
.L861:
	cmpw	$6, -12(%rbp)
	jne	.L866
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L867
.L866:
	movl	$0, %eax
	call	killcard
.L867:
	movw	%ax, -12(%rbp)
	cmpw	$3, -10(%rbp)
	jne	.L868
	cmpw	$0, -8(%rbp)
	jne	.L868
	cmpw	$0, -2(%rbp)
	jne	.L869
.L868:
	cmpw	$2, -8(%rbp)
	jne	.L870
	cmpw	$0, -10(%rbp)
	jne	.L870
	cmpw	$0, -2(%rbp)
	je	.L869
.L870:
	movl	$0, %eax
	call	killcard
.L869:
	cmpw	$7, -12(%rbp)
	jne	.L871
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L872
.L871:
	movl	$0, %eax
	call	killcard
.L872:
	movw	%ax, -12(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_mixColumns_inv
	cmpw	$8, -12(%rbp)
	jne	.L873
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L874
.L873:
	movl	$0, %eax
	call	killcard
.L874:
	movw	%ax, -12(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_shiftRows_inv
	cmpw	$9, -12(%rbp)
	jne	.L875
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L876
.L875:
	movl	$0, %eax
	call	killcard
.L876:
	movw	%ax, -12(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	aes_subBytes_inv
	cmpw	$10, -12(%rbp)
	jne	.L877
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L878
.L877:
	movl	$0, %eax
	call	killcard
.L878:
	movw	%ax, -12(%rbp)
	cmpw	$11, -12(%rbp)
	jne	.L879
	movzwl	-12(%rbp), %eax
	addl	$1, %eax
	jmp	.L880
.L879:
	movl	$0, %eax
	call	killcard
.L880:
	movw	%ax, -12(%rbp)
	jmp	.L837
.L889:
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L882
.L881:
	movl	$0, %eax
	call	killcard
.L882:
	movw	%ax, -6(%rbp)
	cmpw	$1, -12(%rbp)
	jne	.L883
	cmpw	$0, -4(%rbp)
	je	.L884
.L883:
	movl	$0, %eax
	call	killcard
.L884:
	cmpw	$22, -6(%rbp)
	jne	.L885
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L886
.L885:
	movl	$0, %eax
	call	killcard
.L886:
	movw	%ax, -6(%rbp)
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes_addRoundKey
	cmpw	$23, -6(%rbp)
	jne	.L887
	movzwl	-6(%rbp), %eax
	addl	$1, %eax
	jmp	.L888
.L887:
	movl	$0, %eax
	call	killcard
.L888:
	movw	%ax, -6(%rbp)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	aes256_decrypt_ecb, .-aes256_decrypt_ecb
	.section	.rodata
.LC2:
	.string	"txt: "
.LC3:
	.string	"key: "
.LC4:
	.string	"---"
.LC5:
	.string	"enc: "
	.align 8
.LC6:
	.string	"tst: 8e a2 b7 ca 51 67 45 bf ea fc 49 90 4b 49 60 89"
.LC7:
	.string	"dec: "
	.text
	.globl	main
	.type	main, @function
main:
.LFB23:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -24
	movl	%edi, -196(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movw	$13, -184(%rbp)
	cmpw	$13, -184(%rbp)
	jne	.L891
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L892
.L891:
	movl	$0, %eax
	call	killcard
.L892:
	movw	%ax, -184(%rbp)
	cmpw	$14, -184(%rbp)
	jne	.L893
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L894
.L893:
	movl	$0, %eax
	call	killcard
.L894:
	movw	%ax, -184(%rbp)
	cmpw	$15, -184(%rbp)
	jne	.L895
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L896
.L895:
	movl	$0, %eax
	call	killcard
.L896:
	movw	%ax, -184(%rbp)
	cmpw	$16, -184(%rbp)
	jne	.L897
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L898
.L897:
	movl	$0, %eax
	call	killcard
.L898:
	movw	%ax, -184(%rbp)
	movw	$0, -186(%rbp)
	cmpw	$17, -184(%rbp)
	jne	.L899
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L900
.L899:
	movl	$0, %eax
	call	killcard
.L900:
	movw	%ax, -184(%rbp)
	movw	$1, -182(%rbp)
	cmpw	$18, -184(%rbp)
	jne	.L901
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L902
.L901:
	movl	$0, %eax
	call	killcard
.L902:
	movw	%ax, -184(%rbp)
	movb	$0, -187(%rbp)
	cmpw	$19, -184(%rbp)
	jne	.L903
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L904
.L903:
	movl	$0, %eax
	call	killcard
.L904:
	movw	%ax, -184(%rbp)
.L905:
	cmpw	$0, -186(%rbp)
	je	.L906
	cmpw	$5, -186(%rbp)
	je	.L906
	movl	$0, %eax
	call	killcard
	jmp	.L907
.L906:
	movl	$0, %eax
.L907:
	movw	%ax, -186(%rbp)
	movzwl	-186(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -186(%rbp)
	testw	%ax, %ax
	je	.L908
	movl	$0, %eax
	call	killcard
	jmp	.L909
.L908:
	cmpb	$15, -187(%rbp)
	setbe	%al
	movzbl	%al, %eax
.L909:
	movw	%ax, -182(%rbp)
	cmpw	$0, -182(%rbp)
	jne	.L910
	nop
.L911:
	cmpw	$20, -184(%rbp)
	jne	.L920
	jmp	.L979
.L910:
	cmpw	$0, -182(%rbp)
	je	.L912
	cmpw	$1, -186(%rbp)
	jne	.L912
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L913
.L912:
	movl	$0, %eax
	call	killcard
.L913:
	movw	%ax, -186(%rbp)
	cmpw	$2, -186(%rbp)
	jne	.L914
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L915
.L914:
	movl	$0, %eax
	call	killcard
.L915:
	movw	%ax, -186(%rbp)
	movzbl	-187(%rbp), %ecx
	movzbl	-187(%rbp), %edx
	movl	%edx, %eax
	sall	$4, %eax
	addl	%eax, %edx
	movslq	%ecx, %rax
	movb	%dl, -176(%rbp,%rax)
	cmpw	$3, -186(%rbp)
	jne	.L916
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L917
.L916:
	movl	$0, %eax
	call	killcard
.L917:
	movw	%ax, -186(%rbp)
	movzbl	-187(%rbp), %eax
	addl	$1, %eax
	movb	%al, -187(%rbp)
	cmpw	$4, -186(%rbp)
	jne	.L918
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L919
.L918:
	movl	$0, %eax
	call	killcard
.L919:
	movw	%ax, -186(%rbp)
	jmp	.L905
.L979:
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L921
.L920:
	movl	$0, %eax
	call	killcard
.L921:
	movw	%ax, -184(%rbp)
	cmpw	$1, -186(%rbp)
	jne	.L922
	cmpw	$0, -182(%rbp)
	je	.L923
.L922:
	movl	$0, %eax
	call	killcard
.L923:
	cmpw	$21, -184(%rbp)
	jne	.L924
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L925
.L924:
	movl	$0, %eax
	call	killcard
.L925:
	movw	%ax, -184(%rbp)
	movw	$0, -186(%rbp)
	cmpw	$22, -184(%rbp)
	jne	.L926
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L927
.L926:
	movl	$0, %eax
	call	killcard
.L927:
	movw	%ax, -184(%rbp)
	movw	$1, -182(%rbp)
	cmpw	$23, -184(%rbp)
	jne	.L928
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L929
.L928:
	movl	$0, %eax
	call	killcard
.L929:
	movw	%ax, -184(%rbp)
	movb	$0, -187(%rbp)
	cmpw	$24, -184(%rbp)
	jne	.L930
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L931
.L930:
	movl	$0, %eax
	call	killcard
.L931:
	movw	%ax, -184(%rbp)
.L932:
	cmpw	$0, -186(%rbp)
	je	.L933
	cmpw	$5, -186(%rbp)
	je	.L933
	movl	$0, %eax
	call	killcard
	jmp	.L934
.L933:
	movl	$0, %eax
.L934:
	movw	%ax, -186(%rbp)
	movzwl	-186(%rbp), %eax
	leal	1(%rax), %edx
	movw	%dx, -186(%rbp)
	testw	%ax, %ax
	je	.L935
	movl	$0, %eax
	call	killcard
	jmp	.L936
.L935:
	cmpb	$31, -187(%rbp)
	setbe	%al
	movzbl	%al, %eax
.L936:
	movw	%ax, -182(%rbp)
	cmpw	$0, -182(%rbp)
	jne	.L937
	nop
.L938:
	cmpw	$25, -184(%rbp)
	jne	.L947
	jmp	.L980
.L937:
	cmpw	$0, -182(%rbp)
	je	.L939
	cmpw	$1, -186(%rbp)
	jne	.L939
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L940
.L939:
	movl	$0, %eax
	call	killcard
.L940:
	movw	%ax, -186(%rbp)
	cmpw	$2, -186(%rbp)
	jne	.L941
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L942
.L941:
	movl	$0, %eax
	call	killcard
.L942:
	movw	%ax, -186(%rbp)
	movzbl	-187(%rbp), %eax
	cltq
	movzbl	-187(%rbp), %edx
	movb	%dl, -160(%rbp,%rax)
	cmpw	$3, -186(%rbp)
	jne	.L943
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L944
.L943:
	movl	$0, %eax
	call	killcard
.L944:
	movw	%ax, -186(%rbp)
	movzbl	-187(%rbp), %eax
	addl	$1, %eax
	movb	%al, -187(%rbp)
	cmpw	$4, -186(%rbp)
	jne	.L945
	movzwl	-186(%rbp), %eax
	addl	$1, %eax
	jmp	.L946
.L945:
	movl	$0, %eax
	call	killcard
.L946:
	movw	%ax, -186(%rbp)
	jmp	.L932
.L980:
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L948
.L947:
	movl	$0, %eax
	call	killcard
.L948:
	movw	%ax, -184(%rbp)
	cmpw	$1, -186(%rbp)
	jne	.L949
	cmpw	$0, -182(%rbp)
	je	.L950
.L949:
	movl	$0, %eax
	call	killcard
.L950:
	cmpw	$26, -184(%rbp)
	jne	.L951
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L952
.L951:
	movl	$0, %eax
	call	killcard
.L952:
	movw	%ax, -184(%rbp)
	movl	$1, -180(%rbp)
	cmpw	$27, -184(%rbp)
	jne	.L953
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L954
.L953:
	movl	$0, %eax
	call	killcard
.L954:
	movw	%ax, -184(%rbp)
	movzbl	-187(%rbp), %eax
	leaq	-176(%rbp), %rdx
	movl	$16, %ecx
	movl	%eax, %esi
	movl	$.LC2, %edi
	call	DUMP
	cmpw	$28, -184(%rbp)
	jne	.L955
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L956
.L955:
	movl	$0, %eax
	call	killcard
.L956:
	movw	%ax, -184(%rbp)
	movzbl	-187(%rbp), %eax
	leaq	-160(%rbp), %rdx
	movl	$32, %ecx
	movl	%eax, %esi
	movl	$.LC3, %edi
	call	DUMP
	cmpw	$29, -184(%rbp)
	jne	.L957
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L958
.L957:
	movl	$0, %eax
	call	killcard
.L958:
	movw	%ax, -184(%rbp)
	movl	$.LC4, %edi
	call	puts
	cmpw	$30, -184(%rbp)
	jne	.L959
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L960
.L959:
	movl	$0, %eax
	call	killcard
.L960:
	movw	%ax, -184(%rbp)
	leaq	-160(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_init
	cmpw	$31, -184(%rbp)
	jne	.L961
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L962
.L961:
	movl	$0, %eax
	call	killcard
.L962:
	movw	%ax, -184(%rbp)
	leaq	-176(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_encrypt_ecb
	cmpw	$32, -184(%rbp)
	jne	.L963
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L964
.L963:
	movl	$0, %eax
	call	killcard
.L964:
	movw	%ax, -184(%rbp)
	movzbl	-187(%rbp), %eax
	leaq	-176(%rbp), %rdx
	movl	$16, %ecx
	movl	%eax, %esi
	movl	$.LC5, %edi
	call	DUMP
	cmpw	$33, -184(%rbp)
	jne	.L965
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L966
.L965:
	movl	$0, %eax
	call	killcard
.L966:
	movw	%ax, -184(%rbp)
	movl	$.LC6, %edi
	call	puts
	cmpw	$34, -184(%rbp)
	jne	.L967
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L968
.L967:
	movl	$0, %eax
	call	killcard
.L968:
	movw	%ax, -184(%rbp)
	leaq	-160(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_init
	cmpw	$35, -184(%rbp)
	jne	.L969
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L970
.L969:
	movl	$0, %eax
	call	killcard
.L970:
	movw	%ax, -184(%rbp)
	leaq	-176(%rbp), %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	aes256_decrypt_ecb
	cmpw	$36, -184(%rbp)
	jne	.L971
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L972
.L971:
	movl	$0, %eax
	call	killcard
.L972:
	movw	%ax, -184(%rbp)
	movzbl	-187(%rbp), %eax
	leaq	-176(%rbp), %rdx
	movl	$16, %ecx
	movl	%eax, %esi
	movl	$.LC7, %edi
	call	DUMP
	cmpw	$37, -184(%rbp)
	jne	.L973
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L974
.L973:
	movl	$0, %eax
	call	killcard
.L974:
	movw	%ax, -184(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	aes256_done
	cmpw	$38, -184(%rbp)
	jne	.L975
	movzwl	-184(%rbp), %eax
	addl	$1, %eax
	jmp	.L976
.L975:
	movl	$0, %eax
	call	killcard
.L976:
	movw	%ax, -184(%rbp)
	movl	$0, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	je	.L978
	call	__stack_chk_fail
.L978:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.8.1-10ubuntu8) 4.8.1"
	.section	.note.GNU-stack,"",@progbits
