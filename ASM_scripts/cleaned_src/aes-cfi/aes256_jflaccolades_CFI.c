#include <stdio.h>
// SEQUENTIAL STATEMENTS
#define DECL_INIT(cnt,val) unsigned short cnt; if ((cnt = val) != val)killcard();
#define INIT(cnt,val)  if ((cnt = val) != val)killcard();
#define CHECK_INCR(cnt,val) cnt = (cnt == val ? cnt + 1 : killcard());
// IF
#define CHECK_END_IF_ELSE(cnt_then, cnt_else, b, x, y) if ( ! ( (cnt_then == x && cnt_else == 0 && b) || (cnt_else == y && cnt_then == 0 && !b) ) )killcard();
#define CHECK_END_IF(cnt_then, b, x) if (! ( (cnt_then == x && b) || (cnt_then == 0 && !b) ) )killcard();
// FOR/WHILE STATEMENTS
#define CHECK_INCR_COND(b, cnt, val, cond)  (b = ((cnt++ != val) ? killcard() : cond))
#define CHECK_END_LOOP(cnt_loop, b, val) if ( ! (cnt_loop == val && !b) )killcard();
#define CHECK_LOOP_INCR(cnt, val, b) cnt = (b && cnt == val ? cnt + 1 : killcard());
#include "aes256_jflaccolades_CFI.h"
/*  
*   Byte-oriented AES-256 implementation.
*   All lookup tables replaced with 'on the fly' calculations. 
*
*   Copyright (c) 2007-2009 Ilya O. Levin, http://www.literatecode.com
*   Other contributors: Hal Finney
*
*   Permission to use, copy, modify, and distribute this software for any
*   purpose with or without fee is hereby granted, provided that the above
*   copyright notice and this permission notice appear in all copies.
*
*   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
*   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
*   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
*   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
*   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
*   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
*   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include "aes256.h"


//#define DUMP(s, i, buf, sz)  {
void DUMP(char * s, int i, uint8_t * buf, int sz)  {
																		DECL_INIT(CNT_0___DUMP,13)
																		CHECK_INCR(CNT_0___DUMP,13)
	printf("%s", s);
																		CHECK_INCR(CNT_0___DUMP,14)
																		DECL_INIT(CNT_1___DUMPfor,0)
																		CHECK_INCR(CNT_0___DUMP,15)
																		DECL_INIT(CNT_1___DUMP_b,1)
																		CHECK_INCR(CNT_0___DUMP,16)
i = 0;
																		CHECK_INCR(CNT_0___DUMP,17)
beginCNT_1___DUMPfor0:
																		CNT_1___DUMPfor = !(CNT_1___DUMPfor == 0 || CNT_1___DUMPfor == CNT_1___DUMPNBEND0) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___DUMP_b,CNT_1___DUMPfor, 0,  i < (sz))) goto endCNT_1___DUMPfor0;
																		CHECK_LOOP_INCR(CNT_1___DUMPfor,1, CNT_1___DUMP_b)
    {
																		CHECK_INCR(CNT_1___DUMPfor,2)
          printf("%02x ", buf[i]);
																		CHECK_INCR(CNT_1___DUMPfor,3)
    }
i++;
																		CHECK_INCR(CNT_1___DUMPfor,4)
goto beginCNT_1___DUMPfor0;
endCNT_1___DUMPfor0: ;
																		CHECK_INCR(CNT_0___DUMP,18)
																		CHECK_END_LOOP(CNT_1___DUMPfor,CNT_1___DUMP_b, 1)
																		CHECK_INCR(CNT_0___DUMP,19)
    printf("\n");
																		CHECK_INCR(CNT_0___DUMP,20)
}

#define F(x)   (((x)<<1) ^ ((((x)>>7) & 1) * 0x1b))
#define FD(x)  (((x) >> 1) ^ (((x) & 1) ? 0x8d : 0))

// #define BACK_TO_TABLES
#ifdef BACK_TO_TABLES

const uint8_t sbox[256] = {
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5,
    0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
    0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc,
    0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a,
    0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0,
    0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b,
    0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85,
    0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
    0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17,
    0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88,
    0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c,
    0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9,
    0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6,
    0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e,
    0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94,
    0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68,
    0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
};
const uint8_t sboxinv[256] = {
    0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38,
    0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87,
    0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d,
    0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
    0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2,
    0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16,
    0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda,
    0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
    0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a,
    0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02,
    0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea,
    0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85,
    0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89,
    0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20,
    0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31,
    0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d,
    0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0,
    0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26,
    0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d
};

#define rj_sbox(x)     sbox[(x)]
#define rj_sbox_inv(x) sboxinv[(x)]

#else /* tableless subroutines */

/* -------------------------------------------------------------------------- */
uint8_t gf_alog(uint8_t x) // calculate anti-logarithm gen 3
{
																		DECL_INIT(CNT_0___gf_alog,13)
																		CHECK_INCR(CNT_0___gf_alog,13)
    uint8_t atb = 1;
																		CHECK_INCR(CNT_0___gf_alog,14)
    uint8_t z;

																		CHECK_INCR(CNT_0___gf_alog,15)
																		DECL_INIT(CNT_1___gf_alogwhile,0)
																		CHECK_INCR(CNT_0___gf_alog,16)
																		DECL_INIT(CNT_1___gf_alog_b,1)
																		CHECK_INCR(CNT_0___gf_alog,17)
beginCNT_1___gf_alogwhile1:
																		CNT_1___gf_alogwhile = !(CNT_1___gf_alogwhile == 0 || CNT_1___gf_alogwhile == CNT_1___gf_alogNBEND1) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___gf_alog_b,CNT_1___gf_alogwhile, 0, x--)) goto endCNT_1___gf_alogwhile1;
																		CHECK_LOOP_INCR(CNT_1___gf_alogwhile,1, CNT_1___gf_alog_b)
    {
																		CHECK_INCR(CNT_1___gf_alogwhile,2)
      z = atb; 
																		CHECK_INCR(CNT_1___gf_alogwhile,3)
      atb <<= 1; 
																		CHECK_INCR(CNT_1___gf_alogwhile,4)
																		DECL_INIT(CNT_2___gf_alogthen,0)
																		CHECK_INCR(CNT_1___gf_alogwhile,5)
																		DECL_INIT(CNT_2___gf_alogelse,0)
																		CHECK_INCR(CNT_1___gf_alogwhile,6)
																		DECL_INIT(CNT_2___gf_alog_b,1)
																		CHECK_INCR(CNT_1___gf_alogwhile,7)
if (CNT_2___gf_alog_b = (z & 0x80))
      {
																		CHECK_INCR(CNT_2___gf_alogthen,0)
    	  atb^= 0x1b;
																		CHECK_INCR(CNT_2___gf_alogthen,1)
      }
																		CHECK_INCR(CNT_1___gf_alogwhile,8)
																		CHECK_END_IF(CNT_2___gf_alogthen,CNT_2___gf_alog_b,2)
																		CHECK_INCR(CNT_1___gf_alogwhile,9)
      atb ^= z;
																		CHECK_INCR(CNT_1___gf_alogwhile,10)
    }

goto beginCNT_1___gf_alogwhile1;
endCNT_1___gf_alogwhile1: ;
																		CHECK_INCR(CNT_0___gf_alog,18)
																		CHECK_END_LOOP(CNT_1___gf_alogwhile,CNT_1___gf_alog_b, 1)
																		CHECK_INCR(CNT_0___gf_alog,19)
    return atb;
} /* gf_alog */

/* -------------------------------------------------------------------------- */
uint8_t gf_log(uint8_t x) // calculate logarithm gen 3
{
    uint8_t atb = 1, i = 0, z;

    do {
      if (atb == x) break;
      z = atb; 
      atb <<= 1; 
      if (z & 0x80) 
	atb^= 0x1b; 
      atb ^= z;
    } while (++i > 0);
    
    return i;
} /* gf_log */


/* -------------------------------------------------------------------------- */
uint8_t gf_mulinv(uint8_t x) // calculate multiplicative inverse
{
																		DECL_INIT(CNT_0___gf_mulinv,13)
																		CHECK_INCR(CNT_0___gf_mulinv,13)
    return (x) ? gf_alog(255 - gf_log(x)) : 0;
} /* gf_mulinv */

/* -------------------------------------------------------------------------- */
uint8_t rj_sbox(uint8_t x)
{
																		DECL_INIT(CNT_0___rj_sbox,13)
																		CHECK_INCR(CNT_0___rj_sbox,13)
    uint8_t y, sb;

																		CHECK_INCR(CNT_0___rj_sbox,14)
    sb = y = gf_mulinv(x);
																		CHECK_INCR(CNT_0___rj_sbox,15)
    y = (y<<1)|(y>>7); 
																		CHECK_INCR(CNT_0___rj_sbox,16)
    sb ^= y;  
																		CHECK_INCR(CNT_0___rj_sbox,17)
    y = (y<<1)|(y>>7); 
																		CHECK_INCR(CNT_0___rj_sbox,18)
    sb ^= y; 
																		CHECK_INCR(CNT_0___rj_sbox,19)
    y = (y<<1)|(y>>7); 
																		CHECK_INCR(CNT_0___rj_sbox,20)
    sb ^= y; 
																		CHECK_INCR(CNT_0___rj_sbox,21)
    y = (y<<1)|(y>>7); 
																		CHECK_INCR(CNT_0___rj_sbox,22)
    sb ^= y;

																		CHECK_INCR(CNT_0___rj_sbox,23)
    return (sb ^ 0x63);
} /* rj_sbox */

/* -------------------------------------------------------------------------- */
uint8_t rj_sbox_inv(uint8_t x)
{
																		DECL_INIT(CNT_0___rj_sbox_inv,13)
																		CHECK_INCR(CNT_0___rj_sbox_inv,13)
    uint8_t y, sb;

																		CHECK_INCR(CNT_0___rj_sbox_inv,14)
    y = x ^ 0x63;
																		CHECK_INCR(CNT_0___rj_sbox_inv,15)
    sb = y = (y<<1)|(y>>7);
																		CHECK_INCR(CNT_0___rj_sbox_inv,16)
    y = (y<<2)|(y>>6); 
																		CHECK_INCR(CNT_0___rj_sbox_inv,17)
    sb ^= y; 
																		CHECK_INCR(CNT_0___rj_sbox_inv,18)
    y = (y<<3)|(y>>5); 
																		CHECK_INCR(CNT_0___rj_sbox_inv,19)
    sb ^= y;

																		CHECK_INCR(CNT_0___rj_sbox_inv,20)
    return gf_mulinv(sb);
} /* rj_sbox_inv */

#endif

/* -------------------------------------------------------------------------- */
uint8_t rj_xtime(uint8_t x) 
{
																		DECL_INIT(CNT_0___rj_xtime,13)
																		CHECK_INCR(CNT_0___rj_xtime,13)
    return (x & 0x80) ? ((x << 1) ^ 0x1b) : (x << 1);
} /* rj_xtime */

/* -------------------------------------------------------------------------- */
void aes_subBytes(uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes_subBytes,13)
																		CHECK_INCR(CNT_0___aes_subBytes,13)
    register uint8_t i = 16;

																		CHECK_INCR(CNT_0___aes_subBytes,14)
																		DECL_INIT(CNT_1___aes_subByteswhile,0)
																		CHECK_INCR(CNT_0___aes_subBytes,15)
																		DECL_INIT(CNT_1___aes_subBytes_b,1)
																		CHECK_INCR(CNT_0___aes_subBytes,16)
beginCNT_1___aes_subByteswhile2:
																		CNT_1___aes_subByteswhile = !(CNT_1___aes_subByteswhile == 0 || CNT_1___aes_subByteswhile == CNT_1___aes_subBytesNBEND2) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_subBytes_b,CNT_1___aes_subByteswhile, 0, i--)) goto endCNT_1___aes_subByteswhile2;
																		CHECK_LOOP_INCR(CNT_1___aes_subByteswhile,1, CNT_1___aes_subBytes_b)
    {
																		CHECK_INCR(CNT_1___aes_subByteswhile,2)
      buf[i] = rj_sbox(buf[i]);
																		CHECK_INCR(CNT_1___aes_subByteswhile,3)
    }
goto beginCNT_1___aes_subByteswhile2;
endCNT_1___aes_subByteswhile2: ;
																		CHECK_INCR(CNT_0___aes_subBytes,17)
																		CHECK_END_LOOP(CNT_1___aes_subByteswhile,CNT_1___aes_subBytes_b, 1)
																		CHECK_INCR(CNT_0___aes_subBytes,18)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_subBytes,19)
} /* aes_subBytes */

/* -------------------------------------------------------------------------- */
void aes_subBytes_inv(uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes_subBytes_inv,13)
																		CHECK_INCR(CNT_0___aes_subBytes_inv,13)
    register uint8_t i = 16;

																		CHECK_INCR(CNT_0___aes_subBytes_inv,14)
																		DECL_INIT(CNT_1___aes_subBytes_invwhile,0)
																		CHECK_INCR(CNT_0___aes_subBytes_inv,15)
																		DECL_INIT(CNT_1___aes_subBytes_inv_b,1)
																		CHECK_INCR(CNT_0___aes_subBytes_inv,16)
beginCNT_1___aes_subBytes_invwhile3:
																		CNT_1___aes_subBytes_invwhile = !(CNT_1___aes_subBytes_invwhile == 0 || CNT_1___aes_subBytes_invwhile == CNT_1___aes_subBytes_invNBEND3) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_subBytes_inv_b,CNT_1___aes_subBytes_invwhile, 0, i--)) goto endCNT_1___aes_subBytes_invwhile3;
																		CHECK_LOOP_INCR(CNT_1___aes_subBytes_invwhile,1, CNT_1___aes_subBytes_inv_b)
    {
																		CHECK_INCR(CNT_1___aes_subBytes_invwhile,2)
      buf[i] = rj_sbox_inv(buf[i]);
																		CHECK_INCR(CNT_1___aes_subBytes_invwhile,3)
    }
goto beginCNT_1___aes_subBytes_invwhile3;
endCNT_1___aes_subBytes_invwhile3: ;
																		CHECK_INCR(CNT_0___aes_subBytes_inv,17)
																		CHECK_END_LOOP(CNT_1___aes_subBytes_invwhile,CNT_1___aes_subBytes_inv_b, 1)
																		CHECK_INCR(CNT_0___aes_subBytes_inv,18)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_subBytes_inv,19)
} /* aes_subBytes_inv */

/* -------------------------------------------------------------------------- */
void aes_addRoundKey(uint8_t *buf, uint8_t *key)
{
																		DECL_INIT(CNT_0___aes_addRoundKey,13)
																		CHECK_INCR(CNT_0___aes_addRoundKey,13)
    register uint8_t i = 16;

																		CHECK_INCR(CNT_0___aes_addRoundKey,14)
																		DECL_INIT(CNT_1___aes_addRoundKeywhile,0)
																		CHECK_INCR(CNT_0___aes_addRoundKey,15)
																		DECL_INIT(CNT_1___aes_addRoundKey_b,1)
																		CHECK_INCR(CNT_0___aes_addRoundKey,16)
beginCNT_1___aes_addRoundKeywhile4:
																		CNT_1___aes_addRoundKeywhile = !(CNT_1___aes_addRoundKeywhile == 0 || CNT_1___aes_addRoundKeywhile == CNT_1___aes_addRoundKeyNBEND4) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_addRoundKey_b,CNT_1___aes_addRoundKeywhile, 0, i--)) goto endCNT_1___aes_addRoundKeywhile4;
																		CHECK_LOOP_INCR(CNT_1___aes_addRoundKeywhile,1, CNT_1___aes_addRoundKey_b)
    {
																		CHECK_INCR(CNT_1___aes_addRoundKeywhile,2)
      buf[i] ^= key[i];
																		CHECK_INCR(CNT_1___aes_addRoundKeywhile,3)
    }
goto beginCNT_1___aes_addRoundKeywhile4;
endCNT_1___aes_addRoundKeywhile4: ;
																		CHECK_INCR(CNT_0___aes_addRoundKey,17)
																		CHECK_END_LOOP(CNT_1___aes_addRoundKeywhile,CNT_1___aes_addRoundKey_b, 1)
																		CHECK_INCR(CNT_0___aes_addRoundKey,18)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_addRoundKey,19)
} /* aes_addRoundKey */

/* -------------------------------------------------------------------------- */
void aes_addRoundKey_cpy(uint8_t *buf, uint8_t *key, uint8_t *cpk)
{
																		DECL_INIT(CNT_0___aes_addRoundKey_cpy,13)
																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,13)
    register uint8_t i = 16;

																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,14)
																		DECL_INIT(CNT_1___aes_addRoundKey_cpywhile,0)
																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,15)
																		DECL_INIT(CNT_1___aes_addRoundKey_cpy_b,1)
																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,16)
beginCNT_1___aes_addRoundKey_cpywhile5:
																		CNT_1___aes_addRoundKey_cpywhile = !(CNT_1___aes_addRoundKey_cpywhile == 0 || CNT_1___aes_addRoundKey_cpywhile == CNT_1___aes_addRoundKey_cpyNBEND5) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_addRoundKey_cpy_b,CNT_1___aes_addRoundKey_cpywhile, 0, i--)) goto endCNT_1___aes_addRoundKey_cpywhile5;
																		CHECK_LOOP_INCR(CNT_1___aes_addRoundKey_cpywhile,1, CNT_1___aes_addRoundKey_cpy_b)
    {
																		CHECK_INCR(CNT_1___aes_addRoundKey_cpywhile,2)
      buf[i] ^= key[i];
																		CHECK_INCR(CNT_1___aes_addRoundKey_cpywhile,3)
      cpk[i] = key[i];
																		CHECK_INCR(CNT_1___aes_addRoundKey_cpywhile,4)
      cpk[16+i] = key[16 + i];
																		CHECK_INCR(CNT_1___aes_addRoundKey_cpywhile,5)
    }
goto beginCNT_1___aes_addRoundKey_cpywhile5;
endCNT_1___aes_addRoundKey_cpywhile5: ;
																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,17)
																		CHECK_END_LOOP(CNT_1___aes_addRoundKey_cpywhile,CNT_1___aes_addRoundKey_cpy_b, 1)
																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,18)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_addRoundKey_cpy,19)
} /* aes_addRoundKey_cpy */


/* -------------------------------------------------------------------------- */
void aes_shiftRows(uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes_shiftRows,13)
																		CHECK_INCR(CNT_0___aes_shiftRows,13)
    register uint8_t i, j; /* to make it potentially parallelable :) */

																		CHECK_INCR(CNT_0___aes_shiftRows,14)
    i = buf[1]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,15)
    buf[1] = buf[5]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,16)
    buf[5] = buf[9]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,17)
    buf[9] = buf[13];
																		CHECK_INCR(CNT_0___aes_shiftRows,18)
    buf[13] = i;
																		CHECK_INCR(CNT_0___aes_shiftRows,19)
    i = buf[10];
																		CHECK_INCR(CNT_0___aes_shiftRows,20)
    buf[10] = buf[2]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,21)
    buf[2] = i;
																		CHECK_INCR(CNT_0___aes_shiftRows,22)
    j = buf[3]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,23)
    buf[3] = buf[15]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,24)
    buf[15] = buf[11];
																		CHECK_INCR(CNT_0___aes_shiftRows,25)
    buf[11] = buf[7];
																		CHECK_INCR(CNT_0___aes_shiftRows,26)
    buf[7] = j;
																		CHECK_INCR(CNT_0___aes_shiftRows,27)
    j = buf[14]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,28)
    buf[14] = buf[6]; 
																		CHECK_INCR(CNT_0___aes_shiftRows,29)
    buf[6]  = j;
																		CHECK_INCR(CNT_0___aes_shiftRows,30)
    
} /* aes_shiftRows */

/* -------------------------------------------------------------------------- */
void aes_shiftRows_inv(uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes_shiftRows_inv,13)
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,13)
    register uint8_t i, j; /* same as above :) */

																		CHECK_INCR(CNT_0___aes_shiftRows_inv,14)
    i = buf[1]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,15)
    buf[1] = buf[13];
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,16)
    buf[13] = buf[9]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,17)
    buf[9] = buf[5]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,18)
    buf[5] = i;
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,19)
    i = buf[2]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,20)
    buf[2] = buf[10]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,21)
    buf[10] = i;
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,22)
    j = buf[3]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,23)
    buf[3] = buf[7]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,24)
    buf[7] = buf[11]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,25)
    buf[11] = buf[15]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,26)
    buf[15] = j;
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,27)
    j = buf[6]; 
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,28)
    buf[6] = buf[14];
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,29)
    buf[14] = j;
																		CHECK_INCR(CNT_0___aes_shiftRows_inv,30)
    
} /* aes_shiftRows_inv */

/* -------------------------------------------------------------------------- */
void aes_mixColumns(uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes_mixColumns,13)
																		CHECK_INCR(CNT_0___aes_mixColumns,13)
    register uint8_t i, a, b, c, d, e;

																		CHECK_INCR(CNT_0___aes_mixColumns,14)
																		DECL_INIT(CNT_1___aes_mixColumnsfor,0)
																		CHECK_INCR(CNT_0___aes_mixColumns,15)
																		DECL_INIT(CNT_1___aes_mixColumns_b,1)
																		CHECK_INCR(CNT_0___aes_mixColumns,16)
i = 0;
																		CHECK_INCR(CNT_0___aes_mixColumns,17)
beginCNT_1___aes_mixColumnsfor6:
																		CNT_1___aes_mixColumnsfor = !(CNT_1___aes_mixColumnsfor == 0 || CNT_1___aes_mixColumnsfor == CNT_1___aes_mixColumnsNBEND6) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_mixColumns_b,CNT_1___aes_mixColumnsfor, 0,  i < 16)) goto endCNT_1___aes_mixColumnsfor6;
																		CHECK_LOOP_INCR(CNT_1___aes_mixColumnsfor,1, CNT_1___aes_mixColumns_b)
    {
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,2)
        a = buf[i];
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,3)
	b = buf[i + 1]; 
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,4)
	c = buf[i + 2]; 
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,5)
	d = buf[i + 3];
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,6)
        e = a ^ b ^ c ^ d;
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,7)
        buf[i] ^= e ^ rj_xtime(a^b); 
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,8)
	buf[i+1] ^= e ^ rj_xtime(b^c);
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,9)
        buf[i+2] ^= e ^ rj_xtime(c^d); 
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,10)
	buf[i+3] ^= e ^ rj_xtime(d^a);
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,11)
    }
 i += 4;
																		CHECK_INCR(CNT_1___aes_mixColumnsfor,12)
goto beginCNT_1___aes_mixColumnsfor6;
endCNT_1___aes_mixColumnsfor6: ;
																		CHECK_INCR(CNT_0___aes_mixColumns,18)
																		CHECK_END_LOOP(CNT_1___aes_mixColumnsfor,CNT_1___aes_mixColumns_b, 1)
																		CHECK_INCR(CNT_0___aes_mixColumns,19)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_mixColumns,20)
} /* aes_mixColumns */

/* -------------------------------------------------------------------------- */
void aes_mixColumns_inv(uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes_mixColumns_inv,13)
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,13)
    register uint8_t i, a, b, c, d, e, x, y, z;

																		CHECK_INCR(CNT_0___aes_mixColumns_inv,14)
																		DECL_INIT(CNT_1___aes_mixColumns_invfor,0)
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,15)
																		DECL_INIT(CNT_1___aes_mixColumns_inv_b,1)
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,16)
i = 0;
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,17)
beginCNT_1___aes_mixColumns_invfor7:
																		CNT_1___aes_mixColumns_invfor = !(CNT_1___aes_mixColumns_invfor == 0 || CNT_1___aes_mixColumns_invfor == CNT_1___aes_mixColumns_invNBEND7) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_mixColumns_inv_b,CNT_1___aes_mixColumns_invfor, 0,  i < 16)) goto endCNT_1___aes_mixColumns_invfor7;
																		CHECK_LOOP_INCR(CNT_1___aes_mixColumns_invfor,1, CNT_1___aes_mixColumns_inv_b)
    {
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,2)
      a = buf[i];
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,3)
      b = buf[i + 1];
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,4)
      c = buf[i + 2]; 
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,5)
      d = buf[i + 3];
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,6)
      e = a ^ b ^ c ^ d;
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,7)
      z = rj_xtime(e);
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,8)
      x = e ^ rj_xtime(rj_xtime(z^a^c)); 
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,9)
      y = e ^ rj_xtime(rj_xtime(z^b^d));
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,10)
      buf[i] ^= x ^ rj_xtime(a^b);   
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,11)
      buf[i+1] ^= y ^ rj_xtime(b^c);
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,12)
      buf[i+2] ^= x ^ rj_xtime(c^d); 
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,13)
      buf[i+3] ^= y ^ rj_xtime(d^a);
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,14)
    }
 i += 4;
																		CHECK_INCR(CNT_1___aes_mixColumns_invfor,15)
goto beginCNT_1___aes_mixColumns_invfor7;
endCNT_1___aes_mixColumns_invfor7: ;
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,18)
																		CHECK_END_LOOP(CNT_1___aes_mixColumns_invfor,CNT_1___aes_mixColumns_inv_b, 1)
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,19)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_mixColumns_inv,20)
} /* aes_mixColumns_inv */

/* -------------------------------------------------------------------------- */
void aes_expandEncKey(uint8_t *k, uint8_t *rc) 
{
																		DECL_INIT(CNT_0___aes_expandEncKey,13)
																		CHECK_INCR(CNT_0___aes_expandEncKey,13)
    register uint8_t i;

																		CHECK_INCR(CNT_0___aes_expandEncKey,14)
    k[0] ^= rj_sbox(k[29]) ^ (*rc);
																		CHECK_INCR(CNT_0___aes_expandEncKey,15)
    k[1] ^= rj_sbox(k[30]);
																		CHECK_INCR(CNT_0___aes_expandEncKey,16)
    k[2] ^= rj_sbox(k[31]);
																		CHECK_INCR(CNT_0___aes_expandEncKey,17)
    k[3] ^= rj_sbox(k[28]);
																		CHECK_INCR(CNT_0___aes_expandEncKey,18)
    *rc = F( *rc);

																		CHECK_INCR(CNT_0___aes_expandEncKey,19)
																		DECL_INIT(CNT_1___aes_expandEncKeyfor,0)
																		CHECK_INCR(CNT_0___aes_expandEncKey,20)
																		DECL_INIT(CNT_1___aes_expandEncKey_b,1)
																		CHECK_INCR(CNT_0___aes_expandEncKey,21)
i = 4;
																		CHECK_INCR(CNT_0___aes_expandEncKey,22)
beginCNT_1___aes_expandEncKeyfor8:
																		CNT_1___aes_expandEncKeyfor = !(CNT_1___aes_expandEncKeyfor == 0 || CNT_1___aes_expandEncKeyfor == CNT_1___aes_expandEncKeyNBEND8) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_expandEncKey_b,CNT_1___aes_expandEncKeyfor, 0,  i < 16)) goto endCNT_1___aes_expandEncKeyfor8;
																		CHECK_LOOP_INCR(CNT_1___aes_expandEncKeyfor,1, CNT_1___aes_expandEncKey_b)
    {
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,2)
      k[i] ^= k[i-4];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,3)
      k[i+1] ^= k[i-3];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,4)
      k[i+2] ^= k[i-2];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,5)
      k[i+3] ^= k[i-1];	
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,6)
    }
 i += 4;
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,7)
goto beginCNT_1___aes_expandEncKeyfor8;
endCNT_1___aes_expandEncKeyfor8: ;
																		CHECK_INCR(CNT_0___aes_expandEncKey,23)
																		CHECK_END_LOOP(CNT_1___aes_expandEncKeyfor,CNT_1___aes_expandEncKey_b, 1)
																		CHECK_INCR(CNT_0___aes_expandEncKey,24)
    k[16] ^= rj_sbox(k[12]);
																		CHECK_INCR(CNT_0___aes_expandEncKey,25)
    k[17] ^= rj_sbox(k[13]);
																		CHECK_INCR(CNT_0___aes_expandEncKey,26)
    k[18] ^= rj_sbox(k[14]);
																		CHECK_INCR(CNT_0___aes_expandEncKey,27)
    k[19] ^= rj_sbox(k[15]);

																		CHECK_INCR(CNT_0___aes_expandEncKey,28)
																		INIT(CNT_1___aes_expandEncKeyfor,0)
																		CHECK_INCR(CNT_0___aes_expandEncKey,29)
																		INIT(CNT_1___aes_expandEncKey_b,1)
																		CHECK_INCR(CNT_0___aes_expandEncKey,30)
i = 20;
																		CHECK_INCR(CNT_0___aes_expandEncKey,31)
beginCNT_1___aes_expandEncKeyfor9:
																		CNT_1___aes_expandEncKeyfor = !(CNT_1___aes_expandEncKeyfor == 0 || CNT_1___aes_expandEncKeyfor == CNT_1___aes_expandEncKeyNBEND9) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_expandEncKey_b,CNT_1___aes_expandEncKeyfor, 0,  i < 32)) goto endCNT_1___aes_expandEncKeyfor9;
																		CHECK_LOOP_INCR(CNT_1___aes_expandEncKeyfor,1, CNT_1___aes_expandEncKey_b)
    {
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,2)
      k[i] ^= k[i-4];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,3)
      k[i+1] ^= k[i-3];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,4)
      k[i+2] ^= k[i-2];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,5)
      k[i+3] ^= k[i-1];
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,6)
    }
 i += 4;
																		CHECK_INCR(CNT_1___aes_expandEncKeyfor,7)
goto beginCNT_1___aes_expandEncKeyfor9;
endCNT_1___aes_expandEncKeyfor9: ;
																		CHECK_INCR(CNT_0___aes_expandEncKey,32)
																		CHECK_END_LOOP(CNT_1___aes_expandEncKeyfor,CNT_1___aes_expandEncKey_b, 1)
																		CHECK_INCR(CNT_0___aes_expandEncKey,33)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes_expandEncKey,34)
    
} /* aes_expandEncKey */

/* -------------------------------------------------------------------------- */
void aes_expandDecKey(uint8_t *k, uint8_t *rc) 
{
																		DECL_INIT(CNT_0___aes_expandDecKey,13)
																		CHECK_INCR(CNT_0___aes_expandDecKey,13)
    uint8_t i;

																		CHECK_INCR(CNT_0___aes_expandDecKey,14)
																		DECL_INIT(CNT_1___aes_expandDecKeyfor,0)
																		CHECK_INCR(CNT_0___aes_expandDecKey,15)
																		DECL_INIT(CNT_1___aes_expandDecKey_b,1)
																		CHECK_INCR(CNT_0___aes_expandDecKey,16)
i = 28;
																		CHECK_INCR(CNT_0___aes_expandDecKey,17)
beginCNT_1___aes_expandDecKeyfor10:
																		CNT_1___aes_expandDecKeyfor = !(CNT_1___aes_expandDecKeyfor == 0 || CNT_1___aes_expandDecKeyfor == CNT_1___aes_expandDecKeyNBEND10) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_expandDecKey_b,CNT_1___aes_expandDecKeyfor, 0,  i > 16)) goto endCNT_1___aes_expandDecKeyfor10;
																		CHECK_LOOP_INCR(CNT_1___aes_expandDecKeyfor,1, CNT_1___aes_expandDecKey_b)
    {
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,2)
      k[i+0] ^= k[i-4];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,3)
      k[i+1] ^= k[i-3];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,4)
      k[i+2] ^= k[i-2];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,5)
      k[i+3] ^= k[i-1];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,6)
    }
 i -= 4;
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,7)
goto beginCNT_1___aes_expandDecKeyfor10;
endCNT_1___aes_expandDecKeyfor10: ;
																		CHECK_INCR(CNT_0___aes_expandDecKey,18)
																		CHECK_END_LOOP(CNT_1___aes_expandDecKeyfor,CNT_1___aes_expandDecKey_b, 1)
																		CHECK_INCR(CNT_0___aes_expandDecKey,19)
    k[16] ^= rj_sbox(k[12]);
																		CHECK_INCR(CNT_0___aes_expandDecKey,20)
    k[17] ^= rj_sbox(k[13]);
																		CHECK_INCR(CNT_0___aes_expandDecKey,21)
    k[18] ^= rj_sbox(k[14]);
																		CHECK_INCR(CNT_0___aes_expandDecKey,22)
    k[19] ^= rj_sbox(k[15]);

																		CHECK_INCR(CNT_0___aes_expandDecKey,23)
																		INIT(CNT_1___aes_expandDecKeyfor,0)
																		CHECK_INCR(CNT_0___aes_expandDecKey,24)
																		INIT(CNT_1___aes_expandDecKey_b,1)
																		CHECK_INCR(CNT_0___aes_expandDecKey,25)
i = 12;
																		CHECK_INCR(CNT_0___aes_expandDecKey,26)
beginCNT_1___aes_expandDecKeyfor11:
																		CNT_1___aes_expandDecKeyfor = !(CNT_1___aes_expandDecKeyfor == 0 || CNT_1___aes_expandDecKeyfor == CNT_1___aes_expandDecKeyNBEND11) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes_expandDecKey_b,CNT_1___aes_expandDecKeyfor, 0,  i > 0)) goto endCNT_1___aes_expandDecKeyfor11;
																		CHECK_LOOP_INCR(CNT_1___aes_expandDecKeyfor,1, CNT_1___aes_expandDecKey_b)
    {
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,2)
      k[i+0] ^= k[i-4];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,3)
      k[i+1] ^= k[i-3];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,4)
      k[i+2] ^= k[i-2];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,5)
      k[i+3] ^= k[i-1];
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,6)
    }
 i -= 4;
																		CHECK_INCR(CNT_1___aes_expandDecKeyfor,7)
goto beginCNT_1___aes_expandDecKeyfor11;
endCNT_1___aes_expandDecKeyfor11: ;
																		CHECK_INCR(CNT_0___aes_expandDecKey,27)
																		CHECK_END_LOOP(CNT_1___aes_expandDecKeyfor,CNT_1___aes_expandDecKey_b, 1)
																		CHECK_INCR(CNT_0___aes_expandDecKey,28)
    *rc = FD(*rc);
																		CHECK_INCR(CNT_0___aes_expandDecKey,29)
    k[0] ^= rj_sbox(k[29]) ^ (*rc);
																		CHECK_INCR(CNT_0___aes_expandDecKey,30)
    k[1] ^= rj_sbox(k[30]);
																		CHECK_INCR(CNT_0___aes_expandDecKey,31)
    k[2] ^= rj_sbox(k[31]);
																		CHECK_INCR(CNT_0___aes_expandDecKey,32)
    k[3] ^= rj_sbox(k[28]);
																		CHECK_INCR(CNT_0___aes_expandDecKey,33)
} /* aes_expandDecKey */


/* -------------------------------------------------------------------------- */
void aes256_init(aes256_context *ctx, uint8_t *k)
{
																		DECL_INIT(CNT_0___aes256_init,13)
																		CHECK_INCR(CNT_0___aes256_init,13)
    uint8_t rcon = 1;
																		CHECK_INCR(CNT_0___aes256_init,14)
    register uint8_t i;

																		CHECK_INCR(CNT_0___aes256_init,15)
																		DECL_INIT(CNT_1___aes256_initfor,0)
																		CHECK_INCR(CNT_0___aes256_init,16)
																		DECL_INIT(CNT_1___aes256_init_b,1)
																		CHECK_INCR(CNT_0___aes256_init,17)
i = 0;
																		CHECK_INCR(CNT_0___aes256_init,18)
beginCNT_1___aes256_initfor12:
																		CNT_1___aes256_initfor = !(CNT_1___aes256_initfor == 0 || CNT_1___aes256_initfor == CNT_1___aes256_initNBEND12) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes256_init_b,CNT_1___aes256_initfor, 0,  i < sizeof(ctx->key))) goto endCNT_1___aes256_initfor12;
																		CHECK_LOOP_INCR(CNT_1___aes256_initfor,1, CNT_1___aes256_init_b)
    {
																		CHECK_INCR(CNT_1___aes256_initfor,2)
      ctx->enckey[i] = ctx->deckey[i] = k[i];
																		CHECK_INCR(CNT_1___aes256_initfor,3)
    }
 i++;
																		CHECK_INCR(CNT_1___aes256_initfor,4)
goto beginCNT_1___aes256_initfor12;
endCNT_1___aes256_initfor12: ;
																		CHECK_INCR(CNT_0___aes256_init,19)
																		CHECK_END_LOOP(CNT_1___aes256_initfor,CNT_1___aes256_init_b, 1)
																		CHECK_INCR(CNT_0___aes256_init,20)
																		INIT(CNT_1___aes256_initfor,0)
																		CHECK_INCR(CNT_0___aes256_init,21)
																		INIT(CNT_1___aes256_init_b,1)
																		CHECK_INCR(CNT_0___aes256_init,22)
i = 8;
																		CHECK_INCR(CNT_0___aes256_init,23)
beginCNT_1___aes256_initfor13:
																		CNT_1___aes256_initfor = !(CNT_1___aes256_initfor == 0 || CNT_1___aes256_initfor == CNT_1___aes256_initNBEND13) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes256_init_b,CNT_1___aes256_initfor, 0, --i)) goto endCNT_1___aes256_initfor13;
																		CHECK_LOOP_INCR(CNT_1___aes256_initfor,1, CNT_1___aes256_init_b)
    {
																		CHECK_INCR(CNT_1___aes256_initfor,2)
      aes_expandEncKey(ctx->deckey, &rcon);
																		CHECK_INCR(CNT_1___aes256_initfor,3)
    }
;
																		CHECK_INCR(CNT_1___aes256_initfor,4)
goto beginCNT_1___aes256_initfor13;
endCNT_1___aes256_initfor13: ;
																		CHECK_INCR(CNT_0___aes256_init,24)
																		CHECK_END_LOOP(CNT_1___aes256_initfor,CNT_1___aes256_init_b, 1)
																		CHECK_INCR(CNT_0___aes256_init,25)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes256_init,26)
} /* aes256_init */

/* -------------------------------------------------------------------------- */
void aes256_done(aes256_context *ctx)
{
																		DECL_INIT(CNT_0___aes256_done,13)
																		CHECK_INCR(CNT_0___aes256_done,13)
    register uint8_t i;

																		CHECK_INCR(CNT_0___aes256_done,14)
																		DECL_INIT(CNT_1___aes256_donefor,0)
																		CHECK_INCR(CNT_0___aes256_done,15)
																		DECL_INIT(CNT_1___aes256_done_b,1)
																		CHECK_INCR(CNT_0___aes256_done,16)
i = 0;
																		CHECK_INCR(CNT_0___aes256_done,17)
beginCNT_1___aes256_donefor14:
																		CNT_1___aes256_donefor = !(CNT_1___aes256_donefor == 0 || CNT_1___aes256_donefor == CNT_1___aes256_doneNBEND14) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes256_done_b,CNT_1___aes256_donefor, 0,  i < sizeof(ctx->key))) goto endCNT_1___aes256_donefor14;
																		CHECK_LOOP_INCR(CNT_1___aes256_donefor,1, CNT_1___aes256_done_b)
    {
																		CHECK_INCR(CNT_1___aes256_donefor,2)
        ctx->key[i] = ctx->enckey[i] = ctx->deckey[i] = 0;
																		CHECK_INCR(CNT_1___aes256_donefor,3)
    }
 i++;
																		CHECK_INCR(CNT_1___aes256_donefor,4)
goto beginCNT_1___aes256_donefor14;
endCNT_1___aes256_donefor14: ;
																		CHECK_INCR(CNT_0___aes256_done,18)
																		CHECK_END_LOOP(CNT_1___aes256_donefor,CNT_1___aes256_done_b, 1)
																		CHECK_INCR(CNT_0___aes256_done,19)
    int jfl = 1;
																		CHECK_INCR(CNT_0___aes256_done,20)
} /* aes256_done */

/* -------------------------------------------------------------------------- */
void aes256_encrypt_ecb(aes256_context *ctx, uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes256_encrypt_ecb,13)
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,13)
    uint8_t i, rcon;

																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,14)
    aes_addRoundKey_cpy(buf, ctx->enckey, ctx->key);
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,15)
																		DECL_INIT(CNT_1___aes256_encrypt_ecbfor,0)
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,16)
																		DECL_INIT(CNT_1___aes256_encrypt_ecb_b,1)
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,17)
i = 1, rcon = 1;
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,18)
beginCNT_1___aes256_encrypt_ecbfor15:
																		CNT_1___aes256_encrypt_ecbfor = !(CNT_1___aes256_encrypt_ecbfor == 0 || CNT_1___aes256_encrypt_ecbfor == CNT_1___aes256_encrypt_ecbNBEND15) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes256_encrypt_ecb_b,CNT_1___aes256_encrypt_ecbfor, 0,  i < 14)) goto endCNT_1___aes256_encrypt_ecbfor15;
																		CHECK_LOOP_INCR(CNT_1___aes256_encrypt_ecbfor,1, CNT_1___aes256_encrypt_ecb_b)
    {
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,2)
        aes_subBytes(buf);
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,3)
        aes_shiftRows(buf);
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,4)
        aes_mixColumns(buf);
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,5)
																		DECL_INIT(CNT_2___aes256_encrypt_ecbthen,0)
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,6)
																		DECL_INIT(CNT_2___aes256_encrypt_ecbelse,0)
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,7)
																		DECL_INIT(CNT_2___aes256_encrypt_ecb_b,1)
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,8)
if (CNT_2___aes256_encrypt_ecb_b = ( i & 1 ))
        {
																		CHECK_INCR(CNT_2___aes256_encrypt_ecbthen,0)
	  aes_addRoundKey( buf, &ctx->key[16]);
																		CHECK_INCR(CNT_2___aes256_encrypt_ecbthen,1)
        }
        else {
																		CHECK_INCR(CNT_2___aes256_encrypt_ecbelse,0)
	  aes_expandEncKey(ctx->key, &rcon);
																		CHECK_INCR(CNT_2___aes256_encrypt_ecbelse,1)
	  aes_addRoundKey(buf, ctx->key);
																		CHECK_INCR(CNT_2___aes256_encrypt_ecbelse,2)
	}
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,9)
																		CHECK_END_IF_ELSE(CNT_2___aes256_encrypt_ecbthen,CNT_2___aes256_encrypt_ecbelse,CNT_2___aes256_encrypt_ecb_b,2,3)
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,10)
        int jfl = 1;
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,11)
    }
 ++i;
																		CHECK_INCR(CNT_1___aes256_encrypt_ecbfor,12)
goto beginCNT_1___aes256_encrypt_ecbfor15;
endCNT_1___aes256_encrypt_ecbfor15: ;
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,19)
																		CHECK_END_LOOP(CNT_1___aes256_encrypt_ecbfor,CNT_1___aes256_encrypt_ecb_b, 1)
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,20)
    aes_subBytes(buf);
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,21)
    aes_shiftRows(buf);
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,22)
    aes_expandEncKey(ctx->key, &rcon); 
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,23)
    aes_addRoundKey(buf, ctx->key);
																		CHECK_INCR(CNT_0___aes256_encrypt_ecb,24)
} /* aes256_encrypt */

/* -------------------------------------------------------------------------- */
void aes256_decrypt_ecb(aes256_context *ctx, uint8_t *buf)
{
																		DECL_INIT(CNT_0___aes256_decrypt_ecb,13)
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,13)
    uint8_t i, rcon;

																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,14)
    aes_addRoundKey_cpy(buf, ctx->deckey, ctx->key);
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,15)
    aes_shiftRows_inv(buf);
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,16)
    aes_subBytes_inv(buf);

																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,17)
																		DECL_INIT(CNT_1___aes256_decrypt_ecbfor,0)
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,18)
																		DECL_INIT(CNT_1___aes256_decrypt_ecb_b,1)
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,19)
i = 14, rcon = 0x80;
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,20)
beginCNT_1___aes256_decrypt_ecbfor16:
																		CNT_1___aes256_decrypt_ecbfor = !(CNT_1___aes256_decrypt_ecbfor == 0 || CNT_1___aes256_decrypt_ecbfor == CNT_1___aes256_decrypt_ecbNBEND16) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___aes256_decrypt_ecb_b,CNT_1___aes256_decrypt_ecbfor, 0,  --i)) goto endCNT_1___aes256_decrypt_ecbfor16;
																		CHECK_LOOP_INCR(CNT_1___aes256_decrypt_ecbfor,1, CNT_1___aes256_decrypt_ecb_b)
    {
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,2)
																		DECL_INIT(CNT_2___aes256_decrypt_ecbthen,0)
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,3)
																		DECL_INIT(CNT_2___aes256_decrypt_ecbelse,0)
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,4)
																		DECL_INIT(CNT_2___aes256_decrypt_ecb_b,1)
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,5)
if (CNT_2___aes256_decrypt_ecb_b = ( ( i & 1 ) ))
        {
																		CHECK_INCR(CNT_2___aes256_decrypt_ecbthen,0)
            aes_expandDecKey(ctx->key, &rcon);
																		CHECK_INCR(CNT_2___aes256_decrypt_ecbthen,1)
            aes_addRoundKey(buf, &ctx->key[16]);
																		CHECK_INCR(CNT_2___aes256_decrypt_ecbthen,2)
        }
        else 
        {
																		CHECK_INCR(CNT_2___aes256_decrypt_ecbelse,0)
	  aes_addRoundKey(buf, ctx->key);
																		CHECK_INCR(CNT_2___aes256_decrypt_ecbelse,1)
        }
        
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,6)
																		CHECK_END_IF_ELSE(CNT_2___aes256_decrypt_ecbthen,CNT_2___aes256_decrypt_ecbelse,CNT_2___aes256_decrypt_ecb_b,3,2)
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,7)
	aes_mixColumns_inv(buf);
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,8)
        aes_shiftRows_inv(buf);
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,9)
        aes_subBytes_inv(buf);
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,10)
    }
;
																		CHECK_INCR(CNT_1___aes256_decrypt_ecbfor,11)
goto beginCNT_1___aes256_decrypt_ecbfor16;
endCNT_1___aes256_decrypt_ecbfor16: ;
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,21)
																		CHECK_END_LOOP(CNT_1___aes256_decrypt_ecbfor,CNT_1___aes256_decrypt_ecb_b, 1)
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,22)
    aes_addRoundKey( buf, ctx->key); 
																		CHECK_INCR(CNT_0___aes256_decrypt_ecb,23)
} /* aes256_decrypt */


int main(int argc, char *argv[])
{
																		DECL_INIT(CNT_0___main,13)
																		CHECK_INCR(CNT_0___main,13)
    aes256_context ctx; 
																		CHECK_INCR(CNT_0___main,14)
    uint8_t key[32];
																		CHECK_INCR(CNT_0___main,15)
    uint8_t buf[16], i;

    /* put a test vector */
																		CHECK_INCR(CNT_0___main,16)
																		DECL_INIT(CNT_1___mainfor,0)
																		CHECK_INCR(CNT_0___main,17)
																		DECL_INIT(CNT_1___main_b,1)
																		CHECK_INCR(CNT_0___main,18)
i = 0;
																		CHECK_INCR(CNT_0___main,19)
beginCNT_1___mainfor17:
																		CNT_1___mainfor = !(CNT_1___mainfor == 0 || CNT_1___mainfor == CNT_1___mainNBEND17) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___main_b,CNT_1___mainfor, 0,  i < sizeof(buf))) goto endCNT_1___mainfor17;
																		CHECK_LOOP_INCR(CNT_1___mainfor,1, CNT_1___main_b)
    {
																		CHECK_INCR(CNT_1___mainfor,2)
    	buf[i] = i * 16 + i;
																		CHECK_INCR(CNT_1___mainfor,3)
    }
i++;
																		CHECK_INCR(CNT_1___mainfor,4)
goto beginCNT_1___mainfor17;
endCNT_1___mainfor17: ;
																		CHECK_INCR(CNT_0___main,20)
																		CHECK_END_LOOP(CNT_1___mainfor,CNT_1___main_b, 1)
																		CHECK_INCR(CNT_0___main,21)
																		INIT(CNT_1___mainfor,0)
																		CHECK_INCR(CNT_0___main,22)
																		INIT(CNT_1___main_b,1)
																		CHECK_INCR(CNT_0___main,23)
i = 0;
																		CHECK_INCR(CNT_0___main,24)
beginCNT_1___mainfor18:
																		CNT_1___mainfor = !(CNT_1___mainfor == 0 || CNT_1___mainfor == CNT_1___mainNBEND18) ? killcard() : 0;
if (!CHECK_INCR_COND(CNT_1___main_b,CNT_1___mainfor, 0,  i < sizeof(key))) goto endCNT_1___mainfor18;
																		CHECK_LOOP_INCR(CNT_1___mainfor,1, CNT_1___main_b)
    {
																		CHECK_INCR(CNT_1___mainfor,2)
    	key[i] = i;
																		CHECK_INCR(CNT_1___mainfor,3)
    }

i++;
																		CHECK_INCR(CNT_1___mainfor,4)
goto beginCNT_1___mainfor18;
endCNT_1___mainfor18: ;
																		CHECK_INCR(CNT_0___main,25)
																		CHECK_END_LOOP(CNT_1___mainfor,CNT_1___main_b, 1)
																		CHECK_INCR(CNT_0___main,26)
    int jfl = 1;

																		CHECK_INCR(CNT_0___main,27)
    DUMP("txt: ", i, buf, sizeof(buf));
																		CHECK_INCR(CNT_0___main,28)
    DUMP("key: ", i, key, sizeof(key));
																		CHECK_INCR(CNT_0___main,29)
    printf("---\n");

																		CHECK_INCR(CNT_0___main,30)
    aes256_init(&ctx, key);
																		CHECK_INCR(CNT_0___main,31)
    aes256_encrypt_ecb(&ctx, buf);

																		CHECK_INCR(CNT_0___main,32)
    DUMP("enc: ", i, buf, sizeof(buf));
																		CHECK_INCR(CNT_0___main,33)
    printf("tst: 8e a2 b7 ca 51 67 45 bf ea fc 49 90 4b 49 60 89\n");

																		CHECK_INCR(CNT_0___main,34)
    aes256_init(&ctx, key);
																		CHECK_INCR(CNT_0___main,35)
    aes256_decrypt_ecb(&ctx, buf);
																		CHECK_INCR(CNT_0___main,36)
    DUMP("dec: ", i, buf, sizeof(buf));

																		CHECK_INCR(CNT_0___main,37)
    aes256_done(&ctx);

																		CHECK_INCR(CNT_0___main,38)
    return 0;
} /* main */
