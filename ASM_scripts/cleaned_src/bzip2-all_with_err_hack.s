	.file	"all.c"
	.comm	verbosity,4,4
	.comm	keepInputFiles,1,1
	.comm	smallMode,1,1
	.comm	deleteOutputOnInterrupt,1,1
	.comm	forceOverwrite,1,1
	.comm	testFailsExist,1,1
	.comm	unzFailsExist,1,1
	.comm	noisy,1,1
	.comm	numFileNames,4,4
	.comm	numFilesProcessed,4,4
	.comm	blockSize100k,4,4
	.comm	exitValue,4,4
	.comm	opMode,4,4
	.comm	srcMode,4,4
	.comm	longestFileName,4,4
	.comm	inName,1034,32
	.comm	outName,1034,32
	.comm	tmpName,1034,32
	.comm	progName,8,8
	.comm	progNameReally,1034,32
	.comm	outputHandleJustInCase,8,8
	.comm	workFactor,4,4
	.text
	.type	uInt64_from_UInt32s, @function
uInt64_from_UInt32s:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	%edx, -16(%rbp)
	movl	-16(%rbp), %eax
	shrl	$24, %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 7(%rax)
	movl	-16(%rbp), %eax
	shrl	$16, %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 6(%rax)
	movl	-16(%rbp), %eax
	shrl	$8, %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 5(%rax)
	movl	-16(%rbp), %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 4(%rax)
	movl	-12(%rbp), %eax
	shrl	$24, %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 3(%rax)
	movl	-12(%rbp), %eax
	shrl	$16, %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 2(%rax)
	movl	-12(%rbp), %eax
	shrl	$8, %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 1(%rax)
	movl	-12(%rbp), %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	uInt64_from_UInt32s, .-uInt64_from_UInt32s
	.type	uInt64_to_double, @function
uInt64_to_double:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -40(%rbp)
	movabsq	$4607182418800017408, %rax
	movq	%rax, -24(%rbp)
	movl	$0, %eax
	movq	%rax, -16(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L3
.L4:
	movq	-40(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	movzbl	%al, %eax
	cvtsi2sd	%eax, %xmm0
	mulsd	-24(%rbp), %xmm0
	movsd	-16(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -16(%rbp)
	movsd	-24(%rbp), %xmm1
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -24(%rbp)
	addl	$1, -4(%rbp)
.L3:
	cmpl	$7, -4(%rbp)
	jle	.L4
	movq	-16(%rbp), %rax
	movq	%rax, -48(%rbp)
	movsd	-48(%rbp), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	uInt64_to_double, .-uInt64_to_double
	.type	uInt64_isZero, @function
uInt64_isZero:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L6
.L9:
	movq	-24(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L7
	movl	$0, %eax
	jmp	.L8
.L7:
	addl	$1, -4(%rbp)
.L6:
	cmpl	$7, -4(%rbp)
	jle	.L9
	movl	$1, %eax
.L8:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	uInt64_isZero, .-uInt64_isZero
	.type	uInt64_qrm10, @function
uInt64_qrm10:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	$0, -12(%rbp)
	movl	$7, -8(%rbp)
	jmp	.L11
.L12:
	movl	-12(%rbp), %eax
	movl	%eax, %ecx
	sall	$8, %ecx
	movq	-24(%rbp), %rdx
	movl	-8(%rbp), %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	movzbl	%al, %eax
	addl	%ecx, %eax
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, -28(%rbp)
	movl	$-858993459, %edx
	movl	-28(%rbp), %eax
	mull	%edx
	movl	%edx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	movq	-24(%rbp), %rdx
	movl	-8(%rbp), %eax
	cltq
	movb	%cl, (%rdx,%rax)
	movl	-4(%rbp), %ecx
	movl	$-858993459, %edx
	movl	%ecx, %eax
	mull	%edx
	shrl	$3, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -12(%rbp)
	subl	$1, -8(%rbp)
.L11:
	cmpl	$0, -8(%rbp)
	jns	.L12
	movl	-12(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	uInt64_qrm10, .-uInt64_qrm10
	.type	uInt64_toAscii, @function
uInt64_toAscii:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$88, %rsp
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
.L14:
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	.cfi_offset 3, -24
	call	uInt64_qrm10
	movl	%eax, -68(%rbp)
	movl	-68(%rbp), %eax
	leal	48(%rax), %edx
	movl	-72(%rbp), %eax
	cltq
	movb	%dl, -64(%rbp,%rax)
	addl	$1, -72(%rbp)
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	uInt64_isZero
	testb	%al, %al
	je	.L14
	movl	-72(%rbp), %eax
	cltq
	addq	-88(%rbp), %rax
	movb	$0, (%rax)
	movl	$0, -76(%rbp)
	jmp	.L15
.L16:
	movl	-76(%rbp), %eax
	cltq
	addq	-88(%rbp), %rax
	movl	-76(%rbp), %edx
	movl	-72(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	subl	$1, %edx
	movslq	%edx, %rdx
	movzbl	-64(%rbp,%rdx), %edx
	movb	%dl, (%rax)
	addl	$1, -76(%rbp)
.L15:
	movl	-76(%rbp), %eax
	cmpl	-72(%rbp), %eax
	jl	.L16
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L17
	call	__stack_chk_fail
.L17:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	uInt64_toAscii, .-uInt64_toAscii
	.globl	myfeof
	.type	myfeof, @function
myfeof:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	fgetc
	movl	%eax, -4(%rbp)
	cmpl	$-1, -4(%rbp)
	jne	.L19
	movl	$1, %eax
	jmp	.L20
.L19:
	movq	-24(%rbp), %rdx
	movl	-4(%rbp), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	ungetc
	movl	$0, %eax
.L20:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	myfeof, .-myfeof
	.section	.rodata
.LC3:
	.string	" no data compressed.\n"
	.align 8
.LC6:
	.string	"%6.3f:1, %6.3f bits/byte, %5.2f%% saved, %s in, %s out.\n"
.LC7:
	.string	"compress:unexpected error"
.LC8:
	.string	"compress:end"
	.text
	.type	compressStream, @function
compressStream:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$5200, %rsp
	movq	%rdi, -5176(%rbp)
	movq	%rsi, -5184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$0, -5168(%rbp)
	movq	-5176(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L50
.L22:
	movq	-5184(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L51
.L24:
	movl	workFactor(%rip), %edi
	movl	verbosity(%rip), %ecx
	movl	blockSize100k(%rip), %edx
	movq	-5184(%rbp), %rsi
	leaq	-5124(%rbp), %rax
	movl	%edi, %r8d
	movq	%rax, %rdi
	call	BZ2_bzWriteOpen
	movq	%rax, -5168(%rbp)
	movl	-5124(%rbp), %eax
	testl	%eax, %eax
	jne	.L52
.L25:
	movl	verbosity(%rip), %eax
	cmpl	$1, %eax
	jle	.L27
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$10, %edi
	call	fputc
	jmp	.L27
.L55:
	nop
.L27:
	movq	-5176(%rbp), %rax
	movq	%rax, %rdi
	call	myfeof
	testb	%al, %al
	jne	.L53
.L28:
	leaq	-5104(%rbp), %rax
	movq	-5176(%rbp), %rdx
	movq	%rdx, %rcx
	movl	$5000, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fread
	movl	%eax, -5108(%rbp)
	movq	-5176(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L54
.L30:
	cmpl	$0, -5108(%rbp)
	jle	.L31
	movl	-5108(%rbp), %ecx
	leaq	-5104(%rbp), %rdx
	movq	-5168(%rbp), %rsi
	leaq	-5124(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzWrite
.L31:
	movl	-5124(%rbp), %eax
	testl	%eax, %eax
	je	.L55
	jmp	.L26
.L53:
	nop
.L48:
	leaq	-5132(%rbp), %r8
	leaq	-5136(%rbp), %rdi
	leaq	-5140(%rbp), %rdx
	movq	-5168(%rbp), %rsi
	leaq	-5124(%rbp), %rax
	leaq	-5128(%rbp), %rcx
	movq	%rcx, (%rsp)
	movq	%r8, %r9
	movq	%rdi, %r8
	movq	%rdx, %rcx
	movl	$0, %edx
	movq	%rax, %rdi
	call	BZ2_bzWriteClose64
	movl	-5124(%rbp), %eax
	testl	%eax, %eax
	jne	.L56
.L33:
	movq	-5184(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L57
.L34:
	movq	-5184(%rbp), %rax
	movq	%rax, %rdi
	call	fflush
	movl	%eax, -5116(%rbp)
	cmpl	$-1, -5116(%rbp)
	je	.L58
.L35:
	movq	stdout(%rip), %rax
	cmpq	%rax, -5184(%rbp)
	je	.L36
	movq	-5184(%rbp), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, -5112(%rbp)
	cmpl	$0, -5112(%rbp)
	js	.L59
.L37:
	movl	-5112(%rbp), %eax
	movl	%eax, %edi
	call	applySavedFileAttrToOutputFile
	movq	-5184(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	movl	%eax, -5116(%rbp)
	movq	$0, outputHandleJustInCase(%rip)
	cmpl	$-1, -5116(%rbp)
	je	.L60
.L36:
	movq	$0, outputHandleJustInCase(%rip)
	movq	-5176(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L61
.L38:
	movq	-5176(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	movl	%eax, -5116(%rbp)
	cmpl	$-1, -5116(%rbp)
	je	.L62
.L39:
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L63
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	jne	.L41
	movl	-5136(%rbp), %eax
	testl	%eax, %eax
	jne	.L41
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC3, %eax
	movq	%rdx, %rcx
	movl	$21, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	jmp	.L63
.L41:
	movl	-5136(%rbp), %edx
	movl	-5140(%rbp), %ecx
	leaq	-32(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	uInt64_from_UInt32s
	movl	-5128(%rbp), %edx
	movl	-5132(%rbp), %ecx
	leaq	-16(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	uInt64_from_UInt32s
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	uInt64_to_double
	movsd	%xmm0, -5160(%rbp)
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	uInt64_to_double
	movsd	%xmm0, -5152(%rbp)
	leaq	-32(%rbp), %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	uInt64_toAscii
	leaq	-16(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	uInt64_toAscii
	movsd	-5152(%rbp), %xmm0
	divsd	-5160(%rbp), %xmm0
	movsd	.LC0(%rip), %xmm1
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	movsd	.LC4(%rip), %xmm1
	movapd	%xmm0, %xmm2
	mulsd	%xmm1, %xmm2
	movsd	-5152(%rbp), %xmm1
	movsd	.LC5(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	divsd	-5160(%rbp), %xmm1
	movsd	-5160(%rbp), %xmm0
	divsd	-5152(%rbp), %xmm0
	movl	$.LC6, %esi
	movq	stderr(%rip), %rax
	leaq	-64(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	movq	%rax, %rdi
	movl	$3, %eax
	call	fprintf
	jmp	.L63
.L52:
	nop
	jmp	.L26
.L56:
	nop
.L26:
	leaq	-5132(%rbp), %r8
	leaq	-5136(%rbp), %rdi
	leaq	-5140(%rbp), %rdx
	movq	-5168(%rbp), %rsi
	leaq	-5120(%rbp), %rax
	leaq	-5128(%rbp), %rcx
	movq	%rcx, (%rsp)
	movq	%r8, %r9
	movq	%rdi, %r8
	movq	%rdx, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	BZ2_bzWriteClose64
	movl	-5124(%rbp), %eax
	cmpl	$-6, %eax
	je	.L23
	cmpl	$-3, %eax
	je	.L45
	cmpl	$-9, %eax
	jne	.L49
.L44:
	call	configError
	jmp	.L46
.L45:
	call	outOfMemory
	jmp	.L46
.L50:
	nop
	jmp	.L23
.L51:
	nop
	jmp	.L23
.L54:
	nop
	jmp	.L23
.L57:
	nop
	jmp	.L23
.L58:
	nop
	jmp	.L23
.L59:
	nop
	jmp	.L23
.L60:
	nop
	jmp	.L23
.L61:
	nop
	jmp	.L23
.L62:
	nop
.L23:
	call	ioError
	jmp	.L46
.L49:
	movl	$.LC7, %edi
	call	panic
.L46:
	movl	$.LC8, %edi
	call	panic
	jmp	.L21
.L63:
	nop
.L21:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L47
	call	__stack_chk_fail
.L47:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	compressStream, .-compressStream
	.section	.rodata
.LC9:
	.string	"decompress:bzReadGetUnused"
.LC10:
	.string	"\n    "
	.align 8
.LC11:
	.string	"\n%s: %s: trailing garbage after EOF ignored\n"
.LC12:
	.string	"decompress:unexpected error"
.LC13:
	.string	"decompress:end"
	.text
	.type	uncompressStream, @function
uncompressStream:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$10096, %rsp
	movq	%rdi, -10088(%rbp)
	movq	%rsi, -10096(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$0, -10064(%rbp)
	movl	$0, -10040(%rbp)
	movl	$0, -10036(%rbp)
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L112
.L65:
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L113
	jmp	.L67
.L117:
	nop
.L67:
	movl	-10040(%rbp), %r8d
	movzbl	smallMode(%rip), %eax
	movzbl	%al, %ecx
	movl	verbosity(%rip), %edx
	leaq	-5008(%rbp), %rdi
	movq	-10088(%rbp), %rsi
	leaq	-10048(%rbp), %rax
	movl	%r8d, %r9d
	movq	%rdi, %r8
	movq	%rax, %rdi
	call	BZ2_bzReadOpen
	movq	%rax, -10064(%rbp)
	cmpq	$0, -10064(%rbp)
	je	.L68
	movl	-10048(%rbp), %eax
	testl	%eax, %eax
	jne	.L68
	addl	$1, -10036(%rbp)
	jmp	.L69
.L74:
	leaq	-10016(%rbp), %rdx
	movq	-10064(%rbp), %rsi
	leaq	-10048(%rbp), %rax
	movl	$5000, %ecx
	movq	%rax, %rdi
	call	BZ2_bzRead
	movl	%eax, -10028(%rbp)
	movl	-10048(%rbp), %eax
	cmpl	$-5, %eax
	je	.L114
.L70:
	movl	-10048(%rbp), %eax
	testl	%eax, %eax
	je	.L72
	movl	-10048(%rbp), %eax
	cmpl	$4, %eax
	jne	.L73
.L72:
	cmpl	$0, -10028(%rbp)
	jle	.L73
	movl	-10028(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-10016(%rbp), %rax
	movq	-10096(%rbp), %rcx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L73:
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L115
.L69:
	movl	-10048(%rbp), %eax
	testl	%eax, %eax
	je	.L74
	movl	-10048(%rbp), %eax
	cmpl	$4, %eax
	jne	.L116
.L75:
	leaq	-10040(%rbp), %rcx
	leaq	-10072(%rbp), %rdx
	movq	-10064(%rbp), %rsi
	leaq	-10048(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzReadGetUnused
	movl	-10048(%rbp), %eax
	testl	%eax, %eax
	je	.L76
	movl	$.LC9, %edi
	call	panic
.L76:
	movq	-10072(%rbp), %rax
	movq	%rax, -10056(%rbp)
	movl	$0, -10032(%rbp)
	jmp	.L77
.L78:
	movl	-10032(%rbp), %eax
	cltq
	addq	-10056(%rbp), %rax
	movzbl	(%rax), %edx
	movl	-10032(%rbp), %eax
	cltq
	movb	%dl, -5008(%rbp,%rax)
	addl	$1, -10032(%rbp)
.L77:
	movl	-10040(%rbp), %eax
	cmpl	%eax, -10032(%rbp)
	jl	.L78
	movq	-10064(%rbp), %rdx
	leaq	-10048(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	BZ2_bzReadClose
	movl	-10048(%rbp), %eax
	testl	%eax, %eax
	je	.L79
	movl	$.LC9, %edi
	call	panic
.L79:
	movl	-10040(%rbp), %eax
	testl	%eax, %eax
	jne	.L117
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	myfeof
	testb	%al, %al
	je	.L117
	nop
.L81:
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L118
.L82:
	movq	stdout(%rip), %rax
	cmpq	%rax, -10096(%rbp)
	je	.L83
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, -10024(%rbp)
	cmpl	$0, -10024(%rbp)
	js	.L119
.L84:
	movl	-10024(%rbp), %eax
	movl	%eax, %edi
	call	applySavedFileAttrToOutputFile
.L83:
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	movl	%eax, -10020(%rbp)
	cmpl	$-1, -10020(%rbp)
	je	.L120
.L85:
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L121
.L86:
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	fflush
	movl	%eax, -10020(%rbp)
	cmpl	$0, -10020(%rbp)
	jne	.L122
.L87:
	movq	stdout(%rip), %rax
	cmpq	%rax, -10096(%rbp)
	je	.L88
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	movl	%eax, -10020(%rbp)
	movq	$0, outputHandleJustInCase(%rip)
	cmpl	$-1, -10020(%rbp)
	je	.L123
.L88:
	movq	$0, outputHandleJustInCase(%rip)
	movl	verbosity(%rip), %eax
	cmpl	$1, %eax
	jle	.L89
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC10, %eax
	movq	%rdx, %rcx
	movl	$5, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L89:
	movl	$1, %eax
	jmp	.L90
.L114:
	nop
.L110:
.L71:
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	je	.L68
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	rewind
	jmp	.L96
.L126:
	nop
.L96:
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	myfeof
	testb	%al, %al
	jne	.L124
.L91:
	leaq	-10016(%rbp), %rax
	movq	-10088(%rbp), %rdx
	movq	%rdx, %rcx
	movl	$5000, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fread
	movl	%eax, -10028(%rbp)
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L125
.L93:
	cmpl	$0, -10028(%rbp)
	jle	.L94
	movl	-10028(%rbp), %eax
	movslq	%eax, %rdx
	leaq	-10016(%rbp), %rax
	movq	-10096(%rbp), %rcx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L94:
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L126
	jmp	.L66
.L124:
	nop
.L111:
	jmp	.L81
.L116:
	nop
.L68:
	movq	-10064(%rbp), %rdx
	leaq	-10044(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	BZ2_bzReadClose
	movl	-10048(%rbp), %eax
	addl	$9, %eax
	cmpl	$6, %eax
	ja	.L97
	mov	%eax, %eax
	movq	.L103(,%rax,8), %rax
	jmp	*%rax
	.section	.rodata
	.align 8
	.align 4
.L103:
	.quad	.L98
	.quad	.L97
	.quad	.L99
	.quad	.L66
	.quad	.L100
	.quad	.L101
	.quad	.L102
	.text
.L98:
	call	configError
	jmp	.L104
.L112:
	nop
	jmp	.L66
.L113:
	nop
	jmp	.L66
.L115:
	nop
	jmp	.L66
.L118:
	nop
	jmp	.L66
.L119:
	nop
	jmp	.L66
.L120:
	nop
	jmp	.L66
.L121:
	nop
	jmp	.L66
.L122:
	nop
	jmp	.L66
.L123:
	nop
	jmp	.L66
.L125:
	nop
.L66:
	call	ioError
	jmp	.L104
.L101:
	call	crcError
.L102:
	call	outOfMemory
.L99:
	call	compressedStreamEOF
.L100:
	movq	stdin(%rip), %rax
	cmpq	%rax, -10088(%rbp)
	je	.L105
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L105:
	movq	stdout(%rip), %rax
	cmpq	%rax, -10096(%rbp)
	je	.L106
	movq	-10096(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L106:
	cmpl	$1, -10036(%rbp)
	jne	.L107
	movl	$0, %eax
	jmp	.L90
.L107:
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L108
	movq	progName(%rip), %rdx
	movl	$.LC11, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L108:
	movl	$1, %eax
	jmp	.L90
.L97:
	movl	$.LC12, %edi
	call	panic
.L104:
	movl	$.LC13, %edi
	call	panic
	movl	$1, %eax
.L90:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L109
	call	__stack_chk_fail
.L109:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	uncompressStream, .-uncompressStream
	.section	.rodata
.LC14:
	.string	"test:bzReadGetUnused"
.LC15:
	.string	"%s: %s: "
	.align 8
.LC16:
	.string	"data integrity (CRC) error in data\n"
.LC17:
	.string	"file ends unexpectedly\n"
	.align 8
.LC18:
	.string	"bad magic number (file not created by bzip2)\n"
	.align 8
.LC19:
	.string	"trailing garbage after EOF ignored\n"
.LC20:
	.string	"test:unexpected error"
.LC21:
	.string	"test:end"
	.text
	.type	testStream, @function
testStream:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$10096, %rsp
	movq	%rdi, -10088(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$0, -10064(%rbp)
	movl	$0, -10036(%rbp)
	movl	$0, -10032(%rbp)
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L157
	jmp	.L128
.L160:
	nop
.L128:
	movl	-10036(%rbp), %r8d
	movzbl	smallMode(%rip), %eax
	movzbl	%al, %ecx
	movl	verbosity(%rip), %edx
	leaq	-5008(%rbp), %rdi
	movq	-10088(%rbp), %rsi
	leaq	-10044(%rbp), %rax
	movl	%r8d, %r9d
	movq	%rdi, %r8
	movq	%rax, %rdi
	call	BZ2_bzReadOpen
	movq	%rax, -10064(%rbp)
	cmpq	$0, -10064(%rbp)
	je	.L130
	movl	-10044(%rbp), %eax
	testl	%eax, %eax
	jne	.L130
	addl	$1, -10032(%rbp)
	jmp	.L131
.L132:
	leaq	-10016(%rbp), %rdx
	movq	-10064(%rbp), %rsi
	leaq	-10044(%rbp), %rax
	movl	$5000, %ecx
	movq	%rax, %rdi
	call	BZ2_bzRead
	movl	%eax, -10024(%rbp)
	movl	-10044(%rbp), %eax
	cmpl	$-5, %eax
	je	.L158
.L131:
	movl	-10044(%rbp), %eax
	testl	%eax, %eax
	je	.L132
	movl	-10044(%rbp), %eax
	cmpl	$4, %eax
	jne	.L159
.L133:
	leaq	-10036(%rbp), %rcx
	leaq	-10072(%rbp), %rdx
	movq	-10064(%rbp), %rsi
	leaq	-10044(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzReadGetUnused
	movl	-10044(%rbp), %eax
	testl	%eax, %eax
	je	.L134
	movl	$.LC14, %edi
	call	panic
.L134:
	movq	-10072(%rbp), %rax
	movq	%rax, -10056(%rbp)
	movl	$0, -10028(%rbp)
	jmp	.L135
.L136:
	movl	-10028(%rbp), %eax
	cltq
	addq	-10056(%rbp), %rax
	movzbl	(%rax), %edx
	movl	-10028(%rbp), %eax
	cltq
	movb	%dl, -5008(%rbp,%rax)
	addl	$1, -10028(%rbp)
.L135:
	movl	-10036(%rbp), %eax
	cmpl	%eax, -10028(%rbp)
	jl	.L136
	movq	-10064(%rbp), %rdx
	leaq	-10044(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	BZ2_bzReadClose
	movl	-10044(%rbp), %eax
	testl	%eax, %eax
	je	.L137
	movl	$.LC14, %edi
	call	panic
.L137:
	movl	-10036(%rbp), %eax
	testl	%eax, %eax
	jne	.L160
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	myfeof
	testb	%al, %al
	je	.L160
	nop
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L161
.L140:
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	movl	%eax, -10020(%rbp)
	cmpl	$-1, -10020(%rbp)
	je	.L162
.L141:
	movl	verbosity(%rip), %eax
	cmpl	$1, %eax
	jle	.L142
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC10, %eax
	movq	%rdx, %rcx
	movl	$5, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L142:
	movl	$1, %eax
	jmp	.L143
.L158:
	nop
	jmp	.L130
.L159:
	nop
.L130:
	movq	-10064(%rbp), %rdx
	leaq	-10040(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	BZ2_bzReadClose
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jne	.L144
	movq	progName(%rip), %rdx
	movl	$.LC15, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L144:
	movl	-10044(%rbp), %eax
	addl	$9, %eax
	cmpl	$6, %eax
	ja	.L145
	mov	%eax, %eax
	movq	.L151(,%rax,8), %rax
	jmp	*%rax
	.section	.rodata
	.align 8
	.align 4
.L151:
	.quad	.L146
	.quad	.L145
	.quad	.L147
	.quad	.L129
	.quad	.L148
	.quad	.L149
	.quad	.L150
	.text
.L146:
	call	configError
	jmp	.L152
.L157:
	nop
	jmp	.L129
.L161:
	nop
	jmp	.L129
.L162:
	nop
.L129:
	call	ioError
	jmp	.L152
.L149:
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC16, %eax
	movq	%rdx, %rcx
	movl	$35, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	$0, %eax
	jmp	.L143
.L150:
	call	outOfMemory
.L147:
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC17, %eax
	movq	%rdx, %rcx
	movl	$23, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	$0, %eax
	jmp	.L143
.L148:
	movq	stdin(%rip), %rax
	cmpq	%rax, -10088(%rbp)
	je	.L153
	movq	-10088(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L153:
	cmpl	$1, -10032(%rbp)
	jne	.L154
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC18, %eax
	movq	%rdx, %rcx
	movl	$45, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	$0, %eax
	jmp	.L143
.L154:
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L155
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC19, %eax
	movq	%rdx, %rcx
	movl	$35, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L155:
	movl	$1, %eax
	jmp	.L143
.L145:
	movl	$.LC20, %edi
	call	panic
.L152:
	movl	$.LC21, %edi
	call	panic
	movl	$1, %eax
.L143:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L156
	call	__stack_chk_fail
.L156:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	testStream, .-testStream
	.type	setExit, @function
setExit:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	movl	exitValue(%rip), %eax
	cmpl	%eax, -4(%rbp)
	jle	.L163
	movl	-4(%rbp), %eax
	movl	%eax, exitValue(%rip)
.L163:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	setExit, .-setExit
	.section	.rodata
	.align 8
.LC22:
	.string	"\nIt is possible that the compressed file(s) have become corrupted.\nYou can use the -tvv option to test integrity of such files.\n\nYou can use the `bzip2recover' program to attempt to recover\ndata from undamaged sections of corrupted files.\n\n"
	.text
	.type	cadvise, @function
cadvise:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L165
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC22, %eax
	movq	%rdx, %rcx
	movl	$240, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L165:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	cadvise, .-cadvise
	.section	.rodata
	.align 8
.LC23:
	.string	"\tInput file = %s, output file = %s\n"
	.text
	.type	showFileNames, @function
showFileNames:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L167
	movl	$.LC23, %esi
	movq	stderr(%rip), %rax
	movl	$outName, %ecx
	movl	$inName, %edx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L167:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	showFileNames, .-showFileNames
	.section	.rodata
	.align 8
.LC24:
	.string	"%s: Deleting output file %s, if it exists.\n"
	.align 8
.LC25:
	.string	"%s: WARNING: deletion of output file (apparently) failed.\n"
	.align 8
.LC26:
	.string	"%s: WARNING: deletion of output file suppressed\n"
	.align 8
.LC27:
	.string	"%s:    since input file no longer exists.  Output file\n"
	.align 8
.LC28:
	.string	"%s:    `%s' may be incomplete.\n"
	.align 8
.LC29:
	.string	"%s:    I suggest doing an integrity test (bzip2 -tv) of it.\n"
	.align 8
.LC30:
	.string	"%s: WARNING: some files have not been processed:\n%s:    %d specified on command line, %d not processed yet.\n\n"
	.text
	.type	cleanUpAndFail, @function
cleanUpAndFail:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$176, %rsp
	movl	%edi, -164(%rbp)
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L170
	movl	opMode(%rip), %eax
	cmpl	$3, %eax
	je	.L170
	movzbl	deleteOutputOnInterrupt(%rip), %eax
	testb	%al, %al
	je	.L170
	movl	$inName, %eax
	leaq	-160(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	jne	.L171
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L172
	movq	progName(%rip), %rdx
	movl	$.LC24, %esi
	movq	stderr(%rip), %rax
	movl	$outName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L172:
	movq	outputHandleJustInCase(%rip), %rax
	testq	%rax, %rax
	je	.L173
	movq	outputHandleJustInCase(%rip), %rax
	movq	%rax, %rdi
	call	fclose
.L173:
	movl	$outName, %edi
	call	remove
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L170
	movq	progName(%rip), %rdx
	movl	$.LC25, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L170
.L171:
	movq	progName(%rip), %rdx
	movl	$.LC26, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rdx
	movl	$.LC27, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rdx
	movl	$.LC28, %esi
	movq	stderr(%rip), %rax
	movl	$outName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rdx
	movl	$.LC29, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L170:
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L174
	movl	numFileNames(%rip), %eax
	testl	%eax, %eax
	jle	.L174
	movl	numFilesProcessed(%rip), %edx
	movl	numFileNames(%rip), %eax
	cmpl	%eax, %edx
	jge	.L174
	movl	numFileNames(%rip), %edx
	movl	numFilesProcessed(%rip), %eax
	movl	%edx, %r8d
	subl	%eax, %r8d
	movl	numFileNames(%rip), %edi
	movq	progName(%rip), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC30, %esi
	movq	stderr(%rip), %rax
	movl	%r8d, %r9d
	movl	%edi, %r8d
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L174:
	movl	-164(%rbp), %eax
	movl	%eax, %edi
	call	setExit
	movl	exitValue(%rip), %eax
	movl	%eax, %edi
	call	exit
	.cfi_endproc
.LFE12:
	.size	cleanUpAndFail, .-cleanUpAndFail
	.section	.rodata
	.align 8
.LC31:
	.string	"\n%s: PANIC -- internal consistency error:\n\t%s\n\tThis is a BUG.  Please report it to me at:\n\tjseward@bzip.org\n"
	.text
	.type	panic, @function
panic:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	progName(%rip), %rdx
	movl	$.LC31, %esi
	movq	stderr(%rip), %rax
	movq	-8(%rbp), %rcx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	showFileNames
	movl	$3, %edi
	call	cleanUpAndFail
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	panic, .-panic
	.section	.rodata
	.align 8
.LC32:
	.string	"\n%s: Data integrity error when decompressing.\n"
	.text
	.type	crcError, @function
crcError:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progName(%rip), %rdx
	movl	$.LC32, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	showFileNames
	call	cadvise
	movl	$2, %edi
	call	cleanUpAndFail
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	crcError, .-crcError
	.section	.rodata
	.align 8
.LC33:
	.string	"\n%s: Compressed file ends unexpectedly;\n\tperhaps it is corrupted?  *Possible* reason follows.\n"
	.text
	.type	compressedStreamEOF, @function
compressedStreamEOF:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L178
	movq	progName(%rip), %rdx
	movl	$.LC33, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rax
	movq	%rax, %rdi
	call	perror
	call	showFileNames
	call	cadvise
.L178:
	movl	$2, %edi
	call	cleanUpAndFail
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	compressedStreamEOF, .-compressedStreamEOF
	.section	.rodata
	.align 8
.LC34:
	.string	"\n%s: I/O or other error, bailing out.  Possible reason follows.\n"
	.text
	.type	ioError, @function
ioError:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progName(%rip), %rdx
	movl	$.LC34, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rax
	movq	%rax, %rdi
	call	perror
	call	showFileNames
	movl	$1, %edi
	call	cleanUpAndFail
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	ioError, .-ioError
	.section	.rodata
	.align 8
.LC35:
	.string	"\n%s: Control-C or similar caught, quitting.\n"
	.text
	.type	mySignalCatcher, @function
mySignalCatcher:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movq	progName(%rip), %rdx
	movl	$.LC35, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	cleanUpAndFail
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	mySignalCatcher, .-mySignalCatcher
	.section	.rodata
	.align 8
.LC36:
	.ascii	"\n%s: Caught a SIGSEGV or SIGBUS whilst compressing.\n\n   P"
	.ascii	"ossible causes are (most likely first):\n   (1) This compute"
	.ascii	"r has unreliable memory or cache hardware\n       (a surpris"
	.ascii	"ingly common problem; try a different machine.)\n   (2) A bu"
	.ascii	"g in the compiler used to create this executable\n       (un"
	.ascii	"likely, if you didn't compile bzip2 yourself.)\n   (3) A rea"
	.ascii	"l bug in bzip2 -- I hope this should never be the case.\n   "
	.ascii	"The user's manual, Section 4.3, has more info on (1) and (2)"
	.ascii	".\n   \n   If you suspect this is a bug in bzip2, or are uns"
	.ascii	"ure about (1)\n   or (2), feel free to report it to me at: j"
	.ascii	"seward@bzip.org.\n   Sect"
	.string	"ion 4.3 of the user's manual describes the info a useful\n   bug report should have.  If the manual is available on your\n   system, please try and read it before mailing me.  If you don't\n   have the manual or can't be bothered to read it, mail me anyway.\n\n"
	.align 8
.LC37:
	.ascii	"\n%s: Caught a SIGSEGV or SIGBUS whilst decompressing.\n\n  "
	.ascii	" Possible causes are (most likely first):\n   (1) The compre"
	.ascii	"ssed data is corrupted, and bzip2's usual checks\n       fai"
	.ascii	"led to detect this.  Try bzip2 -tvv my_file.bz2.\n   (2) Thi"
	.ascii	"s computer has unreliable memory or cache hardware\n       ("
	.ascii	"a surprisingly common problem; try a different machine.)\n  "
	.ascii	" (3) A bug in the compiler used to create this executable\n "
	.ascii	"      (unlikely, if you didn't compile bzip2 yourself.)\n   "
	.ascii	"(4) A real bug in bzip2 -- I hope this should never be the c"
	.ascii	"ase.\n   The user's manual, Section 4.3, has more info on (2"
	.ascii	") and (3).\n   \n   If you suspect this is a bug in bzip2, o"
	.ascii	"r are unsure about (2)\n   or (3), feel free to report it to"
	.ascii	" me at: jseward@bzip.org.\n   Sect"
	.string	"ion 4.3 of the user's manual describes the info a useful\n   bug report should have.  If the manual is available on your\n   system, please try and read it before mailing me.  If you don't\n   have the manual or can't be bothered to read it, mail me anyway.\n\n"
	.text
	.type	mySIGSEGVorSIGBUScatcher, @function
mySIGSEGVorSIGBUScatcher:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	opMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L182
	movq	progName(%rip), %rdx
	movl	$.LC36, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L183
.L182:
	movq	progName(%rip), %rdx
	movl	$.LC37, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L183:
	call	showFileNames
	movl	opMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L184
	movl	$3, %edi
	call	cleanUpAndFail
	jmp	.L181
.L184:
	call	cadvise
	movl	$2, %edi
	call	cleanUpAndFail
.L181:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	mySIGSEGVorSIGBUScatcher, .-mySIGSEGVorSIGBUScatcher
	.section	.rodata
	.align 8
.LC38:
	.string	"\n%s: couldn't allocate enough memory\n"
	.text
	.type	outOfMemory, @function
outOfMemory:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progName(%rip), %rdx
	movl	$.LC38, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	showFileNames
	movl	$1, %edi
	call	cleanUpAndFail
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
	.size	outOfMemory, .-outOfMemory
	.section	.rodata
	.align 8
.LC39:
	.string	"bzip2: I'm not configured correctly for this platform!\n\tI require Int32, Int16 and Char to have sizes\n\tof 4, 2 and 1 bytes to run properly, and they don't.\n\tProbably you can fix this by defining them correctly,\n\tand recompiling.  Bye!\n"
	.text
	.type	configError, @function
configError:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC39, %eax
	movq	%rdx, %rcx
	movl	$235, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	$3, %edi
	call	setExit
	movl	exitValue(%rip), %eax
	movl	%eax, %edi
	call	exit
	.cfi_endproc
.LFE20:
	.size	configError, .-configError
	.type	pad, @function
pad:
.LFB21:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	$-1, -32(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-32(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, %edx
	movl	longestFileName(%rip), %eax
	cmpl	%eax, %edx
	jge	.L193
.L189:
	movl	$1, -4(%rbp)
	jmp	.L191
.L192:
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$32, %edi
	call	fputc
	addl	$1, -4(%rbp)
.L191:
	movl	longestFileName(%rip), %esi
	movq	-24(%rbp), %rax
	movq	$-1, -32(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-32(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%esi, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	cmpl	-4(%rbp), %eax
	jge	.L192
	jmp	.L188
.L193:
	nop
.L188:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21:
	.size	pad, .-pad
	.section	.rodata
	.align 8
.LC40:
	.string	"bzip2: file name\n`%s'\nis suspiciously (more than %d chars) long.\nTry using a reasonable file name instead.  Sorry! :-)\n"
	.text
	.type	copyFileName, @function
copyFileName:
.LFB22:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	$-1, -24(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-24(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cmpq	$1024, %rax
	jbe	.L195
	movl	$.LC40, %esi
	movq	stderr(%rip), %rax
	movq	-16(%rbp), %rdx
	movl	$1024, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	movl	exitValue(%rip), %eax
	movl	%eax, %edi
	call	exit
.L195:
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$1024, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	strncpy
	movq	-8(%rbp), %rax
	addq	$1024, %rax
	movb	$0, (%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	copyFileName, .-copyFileName
	.section	.rodata
.LC41:
	.string	"rb"
	.text
	.type	fileExists, @function
fileExists:
.LFB23:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	$.LC41, %edx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	setne	%al
	movb	%al, -1(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L197
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L197:
	movzbl	-1(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
	.size	fileExists, .-fileExists
	.type	fopen_output_safely, @function
fopen_output_safely:
.LFB24:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movl	$384, %edx
	movl	$193, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	open
	movl	%eax, -4(%rbp)
	cmpl	$-1, -4(%rbp)
	jne	.L199
	movl	$0, %eax
	jmp	.L200
.L199:
	movq	-32(%rbp), %rdx
	movl	-4(%rbp), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	fdopen
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L201
	movl	-4(%rbp), %eax
	movl	%eax, %edi
	call	close
.L201:
	movq	-16(%rbp), %rax
.L200:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24:
	.size	fopen_output_safely, .-fopen_output_safely
	.type	notAStandardFile, @function
notAStandardFile:
.LFB25:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$176, %rsp
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rax
	leaq	-160(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	lstat
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L203
	movl	$1, %eax
	jmp	.L204
.L203:
	movl	-136(%rbp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	jne	.L205
	movl	$0, %eax
	jmp	.L204
.L205:
	movl	$1, %eax
.L204:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25:
	.size	notAStandardFile, .-notAStandardFile
	.type	countHardLinks, @function
countHardLinks:
.LFB26:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$176, %rsp
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rax
	leaq	-160(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	lstat
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L207
	movl	$0, %eax
	jmp	.L208
.L207:
	movq	-144(%rbp), %rax
	subl	$1, %eax
.L208:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
	.size	countHardLinks, .-countHardLinks
	.local	fileMetaInfo
	.comm	fileMetaInfo,144,32
	.type	saveInputFileMetaInfo, @function
saveInputFileMetaInfo:
.LFB27:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	$fileMetaInfo, %esi
	movq	%rax, %rdi
	call	stat
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L209
	call	ioError
.L209:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27:
	.size	saveInputFileMetaInfo, .-saveInputFileMetaInfo
	.type	applySavedTimeInfoToOutputFile, @function
applySavedTimeInfoToOutputFile:
.LFB28:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	fileMetaInfo+72(%rip), %rax
	movq	%rax, -32(%rbp)
	movq	fileMetaInfo+88(%rip), %rax
	movq	%rax, -24(%rbp)
	leaq	-32(%rbp), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	utime
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L211
	call	ioError
.L211:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28:
	.size	applySavedTimeInfoToOutputFile, .-applySavedTimeInfoToOutputFile
	.type	applySavedFileAttrToOutputFile, @function
applySavedFileAttrToOutputFile:
.LFB29:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	fileMetaInfo+24(%rip), %edx
	movl	-20(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	fchmod
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L214
	call	ioError
.L214:
	movl	fileMetaInfo+32(%rip), %edx
	movl	fileMetaInfo+28(%rip), %ecx
	movl	-20(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	fchown
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29:
	.size	applySavedFileAttrToOutputFile, .-applySavedFileAttrToOutputFile
	.type	containsDubiousChars, @function
containsDubiousChars:
.LFB30:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30:
	.size	containsDubiousChars, .-containsDubiousChars
	.globl	zSuffix
	.section	.rodata
.LC42:
	.string	".bz2"
.LC43:
	.string	".bz"
.LC44:
	.string	".tbz2"
.LC45:
	.string	".tbz"
	.data
	.align 32
	.type	zSuffix, @object
	.size	zSuffix, 32
zSuffix:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.globl	unzSuffix
	.section	.rodata
.LC46:
	.string	""
.LC47:
	.string	".tar"
	.data
	.align 32
	.type	unzSuffix, @object
	.size	unzSuffix, 32
unzSuffix:
	.quad	.LC46
	.quad	.LC46
	.quad	.LC47
	.quad	.LC47
	.text
	.type	hasSuffix, @function
hasSuffix:
.LFB31:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	$-1, -40(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-40(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -8(%rbp)
	movq	-32(%rbp), %rax
	movq	$-1, -40(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-40(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -4(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jge	.L217
	movl	$0, %eax
	jmp	.L218
.L217:
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movl	-4(%rbp), %eax
	cltq
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	addq	-24(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L219
	movl	$1, %eax
	jmp	.L218
.L219:
	movl	$0, %eax
.L218:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31:
	.size	hasSuffix, .-hasSuffix
	.type	mapSuffix, @function
mapSuffix:
.LFB32:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	hasSuffix
	testb	%al, %al
	jne	.L221
	movl	$0, %eax
	jmp	.L222
.L221:
	movq	-8(%rbp), %rax
	movq	$-1, -32(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-32(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rsi
	movq	-16(%rbp), %rax
	movq	$-1, -32(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-32(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movq	%rsi, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	-8(%rbp), %rax
	movb	$0, (%rax)
	movq	-24(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcat
	movl	$1, %eax
.L222:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32:
	.size	mapSuffix, .-mapSuffix
	.section	.rodata
.LC48:
	.string	"compress: bad modes\n"
.LC49:
	.string	"(stdin)"
.LC50:
	.string	"(stdout)"
	.align 8
.LC51:
	.string	"%s: There are no files matching `%s'.\n"
	.align 8
.LC52:
	.string	"%s: Can't open input file %s: %s.\n"
	.align 8
.LC53:
	.string	"%s: Input file %s already has %s suffix.\n"
	.align 8
.LC54:
	.string	"%s: Input file %s is a directory.\n"
	.align 8
.LC55:
	.string	"%s: Input file %s is not a normal file.\n"
	.align 8
.LC56:
	.string	"%s: Output file %s already exists.\n"
.LC57:
	.string	"s"
	.align 8
.LC58:
	.string	"%s: Input file %s has %d other link%s.\n"
	.align 8
.LC59:
	.string	"%s: I won't write compressed data to a terminal.\n"
	.align 8
.LC60:
	.string	"%s: For help, type: `%s --help'.\n"
.LC61:
	.string	"wb"
	.align 8
.LC62:
	.string	"%s: Can't create output file %s: %s.\n"
.LC63:
	.string	"compress: bad srcMode"
.LC64:
	.string	"  %s: "
	.text
	.type	compress, @function
compress:
.LFB33:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movq	%rdi, -184(%rbp)
	movb	$0, deleteOutputOnInterrupt(%rip)
	cmpq	$0, -184(%rbp)
	jne	.L224
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L224
	movl	$.LC48, %edi
	call	panic
.L224:
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	je	.L227
	cmpl	$3, %eax
	je	.L228
	cmpl	$1, %eax
	jne	.L225
.L226:
	movl	$.LC49, %esi
	movl	$inName, %edi
	call	copyFileName
	movl	$.LC50, %esi
	movl	$outName, %edi
	call	copyFileName
	jmp	.L225
.L228:
	movq	-184(%rbp), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	copyFileName
	movq	-184(%rbp), %rax
	movq	%rax, %rsi
	movl	$outName, %edi
	call	copyFileName
	movl	$.LC42, %edx
	movl	$outName, %r8d
	movl	$outName, %eax
	movq	$-1, -192(%rbp)
	movq	%rax, %rsi
	movl	$0, %eax
	movq	-192(%rbp), %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	addq	%r8, %rax
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movzbl	4(%rdx), %edx
	movb	%dl, 4(%rax)
	jmp	.L225
.L227:
	movq	-184(%rbp), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	copyFileName
	movl	$.LC50, %esi
	movl	$outName, %edi
	call	copyFileName
	nop
.L225:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L229
	movl	$inName, %edi
	call	containsDubiousChars
	testb	%al, %al
	je	.L229
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L230
	movq	progName(%rip), %rdx
	movl	$.LC51, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L230:
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L229:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L232
	movl	$inName, %edi
	call	fileExists
	testb	%al, %al
	jne	.L232
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC52, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L232:
	movl	$0, -12(%rbp)
	jmp	.L233
.L236:
	movl	-12(%rbp), %eax
	cltq
	movq	zSuffix(,%rax,8), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	hasSuffix
	testb	%al, %al
	je	.L234
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L235
	movl	-12(%rbp), %eax
	cltq
	movq	zSuffix(,%rax,8), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC53, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L235:
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L234:
	addl	$1, -12(%rbp)
.L233:
	cmpl	$3, -12(%rbp)
	jle	.L236
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	je	.L237
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	jne	.L238
.L237:
	movl	$inName, %eax
	leaq	-176(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	movl	-152(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L238
	movq	progName(%rip), %rdx
	movl	$.LC54, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L238:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L239
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	jne	.L239
	movl	$inName, %edi
	call	notAStandardFile
	testb	%al, %al
	je	.L239
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L240
	movq	progName(%rip), %rdx
	movl	$.LC55, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L240:
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L239:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L241
	movl	$outName, %edi
	call	fileExists
	testb	%al, %al
	je	.L241
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	je	.L242
	movl	$outName, %edi
	call	remove
	jmp	.L241
.L242:
	movq	progName(%rip), %rdx
	movl	$.LC56, %esi
	movq	stderr(%rip), %rax
	movl	$outName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L241:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L243
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	jne	.L243
	movl	$inName, %edi
	call	countHardLinks
	movl	%eax, -8(%rbp)
	cmpl	$0, -8(%rbp)
	jle	.L243
	cmpl	$1, -8(%rbp)
	jle	.L244
	movl	$.LC57, %eax
	jmp	.L245
.L244:
	movl	$.LC46, %eax
.L245:
	movq	progName(%rip), %rdx
	movl	$.LC58, %esi
	movq	stderr(%rip), %rcx
	movq	%rcx, %rdi
	movl	-8(%rbp), %ecx
	movq	%rax, %r9
	movl	%ecx, %r8d
	movl	$inName, %ecx
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L243:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L246
	movl	$inName, %edi
	call	saveInputFileMetaInfo
.L246:
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	je	.L249
	cmpl	$3, %eax
	je	.L250
	cmpl	$1, %eax
	jne	.L262
.L248:
	movq	stdin(%rip), %rax
	movq	%rax, -32(%rbp)
	movq	stdout(%rip), %rax
	movq	%rax, -24(%rbp)
	movq	stdout(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %edi
	call	isatty
	testl	%eax, %eax
	je	.L263
	movq	progName(%rip), %rdx
	movl	$.LC59, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC60, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L249:
	movl	$.LC41, %edx
	movl	$inName, %eax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -32(%rbp)
	movq	stdout(%rip), %rax
	movq	%rax, -24(%rbp)
	movq	stdout(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %edi
	call	isatty
	testl	%eax, %eax
	je	.L253
	movq	progName(%rip), %rdx
	movl	$.LC59, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC60, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpq	$0, -32(%rbp)
	je	.L254
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L254:
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L253:
	cmpq	$0, -32(%rbp)
	jne	.L264
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC52, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L250:
	movl	$.LC41, %edx
	movl	$inName, %eax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -32(%rbp)
	movl	$.LC61, %esi
	movl	$outName, %edi
	call	fopen_output_safely
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L256
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC62, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$outName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpq	$0, -32(%rbp)
	je	.L257
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L257:
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L256:
	cmpq	$0, -32(%rbp)
	jne	.L265
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC52, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpq	$0, -24(%rbp)
	je	.L259
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L259:
	movl	$1, %edi
	call	setExit
	jmp	.L223
.L262:
	movl	$.LC63, %edi
	call	panic
	jmp	.L252
.L263:
	nop
	jmp	.L252
.L264:
	nop
	jmp	.L252
.L265:
	nop
.L252:
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L260
	movl	$.LC64, %ecx
	movq	stderr(%rip), %rax
	movl	$inName, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$inName, %edi
	call	pad
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	call	fflush
.L260:
	movq	-24(%rbp), %rax
	movq	%rax, outputHandleJustInCase(%rip)
	movb	$1, deleteOutputOnInterrupt(%rip)
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	compressStream
	movq	$0, outputHandleJustInCase(%rip)
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L261
	movl	$outName, %edi
	call	applySavedTimeInfoToOutputFile
	movb	$0, deleteOutputOnInterrupt(%rip)
	movzbl	keepInputFiles(%rip), %eax
	testb	%al, %al
	jne	.L261
	movl	$inName, %edi
	call	remove
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L261
	call	ioError
.L261:
	movb	$0, deleteOutputOnInterrupt(%rip)
.L223:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
	.size	compress, .-compress
	.section	.rodata
.LC65:
	.string	"uncompress: bad modes\n"
.LC66:
	.string	".out"
	.align 8
.LC67:
	.string	"%s: Can't guess original name for %s -- using %s\n"
	.align 8
.LC68:
	.string	"%s: I won't read compressed data from a terminal.\n"
	.align 8
.LC69:
	.string	"%s: Can't open input file %s:%s.\n"
.LC70:
	.string	"uncompress: bad srcMode"
.LC71:
	.string	"done\n"
.LC72:
	.string	"not a bzip2 file.\n"
.LC73:
	.string	"%s: %s is not a bzip2 file.\n"
	.text
	.type	uncompress, @function
uncompress:
.LFB34:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdi, -200(%rbp)
	movb	$0, deleteOutputOnInterrupt(%rip)
	cmpq	$0, -200(%rbp)
	jne	.L267
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L267
	movl	$.LC65, %edi
	call	panic
.L267:
	movb	$0, -2(%rbp)
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	je	.L270
	cmpl	$3, %eax
	je	.L271
	cmpl	$1, %eax
	jne	.L268
.L269:
	movl	$.LC49, %esi
	movl	$inName, %edi
	call	copyFileName
	movl	$.LC50, %esi
	movl	$outName, %edi
	call	copyFileName
	jmp	.L268
.L271:
	movq	-200(%rbp), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	copyFileName
	movq	-200(%rbp), %rax
	movq	%rax, %rsi
	movl	$outName, %edi
	call	copyFileName
	movl	$0, -20(%rbp)
	jmp	.L272
.L274:
	movl	-20(%rbp), %eax
	cltq
	movq	unzSuffix(,%rax,8), %rdx
	movl	-20(%rbp), %eax
	cltq
	movq	zSuffix(,%rax,8), %rax
	movq	%rax, %rsi
	movl	$outName, %edi
	call	mapSuffix
	testb	%al, %al
	jne	.L308
.L273:
	addl	$1, -20(%rbp)
.L272:
	cmpl	$3, -20(%rbp)
	jle	.L274
	movb	$1, -2(%rbp)
	movl	$.LC66, %edx
	movl	$outName, %r8d
	movl	$outName, %eax
	movq	$-1, -208(%rbp)
	movq	%rax, %rsi
	movl	$0, %eax
	movq	-208(%rbp), %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	addq	%r8, %rax
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movzbl	4(%rdx), %edx
	movb	%dl, 4(%rax)
	jmp	.L268
.L270:
	movq	-200(%rbp), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	copyFileName
	movl	$.LC50, %esi
	movl	$outName, %edi
	call	copyFileName
	jmp	.L268
.L308:
	nop
.L268:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L275
	movl	$inName, %edi
	call	containsDubiousChars
	testb	%al, %al
	je	.L275
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L276
	movq	progName(%rip), %rdx
	movl	$.LC51, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L276:
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L275:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L278
	movl	$inName, %edi
	call	fileExists
	testb	%al, %al
	jne	.L278
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC52, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L278:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	je	.L279
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	jne	.L280
.L279:
	movl	$inName, %eax
	leaq	-192(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	movl	-168(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L280
	movq	progName(%rip), %rdx
	movl	$.LC54, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L280:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L281
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	jne	.L281
	movl	$inName, %edi
	call	notAStandardFile
	testb	%al, %al
	je	.L281
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L282
	movq	progName(%rip), %rdx
	movl	$.LC55, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L282:
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L281:
	cmpb	$0, -2(%rbp)
	je	.L283
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L283
	movq	progName(%rip), %rdx
	movl	$.LC67, %esi
	movq	stderr(%rip), %rax
	movl	$outName, %r8d
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L283:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L284
	movl	$outName, %edi
	call	fileExists
	testb	%al, %al
	je	.L284
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	je	.L285
	movl	$outName, %edi
	call	remove
	jmp	.L284
.L285:
	movq	progName(%rip), %rdx
	movl	$.LC56, %esi
	movq	stderr(%rip), %rax
	movl	$outName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L284:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L286
	movzbl	forceOverwrite(%rip), %eax
	testb	%al, %al
	jne	.L286
	movl	$inName, %edi
	call	countHardLinks
	movl	%eax, -16(%rbp)
	cmpl	$0, -16(%rbp)
	jle	.L286
	cmpl	$1, -16(%rbp)
	jle	.L287
	movl	$.LC57, %eax
	jmp	.L288
.L287:
	movl	$.LC46, %eax
.L288:
	movq	progName(%rip), %rdx
	movl	$.LC58, %esi
	movq	stderr(%rip), %rcx
	movq	%rcx, %rdi
	movl	-16(%rbp), %ecx
	movq	%rax, %r9
	movl	%ecx, %r8d
	movl	$inName, %ecx
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L286:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L289
	movl	$inName, %edi
	call	saveInputFileMetaInfo
.L289:
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	je	.L292
	cmpl	$3, %eax
	je	.L293
	cmpl	$1, %eax
	jne	.L307
.L291:
	movq	stdin(%rip), %rax
	movq	%rax, -40(%rbp)
	movq	stdout(%rip), %rax
	movq	%rax, -32(%rbp)
	movq	stdin(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %edi
	call	isatty
	testl	%eax, %eax
	je	.L309
	movq	progName(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC60, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L292:
	movl	$.LC41, %edx
	movl	$inName, %eax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -40(%rbp)
	movq	stdout(%rip), %rax
	movq	%rax, -32(%rbp)
	cmpq	$0, -40(%rbp)
	jne	.L310
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC69, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpq	$0, -40(%rbp)
	je	.L297
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L297:
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L293:
	movl	$.LC41, %edx
	movl	$inName, %eax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -40(%rbp)
	movl	$.LC61, %esi
	movl	$outName, %edi
	call	fopen_output_safely
	movq	%rax, -32(%rbp)
	cmpq	$0, -32(%rbp)
	jne	.L298
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC62, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$outName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpq	$0, -40(%rbp)
	je	.L299
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L299:
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L298:
	cmpq	$0, -40(%rbp)
	jne	.L311
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC52, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpq	$0, -32(%rbp)
	je	.L301
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L301:
	movl	$1, %edi
	call	setExit
	jmp	.L266
.L307:
	movl	$.LC70, %edi
	call	panic
	jmp	.L295
.L309:
	nop
	jmp	.L295
.L310:
	nop
	jmp	.L295
.L311:
	nop
.L295:
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L302
	movl	$.LC64, %ecx
	movq	stderr(%rip), %rax
	movl	$inName, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$inName, %edi
	call	pad
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	call	fflush
.L302:
	movq	-32(%rbp), %rax
	movq	%rax, outputHandleJustInCase(%rip)
	movb	$1, deleteOutputOnInterrupt(%rip)
	movq	-32(%rbp), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	uncompressStream
	movb	%al, -1(%rbp)
	movq	$0, outputHandleJustInCase(%rip)
	cmpb	$0, -1(%rbp)
	je	.L303
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L304
	movl	$outName, %edi
	call	applySavedTimeInfoToOutputFile
	movb	$0, deleteOutputOnInterrupt(%rip)
	movzbl	keepInputFiles(%rip), %eax
	testb	%al, %al
	jne	.L304
	movl	$inName, %edi
	call	remove
	movl	%eax, -12(%rbp)
	cmpl	$0, -12(%rbp)
	je	.L304
	call	ioError
	jmp	.L304
.L303:
	movb	$1, unzFailsExist(%rip)
	movb	$0, deleteOutputOnInterrupt(%rip)
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L304
	movl	$outName, %edi
	call	remove
	movl	%eax, -8(%rbp)
	cmpl	$0, -8(%rbp)
	je	.L304
	call	ioError
.L304:
	movb	$0, deleteOutputOnInterrupt(%rip)
	cmpb	$0, -1(%rbp)
	je	.L305
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L266
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC71, %eax
	movq	%rdx, %rcx
	movl	$5, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	jmp	.L266
.L305:
	movl	$2, %edi
	call	setExit
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L306
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC72, %eax
	movq	%rdx, %rcx
	movl	$18, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	jmp	.L266
.L306:
	movq	progName(%rip), %rdx
	movl	$.LC73, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L266:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34:
	.size	uncompress, .-uncompress
	.section	.rodata
.LC74:
	.string	"testf: bad modes\n"
.LC75:
	.string	"(none)"
.LC76:
	.string	"%s: Can't open input %s: %s.\n"
.LC77:
	.string	"testf: bad srcMode"
.LC78:
	.string	"ok\n"
	.text
	.type	testf, @function
testf:
.LFB35:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$176, %rsp
	movq	%rdi, -168(%rbp)
	movb	$0, deleteOutputOnInterrupt(%rip)
	cmpq	$0, -168(%rbp)
	jne	.L313
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L313
	movl	$.LC74, %edi
	call	panic
.L313:
	movl	$.LC75, %esi
	movl	$outName, %edi
	call	copyFileName
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	je	.L316
	cmpl	$3, %eax
	je	.L317
	cmpl	$1, %eax
	jne	.L314
.L315:
	movl	$.LC49, %esi
	movl	$inName, %edi
	call	copyFileName
	jmp	.L314
.L317:
	movq	-168(%rbp), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	copyFileName
	jmp	.L314
.L316:
	movq	-168(%rbp), %rax
	movq	%rax, %rsi
	movl	$inName, %edi
	call	copyFileName
	nop
.L314:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L318
	movl	$inName, %edi
	call	containsDubiousChars
	testb	%al, %al
	je	.L318
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L319
	movq	progName(%rip), %rdx
	movl	$.LC51, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L319:
	movl	$1, %edi
	call	setExit
	jmp	.L312
.L318:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L321
	movl	$inName, %edi
	call	fileExists
	testb	%al, %al
	jne	.L321
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC76, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L312
.L321:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L322
	movl	$inName, %eax
	leaq	-160(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	movl	-136(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L322
	movq	progName(%rip), %rdx
	movl	$.LC54, %esi
	movq	stderr(%rip), %rax
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L312
.L322:
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	je	.L324
	cmpl	$1, %eax
	jl	.L323
	cmpl	$3, %eax
	jg	.L323
	jmp	.L331
.L324:
	movq	stdin(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %edi
	call	isatty
	testl	%eax, %eax
	je	.L326
	movq	progName(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC60, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L312
.L326:
	movq	stdin(%rip), %rax
	movq	%rax, -16(%rbp)
	jmp	.L327
.L331:
	movl	$.LC41, %edx
	movl	$inName, %eax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L332
	call	__errno_location
	movl	(%rax), %eax
	movl	%eax, %edi
	call	strerror
	movq	%rax, %rcx
	movq	progName(%rip), %rdx
	movl	$.LC69, %esi
	movq	stderr(%rip), %rax
	movq	%rcx, %r8
	movl	$inName, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	setExit
	jmp	.L312
.L323:
	movl	$.LC77, %edi
	call	panic
	jmp	.L327
.L332:
	nop
.L327:
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L329
	movl	$.LC64, %ecx
	movq	stderr(%rip), %rax
	movl	$inName, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$inName, %edi
	call	pad
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	call	fflush
.L329:
	movq	$0, outputHandleJustInCase(%rip)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	testStream
	movb	%al, -1(%rbp)
	cmpb	$0, -1(%rbp)
	je	.L330
	movl	verbosity(%rip), %eax
	testl	%eax, %eax
	jle	.L330
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC78, %eax
	movq	%rdx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L330:
	cmpb	$0, -1(%rbp)
	jne	.L312
	movb	$1, testFailsExist(%rip)
.L312:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE35:
	.size	testf, .-testf
	.section	.rodata
	.align 8
.LC79:
	.ascii	"bzip2, a block-sorting file compressor.  Version %s.\n   \n "
	.ascii	"  Copyright (C) 1996-2010 by Julian Seward.\n   \n   This pr"
	.ascii	"ogram is free software; you can redistribute it and/or modif"
	.ascii	"y\n   it under the terms set out in the LICENSE file, which "
	.ascii	"is included\n   in the bzip2-1.0.6 sourc"
	.string	"e distribution.\n   \n   This program is distributed in the hope that it will be useful,\n   but WITHOUT ANY WARRANTY; without even the implied warranty of\n   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n   LICENSE file for more details.\n   \n"
	.text
	.type	license, @function
license:
.LFB36:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BZ2_bzlibVersion
	movq	%rax, %rdx
	movl	$.LC79, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE36:
	.size	license, .-license
	.section	.rodata
	.align 8
.LC80:
	.ascii	"bzip2, a block-sorting file compressor.  Version %s.\n\n   u"
	.ascii	"sage: %s [flags and input files in any order]\n\n   -h --hel"
	.ascii	"p           print this message\n   -d --decompress     force"
	.ascii	" decompression\n   -z --compress       force compression\n  "
	.ascii	" -k --keep           keep (don't delete) input files\n   -f "
	.ascii	"--force          overwrite existing output files\n   -t --te"
	.ascii	"st           test compressed file integrity\n   -c --stdout "
	.ascii	"        output to standard out\n   -q --quiet          suppr"
	.ascii	"ess noncritical error messages\n   -v --verbose        be ve"
	.ascii	"rbose (a 2nd -v gives more)\n   -L --license        display "
	.ascii	"software version & license\n   -V --version        display s"
	.ascii	"oftware version & license\n   -s --small          use less m"
	.ascii	"emory (at most 2500k)\n   -1 .. -9            set block size"
	.ascii	" to 100k .. 900k\n   --fast              alias for -1\n   --"
	.ascii	"best              alias for -9\n\n   If invoked as `bzip2', "
	.ascii	"default action is to compress.\n              as `bunzip2', "
	.ascii	" default action is to decompress.\n"
	.string	"              as `bzcat', default action is to decompress to stdout.\n\n   If no file names are given, bzip2 compresses or decompresses\n   from standard input to standard output.  You can combine\n   short flags, so `-v -4' means the same as -v4 or -4v, &c.\n\n"
	.text
	.type	usage, @function
usage:
.LFB37:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	BZ2_bzlibVersion
	movq	%rax, %rdx
	movl	$.LC80, %esi
	movq	stderr(%rip), %rax
	movq	-8(%rbp), %rcx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE37:
	.size	usage, .-usage
	.section	.rodata
	.align 8
.LC81:
	.string	"%s: %s is redundant in versions 0.9.5 and above\n"
	.text
	.type	redundant, @function
redundant:
.LFB38:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	progName(%rip), %rdx
	movl	$.LC81, %esi
	movq	stderr(%rip), %rax
	movq	-8(%rbp), %rcx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE38:
	.size	redundant, .-redundant
	.type	myMalloc, @function
myMalloc:
.LFB39:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	-20(%rbp), %eax
	cltq
	movq	%rax, %rdi
	call	malloc
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L337
	call	outOfMemory
.L337:
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE39:
	.size	myMalloc, .-myMalloc
	.type	mkCell, @function
mkCell:
.LFB40:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$16, %edi
	call	myMalloc
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE40:
	.size	mkCell, .-mkCell
	.type	snocString, @function
snocString:
.LFB41:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L340
	call	mkCell
	movq	%rax, -8(%rbp)
	movq	-32(%rbp), %rax
	movq	$-1, -40(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-40(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	addl	$5, %eax
	movl	%eax, %edi
	call	myMalloc
	movq	-8(%rbp), %rdx
	movq	%rax, (%rdx)
	movq	-32(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-8(%rbp), %rax
	jmp	.L341
.L340:
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	jmp	.L342
.L343:
	movq	-16(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -16(%rbp)
.L342:
	movq	-16(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L343
	movq	-16(%rbp), %rax
	movq	8(%rax), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	snocString
	movq	-16(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-24(%rbp), %rax
.L341:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE41:
	.size	snocString, .-snocString
	.type	addFlagsFromEnvVar, @function
addFlagsFromEnvVar:
.LFB42:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	getenv
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L344
	movq	-24(%rbp), %rax
	movq	%rax, -32(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L356
.L358:
	nop
.L356:
	movl	-12(%rbp), %eax
	cltq
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L357
.L346:
	movl	-12(%rbp), %eax
	cltq
	addq	%rax, -32(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L347
.L348:
	addq	$1, -32(%rbp)
.L347:
	call	__ctype_b_loc
	movq	(%rax), %rdx
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movsbq	%al, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	andl	$8192, %eax
	testl	%eax, %eax
	jne	.L348
	jmp	.L349
.L351:
	addl	$1, -12(%rbp)
.L349:
	movl	-12(%rbp), %eax
	cltq
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L350
	call	__ctype_b_loc
	movq	(%rax), %rdx
	movl	-12(%rbp), %eax
	cltq
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movsbq	%al, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	andl	$8192, %eax
	testl	%eax, %eax
	je	.L351
.L350:
	cmpl	$0, -12(%rbp)
	jle	.L358
	movl	-12(%rbp), %eax
	movl	%eax, -4(%rbp)
	cmpl	$1024, -4(%rbp)
	jle	.L353
	movl	$1024, -4(%rbp)
.L353:
	movl	$0, -8(%rbp)
	jmp	.L354
.L355:
	movl	-8(%rbp), %eax
	cltq
	addq	-32(%rbp), %rax
	movzbl	(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movb	%dl, tmpName(%rax)
	addl	$1, -8(%rbp)
.L354:
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jl	.L355
	movl	-4(%rbp), %eax
	cltq
	movb	$0, tmpName(%rax)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movl	$tmpName, %esi
	movq	%rax, %rdi
	call	snocString
	movq	-40(%rbp), %rdx
	movq	%rax, (%rdx)
	jmp	.L358
.L357:
	nop
.L344:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE42:
	.size	addFlagsFromEnvVar, .-addFlagsFromEnvVar
	.section	.rodata
.LC82:
	.string	"BZIP2"
.LC83:
	.string	"BZIP"
.LC84:
	.string	"--"
.LC85:
	.string	"unzip"
.LC86:
	.string	"UNZIP"
.LC87:
	.string	"z2cat"
.LC88:
	.string	"Z2CAT"
.LC89:
	.string	"zcat"
.LC90:
	.string	"ZCAT"
.LC91:
	.string	"%s: Bad flag `%s'\n"
.LC92:
	.string	"--stdout"
.LC93:
	.string	"--decompress"
.LC94:
	.string	"--compress"
.LC95:
	.string	"--force"
.LC96:
	.string	"--test"
.LC97:
	.string	"--keep"
.LC98:
	.string	"--small"
.LC99:
	.string	"--quiet"
.LC100:
	.string	"--version"
.LC101:
	.string	"--license"
.LC102:
	.string	"--exponential"
.LC103:
	.string	"--repetitive-best"
.LC104:
	.string	"--repetitive-fast"
.LC105:
	.string	"--fast"
.LC106:
	.string	"--best"
.LC107:
	.string	"--verbose"
.LC108:
	.string	"--help"
	.align 8
.LC109:
	.string	"%s: -c and -t cannot be used together.\n"
	.align 8
.LC110:
	.string	"\nYou can use the `bzip2recover' program to attempt to recover\ndata from undamaged sections of corrupted files.\n\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB43:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movl	%edi, -52(%rbp)
	movq	%rsi, -64(%rbp)
	movq	$0, outputHandleJustInCase(%rip)
	movb	$0, smallMode(%rip)
	movb	$0, keepInputFiles(%rip)
	movb	$0, forceOverwrite(%rip)
	movb	$1, noisy(%rip)
	movl	$0, verbosity(%rip)
	movl	$9, blockSize100k(%rip)
	movb	$0, testFailsExist(%rip)
	movb	$0, unzFailsExist(%rip)
	movl	$0, numFileNames(%rip)
	movl	$0, numFilesProcessed(%rip)
	movl	$30, workFactor(%rip)
	movb	$0, deleteOutputOnInterrupt(%rip)
	movl	$0, exitValue(%rip)
	movl	$0, -8(%rbp)
	movl	-8(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	$mySIGSEGVorSIGBUScatcher, %esi
	movl	$11, %edi
	call	signal
	movl	$mySIGSEGVorSIGBUScatcher, %esi
	movl	$7, %edi
	call	signal
	movl	$.LC75, %esi
	movl	$inName, %edi
	call	copyFileName
	movl	$.LC75, %esi
	movl	$outName, %edi
	call	copyFileName
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rsi
	movl	$progNameReally, %edi
	call	copyFileName
	movq	$progNameReally, progName(%rip)
	movq	$progNameReally, -40(%rbp)
	jmp	.L360
.L362:
	movq	-40(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	$47, %al
	jne	.L361
	movq	-40(%rbp), %rax
	addq	$1, %rax
	movq	%rax, progName(%rip)
.L361:
	addq	$1, -40(%rbp)
.L360:
	movq	-40(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L362
	movq	$0, -48(%rbp)
	leaq	-48(%rbp), %rax
	movl	$.LC82, %esi
	movq	%rax, %rdi
	call	addFlagsFromEnvVar
	leaq	-48(%rbp), %rax
	movl	$.LC83, %esi
	movq	%rax, %rdi
	call	addFlagsFromEnvVar
	movl	$1, -12(%rbp)
	jmp	.L363
.L364:
	movl	-12(%rbp), %eax
	cltq
	salq	$3, %rax
	addq	-64(%rbp), %rax
	movq	(%rax), %rdx
	movq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	snocString
	movq	%rax, -48(%rbp)
	addl	$1, -12(%rbp)
.L363:
	movl	-52(%rbp), %eax
	subl	$1, %eax
	cmpl	-12(%rbp), %eax
	jge	.L364
	movl	$7, longestFileName(%rip)
	movl	$0, numFileNames(%rip)
	movb	$1, -1(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L365
.L369:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC84, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L366
	movb	$0, -1(%rbp)
	jmp	.L367
.L366:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$45, %al
	jne	.L368
	cmpb	$0, -1(%rbp)
	jne	.L462
.L368:
	movl	numFileNames(%rip), %eax
	addl	$1, %eax
	movl	%eax, numFileNames(%rip)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	$-1, -72(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-72(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, %edx
	movl	longestFileName(%rip), %eax
	cmpl	%eax, %edx
	jle	.L367
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	$-1, -72(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-72(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, longestFileName(%rip)
	jmp	.L367
.L462:
	nop
.L367:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
.L365:
	cmpq	$0, -32(%rbp)
	jne	.L369
	movl	numFileNames(%rip), %eax
	testl	%eax, %eax
	jne	.L370
	movl	$1, srcMode(%rip)
	jmp	.L371
.L370:
	movl	$3, srcMode(%rip)
.L371:
	movl	$1, opMode(%rip)
	movq	progName(%rip), %rax
	movl	$.LC85, %esi
	movq	%rax, %rdi
	call	strstr
	testq	%rax, %rax
	jne	.L372
	movq	progName(%rip), %rax
	movl	$.LC86, %esi
	movq	%rax, %rdi
	call	strstr
	testq	%rax, %rax
	je	.L373
.L372:
	movl	$2, opMode(%rip)
.L373:
	movq	progName(%rip), %rax
	movl	$.LC87, %esi
	movq	%rax, %rdi
	call	strstr
	testq	%rax, %rax
	jne	.L374
	movq	progName(%rip), %rax
	movl	$.LC88, %esi
	movq	%rax, %rdi
	call	strstr
	testq	%rax, %rax
	jne	.L374
	movq	progName(%rip), %rax
	movl	$.LC89, %esi
	movq	%rax, %rdi
	call	strstr
	testq	%rax, %rax
	jne	.L374
	movq	progName(%rip), %rax
	movl	$.LC90, %esi
	movq	%rax, %rdi
	call	strstr
	testq	%rax, %rax
	je	.L375
.L374:
	movl	$2, opMode(%rip)
	movl	numFileNames(%rip), %eax
	testl	%eax, %eax
	jne	.L376
	movl	$1, %eax
	jmp	.L377
.L376:
	movl	$2, %eax
.L377:
	movl	%eax, srcMode(%rip)
.L375:
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L378
.L407:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC84, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L463
.L379:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$45, %al
	jne	.L381
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	addq	$1, %rax
	movzbl	(%rax), %eax
	cmpb	$45, %al
	je	.L381
	movl	$1, -8(%rbp)
	jmp	.L382
.L406:
	movq	-32(%rbp), %rax
	movq	(%rax), %rdx
	movl	-8(%rbp), %eax
	cltq
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movsbl	%al, %eax
	subl	$49, %eax
	cmpl	$73, %eax
	ja	.L383
	mov	%eax, %eax
	movq	.L404(,%rax,8), %rax
	jmp	*%rax
	.section	.rodata
	.align 8
	.align 4
.L404:
	.quad	.L384
	.quad	.L385
	.quad	.L386
	.quad	.L387
	.quad	.L388
	.quad	.L389
	.quad	.L390
	.quad	.L391
	.quad	.L392
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L393
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L393
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L394
	.quad	.L395
	.quad	.L383
	.quad	.L396
	.quad	.L383
	.quad	.L397
	.quad	.L383
	.quad	.L383
	.quad	.L398
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L399
	.quad	.L383
	.quad	.L400
	.quad	.L401
	.quad	.L383
	.quad	.L402
	.quad	.L383
	.quad	.L383
	.quad	.L383
	.quad	.L403
	.text
.L394:
	movl	$2, srcMode(%rip)
	jmp	.L405
.L395:
	movl	$2, opMode(%rip)
	jmp	.L405
.L403:
	movl	$1, opMode(%rip)
	jmp	.L405
.L396:
	movb	$1, forceOverwrite(%rip)
	jmp	.L405
.L401:
	movl	$3, opMode(%rip)
	jmp	.L405
.L398:
	movb	$1, keepInputFiles(%rip)
	jmp	.L405
.L400:
	movb	$1, smallMode(%rip)
	jmp	.L405
.L399:
	movb	$0, noisy(%rip)
	jmp	.L405
.L384:
	movl	$1, blockSize100k(%rip)
	jmp	.L405
.L385:
	movl	$2, blockSize100k(%rip)
	jmp	.L405
.L386:
	movl	$3, blockSize100k(%rip)
	jmp	.L405
.L387:
	movl	$4, blockSize100k(%rip)
	jmp	.L405
.L388:
	movl	$5, blockSize100k(%rip)
	jmp	.L405
.L389:
	movl	$6, blockSize100k(%rip)
	jmp	.L405
.L390:
	movl	$7, blockSize100k(%rip)
	jmp	.L405
.L391:
	movl	$8, blockSize100k(%rip)
	jmp	.L405
.L392:
	movl	$9, blockSize100k(%rip)
	jmp	.L405
.L393:
	call	license
	jmp	.L405
.L402:
	movl	verbosity(%rip), %eax
	addl	$1, %eax
	movl	%eax, verbosity(%rip)
	jmp	.L405
.L397:
	movq	progName(%rip), %rax
	movq	%rax, %rdi
	call	usage
	movl	$0, %edi
	call	exit
.L383:
	movq	-32(%rbp), %rax
	movq	(%rax), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC91, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rax
	movq	%rax, %rdi
	call	usage
	movl	$1, %edi
	call	exit
.L405:
	addl	$1, -8(%rbp)
.L382:
	movq	-32(%rbp), %rax
	movq	(%rax), %rdx
	movl	-8(%rbp), %eax
	cltq
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L406
.L381:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
.L378:
	cmpq	$0, -32(%rbp)
	jne	.L407
	jmp	.L380
.L463:
	nop
.L380:
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L408
.L429:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC84, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L464
.L409:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC92, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L411
	movl	$2, srcMode(%rip)
	jmp	.L412
.L411:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC93, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L413
	movl	$2, opMode(%rip)
	jmp	.L412
.L413:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC94, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L414
	movl	$1, opMode(%rip)
	jmp	.L412
.L414:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC95, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L415
	movb	$1, forceOverwrite(%rip)
	jmp	.L412
.L415:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC96, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L416
	movl	$3, opMode(%rip)
	jmp	.L412
.L416:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC97, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L417
	movb	$1, keepInputFiles(%rip)
	jmp	.L412
.L417:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC98, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L418
	movb	$1, smallMode(%rip)
	jmp	.L412
.L418:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC99, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L419
	movb	$0, noisy(%rip)
	jmp	.L412
.L419:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC100, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L420
	call	license
	jmp	.L412
.L420:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC101, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L421
	call	license
	jmp	.L412
.L421:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC102, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L422
	movl	$1, workFactor(%rip)
	jmp	.L412
.L422:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC103, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L423
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	redundant
	jmp	.L412
.L423:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC104, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L424
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	redundant
	jmp	.L412
.L424:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC105, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L425
	movl	$1, blockSize100k(%rip)
	jmp	.L412
.L425:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC106, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L426
	movl	$9, blockSize100k(%rip)
	jmp	.L412
.L426:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC107, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L427
	movl	verbosity(%rip), %eax
	addl	$1, %eax
	movl	%eax, verbosity(%rip)
	jmp	.L412
.L427:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC108, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L428
	movq	progName(%rip), %rax
	movq	%rax, %rdi
	call	usage
	movl	$0, %edi
	call	exit
.L428:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movl	$.LC84, %eax
	movl	$2, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L412
	movq	-32(%rbp), %rax
	movq	(%rax), %rcx
	movq	progName(%rip), %rdx
	movl	$.LC91, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	progName(%rip), %rax
	movq	%rax, %rdi
	call	usage
	movl	$1, %edi
	call	exit
.L412:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
.L408:
	cmpq	$0, -32(%rbp)
	jne	.L429
	jmp	.L410
.L464:
	nop
.L410:
	movl	verbosity(%rip), %eax
	cmpl	$4, %eax
	jle	.L430
	movl	$4, verbosity(%rip)
.L430:
	movl	opMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L431
	movzbl	smallMode(%rip), %eax
	testb	%al, %al
	je	.L431
	movl	blockSize100k(%rip), %eax
	cmpl	$2, %eax
	jle	.L431
	movl	$2, blockSize100k(%rip)
.L431:
	movl	opMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L432
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	jne	.L432
	movq	progName(%rip), %rdx
	movl	$.LC109, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	exit
.L432:
	movl	srcMode(%rip), %eax
	cmpl	$2, %eax
	jne	.L433
	movl	numFileNames(%rip), %eax
	testl	%eax, %eax
	jne	.L433
	movl	$1, srcMode(%rip)
.L433:
	movl	opMode(%rip), %eax
	cmpl	$1, %eax
	je	.L434
	movl	$0, blockSize100k(%rip)
.L434:
	movl	srcMode(%rip), %eax
	cmpl	$3, %eax
	jne	.L435
	movl	$mySignalCatcher, %esi
	movl	$2, %edi
	call	signal
	movl	$mySignalCatcher, %esi
	movl	$15, %edi
	call	signal
	movl	$mySignalCatcher, %esi
	movl	$1, %edi
	call	signal
.L435:
	movl	opMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L436
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L437
	movl	$0, %edi
	call	compress
	jmp	.L438
.L437:
	movb	$1, -1(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L439
.L443:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC84, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L440
	movb	$0, -1(%rbp)
	jmp	.L441
.L440:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$45, %al
	jne	.L442
	cmpb	$0, -1(%rbp)
	jne	.L465
.L442:
	movl	numFilesProcessed(%rip), %eax
	addl	$1, %eax
	movl	%eax, numFilesProcessed(%rip)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	compress
	jmp	.L441
.L465:
	nop
.L441:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
.L439:
	cmpq	$0, -32(%rbp)
	jne	.L443
	jmp	.L438
.L436:
	movl	opMode(%rip), %eax
	cmpl	$2, %eax
	jne	.L444
	movb	$0, unzFailsExist(%rip)
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L445
	movl	$0, %edi
	call	uncompress
	jmp	.L446
.L445:
	movb	$1, -1(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L447
.L451:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC84, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L448
	movb	$0, -1(%rbp)
	jmp	.L449
.L448:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$45, %al
	jne	.L450
	cmpb	$0, -1(%rbp)
	jne	.L466
.L450:
	movl	numFilesProcessed(%rip), %eax
	addl	$1, %eax
	movl	%eax, numFilesProcessed(%rip)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	uncompress
	jmp	.L449
.L466:
	nop
.L449:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
.L447:
	cmpq	$0, -32(%rbp)
	jne	.L451
.L446:
	movzbl	unzFailsExist(%rip), %eax
	testb	%al, %al
	je	.L438
	movl	$2, %edi
	call	setExit
	movl	exitValue(%rip), %eax
	movl	%eax, %edi
	call	exit
.L444:
	movb	$0, testFailsExist(%rip)
	movl	srcMode(%rip), %eax
	cmpl	$1, %eax
	jne	.L452
	movl	$0, %edi
	call	testf
	jmp	.L453
.L452:
	movb	$1, -1(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L454
.L458:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	$.LC84, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L455
	movb	$0, -1(%rbp)
	jmp	.L456
.L455:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	cmpb	$45, %al
	jne	.L457
	cmpb	$0, -1(%rbp)
	jne	.L467
.L457:
	movl	numFilesProcessed(%rip), %eax
	addl	$1, %eax
	movl	%eax, numFilesProcessed(%rip)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	testf
	jmp	.L456
.L467:
	nop
.L456:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
.L454:
	cmpq	$0, -32(%rbp)
	jne	.L458
.L453:
	movzbl	testFailsExist(%rip), %eax
	testb	%al, %al
	je	.L438
	movzbl	noisy(%rip), %eax
	testb	%al, %al
	je	.L438
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC110, %eax
	movq	%rdx, %rcx
	movl	$112, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	$2, %edi
	call	setExit
	movl	exitValue(%rip), %eax
	movl	%eax, %edi
	call	exit
.L438:
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L459
.L461:
	movq	-32(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -24(%rbp)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L460
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	free
.L460:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	free
	movq	-24(%rbp), %rax
	movq	%rax, -32(%rbp)
.L459:
	cmpq	$0, -32(%rbp)
	jne	.L461
	movl	exitValue(%rip), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE43:
	.size	main, .-main
	.type	fallbackSimpleSort, @function
fallbackSimpleSort:
.LFB44:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	movl	%ecx, -40(%rbp)
	movl	-36(%rbp), %eax
	cmpl	-40(%rbp), %eax
	je	.L482
.L469:
	movl	-36(%rbp), %eax
	movl	-40(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$3, %eax
	jle	.L471
	movl	-40(%rbp), %eax
	subl	$4, %eax
	movl	%eax, -16(%rbp)
	jmp	.L472
.L476:
	movl	-16(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -8(%rbp)
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -4(%rbp)
	movl	-16(%rbp), %eax
	addl	$4, %eax
	movl	%eax, -12(%rbp)
	jmp	.L473
.L475:
	movl	-12(%rbp), %eax
	cltq
	subq	$4, %rax
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	-12(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-24(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	addl	$4, -12(%rbp)
.L473:
	movl	-12(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jg	.L474
	movl	-12(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-4(%rbp), %eax
	jb	.L475
.L474:
	movl	-12(%rbp), %eax
	cltq
	subq	$4, %rax
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	-8(%rbp), %edx
	movl	%edx, (%rax)
	subl	$1, -16(%rbp)
.L472:
	movl	-16(%rbp), %eax
	cmpl	-36(%rbp), %eax
	jge	.L476
.L471:
	movl	-40(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -16(%rbp)
	jmp	.L477
.L481:
	movl	-16(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -8(%rbp)
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -4(%rbp)
	movl	-16(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	jmp	.L478
.L480:
	movl	-12(%rbp), %eax
	cltq
	subq	$1, %rax
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	-12(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-24(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	addl	$1, -12(%rbp)
.L478:
	movl	-12(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jg	.L479
	movl	-12(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-4(%rbp), %eax
	jb	.L480
.L479:
	movl	-12(%rbp), %eax
	cltq
	subq	$1, %rax
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	-8(%rbp), %edx
	movl	%edx, (%rax)
	subl	$1, -16(%rbp)
.L477:
	movl	-16(%rbp), %eax
	cmpl	-36(%rbp), %eax
	jge	.L481
	jmp	.L468
.L482:
	nop
.L468:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE44:
	.size	fallbackSimpleSort, .-fallbackSimpleSort
	.type	fallbackQSort3, @function
fallbackQSort3:
.LFB45:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$936, %rsp
	movq	%rdi, -920(%rbp)
	movq	%rsi, -928(%rbp)
	movl	%edx, -932(%rbp)
	movl	%ecx, -936(%rbp)
	movl	$0, -84(%rbp)
	movl	$0, -92(%rbp)
	movl	-92(%rbp), %eax
	cltq
	movl	-932(%rbp), %edx
	movl	%edx, -912(%rbp,%rax,4)
	movl	-92(%rbp), %eax
	cltq
	movl	-936(%rbp), %edx
	movl	%edx, -512(%rbp,%rax,4)
	addl	$1, -92(%rbp)
	jmp	.L484
	.cfi_offset 3, -24
.L509:
	cmpl	$98, -92(%rbp)
	jle	.L485
	movl	$1004, %edi
	call	BZ2_bz__AssertH__fail
.L485:
	subl	$1, -92(%rbp)
	movl	-92(%rbp), %eax
	cltq
	movl	-912(%rbp,%rax,4), %eax
	movl	%eax, -56(%rbp)
	movl	-92(%rbp), %eax
	cltq
	movl	-512(%rbp,%rax,4), %eax
	movl	%eax, -52(%rbp)
	movl	-56(%rbp), %eax
	movl	-52(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$9, %eax
	jg	.L486
	movl	-52(%rbp), %ecx
	movl	-56(%rbp), %edx
	movq	-928(%rbp), %rsi
	movq	-920(%rbp), %rax
	movq	%rax, %rdi
	call	fallbackSimpleSort
	jmp	.L484
.L486:
	movl	-84(%rbp), %eax
	imull	$7621, %eax, %eax
	addl	$1, %eax
	andl	$32767, %eax
	movl	%eax, -84(%rbp)
	movl	-84(%rbp), %ecx
	movl	$-1431655765, %edx
	movl	%ecx, %eax
	mull	%edx
	shrl	%edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -48(%rbp)
	cmpl	$0, -48(%rbp)
	jne	.L487
	movl	-56(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-928(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -88(%rbp)
	jmp	.L488
.L487:
	cmpl	$1, -48(%rbp)
	jne	.L489
	movl	-52(%rbp), %eax
	movl	-56(%rbp), %edx
	addl	%edx, %eax
	sarl	%eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-928(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -88(%rbp)
	jmp	.L488
.L489:
	movl	-52(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-928(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -88(%rbp)
.L488:
	movl	-56(%rbp), %eax
	movl	%eax, -100(%rbp)
	movl	-100(%rbp), %eax
	movl	%eax, -108(%rbp)
	movl	-52(%rbp), %eax
	movl	%eax, -96(%rbp)
	movl	-96(%rbp), %eax
	movl	%eax, -104(%rbp)
.L495:
	movl	-108(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jg	.L511
.L490:
	movl	-108(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-928(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movl	-88(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -44(%rbp)
	cmpl	$0, -44(%rbp)
	jne	.L492
	movl	-108(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -40(%rbp)
	movl	-108(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-100(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-920(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-100(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-40(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -100(%rbp)
	addl	$1, -108(%rbp)
	nop
	jmp	.L495
.L492:
	cmpl	$0, -44(%rbp)
	jg	.L512
.L494:
	addl	$1, -108(%rbp)
	jmp	.L495
.L511:
	nop
	jmp	.L491
.L512:
	nop
.L491:
	movl	-108(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jg	.L513
.L496:
	movl	-104(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-928(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movl	-88(%rbp), %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	movl	%eax, -44(%rbp)
	cmpl	$0, -44(%rbp)
	jne	.L498
	movl	-104(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-104(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-96(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-920(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-96(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-36(%rbp), %edx
	movl	%edx, (%rax)
	subl	$1, -96(%rbp)
	subl	$1, -104(%rbp)
	nop
	jmp	.L491
.L498:
	cmpl	$0, -44(%rbp)
	js	.L514
.L500:
	subl	$1, -104(%rbp)
	jmp	.L491
.L513:
	nop
	jmp	.L497
.L514:
	nop
.L497:
	movl	-108(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jg	.L515
.L501:
	movl	-108(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movl	-108(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-104(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-920(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-104(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-20(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -108(%rbp)
	subl	$1, -104(%rbp)
	jmp	.L495
.L515:
	nop
.L510:
	movl	-96(%rbp), %eax
	cmpl	-100(%rbp), %eax
	jl	.L516
.L503:
	movl	-100(%rbp), %eax
	movl	-108(%rbp), %edx
	subl	%eax, %edx
	movl	-56(%rbp), %eax
	movl	-100(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %edx
	cmovle	%edx, %eax
	movl	%eax, -44(%rbp)
	movl	-56(%rbp), %eax
	movl	%eax, -80(%rbp)
	movl	-44(%rbp), %eax
	movl	-108(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -76(%rbp)
	movl	-44(%rbp), %eax
	movl	%eax, -72(%rbp)
	jmp	.L504
.L505:
	movl	-80(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -32(%rbp)
	movl	-80(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-76(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-920(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-76(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-32(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -80(%rbp)
	addl	$1, -76(%rbp)
	subl	$1, -72(%rbp)
.L504:
	cmpl	$0, -72(%rbp)
	jg	.L505
	movl	-104(%rbp), %eax
	movl	-96(%rbp), %edx
	subl	%eax, %edx
	movl	-96(%rbp), %eax
	movl	-52(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %edx
	cmovle	%edx, %eax
	movl	%eax, -28(%rbp)
	movl	-108(%rbp), %eax
	movl	%eax, -68(%rbp)
	movl	-28(%rbp), %eax
	movl	-52(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, -64(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, -60(%rbp)
	jmp	.L506
.L507:
	movl	-68(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -24(%rbp)
	movl	-68(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-64(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-920(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-64(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-920(%rbp), %rax
	movl	-24(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -68(%rbp)
	addl	$1, -64(%rbp)
	subl	$1, -60(%rbp)
.L506:
	cmpl	$0, -60(%rbp)
	jg	.L507
	movl	-108(%rbp), %eax
	movl	-56(%rbp), %edx
	addl	%edx, %eax
	subl	-100(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -44(%rbp)
	movl	-96(%rbp), %eax
	movl	-104(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	addl	-52(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -28(%rbp)
	movl	-56(%rbp), %eax
	movl	-44(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	-28(%rbp), %eax
	movl	-52(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %ecx
	jle	.L508
	movl	-92(%rbp), %eax
	cltq
	movl	-56(%rbp), %edx
	movl	%edx, -912(%rbp,%rax,4)
	movl	-92(%rbp), %eax
	cltq
	movl	-44(%rbp), %edx
	movl	%edx, -512(%rbp,%rax,4)
	addl	$1, -92(%rbp)
	movl	-92(%rbp), %eax
	cltq
	movl	-28(%rbp), %edx
	movl	%edx, -912(%rbp,%rax,4)
	movl	-92(%rbp), %eax
	cltq
	movl	-52(%rbp), %edx
	movl	%edx, -512(%rbp,%rax,4)
	addl	$1, -92(%rbp)
	jmp	.L484
.L508:
	movl	-92(%rbp), %eax
	cltq
	movl	-28(%rbp), %edx
	movl	%edx, -912(%rbp,%rax,4)
	movl	-92(%rbp), %eax
	cltq
	movl	-52(%rbp), %edx
	movl	%edx, -512(%rbp,%rax,4)
	addl	$1, -92(%rbp)
	movl	-92(%rbp), %eax
	cltq
	movl	-56(%rbp), %edx
	movl	%edx, -912(%rbp,%rax,4)
	movl	-92(%rbp), %eax
	cltq
	movl	-44(%rbp), %edx
	movl	%edx, -512(%rbp,%rax,4)
	addl	$1, -92(%rbp)
	jmp	.L484
.L516:
	nop
.L484:
	cmpl	$0, -92(%rbp)
	jg	.L509
	addq	$936, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE45:
	.size	fallbackQSort3, .-fallbackQSort3
	.section	.rodata
.LC111:
	.string	"        bucket sorting ...\n"
.LC112:
	.string	"        depth %6d has "
.LC113:
	.string	"%6d unresolved strings\n"
	.align 8
.LC114:
	.string	"        reconstructing block ...\n"
	.text
	.type	fallbackSort, @function
fallbackSort:
.LFB46:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$2152, %rsp
	movq	%rdi, -2136(%rbp)
	movq	%rsi, -2144(%rbp)
	movq	%rdx, -2152(%rbp)
	movl	%ecx, -2156(%rbp)
	movl	%r8d, -2160(%rbp)
	movq	-2144(%rbp), %rax
	movq	%rax, -64(%rbp)
	cmpl	$3, -2160(%rbp)
	jle	.L518
	.cfi_offset 3, -24
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC111, %eax
	movq	%rdx, %rcx
	movl	$27, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L518:
	movl	$0, -52(%rbp)
	jmp	.L519
.L520:
	movl	-52(%rbp), %eax
	cltq
	movl	$0, -2128(%rbp,%rax,4)
	addl	$1, -52(%rbp)
.L519:
	cmpl	$256, -52(%rbp)
	jle	.L520
	movl	$0, -52(%rbp)
	jmp	.L521
.L522:
	movl	-52(%rbp), %eax
	cltq
	addq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movslq	%eax, %rdx
	movl	-2128(%rbp,%rdx,4), %edx
	addl	$1, %edx
	cltq
	movl	%edx, -2128(%rbp,%rax,4)
	addl	$1, -52(%rbp)
.L521:
	movl	-52(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jl	.L522
	movl	$0, -52(%rbp)
	jmp	.L523
.L524:
	movl	-52(%rbp), %eax
	cltq
	movl	-2128(%rbp,%rax,4), %edx
	movl	-52(%rbp), %eax
	cltq
	movl	%edx, -1088(%rbp,%rax,4)
	addl	$1, -52(%rbp)
.L523:
	cmpl	$255, -52(%rbp)
	jle	.L524
	movl	$1, -52(%rbp)
	jmp	.L525
.L526:
	movl	-52(%rbp), %eax
	cltq
	movl	-2128(%rbp,%rax,4), %edx
	movl	-52(%rbp), %eax
	subl	$1, %eax
	cltq
	movl	-2128(%rbp,%rax,4), %eax
	addl	%eax, %edx
	movl	-52(%rbp), %eax
	cltq
	movl	%edx, -2128(%rbp,%rax,4)
	addl	$1, -52(%rbp)
.L525:
	cmpl	$256, -52(%rbp)
	jle	.L526
	movl	$0, -52(%rbp)
	jmp	.L527
.L528:
	movl	-52(%rbp), %eax
	cltq
	addq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -48(%rbp)
	movl	-48(%rbp), %eax
	cltq
	movl	-2128(%rbp,%rax,4), %eax
	subl	$1, %eax
	movl	%eax, -44(%rbp)
	movl	-48(%rbp), %eax
	cltq
	movl	-44(%rbp), %edx
	movl	%edx, -2128(%rbp,%rax,4)
	movl	-44(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-2136(%rbp), %rax
	movl	-52(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -52(%rbp)
.L527:
	movl	-52(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jl	.L528
	movl	-2156(%rbp), %eax
	leal	31(%rax), %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	sarl	$5, %eax
	addl	$2, %eax
	movl	%eax, -28(%rbp)
	movl	$0, -52(%rbp)
	jmp	.L529
.L530:
	movl	-52(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	$0, (%rax)
	addl	$1, -52(%rbp)
.L529:
	movl	-52(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jl	.L530
	movl	$0, -52(%rbp)
	jmp	.L531
.L532:
	movl	-52(%rbp), %eax
	cltq
	movl	-2128(%rbp,%rax,4), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	-52(%rbp), %edx
	movslq	%edx, %rdx
	movl	-2128(%rbp,%rdx,4), %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-2152(%rbp), %rdx
	movl	(%rdx), %esi
	movl	-52(%rbp), %edx
	movslq	%edx, %rdx
	movl	-2128(%rbp,%rdx,4), %edx
	andl	$31, %edx
	movl	$1, %edi
	movl	%edi, %ebx
	movl	%edx, %ecx
	sall	%cl, %ebx
	movl	%ebx, %edx
	orl	%esi, %edx
	movl	%edx, (%rax)
	addl	$1, -52(%rbp)
.L531:
	cmpl	$255, -52(%rbp)
	jle	.L532
	movl	$0, -52(%rbp)
	jmp	.L533
.L534:
	movl	-52(%rbp), %eax
	addl	%eax, %eax
	addl	-2156(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	-52(%rbp), %edx
	addl	%edx, %edx
	addl	-2156(%rbp), %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-2152(%rbp), %rdx
	movl	(%rdx), %esi
	movl	-52(%rbp), %edx
	addl	%edx, %edx
	addl	-2156(%rbp), %edx
	andl	$31, %edx
	movl	$1, %edi
	movl	%edi, %ebx
	movl	%edx, %ecx
	sall	%cl, %ebx
	movl	%ebx, %edx
	orl	%esi, %edx
	movl	%edx, (%rax)
	movl	-52(%rbp), %eax
	addl	%eax, %eax
	addl	-2156(%rbp), %eax
	addl	$1, %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	-52(%rbp), %edx
	addl	%edx, %edx
	addl	-2156(%rbp), %edx
	addl	$1, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-2152(%rbp), %rdx
	movl	(%rdx), %esi
	movl	-52(%rbp), %edx
	addl	%edx, %edx
	addl	-2156(%rbp), %edx
	addl	$1, %edx
	andl	$31, %edx
	movl	$1, %edi
	movl	%edi, %ebx
	movl	%edx, %ecx
	sall	%cl, %ebx
	movl	%ebx, %edx
	notl	%edx
	andl	%esi, %edx
	movl	%edx, (%rax)
	addl	$1, -52(%rbp)
.L533:
	cmpl	$31, -52(%rbp)
	jle	.L534
	movl	$1, -56(%rbp)
	jmp	.L566
.L576:
	nop
.L566:
	cmpl	$3, -2160(%rbp)
	jle	.L535
	movl	$.LC112, %ecx
	movq	stderr(%rip), %rax
	movl	-56(%rbp), %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L535:
	movl	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	jmp	.L536
.L539:
	movl	-52(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-52(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	je	.L537
	movl	-52(%rbp), %eax
	movl	%eax, -48(%rbp)
.L537:
	movl	-52(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-2136(%rbp), %rax
	movl	(%rax), %edx
	movl	-56(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -44(%rbp)
	cmpl	$0, -44(%rbp)
	jns	.L538
	movl	-2156(%rbp), %eax
	addl	%eax, -44(%rbp)
.L538:
	movl	-44(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-2144(%rbp), %rax
	movl	-48(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -52(%rbp)
.L536:
	movl	-52(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jl	.L539
	movl	$0, -32(%rbp)
	movl	$-1, -40(%rbp)
	jmp	.L563
.L575:
	nop
.L563:
	movl	-40(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -44(%rbp)
	jmp	.L540
.L542:
	addl	$1, -44(%rbp)
.L540:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-44(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	je	.L541
	movl	-44(%rbp), %eax
	andl	$31, %eax
	testl	%eax, %eax
	jne	.L542
.L541:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-44(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	je	.L543
	jmp	.L544
.L545:
	addl	$32, -44(%rbp)
.L544:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L545
	jmp	.L546
.L547:
	addl	$1, -44(%rbp)
.L546:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-44(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	jne	.L547
.L543:
	movl	-44(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -24(%rbp)
	movl	-24(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jl	.L573
	jmp	.L549
.L552:
	addl	$1, -44(%rbp)
	jmp	.L550
.L573:
	nop
.L550:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-44(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	jne	.L551
	movl	-44(%rbp), %eax
	andl	$31, %eax
	testl	%eax, %eax
	jne	.L552
.L551:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-44(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	jne	.L553
	jmp	.L554
.L555:
	addl	$32, -44(%rbp)
.L554:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L555
	jmp	.L556
.L557:
	addl	$1, -44(%rbp)
.L556:
	movl	-44(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	(%rax), %edx
	movl	-44(%rbp), %eax
	andl	$31, %eax
	movl	$1, %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	andl	%edx, %eax
	testl	%eax, %eax
	je	.L557
.L553:
	movl	-44(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -40(%rbp)
	movl	-40(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jge	.L574
.L558:
	movl	-40(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jle	.L575
	movl	-24(%rbp), %eax
	movl	-40(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$1, %eax
	addl	%eax, -32(%rbp)
	movl	-40(%rbp), %ecx
	movl	-24(%rbp), %edx
	movq	-2144(%rbp), %rsi
	movq	-2136(%rbp), %rax
	movq	%rax, %rdi
	call	fallbackQSort3
	movl	$-1, -36(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -52(%rbp)
	jmp	.L560
.L562:
	movl	-52(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-2136(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	-2144(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movl	-36(%rbp), %eax
	cmpl	-20(%rbp), %eax
	je	.L561
	movl	-52(%rbp), %eax
	sarl	$5, %eax
	cltq
	salq	$2, %rax
	addq	-2152(%rbp), %rax
	movl	-52(%rbp), %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-2152(%rbp), %rdx
	movl	(%rdx), %esi
	movl	-52(%rbp), %edx
	andl	$31, %edx
	movl	$1, %edi
	movl	%edi, %ebx
	movl	%edx, %ecx
	sall	%cl, %ebx
	movl	%ebx, %edx
	orl	%esi, %edx
	movl	%edx, (%rax)
	movl	-20(%rbp), %eax
	movl	%eax, -36(%rbp)
.L561:
	addl	$1, -52(%rbp)
.L560:
	movl	-52(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jle	.L562
	jmp	.L575
.L574:
	nop
.L549:
	cmpl	$3, -2160(%rbp)
	jle	.L564
	movl	$.LC113, %ecx
	movq	stderr(%rip), %rax
	movl	-32(%rbp), %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L564:
	sall	-56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jg	.L565
	cmpl	$0, -32(%rbp)
	jne	.L576
.L565:
	cmpl	$3, -2160(%rbp)
	jle	.L567
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC114, %eax
	movq	%rdx, %rcx
	movl	$33, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L567:
	movl	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	jmp	.L568
.L570:
	addl	$1, -48(%rbp)
	jmp	.L569
.L577:
	nop
.L569:
	movl	-48(%rbp), %eax
	cltq
	movl	-1088(%rbp,%rax,4), %eax
	testl	%eax, %eax
	je	.L570
	movl	-48(%rbp), %eax
	cltq
	movl	-1088(%rbp,%rax,4), %eax
	leal	-1(%rax), %edx
	movl	-48(%rbp), %eax
	cltq
	movl	%edx, -1088(%rbp,%rax,4)
	movl	-52(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-2136(%rbp), %rax
	movl	(%rax), %eax
	mov	%eax, %eax
	addq	-64(%rbp), %rax
	movl	-48(%rbp), %edx
	movb	%dl, (%rax)
	addl	$1, -52(%rbp)
.L568:
	movl	-52(%rbp), %eax
	cmpl	-2156(%rbp), %eax
	jl	.L577
	cmpl	$255, -48(%rbp)
	jle	.L517
	movl	$1005, %edi
	call	BZ2_bz__AssertH__fail
.L517:
	addq	$2152, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE46:
	.size	fallbackSort, .-fallbackSort
	.type	mainGtU, @function
mainGtU:
.LFB47:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movl	%r8d, -44(%rbp)
	movq	%r9, -56(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L579
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L579:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L581
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L581:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L582
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L582:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L583
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L583:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L584
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L584:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L585
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L585:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L586
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L586:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L587
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L587:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L588
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L588:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L589
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L589:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L590
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L590:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L591
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L591:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	movl	-44(%rbp), %eax
	addl	$8, %eax
	movl	%eax, -12(%rbp)
.L610:
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L592
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L592:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L593
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L593:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L594
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L594:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L595
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L595:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L596
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L596:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L597
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L597:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L598
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L598:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L599
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L599:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L600
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L600:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L601
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L601:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L602
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L602:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L603
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L603:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L604
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L604:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L605
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L605:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	mov	-20(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -2(%rbp)
	mov	-24(%rbp), %eax
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	je	.L606
	movzbl	-2(%rbp), %eax
	cmpb	-1(%rbp), %al
	seta	%al
	jmp	.L580
.L606:
	mov	-20(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -6(%rbp)
	mov	-24(%rbp), %eax
	addq	%rax, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -4(%rbp)
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	je	.L607
	movzwl	-6(%rbp), %eax
	cmpw	-4(%rbp), %ax
	seta	%al
	jmp	.L580
.L607:
	addl	$1, -20(%rbp)
	addl	$1, -24(%rbp)
	movl	-20(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jb	.L608
	movl	-44(%rbp), %eax
	subl	%eax, -20(%rbp)
.L608:
	movl	-24(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jb	.L609
	movl	-44(%rbp), %eax
	subl	%eax, -24(%rbp)
.L609:
	subl	$8, -12(%rbp)
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	leal	-1(%rax), %edx
	movq	-56(%rbp), %rax
	movl	%edx, (%rax)
	cmpl	$0, -12(%rbp)
	jns	.L610
	movl	$0, %eax
.L580:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE47:
	.size	mainGtU, .-mainGtU
	.data
	.align 32
	.type	incs, @object
	.size	incs, 56
incs:
	.long	1
	.long	4
	.long	13
	.long	40
	.long	121
	.long	364
	.long	1093
	.long	3280
	.long	9841
	.long	29524
	.long	88573
	.long	265720
	.long	797161
	.long	2391484
	.text
	.type	mainSimpleSort, @function
mainSimpleSort:
.LFB48:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	movq	%rdi, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%ecx, -68(%rbp)
	movl	%r8d, -72(%rbp)
	movl	%r9d, -76(%rbp)
	movl	-72(%rbp), %eax
	movl	-76(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	cmpl	$1, -20(%rbp)
	jle	.L633
	.cfi_offset 3, -24
.L612:
	movl	$0, -24(%rbp)
	jmp	.L614
.L615:
	addl	$1, -24(%rbp)
.L614:
	movl	-24(%rbp), %eax
	cltq
	movl	incs(,%rax,4), %eax
	cmpl	-20(%rbp), %eax
	jl	.L615
	subl	$1, -24(%rbp)
	jmp	.L616
.L632:
	movl	-24(%rbp), %eax
	cltq
	movl	incs(,%rax,4), %eax
	movl	%eax, -16(%rbp)
	movl	-16(%rbp), %eax
	movl	-72(%rbp), %edx
	addl	%edx, %eax
	movl	%eax, -32(%rbp)
	jmp	.L631
.L640:
	nop
.L631:
	movl	-32(%rbp), %eax
	cmpl	-76(%rbp), %eax
	jg	.L634
.L617:
	movl	-32(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -12(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -28(%rbp)
	jmp	.L619
.L621:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-16(%rbp), %edx
	movl	-28(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-48(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-16(%rbp), %eax
	subl	%eax, -28(%rbp)
	movl	-16(%rbp), %eax
	movl	-72(%rbp), %edx
	addl	%edx, %eax
	subl	$1, %eax
	cmpl	-28(%rbp), %eax
	jge	.L635
.L619:
	movl	-68(%rbp), %esi
	movl	16(%rbp), %eax
	addl	-12(%rbp), %eax
	movl	-16(%rbp), %edx
	movl	-28(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-48(%rbp), %rdx
	movl	(%rdx), %ecx
	movl	16(%rbp), %edx
	leal	(%rcx,%rdx), %edi
	movq	24(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r8, %r9
	movl	%esi, %r8d
	movl	%eax, %esi
	call	mainGtU
	testb	%al, %al
	jne	.L621
	jmp	.L620
.L635:
	nop
.L620:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -32(%rbp)
	movl	-32(%rbp), %eax
	cmpl	-76(%rbp), %eax
	jg	.L636
.L622:
	movl	-32(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -12(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -28(%rbp)
	jmp	.L623
.L625:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-16(%rbp), %edx
	movl	-28(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-48(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-16(%rbp), %eax
	subl	%eax, -28(%rbp)
	movl	-16(%rbp), %eax
	movl	-72(%rbp), %edx
	addl	%edx, %eax
	subl	$1, %eax
	cmpl	-28(%rbp), %eax
	jge	.L637
.L623:
	movl	-68(%rbp), %esi
	movl	16(%rbp), %eax
	addl	-12(%rbp), %eax
	movl	-16(%rbp), %edx
	movl	-28(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-48(%rbp), %rdx
	movl	(%rdx), %ecx
	movl	16(%rbp), %edx
	leal	(%rcx,%rdx), %edi
	movq	24(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r8, %r9
	movl	%esi, %r8d
	movl	%eax, %esi
	call	mainGtU
	testb	%al, %al
	jne	.L625
	jmp	.L624
.L637:
	nop
.L624:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -32(%rbp)
	movl	-32(%rbp), %eax
	cmpl	-76(%rbp), %eax
	jg	.L638
.L626:
	movl	-32(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -12(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -28(%rbp)
	jmp	.L627
.L629:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-16(%rbp), %edx
	movl	-28(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-48(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-16(%rbp), %eax
	subl	%eax, -28(%rbp)
	movl	-16(%rbp), %eax
	movl	-72(%rbp), %edx
	addl	%edx, %eax
	subl	$1, %eax
	cmpl	-28(%rbp), %eax
	jge	.L639
.L627:
	movl	-68(%rbp), %esi
	movl	16(%rbp), %eax
	addl	-12(%rbp), %eax
	movl	-16(%rbp), %edx
	movl	-28(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-48(%rbp), %rdx
	movl	(%rdx), %ecx
	movl	16(%rbp), %edx
	leal	(%rcx,%rdx), %edi
	movq	24(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%r8, %r9
	movl	%esi, %r8d
	movl	%eax, %esi
	call	mainGtU
	testb	%al, %al
	jne	.L629
	jmp	.L628
.L639:
	nop
.L628:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -32(%rbp)
	movq	24(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.L640
	jmp	.L611
.L634:
	nop
	jmp	.L618
.L636:
	nop
	jmp	.L618
.L638:
	nop
.L618:
	subl	$1, -24(%rbp)
.L616:
	cmpl	$0, -24(%rbp)
	jns	.L632
	jmp	.L611
.L633:
	nop
.L611:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE48:
	.size	mainSimpleSort, .-mainSimpleSort
	.type	mmed3, @function
mmed3:
.LFB49:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%esi, %ecx
	movl	%edx, %eax
	movb	%dil, -20(%rbp)
	movb	%cl, -24(%rbp)
	movb	%al, -28(%rbp)
	movzbl	-20(%rbp), %eax
	cmpb	-24(%rbp), %al
	jbe	.L642
	movzbl	-20(%rbp), %eax
	movb	%al, -1(%rbp)
	movzbl	-24(%rbp), %eax
	movb	%al, -20(%rbp)
	movzbl	-1(%rbp), %eax
	movb	%al, -24(%rbp)
.L642:
	movzbl	-24(%rbp), %eax
	cmpb	-28(%rbp), %al
	jbe	.L643
	movzbl	-28(%rbp), %eax
	movb	%al, -24(%rbp)
	movzbl	-20(%rbp), %eax
	cmpb	-24(%rbp), %al
	jbe	.L643
	movzbl	-20(%rbp), %eax
	movb	%al, -24(%rbp)
.L643:
	movzbl	-24(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE49:
	.size	mmed3, .-mmed3
	.type	mainQSort3, @function
mainQSort3:
.LFB50:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$1416, %rsp
	movq	%rdi, -1368(%rbp)
	movq	%rsi, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	movl	%ecx, -1388(%rbp)
	movl	%r8d, -1392(%rbp)
	movl	%r9d, -1396(%rbp)
	movl	$0, -100(%rbp)
	movl	-100(%rbp), %eax
	cltq
	movl	-1392(%rbp), %edx
	movl	%edx, -1360(%rbp,%rax,4)
	movl	-100(%rbp), %eax
	cltq
	movl	-1396(%rbp), %edx
	movl	%edx, -960(%rbp,%rax,4)
	movl	-100(%rbp), %eax
	cltq
	movl	16(%rbp), %edx
	movl	%edx, -560(%rbp,%rax,4)
	addl	$1, -100(%rbp)
	jmp	.L645
	.cfi_offset 3, -24
.L672:
	cmpl	$97, -100(%rbp)
	jle	.L646
	movl	$1001, %edi
	call	BZ2_bz__AssertH__fail
.L646:
	subl	$1, -100(%rbp)
	movl	-100(%rbp), %eax
	cltq
	movl	-1360(%rbp,%rax,4), %eax
	movl	%eax, -72(%rbp)
	movl	-100(%rbp), %eax
	cltq
	movl	-960(%rbp,%rax,4), %eax
	movl	%eax, -68(%rbp)
	movl	-100(%rbp), %eax
	cltq
	movl	-560(%rbp,%rax,4), %eax
	movl	%eax, -64(%rbp)
	movl	-72(%rbp), %eax
	movl	-68(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$19, %eax
	jle	.L647
	cmpl	$14, -64(%rbp)
	jle	.L648
.L647:
	movl	-68(%rbp), %r9d
	movl	-72(%rbp), %r8d
	movl	-1388(%rbp), %ecx
	movq	-1384(%rbp), %rdx
	movq	-1376(%rbp), %rsi
	movq	-1368(%rbp), %rax
	movq	24(%rbp), %rdi
	movq	%rdi, 8(%rsp)
	movl	-64(%rbp), %edi
	movl	%edi, (%rsp)
	movq	%rax, %rdi
	call	mainSimpleSort
	movq	24(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jns	.L674
	jmp	.L644
.L648:
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edx
	addl	%edx, %eax
	sarl	%eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %edx
	movl	-64(%rbp), %eax
	addl	%edx, %eax
	mov	%eax, %eax
	addq	-1376(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %edx
	movl	-68(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %ecx
	movl	-64(%rbp), %eax
	addl	%ecx, %eax
	mov	%eax, %eax
	addq	-1376(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movl	-72(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %esi
	movl	-64(%rbp), %eax
	addl	%esi, %eax
	mov	%eax, %eax
	addq	-1376(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	mmed3
	movzbl	%al, %eax
	movl	%eax, -60(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -108(%rbp)
	movl	-108(%rbp), %eax
	movl	%eax, -116(%rbp)
	movl	-68(%rbp), %eax
	movl	%eax, -104(%rbp)
	movl	-104(%rbp), %eax
	movl	%eax, -112(%rbp)
.L656:
	movl	-116(%rbp), %eax
	cmpl	-112(%rbp), %eax
	jg	.L675
.L651:
	movl	-116(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %edx
	movl	-64(%rbp), %eax
	addl	%edx, %eax
	mov	%eax, %eax
	addq	-1376(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	subl	-60(%rbp), %eax
	movl	%eax, -56(%rbp)
	cmpl	$0, -56(%rbp)
	jne	.L653
	movl	-116(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -52(%rbp)
	movl	-116(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-108(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-1368(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-108(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-52(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -108(%rbp)
	addl	$1, -116(%rbp)
	nop
	jmp	.L656
.L653:
	cmpl	$0, -56(%rbp)
	jg	.L676
.L655:
	addl	$1, -116(%rbp)
	jmp	.L656
.L675:
	nop
	jmp	.L652
.L676:
	nop
.L652:
	movl	-116(%rbp), %eax
	cmpl	-112(%rbp), %eax
	jg	.L677
.L657:
	movl	-112(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %edx
	movl	-64(%rbp), %eax
	addl	%edx, %eax
	mov	%eax, %eax
	addq	-1376(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	subl	-60(%rbp), %eax
	movl	%eax, -56(%rbp)
	cmpl	$0, -56(%rbp)
	jne	.L659
	movl	-112(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -48(%rbp)
	movl	-112(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-104(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-1368(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-104(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-48(%rbp), %edx
	movl	%edx, (%rax)
	subl	$1, -104(%rbp)
	subl	$1, -112(%rbp)
	nop
	jmp	.L652
.L659:
	cmpl	$0, -56(%rbp)
	js	.L678
.L661:
	subl	$1, -112(%rbp)
	jmp	.L652
.L677:
	nop
	jmp	.L658
.L678:
	nop
.L658:
	movl	-116(%rbp), %eax
	cmpl	-112(%rbp), %eax
	jg	.L679
.L662:
	movl	-116(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movl	-116(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-112(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-1368(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-112(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-20(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -116(%rbp)
	subl	$1, -112(%rbp)
	jmp	.L656
.L679:
	nop
.L673:
	movl	-104(%rbp), %eax
	cmpl	-108(%rbp), %eax
	jge	.L664
	movl	-100(%rbp), %eax
	cltq
	movl	-72(%rbp), %edx
	movl	%edx, -1360(%rbp,%rax,4)
	movl	-100(%rbp), %eax
	cltq
	movl	-68(%rbp), %edx
	movl	%edx, -960(%rbp,%rax,4)
	movl	-64(%rbp), %eax
	leal	1(%rax), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -560(%rbp,%rax,4)
	addl	$1, -100(%rbp)
	jmp	.L645
.L664:
	movl	-108(%rbp), %eax
	movl	-116(%rbp), %edx
	subl	%eax, %edx
	movl	-72(%rbp), %eax
	movl	-108(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %edx
	cmovle	%edx, %eax
	movl	%eax, -56(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -96(%rbp)
	movl	-56(%rbp), %eax
	movl	-116(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -92(%rbp)
	movl	-56(%rbp), %eax
	movl	%eax, -88(%rbp)
	jmp	.L665
.L666:
	movl	-96(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -44(%rbp)
	movl	-96(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-92(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-1368(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-92(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-44(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -96(%rbp)
	addl	$1, -92(%rbp)
	subl	$1, -88(%rbp)
.L665:
	cmpl	$0, -88(%rbp)
	jg	.L666
	movl	-112(%rbp), %eax
	movl	-104(%rbp), %edx
	subl	%eax, %edx
	movl	-104(%rbp), %eax
	movl	-68(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %edx
	cmovle	%edx, %eax
	movl	%eax, -40(%rbp)
	movl	-116(%rbp), %eax
	movl	%eax, -84(%rbp)
	movl	-40(%rbp), %eax
	movl	-68(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, -80(%rbp)
	movl	-40(%rbp), %eax
	movl	%eax, -76(%rbp)
	jmp	.L667
.L668:
	movl	-84(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-84(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-80(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-1368(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%edx, (%rax)
	movl	-80(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-1368(%rbp), %rax
	movl	-36(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -84(%rbp)
	addl	$1, -80(%rbp)
	subl	$1, -76(%rbp)
.L667:
	cmpl	$0, -76(%rbp)
	jg	.L668
	movl	-116(%rbp), %eax
	movl	-72(%rbp), %edx
	addl	%edx, %eax
	subl	-108(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -56(%rbp)
	movl	-104(%rbp), %eax
	movl	-112(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	addl	-68(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -40(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -160(%rbp)
	movl	-56(%rbp), %eax
	movl	%eax, -144(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	movl	-40(%rbp), %eax
	movl	%eax, -156(%rbp)
	movl	-68(%rbp), %eax
	movl	%eax, -140(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -124(%rbp)
	movl	-56(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -152(%rbp)
	movl	-40(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -136(%rbp)
	movl	-64(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -120(%rbp)
	movl	-144(%rbp), %edx
	movl	-160(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	-140(%rbp), %edx
	movl	-156(%rbp), %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %ecx
	jge	.L669
	movl	-160(%rbp), %eax
	movl	%eax, -32(%rbp)
	movl	-156(%rbp), %eax
	movl	%eax, -160(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -156(%rbp)
	movl	-144(%rbp), %eax
	movl	%eax, -32(%rbp)
	movl	-140(%rbp), %eax
	movl	%eax, -144(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -140(%rbp)
	movl	-128(%rbp), %eax
	movl	%eax, -32(%rbp)
	movl	-124(%rbp), %eax
	movl	%eax, -128(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -124(%rbp)
.L669:
	movl	-140(%rbp), %edx
	movl	-156(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	-136(%rbp), %edx
	movl	-152(%rbp), %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %ecx
	jge	.L670
	movl	-156(%rbp), %eax
	movl	%eax, -28(%rbp)
	movl	-152(%rbp), %eax
	movl	%eax, -156(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, -152(%rbp)
	movl	-140(%rbp), %eax
	movl	%eax, -28(%rbp)
	movl	-136(%rbp), %eax
	movl	%eax, -140(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, -136(%rbp)
	movl	-124(%rbp), %eax
	movl	%eax, -28(%rbp)
	movl	-120(%rbp), %eax
	movl	%eax, -124(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, -120(%rbp)
.L670:
	movl	-144(%rbp), %edx
	movl	-160(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	-140(%rbp), %edx
	movl	-156(%rbp), %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %ecx
	jge	.L671
	movl	-160(%rbp), %eax
	movl	%eax, -24(%rbp)
	movl	-156(%rbp), %eax
	movl	%eax, -160(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -156(%rbp)
	movl	-144(%rbp), %eax
	movl	%eax, -24(%rbp)
	movl	-140(%rbp), %eax
	movl	%eax, -144(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -140(%rbp)
	movl	-128(%rbp), %eax
	movl	%eax, -24(%rbp)
	movl	-124(%rbp), %eax
	movl	%eax, -128(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -124(%rbp)
.L671:
	movl	-160(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -1360(%rbp,%rax,4)
	movl	-144(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -960(%rbp,%rax,4)
	movl	-128(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -560(%rbp,%rax,4)
	addl	$1, -100(%rbp)
	movl	-156(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -1360(%rbp,%rax,4)
	movl	-140(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -960(%rbp,%rax,4)
	movl	-124(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -560(%rbp,%rax,4)
	addl	$1, -100(%rbp)
	movl	-152(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -1360(%rbp,%rax,4)
	movl	-136(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -960(%rbp,%rax,4)
	movl	-120(%rbp), %edx
	movl	-100(%rbp), %eax
	cltq
	movl	%edx, -560(%rbp,%rax,4)
	addl	$1, -100(%rbp)
	jmp	.L645
.L674:
	nop
.L645:
	cmpl	$0, -100(%rbp)
	jg	.L672
.L644:
	addq	$1416, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE50:
	.size	mainQSort3, .-mainQSort3
	.section	.rodata
	.align 8
.LC115:
	.string	"        main sort initialise ...\n"
	.align 8
.LC116:
	.string	"        qsort [0x%x, 0x%x]   done %d   this %d\n"
	.align 8
.LC117:
	.string	"        %d pointers, %d sorted, %d scanned\n"
	.text
	.type	mainSort, @function
mainSort:
.LFB51:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$3480, %rsp
	movq	%rdi, -3432(%rbp)
	movq	%rsi, -3440(%rbp)
	movq	%rdx, -3448(%rbp)
	movq	%rcx, -3456(%rbp)
	movl	%r8d, -3460(%rbp)
	movl	%r9d, -3464(%rbp)
	movq	16(%rbp), %rax
	movq	%rax, -3472(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, -3464(%rbp)
	jle	.L681
	.cfi_offset 3, -24
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC115, %eax
	movq	%rdx, %rcx
	movl	$33, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L681:
	movl	$65536, -352(%rbp)
	jmp	.L682
.L683:
	movl	-352(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	$0, (%rax)
	subl	$1, -352(%rbp)
.L682:
	cmpl	$0, -352(%rbp)
	jns	.L683
	movq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	movl	%eax, -348(%rbp)
	movl	-3460(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -352(%rbp)
	jmp	.L684
.L685:
	movl	-352(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movw	$0, (%rax)
	movl	-348(%rbp), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	movl	-352(%rbp), %eax
	cltq
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -348(%rbp)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	movl	-352(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movw	$0, (%rax)
	movl	-348(%rbp), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	movl	-352(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -348(%rbp)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	movl	-352(%rbp), %eax
	cltq
	subq	$2, %rax
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movw	$0, (%rax)
	movl	-348(%rbp), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	movl	-352(%rbp), %eax
	cltq
	subq	$2, %rax
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -348(%rbp)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	movl	-352(%rbp), %eax
	cltq
	subq	$3, %rax
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movw	$0, (%rax)
	movl	-348(%rbp), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	movl	-352(%rbp), %eax
	cltq
	subq	$3, %rax
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -348(%rbp)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	subl	$4, -352(%rbp)
.L684:
	cmpl	$2, -352(%rbp)
	jg	.L685
	jmp	.L686
.L687:
	movl	-352(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movw	$0, (%rax)
	movl	-348(%rbp), %eax
	movl	%eax, %edx
	sarl	$8, %edx
	movl	-352(%rbp), %eax
	cltq
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -348(%rbp)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	subl	$1, -352(%rbp)
.L686:
	cmpl	$0, -352(%rbp)
	jns	.L687
	movl	$0, -352(%rbp)
	jmp	.L688
.L689:
	movl	-352(%rbp), %eax
	movl	-3460(%rbp), %edx
	addl	%edx, %eax
	cltq
	addq	-3440(%rbp), %rax
	movl	-352(%rbp), %edx
	movslq	%edx, %rdx
	addq	-3440(%rbp), %rdx
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movl	-352(%rbp), %eax
	movl	-3460(%rbp), %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movw	$0, (%rax)
	addl	$1, -352(%rbp)
.L688:
	cmpl	$33, -352(%rbp)
	jle	.L689
	cmpl	$3, -3464(%rbp)
	jle	.L690
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC111, %eax
	movq	%rdx, %rcx
	movl	$27, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L690:
	movl	$1, -352(%rbp)
	jmp	.L691
.L692:
	movl	-352(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-352(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-3456(%rbp), %rdx
	movl	(%rdx), %ecx
	movl	-352(%rbp), %edx
	movslq	%edx, %rdx
	subq	$1, %rdx
	salq	$2, %rdx
	addq	-3456(%rbp), %rdx
	movl	(%rdx), %edx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	addl	$1, -352(%rbp)
.L691:
	cmpl	$65536, -352(%rbp)
	jle	.L692
	movq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	movw	%ax, -294(%rbp)
	movl	-3460(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -352(%rbp)
	jmp	.L693
.L694:
	movzwl	-294(%rbp), %eax
	shrw	$8, %ax
	movl	%eax, %edx
	movl	-352(%rbp), %eax
	cltq
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movw	%ax, -294(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-348(%rbp), %edx
	movl	%edx, (%rax)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	-352(%rbp), %edx
	movl	%edx, (%rax)
	movzwl	-294(%rbp), %eax
	shrw	$8, %ax
	movl	%eax, %edx
	movl	-352(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movw	%ax, -294(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-348(%rbp), %edx
	movl	%edx, (%rax)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	-352(%rbp), %edx
	subl	$1, %edx
	movl	%edx, (%rax)
	movzwl	-294(%rbp), %eax
	shrw	$8, %ax
	movl	%eax, %edx
	movl	-352(%rbp), %eax
	cltq
	subq	$2, %rax
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movw	%ax, -294(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-348(%rbp), %edx
	movl	%edx, (%rax)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	-352(%rbp), %edx
	subl	$2, %edx
	movl	%edx, (%rax)
	movzwl	-294(%rbp), %eax
	shrw	$8, %ax
	movl	%eax, %edx
	movl	-352(%rbp), %eax
	cltq
	subq	$3, %rax
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movw	%ax, -294(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-348(%rbp), %edx
	movl	%edx, (%rax)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	-352(%rbp), %edx
	subl	$3, %edx
	movl	%edx, (%rax)
	subl	$4, -352(%rbp)
.L693:
	cmpl	$2, -352(%rbp)
	jg	.L694
	jmp	.L695
.L696:
	movzwl	-294(%rbp), %eax
	shrw	$8, %ax
	movl	%eax, %edx
	movl	-352(%rbp), %eax
	cltq
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movw	%ax, -294(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	movzwl	-294(%rbp), %eax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-348(%rbp), %edx
	movl	%edx, (%rax)
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	-352(%rbp), %edx
	movl	%edx, (%rax)
	subl	$1, -352(%rbp)
.L695:
	cmpl	$0, -352(%rbp)
	jns	.L696
	movl	$0, -352(%rbp)
	jmp	.L697
.L698:
	movl	-352(%rbp), %eax
	cltq
	movb	$0, -288(%rbp,%rax)
	movl	-352(%rbp), %eax
	cltq
	movl	-352(%rbp), %edx
	movl	%edx, -3424(%rbp,%rax,4)
	addl	$1, -352(%rbp)
.L697:
	cmpl	$255, -352(%rbp)
	jle	.L698
	movl	$1, -336(%rbp)
.L699:
	movl	-336(%rbp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	$1, %eax
	movl	%eax, -336(%rbp)
	cmpl	$256, -336(%rbp)
	jle	.L699
.L705:
	movl	-336(%rbp), %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -336(%rbp)
	movl	-336(%rbp), %eax
	movl	%eax, -352(%rbp)
	jmp	.L700
.L704:
	movl	-352(%rbp), %eax
	cltq
	movl	-3424(%rbp,%rax,4), %eax
	movl	%eax, -328(%rbp)
	movl	-352(%rbp), %eax
	movl	%eax, -348(%rbp)
	jmp	.L701
.L703:
	movl	-336(%rbp), %eax
	movl	-348(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cltq
	movl	-3424(%rbp,%rax,4), %edx
	movl	-348(%rbp), %eax
	cltq
	movl	%edx, -3424(%rbp,%rax,4)
	movl	-336(%rbp), %eax
	subl	%eax, -348(%rbp)
	movl	-336(%rbp), %eax
	subl	$1, %eax
	cmpl	-348(%rbp), %eax
	jge	.L736
.L701:
	movl	-336(%rbp), %eax
	movl	-348(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cltq
	movl	-3424(%rbp,%rax,4), %eax
	addl	$1, %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	movl	-336(%rbp), %eax
	movl	-348(%rbp), %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cltq
	movl	-3424(%rbp,%rax,4), %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	-328(%rbp), %eax
	addl	$1, %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %edx
	movl	-328(%rbp), %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	%eax, %ecx
	ja	.L703
	jmp	.L702
.L736:
	nop
.L702:
	movl	-348(%rbp), %eax
	cltq
	movl	-328(%rbp), %edx
	movl	%edx, -3424(%rbp,%rax,4)
	addl	$1, -352(%rbp)
.L700:
	cmpl	$255, -352(%rbp)
	jle	.L704
	cmpl	$1, -336(%rbp)
	jne	.L705
	movl	$0, -340(%rbp)
	movl	$0, -352(%rbp)
	jmp	.L706
.L734:
	movl	-352(%rbp), %eax
	cltq
	movl	-3424(%rbp,%rax,4), %eax
	movl	%eax, -324(%rbp)
	movl	$0, -348(%rbp)
	jmp	.L707
.L712:
	movl	-348(%rbp), %eax
	cmpl	-324(%rbp), %eax
	je	.L708
	movl	-324(%rbp), %eax
	sall	$8, %eax
	addl	-348(%rbp), %eax
	movl	%eax, -320(%rbp)
	movl	-320(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$2097152, %eax
	testl	%eax, %eax
	jne	.L709
	movl	-320(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$-2097153, %eax
	movl	%eax, -316(%rbp)
	movl	-320(%rbp), %eax
	cltq
	addq	$1, %rax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$-2097153, %eax
	subl	$1, %eax
	movl	%eax, -312(%rbp)
	movl	-312(%rbp), %eax
	cmpl	-316(%rbp), %eax
	jle	.L709
	cmpl	$3, -3464(%rbp)
	jle	.L710
	movl	-316(%rbp), %eax
	movl	-312(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	leal	1(%rax), %r8d
	movl	$.LC116, %esi
	movq	stderr(%rip), %rax
	movl	-340(%rbp), %edi
	movl	-348(%rbp), %ecx
	movl	-324(%rbp), %edx
	movl	%r8d, %r9d
	movl	%edi, %r8d
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L710:
	movl	-312(%rbp), %r9d
	movl	-316(%rbp), %r8d
	movl	-3460(%rbp), %ecx
	movq	-3448(%rbp), %rdx
	movq	-3440(%rbp), %rsi
	movq	-3432(%rbp), %rax
	movq	-3472(%rbp), %rdi
	movq	%rdi, 8(%rsp)
	movl	$2, (%rsp)
	movq	%rax, %rdi
	call	mainQSort3
	movl	-316(%rbp), %eax
	movl	-312(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	addl	$1, %eax
	addl	%eax, -340(%rbp)
	movq	-3472(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	js	.L737
.L709:
	movl	-320(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-320(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-3456(%rbp), %rdx
	movl	(%rdx), %edx
	orl	$2097152, %edx
	movl	%edx, (%rax)
.L708:
	addl	$1, -348(%rbp)
.L707:
	cmpl	$255, -348(%rbp)
	jle	.L712
	movl	-324(%rbp), %eax
	cltq
	movzbl	-288(%rbp,%rax), %eax
	testb	%al, %al
	je	.L713
	movl	$1006, %edi
	call	BZ2_bz__AssertH__fail
.L713:
	movl	$0, -348(%rbp)
	jmp	.L714
.L715:
	movl	-348(%rbp), %eax
	sall	$8, %eax
	addl	-324(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-2097153, %edx
	movl	-348(%rbp), %eax
	cltq
	movl	%edx, -2400(%rbp,%rax,4)
	movl	-348(%rbp), %eax
	sall	$8, %eax
	addl	-324(%rbp), %eax
	cltq
	addq	$1, %rax
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$-2097153, %eax
	subl	$1, %eax
	movl	%eax, %edx
	movl	-348(%rbp), %eax
	cltq
	movl	%edx, -1376(%rbp,%rax,4)
	addl	$1, -348(%rbp)
.L714:
	cmpl	$255, -348(%rbp)
	jle	.L715
	movl	-324(%rbp), %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$-2097153, %eax
	movl	%eax, -348(%rbp)
	jmp	.L716
.L719:
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -344(%rbp)
	cmpl	$0, -344(%rbp)
	jns	.L717
	movl	-3460(%rbp), %eax
	addl	%eax, -344(%rbp)
.L717:
	movl	-344(%rbp), %eax
	cltq
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -289(%rbp)
	movzbl	-289(%rbp), %eax
	cltq
	movzbl	-288(%rbp,%rax), %eax
	testb	%al, %al
	jne	.L718
	movzbl	-289(%rbp), %eax
	leaq	-2400(%rbp), %rdx
	movslq	%eax, %rcx
	salq	$2, %rcx
	leaq	(%rdx,%rcx), %rsi
	cltq
	movl	-2400(%rbp,%rax,4), %eax
	movslq	%eax, %rdx
	salq	$2, %rdx
	addq	-3432(%rbp), %rdx
	movl	-344(%rbp), %ecx
	movl	%ecx, (%rdx)
	addl	$1, %eax
	movl	%eax, (%rsi)
.L718:
	addl	$1, -348(%rbp)
.L716:
	movl	-324(%rbp), %eax
	cltq
	movl	-2400(%rbp,%rax,4), %eax
	cmpl	-348(%rbp), %eax
	jg	.L719
	movl	-324(%rbp), %eax
	addl	$1, %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$-2097153, %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	jmp	.L720
.L723:
	movl	-348(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -344(%rbp)
	cmpl	$0, -344(%rbp)
	jns	.L721
	movl	-3460(%rbp), %eax
	addl	%eax, -344(%rbp)
.L721:
	movl	-344(%rbp), %eax
	cltq
	addq	-3440(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -289(%rbp)
	movzbl	-289(%rbp), %eax
	cltq
	movzbl	-288(%rbp,%rax), %eax
	testb	%al, %al
	jne	.L722
	movzbl	-289(%rbp), %eax
	leaq	-1376(%rbp), %rdx
	movslq	%eax, %rcx
	salq	$2, %rcx
	leaq	(%rdx,%rcx), %rsi
	cltq
	movl	-1376(%rbp,%rax,4), %eax
	movslq	%eax, %rdx
	salq	$2, %rdx
	addq	-3432(%rbp), %rdx
	movl	-344(%rbp), %ecx
	movl	%ecx, (%rdx)
	subl	$1, %eax
	movl	%eax, (%rsi)
.L722:
	subl	$1, -348(%rbp)
.L720:
	movl	-324(%rbp), %eax
	cltq
	movl	-1376(%rbp,%rax,4), %eax
	cmpl	-348(%rbp), %eax
	jl	.L723
	movl	-324(%rbp), %eax
	cltq
	movl	-2400(%rbp,%rax,4), %eax
	leal	-1(%rax), %edx
	movl	-324(%rbp), %eax
	cltq
	movl	-1376(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	je	.L724
	movl	-324(%rbp), %eax
	cltq
	movl	-2400(%rbp,%rax,4), %eax
	testl	%eax, %eax
	jne	.L725
	movl	-324(%rbp), %eax
	cltq
	movl	-1376(%rbp,%rax,4), %eax
	movl	-3460(%rbp), %edx
	subl	$1, %edx
	cmpl	%edx, %eax
	je	.L724
.L725:
	movl	$1007, %edi
	call	BZ2_bz__AssertH__fail
.L724:
	movl	$0, -348(%rbp)
	jmp	.L726
.L727:
	movl	-348(%rbp), %eax
	sall	$8, %eax
	addl	-324(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	-348(%rbp), %edx
	sall	$8, %edx
	addl	-324(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-3456(%rbp), %rdx
	movl	(%rdx), %edx
	orl	$2097152, %edx
	movl	%edx, (%rax)
	addl	$1, -348(%rbp)
.L726:
	cmpl	$255, -348(%rbp)
	jle	.L727
	movl	-324(%rbp), %eax
	cltq
	movb	$1, -288(%rbp,%rax)
	cmpl	$254, -352(%rbp)
	jg	.L728
	movl	-324(%rbp), %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	andl	$-2097153, %eax
	movl	%eax, -308(%rbp)
	movl	-324(%rbp), %eax
	addl	$1, %eax
	sall	$8, %eax
	cltq
	salq	$2, %rax
	addq	-3456(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-2097153, %edx
	movl	-308(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -304(%rbp)
	movl	$0, -332(%rbp)
	jmp	.L729
.L730:
	addl	$1, -332(%rbp)
.L729:
	movl	-332(%rbp), %eax
	movl	-304(%rbp), %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	cmpl	$65534, %eax
	jg	.L730
	movl	-304(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -348(%rbp)
	jmp	.L731
.L733:
	movl	-348(%rbp), %eax
	movl	-308(%rbp), %edx
	addl	%edx, %eax
	cltq
	salq	$2, %rax
	addq	-3432(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -300(%rbp)
	movl	-332(%rbp), %eax
	movl	-348(%rbp), %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	movw	%ax, -292(%rbp)
	movl	-300(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movzwl	-292(%rbp), %edx
	movw	%dx, (%rax)
	cmpl	$33, -300(%rbp)
	jg	.L732
	movl	-3460(%rbp), %eax
	movl	-300(%rbp), %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
	addq	-3448(%rbp), %rax
	movzwl	-292(%rbp), %edx
	movw	%dx, (%rax)
.L732:
	subl	$1, -348(%rbp)
.L731:
	cmpl	$0, -348(%rbp)
	jns	.L733
	movl	-304(%rbp), %eax
	leal	-1(%rax), %edx
	movl	-332(%rbp), %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	cmpl	$65535, %eax
	jle	.L728
	movl	$1002, %edi
	call	BZ2_bz__AssertH__fail
.L728:
	addl	$1, -352(%rbp)
.L706:
	cmpl	$255, -352(%rbp)
	jle	.L734
	cmpl	$3, -3464(%rbp)
	jle	.L680
	movl	-340(%rbp), %eax
	movl	-3460(%rbp), %edx
	movl	%edx, %edi
	subl	%eax, %edi
	movl	$.LC117, %esi
	movq	stderr(%rip), %rax
	movl	-340(%rbp), %ecx
	movl	-3460(%rbp), %edx
	movl	%edi, %r8d
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L680
.L737:
	nop
.L680:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L735
	call	__stack_chk_fail
.L735:
	addq	$3480, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE51:
	.size	mainSort, .-mainSort
	.section	.rodata
	.align 8
.LC119:
	.string	"      %d work, %d block, ratio %5.2f\n"
	.align 8
.LC120:
	.string	"    too repetitive; using fallback sorting algorithm\n"
	.text
	.globl	BZ2_blockSort
	.type	BZ2_blockSort, @function
BZ2_blockSort:
.LFB52:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	56(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -40(%rbp)
	movq	-72(%rbp), %rax
	movl	108(%rax), %eax
	movl	%eax, -12(%rbp)
	movq	-72(%rbp), %rax
	movl	656(%rax), %eax
	movl	%eax, -8(%rbp)
	movq	-72(%rbp), %rax
	movl	88(%rax), %eax
	movl	%eax, -20(%rbp)
	cmpl	$9999, -12(%rbp)
	jg	.L739
	movq	-72(%rbp), %rax
	movq	32(%rax), %rsi
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movl	-8(%rbp), %edi
	movl	-12(%rbp), %ecx
	movq	-40(%rbp), %rdx
	movl	%edi, %r8d
	movq	%rax, %rdi
	call	fallbackSort
	jmp	.L740
.L739:
	movl	-12(%rbp), %eax
	addl	$34, %eax
	movl	%eax, -16(%rbp)
	movl	-16(%rbp), %eax
	andl	$1, %eax
	testb	%al, %al
	je	.L741
	addl	$1, -16(%rbp)
.L741:
	movl	-16(%rbp), %eax
	cltq
	addq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	cmpl	$0, -20(%rbp)
	jg	.L742
	movl	$1, -20(%rbp)
.L742:
	cmpl	$100, -20(%rbp)
	jle	.L743
	movl	$100, -20(%rbp)
.L743:
	movl	-20(%rbp), %eax
	leal	-1(%rax), %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	imull	-12(%rbp), %eax
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, -24(%rbp)
	movl	-8(%rbp), %r9d
	movl	-12(%rbp), %r8d
	movq	-40(%rbp), %rcx
	movq	-32(%rbp), %rdx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rax
	leaq	-24(%rbp), %rdi
	movq	%rdi, (%rsp)
	movq	%rax, %rdi
	call	mainSort
	cmpl	$2, -8(%rbp)
	jle	.L744
	movl	-24(%rbp), %eax
	movl	-4(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cvtsi2ss	%eax, %xmm1
	cmpl	$0, -12(%rbp)
	je	.L745
	cvtsi2ss	-12(%rbp), %xmm0
	jmp	.L746
.L745:
	movss	.LC118(%rip), %xmm0
.L746:
	movaps	%xmm1, %xmm2
	divss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movl	-24(%rbp), %eax
	movl	-4(%rbp), %edx
	movl	%edx, %edi
	subl	%eax, %edi
	movl	$.LC119, %esi
	movq	stderr(%rip), %rax
	movl	-12(%rbp), %edx
	movl	%edx, %ecx
	movl	%edi, %edx
	movq	%rax, %rdi
	movl	$1, %eax
	call	fprintf
.L744:
	movl	-24(%rbp), %eax
	testl	%eax, %eax
	jns	.L740
	cmpl	$1, -8(%rbp)
	jle	.L747
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC120, %eax
	movq	%rdx, %rcx
	movl	$53, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L747:
	movq	-72(%rbp), %rax
	movq	32(%rax), %rsi
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movl	-8(%rbp), %edi
	movl	-12(%rbp), %ecx
	movq	-40(%rbp), %rdx
	movl	%edi, %r8d
	movq	%rax, %rdi
	call	fallbackSort
.L740:
	movq	-72(%rbp), %rax
	movl	$-1, 48(%rax)
	movl	$0, -16(%rbp)
	jmp	.L748
.L751:
	movl	-16(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-56(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L749
	movq	-72(%rbp), %rax
	movl	-16(%rbp), %edx
	movl	%edx, 48(%rax)
	jmp	.L750
.L749:
	addl	$1, -16(%rbp)
.L748:
	movq	-72(%rbp), %rax
	movl	108(%rax), %eax
	cmpl	-16(%rbp), %eax
	jg	.L751
.L750:
	movq	-72(%rbp), %rax
	movl	48(%rax), %eax
	cmpl	$-1, %eax
	jne	.L738
	movl	$1003, %edi
	call	BZ2_bz__AssertH__fail
.L738:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE52:
	.size	BZ2_blockSort, .-BZ2_blockSort
	.globl	BZ2_hbMakeCodeLengths
	.type	BZ2_hbMakeCodeLengths, @function
BZ2_hbMakeCodeLengths:
.LFB53:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$5280, %rsp
	movq	%rdi, -5256(%rbp)
	movq	%rsi, -5264(%rbp)
	movl	%edx, -5268(%rbp)
	movl	%ecx, -5272(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L754
.L757:
	movl	-64(%rbp), %eax
	leal	1(%rax), %edx
	movl	-64(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-5264(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L755
	movl	-64(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-5264(%rbp), %rax
	movl	(%rax), %eax
	sall	$8, %eax
	jmp	.L756
.L755:
	movl	$256, %eax
.L756:
	movslq	%edx, %rdx
	movl	%eax, -5248(%rbp,%rdx,4)
	addl	$1, -64(%rbp)
.L754:
	movl	-64(%rbp), %eax
	cmpl	-5268(%rbp), %eax
	jl	.L757
.L783:
	movl	-5268(%rbp), %eax
	movl	%eax, -72(%rbp)
	movl	$0, -68(%rbp)
	movl	$0, -1120(%rbp)
	movl	$0, -5248(%rbp)
	movl	$-2, -3184(%rbp)
	movl	$1, -64(%rbp)
	jmp	.L758
.L761:
	movl	-64(%rbp), %eax
	cltq
	movl	$-1, -3184(%rbp,%rax,4)
	addl	$1, -68(%rbp)
	movl	-68(%rbp), %eax
	cltq
	movl	-64(%rbp), %edx
	movl	%edx, -1120(%rbp,%rax,4)
	movl	-68(%rbp), %eax
	movl	%eax, -52(%rbp)
	movl	-52(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	movl	%eax, -28(%rbp)
	jmp	.L759
.L760:
	movl	-52(%rbp), %eax
	sarl	%eax
	cltq
	movl	-1120(%rbp,%rax,4), %edx
	movl	-52(%rbp), %eax
	cltq
	movl	%edx, -1120(%rbp,%rax,4)
	sarl	-52(%rbp)
.L759:
	movl	-28(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %edx
	movl	-52(%rbp), %eax
	sarl	%eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jl	.L760
	movl	-52(%rbp), %eax
	cltq
	movl	-28(%rbp), %edx
	movl	%edx, -1120(%rbp,%rax,4)
	addl	$1, -64(%rbp)
.L758:
	movl	-64(%rbp), %eax
	cmpl	-5268(%rbp), %eax
	jle	.L761
	cmpl	$259, -68(%rbp)
	jle	.L763
	movl	$2001, %edi
	call	BZ2_bz__AssertH__fail
	jmp	.L763
.L772:
	movl	-1116(%rbp), %eax
	movl	%eax, -24(%rbp)
	movl	-68(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	movl	%eax, -1116(%rbp)
	subl	$1, -68(%rbp)
	movl	$1, -48(%rbp)
	movl	-48(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	movl	%eax, -20(%rbp)
.L766:
	movl	-48(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, -44(%rbp)
	movl	-44(%rbp), %eax
	cmpl	-68(%rbp), %eax
	jg	.L764
	movl	-44(%rbp), %eax
	cmpl	-68(%rbp), %eax
	jge	.L765
	movl	-44(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %edx
	movl	-44(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jge	.L765
	addl	$1, -44(%rbp)
.L765:
	movl	-20(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %edx
	movl	-44(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jl	.L764
	movl	-44(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %edx
	movl	-48(%rbp), %eax
	cltq
	movl	%edx, -1120(%rbp,%rax,4)
	movl	-44(%rbp), %eax
	movl	%eax, -48(%rbp)
	jmp	.L766
.L764:
	movl	-48(%rbp), %eax
	cltq
	movl	-20(%rbp), %edx
	movl	%edx, -1120(%rbp,%rax,4)
	movl	-1116(%rbp), %eax
	movl	%eax, -16(%rbp)
	movl	-68(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	movl	%eax, -1116(%rbp)
	subl	$1, -68(%rbp)
	movl	$1, -40(%rbp)
	movl	-40(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	movl	%eax, -12(%rbp)
.L769:
	movl	-40(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	cmpl	-68(%rbp), %eax
	jg	.L767
	movl	-36(%rbp), %eax
	cmpl	-68(%rbp), %eax
	jge	.L768
	movl	-36(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %edx
	movl	-36(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jge	.L768
	addl	$1, -36(%rbp)
.L768:
	movl	-12(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %edx
	movl	-36(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jl	.L767
	movl	-36(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %edx
	movl	-40(%rbp), %eax
	cltq
	movl	%edx, -1120(%rbp,%rax,4)
	movl	-36(%rbp), %eax
	movl	%eax, -40(%rbp)
	jmp	.L769
.L767:
	movl	-40(%rbp), %eax
	cltq
	movl	-12(%rbp), %edx
	movl	%edx, -1120(%rbp,%rax,4)
	addl	$1, -72(%rbp)
	movl	-16(%rbp), %eax
	cltq
	movl	-72(%rbp), %edx
	movl	%edx, -3184(%rbp,%rax,4)
	movl	-16(%rbp), %eax
	cltq
	movl	-3184(%rbp,%rax,4), %edx
	movl	-24(%rbp), %eax
	cltq
	movl	%edx, -3184(%rbp,%rax,4)
	movl	-24(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	movl	%eax, %edx
	movb	$0, %dl
	movl	-16(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	movb	$0, %al
	leal	(%rdx,%rax), %ecx
	movl	-16(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	movzbl	%al, %edx
	movl	-24(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	andl	$255, %eax
	cmpl	%eax, %edx
	cmovge	%edx, %eax
	addl	$1, %eax
	orl	%ecx, %eax
	movl	%eax, %edx
	movl	-72(%rbp), %eax
	cltq
	movl	%edx, -5248(%rbp,%rax,4)
	movl	-72(%rbp), %eax
	cltq
	movl	$-1, -3184(%rbp,%rax,4)
	addl	$1, -68(%rbp)
	movl	-68(%rbp), %eax
	cltq
	movl	-72(%rbp), %edx
	movl	%edx, -1120(%rbp,%rax,4)
	movl	-68(%rbp), %eax
	movl	%eax, -32(%rbp)
	movl	-32(%rbp), %eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	movl	%eax, -8(%rbp)
	jmp	.L770
.L771:
	movl	-32(%rbp), %eax
	sarl	%eax
	cltq
	movl	-1120(%rbp,%rax,4), %edx
	movl	-32(%rbp), %eax
	cltq
	movl	%edx, -1120(%rbp,%rax,4)
	sarl	-32(%rbp)
.L770:
	movl	-8(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %edx
	movl	-32(%rbp), %eax
	sarl	%eax
	cltq
	movl	-1120(%rbp,%rax,4), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jl	.L771
	movl	-32(%rbp), %eax
	cltq
	movl	-8(%rbp), %edx
	movl	%edx, -1120(%rbp,%rax,4)
.L763:
	cmpl	$1, -68(%rbp)
	jg	.L772
	cmpl	$515, -72(%rbp)
	jle	.L773
	movl	$2002, %edi
	call	BZ2_bz__AssertH__fail
.L773:
	movb	$0, -1(%rbp)
	movl	$1, -64(%rbp)
	jmp	.L774
.L778:
	movl	$0, -60(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -56(%rbp)
	jmp	.L775
.L776:
	movl	-56(%rbp), %eax
	cltq
	movl	-3184(%rbp,%rax,4), %eax
	movl	%eax, -56(%rbp)
	addl	$1, -60(%rbp)
.L775:
	movl	-56(%rbp), %eax
	cltq
	movl	-3184(%rbp,%rax,4), %eax
	testl	%eax, %eax
	jns	.L776
	movl	-64(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-5256(%rbp), %rax
	movl	-60(%rbp), %edx
	movb	%dl, (%rax)
	movl	-60(%rbp), %eax
	cmpl	-5272(%rbp), %eax
	jle	.L777
	movb	$1, -1(%rbp)
.L777:
	addl	$1, -64(%rbp)
.L774:
	movl	-64(%rbp), %eax
	cmpl	-5268(%rbp), %eax
	jle	.L778
	cmpb	$0, -1(%rbp)
	je	.L785
.L779:
	movl	$1, -64(%rbp)
	jmp	.L781
.L782:
	movl	-64(%rbp), %eax
	cltq
	movl	-5248(%rbp,%rax,4), %eax
	sarl	$8, %eax
	movl	%eax, -60(%rbp)
	movl	-60(%rbp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	movl	-60(%rbp), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movl	-64(%rbp), %eax
	cltq
	movl	%edx, -5248(%rbp,%rax,4)
	addl	$1, -64(%rbp)
.L781:
	movl	-64(%rbp), %eax
	cmpl	-5268(%rbp), %eax
	jle	.L782
	jmp	.L783
.L785:
	nop
.L784:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE53:
	.size	BZ2_hbMakeCodeLengths, .-BZ2_hbMakeCodeLengths
	.globl	BZ2_hbAssignCodes
	.type	BZ2_hbAssignCodes, @function
BZ2_hbAssignCodes:
.LFB54:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	movl	%ecx, -40(%rbp)
	movl	%r8d, -44(%rbp)
	movl	$0, -8(%rbp)
	movl	-36(%rbp), %eax
	movl	%eax, -12(%rbp)
	jmp	.L787
.L791:
	movl	$0, -4(%rbp)
	jmp	.L788
.L790:
	movl	-4(%rbp), %eax
	cltq
	addq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	cmpl	-12(%rbp), %eax
	jne	.L789
	movl	-4(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movl	-8(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -8(%rbp)
.L789:
	addl	$1, -4(%rbp)
.L788:
	movl	-4(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jl	.L790
	sall	-8(%rbp)
	addl	$1, -12(%rbp)
.L787:
	movl	-12(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jle	.L791
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE54:
	.size	BZ2_hbAssignCodes, .-BZ2_hbAssignCodes
	.globl	BZ2_hbCreateDecodeTables
	.type	BZ2_hbCreateDecodeTables, @function
BZ2_hbCreateDecodeTables:
.LFB55:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rcx, -56(%rbp)
	movl	%r8d, -60(%rbp)
	movl	%r9d, -64(%rbp)
	movl	$0, -24(%rbp)
	movl	-60(%rbp), %eax
	movl	%eax, -20(%rbp)
	jmp	.L793
	.cfi_offset 3, -24
.L797:
	movl	$0, -16(%rbp)
	jmp	.L794
.L796:
	movl	-16(%rbp), %eax
	cltq
	addq	-56(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	cmpl	-20(%rbp), %eax
	jne	.L795
	movl	-24(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movl	-16(%rbp), %edx
	movl	%edx, (%rax)
	addl	$1, -24(%rbp)
.L795:
	addl	$1, -16(%rbp)
.L794:
	movl	-16(%rbp), %eax
	cmpl	16(%rbp), %eax
	jl	.L796
	addl	$1, -20(%rbp)
.L793:
	movl	-20(%rbp), %eax
	cmpl	-64(%rbp), %eax
	jle	.L797
	movl	$0, -20(%rbp)
	jmp	.L798
.L799:
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	$0, (%rax)
	addl	$1, -20(%rbp)
.L798:
	cmpl	$22, -20(%rbp)
	jle	.L799
	movl	$0, -20(%rbp)
	jmp	.L800
.L801:
	movl	-20(%rbp), %eax
	cltq
	addq	-56(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	addq	$1, %rax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	(%rax), %edx
	addl	$1, %edx
	movl	%edx, (%rax)
	addl	$1, -20(%rbp)
.L800:
	movl	-20(%rbp), %eax
	cmpl	16(%rbp), %eax
	jl	.L801
	movl	$1, -20(%rbp)
	jmp	.L802
.L803:
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	-20(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-40(%rbp), %rdx
	movl	(%rdx), %ecx
	movl	-20(%rbp), %edx
	movslq	%edx, %rdx
	subq	$1, %rdx
	salq	$2, %rdx
	addq	-40(%rbp), %rdx
	movl	(%rdx), %edx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	addl	$1, -20(%rbp)
.L802:
	cmpl	$22, -20(%rbp)
	jle	.L803
	movl	$0, -20(%rbp)
	jmp	.L804
.L805:
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	$0, (%rax)
	addl	$1, -20(%rbp)
.L804:
	cmpl	$22, -20(%rbp)
	jle	.L805
	movl	$0, -12(%rbp)
	movl	-60(%rbp), %eax
	movl	%eax, -20(%rbp)
	jmp	.L806
.L807:
	movl	-20(%rbp), %eax
	cltq
	addq	$1, %rax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	(%rax), %edx
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	(%rax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, -12(%rbp)
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	-12(%rbp), %edx
	subl	$1, %edx
	movl	%edx, (%rax)
	sall	-12(%rbp)
	addl	$1, -20(%rbp)
.L806:
	movl	-20(%rbp), %eax
	cmpl	-64(%rbp), %eax
	jle	.L807
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	jmp	.L808
.L809:
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	-20(%rbp), %edx
	movslq	%edx, %rdx
	subq	$1, %rdx
	salq	$2, %rdx
	addq	-32(%rbp), %rdx
	movl	(%rdx), %edx
	addl	$1, %edx
	leal	(%rdx,%rdx), %ecx
	movl	-20(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-40(%rbp), %rdx
	movl	(%rdx), %edx
	movl	%ecx, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	movl	%edx, (%rax)
	addl	$1, -20(%rbp)
.L808:
	movl	-20(%rbp), %eax
	cmpl	-64(%rbp), %eax
	jle	.L809
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE55:
	.size	BZ2_hbCreateDecodeTables, .-BZ2_hbCreateDecodeTables
	.globl	BZ2_crc32Table
	.data
	.align 32
	.type	BZ2_crc32Table, @object
	.size	BZ2_crc32Table, 1024
BZ2_crc32Table:
	.long	0
	.long	79764919
	.long	159529838
	.long	222504665
	.long	319059676
	.long	398814059
	.long	445009330
	.long	507990021
	.long	638119352
	.long	583659535
	.long	797628118
	.long	726387553
	.long	890018660
	.long	835552979
	.long	1015980042
	.long	944750013
	.long	1276238704
	.long	1221641927
	.long	1167319070
	.long	1095957929
	.long	1595256236
	.long	1540665371
	.long	1452775106
	.long	1381403509
	.long	1780037320
	.long	1859660671
	.long	1671105958
	.long	1733955601
	.long	2031960084
	.long	2111593891
	.long	1889500026
	.long	1952343757
	.long	-1742489888
	.long	-1662866601
	.long	-1851683442
	.long	-1788833735
	.long	-1960329156
	.long	-1880695413
	.long	-2103051438
	.long	-2040207643
	.long	-1104454824
	.long	-1159051537
	.long	-1213636554
	.long	-1284997759
	.long	-1389417084
	.long	-1444007885
	.long	-1532160278
	.long	-1603531939
	.long	-734892656
	.long	-789352409
	.long	-575645954
	.long	-646886583
	.long	-952755380
	.long	-1007220997
	.long	-827056094
	.long	-898286187
	.long	-231047128
	.long	-151282273
	.long	-71779514
	.long	-8804623
	.long	-515967244
	.long	-436212925
	.long	-390279782
	.long	-327299027
	.long	881225847
	.long	809987520
	.long	1023691545
	.long	969234094
	.long	662832811
	.long	591600412
	.long	771767749
	.long	717299826
	.long	311336399
	.long	374308984
	.long	453813921
	.long	533576470
	.long	25881363
	.long	88864420
	.long	134795389
	.long	214552010
	.long	2023205639
	.long	2086057648
	.long	1897238633
	.long	1976864222
	.long	1804852699
	.long	1867694188
	.long	1645340341
	.long	1724971778
	.long	1587496639
	.long	1516133128
	.long	1461550545
	.long	1406951526
	.long	1302016099
	.long	1230646740
	.long	1142491917
	.long	1087903418
	.long	-1398421865
	.long	-1469785312
	.long	-1524105735
	.long	-1578704818
	.long	-1079922613
	.long	-1151291908
	.long	-1239184603
	.long	-1293773166
	.long	-1968362705
	.long	-1905510760
	.long	-2094067647
	.long	-2014441994
	.long	-1716953613
	.long	-1654112188
	.long	-1876203875
	.long	-1796572374
	.long	-525066777
	.long	-462094256
	.long	-382327159
	.long	-302564546
	.long	-206542021
	.long	-143559028
	.long	-97365931
	.long	-17609246
	.long	-960696225
	.long	-1031934488
	.long	-817968335
	.long	-872425850
	.long	-709327229
	.long	-780559564
	.long	-600130067
	.long	-654598054
	.long	1762451694
	.long	1842216281
	.long	1619975040
	.long	1682949687
	.long	2047383090
	.long	2127137669
	.long	1938468188
	.long	2001449195
	.long	1325665622
	.long	1271206113
	.long	1183200824
	.long	1111960463
	.long	1543535498
	.long	1489069629
	.long	1434599652
	.long	1363369299
	.long	622672798
	.long	568075817
	.long	748617968
	.long	677256519
	.long	907627842
	.long	853037301
	.long	1067152940
	.long	995781531
	.long	51762726
	.long	131386257
	.long	177728840
	.long	240578815
	.long	269590778
	.long	349224269
	.long	429104020
	.long	491947555
	.long	-248556018
	.long	-168932423
	.long	-122852000
	.long	-60002089
	.long	-500490030
	.long	-420856475
	.long	-341238852
	.long	-278395381
	.long	-685261898
	.long	-739858943
	.long	-559578920
	.long	-630940305
	.long	-1004286614
	.long	-1058877219
	.long	-845023740
	.long	-916395085
	.long	-1119974018
	.long	-1174433591
	.long	-1262701040
	.long	-1333941337
	.long	-1371866206
	.long	-1426332139
	.long	-1481064244
	.long	-1552294533
	.long	-1690935098
	.long	-1611170447
	.long	-1833673816
	.long	-1770699233
	.long	-2009983462
	.long	-1930228819
	.long	-2119160460
	.long	-2056179517
	.long	1569362073
	.long	1498123566
	.long	1409854455
	.long	1355396672
	.long	1317987909
	.long	1246755826
	.long	1192025387
	.long	1137557660
	.long	2072149281
	.long	2135122070
	.long	1912620623
	.long	1992383480
	.long	1753615357
	.long	1816598090
	.long	1627664531
	.long	1707420964
	.long	295390185
	.long	358241886
	.long	404320391
	.long	483945776
	.long	43990325
	.long	106832002
	.long	186451547
	.long	266083308
	.long	932423249
	.long	861060070
	.long	1041341759
	.long	986742920
	.long	613929101
	.long	542559546
	.long	756411363
	.long	701822548
	.long	-978770311
	.long	-1050133554
	.long	-869589737
	.long	-924188512
	.long	-693284699
	.long	-764654318
	.long	-550540341
	.long	-605129092
	.long	-475935807
	.long	-413084042
	.long	-366743377
	.long	-287118056
	.long	-257573603
	.long	-194731862
	.long	-114850189
	.long	-35218492
	.long	-1984365303
	.long	-1921392450
	.long	-2143631769
	.long	-2063868976
	.long	-1698919467
	.long	-1635936670
	.long	-1824608069
	.long	-1744851700
	.long	-1347415887
	.long	-1418654458
	.long	-1506661409
	.long	-1561119128
	.long	-1129027987
	.long	-1200260134
	.long	-1254728445
	.long	-1309196108
	.globl	BZ2_rNums
	.align 32
	.type	BZ2_rNums, @object
	.size	BZ2_rNums, 2048
BZ2_rNums:
	.long	619
	.long	720
	.long	127
	.long	481
	.long	931
	.long	816
	.long	813
	.long	233
	.long	566
	.long	247
	.long	985
	.long	724
	.long	205
	.long	454
	.long	863
	.long	491
	.long	741
	.long	242
	.long	949
	.long	214
	.long	733
	.long	859
	.long	335
	.long	708
	.long	621
	.long	574
	.long	73
	.long	654
	.long	730
	.long	472
	.long	419
	.long	436
	.long	278
	.long	496
	.long	867
	.long	210
	.long	399
	.long	680
	.long	480
	.long	51
	.long	878
	.long	465
	.long	811
	.long	169
	.long	869
	.long	675
	.long	611
	.long	697
	.long	867
	.long	561
	.long	862
	.long	687
	.long	507
	.long	283
	.long	482
	.long	129
	.long	807
	.long	591
	.long	733
	.long	623
	.long	150
	.long	238
	.long	59
	.long	379
	.long	684
	.long	877
	.long	625
	.long	169
	.long	643
	.long	105
	.long	170
	.long	607
	.long	520
	.long	932
	.long	727
	.long	476
	.long	693
	.long	425
	.long	174
	.long	647
	.long	73
	.long	122
	.long	335
	.long	530
	.long	442
	.long	853
	.long	695
	.long	249
	.long	445
	.long	515
	.long	909
	.long	545
	.long	703
	.long	919
	.long	874
	.long	474
	.long	882
	.long	500
	.long	594
	.long	612
	.long	641
	.long	801
	.long	220
	.long	162
	.long	819
	.long	984
	.long	589
	.long	513
	.long	495
	.long	799
	.long	161
	.long	604
	.long	958
	.long	533
	.long	221
	.long	400
	.long	386
	.long	867
	.long	600
	.long	782
	.long	382
	.long	596
	.long	414
	.long	171
	.long	516
	.long	375
	.long	682
	.long	485
	.long	911
	.long	276
	.long	98
	.long	553
	.long	163
	.long	354
	.long	666
	.long	933
	.long	424
	.long	341
	.long	533
	.long	870
	.long	227
	.long	730
	.long	475
	.long	186
	.long	263
	.long	647
	.long	537
	.long	686
	.long	600
	.long	224
	.long	469
	.long	68
	.long	770
	.long	919
	.long	190
	.long	373
	.long	294
	.long	822
	.long	808
	.long	206
	.long	184
	.long	943
	.long	795
	.long	384
	.long	383
	.long	461
	.long	404
	.long	758
	.long	839
	.long	887
	.long	715
	.long	67
	.long	618
	.long	276
	.long	204
	.long	918
	.long	873
	.long	777
	.long	604
	.long	560
	.long	951
	.long	160
	.long	578
	.long	722
	.long	79
	.long	804
	.long	96
	.long	409
	.long	713
	.long	940
	.long	652
	.long	934
	.long	970
	.long	447
	.long	318
	.long	353
	.long	859
	.long	672
	.long	112
	.long	785
	.long	645
	.long	863
	.long	803
	.long	350
	.long	139
	.long	93
	.long	354
	.long	99
	.long	820
	.long	908
	.long	609
	.long	772
	.long	154
	.long	274
	.long	580
	.long	184
	.long	79
	.long	626
	.long	630
	.long	742
	.long	653
	.long	282
	.long	762
	.long	623
	.long	680
	.long	81
	.long	927
	.long	626
	.long	789
	.long	125
	.long	411
	.long	521
	.long	938
	.long	300
	.long	821
	.long	78
	.long	343
	.long	175
	.long	128
	.long	250
	.long	170
	.long	774
	.long	972
	.long	275
	.long	999
	.long	639
	.long	495
	.long	78
	.long	352
	.long	126
	.long	857
	.long	956
	.long	358
	.long	619
	.long	580
	.long	124
	.long	737
	.long	594
	.long	701
	.long	612
	.long	669
	.long	112
	.long	134
	.long	694
	.long	363
	.long	992
	.long	809
	.long	743
	.long	168
	.long	974
	.long	944
	.long	375
	.long	748
	.long	52
	.long	600
	.long	747
	.long	642
	.long	182
	.long	862
	.long	81
	.long	344
	.long	805
	.long	988
	.long	739
	.long	511
	.long	655
	.long	814
	.long	334
	.long	249
	.long	515
	.long	897
	.long	955
	.long	664
	.long	981
	.long	649
	.long	113
	.long	974
	.long	459
	.long	893
	.long	228
	.long	433
	.long	837
	.long	553
	.long	268
	.long	926
	.long	240
	.long	102
	.long	654
	.long	459
	.long	51
	.long	686
	.long	754
	.long	806
	.long	760
	.long	493
	.long	403
	.long	415
	.long	394
	.long	687
	.long	700
	.long	946
	.long	670
	.long	656
	.long	610
	.long	738
	.long	392
	.long	760
	.long	799
	.long	887
	.long	653
	.long	978
	.long	321
	.long	576
	.long	617
	.long	626
	.long	502
	.long	894
	.long	679
	.long	243
	.long	440
	.long	680
	.long	879
	.long	194
	.long	572
	.long	640
	.long	724
	.long	926
	.long	56
	.long	204
	.long	700
	.long	707
	.long	151
	.long	457
	.long	449
	.long	797
	.long	195
	.long	791
	.long	558
	.long	945
	.long	679
	.long	297
	.long	59
	.long	87
	.long	824
	.long	713
	.long	663
	.long	412
	.long	693
	.long	342
	.long	606
	.long	134
	.long	108
	.long	571
	.long	364
	.long	631
	.long	212
	.long	174
	.long	643
	.long	304
	.long	329
	.long	343
	.long	97
	.long	430
	.long	751
	.long	497
	.long	314
	.long	983
	.long	374
	.long	822
	.long	928
	.long	140
	.long	206
	.long	73
	.long	263
	.long	980
	.long	736
	.long	876
	.long	478
	.long	430
	.long	305
	.long	170
	.long	514
	.long	364
	.long	692
	.long	829
	.long	82
	.long	855
	.long	953
	.long	676
	.long	246
	.long	369
	.long	970
	.long	294
	.long	750
	.long	807
	.long	827
	.long	150
	.long	790
	.long	288
	.long	923
	.long	804
	.long	378
	.long	215
	.long	828
	.long	592
	.long	281
	.long	565
	.long	555
	.long	710
	.long	82
	.long	896
	.long	831
	.long	547
	.long	261
	.long	524
	.long	462
	.long	293
	.long	465
	.long	502
	.long	56
	.long	661
	.long	821
	.long	976
	.long	991
	.long	658
	.long	869
	.long	905
	.long	758
	.long	745
	.long	193
	.long	768
	.long	550
	.long	608
	.long	933
	.long	378
	.long	286
	.long	215
	.long	979
	.long	792
	.long	961
	.long	61
	.long	688
	.long	793
	.long	644
	.long	986
	.long	403
	.long	106
	.long	366
	.long	905
	.long	644
	.long	372
	.long	567
	.long	466
	.long	434
	.long	645
	.long	210
	.long	389
	.long	550
	.long	919
	.long	135
	.long	780
	.long	773
	.long	635
	.long	389
	.long	707
	.long	100
	.long	626
	.long	958
	.long	165
	.long	504
	.long	920
	.long	176
	.long	193
	.long	713
	.long	857
	.long	265
	.long	203
	.long	50
	.long	668
	.long	108
	.long	645
	.long	990
	.long	626
	.long	197
	.long	510
	.long	357
	.long	358
	.long	850
	.long	858
	.long	364
	.long	936
	.long	638
	.text
	.globl	BZ2_bsInitWrite
	.type	BZ2_bsInitWrite, @function
BZ2_bsInitWrite:
.LFB56:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$0, 644(%rax)
	movq	-8(%rbp), %rax
	movl	$0, 640(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE56:
	.size	BZ2_bsInitWrite, .-BZ2_bsInitWrite
	.type	bsFinishWrite, @function
bsFinishWrite:
.LFB57:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	jmp	.L812
.L813:
	movq	-8(%rbp), %rax
	movq	80(%rax), %rdx
	movq	-8(%rbp), %rax
	movl	116(%rax), %eax
	cltq
	addq	%rax, %rdx
	movq	-8(%rbp), %rax
	movl	640(%rax), %eax
	shrl	$24, %eax
	movb	%al, (%rdx)
	movq	-8(%rbp), %rax
	movl	116(%rax), %eax
	leal	1(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 116(%rax)
	movq	-8(%rbp), %rax
	movl	640(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-8(%rbp), %rax
	movl	%edx, 640(%rax)
	movq	-8(%rbp), %rax
	movl	644(%rax), %eax
	leal	-8(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 644(%rax)
.L812:
	movq	-8(%rbp), %rax
	movl	644(%rax), %eax
	testl	%eax, %eax
	jg	.L813
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE57:
	.size	bsFinishWrite, .-bsFinishWrite
	.type	bsW, @function
bsW:
.LFB58:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movl	%edx, -24(%rbp)
	jmp	.L815
	.cfi_offset 3, -24
.L816:
	movq	-16(%rbp), %rax
	movq	80(%rax), %rdx
	movq	-16(%rbp), %rax
	movl	116(%rax), %eax
	cltq
	addq	%rax, %rdx
	movq	-16(%rbp), %rax
	movl	640(%rax), %eax
	shrl	$24, %eax
	movb	%al, (%rdx)
	movq	-16(%rbp), %rax
	movl	116(%rax), %eax
	leal	1(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 116(%rax)
	movq	-16(%rbp), %rax
	movl	640(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-16(%rbp), %rax
	movl	%edx, 640(%rax)
	movq	-16(%rbp), %rax
	movl	644(%rax), %eax
	leal	-8(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 644(%rax)
.L815:
	movq	-16(%rbp), %rax
	movl	644(%rax), %eax
	cmpl	$7, %eax
	jg	.L816
	movq	-16(%rbp), %rax
	movl	640(%rax), %edx
	movq	-16(%rbp), %rax
	movl	644(%rax), %eax
	movl	$32, %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	subl	-20(%rbp), %eax
	movl	-24(%rbp), %esi
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	orl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	%edx, 640(%rax)
	movq	-16(%rbp), %rax
	movl	644(%rax), %eax
	movl	%eax, %edx
	addl	-20(%rbp), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 644(%rax)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE58:
	.size	bsW, .-bsW
	.type	bsPutUInt32, @function
bsPutUInt32:
.LFB59:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	movq	-8(%rbp), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-12(%rbp), %eax
	shrl	$16, %eax
	movzbl	%al, %edx
	movq	-8(%rbp), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-12(%rbp), %eax
	shrl	$8, %eax
	movzbl	%al, %edx
	movq	-8(%rbp), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-12(%rbp), %eax
	movzbl	%al, %edx
	movq	-8(%rbp), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	bsW
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE59:
	.size	bsPutUInt32, .-bsPutUInt32
	.type	bsPutUChar, @function
bsPutUChar:
.LFB60:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, %eax
	movb	%al, -12(%rbp)
	movzbl	-12(%rbp), %edx
	movq	-8(%rbp), %rax
	movl	$8, %esi
	movq	%rax, %rdi
	call	bsW
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE60:
	.size	bsPutUChar, .-bsPutUChar
	.type	makeMaps_e, @function
makeMaps_e:
.LFB61:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	$0, 124(%rax)
	movl	$0, -4(%rbp)
	jmp	.L820
.L822:
	movq	-24(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	movzbl	128(%rdx,%rax), %eax
	testb	%al, %al
	je	.L821
	movq	-24(%rbp), %rax
	movl	124(%rax), %eax
	movl	%eax, %ecx
	movq	-24(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	movb	%cl, 384(%rdx,%rax)
	movq	-24(%rbp), %rax
	movl	124(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 124(%rax)
.L821:
	addl	$1, -4(%rbp)
.L820:
	cmpl	$255, -4(%rbp)
	jle	.L822
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE61:
	.size	makeMaps_e, .-makeMaps_e
	.type	generateMTFValues, @function
generateMTFValues:
.LFB62:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$336, %rsp
	movq	%rdi, -360(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-360(%rbp), %rax
	movq	56(%rax), %rax
	movq	%rax, -352(%rbp)
	movq	-360(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -344(%rbp)
	movq	-360(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rax, -336(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, %rdi
	.cfi_offset 3, -48
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	makeMaps_e
	movq	-360(%rbp), %rax
	movl	124(%rax), %eax
	addl	$1, %eax
	movl	%eax, -312(%rbp)
	movl	$0, -328(%rbp)
	jmp	.L824
.L825:
	movq	-360(%rbp), %rax
	movl	-328(%rbp), %edx
	movslq	%edx, %rdx
	addq	$168, %rdx
	movl	$0, (%rax,%rdx,4)
	addl	$1, -328(%rbp)
.L824:
	movl	-328(%rbp), %eax
	cmpl	-312(%rbp), %eax
	jle	.L825
	movl	$0, -316(%rbp)
	movl	$0, -320(%rbp)
	movl	$0, -328(%rbp)
	jmp	.L826
.L827:
	movl	-328(%rbp), %eax
	movl	%eax, %edx
	movl	-328(%rbp), %eax
	cltq
	movb	%dl, -304(%rbp,%rax)
	addl	$1, -328(%rbp)
.L826:
	movq	-360(%rbp), %rax
	movl	124(%rax), %eax
	cmpl	-328(%rbp), %eax
	jg	.L827
	movl	$0, -328(%rbp)
	jmp	.L828
.L840:
	movl	-328(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-352(%rbp), %rax
	movl	(%rax), %eax
	subl	$1, %eax
	movl	%eax, -324(%rbp)
	cmpl	$0, -324(%rbp)
	jns	.L829
	movq	-360(%rbp), %rax
	movl	108(%rax), %eax
	addl	%eax, -324(%rbp)
.L829:
	movl	-324(%rbp), %eax
	cltq
	addq	-344(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movq	-360(%rbp), %rdx
	cltq
	movzbl	384(%rdx,%rax), %eax
	movb	%al, -305(%rbp)
	movzbl	-304(%rbp), %eax
	cmpb	-305(%rbp), %al
	jne	.L830
	addl	$1, -320(%rbp)
	jmp	.L831
.L830:
	cmpl	$0, -320(%rbp)
	jle	.L832
	subl	$1, -320(%rbp)
.L837:
	movl	-320(%rbp), %eax
	andl	$1, %eax
	testb	%al, %al
	je	.L833
	movl	-316(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-336(%rbp), %rax
	movw	$1, (%rax)
	addl	$1, -316(%rbp)
	movq	-360(%rbp), %rax
	movl	676(%rax), %eax
	leal	1(%rax), %edx
	movq	-360(%rbp), %rax
	movl	%edx, 676(%rax)
	jmp	.L834
.L833:
	movl	-316(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-336(%rbp), %rax
	movw	$0, (%rax)
	addl	$1, -316(%rbp)
	movq	-360(%rbp), %rax
	movl	672(%rax), %eax
	leal	1(%rax), %edx
	movq	-360(%rbp), %rax
	movl	%edx, 672(%rax)
.L834:
	cmpl	$1, -320(%rbp)
	jle	.L850
.L835:
	movl	-320(%rbp), %eax
	subl	$2, %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -320(%rbp)
	jmp	.L837
.L850:
	nop
.L848:
	movl	$0, -320(%rbp)
.L832:
	movzbl	-303(%rbp), %r12d
	movzbl	-304(%rbp), %eax
	movb	%al, -303(%rbp)
	leaq	-304(%rbp), %rax
	leaq	1(%rax), %rbx
	movzbl	-305(%rbp), %r14d
	jmp	.L838
.L839:
	addq	$1, %rbx
	movl	%r12d, %r13d
	movzbl	(%rbx), %r12d
	movb	%r13b, (%rbx)
.L838:
	cmpb	%r12b, %r14b
	jne	.L839
	movb	%r12b, -304(%rbp)
	movq	%rbx, %rax
	movl	%eax, %edx
	leaq	-304(%rbp), %rax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -324(%rbp)
	movl	-316(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-336(%rbp), %rax
	movl	-324(%rbp), %edx
	addl	$1, %edx
	movw	%dx, (%rax)
	addl	$1, -316(%rbp)
	movl	-324(%rbp), %eax
	addl	$1, %eax
	movq	-360(%rbp), %rdx
	movslq	%eax, %rcx
	addq	$168, %rcx
	movl	(%rdx,%rcx,4), %edx
	leal	1(%rdx), %ecx
	movq	-360(%rbp), %rdx
	cltq
	addq	$168, %rax
	movl	%ecx, (%rdx,%rax,4)
.L831:
	addl	$1, -328(%rbp)
.L828:
	movq	-360(%rbp), %rax
	movl	108(%rax), %eax
	cmpl	-328(%rbp), %eax
	jg	.L840
	cmpl	$0, -320(%rbp)
	jle	.L841
	subl	$1, -320(%rbp)
.L846:
	movl	-320(%rbp), %eax
	andl	$1, %eax
	testb	%al, %al
	je	.L842
	movl	-316(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-336(%rbp), %rax
	movw	$1, (%rax)
	addl	$1, -316(%rbp)
	movq	-360(%rbp), %rax
	movl	676(%rax), %eax
	leal	1(%rax), %edx
	movq	-360(%rbp), %rax
	movl	%edx, 676(%rax)
	jmp	.L843
.L842:
	movl	-316(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-336(%rbp), %rax
	movw	$0, (%rax)
	addl	$1, -316(%rbp)
	movq	-360(%rbp), %rax
	movl	672(%rax), %eax
	leal	1(%rax), %edx
	movq	-360(%rbp), %rax
	movl	%edx, 672(%rax)
.L843:
	cmpl	$1, -320(%rbp)
	jle	.L851
.L844:
	movl	-320(%rbp), %eax
	subl	$2, %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -320(%rbp)
	jmp	.L846
.L851:
	nop
.L849:
	movl	$0, -320(%rbp)
.L841:
	movl	-316(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-336(%rbp), %rax
	movl	-312(%rbp), %edx
	movw	%dx, (%rax)
	addl	$1, -316(%rbp)
	movq	-360(%rbp), %rax
	movl	-312(%rbp), %edx
	movslq	%edx, %rdx
	addq	$168, %rdx
	movl	(%rax,%rdx,4), %eax
	leal	1(%rax), %ecx
	movq	-360(%rbp), %rax
	movl	-312(%rbp), %edx
	movslq	%edx, %rdx
	addq	$168, %rdx
	movl	%ecx, (%rax,%rdx,4)
	movq	-360(%rbp), %rax
	movl	-316(%rbp), %edx
	movl	%edx, 668(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L847
	call	__stack_chk_fail
.L847:
	addq	$336, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE62:
	.size	generateMTFValues, .-generateMTFValues
	.section	.rodata
	.align 8
.LC121:
	.string	"      %d in block, %d after MTF & 1-2 coding, %d+2 syms in use\n"
	.align 8
.LC122:
	.string	"      initial group %d, [%d .. %d], has %d syms (%4.1f%%)\n"
	.align 8
.LC123:
	.string	"      pass %d: size is %d, grp uses are "
.LC124:
	.string	"%d "
.LC125:
	.string	"      bytes: mapping %d, "
.LC126:
	.string	"selectors %d, "
.LC127:
	.string	"code lengths %d, "
.LC128:
	.string	"codes %d\n"
	.text
	.type	sendMTFValues, @function
sendMTFValues:
.LFB63:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$224, %rsp
	movq	%rdi, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-248(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rax, -184(%rbp)
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L853
	.cfi_offset 3, -48
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-248(%rbp), %rax
	movl	124(%rax), %edi
	movq	-248(%rbp), %rax
	movl	668(%rax), %ecx
	movq	-248(%rbp), %rax
	movl	108(%rax), %edx
	movl	$.LC121, %esi
	movq	stderr(%rip), %rax
	movl	%edi, %r8d
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L853:
	movq	-248(%rbp), %rax
	movl	124(%rax), %eax
	addl	$2, %eax
	movl	%eax, -84(%rbp)
	movl	$0, -156(%rbp)
	jmp	.L854
.L857:
	movl	$0, -160(%rbp)
	jmp	.L855
.L856:
	movq	-248(%rbp), %rcx
	movl	-160(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movb	$15, 12(%rax)
	addl	$1, -160(%rbp)
.L855:
	movl	-160(%rbp), %eax
	cmpl	-84(%rbp), %eax
	jl	.L856
	addl	$1, -156(%rbp)
.L854:
	cmpl	$5, -156(%rbp)
	jle	.L857
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	testl	%eax, %eax
	jg	.L858
	movl	$3001, %edi
	call	BZ2_bz__AssertH__fail
.L858:
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	$199, %eax
	jg	.L859
	movl	$2, -104(%rbp)
	jmp	.L860
.L859:
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	$599, %eax
	jg	.L861
	movl	$3, -104(%rbp)
	jmp	.L860
.L861:
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	$1199, %eax
	jg	.L862
	movl	$4, -104(%rbp)
	jmp	.L860
.L862:
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	$2399, %eax
	jg	.L863
	movl	$5, -104(%rbp)
	jmp	.L860
.L863:
	movl	$6, -104(%rbp)
.L860:
	movl	-104(%rbp), %eax
	movl	%eax, -100(%rbp)
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	movl	%eax, -96(%rbp)
	movl	$0, -144(%rbp)
	jmp	.L864
.L874:
	movl	-96(%rbp), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	idivl	-100(%rbp)
	movl	%eax, -80(%rbp)
	movl	-144(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -140(%rbp)
	movl	$0, -92(%rbp)
	jmp	.L865
.L867:
	addl	$1, -140(%rbp)
	movq	-248(%rbp), %rax
	movl	-140(%rbp), %edx
	movslq	%edx, %rdx
	addq	$168, %rdx
	movl	(%rax,%rdx,4), %eax
	addl	%eax, -92(%rbp)
.L865:
	movl	-92(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jge	.L866
	movl	-84(%rbp), %eax
	subl	$1, %eax
	cmpl	-140(%rbp), %eax
	jg	.L867
.L866:
	movl	-140(%rbp), %eax
	cmpl	-144(%rbp), %eax
	jle	.L868
	movl	-100(%rbp), %eax
	cmpl	-104(%rbp), %eax
	je	.L868
	cmpl	$1, -100(%rbp)
	je	.L868
	movl	-100(%rbp), %eax
	movl	-104(%rbp), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$31, %eax
	addl	%eax, %edx
	andl	$1, %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$1, %eax
	jne	.L868
	movq	-248(%rbp), %rax
	movl	-140(%rbp), %edx
	movslq	%edx, %rdx
	addq	$168, %rdx
	movl	(%rax,%rdx,4), %eax
	subl	%eax, -92(%rbp)
	subl	$1, -140(%rbp)
.L868:
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L869
	cvtsi2ss	-92(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	.LC4(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cvtsi2ss	%eax, %xmm1
	unpcklps	%xmm1, %xmm1
	cvtps2pd	%xmm1, %xmm1
	divsd	%xmm1, %xmm0
	movl	$.LC122, %esi
	movq	stderr(%rip), %rax
	movl	-92(%rbp), %r8d
	movl	-140(%rbp), %edi
	movl	-144(%rbp), %ecx
	movl	-100(%rbp), %edx
	movl	%r8d, %r9d
	movl	%edi, %r8d
	movq	%rax, %rdi
	movl	$1, %eax
	call	fprintf
.L869:
	movl	$0, -160(%rbp)
	jmp	.L870
.L873:
	movl	-160(%rbp), %eax
	cmpl	-144(%rbp), %eax
	jl	.L871
	movl	-160(%rbp), %eax
	cmpl	-140(%rbp), %eax
	jg	.L871
	movl	-100(%rbp), %eax
	leal	-1(%rax), %esi
	movq	-248(%rbp), %rcx
	movl	-160(%rbp), %eax
	movslq	%eax, %rdx
	movslq	%esi, %rax
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movb	$0, 12(%rax)
	jmp	.L872
.L871:
	movl	-100(%rbp), %eax
	leal	-1(%rax), %esi
	movq	-248(%rbp), %rcx
	movl	-160(%rbp), %eax
	movslq	%eax, %rdx
	movslq	%esi, %rax
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movb	$15, 12(%rax)
.L872:
	addl	$1, -160(%rbp)
.L870:
	movl	-160(%rbp), %eax
	cmpl	-84(%rbp), %eax
	jl	.L873
	subl	$1, -100(%rbp)
	movl	-140(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -144(%rbp)
	movl	-92(%rbp), %eax
	subl	%eax, -96(%rbp)
.L864:
	cmpl	$0, -100(%rbp)
	jg	.L874
	movl	$0, -124(%rbp)
	jmp	.L875
.L909:
	movl	$0, -156(%rbp)
	jmp	.L876
.L877:
	movl	-156(%rbp), %eax
	cltq
	movl	$0, -240(%rbp,%rax,4)
	addl	$1, -156(%rbp)
.L876:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L877
	movl	$0, -156(%rbp)
	jmp	.L878
.L881:
	movl	$0, -160(%rbp)
	jmp	.L879
.L880:
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	movslq	%eax, %rcx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	$11360, %rax
	movl	$0, 8(%rdx,%rax,4)
	addl	$1, -160(%rbp)
.L879:
	movl	-160(%rbp), %eax
	cmpl	-84(%rbp), %eax
	jl	.L880
	addl	$1, -156(%rbp)
.L878:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L881
	cmpl	$6, -104(%rbp)
	jne	.L882
	movl	$0, -160(%rbp)
	jmp	.L883
.L884:
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	cltq
	movzbl	37966(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	sall	$16, %ecx
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	cltq
	movzbl	37708(%rdx,%rax), %eax
	movzbl	%al, %eax
	orl	%ecx, %eax
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %ecx
	movslq	%ecx, %rcx
	addq	$3227, %rcx
	salq	$4, %rcx
	addq	%rcx, %rdx
	movl	%eax, 8(%rdx)
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	cltq
	movzbl	38482(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	sall	$16, %ecx
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	cltq
	movzbl	38224(%rdx,%rax), %eax
	movzbl	%al, %eax
	orl	%ecx, %eax
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %ecx
	movslq	%ecx, %rcx
	salq	$4, %rcx
	addq	%rcx, %rdx
	addq	$51636, %rdx
	movl	%eax, 8(%rdx)
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	cltq
	movzbl	38998(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	sall	$16, %ecx
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %eax
	cltq
	movzbl	38740(%rdx,%rax), %eax
	movzbl	%al, %eax
	orl	%ecx, %eax
	movq	-248(%rbp), %rdx
	movl	-160(%rbp), %ecx
	movslq	%ecx, %rcx
	salq	$4, %rcx
	addq	%rcx, %rdx
	addq	$51640, %rdx
	movl	%eax, 8(%rdx)
	addl	$1, -160(%rbp)
.L883:
	movl	-160(%rbp), %eax
	cmpl	-84(%rbp), %eax
	jl	.L884
.L882:
	movl	$0, -120(%rbp)
	movl	$0, -136(%rbp)
	movl	$0, -144(%rbp)
.L903:
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	-144(%rbp), %eax
	jle	.L972
.L885:
	movl	-144(%rbp), %eax
	addl	$49, %eax
	movl	%eax, -140(%rbp)
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	-140(%rbp), %eax
	jg	.L887
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	subl	$1, %eax
	movl	%eax, -140(%rbp)
.L887:
	movl	$0, -156(%rbp)
	jmp	.L888
.L889:
	movl	-156(%rbp), %eax
	cltq
	movw	$0, -208(%rbp,%rax,2)
	addl	$1, -156(%rbp)
.L888:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L889
	cmpl	$6, -104(%rbp)
	jne	.L890
	movl	-144(%rbp), %eax
	movl	-140(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$49, %eax
	jne	.L890
	movl	$0, %r12d
	movl	%r12d, %r13d
	movl	%r13d, %r14d
	movl	-144(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$1, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$2, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$3, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$4, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$5, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$6, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$7, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$8, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$9, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$10, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$11, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$12, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$13, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$14, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$15, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$16, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$17, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$18, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$19, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$20, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$21, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$22, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$23, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$24, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$25, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$26, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$27, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$28, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$29, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$30, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$31, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$32, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$33, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$34, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$35, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$36, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$37, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$38, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$39, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$40, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$41, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$42, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$43, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$44, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$45, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$46, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$47, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$48, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	-144(%rbp), %eax
	cltq
	addq	$49, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %ebx
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	addq	$3227, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	8(%rax), %eax
	addl	%eax, %r14d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51636, %rax
	movl	8(%rax), %eax
	addl	%eax, %r13d
	movzwl	%bx, %edx
	movq	-248(%rbp), %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	addq	$51640, %rax
	movl	8(%rax), %eax
	addl	%eax, %r12d
	movl	%r14d, %eax
	movw	%ax, -208(%rbp)
	movl	%r14d, %eax
	shrl	$16, %eax
	movw	%ax, -206(%rbp)
	movl	%r13d, %eax
	movw	%ax, -204(%rbp)
	movl	%r13d, %eax
	shrl	$16, %eax
	movw	%ax, -202(%rbp)
	movl	%r12d, %eax
	movw	%ax, -200(%rbp)
	movl	%r12d, %eax
	shrl	$16, %eax
	movw	%ax, -198(%rbp)
	jmp	.L891
.L890:
	movl	-144(%rbp), %eax
	movl	%eax, -152(%rbp)
	jmp	.L892
.L895:
	movl	-152(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -72(%rbp)
	movl	$0, -156(%rbp)
	jmp	.L893
.L894:
	movl	-156(%rbp), %eax
	cltq
	movzwl	-208(%rbp,%rax,2), %edx
	movzwl	-72(%rbp), %eax
	movq	-248(%rbp), %rsi
	movslq	%eax, %rcx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	addl	%eax, %edx
	movl	-156(%rbp), %eax
	cltq
	movw	%dx, -208(%rbp,%rax,2)
	addl	$1, -156(%rbp)
.L893:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L894
	addl	$1, -152(%rbp)
.L892:
	movl	-152(%rbp), %eax
	cmpl	-140(%rbp), %eax
	jle	.L895
.L891:
	movl	$999999999, -128(%rbp)
	movl	$-1, -132(%rbp)
	movl	$0, -156(%rbp)
	jmp	.L896
.L898:
	movl	-156(%rbp), %eax
	cltq
	movzwl	-208(%rbp,%rax,2), %eax
	movzwl	%ax, %eax
	cmpl	-128(%rbp), %eax
	jge	.L897
	movl	-156(%rbp), %eax
	cltq
	movzwl	-208(%rbp,%rax,2), %eax
	movzwl	%ax, %eax
	movl	%eax, -128(%rbp)
	movl	-156(%rbp), %eax
	movl	%eax, -132(%rbp)
.L897:
	addl	$1, -156(%rbp)
.L896:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L898
	movl	-128(%rbp), %eax
	addl	%eax, -136(%rbp)
	movl	-132(%rbp), %eax
	cltq
	movl	-240(%rbp,%rax,4), %eax
	leal	1(%rax), %edx
	movl	-132(%rbp), %eax
	cltq
	movl	%edx, -240(%rbp,%rax,4)
	movl	-132(%rbp), %eax
	movl	%eax, %ecx
	movq	-248(%rbp), %rdx
	movl	-120(%rbp), %eax
	cltq
	movb	%cl, 1704(%rdx,%rax)
	addl	$1, -120(%rbp)
	cmpl	$6, -104(%rbp)
	jne	.L899
	movl	-144(%rbp), %eax
	movl	-140(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$49, %eax
	jne	.L899
	movl	-144(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$1, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$2, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$3, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$4, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$5, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$6, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$7, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$8, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$9, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$10, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$11, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$12, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$13, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$14, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$15, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$16, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$17, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$18, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$19, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$20, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$21, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$22, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$23, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$24, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$25, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$26, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$27, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$28, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$29, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$30, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$31, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$32, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$33, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$34, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$35, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$36, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$37, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$38, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$39, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$40, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$41, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$42, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$43, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$44, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$45, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$46, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$47, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$48, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	movl	-144(%rbp), %eax
	cltq
	addq	$49, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	jmp	.L900
.L899:
	movl	-144(%rbp), %eax
	movl	%eax, -152(%rbp)
	jmp	.L901
.L902:
	movl	-152(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-248(%rbp), %rcx
	movslq	%edx, %rsi
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	$11360, %rax
	movl	8(%rcx,%rax,4), %eax
	leal	1(%rax), %esi
	movq	-248(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-132(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rdx, %rax
	addq	$11360, %rax
	movl	%esi, 8(%rcx,%rax,4)
	addl	$1, -152(%rbp)
.L901:
	movl	-152(%rbp), %eax
	cmpl	-140(%rbp), %eax
	jle	.L902
.L900:
	movl	-140(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -144(%rbp)
	jmp	.L903
.L972:
	nop
.L970:
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L904
	movl	-136(%rbp), %eax
	leal	7(%rax), %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	movl	%eax, %edx
	movl	-124(%rbp), %eax
	leal	1(%rax), %edi
	movl	$.LC123, %esi
	movq	stderr(%rip), %rax
	movl	%edx, %ecx
	movl	%edi, %edx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$0, -156(%rbp)
	jmp	.L905
.L906:
	movl	-156(%rbp), %eax
	cltq
	movl	-240(%rbp,%rax,4), %edx
	movl	$.LC124, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	addl	$1, -156(%rbp)
.L905:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L906
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$10, %edi
	call	fputc
.L904:
	movl	$0, -156(%rbp)
	jmp	.L907
.L908:
	movq	-248(%rbp), %rax
	leaq	45448(%rax), %rdx
	movl	-156(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	leaq	(%rdx,%rax), %rsi
	movq	-248(%rbp), %rax
	leaq	37708(%rax), %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	leaq	(%rdx,%rax), %rdi
	movl	-84(%rbp), %eax
	movl	$17, %ecx
	movl	%eax, %edx
	call	BZ2_hbMakeCodeLengths
	addl	$1, -156(%rbp)
.L907:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L908
	addl	$1, -124(%rbp)
.L875:
	cmpl	$3, -124(%rbp)
	jle	.L909
	cmpl	$7, -104(%rbp)
	jle	.L910
	movl	$3002, %edi
	call	BZ2_bz__AssertH__fail
.L910:
	cmpl	$32767, -120(%rbp)
	jg	.L911
	cmpl	$18002, -120(%rbp)
	jle	.L912
.L911:
	movl	$3003, %edi
	call	BZ2_bz__AssertH__fail
.L912:
	movl	$0, -152(%rbp)
	jmp	.L913
.L914:
	movl	-152(%rbp), %eax
	movl	%eax, %edx
	movl	-152(%rbp), %eax
	cltq
	movb	%dl, -64(%rbp,%rax)
	addl	$1, -152(%rbp)
.L913:
	movl	-152(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L914
	movl	$0, -152(%rbp)
	jmp	.L915
.L918:
	movq	-248(%rbp), %rdx
	movl	-152(%rbp), %eax
	cltq
	movzbl	1704(%rdx,%rax), %eax
	movb	%al, -66(%rbp)
	movl	$0, -148(%rbp)
	movl	-148(%rbp), %eax
	cltq
	movzbl	-64(%rbp,%rax), %eax
	movb	%al, -67(%rbp)
	jmp	.L916
.L917:
	addl	$1, -148(%rbp)
	movzbl	-67(%rbp), %eax
	movb	%al, -65(%rbp)
	movl	-148(%rbp), %eax
	cltq
	movzbl	-64(%rbp,%rax), %eax
	movb	%al, -67(%rbp)
	movl	-148(%rbp), %eax
	cltq
	movzbl	-65(%rbp), %edx
	movb	%dl, -64(%rbp,%rax)
.L916:
	movzbl	-66(%rbp), %eax
	cmpb	-67(%rbp), %al
	jne	.L917
	movzbl	-67(%rbp), %eax
	movb	%al, -64(%rbp)
	movl	-148(%rbp), %eax
	movl	%eax, %ecx
	movq	-248(%rbp), %rdx
	movl	-152(%rbp), %eax
	cltq
	movb	%cl, 19706(%rdx,%rax)
	addl	$1, -152(%rbp)
.L915:
	movl	-152(%rbp), %eax
	cmpl	-120(%rbp), %eax
	jl	.L918
	movl	$0, -156(%rbp)
	jmp	.L919
.L926:
	movl	$32, -116(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -152(%rbp)
	jmp	.L920
.L923:
	movq	-248(%rbp), %rcx
	movl	-152(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	cmpl	-112(%rbp), %eax
	jle	.L921
	movq	-248(%rbp), %rcx
	movl	-152(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -112(%rbp)
.L921:
	movq	-248(%rbp), %rcx
	movl	-152(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	cmpl	-116(%rbp), %eax
	jge	.L922
	movq	-248(%rbp), %rcx
	movl	-152(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -116(%rbp)
.L922:
	addl	$1, -152(%rbp)
.L920:
	movl	-152(%rbp), %eax
	cmpl	-84(%rbp), %eax
	jl	.L923
	cmpl	$17, -112(%rbp)
	jle	.L924
	movl	$3004, %edi
	call	BZ2_bz__AssertH__fail
.L924:
	cmpl	$0, -116(%rbp)
	jg	.L925
	movl	$3005, %edi
	call	BZ2_bz__AssertH__fail
.L925:
	movq	-248(%rbp), %rax
	leaq	37708(%rax), %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	leaq	(%rdx,%rax), %rsi
	movq	-248(%rbp), %rax
	leaq	39256(%rax), %rdx
	movl	-156(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	leaq	(%rdx,%rax), %rdi
	movl	-84(%rbp), %ecx
	movl	-112(%rbp), %edx
	movl	-116(%rbp), %eax
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%eax, %edx
	call	BZ2_hbAssignCodes
	addl	$1, -156(%rbp)
.L919:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L926
	movl	$0, -152(%rbp)
	jmp	.L927
.L931:
	movl	-152(%rbp), %eax
	cltq
	movb	$0, -64(%rbp,%rax)
	movl	$0, -148(%rbp)
	jmp	.L928
.L930:
	movl	-152(%rbp), %eax
	sall	$4, %eax
	addl	-148(%rbp), %eax
	movq	-248(%rbp), %rdx
	cltq
	movzbl	128(%rdx,%rax), %eax
	testb	%al, %al
	je	.L929
	movl	-152(%rbp), %eax
	cltq
	movb	$1, -64(%rbp,%rax)
.L929:
	addl	$1, -148(%rbp)
.L928:
	cmpl	$15, -148(%rbp)
	jle	.L930
	addl	$1, -152(%rbp)
.L927:
	cmpl	$15, -152(%rbp)
	jle	.L931
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, -76(%rbp)
	movl	$0, -152(%rbp)
	jmp	.L932
.L935:
	movl	-152(%rbp), %eax
	cltq
	movzbl	-64(%rbp,%rax), %eax
	testb	%al, %al
	je	.L933
	movq	-248(%rbp), %rax
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
	jmp	.L934
.L933:
	movq	-248(%rbp), %rax
	movl	$0, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
.L934:
	addl	$1, -152(%rbp)
.L932:
	cmpl	$15, -152(%rbp)
	jle	.L935
	movl	$0, -152(%rbp)
	jmp	.L936
.L942:
	movl	-152(%rbp), %eax
	cltq
	movzbl	-64(%rbp,%rax), %eax
	testb	%al, %al
	je	.L937
	movl	$0, -148(%rbp)
	jmp	.L938
.L941:
	movl	-152(%rbp), %eax
	sall	$4, %eax
	addl	-148(%rbp), %eax
	movq	-248(%rbp), %rdx
	cltq
	movzbl	128(%rdx,%rax), %eax
	testb	%al, %al
	je	.L939
	movq	-248(%rbp), %rax
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
	jmp	.L940
.L939:
	movq	-248(%rbp), %rax
	movl	$0, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
.L940:
	addl	$1, -148(%rbp)
.L938:
	cmpl	$15, -148(%rbp)
	jle	.L941
.L937:
	addl	$1, -152(%rbp)
.L936:
	cmpl	$15, -152(%rbp)
	jle	.L942
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L943
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, %edx
	subl	-76(%rbp), %edx
	movl	$.LC125, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L943:
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, -76(%rbp)
	movl	-104(%rbp), %edx
	movq	-248(%rbp), %rax
	movl	$3, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-120(%rbp), %edx
	movq	-248(%rbp), %rax
	movl	$15, %esi
	movq	%rax, %rdi
	call	bsW
	movl	$0, -152(%rbp)
	jmp	.L944
.L947:
	movl	$0, -148(%rbp)
	jmp	.L945
.L946:
	movq	-248(%rbp), %rax
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
	addl	$1, -148(%rbp)
.L945:
	movq	-248(%rbp), %rdx
	movl	-152(%rbp), %eax
	cltq
	movzbl	19706(%rdx,%rax), %eax
	movzbl	%al, %eax
	cmpl	-148(%rbp), %eax
	jg	.L946
	movq	-248(%rbp), %rax
	movl	$0, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
	addl	$1, -152(%rbp)
.L944:
	movl	-152(%rbp), %eax
	cmpl	-120(%rbp), %eax
	jl	.L947
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L948
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, %edx
	subl	-76(%rbp), %edx
	movl	$.LC126, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L948:
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, -76(%rbp)
	movl	$0, -156(%rbp)
	jmp	.L949
.L956:
	movq	-248(%rbp), %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -88(%rbp)
	movl	-88(%rbp), %edx
	movq	-248(%rbp), %rax
	movl	$5, %esi
	movq	%rax, %rdi
	call	bsW
	movl	$0, -152(%rbp)
	jmp	.L950
.L952:
	movq	-248(%rbp), %rax
	movl	$2, %edx
	movl	$2, %esi
	movq	%rax, %rdi
	call	bsW
	addl	$1, -88(%rbp)
	jmp	.L951
.L973:
	nop
.L951:
	movq	-248(%rbp), %rcx
	movl	-152(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	cmpl	-88(%rbp), %eax
	jg	.L952
	jmp	.L953
.L954:
	movq	-248(%rbp), %rax
	movl	$3, %edx
	movl	$2, %esi
	movq	%rax, %rdi
	call	bsW
	subl	$1, -88(%rbp)
.L953:
	movq	-248(%rbp), %rcx
	movl	-152(%rbp), %eax
	movslq	%eax, %rdx
	movl	-156(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	cmpl	-88(%rbp), %eax
	jl	.L954
	movq	-248(%rbp), %rax
	movl	$0, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
	addl	$1, -152(%rbp)
.L950:
	movl	-152(%rbp), %eax
	cmpl	-84(%rbp), %eax
	jl	.L973
	addl	$1, -156(%rbp)
.L949:
	movl	-156(%rbp), %eax
	cmpl	-104(%rbp), %eax
	jl	.L956
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L957
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, %edx
	subl	-76(%rbp), %edx
	movl	$.LC127, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L957:
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, -76(%rbp)
	movl	$0, -108(%rbp)
	movl	$0, -144(%rbp)
.L966:
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	-144(%rbp), %eax
	jle	.L974
.L958:
	movl	-144(%rbp), %eax
	addl	$49, %eax
	movl	%eax, -140(%rbp)
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	cmpl	-140(%rbp), %eax
	jg	.L960
	movq	-248(%rbp), %rax
	movl	668(%rax), %eax
	subl	$1, %eax
	movl	%eax, -140(%rbp)
.L960:
	movq	-248(%rbp), %rdx
	movl	-108(%rbp), %eax
	cltq
	movzbl	1704(%rdx,%rax), %eax
	movzbl	%al, %eax
	cmpl	-104(%rbp), %eax
	jl	.L961
	movl	$3006, %edi
	call	BZ2_bz__AssertH__fail
.L961:
	cmpl	$6, -104(%rbp)
	jne	.L962
	movl	-144(%rbp), %eax
	movl	-140(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$49, %eax
	jne	.L962
	movq	-248(%rbp), %rax
	leaq	37708(%rax), %rcx
	movq	-248(%rbp), %rdx
	movl	-108(%rbp), %eax
	cltq
	movzbl	1704(%rdx,%rax), %eax
	movzbl	%al, %eax
	addq	%rax, %rax
	movq	%rax, %rdx
	salq	$7, %rdx
	addq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -176(%rbp)
	movq	-248(%rbp), %rax
	leaq	39256(%rax), %rcx
	movq	-248(%rbp), %rdx
	movl	-108(%rbp), %eax
	cltq
	movzbl	1704(%rdx,%rax), %eax
	movzbl	%al, %eax
	salq	$3, %rax
	movq	%rax, %rdx
	salq	$7, %rdx
	addq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -168(%rbp)
	movl	-144(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$1, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$2, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$3, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$4, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$5, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$6, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$7, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$8, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$9, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$10, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$11, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$12, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$13, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$14, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$15, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$16, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$17, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$18, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$19, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$20, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$21, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$22, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$23, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$24, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$25, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$26, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$27, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$28, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$29, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$30, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$31, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$32, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$33, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$34, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$35, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$36, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$37, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$38, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$39, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$40, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$41, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$42, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$43, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$44, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$45, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$46, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$47, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$48, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	movl	-144(%rbp), %eax
	cltq
	addq	$49, %rax
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -70(%rbp)
	movzwl	-70(%rbp), %eax
	salq	$2, %rax
	addq	-168(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	movzwl	-70(%rbp), %eax
	addq	-176(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	jmp	.L963
.L962:
	movl	-144(%rbp), %eax
	movl	%eax, -152(%rbp)
	jmp	.L964
.L965:
	movq	-248(%rbp), %rdx
	movl	-108(%rbp), %eax
	cltq
	movzbl	1704(%rdx,%rax), %eax
	movzbl	%al, %esi
	movl	-152(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movq	-248(%rbp), %rdx
	movslq	%eax, %rcx
	movslq	%esi, %rax
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	$9812, %rax
	movl	8(%rdx,%rax,4), %eax
	movl	%eax, %edx
	movq	-248(%rbp), %rcx
	movl	-108(%rbp), %eax
	cltq
	movzbl	1704(%rcx,%rax), %eax
	movzbl	%al, %edi
	movl	-152(%rbp), %eax
	cltq
	addq	%rax, %rax
	addq	-184(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movq	-248(%rbp), %rsi
	movslq	%eax, %rcx
	movslq	%edi, %rax
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	$37696, %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %ecx
	movq	-248(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	bsW
	addl	$1, -152(%rbp)
.L964:
	movl	-152(%rbp), %eax
	cmpl	-140(%rbp), %eax
	jle	.L965
.L963:
	movl	-140(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -144(%rbp)
	addl	$1, -108(%rbp)
	jmp	.L966
.L974:
	nop
.L971:
	movl	-108(%rbp), %eax
	cmpl	-120(%rbp), %eax
	je	.L967
	movl	$3007, %edi
	call	BZ2_bz__AssertH__fail
.L967:
	movq	-248(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$2, %eax
	jle	.L852
	movq	-248(%rbp), %rax
	movl	116(%rax), %eax
	movl	%eax, %edx
	subl	-76(%rbp), %edx
	movl	$.LC128, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L852:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L969
	call	__stack_chk_fail
.L969:
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE63:
	.size	sendMTFValues, .-sendMTFValues
	.section	.rodata
	.align 8
.LC129:
	.string	"    block %d: crc = 0x%08x, combined CRC = 0x%08x, size = %d\n"
	.align 8
.LC130:
	.string	"    final combined CRC = 0x%08x\n   "
	.text
	.globl	BZ2_compressBlock
	.type	BZ2_compressBlock, @function
BZ2_compressBlock:
.LFB64:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, %eax
	movb	%al, -12(%rbp)
	movq	-8(%rbp), %rax
	movl	108(%rax), %eax
	testl	%eax, %eax
	jle	.L976
	movq	-8(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %edx
	notl	%edx
	movq	-8(%rbp), %rax
	movl	%edx, 648(%rax)
	movq	-8(%rbp), %rax
	movl	652(%rax), %eax
	movl	%eax, %edx
	roll	%edx
	movq	-8(%rbp), %rax
	movl	%edx, 652(%rax)
	movq	-8(%rbp), %rax
	movl	652(%rax), %edx
	movq	-8(%rbp), %rax
	movl	648(%rax), %eax
	xorl	%eax, %edx
	movq	-8(%rbp), %rax
	movl	%edx, 652(%rax)
	movq	-8(%rbp), %rax
	movl	660(%rax), %eax
	cmpl	$1, %eax
	jle	.L977
	movq	-8(%rbp), %rax
	movl	$0, 116(%rax)
.L977:
	movq	-8(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$1, %eax
	jle	.L978
	movq	-8(%rbp), %rax
	movl	108(%rax), %r8d
	movq	-8(%rbp), %rax
	movl	652(%rax), %edi
	movq	-8(%rbp), %rax
	movl	648(%rax), %ecx
	movq	-8(%rbp), %rax
	movl	660(%rax), %edx
	movl	$.LC129, %esi
	movq	stderr(%rip), %rax
	movl	%r8d, %r9d
	movl	%edi, %r8d
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L978:
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_blockSort
.L976:
	movq	-8(%rbp), %rax
	movq	32(%rax), %rdx
	movq	-8(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 80(%rax)
	movq	-8(%rbp), %rax
	movl	660(%rax), %eax
	cmpl	$1, %eax
	jne	.L979
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bsInitWrite
	movq	-8(%rbp), %rax
	movl	$66, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$90, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$104, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	664(%rax), %eax
	addl	$48, %eax
	movzbl	%al, %edx
	movq	-8(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	bsPutUChar
.L979:
	movq	-8(%rbp), %rax
	movl	108(%rax), %eax
	testl	%eax, %eax
	jle	.L980
	movq	-8(%rbp), %rax
	movl	$49, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$65, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$89, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$38, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$83, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$89, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	648(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	bsPutUInt32
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	bsW
	movq	-8(%rbp), %rax
	movl	48(%rax), %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movl	$24, %esi
	movq	%rax, %rdi
	call	bsW
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	generateMTFValues
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	sendMTFValues
.L980:
	cmpb	$0, -12(%rbp)
	je	.L975
	movq	-8(%rbp), %rax
	movl	$23, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$114, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$69, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$56, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$80, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	$144, %esi
	movq	%rax, %rdi
	call	bsPutUChar
	movq	-8(%rbp), %rax
	movl	652(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	bsPutUInt32
	movq	-8(%rbp), %rax
	movl	656(%rax), %eax
	cmpl	$1, %eax
	jle	.L982
	movq	-8(%rbp), %rax
	movl	652(%rax), %edx
	movl	$.LC130, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L982:
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	bsFinishWrite
.L975:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE64:
	.size	BZ2_compressBlock, .-BZ2_compressBlock
	.type	makeMaps_d, @function
makeMaps_d:
.LFB65:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	$0, 3192(%rax)
	movl	$0, -4(%rbp)
	jmp	.L984
.L986:
	movq	-24(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	movzbl	3196(%rdx,%rax), %eax
	testb	%al, %al
	je	.L985
	movq	-24(%rbp), %rax
	movl	3192(%rax), %eax
	movl	-4(%rbp), %edx
	movl	%edx, %ecx
	movq	-24(%rbp), %rdx
	cltq
	movb	%cl, 3468(%rdx,%rax)
	movq	-24(%rbp), %rax
	movl	3192(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 3192(%rax)
.L985:
	addl	$1, -4(%rbp)
.L984:
	cmpl	$255, -4(%rbp)
	jle	.L986
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE65:
	.size	makeMaps_d, .-makeMaps_d
	.section	.rodata
.LC131:
	.string	"\n    [%d: huff+mtf "
.LC132:
	.string	"rt+rld"
	.text
	.globl	BZ2_decompress
	.type	BZ2_decompress, @function
BZ2_decompress:
.LFB66:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$408, %rsp
	movq	%rdi, -392(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -352(%rbp)
	movq	-392(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$10, %eax
	jne	.L988
	.cfi_offset 3, -24
	movq	-392(%rbp), %rax
	movl	$0, 64036(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64040(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64044(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64048(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64052(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64056(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64060(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64064(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64068(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64072(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64076(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64080(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64084(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64088(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64092(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64096(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64100(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64104(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64108(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64112(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 64116(%rax)
	movq	-392(%rbp), %rax
	movq	$0, 64120(%rax)
	movq	-392(%rbp), %rax
	movq	$0, 64128(%rax)
	movq	-392(%rbp), %rax
	movq	$0, 64136(%rax)
.L988:
	movq	-392(%rbp), %rax
	movl	64036(%rax), %eax
	movl	%eax, -332(%rbp)
	movq	-392(%rbp), %rax
	movl	64040(%rax), %eax
	movl	%eax, -328(%rbp)
	movq	-392(%rbp), %rax
	movl	64044(%rax), %eax
	movl	%eax, -324(%rbp)
	movq	-392(%rbp), %rax
	movl	64048(%rax), %eax
	movl	%eax, -320(%rbp)
	movq	-392(%rbp), %rax
	movl	64052(%rax), %eax
	movl	%eax, -316(%rbp)
	movq	-392(%rbp), %rax
	movl	64056(%rax), %eax
	movl	%eax, -312(%rbp)
	movq	-392(%rbp), %rax
	movl	64060(%rax), %eax
	movl	%eax, -308(%rbp)
	movq	-392(%rbp), %rax
	movl	64064(%rax), %eax
	movl	%eax, -304(%rbp)
	movq	-392(%rbp), %rax
	movl	64068(%rax), %eax
	movl	%eax, -300(%rbp)
	movq	-392(%rbp), %rax
	movl	64072(%rax), %eax
	movl	%eax, -296(%rbp)
	movq	-392(%rbp), %rax
	movl	64076(%rax), %eax
	movl	%eax, -292(%rbp)
	movq	-392(%rbp), %rax
	movl	64080(%rax), %eax
	movl	%eax, -288(%rbp)
	movq	-392(%rbp), %rax
	movl	64084(%rax), %eax
	movl	%eax, -284(%rbp)
	movq	-392(%rbp), %rax
	movl	64088(%rax), %eax
	movl	%eax, -280(%rbp)
	movq	-392(%rbp), %rax
	movl	64092(%rax), %eax
	movl	%eax, -276(%rbp)
	movq	-392(%rbp), %rax
	movl	64096(%rax), %eax
	movl	%eax, -216(%rbp)
	movq	-392(%rbp), %rax
	movl	64100(%rax), %eax
	movl	%eax, -272(%rbp)
	movq	-392(%rbp), %rax
	movl	64104(%rax), %eax
	movl	%eax, -268(%rbp)
	movq	-392(%rbp), %rax
	movl	64108(%rax), %eax
	movl	%eax, -264(%rbp)
	movq	-392(%rbp), %rax
	movl	64112(%rax), %eax
	movl	%eax, -260(%rbp)
	movq	-392(%rbp), %rax
	movl	64116(%rax), %eax
	movl	%eax, -256(%rbp)
	movq	-392(%rbp), %rax
	movq	64120(%rax), %rax
	movq	%rax, -376(%rbp)
	movq	-392(%rbp), %rax
	movq	64128(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-392(%rbp), %rax
	movq	64136(%rax), %rax
	movq	%rax, -360(%rbp)
	movl	$0, -344(%rbp)
	movq	-392(%rbp), %rax
	movl	8(%rax), %eax
	subl	$10, %eax
	cmpl	$40, %eax
	ja	.L989
	mov	%eax, %eax
	movq	.L1031(,%rax,8), %rax
	jmp	*%rax
	.section	.rodata
	.align 8
	.align 4
.L1031:
	.quad	.L990
	.quad	.L991
	.quad	.L992
	.quad	.L993
	.quad	.L994
	.quad	.L995
	.quad	.L996
	.quad	.L997
	.quad	.L998
	.quad	.L999
	.quad	.L1000
	.quad	.L1001
	.quad	.L1002
	.quad	.L1003
	.quad	.L1004
	.quad	.L1005
	.quad	.L1006
	.quad	.L1007
	.quad	.L1008
	.quad	.L1009
	.quad	.L1010
	.quad	.L1011
	.quad	.L1012
	.quad	.L1013
	.quad	.L1014
	.quad	.L1015
	.quad	.L1016
	.quad	.L1017
	.quad	.L1018
	.quad	.L1019
	.quad	.L1020
	.quad	.L1021
	.quad	.L1022
	.quad	.L1023
	.quad	.L1024
	.quad	.L1025
	.quad	.L1026
	.quad	.L1027
	.quad	.L1028
	.quad	.L1029
	.quad	.L1030
	.text
.L990:
	movq	-392(%rbp), %rax
	movl	$10, 8(%rax)
.L1037:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1032
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -212(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-212(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$66, -35(%rbp)
	jne	.L1033
	jmp	.L991
.L1032:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1034
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1034:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1037
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1037
.L1033:
	movl	$-5, -344(%rbp)
	jmp	.L1035
.L991:
	movq	-392(%rbp), %rax
	movl	$11, 8(%rax)
.L1042:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1038
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -208(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-208(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$90, -35(%rbp)
	jne	.L1039
	jmp	.L992
.L1038:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1040
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1040:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1042
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1042
.L1039:
	movl	$-5, -344(%rbp)
	jmp	.L1035
.L992:
	movq	-392(%rbp), %rax
	movl	$12, 8(%rax)
.L1047:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1043
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -204(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-204(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$104, -35(%rbp)
	jne	.L1044
	jmp	.L993
.L1043:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1045
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1045:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1047
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1047
.L1044:
	movl	$-5, -344(%rbp)
	jmp	.L1035
.L993:
	movq	-392(%rbp), %rax
	movl	$13, 8(%rax)
.L1053:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1048
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -200(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-200(%rbp), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 40(%rax)
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	cmpl	$48, %eax
	jle	.L1049
	jmp	.L1370
.L1048:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1051
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1051:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1053
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1053
.L1370:
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	cmpl	$57, %eax
	jle	.L1054
.L1049:
	movl	$-5, -344(%rbp)
	jmp	.L1035
.L1054:
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	leal	-48(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 40(%rax)
	movq	-392(%rbp), %rax
	movzbl	44(%rax), %eax
	testb	%al, %al
	je	.L1055
	movq	-352(%rbp), %rax
	movq	56(%rax), %r8
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$200000, %eax, %eax
	movl	%eax, %ecx
	movq	-352(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	*%r8
	movq	-392(%rbp), %rdx
	movq	%rax, 3160(%rdx)
	movq	-352(%rbp), %rax
	movq	56(%rax), %rcx
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	addl	$1, %eax
	movl	%eax, %esi
	sarl	%esi
	movq	-352(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movq	%rax, %rdi
	call	*%rcx
	movq	-392(%rbp), %rdx
	movq	%rax, 3168(%rdx)
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	testq	%rax, %rax
	je	.L1056
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rax
	testq	%rax, %rax
	jne	.L994
.L1056:
	movl	$-3, -344(%rbp)
	jmp	.L1035
.L1055:
	movq	-352(%rbp), %rax
	movq	56(%rax), %r8
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$400000, %eax, %eax
	movl	%eax, %ecx
	movq	-352(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	*%r8
	movq	-392(%rbp), %rdx
	movq	%rax, 3152(%rdx)
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rax
	testq	%rax, %rax
	jne	.L994
	movl	$-3, -344(%rbp)
	jmp	.L1035
.L994:
	movq	-392(%rbp), %rax
	movl	$14, 8(%rax)
.L1062:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1057
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -196(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-196(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$23, -35(%rbp)
	je	.L1382
	jmp	.L1371
.L1057:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1060
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1060:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1062
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1062
.L1371:
	cmpb	$49, -35(%rbp)
	je	.L995
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L995:
	movq	-392(%rbp), %rax
	movl	$15, 8(%rax)
.L1067:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1063
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -192(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-192(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$65, -35(%rbp)
	jne	.L1064
	jmp	.L996
.L1063:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1065
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1065:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1067
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1067
.L1064:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L996:
	movq	-392(%rbp), %rax
	movl	$16, 8(%rax)
.L1072:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1068
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -188(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-188(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$89, -35(%rbp)
	jne	.L1069
	jmp	.L997
.L1068:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1070
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1070:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1072
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1072
.L1069:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L997:
	movq	-392(%rbp), %rax
	movl	$17, 8(%rax)
.L1077:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1073
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -184(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-184(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$38, -35(%rbp)
	jne	.L1074
	jmp	.L998
.L1073:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1075
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1075:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1077
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1077
.L1074:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L998:
	movq	-392(%rbp), %rax
	movl	$18, 8(%rax)
.L1082:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1078
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -180(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-180(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$83, -35(%rbp)
	jne	.L1079
	jmp	.L999
.L1078:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1080
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1080:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1082
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1082
.L1079:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L999:
	movq	-392(%rbp), %rax
	movl	$19, 8(%rax)
.L1088:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1083
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -176(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-176(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$89, -35(%rbp)
	jne	.L1084
	jmp	.L1372
.L1083:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1086
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1086:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1088
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1088
.L1084:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1372:
	movq	-392(%rbp), %rax
	movl	48(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 48(%rax)
	movq	-392(%rbp), %rax
	movl	52(%rax), %eax
	cmpl	$1, %eax
	jle	.L1089
	movq	-392(%rbp), %rax
	movl	48(%rax), %edx
	movl	$.LC131, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L1089:
	movq	-392(%rbp), %rax
	movl	$0, 3176(%rax)
.L1000:
	movq	-392(%rbp), %rax
	movl	$20, 8(%rax)
.L1093:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1090
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -172(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-172(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3176(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3176(%rax)
	jmp	.L1001
.L1090:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1091
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1091:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1093
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1093
.L1001:
	movq	-392(%rbp), %rax
	movl	$21, 8(%rax)
.L1097:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1094
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -168(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-168(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3176(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3176(%rax)
	jmp	.L1002
.L1094:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1095
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1095:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1097
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1097
.L1002:
	movq	-392(%rbp), %rax
	movl	$22, 8(%rax)
.L1101:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1098
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -164(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-164(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3176(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3176(%rax)
	jmp	.L1003
.L1098:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1099
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1099:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1101
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1101
.L1003:
	movq	-392(%rbp), %rax
	movl	$23, 8(%rax)
.L1105:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1102
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -160(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-160(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3176(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3176(%rax)
	jmp	.L1004
.L1102:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1103
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1103:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1105
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1105
.L1004:
	movq	-392(%rbp), %rax
	movl	$24, 8(%rax)
.L1109:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1106
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -156(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-156(%rbp), %eax
	movl	%eax, %edx
	movq	-392(%rbp), %rax
	movb	%dl, 20(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 56(%rax)
	jmp	.L1005
.L1106:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1107
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1107:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1109
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1109
.L1005:
	movq	-392(%rbp), %rax
	movl	$25, 8(%rax)
.L1113:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1110
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -152(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-152(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 56(%rax)
	jmp	.L1006
.L1110:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1111
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1111:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1113
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1113
.L1006:
	movq	-392(%rbp), %rax
	movl	$26, 8(%rax)
.L1117:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1114
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -148(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-148(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 56(%rax)
	jmp	.L1007
.L1114:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1115
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1115:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1117
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1117
.L1007:
	movq	-392(%rbp), %rax
	movl	$27, 8(%rax)
.L1123:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1118
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -144(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-144(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 56(%rax)
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	testl	%eax, %eax
	js	.L1119
	jmp	.L1373
.L1118:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1121
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1121:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1123
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1123
.L1119:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1373:
	movq	-392(%rbp), %rax
	movl	56(%rax), %edx
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	addl	$10, %eax
	cmpl	%eax, %edx
	jle	.L1124
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1124:
	movl	$0, -332(%rbp)
	jmp	.L1125
.L1008:
	movq	-392(%rbp), %rax
	movl	$28, 8(%rax)
.L1131:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1126
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -56(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-56(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$1, -35(%rbp)
	je	.L1127
	jmp	.L1374
.L1126:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1129
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1129:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1131
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1131
.L1127:
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movb	$1, 3452(%rdx,%rax)
	jmp	.L1132
.L1374:
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movb	$0, 3452(%rdx,%rax)
.L1132:
	addl	$1, -332(%rbp)
.L1125:
	cmpl	$15, -332(%rbp)
	jle	.L1008
	movl	$0, -332(%rbp)
	jmp	.L1133
.L1134:
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movb	$0, 3196(%rdx,%rax)
	addl	$1, -332(%rbp)
.L1133:
	cmpl	$255, -332(%rbp)
	jle	.L1134
	movl	$0, -332(%rbp)
	jmp	.L1135
.L1144:
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movzbl	3452(%rdx,%rax), %eax
	testb	%al, %al
	je	.L1136
	movl	$0, -328(%rbp)
	jmp	.L1137
.L1009:
	movq	-392(%rbp), %rax
	movl	$29, 8(%rax)
.L1143:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1138
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -52(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-52(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$1, -35(%rbp)
	je	.L1139
	jmp	.L1140
.L1138:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1141
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1141:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1143
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1143
.L1139:
	movl	-332(%rbp), %eax
	sall	$4, %eax
	addl	-328(%rbp), %eax
	movq	-392(%rbp), %rdx
	cltq
	movb	$1, 3196(%rdx,%rax)
.L1140:
	addl	$1, -328(%rbp)
.L1137:
	cmpl	$15, -328(%rbp)
	jle	.L1009
.L1136:
	addl	$1, -332(%rbp)
.L1135:
	cmpl	$15, -332(%rbp)
	jle	.L1144
	movq	-392(%rbp), %rax
	movq	%rax, %rdi
	call	makeMaps_d
	movq	-392(%rbp), %rax
	movl	3192(%rax), %eax
	testl	%eax, %eax
	jne	.L1145
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1145:
	movq	-392(%rbp), %rax
	movl	3192(%rax), %eax
	addl	$2, %eax
	movl	%eax, -320(%rbp)
.L1010:
	movq	-392(%rbp), %rax
	movl	$30, 8(%rax)
.L1151:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$2, %eax
	jle	.L1146
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$3, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$7, %eax
	movl	%eax, -48(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-3(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-48(%rbp), %eax
	movl	%eax, -316(%rbp)
	cmpl	$1, -316(%rbp)
	jle	.L1147
	jmp	.L1375
.L1146:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1149
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1149:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1151
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1151
.L1375:
	cmpl	$6, -316(%rbp)
	jle	.L1011
.L1147:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1011:
	movq	-392(%rbp), %rax
	movl	$31, 8(%rax)
.L1157:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$14, %eax
	jle	.L1152
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$15, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$32767, %eax
	movl	%eax, -44(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-15(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-44(%rbp), %eax
	movl	%eax, -312(%rbp)
	cmpl	$0, -312(%rbp)
	jle	.L1153
	jmp	.L1376
.L1152:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1155
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1155:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1157
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1157
.L1153:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1376:
	movl	$0, -332(%rbp)
	jmp	.L1158
.L1167:
	movl	$0, -328(%rbp)
	jmp	.L1012
.L1384:
	nop
.L1012:
	movq	-392(%rbp), %rax
	movl	$32, 8(%rax)
.L1164:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1159
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -40(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-40(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$0, -35(%rbp)
	je	.L1383
	jmp	.L1377
.L1159:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1162
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1162:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1164
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1164
.L1377:
	addl	$1, -328(%rbp)
	movl	-328(%rbp), %eax
	cmpl	-316(%rbp), %eax
	jl	.L1384
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1383:
	nop
.L1165:
	movl	-328(%rbp), %eax
	movl	%eax, %ecx
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movb	%cl, 25886(%rdx,%rax)
	addl	$1, -332(%rbp)
.L1158:
	movl	-332(%rbp), %eax
	cmpl	-312(%rbp), %eax
	jl	.L1167
	movb	$0, -34(%rbp)
	jmp	.L1168
.L1169:
	movzbl	-34(%rbp), %eax
	cltq
	movzbl	-34(%rbp), %edx
	movb	%dl, -32(%rbp,%rax)
	addb	$1, -34(%rbp)
.L1168:
	movzbl	-34(%rbp), %eax
	cmpl	-316(%rbp), %eax
	jl	.L1169
	movl	$0, -332(%rbp)
	jmp	.L1170
.L1173:
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movzbl	25886(%rdx,%rax), %eax
	movb	%al, -34(%rbp)
	movzbl	-34(%rbp), %eax
	cltq
	movzbl	-32(%rbp,%rax), %eax
	movb	%al, -33(%rbp)
	jmp	.L1171
.L1172:
	movzbl	-34(%rbp), %ecx
	movzbl	-34(%rbp), %eax
	subl	$1, %eax
	cltq
	movzbl	-32(%rbp,%rax), %edx
	movslq	%ecx, %rax
	movb	%dl, -32(%rbp,%rax)
	subb	$1, -34(%rbp)
.L1171:
	cmpb	$0, -34(%rbp)
	jne	.L1172
	movzbl	-33(%rbp), %eax
	movb	%al, -32(%rbp)
	movq	-392(%rbp), %rdx
	movl	-332(%rbp), %eax
	cltq
	movzbl	-33(%rbp), %ecx
	movb	%cl, 7884(%rdx,%rax)
	addl	$1, -332(%rbp)
.L1170:
	movl	-332(%rbp), %eax
	cmpl	-312(%rbp), %eax
	jl	.L1173
	movl	$0, -324(%rbp)
	jmp	.L1174
.L1013:
	movq	-392(%rbp), %rax
	movl	$33, 8(%rax)
.L1179:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$4, %eax
	jle	.L1175
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$5, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$31, %eax
	movl	%eax, -104(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-5(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-104(%rbp), %eax
	movl	%eax, -276(%rbp)
	movl	$0, -332(%rbp)
	jmp	.L1176
.L1175:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1177
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1177:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1179
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1179
.L1194:
	cmpl	$0, -276(%rbp)
	jle	.L1180
	cmpl	$20, -276(%rbp)
	jle	.L1014
.L1180:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1014:
	movq	-392(%rbp), %rax
	movl	$34, 8(%rax)
.L1185:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1181
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -100(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-100(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$0, -35(%rbp)
	je	.L1385
	jmp	.L1015
.L1181:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1183
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1183:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1185
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1185
.L1015:
	movq	-392(%rbp), %rax
	movl	$35, 8(%rax)
.L1192:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1187
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -96(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-96(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$0, -35(%rbp)
	je	.L1188
	jmp	.L1378
.L1187:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1190
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1190:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1192
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1192
.L1188:
	addl	$1, -276(%rbp)
	jmp	.L1194
.L1378:
	subl	$1, -276(%rbp)
	jmp	.L1194
.L1385:
	nop
.L1186:
	movl	-276(%rbp), %eax
	movl	%eax, %edx
	movq	-392(%rbp), %rsi
	movl	-332(%rbp), %eax
	movslq	%eax, %rcx
	movl	-324(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	$43888, %rax
	movb	%dl, (%rax)
	addl	$1, -332(%rbp)
.L1176:
	movl	-332(%rbp), %eax
	cmpl	-320(%rbp), %eax
	jl	.L1194
	addl	$1, -324(%rbp)
.L1174:
	movl	-324(%rbp), %eax
	cmpl	-316(%rbp), %eax
	jl	.L1013
	movl	$0, -324(%rbp)
	jmp	.L1195
.L1200:
	movl	$32, -340(%rbp)
	movl	$0, -336(%rbp)
	movl	$0, -332(%rbp)
	jmp	.L1196
.L1199:
	movq	-392(%rbp), %rcx
	movl	-332(%rbp), %eax
	movslq	%eax, %rdx
	movl	-324(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$43888, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	cmpl	-336(%rbp), %eax
	jle	.L1197
	movq	-392(%rbp), %rcx
	movl	-332(%rbp), %eax
	movslq	%eax, %rdx
	movl	-324(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$43888, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -336(%rbp)
.L1197:
	movq	-392(%rbp), %rcx
	movl	-332(%rbp), %eax
	movslq	%eax, %rdx
	movl	-324(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$43888, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	cmpl	-340(%rbp), %eax
	jge	.L1198
	movq	-392(%rbp), %rcx
	movl	-332(%rbp), %eax
	movslq	%eax, %rdx
	movl	-324(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	$43888, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -340(%rbp)
.L1198:
	addl	$1, -332(%rbp)
.L1196:
	movl	-332(%rbp), %eax
	cmpl	-320(%rbp), %eax
	jl	.L1199
	movq	-392(%rbp), %rax
	leaq	43888(%rax), %rdx
	movl	-324(%rbp), %eax
	cltq
	addq	%rax, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	leaq	(%rdx,%rax), %rcx
	movq	-392(%rbp), %rax
	leaq	57820(%rax), %rdx
	movl	-324(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rsi
	salq	$7, %rsi
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	-392(%rbp), %rax
	leaq	51628(%rax), %rsi
	movl	-324(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rdi
	salq	$7, %rdi
	addq	%rdi, %rax
	addq	%rax, %rsi
	movq	-392(%rbp), %rax
	leaq	45436(%rax), %rdi
	movl	-324(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %r8
	salq	$7, %r8
	addq	%r8, %rax
	addq	%rax, %rdi
	movl	-336(%rbp), %r9d
	movl	-340(%rbp), %r8d
	movl	-320(%rbp), %eax
	movl	%eax, (%rsp)
	call	BZ2_hbCreateDecodeTables
	movq	-392(%rbp), %rax
	movl	-324(%rbp), %edx
	movslq	%edx, %rdx
	leaq	16000(%rdx), %rcx
	movl	-340(%rbp), %edx
	movl	%edx, 12(%rax,%rcx,4)
	addl	$1, -324(%rbp)
.L1195:
	movl	-324(%rbp), %eax
	cmpl	-316(%rbp), %eax
	jl	.L1200
	movq	-392(%rbp), %rax
	movl	3192(%rax), %eax
	addl	$1, %eax
	movl	%eax, -308(%rbp)
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	movl	%eax, -292(%rbp)
	movl	$-1, -304(%rbp)
	movl	$0, -300(%rbp)
	movl	$0, -332(%rbp)
	jmp	.L1201
.L1202:
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$16, %rdx
	movl	$0, 4(%rax,%rdx,4)
	addl	$1, -332(%rbp)
.L1201:
	cmpl	$255, -332(%rbp)
	jle	.L1202
	movl	$4095, -244(%rbp)
	movl	$15, -252(%rbp)
	jmp	.L1203
.L1206:
	movl	$15, -248(%rbp)
	jmp	.L1204
.L1205:
	movl	-252(%rbp), %eax
	movl	%eax, %edx
	sall	$4, %edx
	movl	-248(%rbp), %eax
	leal	(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movl	-244(%rbp), %eax
	cltq
	movb	%cl, 3724(%rdx,%rax)
	subl	$1, -244(%rbp)
	subl	$1, -248(%rbp)
.L1204:
	cmpl	$0, -248(%rbp)
	jns	.L1205
	movl	-244(%rbp), %eax
	leal	1(%rax), %ecx
	movq	-392(%rbp), %rax
	movl	-252(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	%ecx, 12(%rax,%rdx,4)
	subl	$1, -252(%rbp)
.L1203:
	cmpl	$0, -252(%rbp)
	jns	.L1206
	movl	$0, -288(%rbp)
	cmpl	$0, -300(%rbp)
	jne	.L1207
	addl	$1, -304(%rbp)
	movl	-304(%rbp), %eax
	cmpl	-312(%rbp), %eax
	jl	.L1208
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1208:
	movl	$50, -300(%rbp)
	movq	-392(%rbp), %rdx
	movl	-304(%rbp), %eax
	cltq
	movzbl	7884(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -260(%rbp)
	movq	-392(%rbp), %rax
	movl	-260(%rbp), %edx
	movslq	%edx, %rdx
	addq	$16000, %rdx
	movl	12(%rax,%rdx,4), %eax
	movl	%eax, -256(%rbp)
	movq	-392(%rbp), %rax
	leaq	45436(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -376(%rbp)
	movq	-392(%rbp), %rax
	leaq	57820(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -360(%rbp)
	movq	-392(%rbp), %rax
	leaq	51628(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -368(%rbp)
.L1207:
	subl	$1, -300(%rbp)
	movl	-256(%rbp), %eax
	movl	%eax, -272(%rbp)
.L1016:
	movq	-392(%rbp), %rax
	movl	$36, 8(%rax)
.L1213:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	-272(%rbp), %eax
	jl	.L1209
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	-272(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	movl	-272(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	andl	%esi, %eax
	movl	%eax, -92(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	movl	%eax, %edx
	subl	-272(%rbp), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-92(%rbp), %eax
	movl	%eax, -268(%rbp)
	jmp	.L1210
.L1209:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1211
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1211:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1213
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1213
.L1210:
	cmpl	$20, -272(%rbp)
	jle	.L1214
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1214:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-376(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-268(%rbp), %eax
	jge	.L1215
	addl	$1, -272(%rbp)
.L1017:
	movq	-392(%rbp), %rax
	movl	$37, 8(%rax)
.L1219:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1216
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -88(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-88(%rbp), %eax
	movl	%eax, -264(%rbp)
	movl	-268(%rbp), %eax
	addl	%eax, %eax
	orl	-264(%rbp), %eax
	movl	%eax, -268(%rbp)
	jmp	.L1210
.L1216:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1217
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1217:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1219
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1219
.L1215:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	testl	%eax, %eax
	js	.L1220
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	$257, %eax
	jle	.L1221
.L1220:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1221:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cltq
	salq	$2, %rax
	addq	-360(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -296(%rbp)
.L1286:
	movl	-296(%rbp), %eax
	cmpl	-308(%rbp), %eax
	je	.L1386
.L1222:
	cmpl	$0, -296(%rbp)
	je	.L1224
	cmpl	$1, -296(%rbp)
	jne	.L1225
.L1224:
	movl	$-1, -284(%rbp)
	movl	$1, -280(%rbp)
.L1244:
	cmpl	$2097151, -280(%rbp)
	jle	.L1226
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1226:
	cmpl	$0, -296(%rbp)
	jne	.L1227
	movl	-280(%rbp), %eax
	addl	%eax, -284(%rbp)
	jmp	.L1228
.L1227:
	cmpl	$1, -296(%rbp)
	jne	.L1228
	movl	-280(%rbp), %eax
	addl	%eax, %eax
	addl	%eax, -284(%rbp)
.L1228:
	sall	-280(%rbp)
	cmpl	$0, -300(%rbp)
	jne	.L1229
	addl	$1, -304(%rbp)
	movl	-304(%rbp), %eax
	cmpl	-312(%rbp), %eax
	jl	.L1230
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1230:
	movl	$50, -300(%rbp)
	movq	-392(%rbp), %rdx
	movl	-304(%rbp), %eax
	cltq
	movzbl	7884(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -260(%rbp)
	movq	-392(%rbp), %rax
	movl	-260(%rbp), %edx
	movslq	%edx, %rdx
	addq	$16000, %rdx
	movl	12(%rax,%rdx,4), %eax
	movl	%eax, -256(%rbp)
	movq	-392(%rbp), %rax
	leaq	45436(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -376(%rbp)
	movq	-392(%rbp), %rax
	leaq	57820(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -360(%rbp)
	movq	-392(%rbp), %rax
	leaq	51628(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -368(%rbp)
.L1229:
	subl	$1, -300(%rbp)
	movl	-256(%rbp), %eax
	movl	%eax, -272(%rbp)
.L1018:
	movq	-392(%rbp), %rax
	movl	$38, 8(%rax)
.L1235:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	-272(%rbp), %eax
	jl	.L1231
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	-272(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	movl	-272(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	andl	%esi, %eax
	movl	%eax, -68(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	movl	%eax, %edx
	subl	-272(%rbp), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-68(%rbp), %eax
	movl	%eax, -268(%rbp)
	jmp	.L1232
.L1231:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1233
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1233:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1235
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1235
.L1232:
	cmpl	$20, -272(%rbp)
	jle	.L1236
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1236:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-376(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-268(%rbp), %eax
	jge	.L1237
	addl	$1, -272(%rbp)
.L1019:
	movq	-392(%rbp), %rax
	movl	$39, 8(%rax)
.L1241:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1238
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -64(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-64(%rbp), %eax
	movl	%eax, -264(%rbp)
	movl	-268(%rbp), %eax
	addl	%eax, %eax
	orl	-264(%rbp), %eax
	movl	%eax, -268(%rbp)
	jmp	.L1232
.L1238:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1239
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1239:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1241
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1241
.L1237:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	testl	%eax, %eax
	js	.L1242
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	$257, %eax
	jle	.L1243
.L1242:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1243:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cltq
	salq	$2, %rax
	addq	-360(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -296(%rbp)
	cmpl	$0, -296(%rbp)
	je	.L1244
	cmpl	$1, -296(%rbp)
	je	.L1244
	addl	$1, -284(%rbp)
	movq	-392(%rbp), %rax
	movl	7820(%rax), %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %eax
	movzbl	%al, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3468(%rdx,%rax), %eax
	movb	%al, -35(%rbp)
	movzbl	-35(%rbp), %ecx
	movzbl	-35(%rbp), %edx
	movq	-392(%rbp), %rax
	movslq	%edx, %rdx
	addq	$16, %rdx
	movl	4(%rax,%rdx,4), %eax
	movl	%eax, %edx
	addl	-284(%rbp), %edx
	movq	-392(%rbp), %rax
	movslq	%ecx, %rcx
	addq	$16, %rcx
	movl	%edx, 4(%rax,%rcx,4)
	movq	-392(%rbp), %rax
	movzbl	44(%rax), %eax
	testb	%al, %al
	je	.L1250
	jmp	.L1246
.L1248:
	movl	-288(%rbp), %eax
	cmpl	-292(%rbp), %eax
	jl	.L1247
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1247:
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-288(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	addq	%rax, %rdx
	movzbl	-35(%rbp), %eax
	movw	%ax, (%rdx)
	addl	$1, -288(%rbp)
	subl	$1, -284(%rbp)
.L1246:
	cmpl	$0, -284(%rbp)
	jg	.L1248
	jmp	.L1380
.L1252:
	movl	-288(%rbp), %eax
	cmpl	-292(%rbp), %eax
	jl	.L1251
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1251:
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rax
	movl	-288(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rax, %rdx
	movzbl	-35(%rbp), %eax
	movl	%eax, (%rdx)
	addl	$1, -288(%rbp)
	subl	$1, -284(%rbp)
.L1250:
	cmpl	$0, -284(%rbp)
	jg	.L1252
	jmp	.L1253
.L1380:
	jmp	.L1253
.L1225:
	movl	-288(%rbp), %eax
	cmpl	-292(%rbp), %eax
	jl	.L1254
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1254:
	movl	-296(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -220(%rbp)
	cmpl	$15, -220(%rbp)
	ja	.L1255
	movq	-392(%rbp), %rax
	movl	7820(%rax), %eax
	movl	%eax, -228(%rbp)
	movl	-228(%rbp), %eax
	addl	-220(%rbp), %eax
	movq	-392(%rbp), %rdx
	mov	%eax, %eax
	movzbl	3724(%rdx,%rax), %eax
	movb	%al, -35(%rbp)
	jmp	.L1256
.L1257:
	movl	-228(%rbp), %eax
	addl	-220(%rbp), %eax
	movl	%eax, -80(%rbp)
	movl	-80(%rbp), %eax
	subl	$1, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movl	-80(%rbp), %eax
	cltq
	movb	%cl, 3724(%rdx,%rax)
	movl	-80(%rbp), %eax
	leal	-1(%rax), %esi
	movl	-80(%rbp), %eax
	subl	$2, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movslq	%esi, %rax
	movb	%cl, 3724(%rdx,%rax)
	movl	-80(%rbp), %eax
	leal	-2(%rax), %esi
	movl	-80(%rbp), %eax
	subl	$3, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movslq	%esi, %rax
	movb	%cl, 3724(%rdx,%rax)
	movl	-80(%rbp), %eax
	leal	-3(%rax), %esi
	movl	-80(%rbp), %eax
	subl	$4, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movslq	%esi, %rax
	movb	%cl, 3724(%rdx,%rax)
	subl	$4, -220(%rbp)
.L1256:
	cmpl	$3, -220(%rbp)
	ja	.L1257
	jmp	.L1258
.L1259:
	movl	-228(%rbp), %eax
	movl	%eax, %esi
	addl	-220(%rbp), %esi
	movl	-228(%rbp), %eax
	addl	-220(%rbp), %eax
	subl	$1, %eax
	movq	-392(%rbp), %rdx
	mov	%eax, %eax
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	mov	%esi, %eax
	movb	%cl, 3724(%rdx,%rax)
	subl	$1, -220(%rbp)
.L1258:
	cmpl	$0, -220(%rbp)
	jne	.L1259
	movq	-392(%rbp), %rdx
	movl	-228(%rbp), %eax
	cltq
	movzbl	-35(%rbp), %ecx
	movb	%cl, 3724(%rdx,%rax)
	jmp	.L1260
.L1255:
	movl	-220(%rbp), %eax
	shrl	$4, %eax
	movl	%eax, -224(%rbp)
	movl	-220(%rbp), %eax
	andl	$15, %eax
	movl	%eax, -76(%rbp)
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %eax
	addl	-76(%rbp), %eax
	movl	%eax, -228(%rbp)
	movq	-392(%rbp), %rdx
	movl	-228(%rbp), %eax
	cltq
	movzbl	3724(%rdx,%rax), %eax
	movb	%al, -35(%rbp)
	jmp	.L1261
.L1262:
	movl	-228(%rbp), %eax
	subl	$1, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movl	-228(%rbp), %eax
	cltq
	movb	%cl, 3724(%rdx,%rax)
	subl	$1, -228(%rbp)
.L1261:
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %eax
	cmpl	-228(%rbp), %eax
	jl	.L1262
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %eax
	leal	1(%rax), %ecx
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	%ecx, 12(%rax,%rdx,4)
	jmp	.L1263
.L1264:
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %eax
	leal	-1(%rax), %ecx
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	%ecx, 12(%rax,%rdx,4)
	movq	-392(%rbp), %rax
	movl	-224(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %esi
	movl	-224(%rbp), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %eax
	addl	$15, %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movslq	%esi, %rax
	movb	%cl, 3724(%rdx,%rax)
	subl	$1, -224(%rbp)
.L1263:
	cmpl	$0, -224(%rbp)
	jg	.L1264
	movq	-392(%rbp), %rax
	movl	7820(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 7820(%rax)
	movq	-392(%rbp), %rax
	movl	7820(%rax), %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	-35(%rbp), %ecx
	movb	%cl, 3724(%rdx,%rax)
	movq	-392(%rbp), %rax
	movl	7820(%rax), %eax
	testl	%eax, %eax
	jne	.L1260
	movl	$4095, -232(%rbp)
	movl	$15, -240(%rbp)
	jmp	.L1265
.L1268:
	movl	$15, -236(%rbp)
	jmp	.L1266
.L1267:
	movq	-392(%rbp), %rax
	movl	-240(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	12(%rax,%rdx,4), %eax
	addl	-236(%rbp), %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3724(%rdx,%rax), %ecx
	movq	-392(%rbp), %rdx
	movl	-232(%rbp), %eax
	cltq
	movb	%cl, 3724(%rdx,%rax)
	subl	$1, -232(%rbp)
	subl	$1, -236(%rbp)
.L1266:
	cmpl	$0, -236(%rbp)
	jns	.L1267
	movl	-232(%rbp), %eax
	leal	1(%rax), %ecx
	movq	-392(%rbp), %rax
	movl	-240(%rbp), %edx
	movslq	%edx, %rdx
	addq	$1952, %rdx
	movl	%ecx, 12(%rax,%rdx,4)
	subl	$1, -240(%rbp)
.L1265:
	cmpl	$0, -240(%rbp)
	jns	.L1268
.L1260:
	movzbl	-35(%rbp), %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3468(%rdx,%rax), %eax
	movzbl	%al, %eax
	movq	-392(%rbp), %rdx
	movslq	%eax, %rcx
	addq	$16, %rcx
	movl	4(%rdx,%rcx,4), %edx
	leal	1(%rdx), %ecx
	movq	-392(%rbp), %rdx
	cltq
	addq	$16, %rax
	movl	%ecx, 4(%rdx,%rax,4)
	movq	-392(%rbp), %rax
	movzbl	44(%rax), %eax
	testb	%al, %al
	je	.L1269
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-288(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	leaq	(%rax,%rdx), %rcx
	movzbl	-35(%rbp), %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3468(%rdx,%rax), %eax
	movzbl	%al, %eax
	movw	%ax, (%rcx)
	jmp	.L1270
.L1269:
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rax
	movl	-288(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	leaq	(%rax,%rdx), %rcx
	movzbl	-35(%rbp), %eax
	movq	-392(%rbp), %rdx
	cltq
	movzbl	3468(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, (%rcx)
.L1270:
	addl	$1, -288(%rbp)
	cmpl	$0, -300(%rbp)
	jne	.L1271
	addl	$1, -304(%rbp)
	movl	-304(%rbp), %eax
	cmpl	-312(%rbp), %eax
	jl	.L1272
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1272:
	movl	$50, -300(%rbp)
	movq	-392(%rbp), %rdx
	movl	-304(%rbp), %eax
	cltq
	movzbl	7884(%rdx,%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -260(%rbp)
	movq	-392(%rbp), %rax
	movl	-260(%rbp), %edx
	movslq	%edx, %rdx
	addq	$16000, %rdx
	movl	12(%rax,%rdx,4), %eax
	movl	%eax, -256(%rbp)
	movq	-392(%rbp), %rax
	leaq	45436(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -376(%rbp)
	movq	-392(%rbp), %rax
	leaq	57820(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -360(%rbp)
	movq	-392(%rbp), %rax
	leaq	51628(%rax), %rdx
	movl	-260(%rbp), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rcx
	salq	$7, %rcx
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, -368(%rbp)
.L1271:
	subl	$1, -300(%rbp)
	movl	-256(%rbp), %eax
	movl	%eax, -272(%rbp)
.L1020:
	movq	-392(%rbp), %rax
	movl	$40, 8(%rax)
.L1277:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	-272(%rbp), %eax
	jl	.L1273
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	-272(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	movl	-272(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	andl	%esi, %eax
	movl	%eax, -72(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	movl	%eax, %edx
	subl	-272(%rbp), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-72(%rbp), %eax
	movl	%eax, -268(%rbp)
	jmp	.L1274
.L1273:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1275
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1275:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1277
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1277
.L1274:
	cmpl	$20, -272(%rbp)
	jle	.L1278
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1278:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-376(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-268(%rbp), %eax
	jge	.L1279
	addl	$1, -272(%rbp)
.L1021:
	movq	-392(%rbp), %rax
	movl	$41, 8(%rax)
.L1283:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.L1280
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$1, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -60(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-60(%rbp), %eax
	movl	%eax, -264(%rbp)
	movl	-268(%rbp), %eax
	addl	%eax, %eax
	orl	-264(%rbp), %eax
	movl	%eax, -268(%rbp)
	jmp	.L1274
.L1280:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1281
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1281:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1283
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1283
.L1279:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	testl	%eax, %eax
	js	.L1284
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	cmpl	$257, %eax
	jle	.L1285
.L1284:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1285:
	movl	-272(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-368(%rbp), %rax
	movl	(%rax), %eax
	movl	-268(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cltq
	salq	$2, %rax
	addq	-360(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -296(%rbp)
	nop
.L1253:
	jmp	.L1286
.L1386:
	nop
.L1379:
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	testl	%eax, %eax
	js	.L1287
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	cmpl	-288(%rbp), %eax
	jl	.L1288
.L1287:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1288:
	movl	$0, -332(%rbp)
	jmp	.L1289
.L1292:
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$16, %rdx
	movl	4(%rax,%rdx,4), %eax
	testl	%eax, %eax
	js	.L1290
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$16, %rdx
	movl	4(%rax,%rdx,4), %eax
	cmpl	-288(%rbp), %eax
	jle	.L1291
.L1290:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1291:
	addl	$1, -332(%rbp)
.L1289:
	cmpl	$255, -332(%rbp)
	jle	.L1292
	movq	-392(%rbp), %rax
	movl	$0, 1096(%rax)
	movl	$1, -332(%rbp)
	jmp	.L1293
.L1294:
	movl	-332(%rbp), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movslq	%edx, %rdx
	addq	$16, %rdx
	movl	4(%rax,%rdx,4), %edx
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %ecx
	movslq	%ecx, %rcx
	addq	$272, %rcx
	movl	%edx, 8(%rax,%rcx,4)
	addl	$1, -332(%rbp)
.L1293:
	cmpl	$256, -332(%rbp)
	jle	.L1294
	movl	$1, -332(%rbp)
	jmp	.L1295
.L1296:
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$272, %rdx
	movl	8(%rax,%rdx,4), %edx
	movl	-332(%rbp), %eax
	leal	-1(%rax), %ecx
	movq	-392(%rbp), %rax
	movslq	%ecx, %rcx
	addq	$272, %rcx
	movl	8(%rax,%rcx,4), %eax
	leal	(%rdx,%rax), %ecx
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$272, %rdx
	movl	%ecx, 8(%rax,%rdx,4)
	addl	$1, -332(%rbp)
.L1295:
	cmpl	$256, -332(%rbp)
	jle	.L1296
	movl	$0, -332(%rbp)
	jmp	.L1297
.L1300:
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$272, %rdx
	movl	8(%rax,%rdx,4), %eax
	testl	%eax, %eax
	js	.L1298
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$272, %rdx
	movl	8(%rax,%rdx,4), %eax
	cmpl	-288(%rbp), %eax
	jle	.L1299
.L1298:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1299:
	addl	$1, -332(%rbp)
.L1297:
	cmpl	$256, -332(%rbp)
	jle	.L1300
	movl	$1, -332(%rbp)
	jmp	.L1301
.L1303:
	movl	-332(%rbp), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movslq	%edx, %rdx
	addq	$272, %rdx
	movl	8(%rax,%rdx,4), %edx
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %ecx
	movslq	%ecx, %rcx
	addq	$272, %rcx
	movl	8(%rax,%rcx,4), %eax
	cmpl	%eax, %edx
	jle	.L1302
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1302:
	addl	$1, -332(%rbp)
.L1301:
	cmpl	$256, -332(%rbp)
	jle	.L1303
	movq	-392(%rbp), %rax
	movl	$0, 16(%rax)
	movq	-392(%rbp), %rax
	movb	$0, 12(%rax)
	movq	-392(%rbp), %rax
	movl	$-1, 3184(%rax)
	movq	-392(%rbp), %rax
	movl	$2, 8(%rax)
	movq	-392(%rbp), %rax
	movl	52(%rax), %eax
	cmpl	$1, %eax
	jle	.L1304
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC132, %eax
	movq	%rdx, %rcx
	movl	$6, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L1304:
	movq	-392(%rbp), %rax
	movzbl	44(%rax), %eax
	testb	%al, %al
	je	.L1305
	movl	$0, -332(%rbp)
	jmp	.L1306
.L1307:
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	$272, %rdx
	movl	8(%rax,%rdx,4), %edx
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %ecx
	movslq	%ecx, %rcx
	addq	$528, %rcx
	movl	%edx, 12(%rax,%rcx,4)
	addl	$1, -332(%rbp)
.L1306:
	cmpl	$256, -332(%rbp)
	jle	.L1307
	movl	$0, -332(%rbp)
	jmp	.L1308
.L1311:
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	addq	%rax, %rdx
	movzbl	-35(%rbp), %ecx
	movq	-392(%rbp), %rax
	movslq	%ecx, %rcx
	addq	$528, %rcx
	movl	12(%rax,%rcx,4), %eax
	movw	%ax, (%rdx)
	movl	-332(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	jne	.L1309
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rdx
	movl	-332(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rax, %rdx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movl	-332(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movl	$-16, %esi
	andl	%eax, %esi
	movzbl	-35(%rbp), %ecx
	movq	-392(%rbp), %rax
	movslq	%ecx, %rcx
	addq	$528, %rcx
	movl	12(%rax,%rcx,4), %eax
	sarl	$16, %eax
	orl	%esi, %eax
	movb	%al, (%rdx)
	jmp	.L1310
.L1309:
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rdx
	movl	-332(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rax, %rdx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movl	-332(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movl	$15, %esi
	andl	%eax, %esi
	movzbl	-35(%rbp), %ecx
	movq	-392(%rbp), %rax
	movslq	%ecx, %rcx
	addq	$528, %rcx
	movl	12(%rax,%rcx,4), %eax
	sarl	$16, %eax
	sall	$4, %eax
	orl	%esi, %eax
	movb	%al, (%rdx)
.L1310:
	movzbl	-35(%rbp), %eax
	movq	-392(%rbp), %rdx
	movslq	%eax, %rcx
	addq	$528, %rcx
	movl	12(%rdx,%rcx,4), %edx
	leal	1(%rdx), %ecx
	movq	-392(%rbp), %rdx
	cltq
	addq	$528, %rax
	movl	%ecx, 12(%rdx,%rax,4)
	addl	$1, -332(%rbp)
.L1308:
	movl	-332(%rbp), %eax
	cmpl	-288(%rbp), %eax
	jl	.L1311
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	movl	%eax, -332(%rbp)
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movl	-332(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movl	-332(%rbp), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%edx, %eax
	movl	%eax, -328(%rbp)
.L1314:
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-328(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movl	-328(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movl	-328(%rbp), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%edx, %eax
	movl	%eax, -84(%rbp)
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rax
	movl	-328(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	addq	%rax, %rdx
	movl	-332(%rbp), %eax
	movw	%ax, (%rdx)
	movl	-328(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	jne	.L1312
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rdx
	movl	-328(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rax, %rdx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movl	-328(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movl	%eax, %ecx
	andl	$-16, %ecx
	movl	-332(%rbp), %eax
	sarl	$16, %eax
	orl	%ecx, %eax
	movb	%al, (%rdx)
	jmp	.L1313
.L1312:
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rdx
	movl	-328(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rax, %rdx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movl	-328(%rbp), %eax
	sarl	%eax
	cltq
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	movl	-332(%rbp), %eax
	sarl	$16, %eax
	sall	$4, %eax
	orl	%ecx, %eax
	movb	%al, (%rdx)
.L1313:
	movl	-328(%rbp), %eax
	movl	%eax, -332(%rbp)
	movl	-84(%rbp), %eax
	movl	%eax, -328(%rbp)
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	cmpl	-332(%rbp), %eax
	jne	.L1314
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	movl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 1092(%rax)
	movq	-392(%rbp), %rax
	movzbl	20(%rax), %eax
	testb	%al, %al
	je	.L1315
	movq	-392(%rbp), %rax
	movl	$0, 24(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 28(%rax)
	movq	-392(%rbp), %rax
	movl	60(%rax), %edx
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1316
	movl	$1, %eax
	jmp	.L1317
.L1316:
	movq	-392(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movq	-392(%rbp), %rdx
	movl	%eax, 64(%rdx)
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-392(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1318
	movq	-392(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-392(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-392(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1318
	movq	-392(%rbp), %rax
	movl	$0, 28(%rax)
.L1318:
	movq	-392(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-392(%rbp), %rax
	movl	64(%rax), %edx
	movq	-392(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	xorl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1319
.L1315:
	movq	-392(%rbp), %rax
	movl	60(%rax), %edx
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1320
	movl	$1, %eax
	jmp	.L1317
.L1320:
	movq	-392(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movq	-392(%rbp), %rdx
	movl	%eax, 64(%rdx)
	movq	-392(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-392(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 1092(%rax)
	jmp	.L1319
.L1305:
	movl	$0, -332(%rbp)
	jmp	.L1321
.L1322:
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rax
	movl	-332(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movl	(%rax), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rdx
	movzbl	-35(%rbp), %ecx
	movq	-392(%rbp), %rax
	movslq	%ecx, %rcx
	addq	$272, %rcx
	movl	8(%rax,%rcx,4), %eax
	cltq
	salq	$2, %rax
	addq	%rax, %rdx
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rcx
	movzbl	-35(%rbp), %esi
	movq	-392(%rbp), %rax
	movslq	%esi, %rsi
	addq	$272, %rsi
	movl	8(%rax,%rsi,4), %eax
	cltq
	salq	$2, %rax
	addq	%rcx, %rax
	movl	(%rax), %ecx
	movl	-332(%rbp), %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdx)
	movzbl	-35(%rbp), %eax
	movq	-392(%rbp), %rdx
	movslq	%eax, %rcx
	addq	$272, %rcx
	movl	8(%rdx,%rcx,4), %edx
	leal	1(%rdx), %ecx
	movq	-392(%rbp), %rdx
	cltq
	addq	$272, %rax
	movl	%ecx, 8(%rdx,%rax,4)
	addl	$1, -332(%rbp)
.L1321:
	movl	-332(%rbp), %eax
	cmpl	-288(%rbp), %eax
	jl	.L1322
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	56(%rax), %eax
	cltq
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 1092(%rax)
	movq	-392(%rbp), %rax
	movzbl	20(%rax), %eax
	testb	%al, %al
	je	.L1323
	movq	-392(%rbp), %rax
	movl	$0, 24(%rax)
	movq	-392(%rbp), %rax
	movl	$0, 28(%rax)
	movq	-392(%rbp), %rax
	movl	60(%rax), %edx
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1324
	movl	$1, %eax
	jmp	.L1317
.L1324:
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	movzbl	%al, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 64(%rax)
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-392(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1325
	movq	-392(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-392(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-392(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1325
	movq	-392(%rbp), %rax
	movl	$0, 28(%rax)
.L1325:
	movq	-392(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-392(%rbp), %rax
	movl	64(%rax), %edx
	movq	-392(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	xorl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1319
.L1323:
	movq	-392(%rbp), %rax
	movl	60(%rax), %edx
	movq	-392(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1326
	movl	$1, %eax
	jmp	.L1317
.L1326:
	movq	-392(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	movzbl	%al, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 64(%rax)
	movq	-392(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-392(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 1092(%rax)
.L1319:
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1382:
	nop
.L1022:
	movq	-392(%rbp), %rax
	movl	$42, 8(%rax)
.L1331:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1327
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -140(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-140(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$114, -35(%rbp)
	jne	.L1328
	jmp	.L1023
.L1327:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1329
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1329:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1331
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1331
.L1328:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1023:
	movq	-392(%rbp), %rax
	movl	$43, 8(%rax)
.L1336:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1332
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -136(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-136(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$69, -35(%rbp)
	jne	.L1333
	jmp	.L1024
.L1332:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1334
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1334:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1336
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1336
.L1333:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1024:
	movq	-392(%rbp), %rax
	movl	$44, 8(%rax)
.L1341:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1337
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -132(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-132(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$56, -35(%rbp)
	jne	.L1338
	jmp	.L1025
.L1337:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1339
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1339:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1341
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1341
.L1338:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1025:
	movq	-392(%rbp), %rax
	movl	$45, 8(%rax)
.L1346:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1342
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -128(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-128(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$80, -35(%rbp)
	jne	.L1343
	jmp	.L1026
.L1342:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1344
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1344:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1346
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1346
.L1343:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1026:
	movq	-392(%rbp), %rax
	movl	$46, 8(%rax)
.L1352:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1347
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -124(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-124(%rbp), %eax
	movb	%al, -35(%rbp)
	cmpb	$-112, -35(%rbp)
	jne	.L1348
	jmp	.L1381
.L1347:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1350
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1350:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1352
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1352
.L1348:
	movl	$-4, -344(%rbp)
	jmp	.L1035
.L1381:
	movq	-392(%rbp), %rax
	movl	$0, 3180(%rax)
.L1027:
	movq	-392(%rbp), %rax
	movl	$47, 8(%rax)
.L1356:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1353
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -120(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-120(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3180(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3180(%rax)
	jmp	.L1028
.L1353:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1354
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1354:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1356
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1356
.L1028:
	movq	-392(%rbp), %rax
	movl	$48, 8(%rax)
.L1360:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1357
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -116(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-116(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3180(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3180(%rax)
	jmp	.L1029
.L1357:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1358
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1358:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1360
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1360
.L1029:
	movq	-392(%rbp), %rax
	movl	$49, 8(%rax)
.L1364:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1361
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -112(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-112(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3180(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3180(%rax)
	jmp	.L1030
.L1361:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1362
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1362:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1364
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1364
.L1030:
	movq	-392(%rbp), %rax
	movl	$50, 8(%rax)
.L1368:
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	cmpl	$7, %eax
	jle	.L1365
	movq	-392(%rbp), %rax
	movl	32(%rax), %edx
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	subl	$8, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$255, %eax
	movl	%eax, -108(%rbp)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	-8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movl	-108(%rbp), %eax
	movb	%al, -35(%rbp)
	movq	-392(%rbp), %rax
	movl	3180(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movzbl	-35(%rbp), %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 3180(%rax)
	movq	-392(%rbp), %rax
	movl	$1, 8(%rax)
	movl	$4, -344(%rbp)
	jmp	.L1035
.L1365:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1366
	movl	$0, -344(%rbp)
	jmp	.L1035
.L1366:
	movq	-392(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	orl	%eax, %edx
	movq	-392(%rbp), %rax
	movl	%edx, 32(%rax)
	movq	-392(%rbp), %rax
	movl	36(%rax), %eax
	leal	8(%rax), %edx
	movq	-392(%rbp), %rax
	movl	%edx, 36(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1368
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1368
.L989:
	movl	$4001, %edi
	call	BZ2_bz__AssertH__fail
	movl	$4002, %edi
	call	BZ2_bz__AssertH__fail
.L1035:
	movq	-392(%rbp), %rax
	movl	-332(%rbp), %edx
	movl	%edx, 64036(%rax)
	movq	-392(%rbp), %rax
	movl	-328(%rbp), %edx
	movl	%edx, 64040(%rax)
	movq	-392(%rbp), %rax
	movl	-324(%rbp), %edx
	movl	%edx, 64044(%rax)
	movq	-392(%rbp), %rax
	movl	-320(%rbp), %edx
	movl	%edx, 64048(%rax)
	movq	-392(%rbp), %rax
	movl	-316(%rbp), %edx
	movl	%edx, 64052(%rax)
	movq	-392(%rbp), %rax
	movl	-312(%rbp), %edx
	movl	%edx, 64056(%rax)
	movq	-392(%rbp), %rax
	movl	-308(%rbp), %edx
	movl	%edx, 64060(%rax)
	movq	-392(%rbp), %rax
	movl	-304(%rbp), %edx
	movl	%edx, 64064(%rax)
	movq	-392(%rbp), %rax
	movl	-300(%rbp), %edx
	movl	%edx, 64068(%rax)
	movq	-392(%rbp), %rax
	movl	-296(%rbp), %edx
	movl	%edx, 64072(%rax)
	movq	-392(%rbp), %rax
	movl	-292(%rbp), %edx
	movl	%edx, 64076(%rax)
	movq	-392(%rbp), %rax
	movl	-288(%rbp), %edx
	movl	%edx, 64080(%rax)
	movq	-392(%rbp), %rax
	movl	-284(%rbp), %edx
	movl	%edx, 64084(%rax)
	movq	-392(%rbp), %rax
	movl	-280(%rbp), %edx
	movl	%edx, 64088(%rax)
	movq	-392(%rbp), %rax
	movl	-276(%rbp), %edx
	movl	%edx, 64092(%rax)
	movq	-392(%rbp), %rax
	movl	-216(%rbp), %edx
	movl	%edx, 64096(%rax)
	movq	-392(%rbp), %rax
	movl	-272(%rbp), %edx
	movl	%edx, 64100(%rax)
	movq	-392(%rbp), %rax
	movl	-268(%rbp), %edx
	movl	%edx, 64104(%rax)
	movq	-392(%rbp), %rax
	movl	-264(%rbp), %edx
	movl	%edx, 64108(%rax)
	movq	-392(%rbp), %rax
	movl	-260(%rbp), %edx
	movl	%edx, 64112(%rax)
	movq	-392(%rbp), %rax
	movl	-256(%rbp), %edx
	movl	%edx, 64116(%rax)
	movq	-392(%rbp), %rax
	movq	-376(%rbp), %rdx
	movq	%rdx, 64120(%rax)
	movq	-392(%rbp), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, 64128(%rax)
	movq	-392(%rbp), %rax
	movq	-360(%rbp), %rdx
	movq	%rdx, 64136(%rax)
	movl	-344(%rbp), %eax
.L1317:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L1369
	call	__stack_chk_fail
.L1369:
	addq	$408, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE66:
	.size	BZ2_decompress, .-BZ2_decompress
	.section	.rodata
	.align 8
.LC133:
	.ascii	"\n\nbzip2/libbzip2: internal error number %d.\nThis is a bug"
	.ascii	" in bzip2/libbzip2, %s.\nPlease report it to me at: jseward@"
	.ascii	"bzip.org.  If this happened\nwhen you were using some progra"
	.ascii	"m which use"
	.string	"s libbzip2 as a\ncomponent, you should also report this bug to the author(s)\nof that program.  Please make an effort to report this bug;\ntimely and accurate bug reports eventually lead to higher\nquality software.  Thanks.  Julian Seward, 10 December 2007.\n\n"
	.align 8
.LC134:
	.ascii	"\n*** A special note about internal error number 1007 ***\n\n"
	.ascii	"Experience suggests that a common cause of i.e. 1007\nis unr"
	.ascii	"eliable memory or other hardware.  The 1007 assertion\njust "
	.ascii	"happens to cross-check the results of huge numbers of\nmemor"
	.ascii	"y reads/writes, and so acts (unintendedly) as a stress\ntest"
	.ascii	" of your memory system.\n\nI suggest the following: try comp"
	.ascii	"ressing the file again,\npossibly monitoring progress in det"
	.ascii	"ail with the -vv flag.\n\n* If the error cannot be reproduce"
	.ascii	"d, and/or happens at different\n  points in compression, you"
	.ascii	" may have a flaky memory system.\n  Try a memory-test progra"
	.ascii	"m.  I have used Memtest86\n  (www.memtest86.com).  At the ti"
	.ascii	"me of writing it is free (GPLd).\n  Memtest86 tests memory m"
	.ascii	"uch more thorougly than your BIOSs\n  power-on test, and may"
	.ascii	" find failures that the BIOS doesn't"
	.string	".\n\n* If the error can be repeatably reproduced, this is a bug in\n  bzip2, and I would very much like to hear about it.  Please\n  let me know, and, ideally, save a copy of the file causing the\n  problem -- without which I will be unable to investigate it.\n\n"
	.text
	.globl	BZ2_bz__AssertH__fail
	.type	BZ2_bz__AssertH__fail, @function
BZ2_bz__AssertH__fail:
.LFB67:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	call	BZ2_bzlibVersion
	movq	%rax, %rcx
	movl	$.LC133, %esi
	movq	stderr(%rip), %rax
	movl	-4(%rbp), %edx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	cmpl	$1007, -4(%rbp)
	jne	.L1388
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC134, %eax
	movq	%rdx, %rcx
	movl	$1056, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
.L1388:
	movl	$3, %edi
	call	exit
	.cfi_endproc
.LFE67:
	.size	BZ2_bz__AssertH__fail, .-BZ2_bz__AssertH__fail
	.type	bz_config_ok, @function
bz_config_ok:
.LFB68:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE68:
	.size	bz_config_ok, .-bz_config_ok
	.type	default_bzalloc, @function
default_bzalloc:
.LFB69:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	movl	-28(%rbp), %eax
	imull	-32(%rbp), %eax
	cltq
	movq	%rax, %rdi
	call	malloc
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE69:
	.size	default_bzalloc, .-default_bzalloc
	.type	default_bzfree, @function
default_bzfree:
.LFB70:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L1391
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	free
.L1391:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE70:
	.size	default_bzfree, .-default_bzfree
	.type	prepare_new_block, @function
prepare_new_block:
.LFB71:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	$0, 108(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 116(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 120(%rax)
	movq	-24(%rbp), %rax
	movl	$-1, 648(%rax)
	movl	$0, -4(%rbp)
	jmp	.L1394
.L1395:
	movq	-24(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	movb	$0, 128(%rdx,%rax)
	addl	$1, -4(%rbp)
.L1394:
	cmpl	$255, -4(%rbp)
	jle	.L1395
	movq	-24(%rbp), %rax
	movl	660(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 660(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE71:
	.size	prepare_new_block, .-prepare_new_block
	.type	init_RL, @function
init_RL:
.LFB72:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$256, 92(%rax)
	movq	-8(%rbp), %rax
	movl	$0, 96(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE72:
	.size	init_RL, .-init_RL
	.type	isempty_RL, @function
isempty_RL:
.LFB73:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	$255, %eax
	ja	.L1398
	movq	-8(%rbp), %rax
	movl	96(%rax), %eax
	testl	%eax, %eax
	jle	.L1398
	movl	$0, %eax
	jmp	.L1399
.L1398:
	movl	$1, %eax
.L1399:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE73:
	.size	isempty_RL, .-isempty_RL
	.globl	BZ2_bzCompressInit
	.type	BZ2_bzCompressInit, @function
BZ2_bzCompressInit:
.LFB74:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	movl	%ecx, -36(%rbp)
	call	bz_config_ok
	testl	%eax, %eax
	jne	.L1401
	movl	$-9, %eax
	jmp	.L1402
.L1401:
	cmpq	$0, -24(%rbp)
	je	.L1403
	cmpl	$0, -28(%rbp)
	jle	.L1403
	cmpl	$9, -28(%rbp)
	jg	.L1403
	cmpl	$0, -36(%rbp)
	js	.L1403
	cmpl	$250, -36(%rbp)
	jle	.L1404
.L1403:
	movl	$-2, %eax
	jmp	.L1402
.L1404:
	cmpl	$0, -36(%rbp)
	jne	.L1405
	movl	$30, -36(%rbp)
.L1405:
	movq	-24(%rbp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	jne	.L1406
	movq	-24(%rbp), %rax
	movq	$default_bzalloc, 56(%rax)
.L1406:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	jne	.L1407
	movq	-24(%rbp), %rax
	movq	$default_bzfree, 64(%rax)
.L1407:
	movq	-24(%rbp), %rax
	movq	56(%rax), %rcx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movl	$55768, %esi
	movq	%rax, %rdi
	call	*%rcx
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L1408
	movl	$-3, %eax
	jmp	.L1402
.L1408:
	movq	-16(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-16(%rbp), %rax
	movq	$0, 24(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 32(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 40(%rax)
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	movl	%eax, -4(%rbp)
	movq	-24(%rbp), %rax
	movq	56(%rax), %r8
	movl	-4(%rbp), %eax
	sall	$2, %eax
	movl	%eax, %ecx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	*%r8
	movq	-16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	-24(%rbp), %rax
	movq	56(%rax), %rcx
	movl	-4(%rbp), %eax
	addl	$34, %eax
	leal	0(,%rax,4), %esi
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movq	%rax, %rdi
	call	*%rcx
	movq	-16(%rbp), %rdx
	movq	%rax, 32(%rdx)
	movq	-24(%rbp), %rax
	movq	56(%rax), %rcx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movl	$262148, %esi
	movq	%rax, %rdi
	call	*%rcx
	movq	-16(%rbp), %rdx
	movq	%rax, 40(%rdx)
	movq	-16(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1409
	movq	-16(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1409
	movq	-16(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L1410
.L1409:
	movq	-16(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1411
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-16(%rbp), %rax
	movq	24(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1411:
	movq	-16(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1412
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-16(%rbp), %rax
	movq	32(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1412:
	movq	-16(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L1413
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-16(%rbp), %rax
	movq	40(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1413:
	cmpq	$0, -16(%rbp)
	je	.L1414
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1414:
	movl	$-3, %eax
	jmp	.L1402
.L1410:
	movq	-16(%rbp), %rax
	movl	$0, 660(%rax)
	movq	-16(%rbp), %rax
	movl	$2, 12(%rax)
	movq	-16(%rbp), %rax
	movl	$2, 8(%rax)
	movq	-16(%rbp), %rax
	movl	$0, 652(%rax)
	movq	-16(%rbp), %rax
	movl	-28(%rbp), %edx
	movl	%edx, 664(%rax)
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	leal	-19(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 112(%rax)
	movq	-16(%rbp), %rax
	movl	-32(%rbp), %edx
	movl	%edx, 656(%rax)
	movq	-16(%rbp), %rax
	movl	-36(%rbp), %edx
	movl	%edx, 88(%rax)
	movq	-16(%rbp), %rax
	movq	32(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 64(%rax)
	movq	-16(%rbp), %rax
	movq	24(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 72(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 80(%rax)
	movq	-16(%rbp), %rax
	movq	24(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 56(%rax)
	movq	-24(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 12(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 16(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 36(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 40(%rax)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	init_RL
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	prepare_new_block
	movl	$0, %eax
.L1402:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE74:
	.size	BZ2_bzCompressInit, .-BZ2_bzCompressInit
	.type	add_pair_to_block, @function
add_pair_to_block:
.LFB75:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	movb	%al, -1(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L1416
.L1417:
	movq	-24(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-24(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movzbl	-1(%rbp), %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, 648(%rax)
	addl	$1, -8(%rbp)
.L1416:
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	-8(%rbp), %eax
	jg	.L1417
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	movq	-24(%rbp), %rdx
	mov	%eax, %eax
	movb	$1, 128(%rdx,%rax)
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	$2, %eax
	je	.L1420
	cmpl	$3, %eax
	je	.L1421
	cmpl	$1, %eax
	jne	.L1423
.L1419:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	jmp	.L1415
.L1420:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	jmp	.L1415
.L1421:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	jmp	.L1415
.L1423:
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	subl	$4, %eax
	movq	-24(%rbp), %rdx
	cltq
	movb	$1, 128(%rdx,%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	subl	$4, %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	nop
.L1415:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE75:
	.size	add_pair_to_block, .-add_pair_to_block
	.type	flush_RL, @function
flush_RL:
.LFB76:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	$255, %eax
	ja	.L1425
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	add_pair_to_block
.L1425:
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	init_RL
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE76:
	.size	flush_RL, .-flush_RL
	.type	copy_input_until_stop, @function
copy_input_until_stop:
.LFB77:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movq	%rdi, -24(%rbp)
	movb	$0, -3(%rbp)
	movq	-24(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$2, %eax
	jne	.L1427
	jmp	.L1437
.L1450:
	nop
.L1437:
	movq	-24(%rbp), %rax
	movl	108(%rax), %edx
	movq	-24(%rbp), %rax
	movl	112(%rax), %eax
	cmpl	%eax, %edx
	jge	.L1448
.L1428:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L1449
.L1430:
	movb	$1, -3(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -12(%rbp)
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	-12(%rbp), %eax
	je	.L1431
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	$1, %eax
	jne	.L1431
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	movb	%al, -2(%rbp)
	movq	-24(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-24(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movzbl	-2(%rbp), %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, 648(%rax)
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	movq	-24(%rbp), %rdx
	mov	%eax, %eax
	movb	$1, 128(%rdx,%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-2(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, 92(%rax)
	jmp	.L1432
.L1431:
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	-12(%rbp), %eax
	jne	.L1433
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	$255, %eax
	jne	.L1434
.L1433:
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	$255, %eax
	ja	.L1435
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	add_pair_to_block
.L1435:
	movq	-24(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, 92(%rax)
	movq	-24(%rbp), %rax
	movl	$1, 96(%rax)
	jmp	.L1432
.L1434:
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 96(%rax)
.L1432:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1450
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
	jmp	.L1450
.L1427:
	movq	-24(%rbp), %rax
	movl	108(%rax), %edx
	movq	-24(%rbp), %rax
	movl	112(%rax), %eax
	cmpl	%eax, %edx
	jge	.L1451
.L1439:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L1452
.L1440:
	movq	-24(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L1453
.L1441:
	movb	$1, -3(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -8(%rbp)
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	-8(%rbp), %eax
	je	.L1442
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	$1, %eax
	jne	.L1442
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	movb	%al, -1(%rbp)
	movq	-24(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-24(%rbp), %rax
	movl	648(%rax), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movzbl	-1(%rbp), %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, 648(%rax)
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	movq	-24(%rbp), %rdx
	mov	%eax, %eax
	movb	$1, 128(%rdx,%rax)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	cltq
	addq	%rax, %rdx
	movzbl	-1(%rbp), %eax
	movb	%al, (%rdx)
	movq	-24(%rbp), %rax
	movl	108(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 108(%rax)
	movq	-24(%rbp), %rax
	movl	-8(%rbp), %edx
	movl	%edx, 92(%rax)
	jmp	.L1443
.L1442:
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	-8(%rbp), %eax
	jne	.L1444
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	cmpl	$255, %eax
	jne	.L1445
.L1444:
	movq	-24(%rbp), %rax
	movl	92(%rax), %eax
	cmpl	$255, %eax
	ja	.L1446
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	add_pair_to_block
.L1446:
	movq	-24(%rbp), %rax
	movl	-8(%rbp), %edx
	movl	%edx, 92(%rax)
	movq	-24(%rbp), %rax
	movl	$1, 96(%rax)
	jmp	.L1443
.L1445:
	movq	-24(%rbp), %rax
	movl	96(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 96(%rax)
.L1443:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %edx
	subl	$1, %edx
	movl	%edx, 8(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %edx
	addl	$1, %edx
	movl	%edx, 12(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L1447
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	16(%rax), %edx
	addl	$1, %edx
	movl	%edx, 16(%rax)
.L1447:
	movq	-24(%rbp), %rax
	movl	16(%rax), %eax
	leal	-1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 16(%rax)
	jmp	.L1427
.L1448:
	nop
	jmp	.L1438
.L1449:
	nop
	jmp	.L1438
.L1451:
	nop
	jmp	.L1438
.L1452:
	nop
	jmp	.L1438
.L1453:
	nop
.L1438:
	movzbl	-3(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE77:
	.size	copy_input_until_stop, .-copy_input_until_stop
	.type	copy_output_until_stop, @function
copy_output_until_stop:
.LFB78:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movb	$0, -1(%rbp)
	jmp	.L1459
.L1462:
	nop
.L1459:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %eax
	testl	%eax, %eax
	je	.L1460
.L1455:
	movq	-24(%rbp), %rax
	movl	120(%rax), %edx
	movq	-24(%rbp), %rax
	movl	116(%rax), %eax
	cmpl	%eax, %edx
	jge	.L1461
.L1457:
	movb	$1, -1(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	-24(%rbp), %rdx
	movq	80(%rdx), %rcx
	movq	-24(%rbp), %rdx
	movl	120(%rdx), %edx
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movq	-24(%rbp), %rax
	movl	120(%rax), %eax
	leal	1(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 120(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %edx
	subl	$1, %edx
	movl	%edx, 32(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, 24(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %edx
	addl	$1, %edx
	movl	%edx, 36(%rax)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jne	.L1462
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movl	40(%rax), %edx
	addl	$1, %edx
	movl	%edx, 40(%rax)
	jmp	.L1462
.L1460:
	nop
	jmp	.L1456
.L1461:
	nop
.L1456:
	movzbl	-1(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE78:
	.size	copy_output_until_stop, .-copy_output_until_stop
	.type	handle_compress, @function
handle_compress:
.LFB79:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movb	$0, -2(%rbp)
	movb	$0, -1(%rbp)
	movq	-24(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -16(%rbp)
	jmp	.L1471
.L1478:
	nop
.L1471:
	movq	-16(%rbp), %rax
	movl	12(%rax), %eax
	cmpl	$1, %eax
	jne	.L1464
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	copy_output_until_stop
	orb	%al, -1(%rbp)
	movq	-16(%rbp), %rax
	movl	120(%rax), %edx
	movq	-16(%rbp), %rax
	movl	116(%rax), %eax
	cmpl	%eax, %edx
	jl	.L1475
.L1465:
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$4, %eax
	jne	.L1467
	movq	-16(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1467
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	isempty_RL
	testb	%al, %al
	jne	.L1476
.L1467:
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	prepare_new_block
	movq	-16(%rbp), %rax
	movl	$2, 12(%rax)
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$3, %eax
	jne	.L1464
	movq	-16(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1464
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	isempty_RL
	testb	%al, %al
	jne	.L1477
.L1464:
	movq	-16(%rbp), %rax
	movl	12(%rax), %eax
	cmpl	$2, %eax
	jne	.L1478
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	copy_input_until_stop
	orb	%al, -2(%rbp)
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$2, %eax
	je	.L1469
	movq	-16(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1469
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	flush_RL
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$4, %eax
	sete	%al
	movzbl	%al, %edx
	movq	-16(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	BZ2_compressBlock
	movq	-16(%rbp), %rax
	movl	$1, 12(%rax)
	jmp	.L1478
.L1469:
	movq	-16(%rbp), %rax
	movl	108(%rax), %edx
	movq	-16(%rbp), %rax
	movl	112(%rax), %eax
	cmpl	%eax, %edx
	jl	.L1470
	movq	-16(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	BZ2_compressBlock
	movq	-16(%rbp), %rax
	movl	$1, 12(%rax)
	jmp	.L1478
.L1470:
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1478
	jmp	.L1466
.L1475:
	nop
	jmp	.L1466
.L1476:
	nop
	jmp	.L1466
.L1477:
	nop
.L1466:
	cmpb	$0, -2(%rbp)
	jne	.L1472
	cmpb	$0, -1(%rbp)
	je	.L1473
.L1472:
	movl	$1, %eax
	jmp	.L1474
.L1473:
	movl	$0, %eax
.L1474:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE79:
	.size	handle_compress, .-handle_compress
	.globl	BZ2_bzCompress
	.type	BZ2_bzCompress, @function
BZ2_bzCompress:
.LFB80:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L1480
	movl	$-2, %eax
	jmp	.L1481
.L1480:
	movq	-24(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L1482
	movl	$-2, %eax
	jmp	.L1481
.L1482:
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	-24(%rbp), %rax
	je	.L1483
	movl	$-2, %eax
	jmp	.L1481
.L1483:
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$2, %eax
	je	.L1486
	cmpl	$2, %eax
	jg	.L1489
	cmpl	$1, %eax
	je	.L1485
	jmp	.L1484
.L1489:
	cmpl	$3, %eax
	je	.L1487
	cmpl	$4, %eax
	je	.L1488
	jmp	.L1484
.L1485:
	movl	$-1, %eax
	jmp	.L1481
.L1486:
	cmpl	$0, -28(%rbp)
	jne	.L1490
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	handle_compress
	movb	%al, -1(%rbp)
	cmpb	$0, -1(%rbp)
	je	.L1491
	movl	$1, %eax
	jmp	.L1492
.L1491:
	movl	$-2, %eax
.L1492:
	jmp	.L1481
.L1490:
	cmpl	$1, -28(%rbp)
	jne	.L1493
	movq	-24(%rbp), %rax
	movl	8(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-16(%rbp), %rax
	movl	$3, 8(%rax)
	jmp	.L1483
.L1493:
	cmpl	$2, -28(%rbp)
	jne	.L1494
	movq	-24(%rbp), %rax
	movl	8(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-16(%rbp), %rax
	movl	$4, 8(%rax)
	jmp	.L1483
.L1494:
	movl	$-2, %eax
	jmp	.L1481
.L1487:
	cmpl	$1, -28(%rbp)
	je	.L1495
	movl	$-1, %eax
	jmp	.L1481
.L1495:
	movq	-16(%rbp), %rax
	movl	16(%rax), %edx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	cmpl	%eax, %edx
	je	.L1496
	movl	$-1, %eax
	jmp	.L1481
.L1496:
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	handle_compress
	movb	%al, -1(%rbp)
	movq	-16(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1497
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	isempty_RL
	testb	%al, %al
	je	.L1497
	movq	-16(%rbp), %rax
	movl	120(%rax), %edx
	movq	-16(%rbp), %rax
	movl	116(%rax), %eax
	cmpl	%eax, %edx
	jge	.L1498
.L1497:
	movl	$2, %eax
	jmp	.L1481
.L1498:
	movq	-16(%rbp), %rax
	movl	$2, 8(%rax)
	movl	$1, %eax
	jmp	.L1481
.L1488:
	cmpl	$2, -28(%rbp)
	je	.L1499
	movl	$-1, %eax
	jmp	.L1481
.L1499:
	movq	-16(%rbp), %rax
	movl	16(%rax), %edx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movl	8(%rax), %eax
	cmpl	%eax, %edx
	je	.L1500
	movl	$-1, %eax
	jmp	.L1481
.L1500:
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	handle_compress
	movb	%al, -1(%rbp)
	cmpb	$0, -1(%rbp)
	jne	.L1501
	movl	$-1, %eax
	jmp	.L1481
.L1501:
	movq	-16(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1502
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	isempty_RL
	testb	%al, %al
	je	.L1502
	movq	-16(%rbp), %rax
	movl	120(%rax), %edx
	movq	-16(%rbp), %rax
	movl	116(%rax), %eax
	cmpl	%eax, %edx
	jge	.L1503
.L1502:
	movl	$3, %eax
	jmp	.L1481
.L1503:
	movq	-16(%rbp), %rax
	movl	$1, 8(%rax)
	movl	$4, %eax
	jmp	.L1481
.L1484:
	movl	$0, %eax
.L1481:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE80:
	.size	BZ2_bzCompress, .-BZ2_bzCompress
	.globl	BZ2_bzCompressEnd
	.type	BZ2_bzCompressEnd, @function
BZ2_bzCompressEnd:
.LFB81:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L1505
	movl	$-2, %eax
	jmp	.L1506
.L1505:
	movq	-24(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L1507
	movl	$-2, %eax
	jmp	.L1506
.L1507:
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	cmpq	-24(%rbp), %rax
	je	.L1508
	movl	$-2, %eax
	jmp	.L1506
.L1508:
	movq	-8(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1509
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	24(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1509:
	movq	-8(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1510
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	32(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1510:
	movq	-8(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L1511
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	40(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1511:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-24(%rbp), %rax
	movq	48(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
	movq	-24(%rbp), %rax
	movq	$0, 48(%rax)
	movl	$0, %eax
.L1506:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE81:
	.size	BZ2_bzCompressEnd, .-BZ2_bzCompressEnd
	.globl	BZ2_bzDecompressInit
	.type	BZ2_bzDecompressInit, @function
BZ2_bzDecompressInit:
.LFB82:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	call	bz_config_ok
	testl	%eax, %eax
	jne	.L1513
	movl	$-9, %eax
	jmp	.L1514
.L1513:
	cmpq	$0, -24(%rbp)
	jne	.L1515
	movl	$-2, %eax
	jmp	.L1514
.L1515:
	cmpl	$0, -32(%rbp)
	je	.L1516
	cmpl	$1, -32(%rbp)
	je	.L1516
	movl	$-2, %eax
	jmp	.L1514
.L1516:
	cmpl	$0, -28(%rbp)
	js	.L1517
	cmpl	$4, -28(%rbp)
	jle	.L1518
.L1517:
	movl	$-2, %eax
	jmp	.L1514
.L1518:
	movq	-24(%rbp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	jne	.L1519
	movq	-24(%rbp), %rax
	movq	$default_bzalloc, 56(%rax)
.L1519:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	jne	.L1520
	movq	-24(%rbp), %rax
	movq	$default_bzfree, 64(%rax)
.L1520:
	movq	-24(%rbp), %rax
	movq	56(%rax), %rcx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movl	$1, %edx
	movl	$64144, %esi
	movq	%rax, %rdi
	call	*%rcx
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L1521
	movl	$-3, %eax
	jmp	.L1514
.L1521:
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	-8(%rbp), %rax
	movl	$10, 8(%rax)
	movq	-8(%rbp), %rax
	movl	$0, 36(%rax)
	movq	-8(%rbp), %rax
	movl	$0, 32(%rax)
	movq	-8(%rbp), %rax
	movl	$0, 3188(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 12(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 16(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 36(%rax)
	movq	-24(%rbp), %rax
	movl	$0, 40(%rax)
	movl	-32(%rbp), %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 44(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 3168(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 3160(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 3152(%rax)
	movq	-8(%rbp), %rax
	movl	$0, 48(%rax)
	movq	-8(%rbp), %rax
	movl	-28(%rbp), %edx
	movl	%edx, 52(%rax)
	movl	$0, %eax
.L1514:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE82:
	.size	BZ2_bzDecompressInit, .-BZ2_bzDecompressInit
	.type	unRLE_obuf_to_output_FAST, @function
unRLE_obuf_to_output_FAST:
.LFB83:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rax
	movzbl	20(%rax), %eax
	testb	%al, %al
	je	.L1523
	.cfi_offset 3, -24
	jmp	.L1529
.L1574:
	nop
.L1529:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %eax
	testl	%eax, %eax
	jne	.L1524
	movl	$0, %eax
	jmp	.L1525
.L1524:
	movq	-80(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L1573
.L1526:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	-80(%rbp), %rdx
	movzbl	12(%rdx), %edx
	movb	%dl, (%rax)
	movq	-80(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-80(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	-80(%rbp), %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%eax, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 3184(%rax)
	movq	-80(%rbp), %rax
	movl	16(%rax), %eax
	leal	-1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, 24(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %edx
	subl	$1, %edx
	movl	%edx, 32(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %edx
	addl	$1, %edx
	movl	%edx, 36(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jne	.L1574
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	40(%rax), %edx
	addl	$1, %edx
	movl	%edx, 40(%rax)
	jmp	.L1574
.L1573:
	nop
.L1572:
	movq	-80(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-80(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jne	.L1530
	movl	$0, %eax
	jmp	.L1525
.L1530:
	movq	-80(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-80(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jle	.L1531
	movl	$1, %eax
	jmp	.L1525
.L1531:
	movq	-80(%rbp), %rax
	movl	$1, 16(%rax)
	movq	-80(%rbp), %rax
	movl	64(%rax), %eax
	movl	%eax, %edx
	movq	-80(%rbp), %rax
	movb	%dl, 12(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %edx
	movq	-80(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1532
	movl	$1, %eax
	jmp	.L1525
.L1532:
	movq	-80(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1533
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1533
	movq	-80(%rbp), %rax
	movl	$0, 28(%rax)
.L1533:
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-80(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1575
.L1534:
	movzbl	-9(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1536
	movzbl	-9(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1535
.L1536:
	movq	-80(%rbp), %rax
	movl	$2, 16(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %edx
	movq	-80(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1537
	movl	$1, %eax
	jmp	.L1525
.L1537:
	movq	-80(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1538
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1538
	movq	-80(%rbp), %rax
	movl	$0, 28(%rax)
.L1538:
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-80(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1576
.L1539:
	movzbl	-9(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1540
	movzbl	-9(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1535
.L1540:
	movq	-80(%rbp), %rax
	movl	$3, 16(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %edx
	movq	-80(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1541
	movl	$1, %eax
	jmp	.L1525
.L1541:
	movq	-80(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1542
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1542
	movq	-80(%rbp), %rax
	movl	$0, 28(%rax)
.L1542:
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-80(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1577
.L1543:
	movzbl	-9(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1544
	movzbl	-9(%rbp), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1535
.L1544:
	movq	-80(%rbp), %rax
	movl	60(%rax), %edx
	movq	-80(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1545
	movl	$1, %eax
	jmp	.L1525
.L1545:
	movq	-80(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1546
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1546
	movq	-80(%rbp), %rax
	movl	$0, 28(%rax)
.L1546:
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 1092(%rax)
	movzbl	-9(%rbp), %eax
	leal	4(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %edx
	movq	-80(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1547
	movl	$1, %eax
	jmp	.L1525
.L1547:
	movq	-80(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	salq	$2, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movzbl	%al, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 64(%rax)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, %edx
	shrl	$8, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1548
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-80(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1548
	movq	-80(%rbp), %rax
	movl	$0, 28(%rax)
.L1548:
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-80(%rbp), %rax
	movl	64(%rax), %edx
	movq	-80(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	xorl	%eax, %edx
	movq	-80(%rbp), %rax
	movl	%edx, 64(%rax)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, 1092(%rax)
	jmp	.L1529
.L1575:
	nop
	jmp	.L1535
.L1576:
	nop
	jmp	.L1535
.L1577:
	nop
.L1535:
	jmp	.L1529
.L1523:
	movq	-80(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, -52(%rbp)
	movq	-80(%rbp), %rax
	movzbl	12(%rax), %eax
	movb	%al, -10(%rbp)
	movq	-80(%rbp), %rax
	movl	16(%rax), %eax
	movl	%eax, -48(%rbp)
	movq	-80(%rbp), %rax
	movl	1092(%rax), %eax
	movl	%eax, -44(%rbp)
	movq	-80(%rbp), %rax
	movl	64(%rax), %eax
	movl	%eax, -40(%rbp)
	movq	-80(%rbp), %rax
	movq	3152(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	-80(%rbp), %rax
	movl	60(%rax), %eax
	movl	%eax, -36(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %eax
	movl	%eax, -32(%rbp)
	movq	-80(%rbp), %rax
	movl	40(%rax), %eax
	movl	%eax, -28(%rbp)
	movl	-32(%rbp), %eax
	movl	%eax, -24(%rbp)
	movq	-80(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
.L1570:
	cmpl	$0, -48(%rbp)
	jle	.L1549
.L1554:
	cmpl	$0, -32(%rbp)
	je	.L1578
.L1550:
	cmpl	$1, -48(%rbp)
	je	.L1579
.L1552:
	movq	-72(%rbp), %rax
	movzbl	-10(%rbp), %edx
	movb	%dl, (%rax)
	movl	-52(%rbp), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movl	-52(%rbp), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movzbl	-10(%rbp), %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%edx, %eax
	movl	%eax, -52(%rbp)
	subl	$1, -48(%rbp)
	addq	$1, -72(%rbp)
	subl	$1, -32(%rbp)
	jmp	.L1554
.L1579:
	nop
	jmp	.L1553
.L1580:
	nop
.L1553:
	cmpl	$0, -32(%rbp)
	jne	.L1555
	movl	$1, -48(%rbp)
	jmp	.L1551
.L1555:
	movq	-72(%rbp), %rax
	movzbl	-10(%rbp), %edx
	movb	%dl, (%rax)
	movl	-52(%rbp), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movl	-52(%rbp), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movzbl	-10(%rbp), %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%edx, %eax
	movl	%eax, -52(%rbp)
	addq	$1, -72(%rbp)
	subl	$1, -32(%rbp)
.L1549:
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jle	.L1556
	movl	$1, %eax
	jmp	.L1525
.L1556:
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L1557
	movl	$0, -48(%rbp)
	jmp	.L1551
.L1557:
	movl	-40(%rbp), %eax
	movb	%al, -10(%rbp)
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	cmpl	-36(%rbp), %eax
	ja	.L1558
	movl	$1, %eax
	jmp	.L1525
.L1558:
	mov	-36(%rbp), %eax
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	movb	%al, -9(%rbp)
	shrl	$8, -36(%rbp)
	addl	$1, -44(%rbp)
	movzbl	-9(%rbp), %eax
	cmpl	-40(%rbp), %eax
	je	.L1559
	movzbl	-9(%rbp), %eax
	movl	%eax, -40(%rbp)
	jmp	.L1553
.L1559:
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	je	.L1580
.L1560:
	movl	$2, -48(%rbp)
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	cmpl	-36(%rbp), %eax
	ja	.L1561
	movl	$1, %eax
	jmp	.L1525
.L1561:
	mov	-36(%rbp), %eax
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	movb	%al, -9(%rbp)
	shrl	$8, -36(%rbp)
	addl	$1, -44(%rbp)
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	je	.L1581
.L1562:
	movzbl	-9(%rbp), %eax
	cmpl	-40(%rbp), %eax
	je	.L1564
	movzbl	-9(%rbp), %eax
	movl	%eax, -40(%rbp)
	jmp	.L1563
.L1564:
	movl	$3, -48(%rbp)
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	cmpl	-36(%rbp), %eax
	ja	.L1565
	movl	$1, %eax
	jmp	.L1525
.L1565:
	mov	-36(%rbp), %eax
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	movb	%al, -9(%rbp)
	shrl	$8, -36(%rbp)
	addl	$1, -44(%rbp)
	movl	-44(%rbp), %eax
	cmpl	-20(%rbp), %eax
	je	.L1582
.L1566:
	movzbl	-9(%rbp), %eax
	cmpl	-40(%rbp), %eax
	je	.L1567
	movzbl	-9(%rbp), %eax
	movl	%eax, -40(%rbp)
	jmp	.L1563
.L1567:
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	cmpl	-36(%rbp), %eax
	ja	.L1568
	movl	$1, %eax
	jmp	.L1525
.L1568:
	mov	-36(%rbp), %eax
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	movb	%al, -9(%rbp)
	shrl	$8, -36(%rbp)
	addl	$1, -44(%rbp)
	movzbl	-9(%rbp), %eax
	addl	$4, %eax
	movl	%eax, -48(%rbp)
	movl	-28(%rbp), %eax
	imull	$100000, %eax, %eax
	cmpl	-36(%rbp), %eax
	ja	.L1569
	movl	$1, %eax
	jmp	.L1525
.L1569:
	mov	-36(%rbp), %eax
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	movzbl	%al, %eax
	movl	%eax, -40(%rbp)
	shrl	$8, -36(%rbp)
	addl	$1, -44(%rbp)
	jmp	.L1570
.L1581:
	nop
	jmp	.L1563
.L1582:
	nop
.L1563:
	jmp	.L1570
.L1578:
	nop
.L1551:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %eax
	movl	%eax, -16(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	(%rdx), %rdx
	movl	36(%rdx), %edx
	movl	-32(%rbp), %ecx
	movl	-24(%rbp), %esi
	movl	%esi, %ebx
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	addl	%ecx, %edx
	movl	%edx, 36(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %eax
	cmpl	-16(%rbp), %eax
	jae	.L1571
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	40(%rax), %edx
	addl	$1, %edx
	movl	%edx, 40(%rax)
.L1571:
	movq	-80(%rbp), %rax
	movl	-52(%rbp), %edx
	movl	%edx, 3184(%rax)
	movq	-80(%rbp), %rax
	movzbl	-10(%rbp), %edx
	movb	%dl, 12(%rax)
	movq	-80(%rbp), %rax
	movl	-48(%rbp), %edx
	movl	%edx, 16(%rax)
	movq	-80(%rbp), %rax
	movl	-44(%rbp), %edx
	movl	%edx, 1092(%rax)
	movq	-80(%rbp), %rax
	movl	-40(%rbp), %edx
	movl	%edx, 64(%rax)
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%rdx, 3152(%rax)
	movq	-80(%rbp), %rax
	movl	-36(%rbp), %edx
	movl	%edx, 60(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	-72(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	-32(%rbp), %edx
	movl	%edx, 32(%rax)
	movl	$0, %eax
.L1525:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE83:
	.size	unRLE_obuf_to_output_FAST, .-unRLE_obuf_to_output_FAST
	.globl	BZ2_indexIntoF
	.type	BZ2_indexIntoF, @function
BZ2_indexIntoF:
.LFB84:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$0, -12(%rbp)
	movl	$256, -8(%rbp)
.L1586:
	movl	-8(%rbp), %eax
	movl	-12(%rbp), %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-20(%rbp), %eax
	jg	.L1584
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)
	jmp	.L1585
.L1584:
	movl	-4(%rbp), %eax
	movl	%eax, -8(%rbp)
.L1585:
	movl	-12(%rbp), %eax
	movl	-8(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$1, %eax
	jne	.L1586
	movl	-12(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE84:
	.size	BZ2_indexIntoF, .-BZ2_indexIntoF
	.type	unRLE_obuf_to_output_SMALL, @function
unRLE_obuf_to_output_SMALL:
.LFB85:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rax
	movzbl	20(%rax), %eax
	testb	%al, %al
	je	.L1588
	.cfi_offset 3, -24
	jmp	.L1594
.L1635:
	nop
.L1594:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %eax
	testl	%eax, %eax
	jne	.L1589
	movl	$0, %eax
	jmp	.L1590
.L1589:
	movq	-32(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L1634
.L1591:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	-32(%rbp), %rdx
	movzbl	12(%rdx), %edx
	movb	%dl, (%rax)
	movq	-32(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-32(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	-32(%rbp), %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 3184(%rax)
	movq	-32(%rbp), %rax
	movl	16(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, 24(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %edx
	subl	$1, %edx
	movl	%edx, 32(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %edx
	addl	$1, %edx
	movl	%edx, 36(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jne	.L1635
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	40(%rax), %edx
	addl	$1, %edx
	movl	%edx, 40(%rax)
	jmp	.L1635
.L1634:
	nop
.L1632:
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jne	.L1595
	movl	$0, %eax
	jmp	.L1590
.L1595:
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jle	.L1596
	movl	$1, %eax
	jmp	.L1590
.L1596:
	movq	-32(%rbp), %rax
	movl	$1, 16(%rax)
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	movl	%eax, %edx
	movq	-32(%rbp), %rax
	movb	%dl, 12(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1597
	movl	$1, %eax
	jmp	.L1590
.L1597:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1598
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1598
	movq	-32(%rbp), %rax
	movl	$0, 28(%rax)
.L1598:
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1636
.L1599:
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1601
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1600
.L1601:
	movq	-32(%rbp), %rax
	movl	$2, 16(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1602
	movl	$1, %eax
	jmp	.L1590
.L1602:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1603
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1603
	movq	-32(%rbp), %rax
	movl	$0, 28(%rax)
.L1603:
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1637
.L1604:
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1605
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1600
.L1605:
	movq	-32(%rbp), %rax
	movl	$3, 16(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1606
	movl	$1, %eax
	jmp	.L1590
.L1606:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1607
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1607
	movq	-32(%rbp), %rax
	movl	$0, 28(%rax)
.L1607:
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1638
.L1608:
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1609
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1600
.L1609:
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1610
	movl	$1, %eax
	jmp	.L1590
.L1610:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1611
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1611
	movq	-32(%rbp), %rax
	movl	$0, 28(%rax)
.L1611:
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	movzbl	-9(%rbp), %eax
	xorl	%edx, %eax
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movzbl	-9(%rbp), %eax
	leal	4(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1612
	movl	$1, %eax
	jmp	.L1590
.L1612:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movq	-32(%rbp), %rdx
	movl	%eax, 64(%rdx)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1613
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cltq
	movl	BZ2_rNums(,%rax,4), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 28(%rax)
	movq	-32(%rbp), %rax
	movl	28(%rax), %eax
	cmpl	$512, %eax
	jne	.L1613
	movq	-32(%rbp), %rax
	movl	$0, 28(%rax)
.L1613:
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 24(%rax)
	movq	-32(%rbp), %rax
	movl	64(%rax), %edx
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	cmpl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	xorl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	jmp	.L1594
.L1636:
	nop
	jmp	.L1600
.L1637:
	nop
	jmp	.L1600
.L1638:
	nop
.L1600:
	jmp	.L1594
.L1640:
	nop
.L1588:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %eax
	testl	%eax, %eax
	jne	.L1614
	movl	$0, %eax
	jmp	.L1590
.L1614:
	movq	-32(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L1639
.L1615:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	-32(%rbp), %rdx
	movzbl	12(%rdx), %edx
	movb	%dl, (%rax)
	movq	-32(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %edx
	sall	$8, %edx
	movq	-32(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movq	-32(%rbp), %rax
	movzbl	12(%rax), %eax
	movzbl	%al, %eax
	xorl	%ecx, %eax
	mov	%eax, %eax
	movl	BZ2_crc32Table(,%rax,4), %eax
	xorl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 3184(%rax)
	movq	-32(%rbp), %rax
	movl	16(%rax), %eax
	leal	-1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdx
	addq	$1, %rdx
	movq	%rdx, 24(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	32(%rax), %edx
	subl	$1, %edx
	movl	%edx, 32(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %edx
	addl	$1, %edx
	movl	%edx, 36(%rax)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jne	.L1640
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	40(%rax), %edx
	addl	$1, %edx
	movl	%edx, 40(%rax)
	jmp	.L1640
.L1639:
	nop
.L1633:
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jne	.L1618
	movl	$0, %eax
	jmp	.L1590
.L1618:
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jle	.L1619
	movl	$1, %eax
	jmp	.L1590
.L1619:
	movq	-32(%rbp), %rax
	movl	$1, 16(%rax)
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	movl	%eax, %edx
	movq	-32(%rbp), %rax
	movb	%dl, 12(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1620
	movl	$1, %eax
	jmp	.L1590
.L1620:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1641
.L1621:
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1623
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1622
.L1623:
	movq	-32(%rbp), %rax
	movl	$2, 16(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1624
	movl	$1, %eax
	jmp	.L1590
.L1624:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1642
.L1625:
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1626
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1622
.L1626:
	movq	-32(%rbp), %rax
	movl	$3, 16(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1627
	movl	$1, %eax
	jmp	.L1590
.L1627:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-32(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	je	.L1643
.L1628:
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	64(%rax), %eax
	cmpl	%eax, %edx
	je	.L1629
	movzbl	-9(%rbp), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 64(%rax)
	jmp	.L1622
.L1629:
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1630
	movl	$1, %eax
	jmp	.L1590
.L1630:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movb	%al, -9(%rbp)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	movzbl	-9(%rbp), %eax
	leal	4(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-32(%rbp), %rax
	movl	60(%rax), %edx
	movq	-32(%rbp), %rax
	movl	40(%rax), %eax
	imull	$100000, %eax, %eax
	cmpl	%eax, %edx
	jb	.L1631
	movl	$1, %eax
	jmp	.L1590
.L1631:
	movq	-32(%rbp), %rax
	leaq	1096(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	BZ2_indexIntoF
	movq	-32(%rbp), %rdx
	movl	%eax, 64(%rdx)
	movq	-32(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	mov	%eax, %eax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %edx
	movq	-32(%rbp), %rax
	movq	3168(%rax), %rcx
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	shrl	%eax
	mov	%eax, %eax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %esi
	movq	-32(%rbp), %rax
	movl	60(%rax), %eax
	sall	$2, %eax
	andl	$4, %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	andl	$15, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-32(%rbp), %rax
	movl	%edx, 60(%rax)
	movq	-32(%rbp), %rax
	movl	1092(%rax), %eax
	leal	1(%rax), %edx
	movq	-32(%rbp), %rax
	movl	%edx, 1092(%rax)
	jmp	.L1588
.L1641:
	nop
	jmp	.L1622
.L1642:
	nop
	jmp	.L1622
.L1643:
	nop
.L1622:
	jmp	.L1588
.L1590:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE85:
	.size	unRLE_obuf_to_output_SMALL, .-unRLE_obuf_to_output_SMALL
	.section	.rodata
.LC135:
	.string	" {0x%08x, 0x%08x}"
	.align 8
.LC136:
	.string	"\n    combined CRCs: stored = 0x%08x, computed = 0x%08x"
	.text
	.globl	BZ2_bzDecompress
	.type	BZ2_bzDecompress, @function
BZ2_bzDecompress:
.LFB86:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L1645
	movl	$-2, %eax
	jmp	.L1646
.L1645:
	movq	-24(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L1647
	movl	$-2, %eax
	jmp	.L1646
.L1647:
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	-24(%rbp), %rax
	je	.L1648
	movl	$-2, %eax
	jmp	.L1646
.L1662:
	nop
.L1648:
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$1, %eax
	jne	.L1649
	movl	$-1, %eax
	jmp	.L1646
.L1649:
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$2, %eax
	jne	.L1650
	movq	-16(%rbp), %rax
	movzbl	44(%rax), %eax
	testb	%al, %al
	je	.L1651
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	unRLE_obuf_to_output_SMALL
	movb	%al, -1(%rbp)
	jmp	.L1652
.L1651:
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	unRLE_obuf_to_output_FAST
	movb	%al, -1(%rbp)
.L1652:
	cmpb	$0, -1(%rbp)
	je	.L1653
	movl	$-4, %eax
	jmp	.L1646
.L1653:
	movq	-16(%rbp), %rax
	movl	1092(%rax), %edx
	movq	-16(%rbp), %rax
	movl	64080(%rax), %eax
	addl	$1, %eax
	cmpl	%eax, %edx
	jne	.L1654
	movq	-16(%rbp), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1654
	movq	-16(%rbp), %rax
	movl	3184(%rax), %eax
	movl	%eax, %edx
	notl	%edx
	movq	-16(%rbp), %rax
	movl	%edx, 3184(%rax)
	movq	-16(%rbp), %rax
	movl	52(%rax), %eax
	cmpl	$2, %eax
	jle	.L1655
	movq	-16(%rbp), %rax
	movl	3184(%rax), %ecx
	movq	-16(%rbp), %rax
	movl	3176(%rax), %edx
	movl	$.LC135, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L1655:
	movq	-16(%rbp), %rax
	movl	52(%rax), %eax
	cmpl	$1, %eax
	jle	.L1656
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$93, %edi
	call	fputc
.L1656:
	movq	-16(%rbp), %rax
	movl	3184(%rax), %edx
	movq	-16(%rbp), %rax
	movl	3176(%rax), %eax
	cmpl	%eax, %edx
	je	.L1657
	movl	$-4, %eax
	jmp	.L1646
.L1657:
	movq	-16(%rbp), %rax
	movl	3188(%rax), %eax
	movl	%eax, %edx
	roll	%edx
	movq	-16(%rbp), %rax
	movl	%edx, 3188(%rax)
	movq	-16(%rbp), %rax
	movl	3188(%rax), %edx
	movq	-16(%rbp), %rax
	movl	3184(%rax), %eax
	xorl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	%edx, 3188(%rax)
	movq	-16(%rbp), %rax
	movl	$14, 8(%rax)
	jmp	.L1650
.L1654:
	movl	$0, %eax
	jmp	.L1646
.L1650:
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$9, %eax
	jle	.L1662
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_decompress
	movl	%eax, -8(%rbp)
	cmpl	$4, -8(%rbp)
	jne	.L1659
	movq	-16(%rbp), %rax
	movl	52(%rax), %eax
	cmpl	$2, %eax
	jle	.L1660
	movq	-16(%rbp), %rax
	movl	3188(%rax), %ecx
	movq	-16(%rbp), %rax
	movl	3180(%rax), %edx
	movl	$.LC136, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L1660:
	movq	-16(%rbp), %rax
	movl	3188(%rax), %edx
	movq	-16(%rbp), %rax
	movl	3180(%rax), %eax
	cmpl	%eax, %edx
	je	.L1661
	movl	$-4, %eax
	jmp	.L1646
.L1661:
	movl	-8(%rbp), %eax
	jmp	.L1646
.L1659:
	movq	-16(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	$2, %eax
	je	.L1662
	movl	-8(%rbp), %eax
.L1646:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE86:
	.size	BZ2_bzDecompress, .-BZ2_bzDecompress
	.globl	BZ2_bzDecompressEnd
	.type	BZ2_bzDecompressEnd, @function
BZ2_bzDecompressEnd:
.LFB87:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L1664
	movl	$-2, %eax
	jmp	.L1665
.L1664:
	movq	-24(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L1666
	movl	$-2, %eax
	jmp	.L1665
.L1666:
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	cmpq	-24(%rbp), %rax
	je	.L1667
	movl	$-2, %eax
	jmp	.L1665
.L1667:
	movq	-8(%rbp), %rax
	movq	3152(%rax), %rax
	testq	%rax, %rax
	je	.L1668
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	3152(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1668:
	movq	-8(%rbp), %rax
	movq	3160(%rax), %rax
	testq	%rax, %rax
	je	.L1669
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	3160(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1669:
	movq	-8(%rbp), %rax
	movq	3168(%rax), %rax
	testq	%rax, %rax
	je	.L1670
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	3168(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
.L1670:
	movq	-24(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-24(%rbp), %rax
	movq	48(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	*%rcx
	movq	-24(%rbp), %rax
	movq	$0, 48(%rax)
	movl	$0, %eax
.L1665:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE87:
	.size	BZ2_bzDecompressEnd, .-BZ2_bzDecompressEnd
	.globl	BZ2_bzWriteOpen
	.type	BZ2_bzWriteOpen, @function
BZ2_bzWriteOpen:
.LFB88:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	movl	%ecx, -40(%rbp)
	movl	%r8d, -44(%rbp)
	movq	$0, -16(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L1672
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1672:
	cmpq	$0, -16(%rbp)
	je	.L1673
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1673:
	cmpq	$0, -32(%rbp)
	je	.L1674
	cmpl	$0, -36(%rbp)
	jle	.L1674
	cmpl	$9, -36(%rbp)
	jg	.L1674
	cmpl	$0, -44(%rbp)
	js	.L1674
	cmpl	$250, -44(%rbp)
	jg	.L1674
	cmpl	$0, -40(%rbp)
	js	.L1674
	cmpl	$4, -40(%rbp)
	jle	.L1675
.L1674:
	cmpq	$0, -24(%rbp)
	je	.L1676
	movq	-24(%rbp), %rax
	movl	$-2, (%rax)
.L1676:
	cmpq	$0, -16(%rbp)
	je	.L1677
	movq	-16(%rbp), %rax
	movl	$-2, 5096(%rax)
.L1677:
	movl	$0, %eax
	jmp	.L1678
.L1675:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1679
	cmpq	$0, -24(%rbp)
	je	.L1680
	movq	-24(%rbp), %rax
	movl	$-6, (%rax)
.L1680:
	cmpq	$0, -16(%rbp)
	je	.L1681
	movq	-16(%rbp), %rax
	movl	$-6, 5096(%rax)
.L1681:
	movl	$0, %eax
	jmp	.L1678
.L1679:
	movl	$5104, %edi
	call	malloc
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L1682
	cmpq	$0, -24(%rbp)
	je	.L1683
	movq	-24(%rbp), %rax
	movl	$-3, (%rax)
.L1683:
	cmpq	$0, -16(%rbp)
	je	.L1684
	movq	-16(%rbp), %rax
	movl	$-3, 5096(%rax)
.L1684:
	movl	$0, %eax
	jmp	.L1678
.L1682:
	cmpq	$0, -24(%rbp)
	je	.L1685
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1685:
	cmpq	$0, -16(%rbp)
	je	.L1686
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1686:
	movq	-16(%rbp), %rax
	movb	$0, 5100(%rax)
	movq	-16(%rbp), %rax
	movl	$0, 5008(%rax)
	movq	-16(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-16(%rbp), %rax
	movb	$1, 5012(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 5072(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 5080(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 5088(%rax)
	cmpl	$0, -44(%rbp)
	jne	.L1687
	movl	$30, -44(%rbp)
.L1687:
	movq	-16(%rbp), %rax
	leaq	5016(%rax), %rdi
	movl	-44(%rbp), %ecx
	movl	-40(%rbp), %edx
	movl	-36(%rbp), %eax
	movl	%eax, %esi
	call	BZ2_bzCompressInit
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L1688
	cmpq	$0, -24(%rbp)
	je	.L1689
	movq	-24(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
.L1689:
	cmpq	$0, -16(%rbp)
	je	.L1690
	movq	-16(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, 5096(%rax)
.L1690:
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	free
	movl	$0, %eax
	jmp	.L1678
.L1688:
	movq	-16(%rbp), %rax
	movl	$0, 5024(%rax)
	movq	-16(%rbp), %rax
	movb	$1, 5100(%rax)
	movq	-16(%rbp), %rax
.L1678:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE88:
	.size	BZ2_bzWriteOpen, .-BZ2_bzWriteOpen
	.globl	BZ2_bzWrite
	.type	BZ2_bzWrite, @function
BZ2_bzWrite:
.LFB89:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movl	%ecx, -60(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
	cmpq	$0, -40(%rbp)
	je	.L1692
	movq	-40(%rbp), %rax
	movl	$0, (%rax)
.L1692:
	cmpq	$0, -24(%rbp)
	je	.L1693
	movq	-24(%rbp), %rax
	movl	$0, 5096(%rax)
.L1693:
	cmpq	$0, -24(%rbp)
	je	.L1694
	cmpq	$0, -56(%rbp)
	je	.L1694
	cmpl	$0, -60(%rbp)
	jns	.L1695
.L1694:
	cmpq	$0, -40(%rbp)
	je	.L1696
	movq	-40(%rbp), %rax
	movl	$-2, (%rax)
.L1696:
	cmpq	$0, -24(%rbp)
	je	.L1719
	movq	-24(%rbp), %rax
	movl	$-2, 5096(%rax)
	jmp	.L1719
.L1695:
	movq	-24(%rbp), %rax
	movzbl	5012(%rax), %eax
	testb	%al, %al
	jne	.L1699
	cmpq	$0, -40(%rbp)
	je	.L1700
	movq	-40(%rbp), %rax
	movl	$-1, (%rax)
.L1700:
	cmpq	$0, -24(%rbp)
	je	.L1720
	movq	-24(%rbp), %rax
	movl	$-1, 5096(%rax)
	jmp	.L1720
.L1699:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1702
	cmpq	$0, -40(%rbp)
	je	.L1703
	movq	-40(%rbp), %rax
	movl	$-6, (%rax)
.L1703:
	cmpq	$0, -24(%rbp)
	je	.L1721
	movq	-24(%rbp), %rax
	movl	$-6, 5096(%rax)
	jmp	.L1721
.L1702:
	cmpl	$0, -60(%rbp)
	jne	.L1705
	cmpq	$0, -40(%rbp)
	je	.L1706
	movq	-40(%rbp), %rax
	movl	$0, (%rax)
.L1706:
	cmpq	$0, -24(%rbp)
	je	.L1722
	movq	-24(%rbp), %rax
	movl	$0, 5096(%rax)
	jmp	.L1722
.L1705:
	movl	-60(%rbp), %edx
	movq	-24(%rbp), %rax
	movl	%edx, 5024(%rax)
	movq	-24(%rbp), %rax
	movq	-56(%rbp), %rdx
	movq	%rdx, 5016(%rax)
	jmp	.L1718
.L1725:
	nop
.L1718:
	movq	-24(%rbp), %rax
	movl	$5000, 5048(%rax)
	movq	-24(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, 5040(%rax)
	movq	-24(%rbp), %rax
	addq	$5016, %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	BZ2_bzCompress
	movl	%eax, -12(%rbp)
	cmpl	$1, -12(%rbp)
	je	.L1708
	cmpq	$0, -40(%rbp)
	je	.L1709
	movq	-40(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, (%rax)
.L1709:
	cmpq	$0, -24(%rbp)
	je	.L1723
	movq	-24(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, 5096(%rax)
	jmp	.L1723
.L1708:
	movq	-24(%rbp), %rax
	movl	5048(%rax), %eax
	cmpl	$4999, %eax
	ja	.L1711
	movq	-24(%rbp), %rax
	movl	5048(%rax), %eax
	movl	$5000, %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -8(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	%eax, -4(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jne	.L1712
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1711
.L1712:
	cmpq	$0, -40(%rbp)
	je	.L1713
	movq	-40(%rbp), %rax
	movl	$-6, (%rax)
.L1713:
	cmpq	$0, -24(%rbp)
	je	.L1724
	movq	-24(%rbp), %rax
	movl	$-6, 5096(%rax)
	jmp	.L1724
.L1711:
	movq	-24(%rbp), %rax
	movl	5024(%rax), %eax
	testl	%eax, %eax
	jne	.L1725
	cmpq	$0, -40(%rbp)
	je	.L1716
	movq	-40(%rbp), %rax
	movl	$0, (%rax)
.L1716:
	cmpq	$0, -24(%rbp)
	je	.L1726
	movq	-24(%rbp), %rax
	movl	$0, 5096(%rax)
	jmp	.L1726
.L1719:
	nop
	jmp	.L1691
.L1720:
	nop
	jmp	.L1691
.L1721:
	nop
	jmp	.L1691
.L1722:
	nop
	jmp	.L1691
.L1723:
	nop
	jmp	.L1691
.L1724:
	nop
	jmp	.L1691
.L1726:
	nop
.L1691:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE89:
	.size	BZ2_bzWrite, .-BZ2_bzWrite
	.globl	BZ2_bzWriteClose
	.type	BZ2_bzWriteClose, @function
BZ2_bzWriteClose:
.LFB90:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	movq	-40(%rbp), %rdi
	movq	-32(%rbp), %rcx
	movl	-20(%rbp), %edx
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rax
	movq	$0, (%rsp)
	movq	%rdi, %r9
	movl	$0, %r8d
	movq	%rax, %rdi
	call	BZ2_bzWriteClose64
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE90:
	.size	BZ2_bzWriteClose, .-BZ2_bzWriteClose
	.globl	BZ2_bzWriteClose64
	.type	BZ2_bzWriteClose64, @function
BZ2_bzWriteClose64:
.LFB91:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movl	%edx, -52(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -80(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L1729
	cmpq	$0, -40(%rbp)
	je	.L1730
	movq	-40(%rbp), %rax
	movl	$0, (%rax)
.L1730:
	cmpq	$0, -24(%rbp)
	je	.L1762
	movq	-24(%rbp), %rax
	movl	$0, 5096(%rax)
	jmp	.L1762
.L1729:
	movq	-24(%rbp), %rax
	movzbl	5012(%rax), %eax
	testb	%al, %al
	jne	.L1733
	cmpq	$0, -40(%rbp)
	je	.L1734
	movq	-40(%rbp), %rax
	movl	$-1, (%rax)
.L1734:
	cmpq	$0, -24(%rbp)
	je	.L1763
	movq	-24(%rbp), %rax
	movl	$-1, 5096(%rax)
	jmp	.L1763
.L1733:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1736
	cmpq	$0, -40(%rbp)
	je	.L1737
	movq	-40(%rbp), %rax
	movl	$-6, (%rax)
.L1737:
	cmpq	$0, -24(%rbp)
	je	.L1764
	movq	-24(%rbp), %rax
	movl	$-6, 5096(%rax)
	jmp	.L1764
.L1736:
	cmpq	$0, -64(%rbp)
	je	.L1739
	movq	-64(%rbp), %rax
	movl	$0, (%rax)
.L1739:
	cmpq	$0, -72(%rbp)
	je	.L1740
	movq	-72(%rbp), %rax
	movl	$0, (%rax)
.L1740:
	cmpq	$0, -80(%rbp)
	je	.L1741
	movq	-80(%rbp), %rax
	movl	$0, (%rax)
.L1741:
	cmpq	$0, 16(%rbp)
	je	.L1742
	movq	16(%rbp), %rax
	movl	$0, (%rax)
.L1742:
	cmpl	$0, -52(%rbp)
	jne	.L1743
	movq	-24(%rbp), %rax
	movl	5096(%rax), %eax
	testl	%eax, %eax
	jne	.L1743
	jmp	.L1752
.L1767:
	nop
.L1752:
	movq	-24(%rbp), %rax
	movl	$5000, 5048(%rax)
	movq	-24(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, 5040(%rax)
	movq	-24(%rbp), %rax
	addq	$5016, %rax
	movl	$2, %esi
	movq	%rax, %rdi
	call	BZ2_bzCompress
	movl	%eax, -12(%rbp)
	cmpl	$3, -12(%rbp)
	je	.L1744
	cmpl	$4, -12(%rbp)
	je	.L1744
	cmpq	$0, -40(%rbp)
	je	.L1745
	movq	-40(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, (%rax)
.L1745:
	cmpq	$0, -24(%rbp)
	je	.L1765
	movq	-24(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, 5096(%rax)
	jmp	.L1765
.L1744:
	movq	-24(%rbp), %rax
	movl	5048(%rax), %eax
	cmpl	$4999, %eax
	ja	.L1747
	movq	-24(%rbp), %rax
	movl	5048(%rax), %eax
	movl	$5000, %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -8(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rcx
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	%eax, -4(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jne	.L1748
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1747
.L1748:
	cmpq	$0, -40(%rbp)
	je	.L1749
	movq	-40(%rbp), %rax
	movl	$-6, (%rax)
.L1749:
	cmpq	$0, -24(%rbp)
	je	.L1766
	movq	-24(%rbp), %rax
	movl	$-6, 5096(%rax)
	jmp	.L1766
.L1747:
	cmpl	$4, -12(%rbp)
	jne	.L1767
	nop
.L1743:
	cmpl	$0, -52(%rbp)
	jne	.L1753
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	jne	.L1753
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	fflush
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1753
	cmpq	$0, -40(%rbp)
	je	.L1754
	movq	-40(%rbp), %rax
	movl	$-6, (%rax)
.L1754:
	cmpq	$0, -24(%rbp)
	je	.L1768
	movq	-24(%rbp), %rax
	movl	$-6, 5096(%rax)
	jmp	.L1768
.L1753:
	cmpq	$0, -64(%rbp)
	je	.L1756
	movq	-24(%rbp), %rax
	movl	5028(%rax), %edx
	movq	-64(%rbp), %rax
	movl	%edx, (%rax)
.L1756:
	cmpq	$0, -72(%rbp)
	je	.L1757
	movq	-24(%rbp), %rax
	movl	5032(%rax), %edx
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L1757:
	cmpq	$0, -80(%rbp)
	je	.L1758
	movq	-24(%rbp), %rax
	movl	5052(%rax), %edx
	movq	-80(%rbp), %rax
	movl	%edx, (%rax)
.L1758:
	cmpq	$0, 16(%rbp)
	je	.L1759
	movq	-24(%rbp), %rax
	movl	5056(%rax), %edx
	movq	16(%rbp), %rax
	movl	%edx, (%rax)
.L1759:
	cmpq	$0, -40(%rbp)
	je	.L1760
	movq	-40(%rbp), %rax
	movl	$0, (%rax)
.L1760:
	cmpq	$0, -24(%rbp)
	je	.L1761
	movq	-24(%rbp), %rax
	movl	$0, 5096(%rax)
.L1761:
	movq	-24(%rbp), %rax
	addq	$5016, %rax
	movq	%rax, %rdi
	call	BZ2_bzCompressEnd
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	free
	jmp	.L1728
.L1762:
	nop
	jmp	.L1728
.L1763:
	nop
	jmp	.L1728
.L1764:
	nop
	jmp	.L1728
.L1765:
	nop
	jmp	.L1728
.L1766:
	nop
	jmp	.L1728
.L1768:
	nop
.L1728:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE91:
	.size	BZ2_bzWriteClose64, .-BZ2_bzWriteClose64
	.globl	BZ2_bzReadOpen
	.type	BZ2_bzReadOpen, @function
BZ2_bzReadOpen:
.LFB92:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	movl	%ecx, -40(%rbp)
	movq	%r8, -48(%rbp)
	movl	%r9d, -52(%rbp)
	movq	$0, -16(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L1770
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1770:
	cmpq	$0, -16(%rbp)
	je	.L1771
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1771:
	cmpq	$0, -32(%rbp)
	je	.L1772
	cmpl	$0, -40(%rbp)
	je	.L1773
	cmpl	$1, -40(%rbp)
	jne	.L1772
.L1773:
	cmpl	$0, -36(%rbp)
	js	.L1772
	cmpl	$4, -36(%rbp)
	jg	.L1772
	cmpq	$0, -48(%rbp)
	jne	.L1774
	cmpl	$0, -52(%rbp)
	jne	.L1772
.L1774:
	cmpq	$0, -48(%rbp)
	je	.L1775
	cmpl	$0, -52(%rbp)
	js	.L1772
	cmpl	$5000, -52(%rbp)
	jle	.L1775
.L1772:
	cmpq	$0, -24(%rbp)
	je	.L1776
	movq	-24(%rbp), %rax
	movl	$-2, (%rax)
.L1776:
	cmpq	$0, -16(%rbp)
	je	.L1777
	movq	-16(%rbp), %rax
	movl	$-2, 5096(%rax)
.L1777:
	movl	$0, %eax
	jmp	.L1778
.L1775:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1779
	cmpq	$0, -24(%rbp)
	je	.L1780
	movq	-24(%rbp), %rax
	movl	$-6, (%rax)
.L1780:
	cmpq	$0, -16(%rbp)
	je	.L1781
	movq	-16(%rbp), %rax
	movl	$-6, 5096(%rax)
.L1781:
	movl	$0, %eax
	jmp	.L1778
.L1779:
	movl	$5104, %edi
	call	malloc
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L1782
	cmpq	$0, -24(%rbp)
	je	.L1783
	movq	-24(%rbp), %rax
	movl	$-3, (%rax)
.L1783:
	cmpq	$0, -16(%rbp)
	je	.L1784
	movq	-16(%rbp), %rax
	movl	$-3, 5096(%rax)
.L1784:
	movl	$0, %eax
	jmp	.L1778
.L1782:
	cmpq	$0, -24(%rbp)
	je	.L1785
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1785:
	cmpq	$0, -16(%rbp)
	je	.L1786
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1786:
	movq	-16(%rbp), %rax
	movb	$0, 5100(%rax)
	movq	-16(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-16(%rbp), %rax
	movl	$0, 5008(%rax)
	movq	-16(%rbp), %rax
	movb	$0, 5012(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 5072(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 5080(%rax)
	movq	-16(%rbp), %rax
	movq	$0, 5088(%rax)
	jmp	.L1787
.L1788:
	movq	-16(%rbp), %rax
	movl	5008(%rax), %esi
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	movl	%eax, %ecx
	movq	-16(%rbp), %rdx
	movslq	%esi, %rax
	movb	%cl, 8(%rdx,%rax)
	movq	-16(%rbp), %rax
	movl	5008(%rax), %eax
	leal	1(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 5008(%rax)
	addq	$1, -48(%rbp)
	subl	$1, -52(%rbp)
.L1787:
	cmpl	$0, -52(%rbp)
	jg	.L1788
	movq	-16(%rbp), %rax
	leaq	5016(%rax), %rcx
	movl	-40(%rbp), %edx
	movl	-36(%rbp), %eax
	movl	%eax, %esi
	movq	%rcx, %rdi
	call	BZ2_bzDecompressInit
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L1789
	cmpq	$0, -24(%rbp)
	je	.L1790
	movq	-24(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
.L1790:
	cmpq	$0, -16(%rbp)
	je	.L1791
	movq	-16(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, 5096(%rax)
.L1791:
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	free
	movl	$0, %eax
	jmp	.L1778
.L1789:
	movq	-16(%rbp), %rax
	movl	5008(%rax), %eax
	movl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	%edx, 5024(%rax)
	movq	-16(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 5016(%rax)
	movq	-16(%rbp), %rax
	movb	$1, 5100(%rax)
	movq	-16(%rbp), %rax
.L1778:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE92:
	.size	BZ2_bzReadOpen, .-BZ2_bzReadOpen
	.globl	BZ2_bzReadClose
	.type	BZ2_bzReadClose, @function
BZ2_bzReadClose:
.LFB93:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -8(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L1793
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1793:
	cmpq	$0, -8(%rbp)
	je	.L1794
	movq	-8(%rbp), %rax
	movl	$0, 5096(%rax)
.L1794:
	cmpq	$0, -8(%rbp)
	jne	.L1795
	cmpq	$0, -24(%rbp)
	je	.L1796
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1796:
	cmpq	$0, -8(%rbp)
	je	.L1803
	movq	-8(%rbp), %rax
	movl	$0, 5096(%rax)
	jmp	.L1803
.L1795:
	movq	-8(%rbp), %rax
	movzbl	5012(%rax), %eax
	testb	%al, %al
	je	.L1799
	cmpq	$0, -24(%rbp)
	je	.L1800
	movq	-24(%rbp), %rax
	movl	$-1, (%rax)
.L1800:
	cmpq	$0, -8(%rbp)
	je	.L1804
	movq	-8(%rbp), %rax
	movl	$-1, 5096(%rax)
	jmp	.L1804
.L1799:
	movq	-8(%rbp), %rax
	movzbl	5100(%rax), %eax
	testb	%al, %al
	je	.L1802
	movq	-8(%rbp), %rax
	addq	$5016, %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompressEnd
.L1802:
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	free
	jmp	.L1792
.L1803:
	nop
	jmp	.L1792
.L1804:
	nop
.L1792:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE93:
	.size	BZ2_bzReadClose, .-BZ2_bzReadClose
	.globl	BZ2_bzRead
	.type	BZ2_bzRead, @function
BZ2_bzRead:
.LFB94:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movl	%ecx, -44(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -16(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L1806
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1806:
	cmpq	$0, -16(%rbp)
	je	.L1807
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1807:
	cmpq	$0, -16(%rbp)
	je	.L1808
	cmpq	$0, -40(%rbp)
	je	.L1808
	cmpl	$0, -44(%rbp)
	jns	.L1809
.L1808:
	cmpq	$0, -24(%rbp)
	je	.L1810
	movq	-24(%rbp), %rax
	movl	$-2, (%rax)
.L1810:
	cmpq	$0, -16(%rbp)
	je	.L1811
	movq	-16(%rbp), %rax
	movl	$-2, 5096(%rax)
.L1811:
	movl	$0, %eax
	jmp	.L1812
.L1809:
	movq	-16(%rbp), %rax
	movzbl	5012(%rax), %eax
	testb	%al, %al
	je	.L1813
	cmpq	$0, -24(%rbp)
	je	.L1814
	movq	-24(%rbp), %rax
	movl	$-1, (%rax)
.L1814:
	cmpq	$0, -16(%rbp)
	je	.L1815
	movq	-16(%rbp), %rax
	movl	$-1, 5096(%rax)
.L1815:
	movl	$0, %eax
	jmp	.L1812
.L1813:
	cmpl	$0, -44(%rbp)
	jne	.L1816
	cmpq	$0, -24(%rbp)
	je	.L1817
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1817:
	cmpq	$0, -16(%rbp)
	je	.L1818
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1818:
	movl	$0, %eax
	jmp	.L1812
.L1816:
	movl	-44(%rbp), %edx
	movq	-16(%rbp), %rax
	movl	%edx, 5048(%rax)
	movq	-16(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, 5040(%rax)
	jmp	.L1838
.L1839:
	nop
.L1838:
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1819
	cmpq	$0, -24(%rbp)
	je	.L1820
	movq	-24(%rbp), %rax
	movl	$-6, (%rax)
.L1820:
	cmpq	$0, -16(%rbp)
	je	.L1821
	movq	-16(%rbp), %rax
	movl	$-6, 5096(%rax)
.L1821:
	movl	$0, %eax
	jmp	.L1812
.L1819:
	movq	-16(%rbp), %rax
	movl	5024(%rax), %eax
	testl	%eax, %eax
	jne	.L1822
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	myfeof
	testb	%al, %al
	jne	.L1822
	movq	-16(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	addq	$8, %rax
	movq	%rdx, %rcx
	movl	$5000, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fread
	movl	%eax, -8(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	ferror
	testl	%eax, %eax
	je	.L1823
	cmpq	$0, -24(%rbp)
	je	.L1824
	movq	-24(%rbp), %rax
	movl	$-6, (%rax)
.L1824:
	cmpq	$0, -16(%rbp)
	je	.L1825
	movq	-16(%rbp), %rax
	movl	$-6, 5096(%rax)
.L1825:
	movl	$0, %eax
	jmp	.L1812
.L1823:
	movq	-16(%rbp), %rax
	movl	-8(%rbp), %edx
	movl	%edx, 5008(%rax)
	movq	-16(%rbp), %rax
	movl	5008(%rax), %eax
	movl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	%edx, 5024(%rax)
	movq	-16(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 5016(%rax)
.L1822:
	movq	-16(%rbp), %rax
	addq	$5016, %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompress
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L1826
	cmpl	$4, -4(%rbp)
	je	.L1826
	cmpq	$0, -24(%rbp)
	je	.L1827
	movq	-24(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
.L1827:
	cmpq	$0, -16(%rbp)
	je	.L1828
	movq	-16(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, 5096(%rax)
.L1828:
	movl	$0, %eax
	jmp	.L1812
.L1826:
	cmpl	$0, -4(%rbp)
	jne	.L1829
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	myfeof
	testb	%al, %al
	je	.L1829
	movq	-16(%rbp), %rax
	movl	5024(%rax), %eax
	testl	%eax, %eax
	jne	.L1829
	movq	-16(%rbp), %rax
	movl	5048(%rax), %eax
	testl	%eax, %eax
	je	.L1829
	cmpq	$0, -24(%rbp)
	je	.L1830
	movq	-24(%rbp), %rax
	movl	$-7, (%rax)
.L1830:
	cmpq	$0, -16(%rbp)
	je	.L1831
	movq	-16(%rbp), %rax
	movl	$-7, 5096(%rax)
.L1831:
	movl	$0, %eax
	jmp	.L1812
.L1829:
	cmpl	$4, -4(%rbp)
	jne	.L1832
	cmpq	$0, -24(%rbp)
	je	.L1833
	movq	-24(%rbp), %rax
	movl	$4, (%rax)
.L1833:
	cmpq	$0, -16(%rbp)
	je	.L1834
	movq	-16(%rbp), %rax
	movl	$4, 5096(%rax)
.L1834:
	movl	-44(%rbp), %edx
	movq	-16(%rbp), %rax
	movl	5048(%rax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.L1812
.L1832:
	movq	-16(%rbp), %rax
	movl	5048(%rax), %eax
	testl	%eax, %eax
	jne	.L1839
	cmpq	$0, -24(%rbp)
	je	.L1836
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1836:
	cmpq	$0, -16(%rbp)
	je	.L1837
	movq	-16(%rbp), %rax
	movl	$0, 5096(%rax)
.L1837:
	movl	-44(%rbp), %eax
.L1812:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE94:
	.size	BZ2_bzRead, .-BZ2_bzRead
	.globl	BZ2_bzReadGetUnused
	.type	BZ2_bzReadGetUnused, @function
BZ2_bzReadGetUnused:
.LFB95:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L1841
	cmpq	$0, -24(%rbp)
	je	.L1842
	movq	-24(%rbp), %rax
	movl	$-2, (%rax)
.L1842:
	cmpq	$0, -8(%rbp)
	je	.L1854
	movq	-8(%rbp), %rax
	movl	$-2, 5096(%rax)
	jmp	.L1854
.L1841:
	movq	-8(%rbp), %rax
	movl	5096(%rax), %eax
	cmpl	$4, %eax
	je	.L1845
	cmpq	$0, -24(%rbp)
	je	.L1846
	movq	-24(%rbp), %rax
	movl	$-1, (%rax)
.L1846:
	cmpq	$0, -8(%rbp)
	je	.L1855
	movq	-8(%rbp), %rax
	movl	$-1, 5096(%rax)
	jmp	.L1855
.L1845:
	cmpq	$0, -40(%rbp)
	je	.L1848
	cmpq	$0, -48(%rbp)
	jne	.L1849
.L1848:
	cmpq	$0, -24(%rbp)
	je	.L1850
	movq	-24(%rbp), %rax
	movl	$-2, (%rax)
.L1850:
	cmpq	$0, -8(%rbp)
	je	.L1856
	movq	-8(%rbp), %rax
	movl	$-2, 5096(%rax)
	jmp	.L1856
.L1849:
	cmpq	$0, -24(%rbp)
	je	.L1852
	movq	-24(%rbp), %rax
	movl	$0, (%rax)
.L1852:
	cmpq	$0, -8(%rbp)
	je	.L1853
	movq	-8(%rbp), %rax
	movl	$0, 5096(%rax)
.L1853:
	movq	-8(%rbp), %rax
	movl	5024(%rax), %eax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	movl	%edx, (%rax)
	movq	-8(%rbp), %rax
	movq	5016(%rax), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L1840
.L1854:
	nop
	jmp	.L1840
.L1855:
	nop
	jmp	.L1840
.L1856:
	nop
.L1840:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE95:
	.size	BZ2_bzReadGetUnused, .-BZ2_bzReadGetUnused
	.globl	BZ2_bzBuffToBuffCompress
	.type	BZ2_bzBuffToBuffCompress, @function
BZ2_bzBuffToBuffCompress:
.LFB96:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$144, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%ecx, -124(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%r9d, -132(%rbp)
	cmpq	$0, -104(%rbp)
	je	.L1858
	cmpq	$0, -112(%rbp)
	je	.L1858
	cmpq	$0, -120(%rbp)
	je	.L1858
	cmpl	$0, -128(%rbp)
	jle	.L1858
	cmpl	$9, -128(%rbp)
	jg	.L1858
	cmpl	$0, -132(%rbp)
	js	.L1858
	cmpl	$4, -132(%rbp)
	jg	.L1858
	cmpl	$0, 16(%rbp)
	js	.L1858
	cmpl	$250, 16(%rbp)
	jle	.L1859
.L1858:
	movl	$-2, %eax
	jmp	.L1860
.L1859:
	cmpl	$0, 16(%rbp)
	jne	.L1861
	movl	$30, 16(%rbp)
.L1861:
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	$0, -24(%rbp)
	movl	-132(%rbp), %edx
	movl	-128(%rbp), %esi
	leaq	-96(%rbp), %rax
	movl	16(%rbp), %ecx
	movq	%rax, %rdi
	call	BZ2_bzCompressInit
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L1862
	movl	-4(%rbp), %eax
	jmp	.L1860
.L1862:
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -72(%rbp)
	movl	-124(%rbp), %eax
	movl	%eax, -88(%rbp)
	movq	-112(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -64(%rbp)
	leaq	-96(%rbp), %rax
	movl	$2, %esi
	movq	%rax, %rdi
	call	BZ2_bzCompress
	movl	%eax, -4(%rbp)
	cmpl	$3, -4(%rbp)
	je	.L1869
.L1863:
	cmpl	$4, -4(%rbp)
	jne	.L1870
.L1865:
	movq	-112(%rbp), %rax
	movl	(%rax), %edx
	movl	-64(%rbp), %eax
	subl	%eax, %edx
	movq	-112(%rbp), %rax
	movl	%edx, (%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzCompressEnd
	movl	$0, %eax
	jmp	.L1860
.L1869:
	nop
.L1867:
.L1864:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzCompressEnd
	movl	$-8, %eax
	jmp	.L1860
.L1870:
	nop
.L1868:
.L1866:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzCompressEnd
	movl	-4(%rbp), %eax
.L1860:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE96:
	.size	BZ2_bzBuffToBuffCompress, .-BZ2_bzBuffToBuffCompress
	.globl	BZ2_bzBuffToBuffDecompress
	.type	BZ2_bzBuffToBuffDecompress, @function
BZ2_bzBuffToBuffDecompress:
.LFB97:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$144, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%ecx, -124(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%r9d, -132(%rbp)
	cmpq	$0, -104(%rbp)
	je	.L1872
	cmpq	$0, -112(%rbp)
	je	.L1872
	cmpq	$0, -120(%rbp)
	je	.L1872
	cmpl	$0, -128(%rbp)
	je	.L1873
	cmpl	$1, -128(%rbp)
	jne	.L1872
.L1873:
	cmpl	$0, -132(%rbp)
	js	.L1872
	cmpl	$4, -132(%rbp)
	jle	.L1874
.L1872:
	movl	$-2, %eax
	jmp	.L1875
.L1874:
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	$0, -24(%rbp)
	movl	-128(%rbp), %edx
	movl	-132(%rbp), %ecx
	leaq	-96(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	BZ2_bzDecompressInit
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L1876
	movl	-4(%rbp), %eax
	jmp	.L1875
.L1876:
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -72(%rbp)
	movl	-124(%rbp), %eax
	movl	%eax, -88(%rbp)
	movq	-112(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -64(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompress
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L1884
.L1877:
	cmpl	$4, -4(%rbp)
	jne	.L1885
.L1879:
	movq	-112(%rbp), %rax
	movl	(%rax), %edx
	movl	-64(%rbp), %eax
	subl	%eax, %edx
	movq	-112(%rbp), %rax
	movl	%edx, (%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompressEnd
	movl	$0, %eax
	jmp	.L1875
.L1884:
	nop
.L1882:
.L1878:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	je	.L1881
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompressEnd
	movl	$-7, %eax
	jmp	.L1875
.L1881:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompressEnd
	movl	$-8, %eax
	jmp	.L1875
.L1885:
	nop
.L1883:
.L1880:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzDecompressEnd
	movl	-4(%rbp), %eax
.L1875:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE97:
	.size	BZ2_bzBuffToBuffDecompress, .-BZ2_bzBuffToBuffDecompress
	.section	.rodata
.LC137:
	.string	"1.0.6, 6-Sept-2010"
	.text
	.globl	BZ2_bzlibVersion
	.type	BZ2_bzlibVersion, @function
BZ2_bzlibVersion:
.LFB98:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC137, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE98:
	.size	BZ2_bzlibVersion, .-BZ2_bzlibVersion
	.section	.rodata
.LC138:
	.string	"w"
.LC139:
	.string	"r"
.LC140:
	.string	"b"
	.text
	.type	bzopen_or_bzdopen, @function
bzopen_or_bzdopen:
.LFB99:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$5120, %rsp
	movq	%rdi, -5096(%rbp)
	movl	%esi, -5100(%rbp)
	movq	%rdx, -5112(%rbp)
	movl	%ecx, -5104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$9, -5064(%rbp)
	movl	$0, -5060(%rbp)
	movq	$0, -32(%rbp)
	movw	$0, -24(%rbp)
	movq	$0, -5088(%rbp)
	movq	$0, -5080(%rbp)
	movl	$0, -5052(%rbp)
	movl	$30, -5048(%rbp)
	movl	$0, -5056(%rbp)
	movl	$0, -5044(%rbp)
	cmpq	$0, -5112(%rbp)
	jne	.L1915
	movl	$0, %eax
	jmp	.L1889
.L1896:
	movq	-5112(%rbp), %rax
	movzbl	(%rax), %eax
	movsbl	%al, %eax
	cmpl	$115, %eax
	je	.L1893
	cmpl	$119, %eax
	je	.L1894
	cmpl	$114, %eax
	jne	.L1914
.L1892:
	movl	$0, -5060(%rbp)
	jmp	.L1895
.L1894:
	movl	$1, -5060(%rbp)
	jmp	.L1895
.L1893:
	movl	$1, -5056(%rbp)
	jmp	.L1895
.L1914:
	call	__ctype_b_loc
	movq	(%rax), %rdx
	movq	-5112(%rbp), %rax
	movzbl	(%rax), %eax
	movsbq	%al, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	andl	$2048, %eax
	testl	%eax, %eax
	je	.L1895
	movq	-5112(%rbp), %rax
	movzbl	(%rax), %eax
	movsbl	%al, %eax
	subl	$48, %eax
	movl	%eax, -5064(%rbp)
.L1895:
	addq	$1, -5112(%rbp)
	jmp	.L1890
.L1915:
	nop
.L1890:
	movq	-5112(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L1896
	cmpl	$0, -5060(%rbp)
	je	.L1897
	movl	$.LC138, %eax
	jmp	.L1898
.L1897:
	movl	$.LC139, %eax
.L1898:
	movq	%rax, %rsi
	leaq	-32(%rbp), %rax
	movq	$-1, -5120(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-5120(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	leaq	-32(%rbp), %rax
	addq	%rax, %rdx
	movzwl	(%rsi), %eax
	movw	%ax, (%rdx)
	movl	$.LC140, %esi
	leaq	-32(%rbp), %rax
	movq	$-1, -5120(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-5120(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	leaq	-32(%rbp), %rax
	addq	%rax, %rdx
	movzwl	(%rsi), %eax
	movw	%ax, (%rdx)
	cmpl	$0, -5104(%rbp)
	jne	.L1899
	cmpq	$0, -5096(%rbp)
	je	.L1900
	movq	-5096(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L1901
.L1900:
	cmpl	$0, -5060(%rbp)
	je	.L1902
	movq	stdout(%rip), %rax
	jmp	.L1903
.L1902:
	movq	stdin(%rip), %rax
.L1903:
	movq	%rax, -5088(%rbp)
	jmp	.L1905
.L1901:
	leaq	-32(%rbp), %rdx
	movq	-5096(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	fopen
	movq	%rax, -5088(%rbp)
	jmp	.L1905
.L1899:
	leaq	-32(%rbp), %rdx
	movl	-5100(%rbp), %eax
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	fdopen
	movq	%rax, -5088(%rbp)
.L1905:
	cmpq	$0, -5088(%rbp)
	jne	.L1906
	movl	$0, %eax
	jmp	.L1889
.L1906:
	cmpl	$0, -5060(%rbp)
	je	.L1907
	cmpl	$0, -5064(%rbp)
	jg	.L1908
	movl	$1, -5064(%rbp)
.L1908:
	cmpl	$9, -5064(%rbp)
	jle	.L1909
	movl	$9, -5064(%rbp)
.L1909:
	movl	-5048(%rbp), %edi
	movl	-5052(%rbp), %ecx
	movl	-5064(%rbp), %edx
	movq	-5088(%rbp), %rsi
	leaq	-5068(%rbp), %rax
	movl	%edi, %r8d
	movq	%rax, %rdi
	call	BZ2_bzWriteOpen
	movq	%rax, -5080(%rbp)
	jmp	.L1910
.L1907:
	movl	-5044(%rbp), %r8d
	leaq	-5040(%rbp), %rdi
	movl	-5056(%rbp), %ecx
	movl	-5052(%rbp), %edx
	movq	-5088(%rbp), %rsi
	leaq	-5068(%rbp), %rax
	movl	%r8d, %r9d
	movq	%rdi, %r8
	movq	%rax, %rdi
	call	BZ2_bzReadOpen
	movq	%rax, -5080(%rbp)
.L1910:
	cmpq	$0, -5080(%rbp)
	jne	.L1911
	movq	stdin(%rip), %rax
	cmpq	%rax, -5088(%rbp)
	je	.L1912
	movq	stdout(%rip), %rax
	cmpq	%rax, -5088(%rbp)
	je	.L1912
	movq	-5088(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L1912:
	movl	$0, %eax
	jmp	.L1889
.L1911:
	movq	-5080(%rbp), %rax
.L1889:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L1913
	call	__stack_chk_fail
.L1913:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE99:
	.size	bzopen_or_bzdopen, .-bzopen_or_bzdopen
	.globl	BZ2_bzopen
	.type	BZ2_bzopen, @function
BZ2_bzopen:
.LFB100:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movl	$0, %ecx
	movl	$-1, %esi
	movq	%rax, %rdi
	call	bzopen_or_bzdopen
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE100:
	.size	BZ2_bzopen, .-BZ2_bzopen
	.globl	BZ2_bzdopen
	.type	BZ2_bzdopen, @function
BZ2_bzdopen:
.LFB101:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movl	-4(%rbp), %eax
	movl	$1, %ecx
	movl	%eax, %esi
	movl	$0, %edi
	call	bzopen_or_bzdopen
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE101:
	.size	BZ2_bzdopen, .-BZ2_bzdopen
	.globl	BZ2_bzread
	.type	BZ2_bzread, @function
BZ2_bzread:
.LFB102:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	movq	-24(%rbp), %rax
	movl	5096(%rax), %eax
	cmpl	$4, %eax
	jne	.L1919
	movl	$0, %eax
	jmp	.L1920
.L1919:
	movl	-36(%rbp), %ecx
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rsi
	leaq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzRead
	movl	%eax, -4(%rbp)
	movl	-8(%rbp), %eax
	testl	%eax, %eax
	je	.L1921
	movl	-8(%rbp), %eax
	cmpl	$4, %eax
	jne	.L1922
.L1921:
	movl	-4(%rbp), %eax
	jmp	.L1920
.L1922:
	movl	$-1, %eax
.L1920:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE102:
	.size	BZ2_bzread, .-BZ2_bzread
	.globl	BZ2_bzwrite
	.type	BZ2_bzwrite, @function
BZ2_bzwrite:
.LFB103:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	movl	-36(%rbp), %ecx
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rsi
	leaq	-4(%rbp), %rax
	movq	%rax, %rdi
	call	BZ2_bzWrite
	movl	-4(%rbp), %eax
	testl	%eax, %eax
	jne	.L1924
	movl	-36(%rbp), %eax
	jmp	.L1925
.L1924:
	movl	$-1, %eax
.L1925:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE103:
	.size	BZ2_bzwrite, .-BZ2_bzwrite
	.globl	BZ2_bzflush
	.type	BZ2_bzflush, @function
BZ2_bzflush:
.LFB104:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE104:
	.size	BZ2_bzflush, .-BZ2_bzflush
	.globl	BZ2_bzclose
	.type	BZ2_bzclose, @function
BZ2_bzclose:
.LFB105:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L1932
.L1928:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-24(%rbp), %rax
	movzbl	5012(%rax), %eax
	testb	%al, %al
	je	.L1930
	movq	-24(%rbp), %rsi
	leaq	-4(%rbp), %rax
	movl	$0, %r8d
	movl	$0, %ecx
	movl	$0, %edx
	movq	%rax, %rdi
	call	BZ2_bzWriteClose
	movl	-4(%rbp), %eax
	testl	%eax, %eax
	je	.L1931
	movq	-24(%rbp), %rax
	movl	$0, %r8d
	movl	$0, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movl	$0, %edi
	call	BZ2_bzWriteClose
	jmp	.L1931
.L1930:
	movq	-24(%rbp), %rdx
	leaq	-4(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	BZ2_bzReadClose
.L1931:
	movq	stdin(%rip), %rax
	cmpq	%rax, -16(%rbp)
	je	.L1927
	movq	stdout(%rip), %rax
	cmpq	%rax, -16(%rbp)
	je	.L1927
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
	jmp	.L1927
.L1932:
	nop
.L1927:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE105:
	.size	BZ2_bzclose, .-BZ2_bzclose
	.section	.rodata
.LC141:
	.string	"OK"
.LC142:
	.string	"SEQUENCE_ERROR"
.LC143:
	.string	"PARAM_ERROR"
.LC144:
	.string	"MEM_ERROR"
.LC145:
	.string	"DATA_ERROR"
.LC146:
	.string	"DATA_ERROR_MAGIC"
.LC147:
	.string	"IO_ERROR"
.LC148:
	.string	"UNEXPECTED_EOF"
.LC149:
	.string	"OUTBUFF_FULL"
.LC150:
	.string	"CONFIG_ERROR"
.LC151:
	.string	"???"
	.data
	.align 32
	.type	bzerrorstrings, @object
	.size	bzerrorstrings, 128
bzerrorstrings:
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC151
	.quad	.LC151
	.quad	.LC151
	.quad	.LC151
	.quad	.LC151
	.text
	.globl	BZ2_bzerror
	.type	BZ2_bzerror, @function
BZ2_bzerror:
.LFB106:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movl	5096(%rax), %eax
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	jle	.L1934
	movl	$0, -4(%rbp)
.L1934:
	movq	-32(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
	movl	-4(%rbp), %eax
	negl	%eax
	cltq
	movq	bzerrorstrings(,%rax,8), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE106:
	.size	BZ2_bzerror, .-BZ2_bzerror
	.section	.rodata
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	1081081856
	.align 8
.LC4:
	.long	0
	.long	1079574528
	.align 8
.LC5:
	.long	0
	.long	1075838976
	.align 4
.LC118:
	.long	1065353216
	.ident	"GCC: (Ubuntu/Linaro 4.6.1-9ubuntu3) 4.6.1"
	.section	.note.GNU-stack,"",@progbits
