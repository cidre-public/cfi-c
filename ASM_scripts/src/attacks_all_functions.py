#!/usr/bin/python
# N.Audiot
# A.Schorr
#
# lance les attaques par bloc transcientes sur 
# ttes les fonctions d'un prog
# appelle att_bloc_transciente
# 
# argv[1] the .s file to attack
# argv[2] the subdir of out/ where to put results

from sys import argv
from subprocess import PIPE,Popen
from threading import Thread
from Queue import Queue
import re

global RESDIR
RESDIR=argv[2]

def do_work(item):
	#Popen(["python2.7","friedland.py",argv[1],item]);
	#retcode=Popen(["python2.7","friedland.py",argv[1],item]);#,shell = False,stderr=PIPE);
	q = Popen(["/usr/bin/python2.7","src/att_ligne_transciente.py",argv[1],item,RESDIR],shell = False,stderr=PIPE);
	#retcode=call(["python","friedland.py",argv[1],item],shell = False,stderr=PIPE);
	errors = q.communicate()[1]
	retcode = q.returncode
	if retcode != 0:
		print "ERROR launching : "+ item
		print errors

def worker():
	while True:
		item = q.get()
		print "Launching for "+item+"...\n"
		do_work(item)
		q.task_done()
		print item+" : Finished\n"

#########################################
#	 creation liste fonctions	#
#########################################
fonctions = []

for line in open(argv[1]):
	if re.match("^\w+:$", line):
		if not re.match("var_hack", line):
			fonctions.append(line.strip(':,\n'))
	else:
		pass
	
print "Detected: " + str(len(fonctions)) + " functions."
print str(fonctions)
for f in fonctions:
	if f == "gf_log":
		print "Removing functgion gf_log (HACK for AES)"
		fonctions.remove("gf_log")
print "\n*****   LAUNCHING   *****\n"


q = Queue()

print "create queue"
for item in fonctions:
	if (item!="err_hack"):
		q.put(item)

print "launch threads"
for i in range(4):
	t = Thread(target=worker)
	t.daemon = True
	t.start()


q.join()
print "*******ALL DONE !"
