#!/usr/bin/python
#N.Audiot <nicolas.audiot@ensi-bourges.fr>
#A.Schorr <arnaud.schorr@ensi-bourges.fr>
#
#Program created to parse ASM files and return their code tree.
#The result is a DOT file.
# dot -Tjpg file.dot -o file.jpg
#this command line creates a JPEG file to show the code tree.

from sys import argv
from re import compile,match
from string import strip

#creation des blocs manquants
print "Creation des blocs manquants\n==> Generation du .PHASME\n"
jump = False;
file = open(argv[1]+".phasme",'w')
for line in open(argv[1]):
	if compile(r"^\.L[0-9F]").match(line):
		label = line.strip('\n').strip(':')
		index=0
		if jump:
			jump = False
	elif compile(r"^\s+\.").match(line):
		pass
	elif jump:
		file.write(label+"_"+str(index)+'\n')
		index+=1
		if not compile(r"^.j").match(line):
			jump = False
	elif compile(r"^\s+jmp").match(line):
		jump = True
	elif compile(r"^\s+j").match(line):
		jump = True
	file.write(line)
file.close()

#creation du ficher DOT
print "Generation de l'arbre de code\n==> Creation du .DOT\n"
dot = open(argv[1]+".dot",'w')
dot.write("digraph G{\n")
#jmp conditonnel,indique attendre 2e sortie
encours = False
#indique passage par JMP
jump = False
#indique appel fonction exit
exit = False
#premier parcours, creation de l'arbre
index=0
for line in open(argv[1]+".phasme"):
	if compile(r"^\.L[0-9F]").match(line):
		if (encours or not jump) and not exit:
			dot.write(label+"->"+line.strip('\n,.,:')+";\n")
		encours = False
		jump = False
		exit = False
		label = line.strip('\n').strip('.').strip(':')
	elif compile(r"^\s+call\sexit").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n')+"__"+str(index)+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+"->"+label+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+" [shape=box,style=filled,color=red];\n")
		index+=1
		exit = True
	elif compile(r"^\s+ret").match(line):
		exit=True
	elif compile(r"^[^\s]").match(line):
		label = line.strip('\n,:').strip('.')
	elif compile(r"^\s+jmp").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n,.')+";\n")
		jump=True
	elif compile(r"^\s+j").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n,.')+";\n")
		encours = True
	elif compile(r"^\s+call").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n')+"__"+str(index)+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+"->"+label+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+" [shape=box,style=filled,color=red];\n")
		index+=1

#deuxieme parcours, on insere le code dans les noeuds de l'arbre
dot.write("init [label\"")
for line in open(argv[1]+".phasme"):
	if compile(r"^\.L").match(line):
		dot.write("\"];\n"+line.strip('\n').strip('.,:')+" [label=\""+line.strip('\n').strip('.,:')+"\\n")
	elif not compile(r"^\s+").match(line) and not compile(r"^\.").match(line):
		dot.write("\"];\n"+line.strip('\n').strip('.,:')+" [label=\""+line.strip('\n').strip('.,:')+"\\n")
	else:
		dot.write(line.strip('\n').replace('	',' ').replace('"','\\"')+"\\n")
dot.write("\"];\n}\n")
dot.close()
