#!/usr/bin/python
#N.Audiot <nicolas.audiot@ensi-bourges.fr>
#A.Schorr <arnaud.schorr@ensi-bourges.fr>
#
# V1.7.2
#	traitement du code mort
#	finalise. test sur ultimate, fghi
#
#
#Program created to parse ASM files and return their code tree.
#The result is a DOT file.
# dot -Tjpg file.dot -o file.jpg
#this command line creates a JPEG file to show the code tree.

from sys import argv
from re import compile,match
from string import strip

# phase I
#creation des blocs manquants
print "Creation des blocs manquants\n==> Generation du .PHASME\n"
jump = False;
total_instruction = 0;
total_blocs = 0;
file = open(argv[1]+".phasme",'w')
for line in open(argv[1]):
	if compile(r"^\.L[^C]").match(line) or compile(r"^[^\s]").match(line):
		label = line.strip('\n').strip(':')
		index=0
		if jump:
			jump = False
		total_blocs += 1
	elif compile(r"^\s+\.").match(line):
		pass
	elif jump:
		file.write(label+"_"+str(index)+'\n')
		index+=1
		if not compile(r"^.j").match(line):
			jump = False
		total_instruction+=1
		total_blocs+=1
	elif compile(r"^\s+jmp").match(line):
		jump = True
		total_instruction+=1
	elif compile(r"^\s+j").match(line):
		jump = True
		total_instruction+=1
	else:
		total_instruction+=1
	file.write(line)
file.close()

# phase Ib
#preparation detection code mort
print "Nombre d'instructions presentes = "+str(total_instruction)
print "Nombre de blocs de code = "+str(total_blocs)+"\n"
etat_blocs = [[0 for i in range(total_blocs)]for j in range(2)]

def isin(arr,x,n):
	for i in range(n):
		if arr[0][i]==x:
			return True
		else:
			pass
	return False

def valeur_label(array,x,n):
	for i in range(n):
		if array[0][i]==x:
			return array[1][i]

def indice_label(array,x,n):
	for i in range(n):
		if array[0][i]==x:
			return i

def init_is_dead(x,y,array,i,n):
	if isin(array,x,n):
		if isin(array,y,n):
			if valeur_label(array,x,n):
				array[1][indice_label(array,y,n)] = True
		else:
			array[0][i] = y
			array[1][i] = array[1][indice_label(array,x,n)]
			i = i+1
	else:
		if isin(array,y,n):
			pass
		else:
			array[0][i] = y
			array[1][i] = False
		i = i+1
	return i

# phase II
#creation du ficher DOT
print "Generation de l'arbre de code\n==> Creation du .DOT\n"
dot = open(argv[1]+".dot",'w')
temp = open(argv[1]+".temp",'w')
dot.write("digraph G{\n")
#jmp conditonnel,indique attendre 2e sortie
encours = False
#indique passage par JMP
jump = False
#indique appel fonction exit
exit = False
#compteur pr tableau etat des blocs
compt = 0
#premier parcours, creation de l'arbre
index=0
for line in open(argv[1]+".phasme"):
	if compile(r"^\.L[^C]").match(line):
		if (encours or not jump) and not exit:
			dot.write(label+"->"+line.strip('\n,.,:')+";\n")
			temp.write(label+" -> "+line.strip('\n,.,:')+"\n")
			compt = init_is_dead(label,line.strip('\n,.,:'),etat_blocs,compt,total_blocs)
		else:
			etat_blocs[0][compt] = line.strip('\n').strip('.').strip(':')
			etat_blocs[1][compt] = False
			compt+=1
		encours = False
		jump = False
		exit = False
		#premiere iteration detection code mort
		#compt = init_is_dead(label,line.strip('\n,.,:'),etat_blocs,compt,total_blocs)
		#end
		label = line.strip('\n').strip('.').strip(':')
	elif compile(r"^\s+call\sexit").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n')+"__"+str(index)+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+"->"+label+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+" [shape=box,style=filled,color=red];\n")
		index+=1
		exit = True
	elif compile(r"^\s+ret").match(line):
		exit=True
	elif compile(r"^\.LC").match(line) or compile(r"^\.glob").match(line):
		pass
	elif compile(r"^[^\s]").match(line):
		label = line.strip('\n,:').strip('.')
		#init (debut de fonction) pr detection code mort
		etat_blocs[0][compt] = label
		etat_blocs[1][compt] = True
		compt+=1
		#end
	elif compile(r"^\s+jmp").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n,.')+";\n")
		temp.write(label+" -> "+line.split()[1].strip('\n,.,:')+"\n")
		jump=True
	elif compile(r"^\s+j").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n,.')+";\n")
		temp.write(label+" -> "+line.split()[1].strip('\n,.,:')+"\n")
		encours = True
	elif compile(r"^\s+call").match(line):
		dot.write(label+"->"+line.split()[1].strip('\n')+"__"+str(index)+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+"->"+label+";\n")
		dot.write(line.split()[1].strip('\n')+"__"+str(index)+" [shape=diamond,style=filled,color=red];\n")
		index+=1
temp.close()

print "Iteration du processus pour le code mort\n==> Terminaison de l'identification\n"
# phase IIb
# iteration / terminaison identification code mort
modification = True
while(modification):
	modification = False
	for line in open(argv[1]+".temp"):
		if (etat_blocs[1][indice_label(etat_blocs,line.split()[0],total_blocs)] != etat_blocs[1][indice_label(etat_blocs,line.split()[2],total_blocs)]):
			if(etat_blocs[1][indice_label(etat_blocs,line.split()[0],total_blocs)]):
				modification = True
				etat_blocs[1][indice_label(etat_blocs,line.split()[2],total_blocs)] = True


print "Insertion du code dans l'arbre\n==> Completion du DOT\n"
#deuxieme parcours, on insere le code dans les noeuds de l'arbre
code_mort = False
compteur_code_mort = 0
dot.write("init [label\"")
for line in open(argv[1]+".phasme"):
	if compile(r"^\.L[^C]").match(line):
		dot.write("\"];\n"+line.strip('\n').strip('.,:')+" [")
		#code mort
		if not etat_blocs[1][indice_label(etat_blocs,line.strip('\n').strip('.,:'),total_blocs)]:
			code_mort = True
			dot.write("style=filled,color=grey,")
		else:
			code_mort = False
		dot.write("shape=box,label=\""+line.strip('\n').strip('.,:')+"\\n")
	elif not compile(r"^\s+").match(line) and not compile(r"^\.").match(line):
		dot.write("\"];\n"+line.strip('\n').strip('.,:')+" [shape=box,label=\""+line.strip('\n').strip('.,:')+"\\n")
	#code mort
		code_mort = False
	else:
		dot.write(line.strip('\n').replace('	',' ').replace('"','\\"')+"\\n")
		if code_mort:
			compteur_code_mort +=1
dot.write("\"];\n}\n")
dot.close()

print "Nombre instructions non executees = "+str(compteur_code_mort)
res = 1*100/float(total_instruction)
print res
print "Pourcentage code mort : "+str(compteur_code_mort/total_instruction*100)+" %\n"

