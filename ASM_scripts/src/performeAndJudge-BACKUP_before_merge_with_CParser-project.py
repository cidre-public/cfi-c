import subprocess
import time
import re
import math

class performAndJudge:
    
    statistics = {}
    heatmap = []
    md5dictionnaire = {}
    reference_execution_time = 1.0
    original_execution = "This string will be overwritten by the original execution output."
    function_name = "X"
    
    # Activate output
    compare_output = True
            
    def __init__(self, function_name="Please_set_function_name_when_calling_performAndJudge_constructor"):  
        self.statistics["total"] = 0
        self.statistics["bad"] = 0
        self.statistics["good"] = 0
        self.statistics["killed"] = 0
        self.statistics["error"] = 0
        self.statistics["SIG"] = 0
        self.statistics["nottriggered"] = 0
        self.function_name = function_name
        
    # Perform the reference execution
    def original_execution(self, original):
        t = time.time()
        qcat = subprocess.Popen(["cat", "reference.txt"],shell=False,stdout=subprocess.PIPE)
        p = subprocess.Popen([original, "-c"],shell=False,stdin=qcat.stdout, stdout=subprocess.PIPE)
        p2 = subprocess.Popen("md5sum",shell=False,stdin=p.stdout, stdout=subprocess.PIPE)
        p2.wait()
        self.reference_execution_time = time.time() - t
        self.original_execution = p2.communicate()[0]
        #print "ORIGINAL EXECUTION: "
        #print "--------------------"
        #print self.original_execution
        #print "--------------------"

    def perform(self, exe, i=-1, j=-1, setenv=False):
        self.statistics["total"] = self.statistics["total"] + 1
        
        # Creating the command: cat reference.txt | exe -c | md5sum
        # =========================================================
        qcat = subprocess.Popen(["cat", "reference.txt"],shell=False,stdout=subprocess.PIPE)
        # It can be an attack that needs to define environment variables
        if setenv:
            env2 = {"ATTACK_SOURCE" : str(i), "ATTACK_DESTINATION" : str(j)}
        else:
            env2 = {}
        q =  subprocess.Popen([exe, "-c"],shell=False,stdin=qcat.stdout,stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env2)
        q2 = subprocess.Popen("md5sum",shell=False,stdin=q.stdout,stdout=subprocess.PIPE)
                    
        # Waiting processus q to terminate
        t = time.time()
        while (q.poll() == None and t + 2 * self.reference_execution_time > time.time()):
            time.sleep(0.05)
            pass
        
        # Killing if necessary
        killed = False
        errors = ""
        output = ""
        if (q.poll() == None):
            try:
                #print "Killing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                killed = True
                qcat.stdout.close()
                q.stdout.close()
                q2.stdout.close()
                try:
                    qcat.kill()
                    qcat.wait() # Waits that the kill signal take effect
                except OSError:
                    pass
                try:
                    q.kill()
                    q.wait() # Waits that the kill signal take effect
                except OSError:
                    pass
                try:
                    q2.kill()
                    q2.wait() # Waits that the kill signal take effect
                except OSError:
                    pass
            except OSError:
                print "Ooops: try to kill a process but it died before I killed it !"
        else:
            #print "ALL ok: waiting md5sum"
            q2.wait()
            #print "md5sum done."
            errors = q.communicate()[1]
            output = q2.communicate()[0]
        
        # Killing cat, whatever happens to q
        # Closing all pipes and Killing if necessary
        try:
            qcat.kill()
            qcat.wait() # Waits that the kill signal take effect
        except OSError:
            pass
        try:
            q2.kill()
            q2.wait() # Waits that the kill signal take effect
        except OSError:
            pass
                       
        
        # Prints the 2 outputs
#        if (self.compare_output):
#            print "ORIGINAL:"
 #           print self.original_execution
  #          print "-----------------------------------------------------------------------"
   #         print "ATTACKED OUTPUT:" 
    #        print output
     #       print "-----------------------------------------------------------------------"
      #      print "ATTACKED ERRORS:"
       #     print errors
        #    print "-----------------------------------------------------------------------"
            
        # Determination of results
        attack_launched = False
        sigsegv = False
        error_detected = False
        for line_error in errors.split("\n"):
#            print "l:" + line_error
            #if not re.match(line_error,"^$"):
            if len(line_error) != 0:
                if line_error.find("Jumping to:") != -1:
                    attack_launched = True
                elif line_error.find("SIG") != -1:
                    sigsegv = True
                else:
                    error_detected = True
            
        
                   
        # Comparison
        if (killed):
            if (self.compare_output):
                print "Process killed"
            self.statistics["killed"] = self.statistics["killed"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 3")
        elif (sigsegv):
            if (self.compare_output):
                print "SIG sent by executable !"
            self.statistics["SIG"] = self.statistics["SIG"] + 1
        elif (error_detected):
            if (self.compare_output):
                print "Error sent by executable !"
            self.statistics["error"] = self.statistics["error"] + 1
        elif (output != self.original_execution):
            if (self.compare_output):
                print "Output is different !"
            self.statistics["bad"] = self.statistics["bad"] + 1
            if (not(self.md5dictionnaire.has_key(output))):
                self.md5dictionnaire[output] = 1
            else:
                self.md5dictionnaire[output] = self.md5dictionnaire[output] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 1")
        elif not attack_launched:
            if (self.compare_output):
                print "not triggered attack."
            self.statistics["nottriggered"] = self.statistics["nottriggered"] + 1
        elif output == self.original_execution:
            if (self.compare_output):
                print "Executions equals :)"
            self.statistics["good"] = self.statistics["good"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 2")
        else:
            print "ERROR: this case should not happen !"
            pass
        
    def write_results(self):
        
        # md5sum logs
        md5logs = open("out/out.md5", "a")
        for hash in self.md5dictionnaire:
            md5logs.write(str(self.md5dictionnaire[hash]) + " " + str(hash))
        md5logs.close()
        
        # print results                
        print self.statistics
            
        stats = open("out/out.data", "a")
        stats.write(self.function_name)
        for key in sorted(self.statistics):
            if self.statistics[key] != 0:
                logv = math.log((float)(self.statistics[key]), 10)
            else:
                logv = 0
            stats.write(" " + key + " " + str(self.statistics[key]) + " " + str(logv))
        stats.write("\n")
        stats.close()  
        
        stats = open("out/out-" + self.function_name + ".data", "w")
        for values in self.heatmap:
            stats.write(values + "\n")
        stats.close()  
