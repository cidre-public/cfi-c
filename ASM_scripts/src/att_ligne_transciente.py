#!/usr/bin/python
#N.Audiot <nicolas.audiot@ensi-bourges.fr>
#A.Schorr <arnaud.schorr@ensi-bourges.fr>
#
# WAGRAM v1.0
# reexplore le fichier, a modifier
# 
# attaque ligne a ligne systematique d'une fonction
# fonction cible est passee parametre ( argv[2] )
# CIBLE : BZIP2
# argv[2] the subdir of out/ where to put results

from sys import argv
from re import compile
from subprocess import call
from performeAndJudge import performAndJudge
import os
original = "/home/jf/ensib/developement/oberthur/workspace/ASM_scripts/cleaned_src/aes/original-aes"
TIMEOUT = 0.001

FONCTION = argv[2]
RESDIR = argv[3]


#parcours du fichier determination fonctions
total_func = 0
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		total_func +=1
	else:
		pass

# print nombre de fonctions presentes dans le prog
# print total_func

fonctions = [[0 for i in range(total_func)]for j in range(4)]
#print fonctions

# pas sur que les fonctions suivantes soient utiles, on verra
def isin(arr,x,n):
	for i in range(n):
		if arr[0][i]==x:
			return True
		else:
			pass
	return False

def indice_func(array,x,n):
	for i in range(n):
		if array[0][i]==x:
			return i
	print "fonction "+x+" introuvable"

#insertion stats fonctions dans le tableau
# regex : debut de ligne sans blanc sans point
#	match normalement uniquement les noms de fonctions
compteur = 1
index = 0
retour = False
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		fonctions[0][index] = line.strip(':,\n')
		fonctions[1][index] = compteur
		fonctions[3][index]= fonctions[3][index] +1
		if index>0 and not retour:
			fonctions[2][index-1] = compteur
		retour = False
		index+=1
	elif compile(r"^\s+ret").match(line):
		fonctions[2][index-1] = compteur
		retour = True
	elif compile(r"^\.L[0-9]").match(line):
		fonctions[3][index-1] =fonctions[3][index-1]+1
	compteur +=1

#print "nombre de blocs par fonctions: "
#print fonctions[3]
#attention, si pas de retour pour la derniere fonction
# (si pas de main p.ex) on aura pas de valeur de fin pour celle-ci

#for i in range(total_func):
#	print fonctions[0][i],fonctions[1][i],fonctions[2][i]

# creation tableau recensement label blocs des fonctions
max=0
for i in range(total_func):
	if fonctions[3][i]>max:
		max = fonctions[3][i]

blocs = [[0 for i in range(max)]for j in range(total_func)]

# nouveau parcours fichiers, sauvegarde ligne labels
index=0
index_bloc=0
compteur=0
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		blocs[index][0] = compteur
		index+=1
		index_bloc=1
	elif compile(r"^\.L[0-9]").match(line):
		blocs[index-1][index_bloc] = compteur
		index_bloc+=1
	compteur +=1

#print "indexage bloc: "
#print blocs

# NOTE
# pas encore de calcul du temps d'execution standard du programme
# par defaut, fixe a 0.001, defini plus haut

# save empreinte MD5 de base pour comparaison
#md = Popen(["md5sum","cool.bz2"],stdout=PIPE).stdout.read().split()[0]

# fichier resultat
#res = open('bzip2_results.txt','w')
#res.write('normal			'+md)


# fonction compilation
def compile(file):
	gcc = ["gcc","-o",file+".exe",file, "inclusion_err_hack/err_hack.c"]
	try:
		call(gcc,shell=False);
	except OSError:
		plantage = open("out/plantages.log")
		plantage.write("fail GCC "+str(i)+" -> "+str(j))
		plantage.close()

# JFL
judge = performAndJudge(FONCTION, RESDIR)
judge.original_execution(original)

# INSERTION ATTAQUE PAR LIGNE
# attaque sur une seule fonction, $FONCTION
h = indice_func(fonctions,FONCTION,total_func)
nb_done = 0
debut = fonctions[1][indice_func(fonctions,FONCTION,total_func)]
fin = fonctions[2][indice_func(fonctions,FONCTION,total_func)]
print "Range: " + str(debut) + " -> " + str(fin+1) + ": " + str(fin-debut+1) + "^2 to do."
print "Size of function: " + str(fin - debut)
print "To do: " + str((fin - debut)*(fin - debut))
DISTANCE = 100
TRIGGER_LIMIT = 1200
if fin-debut > TRIGGER_LIMIT: # Attacking at distance DISTANCE from the source
		print "FILTERED TRIGGERED *******************************************"
		print "Attack zone > 1200: reducing to DISTANCE=" + str(DISTANCE)
		print "Reducing to : " + str((fin-debut+1)*2*DISTANCE)
for i in range(debut,fin+1):
	for j in range(debut,fin+1):
#		nb_done+=1
#		if(nb_done>3):
#			break
		#if (i!=j):
		if (True):
			func = "out/" + RESDIR + "/" + fonctions[0][h]+"_"+str(i)+"_"+str(j)+".s"
			file=open(func, 'w')
			cpt_ligne=0
			for line in open(argv[1]):
				file.write(line)
				if cpt_ligne == i:
					for linehack in open("inclusion_err_hack/transient-hack.s"):
						file.write(linehack)
				if cpt_ligne == j:
					file.write(".JumpThere:\n")
				cpt_ligne+=1
			file.close()
			compile(func)
			if os.path.exists(func+".exe"):
				print "Judging: " + str(i) + " -> " + str(j)
				judge.perform(func+".exe", i, j, 1000)
			os.remove(func)
			os.remove(func+".exe")
judge.write_results()



