#!/usr/bin/python
#N.Audiot
#A.Schorr
#
#
# lancement des attaques par bloc systematique sur un prog
# appelle att_bloc_systematique sur chaque fonction du prog
#
# on modifie le nombre de threads en bas (par def. "range(6)" )
#

from sys import argv
from re import compile
from subprocess import call,PIPE,Popen
from threading import Thread
from Queue import Queue


# cleaning
try:
	os.remove("out/out.data")
	os.remove("out/plantages.log")
except:
	pass

try:
	os.remove("out/out.md5")
except:
	pass


def do_work(item):
	#Popen(["python2.7","friedland.py",argv[1],item]);
	#retcode=Popen(["python2.7","friedland.py",argv[1],item]);#,shell = False,stderr=PIPE);
	retcode=Popen(["python","att_bloc_systematique.py",argv[1],item],shell = False,stderr=PIPE);
	#retcode=call(["python","friedland.py",argv[1],item],shell = False,stderr=PIPE);
	if retcode!=0:
		print "fail : "+item
		facepalm=open("out/plantage.log",'a')
		facepalm.write("fail : "+item+"\n")
		facepalm.close()

def worker():
	while True:
		item = q.get()
		print "Launching for "+item+"...\n"
		do_work(item)
		q.task_done()
		print item+" : Finished\n"

#########################################
#	 creation liste fonctions	#
#########################################
total_func = 0
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		total_func+=1
	else:
		pass

fonctions = [0 for i in range(total_func)]

cpt=0
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		fonctions[cpt] = line.strip(':,\n')
		cpt+=1
	else:
		pass
print total_func
print fonctions
print "\n\n\n *****   LAUNCHING   *****\n\n\n"


q = Queue()

print "create queue"
for item in fonctions:
	if (item!="err_hack"):
		q.put(item)

print "launch threads"
for i in range(6):
	t = Thread(target=worker)
	t.daemon = True
	t.start()


q.join()
print "\n\n\n\n\n\n\n	************************************************\n\n\n\n\n\n\n\n\n\n		DONE\n\n\n\n\n\n\n\n"
