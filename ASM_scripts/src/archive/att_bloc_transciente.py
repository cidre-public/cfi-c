#!/usr/bin/python
#N.Audiot <nicolas.audiot@ensi-bourges.fr>
#A.Schorr <arnaud.schorr@ensi-bourges.fr>
#
# attaque par bloc transciente sur une fonction
#	nom func passe en parametre
#


from sys import argv
from re import compile
from os import system
from subprocess import call,Popen, PIPE
from signal import signal,alarm,SIGALRM

#JFL
from performeAndJudge import performAndJudge
import os

#FONCTION = "usage"
#FONCTION = "BZ2_bzWrite"
FONCTION = argv[2]
#FONCTION = "main"
TIMEOUT = 0.001
#JFL
original = "bzip2"

print FONCTION
#facepalm = open("out/plantage"+FONCTION+".log",'w')
#facepalm.write("Started for : "+FONCTION)

#parcours du fichier determination fonctions
total_func = 0
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		total_func +=1
	else:
		pass

# print nombre de fonctions presentes dans le prog
# print total_func

fonctions = [[0 for i in range(total_func)]for j in range(4)]
#print fonctions

# pas sur que les fonctions suivantes soient utiles, on verra
def isin(arr,x,n):
	for i in range(n):
		if arr[0][i]==x:
			return True
		else:
			pass
	return False

def indice_func(array,x,n):
	for i in range(n):
		if array[0][i]==x:
			return i
	print "fonction "+x+" introuvable"

#insertion stats fonctions dans le tableau
# regex : debut de ligne sans blanc sans point
#	match normalement uniquement les noms de fonctions
compteur = 1
index = 0
retour = False
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		fonctions[0][index] = line.strip(':,\n')
		fonctions[1][index] = compteur
		fonctions[3][index]= fonctions[3][index] +1
		if index>0 and not retour:
			fonctions[2][index-1] = compteur
		retour = False
		index+=1
	elif compile(r"^\s+ret").match(line):
		fonctions[2][index-1] = compteur
		retour = True
	elif compile(r"^\.L[0-9]").match(line):
		fonctions[3][index-1] =fonctions[3][index-1]+1
	compteur +=1

#print "nombre de blocs par fonctions: "
#print fonctions[3]
#attention, si pas de retour pour la derniere fonction
# (si pas de main p.ex) on aura pas de valeur de fin pour celle-ci

#for i in range(total_func):
#	print fonctions[0][i],fonctions[1][i],fonctions[2][i]

# creation tableau recensement label blocs des fonctions
max=0
for i in range(total_func):
	if fonctions[3][i]>max:
		max = fonctions[3][i]

blocs = [[0 for i in range(max)]for j in range(total_func)]

# nouveau parcours fichiers, sauvegarde ligne labels
index=0
index_bloc=0
compteur=0
for line in open(argv[1]):
	if compile(r"^[^\.^\s]").match(line):
		blocs[index][0] = compteur
		index+=1
		index_bloc=1
	elif compile(r"^\.L[0-9]").match(line):
		blocs[index-1][index_bloc] = compteur
		index_bloc+=1
	compteur +=1

#print "indexage bloc: "
#print blocs

# NOTE
# pas encore de calcul du temps d'execution standard du programme
# par defaut, fixe a 0.001, defini plus haut

# save empreinte MD5 de base pour comparaison
#md = Popen(["md5sum","cool.bz2"],stdout=PIPE).stdout.read().split()[0]

# fichier resultat
#res = open('bzip2_results.txt','w')
#res.write('normal			'+md)


# fonction compilation
def compile(file):
	#gcc = ["gcc","-fno-inlines","-masm=att","-o",file+".exe",file]
	gcc = ["gcc","-m32","-o",file+".exe",file]
	try:
		call(gcc,shell=False);
	except OSError:
		plantage = open("out/plantages.log")
		plantage.write("fail GCC "+str(i)+" -> "+str(j))
		plantage.close()

# JFL
judge = performAndJudge(FONCTION)
judge.original_execution(original)

# INSERTION ATTAQUE PAR BLOCS
# attaque sur une seule fonction, $FONCTION
h = indice_func(fonctions,FONCTION,total_func)
nb_done = 0
#print blocs[h]
for i in blocs[h]:
#	print i
	if i !=0:
		for j in blocs[h]:
#			print j
#			nb_done=nb_done+1
#			if(nb_done>5):
#				print "\n\n\n\n\n\n\nbreaking\n\n\n\n\n"
#				break
			if (j!=0) and (i!=j):
				func = fonctions[0][h]+"_"+str(i)+"_"+str(j)+".s"
				file=open(func, 'w')
				cpt_ligne=0
				for line in open(argv[1]):
					if cpt_ligne == i:
						file.write(line)
						file.write(".debuthack:\n	jmp	.hack\n.finhack:\n")
					elif cpt_ligne == j:
						file.write(line)
						file.write(".desthack:\n")
					else:
						file.write(line)
					cpt_ligne+=1
				file.close()
#				print "compiling "+func
				compile(func)
				if os.path.exists(func+".exe"):
					judge.perform(func+".exe")
				os.remove(func)
				os.remove(func+".exe")
judge.write_results()



#facepalm.write("Done for : "+FONCTION)
#facepalm.close()
