#!/usr/bin/python
#N.Audiot <nicolas.audiot@ensi-bourges.fr>
#A.Schorr <arnaud.schorr@ensi-bourges.fr>
#
# script de creation des labels de blocs supplementaires
#

from sys import argv
from re import compile,match
from string import strip

# phase I
#creation des blocs manquants
print "Creation des blocs manquants\n==> Generation du .PHASME\n"
jump = False;
total_instruction = 0;
total_blocs = 0;
file = open(argv[1]+".phasme",'w')
for line in open(argv[1]):
	if compile(r"^\.L[^C]").match(line) or compile(r"^[^\s]").match(line):
		label = line.strip('\n').strip(':')
		index=0
		if jump:
			jump = False
		total_blocs += 1
	elif compile(r"^\s+\.").match(line):
		pass
	elif jump:
		file.write(label+"_"+str(index)+'\n')
		index+=1
		if not compile(r"^.j").match(line):
			jump = False
		total_instruction+=1
		total_blocs+=1
	elif compile(r"^\s+jmp").match(line):
		jump = True
		total_instruction+=1
	elif compile(r"^\s+j").match(line):
		jump = True
		total_instruction+=1
	else:
		total_instruction+=1
	file.write(line)
file.close()

