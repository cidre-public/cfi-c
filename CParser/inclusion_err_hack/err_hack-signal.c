#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void err_hack()
{
  fprintf(stderr, "Jumping to: 0\n");
}

int killcard()
{
  //exit(6);
  raise(SIGINT);
  return 0;
}
