.JumpFromThisLine:
.LHack3:
 	push	%rax
	push %rbx      
	push %rcx      
	push %rdx      
	push %rbp      
	push %rdi      
	push %rsi      
	push %r8         
	push %r9         
	push %r10      
	push %r11      
	push %r12      
	push %r13      
	push %r14      
	push %r15
	// Sortie de HACKING sur stderr
/*	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC01, %eax
	movq	%rdx, %rcx
	movl	$8, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite*/
	// Sortie de HACKING sur stdout
/*	movl	$.LC01, %eax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf */
	movl	var_hack(%rip), %eax
	subl	$1, %eax
	movl	%eax, var_hack(%rip)
	movl	var_hack(%rip), %eax
	testl	%eax, %eax
	jne	.LHack2
	jmp	.LHack1
	
.LHack1:
	// Sortie de JUMPING sur stderr
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC00, %eax
	movq	%rdx, %rcx
	movl	$8, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	// Sortie de JUMPING sur stdout
/*	movl	$.LC00, %eax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf*/
	pop %r15        
	pop %r14         
	pop %r13         
	pop %r12        
	pop %r11        
	pop %r10         
	pop %r9         
	pop %r8         
	pop %rsi         
	pop %rdi         
	pop %rbp        
	pop %rdx        
	pop %rcx         
	pop %rbx         
	pop	%rax
	jmp	.JumpThere

.LHack2:
	pop %r15         
	pop %r14         
	pop %r13         
	pop %r12         
	pop %r11         
	pop %r10         
	pop %r9         
	pop %r8         
	pop %rsi        
	pop %rdi         
	pop %rbp         
	pop %rdx         
	pop %rcx         
	pop %rbx         
	pop	%rax
	jmp 	.Suite
	nop

//	jmp 	.LHack3
.Suite:	
