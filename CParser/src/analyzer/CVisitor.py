from pycparser import c_ast
import sys


# Stores information for a line of code
#######################################
class InfoForLine():
    level = -1
    start = False
    end = False
    function_name = "NULL"
    isIf = False
    isFor = False
    isWhile = False
    isFirstElse = False
    isFuncCall = False
    
    def __repr__(self):
        return 'Info(' + self.function_name + ':lvl=' + str(self.level) + \
                            (",start" if self.start else '') + \
                             (",end" if self.end else '') + ")"
    
# Visitor of the C source code
##############################
class FuncDeclVisitor(c_ast.NodeVisitor) :

    # Counting the number of functions
    nb_function = 0
    
    # Current level in the code
    level = -1
    
    # Current function name
    function_name = "NULL"
    
    # Toggle when entering a new block
    nextStatementIsFirst = False
    
    # Record last statement of block
    lastViewedStatement = 0
    
    # Fonction starts line
    fonctions_start = {}
    
    # Storing all calls to functions
    fonctions_calls = {}
    
    # For recording the fact that next statement is in an Else block
    toggle_else_detection = False
    
    def __init__(self, collected_lines, all_functions, gcovcovering={}, doGCOV=False):
        self.collected_lines = collected_lines
        self.all_functions = all_functions
        self.gcovcovering = gcovcovering
        self.doGCOV = doGCOV
     
    def ob_coord(self, node) :
        coord = (str(node.coord)).split(':')
        if sys.platform == 'win32' :
            return coord[2]
        elif len(coord) == 2 :
            return coord[1]
        else :
            print "ERROR: this object has no coord object !"
            print node
            node.show()
            sys.exit()
            
################## Visite de depart #################
    def flat_visit(self, node):
        for c in node.children():
            if (isinstance(c, c_ast.FuncDef)) :
                self.nb_function = self.nb_function + 1
                self.function_visit(c)

################## Les visiteurs #################
    def generic_visit(self, node):
        for c in node.children():
            
            # One statement is a compound: visiting recursively
            if (isinstance(c, c_ast.Compound)) :
                #self.collect_start(c, function_name)
                self.block_visit(c)
    
    def gcov_storing_update(self, node):
        if self.doGCOV:
            self.gcov_storing[self.level] = self.gcovcovering[str(self.ob_coord(node))]
        
    def gcov_covering_update(self, node):
        if self.doGCOV:
            self.gcovcovering[str(self.ob_coord(node))] = self.gcovcovering[str(self.ob_coord(node))] 
    
    def gcov_covering_update_loops(self, node):
        if self.doGCOV:
            self.gcovcovering[str(self.ob_coord(node))] = self.gcov_storing[self.level]
    
    def function_visit(self, node):
        # Function detected
        # print "--------------------------------------"
        # print node.decl.name + " L " + str(self.ob_coord(node))
        # print "--------------------------------------"
        # Creating the dictionnary entry for that function
        # "line number" -> InfoForLine
        
        # GCOV
        self.gcov_storing = {} # Reinit for this new function
        # Storing the nb times this statement of this level
        self.gcov_storing_update(node)
        #print "At level " + str(self.level) + " we are at line " + str(self.ob_coord(node)) + " with gcov=" + self.gcovcovering[str(self.ob_coord(node))]
        
        # Storing the name of the function        
        self.function_name = node.decl.name
        # Collecting the starting point of the function
        self.collect_start(node, self.function_name)
        # Visiting the function statements
        #if not self.function_name == "gf_log": # HACK FOR AES TO AVOID THIS FUNCTION
        if True:
            self.all_functions.append(node.decl.name)
            self.generic_visit(node)
        self.function_name = "NULL"
        
    def block_visit(self, node):
        self.level = self.level + 1
        # The next statement is the first satement of a block    
        self.nextStatementIsFirst = True
        #print ">>>>>>>>>> ENTERING BLOCK VISIT <<<<<<<<<<<< " + str(node.coord)
        #print "is instance of If ?" + str(isinstance(node, c_ast.If))
        #print "is instance of Compound ?" + str(isinstance(node, c_ast.Compound))
        if isinstance(node, c_ast.Compound):
            self.compound_visit(node)
        else:
            self.normal_node_visit(node, False)
        
        # if no line has been collected, the next statement is not a starting line
        #print ">>>>>>>>>> EXITING BLOCK VISIT <<<<<<<<<<<<"
        self.lastViewedStatement.finishesABlock = True
        #print "This statement is the last of the block "
        self.nextStatementIsFirst = False 
        self.level = self.level - 1
        
    def compound_visit(self, node):
        #print "Compound visit"
        # Keep i here !
        for i, c in enumerate(node.children()):
            self.normal_node_visit(c, True)
            
    def normal_node_visit(self, c, from_a_compound):
        
            # Normal statement
            # ================
            #print "Normal visit of " + str(c) + " at line " + str(c.coord)
            #c.show()
            #print "is instance of Decl ?" + str(isinstance(c, c_ast.Decl))
            #c.show()
            
            # Storing GCOV info
            oldgcovvalue = -1000
            #print str(self.gcov_storing)
            if self.gcov_storing.has_key(self.level):
                oldgcovvalue = self.gcov_storing[self.level]
            self.gcov_storing_update(c)
            self.gcov_covering_update(c)
            
            #print "At level " + str(self.level) + " we are at line " + str(self.ob_coord(c)) + " with gcov=" + self.gcov_storing[self.level]
            
            # None object: nothing to do
            if (c == None) :
                pass
            # Compound:
            elif (isinstance(c, c_ast.Compound)) :
                #print "nb node.children: " + str(len(node.children()))
                #print "nb c.children: " + str(len(c.children()))
                #c.show()
                #if len(c.children()) > 1:
                    #self.collect_line(c)
                self.compound_visit(c)
            # Declaration: collected
            elif (isinstance(c, c_ast.Decl)) :
                self.collect_line(c, from_a_compound)
            # Assignement: collected
            elif (isinstance(c, c_ast.Assignment)) :
                self.collect_line(c, from_a_compound)
            # FuncCall: collected
            elif (isinstance(c, c_ast.FuncCall)) :
                self.collect_line(c, from_a_compound)
            # Return: collected
            elif (isinstance(c, c_ast.Return)) :
                self.collect_line(c, from_a_compound)
            # If: collected + visit it !
            elif (isinstance(c, c_ast.If)) :
                self.collect_line(c, from_a_compound, True, False)
                self.block_visit(c.iftrue)
                if (c.iffalse != None):
                    self.toggle_else_detection = True
                    self.block_visit(c.iffalse)
            # While: collected + visit it !
            elif (isinstance(c, c_ast.While)) :
                #print "Detected: while> " + self.ob_coord(c)
                # Binary op: i < 20
                # Compound
                self.collect_line(c, from_a_compound, False, False, True)
                # For a while, the gcov value must be overwritten
                self.gcov_storing[self.level] = oldgcovvalue
                self.gcov_covering_update_loops(c)
                #print "UPDATE: At level " + str(self.level) + " we are at line " + str(self.ob_coord(c)) + " with gcov=" + self.gcov_storing[self.level]
                for c_for in c.children():
                    if isinstance(c_for,c_ast.Compound):
                        self.block_visit(c_for)
            # Do ... While: collected + visit it !
            elif (isinstance(c, c_ast.DoWhile)) :
                self.collect_line(c, self.function_name)
                # For a do while, the gcov value must be overwritten
                self.gcov_storing[self.level] = oldgcovvalue
                self.gcov_covering_update_loops(c)
                #TODO: traiter la fin du do while ??
                #print c.children()[1]
                #self.collect_line(c.children()[0], function_name)
                # Visit ONLY the statement of do while(cond) and not cond
                self.compound_visit(c.stmt)
            # BinaryOp: dropped
            elif (isinstance(c, c_ast.BinaryOp)) :
            # Nothing to do
            # if (a < b) => we drop the comparison
                pass
            # UnaryOp:
            elif (isinstance(c, c_ast.UnaryOp)) :
                self.collect_line(c, from_a_compound)
                pass
            # For:
            elif isinstance(c, c_ast.For) :
                # print "Detected: for> " + self.ob_coord(c)
                # For
                # Assignement: i = 0
                # Binary op: i < 20
                # Unary op: i++
                # Compound
                self.collect_line(c, from_a_compound, False, True, False)
                # For a for, the gcov value must be overwritten
                self.gcov_storing[self.level] = oldgcovvalue
                self.gcov_covering_update_loops(c)
                #print "UPDATE: At level " + str(self.level) + " we are at line " + str(self.ob_coord(c)) + " with gcov=" + self.gcov_storing[self.level]
                for c_for in c.children():
                    if isinstance(c_for,c_ast.Compound):
                        self.block_visit(c_for)
            # Goto:
            elif (isinstance(c, c_ast.Goto)) :
                self.collect_line(c, from_a_compound)
                print "WARNING: GOTO DETECTED (probably countermeasure will not work) ! (" + str(c.coord)
                #print "Coordinates : " + str(c.coord)
                #sys.exit()
            # Cast:
            elif (isinstance(c, c_ast.Cast)) :
                self.collect_line(c, from_a_compound)
                pass
            # Break / Continue: collected BUGGY !!!!!!!!!!!!!! CANNOT BE DONE WITH OUR INJECTED CODE
            elif (isinstance(c, c_ast.Break) or isinstance(c, c_ast.Continue)) :
                self.collect_line(c, from_a_compound)
                print "WARNING: Rupture de sequence, on est MAL !" + str(c)
                print "Coordinates : " + str(c.coord)
                #sys.exit()
            # Label: nothing to do
            elif (isinstance(c, c_ast.Label)) :
                self.collect_line(c, from_a_compound)
                if c.stmt is not None:
                    self.block_visit(c.stmt)
                pass
            # Switch: collected + visit it !
            elif (isinstance(c, c_ast.Switch)) :
                self.collect_line(c, from_a_compound)
                self.block_visit(c.stmt)
                pass
            # Id: signle reference to a variable, e.g. if (truc)
            elif (isinstance(c, c_ast.ID)) :
                pass
            # Case:
            elif (isinstance(c, c_ast.Case) or isinstance(c, c_ast.Default)) :
                self.collect_line(c, from_a_compound)
            # ExprList: a list for variadic function: to ignore !
            elif (isinstance(c, c_ast.ExprList)) :
                print "Ignoring  ExprList: (a list for variadic function ?)"
                pass
            # ArrayRef: an access to tab[j] e.g. switch(tab[j])
            elif (isinstance(c, c_ast.ArrayRef)) :
                print "Should not happen: " + str(c)
                print "Coordinates : " + str(c.coord)
                pass
            # Constant: an access to a constant e.g. while(1)
            elif (isinstance(c, c_ast.Constant)) :
                print "Warning: Should not happen: Constant access (e.g. while(1)) " + str(c)
                print "Coordinates : " + str(c.coord)
                #sys.exit()
                pass
            # StructRef: an access to a struct e.g. while(s->state)
            elif (isinstance(c, c_ast.StructRef)) :
                print "Should not happen: " + str(c)
                print "Coordinates : " + str(c.coord)
                pass
            else :
                print "DO YOU JOB: new PyCParser to take into account: " + str(c)
                #print "Coordinates : " + str(c.coord)
                sys.exit()
               
    # Search recursivelly in a assignement statement
    # all possible funcalls 
    def searchFuncCalls(self, node):
        res = False
        this_level_funccalls = []
        #print "considered node: " + str(node)
        # A simple func call
        if isinstance(node, c_ast.FuncCall):
            this_level_funccalls.append(node.name.name)
            res = True
            return res, this_level_funccalls
   
        for c in node.children():
            #if isinstance(c, c_ast.FuncCall):
            #    res = True
            #    # Adding the name of the function called here
            #    this_level_funccalls.append(c.name.name)
            if isinstance(c, c_ast.FuncCall):
                #print "Found funccall: " + str(c)
                this_level_funccalls.append(c.name.name)
                res = True
                #print "detected funccall: " + str(c.name.name)
                #print "searching recursively"
                for c_child in c.children():
                    tmp_child = []
                    tmp_res = False
                    res_subchildren, subchildren_funccalls = self.searchFuncCalls(c_child)
                    tmp_child.append(subchildren_funccalls)
                    tmp_res = tmp_res or res_subchildren
                    #print "Getted: " + str(subchildren_funccalls)
                    if tmp_res:
                        #print "Appending : " + str(this_level_funccalls) + " and " + str(tmp_child)
                        this_level_funccalls = this_level_funccalls + tmp_child
            else:
                res_subchildren, subchildren_funccalls = self.searchFuncCalls(c)
                if res_subchildren:
                    #print "merging " + str(this_level_funccalls) + " and " + str(subchildren_funccalls)
                    #print str(this_level_funccalls + subchildren_funccalls)
                    #this_level_funccalls = this_level_funccalls + subchildren_funccalls
                    this_level_funccalls = this_level_funccalls + subchildren_funccalls
                    res = res_subchildren
        #print "ici: " + str(this_level_funccalls)
        #print res
        return res, this_level_funccalls   


    def collect_line(self, node, from_a_compound, isIf=False, isFor=False, isWhile=False):
        # Possible bug in pycparser ?
        #if self.ob_coord(node) == None:
        #    print "ERROR: Ooops coord is None ! (passing)"
        #    print node
        #else:
        
        # We where not in a compound before the analyzed statement
        # Thus, it's a short if or short while etc...
        # We cannot collect
        if not(from_a_compound):
            return
        
        #print "Collected line: " + self.ob_coord(node) + "(Starts a block = " + str(self.nextStatementIsFirst)
        
        # maybe this lines have been already collected
        if self.ob_coord(node) not in self.collected_lines:
            info = InfoForLine()
            info.level = self.level
            info.function_name = self.function_name
            info.start = self.nextStatementIsFirst
            info.end = False
            info.finishesABlock = False
            info.isIf = isIf
            info.isFor = isFor
            info.isWhile = isWhile
            # Determining if the statement contains a Funccall
            # storing if so.
            info.isFuncCall = isinstance(node, c_ast.FuncCall)
            if isinstance(node, c_ast.FuncCall):
                info.isFuncCall, allfunccalls = self.searchFuncCalls(node)
            if isinstance(node, c_ast.Assignment) or isinstance(node, c_ast.Return):
                info.isFuncCall, allfunccalls = self.searchFuncCalls(node)
            if info.isFuncCall:
                self.fonctions_calls[self.ob_coord(node)] = allfunccalls
                
            
            #print "==========> collecting a line" + str(node) + " lvl=" + str(info.level) + " at line " + self.ob_coord(node)
        
            if self.nextStatementIsFirst:
                self.nextStatementIsFirst = False
            
            global toggle_else_detection
            if self.toggle_else_detection:
                info.isFirstElse = True
                self.toggle_else_detection = False
            
            # Collecting the line !
            self.collected_lines[self.ob_coord(node)]=info
            
            #print "Remembering this statement as the last seen: " + self.ob_coord(node)
            self.lastViewedStatement = info 
       
        else:
            #print "Line already collected: keeping previous state (too remember if it is a first line"
            pass
            
            
            
    def collect_start(self, node, function_name):
        self.fonctions_start[function_name] = self.ob_coord(node)
        
