import subprocess

# Compile file. Filename has no extension
def gcccompile(filename, attack_source = 0, attack_destination = 0, attack_transient_round = -1, gcov = False):
    gcc = ["gcc", filename + ".c",
                          "-o", filename + ".exe", "./inclusion_err_hack/err_hack.c"]
    if attack_source != 0 and attack_destination != 0:
        gcc.append("-DATTACK_SOURCE" + str(attack_source) + "=attack" + str(attack_destination))
        gcc.append("-DATTACK_DESTINATION" + str(attack_destination) + "=1")
        gcc.append("-DATTACK_TRANSIENT_ROUND=" + str(attack_transient_round))
    logline = ""
    if gcov:
        gcc.append("-fprofile-arcs")
        gcc.append("-ftest-coverage")
    try:
        subprocess.call(gcc, shell=False)
    except OSError:
        print "Plantage de la compilation !"
        pass