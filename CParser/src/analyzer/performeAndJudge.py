import subprocess
import time
import re
import math

def maxi(x, y):
    if x > y:
        return x
    else:
        return y

class performAndJudge:
    
    statistics = {}
    heatmap = []
    md5dictionnaire = {}
    reference_execution_time = 1.0
    original_execution = "This string will be overwritten by the original execution output."
    function_name = "X"
    
    # Activate output
    compare_output = False
                
    def __init__(self, function_name="Please_set_function_name_when_calling_performAndJudge_constructor", resultdirectory=""):  
        self.statistics["total"] = 0
        self.statistics["bad"] = 0
        self.statistics["bad1"] = 0
        self.statistics["good"] = 0
        self.statistics["killed"] = 0
        self.statistics["SIG"] = 0
        self.statistics["nottriggered"] = 0
        self.statistics["killcard"] = 0
        self.function_name = function_name
        self.resultdirectory = resultdirectory
        
    # Perform the reference execution
    def original_execution(self, original):
        t = time.time()
        #qcat = subprocess.Popen(["cat", "reference.txt"],shell=False,stdout=subprocess.PIPE)
        # For bzip2, we need the -c option
        #p = subprocess.Popen([original, "-c"],shell=False,stdin=qcat.stdout, stdout=subprocess.PIPE)
        #p = subprocess.Popen([original],shell=False,stdin=qcat.stdout, stdout=subprocess.PIPE)
        p = subprocess.Popen([original], stdout=subprocess.PIPE, shell=False)
        p2 = subprocess.Popen("md5sum",shell=False,stdin=p.stdout, stdout=subprocess.PIPE)
        p2.wait()
        
        # THIS WILL FAIL IF THE OUTPUT IS TOO LONG (it needs a call to communicate to empty
        # the OP buffer and release the wait. See python doc about wait().
        p.wait()
        
        self.reference_execution_time = time.time() - t
        self.original_execution = p2.communicate()[0]
        self.original_return_code = p.returncode
        #self.original_execution = p.communicate()[0]
        print "Original execution time: " + str(self.reference_execution_time) + " and return code: "  + str(self.original_return_code)
        

    def perform(self, exe, i=-1, j=-1, taille=-666, transientround=1):
        self.statistics["total"] = self.statistics["total"] + 1

        # Creating the command: cat reference.txt | exe -c | md5sum
        # =========================================================
        #qcat = subprocess.Popen(["cat", "reference.txt"],shell=False,stdout=subprocess.PIPE)
        # It can be an attack that needs to define environment variables
        # For bzip2, we need the -c option
        #q =  subprocess.Popen([exe, "-c"],shell=False,stdin=qcat.stdout,stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env2)
        #q =  subprocess.Popen([exe],shell=False,stdin=qcat.stdout,stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env2)
        q =  subprocess.Popen([exe],shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        q2 = subprocess.Popen("md5sum",shell=False,stdin=q.stdout,stdout=subprocess.PIPE)
                    
        # Waiting processus q to terminate
        t = time.time()
        while (q.poll() == None and t + maxi(3 * self.reference_execution_time, 0.5) > time.time()):
            time.sleep(0.01)
            pass
        
        # Killing if necessary
        killed = False
        errors = ""
        output = ""
        returncode = 0
        if (q.poll() == None):
            try:
                #print "Killing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                killed = True
                #qcat.stdout.close()
                q.stdout.close()
                q2.stdout.close()
                try:
                    q.kill()
                    q.wait() # Waits that the kill signal take effect
                except OSError:
                    pass
                try:
                    q2.kill()
                    q2.wait() # Waits that the kill signal take effect
                except OSError:
                    pass
            except OSError:
                print "Ooops: try to kill a process but it died before I killed it !"
        else:
            # All processes are dead        
            #output, errors = q.communicate()
            output = q2.communicate()[0]
            errors = q.communicate()[1]
            returncode = q.returncode
            #print "RETURN CODE:" + str(returncode)
        
        # Prints the 2 output
        if (self.compare_output):
            print "ORIGINAL:"
            print self.original_execution
            print "-----------------------------------------------------------------------"
            print "ATTACKED OUTPUT:" 
            print output
            print "-----------------------------------------------------------------------"
            print "ATTACKED ERRORS:"
            print errors
            print "-----------------------------------------------------------------------"
            
        # Determination of results
        attack_launched = False
        sigsegv = False
        killcard = False
        error_detected = False
        line_error_to_report = ""
        for line_error in errors.split("\n"):
            #print "l:" + line_error
            #if not re.match(line_error,"^$"):
            if len(line_error) != 0:
                if line_error.find("Jumping") != -1:
                    attack_launched = True
                elif line_error.find("SIG") != -1:
                    sigsegv = True
                elif line_error.find("Erreur") != -1:
                    sigsegv = True
                elif line_error.find("KILLCARD") != -1:
                    killcard = True
                else:
                    error_detected = True
                    line_error_to_report = line_error 
        
        if returncode != self.original_return_code:
            sigsegv = True
        if returncode == 6:
            killcard = True
                   
        # Comparison
        if (killed):
            if (self.compare_output):
                print "Process killed"
            self.statistics["killed"] = self.statistics["killed"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 3")
        elif (killcard):
            if (self.compare_output):
                print "KILLCARD"
            self.statistics["killcard"] = self.statistics["killcard"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 5")
        elif (sigsegv):
            if (self.compare_output):
                print "Error detected (SIG,Erreur). return code = " + str(returncode)
            self.statistics["SIG"] = self.statistics["SIG"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 4")
        elif (error_detected):
            print "Not catched error:" + line_error_to_report
        elif (output != self.original_execution):
            if (self.compare_output):
                print "Output is different !"
            if taille == 1.0:
                self.statistics["bad1"] = self.statistics["bad1"] + 1
                self.heatmap.append(str(i) + " " + str(j) + " 6")
            elif taille > 1:
                self.statistics["bad"] = self.statistics["bad"] + 1
                self.heatmap.append(str(i) + " " + str(j) + " 1")
            else:
                print "ERROR taille should be > 0 with a BAD: taille = " + str(taille) + " (for function " + self.function_name + ")"
            if (not(self.md5dictionnaire.has_key(output))):
                self.md5dictionnaire[output] = 1
            else:
                self.md5dictionnaire[output] = self.md5dictionnaire[output] + 1
            
        elif not attack_launched:
            if (self.compare_output):
                print "not triggered attack."
            self.statistics["nottriggered"] = self.statistics["nottriggered"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 7")
        elif output == self.original_execution:
            if (self.compare_output):
                print "Executions equals :)"
            self.statistics["good"] = self.statistics["good"] + 1
            self.heatmap.append(str(i) + " " + str(j) + " 2")
        else:
            print "ERROR: this case should not happen !"
            pass
        
    def write_results(self):
        
# MAY BE BROKEN
# SEEMS THAT SOME RESULTS ARE MISSING IN OUT.DATA
#         # md5sum logs
#         md5logs = open("out/" + self.resultdirectory + "/out.md5", "a")
#         for hash in self.md5dictionnaire:
#             md5logs.write(str(self.md5dictionnaire[hash]) + " " + str(hash))
#         md5logs.close()
#         
#         # print results                
#         print self.statistics
            
#         stats = open("out/" + self.resultdirectory + "/out.data", "a")
#         stats.write(self.function_name)
#         for key in sorted(self.statistics):
#             if self.statistics[key] != 0:
#                 logv = math.log((float)(self.statistics[key]), 10)
#             else:
#                 logv = 0
#             stats.write(" " + key + " " + str(self.statistics[key]) + " " + str(logv))
#         stats.write("\n")
#         stats.close()  
        
        stats = open("out/" + self.resultdirectory + "/out-" + self.function_name + ".data", "w")
        for values in self.heatmap:
            stats.write(values + "\n")
        stats.close()  