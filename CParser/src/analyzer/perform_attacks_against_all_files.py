# -*- coding:Utf8 -*-
from Queue import Queue
from threading import Thread
import subprocess
import os
import re
import argparse
import operator
import time
import datetime

# FUNCTION FILTER
# ===============
# BZIP2 FILTER - 8/2/2012 - the functions that we want to include in the analyzis
#filtering = [ "addFlagsFromEnvVar", "add_pair_to_block", "BZ2_blockSort", "BZ2_bzCompressEnd", "BZ2_bzCompressInit", "BZ2_bzCompress", "BZ2_bzWriteClose64", "BZ2_bzWriteOpen", "BZ2_bzWrite", "BZ2_compressBlock", "BZ2_hbMakeCodeLengths", "compress", "compressStream", "copy_input_until_stop", "copy_output_until_stop", "generateMTFValues", "handle_compress", "mainGtU", "mainQSort3", "main", "mainSimpleSort", "mainSort"]
# FILTER - 29/4/2013 - the functions we want to exclude
filtering = []
# BZIP2 FILTER
t_start = time.time()
parser = argparse.ArgumentParser(description='Attack all files found in a dir.')
parser.add_argument('--out', metavar='out',
                   help='the subdirectory of ./out/ where to find .c files and where to put results')
args = parser.parse_args()
subdir=args.out

# CLEAN EXE
# ---------
try:
    os.remove("out/" + subdir + "/*.exe")
except:
    pass

# CLEAN Statistics
# ================
try:
    os.remove("out/" + subdir + "/time.log")
    os.remove("out/" + subdir + "/out.data")
    os.remove("out/" + subdir + "/plantages.log")
except:
    pass

try:    
    os.remove("out/" + subdir + "/out.md5")
except:
    pass

def do_work(item):
    #retcode = subprocess.call(["python2.7", "./src/analyzer/perform_attacks_in_C.py", 
    q = subprocess.Popen(["python2.7", "./src/analyzer/perform_attacks_in_C.py",
                              "--filename=" + item,"--out=" + subdir], shell=False, stderr=subprocess.PIPE)
    errors = q.communicate()[1]
    retcode = q.returncode
    if retcode != 0:
        plantage = open("out/" + subdir + "/plantages.log", "a")
        plantage.write("Plantage pour " + item + "\n")
        #plantage.write(q.communicate()[1])
        plantage.close()
        print "Plantage pour " + item + " - " + errors

def worker():
    while True:
        item = q.get()
        f_name = re.split("(.*)_c-(.*)$", item)
        print "Launching: " + str(f_name[2]) + "\n"
        do_work(item)
        
        q.task_done()
        print "Finished: " + str(f_name[2])

# Build the number of functions to process using the number of found .c files
# Order by size
jobs = []
getall = [ [files, os.path.getsize("./out/" + subdir + "/" + files)] for files in os.listdir("./out/" + subdir) ]
getall = sorted(getall, key=operator.itemgetter(1), reverse=True)
direc = [l[0] for l in getall]
for each_file in direc:
    if re.match(".*\.c$", each_file):
        
        # Filtering the function that need to be executed
        function_names = re.split("_c-", each_file)
        if len(function_names) >= 2:
            right_part = function_names[1]
            function_name = re.split("\.", right_part)[0]
            if function_name in filtering:
                print "Dropped function " + function_name
            else:
                jobs.append("out/" + subdir + "/" + each_file.replace(".c",""))

# Launch k workers that process jobs
q = Queue()
for i in range(4):
    t = Thread(target=worker)
    t.daemon = True
    t.start()

for item in jobs:
    q.put(item)

q.join() # block until all tasks are done
print "ALL DONE !"
timefile = open("out/" + subdir + "/time.log", "a")
reference_execution_time = time.time() - t_start
print reference_execution_time
#simutime = time.strftime("%dd %Hh %Mm %Ss", time.gmtime(reference_execution_time))
simutime = str(datetime.timedelta(seconds=reference_execution_time))
print "Simulation time: " + simutime
timefile.write("Simulation time:\n")
timefile.write(str(reference_execution_time) + "\n")
timefile.write(simutime + "\n")
timefile.close()
