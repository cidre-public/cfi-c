# -*- coding:Utf8 -*-

# WARNING
# WARNING
# WARNING
# COMPATIBLE WITH pycparser 2.04
# WARNING
# WARNING
# WARNING

################### LIBRAIRIES ###################################
import argparse
import sys
import os
import re
from performeAndJudge import performAndJudge
import subprocess
import logging
import math
import gcccompile



# Command line operations
# =======================
parser = argparse.ArgumentParser(description='Executes exhaustively the attacks.')
parser.add_argument('--filename', metavar='filename',
                   help='the filename of the C/EXE file to execute WITHOUT extension')
parser.add_argument('--out', metavar='out',
                   help='the subdirectory of out where to put results')

args = parser.parse_args()
filename=args.filename
resultdirectory=args.out

# If no argument is given:
if (filename == None):
#    filename = "out/out_attacks-cleaned_src_bzip2-modified_okv2_all-secured2_c-BZ2_blockSort"
    print "No argument --filename ! Exiting."
    sys.exit()

path=filename

rematch = re.match(r"(.*)_c-([_a-zA-Z0-9]*)$", filename)
if (rematch == None):
    print "ERROR with the REGEX. Please JFL, fix this !!!" + filename
    sys.exit()
original = rematch.group(1)
function_name = rematch.group(2)

attacks = os.popen("cat " + path + ".c | egrep 'attack.*:'").read()

all = attacks.split('\n')

#labels = set()
#labels = OrderedSet()
labels = []

# Getting labels "attackXX" in the C file
gcovcovering = {}
for attack in all:
    splitattack = re.split("^attack([0-9]+): ; // GCOV=([0-9]+)", attack)
    if (len(splitattack) == 4):
        labels.append(splitattack[1])
        gcovcovering[splitattack[1]] = int(splitattack[2])

#DEVNULL = os.open(os.devnull, os.O_RDWR)

# Compilation of reference file
judge = performAndJudge(function_name, resultdirectory)
# SHOULD KEEP THIS TEST BECAUSE MULTIPLE PROCESS WILL COMPILE THE SAME FILE
# AND SOME OF THEM WILL CONCURRENTLY WRITE THE SAME .exe FILE
# Instead, the general program should erase all .exe file before starting
if not os.path.exists(filename + ".exe"):
    gcccompile.gcccompile(original, gcov=False)
    
while True:
    try:
        judge.original_execution(original + ".exe")
        break
    except OSError:
        print "Warning: ignoring an error when launching the original execution. Doing again !!!"
        pass



# DEBUG
#labels = ['487', '488', '489', '490', '491', '492']

# Attacking
nb_done = 1
nb_total = (int(len(labels))-1)  * (int(len(labels))-1)
for i in range(len(labels)):
    #print "Attack range " + str(i) + "."
    for j in range(len(labels)):
        # Activate this hack to shorten execution (TEST):
        #if (nb_done> 5):
        #    break
        # To avoid jumping at the end or at the beginning in main function
        if ("main" not in filename or (j != len(labels) - 1 and j != 0)):
            source = labels[i]
            dest = labels[j]
            
            # Computing transient round
            transient_rounds = gcovcovering[source]
            #print "TR: " + str(transient_rounds)
            taille = math.fabs(i-j)

            for TR in range(transient_rounds):
            #for TR in range(transient_rounds):
                #print "Attack " + labels[i] + " -> " + labels[j] + " TR = " + str(TR) + " size=" + str(taille) + " : " + str(nb_done) + " done over " + str(nb_total)
                nb_done = nb_done + 1
                try:
                    os.remove(filename + ".exe")
                except:
                    pass
                gcccompile.gcccompile(filename, source, dest, TR)
                if os.path.exists(filename + ".exe"): # The .exe file may not exist (compilation problem)
                    judge.perform(filename + ".exe", source, dest, taille, TR)
                else:
                    print "ERROR: file " + filename  + " does not exist."
                    nb_done = nb_done - 1
                
judge.write_results()
