# -*- coding:Utf8 -*-
################### LIBRAIRIES ###################################
#import commands
import argparse

import os
import shutil
from CVisitor import FuncDeclVisitor
import gcccompile
from performeAndJudge import performAndJudge
import subprocess
import re

CPPPATH = 'cpp'
from pycparser import parse_file

################### VARIABLES GLOBALES ###########################
collected_lines = {}
all_functions = []
transient = True
##################################################################

class InfoForLine():
    level = -1
    start = False
    end = False
    function_name = "NULL"
    isIf = False
    isFor = False
    isWhile = False
    isFirstElse = False

### Instrumentation ###
def instrument(target_file, number, gcovcovering):
    target_file.write("#ifdef ATTACK_DESTINATION" + str(number) + "\n")
    target_file.write("attack" + str(number) + ": ; // GCOV=" + gcovcovering[str(number)] +  "\n") # ";" is important to solve any problem when inserting at the beginning of a function
    target_file.write("#endif\n")
    #target_file.write("if (glob_received_source_attack == " + str(number) + ") { goto HACK; } ")
    target_file.write("#ifdef ATTACK_SOURCE" + str(number) + "\n")
    if transient:
        target_file.write("if (!glob_transient && local_transient == ATTACK_TRANSIENT_ROUND) { glob_transient = -1; \n")
    target_file.write("fprintf(stderr,\"Jumping to: %i\\n\"," + str(number) +");\n")
    target_file.write("goto ATTACK_SOURCE" + str(number) + ";\n")
    if transient:
        target_file.write("} else { if (!glob_transient) local_transient++; } \n")
    target_file.write("#endif\n")
    
def instrument_transient(target_file, var):
    target_file.write("int " + var + " = 0;\n")

def nextCollectedLines(current, collected_lines):
    bestLine = 100000000
    for lineNumber in collected_lines:
        if int(lineNumber) > current and int(lineNumber) < bestLine :
            bestLine = int(lineNumber)
            #print "updating bestLine with " + lineNumber
    if bestLine != 100000000:
        #print "Line " + str(bestLine) + " > " + str(current)
        return bestLine
    else:
        #print "No more lines !!"
        return None

# Command line operations
# =======================
parser = argparse.ArgumentParser(description='Generates the flow graph of a C file.')
parser.add_argument('--filename', metavar='filename',
                   help='the filename of the C file to parse')
parser.add_argument('--out', metavar='out',
                   help='the subdirectory of out where to put results')
args = parser.parse_args()
filename=args.filename
resultdirectory=args.out



# Copying original source
# =======================
filename_first_part = "out/" + resultdirectory + "/" + os.path.basename(filename)
os.popen("cp " + filename + " " + filename_first_part)
filenameh = filename.replace(".c",".h")
if os.path.exists(filenameh):
    print "Copying " + filenameh + " to out/" + resultdirectory
    shutil.copy2(filenameh, "out/" + resultdirectory + "/")
else:
    print "No file " + filenameh + " to copy."

# GCOV
# ====
print "GCOV: Compiling."
gcccompile.gcccompile(filename_first_part.replace(".c",""), gcov=True)
print "GCOV: Executing."
judge = performAndJudge("", resultdirectory)
judge.original_execution(filename_first_part.replace(".c","") + ".exe")
print "GCOV: Generating GCOV."
subprocess.call(["gcov", "-s", "out/" + resultdirectory, os.path.basename(filename)], shell=False)
#print ["gcov", "-s", "out/" + resultdirectory, os.path.basename(filename)]

print "GCOV: extra compiling without GCOV."
print "Helps with blowfish that seems impacted with the introduction of gcov. Restore an clean .exe"
gcccompile.gcccompile(filename_first_part.replace(".c",""), gcov=False)

# GCOV
gcovcovering = {}
lasthowmany = -1
for gcovline in open(os.path.basename(filename) + ".gcov", "r"):
    splitline = re.split("^\s+([0-9\-#]+):\s*([0-9]+):(.*)$", gcovline)
    howmany = splitline[1]
    linenumber = splitline[2]
    sourcecodeline = splitline[3]
    # Approximating this line with the preceeding line (probably the function) 
    if howmany == "-":
        gcovcovering[linenumber] = lasthowmany
        pass
    elif howmany == "#####": # Unexecuted line
        gcovcovering[linenumber] = 1
    else: # Reading how many times this line is seen by gcov
      
        gcovcovering[linenumber] = howmany
        lasthowmany = howmany

# Parsing + Visiting
# ==================
ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH, cpp_args=r'-I./fake_libc_include')
v = FuncDeclVisitor(collected_lines, all_functions, gcovcovering, True)
v.flat_visit(ast)

# Updating GCOV
# For each function, divides GCOV value by the number of times the function is called
current_dividing = 1
for linenumber in range(len(gcovcovering)):
    for each_func, fline in v.fonctions_start.items():
        if int(fline) == linenumber:
            current_dividing = gcovcovering[str(fline)]
    gcovcovering[str(linenumber)] = str(int(gcovcovering[str(linenumber)]) / int(current_dividing))
    
        
    

# Debug: 
# ======
#   - prints the attackable detected lines for each function
#   - prints the detected line for the start of each function
#for f in collected_lines:
#    print f + ": " + str(collected_lines[f])
# print fonctions_start

#filename_first_part = "out/" + resultdirectory + "/out_attacks-" + filename.replace("/","_")
write_starting_function_wait_bracket = False
post_write = False
for target in all_functions:

    print "Generating file for function " + target
    original_file = open(filename, 'r')
        
    target_file = open(filename_first_part.replace(".","_") + "-" + target + ".c", 'w')
    number = 0
    for line in original_file:
        number = number + 1
        
        # Writting an attack label for the ending block
        if post_write:
            instrument(target_file, number, v.gcovcovering)
            post_write = False
        
        # Writes the first line (new global, etc...)
        if (number == 1):
            instrument_transient(target_file, "glob_transient")
                
        # Writes an attack
        if (collected_lines.has_key(str(number))):
            info = collected_lines[str(number)]
            if info.function_name == target:
                
                # Getting info for the next line
                numberNext = nextCollectedLines(number, collected_lines)
                infoNextLine = collected_lines.get(str(numberNext))
                
                # This line is the last before exiting a block
                # We have to remember that we have to write an attack label
                if infoNextLine is not None and infoNextLine.level < info.level:
                    post_write = True
                if infoNextLine is not None and infoNextLine.function_name != info.function_name and 'return' not in line:
                    post_write = True
                
                # This line has a statement: we inject an attack just before
                instrument(target_file, number, v.gcovcovering)
            
        # Writes the original line
        target_file.write(line)
        
        # Writes at the beginning of the function
        if (v.fonctions_start[target] == str(number)):
            write_starting_function_wait_bracket = True
            
        if write_starting_function_wait_bracket and re.match(".*\{.*", line):
            instrument_transient(target_file, "local_transient")
            write_starting_function_wait_bracket = False
            

    original_file.close()
    target_file.close()

print "The code contains " + str(v.nb_function) + " functions."
print "Generated file: " + filename
print "Finished."
