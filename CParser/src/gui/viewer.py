# -*- coding:Utf8 -*-
import argparse
import os
import re
import wx.stc
from wxPython.stc import wxSTC_CARET_SLOP

import sys

class ContreAttaqueDialog(wx.Dialog):
 
    def __init__(self, parent, id, title):

        """Constructor"""
        #wx.PopupWindow.__init__(self, parent, style)

        wx.Dialog.__init__(self, parent, id, "Code contre-mesure", size=(250, 210))
 
        panell = wx.Panel(self, -1)
        vbox = wx.BoxSizer(wx.VERTICAL)
        textctrl = wx.TextCtrl(panell, -1, '', (95, 105))
    """
    #On peut éventuellement ajouter des événements sur la fenêtre
        panell.Bind(wx.EVT_LEFT_DOWN, self.OnMouseLeftDown)
        panell.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        panell.Bind(wx.EVT_LEFT_UP, self.OnMouseLeftUp)
        panell.Bind(wx.EVT_RIGHT_UP, self.OnRightUp)

        st.Bind(wx.EVT_LEFT_DOWN, self.OnMouseLeftDown)
        st.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        st.Bind(wx.EVT_LEFT_UP, self.OnMouseLeftUp)
        st.Bind(wx.EVT_RIGHT_UP, self.OnRightUp)
  
    def OnMouseLeftDown(self, evt):
        self.Refresh()
        self.ldPos = evt.GetEventObject().ClientToScreen(evt.GetPosition())
        self.wPos = self.ClientToScreen((0,0))
        self.panell.CaptureMouse()

    def OnMouseMotion(self, evt):
        if evt.Dragging() and evt.LeftIsDown():
            dPos = evt.GetEventObject().ClientToScreen(evt.GetPosition())
            nPos = (self.wPos.x + (dPos.x - self.ldPos.x),
                    self.wPos.y + (dPos.y - self.ldPos.y))
            self.Move(nPos)

    def OnMouseLeftUp(self, evt):
        if self.panell.HasCapture():
            self.panell.ReleaseMouse()

"""

        
class Code(wx.Panel):
    oldbadbegin = -1
    oldbadend = -1
    
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        #self.quote = wx.StaticText(self, label="Your quote :", pos=(20, 30))

        # A button
        #self.button =wx.Button(self, label="Save", pos=(200, 325))
        #self.lblname = wx.StaticText(self, label="Your name :", pos=(20,60))
        #self.editname = wx.TextCtrl(self, value="",  pos=(0, 0), size=(600,600), style=wx.TE_MULTILINE|wx.TE_READONLY)
        self.editname = wx.stc.StyledTextCtrl(self,   pos=(0, 0), size=(1000,900), style=wx.TE_MULTILINE|wx.TE_READONLY)
        
        
    def load(self, tabularsourcecode):
        sourcecode = ""
        for line in tabularsourcecode:
            sourcecode = sourcecode + line
        #self.editname.SetValue(sourcecode)
        
        self.editname.SetText(sourcecode)
        faces = {
        'times': 'Times',
        'mono' : 'Courier',
        'helv' : 'Helvetica',
        'other': 'new century schoolbook',
        'size' : 12,
        'size2': 10,
        }
        
        cppkeywords = "void char int short long float unsigned NULL register" + \
" template typename for while do if class this"
        self.editname.SetLexer(wx.stc.STC_LEX_CPP)
#         self.editname.SetKeyWords(0, cppkeywords)
#         self.editname.SetCaretForeground("blue")
#         self.editname.StyleSetSpec(wx.stc.STC_STYLE_LINENUMBER,        "back:#C0C0C0,face:%(helv)s,size:%(size2)d" % faces)
#         self.editname.Colourise(0, len(sourcecode))
#         self.editname.SetMarginWidth(1, 100)
#         self.editname.StartStyling(0, wx.stc.STC_INDICS_MASK)
#         #self.editname.StartStyling(0, wx.stc.STC_INDICS_MASK)
#         self.editname.StyleSetSpec(wx.stc.STC_STYLE_LINENUMBER,"fore:#a0a000,back:#C0C0C0,size:5")
#         self.editname.MarkerDefine(wx.stc.STC_MARKNUM_FOLDEROPEN,    wx.stc.STC_MARK_BOXMINUS,          "white", "#808080")
        self.editname.SetMarginType(0, wx.stc.STC_MARGIN_NUMBER)
        self.editname.SetMarginMask(0, 0)
        self.editname.SetMarginWidth(0, 40)
         
        self.editname.StyleSetSpec(wx.stc.STC_C_COMMENT, "fore:#0000ff")
        self.editname.StyleSetSpec(wx.stc.STC_C_COMMENTLINE, "fore:#0000ff")
        self.editname.StyleSetSpec(wx.stc.STC_C_STRING, "fore:#7F007F")
        self.editname.StyleSetSpec(wx.stc.STC_C_IDENTIFIER, "fore:#000000")
        self.editname.StyleSetSpec(wx.stc.STC_C_WORD, "fore:#000000, bold")
        self.editname.SetYCaretPolicy(wxSTC_CARET_SLOP, 0)

    def gotoAttack(self, line, lineend):
        
        reverse = line > lineend
        
        if line > lineend:
            tmp = line
            line = lineend
            lineend = tmp

        # restoring old color
        if self.oldbadbegin != -1:
            self.editname.StartStyling(self.oldbadbegin, 0xffff)
            self.editname.SetStyling(self.oldbadend-self.oldbadbegin, 0)
            
        self.editname.GotoLine(lineend-1)
        posend = self.editname.GetCurrentPos()
        
        self.editname.GotoLine(line-1)
        pos = self.editname.GetCurrentPos()
        if not reverse:
            self.editname.StyleSetSpec(17, "back:#ff0000,fore:#000000")
        else:
            self.editname.StyleSetSpec(17, "back:#ffffff,fore:#ff000000")
        self.editname.StartStyling(pos, 0xffff)
        self.editname.SetStyling(posend-pos, 17)
        self.editname.SetStyling(posend, 0)
        
        self.oldbadbegin = pos
        self.oldbadend = posend

        
        
    def gotoFunction(self, function):
        #self.editname.SetYCaretPolicy(wxSTC_CARET_JUMPS, 0)
        for i in range(self.editname.GetLineCount()):
            line = self.editname.GetLineRaw(i)
            if re.match("^(\w+) (\w+)\((.*)\)", line) and function in line:
                self.editname.GotoLine(i-1)
                return
        

class MyForm(wx.Frame):

    def __init__(self, dirname, functions, tabularsourcecode, stats):
        self.dirname = dirname
        self.functions = functions
        self.stats = stats
        self.rouge = wx.Colour(255,0,0)
        self.vert = wx.Colour(0,178,0)
        self.jaune = wx.Colour(255,255,0)
        self.bleu = wx.Colour(0,102,204)
        self.black = wx.Colour(0,0,0)
        self.orange = wx.Colour(255,158,35)
        
        wx.Frame.__init__(self, None, wx.ID_ANY, "Viewer", (-1,-1), (1600,900)) #1600 pour 16/9

        # Add a panel so it looks the correct on all platforms
        panel = wx.Panel(self, wx.ID_ANY)

        control_panel = wx.Panel(panel)
        self.code_panel = Code(panel)
        
        self.code_panel.load(tabularsourcecode)

        self.listBox = wx.ListBox(control_panel, -1, (0, 0), (600, 300), functions, wx.LB_SINGLE)
        self.listBox.Bind(wx.EVT_LISTBOX, self.chooseFunction)
        self.image_panel = wx.Panel(control_panel)        
        attacks_panel = wx.Panel(panel)
        #self.listBox_attacks = wx.ListBox(attacks_panel, -1, (0, 0), (600, 300), [], wx.LB_SINGLE)
        self.listBox_attacks = wx.ListCtrl(attacks_panel, -1, (0, 0), (400, 300))
        self.listBox_attacks.Bind(wx.EVT_LIST_ITEM_SELECTED, self.gotoAttack)
        self.attack_resume = wx.stc.StyledTextCtrl(attacks_panel,   pos=(0, 0), size=(200,300), style=wx.TE_MULTILINE|wx.TE_READONLY)
        sizer3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer3.Add(self.listBox_attacks, 2, wx.EXPAND)
        sizer3.Add(self.attack_resume, 1, wx.EXPAND)
        attacks_panel.SetSizer(sizer3)
        
        checkbox_panel = wx.Panel(panel)
        x = 0
        self.c_bad = wx.CheckBox(checkbox_panel, -1, "WA J>1", (x, 0))
        self.c_bad.SetValue(True)
        self.c_bad.Bind(wx.EVT_CHECKBOX, self.updateAttacks)
        self.c_bad.SetForegroundColour(self.rouge)
        x = x + 80
        self.c_bad1 = wx.CheckBox(checkbox_panel, -1, "WA J=1", (x, 0))
        self.c_bad1.SetValue(True)
        self.c_bad1.Bind(wx.EVT_CHECKBOX, self.updateAttacks)
        self.c_bad1.SetForegroundColour(self.orange)
        x = x + 80
        self.c_good = wx.CheckBox(checkbox_panel, -1, "EL", (x, 0))
        self.c_good.Bind(wx.EVT_CHECKBOX, self.updateAttacks)
        self.c_good.SetForegroundColour(self.vert)
        x = x + 80
        self.c_error = wx.CheckBox(checkbox_panel, -1, "Error", (x, 0))
        self.c_error.Bind(wx.EVT_CHECKBOX, self.updateAttacks)
        self.c_error.SetForegroundColour(self.bleu)
        #x = x + 80
        #self.c_timeout = wx.CheckBox(checkbox_panel, -1, "Timeout", (x, 0))
        #self.c_timeout.Bind(wx.EVT_CHECKBOX, self.updateAttacks)
        #self.c_timeout.SetForegroundColour(self.jaune)
        x = x + 80
        self.c_killcard = wx.CheckBox(checkbox_panel, -1, "Killcard", (x, 0))
        self.c_killcard.Bind(wx.EVT_CHECKBOX, self.updateAttacks)
        self.c_killcard.SetForegroundColour(self.black)
        
        
        sizer2 = wx.BoxSizer(wx.VERTICAL)
        sizer2.Add(self.listBox, 1, wx.EXPAND)
        sizer2.Add(checkbox_panel, 0.2, wx.EXPAND)
        sizer2.Add(attacks_panel, 1, wx.EXPAND)
        sizer2.Add(self.image_panel, 1, wx.EXPAND)
        control_panel.SetSizer(sizer2)

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(control_panel, 1, wx.EXPAND)
        sizer.Add(self.code_panel, 2, wx.EXPAND)
        panel.SetSizer(sizer)
        
    def analyzeAttackLine(self, attackstring):
        attacksplit = re.split("([0-9]+)->([0-9]+)( \([0-9]\))?", attackstring)
        return attacksplit[1], attacksplit[2]
            

    def addItem(self, source, dest, cumulator, curtype):
        if curtype == 1 and self.c_bad.Value or curtype == 2 and self.c_good.Value or \
           curtype == 3 and self.c_timeout.Value or \
           curtype == 4 and self.c_error.Value or curtype == 5 and self.c_killcard.Value or \
           curtype == 6 and self.c_bad1.Value: 
            item = wx.ListItem()
            if curtype == 1:
                color = self.rouge
            if curtype == 2:
                color = self.vert
            if curtype == 3:
                color = self.jaune
            if curtype == 4:
                color = self.bleu
            if curtype == 5:
                color = self.black
            if curtype == 6 :
                color = self.orange
            string_line = source + "->" + dest + ""
            if cumulator > 0:
                string_line = string_line + " (" + str(cumulator) + ")"
                item.SetText(string_line)
                item.SetTextColour(color)
                self.listBox_attacks.InsertItem(item)


    #La fonction drawFunction permet de dessiner le graphe de la fonction dont le dictionnaire est dic
    def drawFunction(self,event, dic):
        
        dc = wx.PaintDC(self.image_panel)
        dc.Clear()
        
        dc.BeginDrawing()

        #dessin du Rectangle qui va contenir le graphe
        dc.SetPen(wx.Pen('black', 2))
        dc.DrawRectangle( 69, 26, 252, 252)

        #les tableaux minn et maxx contienderont les indices des lignes min et max de chaque filtre
        minn = []
        maxx = []   
        
        for  (dest,source), curtypelist in dic.iteritems():
            minn.append(source)
            maxx.append(dest)

        #les variables minnn et maxxx seront les min et max globaux de la fonction
        minnn= min(min(minn), min(maxx))
        maxxx= max(max(minn),max(maxx))

        #nbrLignes indique le nombre de lignes de la fonction et donc le nombre de cases
        nbrLignes = int(maxxx) - int(minnn) + 1

        #recSize est la taille de la case à dessiner  = 250 qui est le nombre de pixels du graphe / nombre de cases à dessiner
        recSize= 250/nbrLignes

        for  (source ,dest), curtypeList in dic.iteritems():

            #pour chaque élément du dictionnaire on calcul les coordonnées par rapport au repère du graphe
            a = ((int(source) - int(minnn)))* recSize
            b = ((int(dest) - int(minnn)+1)) * recSize

            #on calcul le x et y par rapport au repère du image_panel
            x = a + 70
            y = 275 - b

            #on acctualise les couleurs en blanc
            dc.SetPen(wx.Pen('white', 0.1))
            dc.SetBrush(wx.Brush('white'))

            #si le couple (source, dest) a un seul type d'attaque on l'affiche
            if len(curtypeList) == 1:
                curtype = curtypeList[0]
                if int(curtype) == 1 and self.c_bad.Value :
                    dc.SetBrush(wx.Brush('red'))
                    dc.SetPen(wx.Pen('red', 1))
                if int(curtype) == 2 and self.c_good.Value :
                    dc.SetBrush(wx.Brush('green'))
                    dc.SetPen(wx.Pen('green', 1))
                if int(curtype) == 3 and self.c_timeout.Value :
                    dc.SetBrush(wx.Brush('yellow'))
                    dc.SetPen(wx.Pen('yellow', 1))    
                if int(curtype) == 4 and self.c_error.Value :
                    dc.SetBrush(wx.Brush('blue'))
                    dc.SetPen(wx.Pen('blue', 1))
                if int(curtype)  == 5 and self.c_killcard.Value :
                    dc.SetBrush(wx.Brush('black'))
                    dc.SetPen(wx.Pen('black', 1))
                if int(curtype) == 6 and self.c_bad1.Value :
                    dc.SetBrush(wx.Brush('orange'))
                    dc.SetPen(wx.Pen('orange', 1))
                dc.DrawRectangle(x,y,recSize,recSize)

            #si le couple (source, dest) a plusieurs types d'attaque on répartie le rectangle sur ces types
            else :
                #on calcul le nombre de type d'attaques qu'a subi (source, dest) 
                nbrCurtype = len(curtypeList)

                #on calcul la largeur de chaque petit rectangle affichant chaque type d'attaque pour (source,dest)
                width = int(recSize) / nbrCurtype

                #i nous permettera de se déplacer dans l'axe Y
                i=0
                
                for curtype in curtypelist:
                    if int(curtype) == 1 and self.c_bad.Value :
                        dc.SetBrush(wx.Brush('red'))
                        dc.SetPen(wx.Pen('red', 1))
                    if int(curtype) == 2 and self.c_good.Value :
                        dc.SetBrush(wx.Brush('green'))
                        dc.SetPen(wx.Pen('green', 1))
                    if int(curtype) == 3 and self.c_timeout.Value :
                        dc.SetBrush(wx.Brush('yellow'))
                        dc.SetPen(wx.Pen('yellow', 1))    
                    if int(curtype) == 4 and self.c_error.Value :
                        dc.SetBrush(wx.Brush('blue'))
                        dc.SetPen(wx.Pen('blue', 1))
                    if int(curtype)  == 5 and self.c_killcard.Value :
                        dc.SetBrush(wx.Brush('black'))
                        dc.SetPen(wx.Pen('black', 1))
                    if int(curtype) == 6 and self.c_bad1.Value :
                        dc.SetBrush(wx.Brush('orange'))
                        dc.SetPen(wx.Pen('orange', 1))
                    # on dessine un petit rectangle dont la longueur est recSize et la largeur est width précédemment calculée
                    dc.DrawRectangle(x, y + (i * width), width,int(recSize))
                    i= i+1

        #cette boucle nous permettra d'afficher les indices de lignes dans le repère 
        for i in range(0 , 7) :
            text = int(minnn) + ( i * int(nbrLignes) ) / 6
            saut = (i * 250) / 6 + 63
            dc.DrawText(str(text) , saut, 275)
            text = int(minnn) + ( i * int(nbrLignes) ) / 6   
            saut =  255 - (i * 250) / 6 
            dc.DrawText(str(text) , 40, saut)
            
        #dc.DrawText(minnn, 40, 255) 
        #dc.DrawText(minnn, 63, 275)
        #dc.DrawText(maxxx, 40, 25)
        #dc.DrawText(maxxx, 300, 275)
        dc.EndDrawing()

        #On bind l'image_panel à la fonction onClick par un simple click dans le panel et à la fonction OnShowPopup par un double click
        self.image_panel.Bind(wx.EVT_LEFT_DOWN, lambda evt, temp=minnn, temp2= maxxx, temp3 =recSize: self.dcClick(evt, temp, temp2, recSize))
        self.image_panel.Bind(wx.EVT_LEFT_DCLICK, self.onShowPopup)
        

    #La fonction onClick permettra de mettre en valeur la portion de code de la fonction associée au click en appellant gotoAttack (source,dest)
    def dcClick(self, event, minnn, maxxx, recSize):
        (x,y) = event.GetPosition()

        #ici on change de repère : repère du graphe -> repère de image_panel
        x= x - 70
        y= 275 - y

        
        if x > 0 and x < 250 and y >0 and y < 250 :
            #calcul de la source et de destination en ajoutant l'indice minnn
            x = x / int(recSize) + int(minnn)
            y = y / int(recSize) + int(minnn)
            
            self.code_panel.gotoAttack(int(x), int(y))
        
 
       
    #Cette fonction permettra d'afficher la fenetre où on sera en mesure d'ajouter du code de contre-mesure des attaques sensibles à la main
    def onShowPopup(self, event):
        win = ContreAttaqueDialog(None, -1, 'Code Contre-mesure')
        win.Centre(wx.CENTER_ON_SCREEN) 
        win.Show(True)

    
    def chooseFunction(self, event):
        selName = self.listBox.GetStringSelection()
        self.SetTitle(selName)
        
        self.bad1 = 0
        self.bad = 0
        self.error = 0
        self.good = 0
        self.timeout = 0
        self.killcard = 0
        self.nottriggered = 0
        
        #imgfile = self.dirname + "/out-" + selName + ".datu_heatmap.png"
        #if os.path.isfile(imgfile):
        #    image2 = wx.Image(imgfile, wx.BITMAP_TYPE_ANY)
        #    #image2 = image2.Scale(0.9*image2.GetWidth(),0.9*image2.GetHeight())
        #    wx.StaticBitmap(self.image_panel, wx.ID_ANY, wx.BitmapFromImage(image2))
        #else:
        #    wx.StaticBitmap(self.image_panel, wx.ID_ANY, wx.NullBitmap)
        
        func_stats = self.stats[selName]
        for each_stat in func_stats:
            curtype = int(each_stat[2])
            if curtype == 1:
                self.bad = self.bad + 1
            if curtype == 2:
                self.good = self.good + 1
            if curtype == 3:
                self.timeout = self.timeout + 1
            if curtype == 4:
                self.error = self.error + 1
            if curtype == 5:
                self.killcard = self.killcard + 1
            if curtype == 6 :
                self.bad1 = self.bad1 + 1
            if curtype == 7 :
                self.nottriggered = self.nottriggered + 1
                            
        self.code_panel.gotoFunction(selName)
        self.attack_resume.SetText("Program:\n")
        self.attack_resume.AppendText("Nb functions:" + str(len(self.functions)) + "\n\n")
        self.attack_resume.AppendText("Attack summary:\n")
        self.attack_resume.AppendText("Bad: " + str(self.bad) + "\n")
        self.attack_resume.AppendText("Bad 1: " + str(self.bad1) + "\n")
        self.attack_resume.AppendText("Good: " + str(self.good) + "\n")
        self.attack_resume.AppendText("Error: " + str(self.error) + "\n")
        self.attack_resume.AppendText("Killcard: " + str(self.killcard) + "\n")
        #self.attack_resume.AppendText("Not triggered: " + str(self.nottriggered) + "\n")
        #self.attack_resume.AppendText("Timeout: " + str(self.timeout) + "\n")
        
        self.updateAttacks(event)
        
    
    
    def updateAttacks(self, event):
        selName = self.listBox.GetStringSelection()
        self.listBox_attacks.ClearAll()
        cumulator = 1
        prec_source = "-1"
        prec_dest = "-1"
        prec_type = -1
        func_stats = self.stats[selName]
        
        #On calcul un dictionnaire de donnée pour chaque fonction choisie
        dic ={}
        
        for each_stat in func_stats:
            curtype = int(each_stat[2])


            #si le couple (each_stat[0],each_stat[1]) qu'est (source, dest) existe dans le dictionnaire on ajoute each_stat[2] qui est le type d'attaque au dictionnaire 
            if (each_stat[0],each_stat[1]) in dic.keys():
                if each_stat[2] not in  dic [(each_stat[0],each_stat[1])]:
                    dic [(each_stat[0],each_stat[1])].append(each_stat[2])
            else:
            #sinon on ajoute un couple (clé , valeur) avec clé = (source, dest) et valeur = [typeAttaque]
                dic [(each_stat[0],each_stat[1])]=[each_stat[2]]
                        
            ############################################################################################################
                
            if prec_source != -1:
                if prec_source == each_stat[0] and prec_dest == each_stat[1] and prec_type == curtype:
                    cumulator = cumulator + 1
                else:
                    if prec_source != -1:
                        self.addItem(prec_source, prec_dest, cumulator, prec_type)
                        #self.addPoint(prec_source, prec_dest, cumulator, prec_type, dc)
                        #print prec_source, prec_dest, cumulator, prec_type, dc
                    cumulator = 1
                    prec_source = each_stat[0]
                    prec_dest = each_stat[1]
                    prec_type = curtype
   
        # Eventually, the last should be added:
        if prec_source != -1:
            self.addItem(prec_source, prec_dest, cumulator, prec_type)

        self.drawFunction(event, dic)

        
        
            
    def gotoAttack(self, event):
        selected = self.listBox_attacks.GetFirstSelected()
        attackstring = self.listBox_attacks.GetItem(selected).GetText()
        attacksource, attackdest = self.analyzeAttackLine(attackstring)
        self.code_panel.gotoAttack(int(attacksource), int(attackdest))
            
        
# Run the program
if __name__ == "__main__":
    functions = []
    parser = argparse.ArgumentParser(description='Injects countermeasures.')
    parser.add_argument('--dir', metavar='dir',
                   help='the directory where the files are')
    parser.add_argument('--cfile', metavar='out',
                   help='the original c file')
    args = parser.parse_args()
    dirname = args.dir
    studiedCfile = args.cfile
    
    # Preparing list of functions
    dirList=os.listdir(dirname)
    for fname in dirList:
        if re.match("out-(.*)\.data$", fname):
            funcname = re.split("out-(.*)\.data$", fname)[1]
            functions.append(funcname)
            
    # Preparing source code display
    tabularsourcecode = []
    for line in open(dirname + "/" + studiedCfile):
        tabularsourcecode.append(line)
    
    # Preparing results
    print "Loading data....",
    stats = {}
    for selName in functions:
        func_stats = []
        sys.stdout.write(".")
        for line in open(dirname + "/out-" + selName + ".data"):
            if re.match("^([0-9]+) ([0-9]+) ([0-9]?)$", line):
                splitline = re.split("^([0-9]+) ([0-9]+) ([0-9]?)$", line)
                func_stats.append((splitline[1], splitline[2], splitline[3]))
        stats[selName] = func_stats
        
    app = wx.PySimpleApp()
    frame = MyForm(dirname, functions, tabularsourcecode, stats).Show()
    app.MainLoop()
