# -*- coding:Utf8 -*-
################### LIBRAIRIES ###################################
#import commands
from pycparser import parse_file
#from analyzer.CVisitor import FuncDeclVisitor
from analyzer import CVisitor
import argparse
import os
import sys
import re

CPPPATH = 'cpp'


################### VARIABLES GLOBALES ###########################
collected_lines = {}
all_functions = []
all_functions_nbid_nbcalls = {}
global tabulation
tabulation = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
##################################################################


def nextCollectedLines(current):
    bestLine = 100000000
    for lineNumber in collected_lines:
        if int(lineNumber) > current and int(lineNumber) < bestLine :
            bestLine = int(lineNumber)
            #print "updating bestLine with " + lineNumber
    if bestLine != 100000000:
        #print "Line " + str(bestLine) + " > " + str(current)
        return bestLine
    else:
        #print "No more lines !!"
        return None
    
# The nocheck boolean enables or disable the CHECKs inside basic blocks.
# nocheck=False: a CHECK_INCR macro is inserted between each line of code
# nocheck=True: a INCR is inserted insides basic blocks
def countermeasure(counter_name, counter_state, counter_derivation, nocheck=True):
    global check_in_BB
    new_line = ""
    if counter_derivation.has_key(counter_name):
        derivation = counter_derivation[counter_name]
    else:
        derivation = ""
    final_counter_name = counter_name + derivation
    if nocheck and not check_in_BB:
        new_line = new_line + tabulation + "INCR(" +  final_counter_name + ")\n"
    else:
        new_line = new_line + tabulation + "CHECK_INCR(" +  final_counter_name + "," + str(counter_state[final_counter_name]) + ")\n"
    counter_state[final_counter_name] = counter_state[final_counter_name] + 1
    return new_line



def countermeasure_after_funccall(counter_call, counter_name, counter_state, counter_derivation):
    new_line = ""
    if counter_derivation.has_key(counter_name):
        derivation = counter_derivation[counter_name]
    else:
        derivation = ""
    final_counter_name = counter_name + derivation
    # Retransforming counter name without the trainling number:
    split_counter_name = re.split("(.*)_CALLNB_(.*)", counter_call)
    first_part_counter_name = split_counter_name[1]
    new_line = new_line + tabulation + "CHECK_INCR_FUNC(" +  final_counter_name + "," + str(counter_state[final_counter_name]) + "," + counter_call + "," + first_part_counter_name + "NBENDFUNC" + ")\n"
    counter_state[final_counter_name] = counter_state[final_counter_name] + 1
    return new_line

def check_loop(counter_name, counter_state, counter_derivation, b):
    new_line = ""
    if counter_derivation.has_key(counter_name):
        derivation = counter_derivation[counter_name]
    else:
        derivation = ""
    final_counter_name = counter_name + derivation
    new_line = new_line + tabulation + "CHECK_LOOP_INCR(" +  final_counter_name + "," + str(counter_state[final_counter_name]) + ", " + b +  ")\n"
    counter_state[final_counter_name] = counter_state[final_counter_name] + 1
    return new_line

def check_incr_cond_if(counter_b, counter_name, cond, counter_state, counter_derivation, nocheck=True):
    if counter_derivation.has_key(counter_name):
        derivation = counter_derivation[counter_name]
    else:
        derivation = ""
    final_counter_name = counter_name + derivation
    if not(nocheck):
        new_line = "CHECK_INCR_COND(" + counter_b + "," + final_counter_name + ", " + str(counter_state[final_counter_name]) + ", " + cond + ")"
    else:
        new_line = "INCR_COND(" + counter_b + "," + final_counter_name + ", " + cond + ")"
    counter_state[final_counter_name] = counter_state[final_counter_name] + 1
    return new_line

def check_incr_cond(counter_b, counter_while, while_line, counter_state, nocheck=True):
    if not(nocheck):
        new_line = "CHECK_INCR_COND(" + counter_b + "," + counter_while + ", 0, " + while_line + ")"
    else:
        new_line = "INCR_COND(" + counter_b + "," + counter_while + ", " + while_line + ")"
    counter_state[counter_while] = counter_state[counter_while] + 1
    return new_line

def check_if_else(counter_name, counter_state, counter_derivation, counter_boolean):
    return tabulation + "CHECK_END_IF_ELSE(" +  counter_name + "then," + counter_name + "else," + counter_boolean + "," \
               + str(counter_state[counter_name + "then"]) + "," + str(counter_state[counter_name + "else"]) +  ")\n"

def check_if(counter_name, counter_state, counter_derivation, counter_boolean):
    return tabulation + "CHECK_END_IF(" +  counter_name + "then," + counter_boolean + "," \
               + str(counter_state[counter_name + "then"]) + ")\n"

def check_for(counter_name, counter_boolean):
    return tabulation + "CHECK_END_LOOP(" +  counter_name + "for," + counter_boolean +  ", 1)\n"

def check_while(counter_name, counter_boolean):
    return tabulation + "CHECK_END_LOOP(" +  counter_name + "while," + counter_boolean +  ", 1)\n"


def decl_init(counter_name, counter_state, state):
    if counter_state.has_key(counter_name):
        counter_state[counter_name] = state
        return tabulation + "INIT(" + counter_name + "," + str(state) + ")\n"
    else:
        counter_state[counter_name] = state
        return tabulation + "DECL_INIT(" + counter_name + "," + str(state) + ")\n"

def decl_init_main(counter_name, counter_state, state):
        counter_state[counter_name] = state
        return tabulation + "DECL_INIT_MAIN(" + counter_name + "," + str(state) + ")\n"

def decl(counter_name, counter_state):
    if counter_state.has_key(counter_name):
        # nothing to do: already declared
        return ""
    else:
        counter_state[counter_name] = -666
        return tabulation + "DECL(" + counter_name + ")\n"

def dropping_counter(counter_state, level):
    for counter in counter_state.keys():
        countersplit = re.split("CNT_(.*)___.*", counter)
        if int(countersplit[1]) - 1 > level:
            counter_state.pop(counter)
            print "Dropping counter " + counter

def generate_counter_name(function_name, all_functions_nbid_nbcalls):
    counter_calls_to_f = all_functions_nbid_nbcalls[function_name][1]
    all_functions_nbid_nbcalls[function_name] = [all_functions_nbid_nbcalls[function_name][0], counter_calls_to_f+1]
    return "CNT_0___" + function_name + "_CALLNB_" +  str(counter_calls_to_f)

def infoHasGeneratedPostCHECK(info):
    return info is not None and \
        (info.isFor or  info.isWhile or (info.isFuncCall and not hasattr(info, 'isNotPatchedFuncCall')))

def decl_init_funccalls(original_line, line, funccalls, counter_state, all_functions_nbid_nbcalls, all_functions, counter_name_current,  counter_derivation):
    # funccalls = [['gf_mulinv', ['blop']]]
    # x = gf_mulinv(blop(k))
    # Command line operations
    # =======================
    
    new_decl = "" # To avoid any output if the function should not be patched
    new_line = ""# To avoid any output if the function should not be patched
    new_post_line = ""# To avoid any output if the function should not be patched
    #print funccalls
    if len(funccalls) == 1:
        if isinstance(funccalls[0], basestring): #['printf']
            #extract_funccall = re.split(r'' + funccalls[0] + '\((.*)\)', line)
            # THIS REG EX DOES NOT WORK FOR EXAMPLE IF THE LINE IS:
            # func(bla bla )+(blob)
            # Thus, we have to search the right ending parenthese
            extract_funccall = re.split(r'' + funccalls[0] + '\s*\(', line)
            after_func_name = extract_funccall[1] 
            search_ending_parenthese = 0
            parenthese_level = 1
            while parenthese_level != 0:
                if after_func_name[search_ending_parenthese] == '(':
                    parenthese_level = parenthese_level + 1
                elif after_func_name[search_ending_parenthese] == ')':
                    parenthese_level = parenthese_level - 1
                search_ending_parenthese = search_ending_parenthese + 1
            func_parameters = after_func_name[:search_ending_parenthese-1]
            end_of_line = after_func_name[search_ending_parenthese:]
            # Here we have: 
            # line = extract_funccall[0] + funccalls[0] + "(" + func_parameters + ")" + end_of_line
            if funccalls[0] in all_functions: # This function is one of mines (eg not a printf)
                # Creating counter and DECL_INIT
                counter_name = generate_counter_name(funccalls[0], all_functions_nbid_nbcalls)
                new_decl = decl_init(counter_name, counter_state, all_functions_nbid_nbcalls[funccalls[0]][0])
                new_decl = new_decl + countermeasure(counter_name_current, counter_state, counter_derivation) 
                #print "Creating NEW EXTERNAL counter " + counter_name
                # Rewritting function call
                virgule = True
                if re.match("^\s*$", func_parameters):
                    virgule  = False
                new_line = extract_funccall[0] + funccalls[0] + "(" +  func_parameters + ("," if virgule else "") + " & " + counter_name + ")"+ end_of_line
                if "return" not in original_line: # To avoid a post line that would add something behind a return
                    new_post_line = countermeasure_after_funccall(counter_name, counter_name_current, counter_state, counter_derivation)
            else:
                new_line = line
            
            
        else: # [['printf']]
            new_decl, new_line, new_post_line = decl_init_funccalls(original_line, line, funccalls[0], counter_state, all_functions_nbid_nbcalls, all_functions, counter_name_current,  counter_derivation)
    elif len(funccalls) >= 2: 
        # We have my_function( .. my_other_function ..): ['my_function', ['my_other_f']]
        # this is a call of a call ie my_function( my other_f)
        #
        # but we can have: ['multiply', 'multiply', 'multiply', ['f']]
        # and this is because it is two calls multiply(A) ^ multiply(B) ^ multiply( f ())
        # We take the first, and then continue recursively.
        #print funccalls
        #print len(funccalls)
        #print line
        assert isinstance(funccalls[0], basestring)
        if isinstance(funccalls[1], basestring) or len(funccalls) > 2: 
            #['multiply', 'multiply', ...]
            #['multiply', 'multiply', 'f', 'g']
            #['multiply', ['multiply', 'f'], 'g']
            # Treat the first:
            extract_funccall = re.split(r'' + funccalls[0] + '\s*\((.*)$', line)
            begin_line = extract_funccall[0] + funccalls[0] + "("
            level_parenthese = 1
            i = 0
            while level_parenthese != 0:
                c = extract_funccall[1][i]
                if c == '(':
                    level_parenthese = level_parenthese + 1
                    begin_line = begin_line + c
                elif c == ')':
                    level_parenthese = level_parenthese - 1
                else:
                    begin_line = begin_line + c
                i = i + 1
            end_line = extract_funccall[1][i:]
            #print "new begin line: " + begin_line
            if funccalls[0] in all_functions: # This function is one of mines (eg not a printf)
                counter_name = generate_counter_name(funccalls[0], all_functions_nbid_nbcalls)
                new_decl = decl_init(counter_name, counter_state, all_functions_nbid_nbcalls[funccalls[0]][0])
                new_decl = new_decl + countermeasure(counter_name_current, counter_state, counter_derivation)
                begin_line = begin_line + "," + " &" + counter_name 
            begin_line = begin_line + ")";
            #print "new begin line: " + begin_line
            new_list = funccalls[1:]
            #print new_list
            #print "computing on the rest of the line: " + str(end_line) + " with table " + str(new_list)
            
            new_decl_rec, new_next_line, new_post_line_rec = decl_init_funccalls(original_line, end_line, new_list, counter_state, all_functions_nbid_nbcalls, all_functions, counter_name_current,  counter_derivation)
            new_post_line = countermeasure_after_funccall(counter_name, counter_name_current, counter_state, counter_derivation)
            new_line = begin_line + new_next_line
            new_decl = new_decl + new_decl_rec
            new_post_line = new_post_line_rec + new_post_line 
            #print "final new_line " + new_line
#                 
        elif not isinstance(funccalls[1], basestring) and len(funccalls) == 2: #['multiply', ['multiply',]]
            extract_funccall = re.split(r'' + funccalls[0] + '\((.*)\)', line)
            if funccalls[0] in all_functions: # This function is one of mines (eg not a printf)
                new_decl_rec, new_param, new_post_line = decl_init_funccalls(original_line, extract_funccall[1], funccalls[1], counter_state, all_functions_nbid_nbcalls, all_functions, counter_name_current,  counter_derivation)
                # Creating counter and DECL_INIT
                counter_name = generate_counter_name(funccalls[0], all_functions_nbid_nbcalls)
                new_decl = decl_init(counter_name, counter_state, all_functions_nbid_nbcalls[funccalls[0]][0]) + new_decl_rec
                #print "Creating NEW EXTERNAL counter " + counter_name
                # Rewritting function call
                virgule = True
                if re.match("^\s*$", new_param):
                    virgule  = False
                new_line = extract_funccall[0] + funccalls[0] + "(" +  new_param + ("," if virgule else "") + " &" + counter_name + ")" + extract_funccall[2]
            else:
                new_decl_rec, new_param, new_post_line = decl_init_funccalls(original_line, extract_funccall[1], funccalls[1], counter_state, all_functions_nbid_nbcalls, all_functions, counter_name_current,  counter_derivation)
                new_line = extract_funccall[0] + funccalls[0] + "(" +  new_param + ")" + extract_funccall[2]
        else:
            print "More than 2 function calls: JFL, DO YOUR JOB !"
            sys.exit()
        
    return new_decl, new_line, new_post_line
#     find_funccalls = re.split(r'[^.*=]([\s\t]*)(\w+)\(.*\);', line)
#     if len(find_funccalls) != 4:
#         print "Error impossible to parse:"
#         print line
#         print str(find_funccalls)
#         sys.exit(-1)
    pass


parser = argparse.ArgumentParser(description='Injects countermeasures.')
parser.add_argument('--filename', metavar='filename',
                   help='the filename of the C file to parse')
parser.add_argument('--out', metavar='out',
                   help='the subdirectory of out where to put results')
parser.add_argument('--check_in_BB', dest='check_in_BB', action='store_true',
                   help='to activate checks inside Basic Blocks')
parser.set_defaults(check_in_BB=False)
args = parser.parse_args()
filename=args.filename
resultdirectory=args.out
check_in_BB=args.check_in_BB
print "Checking inside Basic Blocks (option --check_in_BB): " + str(check_in_BB)

# Parsing + Visiting
# ==================
ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH, cpp_args=r'-I./fake_libc_include')
v = CVisitor.FuncDeclVisitor(collected_lines, all_functions)
v.flat_visit(ast)

# Preparing the numbering of functions and the numbering of calls
# ===============================================================
nbid = 48
for f in all_functions:
    all_functions_nbid_nbcalls[f] = [nbid, 1]
    nbid = nbid + 1 

filenamesplit = filename.split("/")
filename_first_part = "out-cfi/" + resultdirectory + "/" + filenamesplit[len(filenamesplit)-1]
# Copying original source
os.popen("cp " + filename + " " + filename_first_part)
    
original_file = open(filename, 'r')
        
target_file = open(filename_first_part.replace(".","_CFI."), 'w')
artefacts = open(filename_first_part.replace(".c","_CFI.h"), 'w')
number = 0
number_preceeding = 0
counter_state = {}
counter_derivation = {}
counter_already_created = {}
already_defined = []
counter_stack = {}
current_function_name = "----"
artefact_numbering = {}
artefact_counter = 0
write_starting_function_wait_bracket = False
stack_line = ""
for line in original_file:
    number = number + 1
    pre_line = ""
    post_line = ""
        
    # Writes the first line (new global, etc...)
    if (number == 1):
        macros = open("security_macros.h", 'r')
        for security_line in macros:
            artefacts.write(security_line)
        artefacts.write("\n\n")
        macros.close()
        target_file.write("#include \"" + os.path.split(artefacts.name)[1] + "\"\n")
    
    # Detecting the beginning of a function
    # =====================================
    for f,functions_line_number in v.fonctions_start.items():
        if functions_line_number == str(number):
            current_function_name = f
            info_preceeding = None
            
    # Rewritting the signature of functions
    # =====================================
    if v.fonctions_start.has_key(current_function_name) and v.fonctions_start[current_function_name] == str(number):
        #    write_starting_function_wait_bracket = True
        #    
        #if write_starting_function_wait_bracket and re.match(".*\{.*", line):
        #write_starting_function_wait_bracket = False
        split_func_decl = re.split("(.*)\)(.*)", line)

        
        created_counter_name_current = "*CNT_0___" + str(current_function_name)
        fake_declaration = decl_init(created_counter_name_current, counter_state, all_functions_nbid_nbcalls[current_function_name][0])
        #99 - > 
        counter_stack[0] = created_counter_name_current
        
        if current_function_name != "main":
            virgule = True
            if re.match(".*\(\s*$", split_func_decl[1]):
                    virgule  = False
            line = split_func_decl[1] + ("," if virgule else "") + "  unsigned short " + created_counter_name_current + ")" + split_func_decl[2] + "\n"
            # Fake declaration: initializes the counter in the map
        else: #Exception for main: we inject the declaration and a countermeasure with a check
            stack_line = stack_line + decl_init_main("CNT_0___" + str(current_function_name), counter_state, all_functions_nbid_nbcalls[current_function_name][0])
            stack_line = stack_line + countermeasure(created_counter_name_current, counter_state, counter_derivation, False)
       
        
    # Builds the countermeasure for this collected line
    # =================================================
    if (str(number) in collected_lines.keys()):
        # Getting info about the current line
        info = collected_lines.get(str(number))
        # Getting info for the preceeding line
        info_preceeding = collected_lines.get(str(number_preceeding))
        
        # Getting info for the next line
        numberNext = nextCollectedLines(number)
        infoNextLine = collected_lines.get(str(numberNext))
        
        # If we are entering a new function then, we reset the preceeding counter
#         if info.function_name != current_function_name:
#             current_function_name = info.function_name
#             info_preceeding = None
        
        # Calculating counter names
        # =========================
        counter_name_current = ("*" if info.level == 0 else "") + "CNT_" + str(info.level) + "___" + str(info.function_name) 
        if infoNextLine is not None:
            counter_name_next = ("*" if infoNextLine.level == 0 else "") + "CNT_" + str(infoNextLine.level) + "___" + str(info.function_name)
        if info_preceeding is not None:
            counter_name_prec = ("*" if info_preceeding.level == 0 else "") + "CNT_" + str(info_preceeding.level) + "___" + str(info.function_name)
        

        
        # The line just entered a for
        # prec = LVL 0 ------> current = LVL 1 
        # ====================================
        # We just change of block If->Else
        if info.isFirstElse:
            counter_derivation[counter_name_current] = "else"
            # Dropping counters that are not declared anymore
            # If we change of block from an IF to an ELSE, all counters of current level -1 have to expire
            dropping_counter(counter_state, info.level-1)
            
        # The current line is just after an exiting block
        # Prec = LVL X -----------> Current = LVL 0
        # =========================================
        if info_preceeding is not None and info_preceeding.level > info.level and not info.isFirstElse:
            counter_prec_of_min_level = counter_stack[info.level + 1]
            if counter_derivation[counter_prec_of_min_level] == "for":
                artefactnb = str(artefact_numbering[counter_prec_of_min_level][0])
                incrementation_part_of_for = str(artefact_numbering[counter_prec_of_min_level][1])
                pre_line = pre_line + incrementation_part_of_for + ";\n";
                pre_line = pre_line + countermeasure(counter_prec_of_min_level, counter_state, counter_derivation)
                pre_line = pre_line + "goto " + "begin" + counter_prec_of_min_level + "for" + artefactnb + ";\n"
                #pre_line = pre_line + "goto " + "begin" + counter_prec_of_min_level + "for" + artefactnb + ";\n" # Duplicate
                pre_line = pre_line + "end" + counter_prec_of_min_level + "for" + artefactnb + ": ;\n" # ; necessary to put a statement after this label
                print "Creating artefact " + counter_prec_of_min_level + "NBEND" +  artefactnb
                artefacts.write("#define " + counter_prec_of_min_level + "NBEND" +  artefactnb + " " \
                                + str(counter_state[counter_prec_of_min_level + "for"]) + "\n");
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + check_for(counter_prec_of_min_level, counter_prec_of_min_level + "_b")
            if counter_derivation[counter_prec_of_min_level] == "while":
                artefactnb = str(artefact_numbering[counter_prec_of_min_level][0])
                pre_line = pre_line + "goto " + "begin" + counter_prec_of_min_level + "while" + artefactnb + ";\n"
                #pre_line = pre_line + "goto " + "begin" + counter_prec_of_min_level + "while" + artefactnb + ";\n" # duplicate !!!
                pre_line = pre_line + "end" + counter_prec_of_min_level + "while" + artefactnb + ": ;\n" # ; necessary to put a statement after this label
                print "Creating artefact " + counter_prec_of_min_level + "NBEND" +  artefactnb
                artefacts.write("#define " + counter_prec_of_min_level + "NBEND" +  artefactnb + " " \
                                + str(counter_state[counter_prec_of_min_level + "while"]) + "\n");
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + check_while(counter_prec_of_min_level, counter_prec_of_min_level + "_b")
            
            if counter_derivation[counter_prec_of_min_level] == "else":
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + check_if_else(counter_prec_of_min_level, counter_state, counter_derivation, counter_prec_of_min_level + "_b")
     
            if counter_derivation[counter_prec_of_min_level] == "then":
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + check_if(counter_prec_of_min_level, counter_state, counter_derivation, counter_prec_of_min_level + "_b")
         
            # Dropping counters that are not declared anymore
            dropping_counter(counter_state, info.level)
            
        # The 2 lines has same level
        # Current = LVL 0 -------> Next = LVL 0
        # =====================================
        if infoNextLine is not None and info.level == infoNextLine.level:
     
            # Normal rule:
            if info_preceeding is None or \
                not info_preceeding.level < info.level and not infoHasGeneratedPostCHECK(info_preceeding) : #(if not entering a block and it was a funccall before)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
            

        # The next line enters a block
        # Current = LVL 0 -------> Next = LVL 1 
        # =====================================
        # BLOCK IF / BLOCK FOR
        if infoNextLine is not None and infoNextLine.level > info.level:
            # Stacking new name counter
            counter_stack[infoNextLine.level] = counter_name_next
            if info.isIf:
                print "Creating counter for an IF " + counter_name_next
                counter_derivation[counter_name_next] = "then"
                if not infoHasGeneratedPostCHECK(info_preceeding):
                    pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + decl_init(counter_name_next + "then", counter_state, 0)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + decl_init(counter_name_next + "else", counter_state, 0)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                # Extract condition of if to make b = (cond)
                ifline = re.split('^[\t\s]*if\s*\((.*)\)(.*)$', line)
                if len(ifline) != 4:
                    print "Error parsing if line:" + str(ifline)
                    sys.exit()
                pre_line = pre_line + decl_init(counter_name_next + "_b", counter_state, 1)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                # overwritting if (cond) by if (b = cond)
                #line = "if (" + counter_name_next + "_b = (" + ifline[1] + "))" + ifline[3]
                # Patch KH: overwritting if (cond) by if (CHECK_INCR_COND(b, cnt, 13, cond)
                line = "if (" + check_incr_cond_if(counter_name_next + "_b", counter_name_current, ifline[1], counter_state, counter_derivation) + ")"
            if info.isFor:
                print "Creating counter for a For " + counter_name_next
                counter_derivation[counter_name_next] = "for"
                if not infoHasGeneratedPostCHECK(info_preceeding):
                    pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + decl_init(counter_name_next + "for", counter_state, 0)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                # Extract condition of while/for to make b = (cond)
                forline = re.split('^[\t\s]*for\s*\((.*);(.*);(.*)\)(.*)$', line)
                if len(forline) != 6:
                    print "Error parsing for line:" + str(forline)
                    sys.exit()
                pre_line = pre_line + decl_init(counter_name_next + "_b", counter_state, 1)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                # storing a number (incremented) associated to this constant
                # [identification number, incrementation instruction of the for]
                artefact_numbering[counter_name_next] = [artefact_counter, forline[3]]
                # overwritting the for line:
                line = forline[1] + ";\n" + countermeasure(counter_name_current, counter_state, counter_derivation) + \
                    "begin" + counter_name_next + "for" + str(artefact_counter) + ":\n" + \
                    tabulation + counter_name_next + "for" + " = !(" + counter_name_next + "for == 0 || " \
                    + counter_name_next + "for" + " == " + counter_name_next + "NBEND" + str(artefact_counter) + \
                    ") ? killcard() : 0;\n" + \
                    "if (!" + check_incr_cond(counter_name_next + "_b", counter_name_next + "for", forline[2], counter_state) + \
                    ") goto end" + counter_name_next + "for" + str(artefact_counter) +";\n"
                #   "if (!(" + counter_name_next + "_b = (" + forline[2] + "))) goto end" + counter_name_next + "for" + str(artefact_counter) +";\n"
                # To eliminate a an attack that jump from after a for just inside the for
                post_line = post_line + check_loop(counter_name_next, counter_state, counter_derivation, counter_name_next + "_b")
                artefact_counter = artefact_counter + 1
            if info.isWhile:
                print "Creating counter for a While " + counter_name_next
                counter_derivation[counter_name_next] = "while"
                if not infoHasGeneratedPostCHECK(info_preceeding):
                    pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                pre_line = pre_line + decl_init(counter_name_next + "while", counter_state, 0)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                # Extract condition of while/for to make b = (cond)
                whileline = re.split('^[\t\s]*while\s*\((.*)\)(.*)$', line)
                if len(whileline) != 4:
                    print "Error parsing for line:" + str(whileline)
                    sys.exit()
                pre_line = pre_line + decl_init(counter_name_next + "_b", counter_state, 1)
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
                # storing a number (incremented) associated to this constant
                # [identification number, incrementation instruction of the while]
                artefact_numbering[counter_name_next] = [artefact_counter, ';']
                # overwritting the for line:
                line =  "begin" + counter_name_next + "while" + str(artefact_counter) + ":\n" + \
                    tabulation + counter_name_next + "while" + " = !(" + counter_name_next + "while == 0 || " \
                    + counter_name_next + "while" + " == " + counter_name_next + "NBEND" + str(artefact_counter) + \
                    ") ? killcard() : 0;\n" + \
                    "if (!" + check_incr_cond(counter_name_next + "_b", counter_name_next + "while", whileline[1], counter_state) + \
                    ") goto end" + counter_name_next + "while" + str(artefact_counter) +";\n"
                # To eliminate a an attack that jump from after a for just inside the for
                post_line = post_line + check_loop(counter_name_next, counter_state, counter_derivation, counter_name_next + "_b")
                artefact_counter = artefact_counter + 1


            
        # The next line exists a block (case 1)
        # Current = LVL 1 -------> Next = LVL 0
        # =====================================
        # BLOCK IF / FOR / AUTRE ?
        if infoNextLine is not None and infoNextLine.level < info.level and not info_preceeding.isFuncCall  and not info_preceeding.isWhile and not info_preceeding.isFor:#(if not entering a block and it was a funccall before)
            pre_line = pre_line +  countermeasure(counter_name_current, counter_state, counter_derivation)
        
        # FuncCall
        # ========
        if info.isFuncCall:
            #find_funccalls = re.split(r'^([\s\t]*)(\w+)\([\w\s&",->:\\\[%\]]+\);', line)
            
            #pre_line = pre_line + "/* *****************************************************" + str(v.fonctions_calls[str(number)]) + "*/"
            #temp_countermeasure = countermeasure(counter_name_current, counter_state, counter_derivation)
            pre_line_funccalls, new_line, post_line_funccalls = decl_init_funccalls(line, line, v.fonctions_calls[str(number)], \
                                        counter_state, all_functions_nbid_nbcalls, all_functions, counter_name_current,  counter_derivation)
            pre_line = pre_line + pre_line_funccalls
            #pre_line = pre_line + temp_countermeasure # This must be inserted (but computed before) the funccalls countermeasures
            if line == new_line: # This means that this function has not been patched (not mine !)
                info.isNotPatchedFuncCall = True
            line = new_line
            post_line = post_line + post_line_funccalls

        # The next line exists a block (case 2)
        # Current = LVL 1 -------> Next = LVL 0
        # =====================================
        # BLOCK IF / FOR / AUTRE ?
        if infoNextLine is not None and infoNextLine.level < info.level and not infoHasGeneratedPostCHECK(info):
            post_line = post_line + countermeasure(counter_name_current, counter_state, counter_derivation)
        
        # The 2 lines has same level: checking if we pass from a THEN to an ELSE
        # Current = LVL 0 -------> Next = LVL 0
        # =====================================
        if infoNextLine is not None and info.level == infoNextLine.level:
            # Add a check when passing from a then branch to an else branch
            if infoNextLine.isFirstElse:
                post_line = post_line + countermeasure(counter_name_current, counter_state, counter_derivation)


            
        # Last statements
        # ==================================================================
        # of the file: should add a countermeasure (probably)
        if infoNextLine is None:
            pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation)
        # of the function
        if  infoNextLine is None or infoNextLine.function_name != info.function_name:
            if info.function_name == "main": # Adding a CM with a check for the last line of main
                pre_line = pre_line + countermeasure(counter_name_current, counter_state, counter_derivation, False)
            if "return" not in line: # Adding a CM without CM for the last line of a function without return
                post_line = post_line + countermeasure(counter_name_current, counter_state, counter_derivation)
            print "Creating artefact " + counter_name_current + "NBENDFUNC" + " with value: " + str(counter_state[counter_name_current])
            artefacts.write("#define " + counter_name_current[1:] + "NBENDFUNC" + " " \
                                + str(counter_state[counter_name_current]) + "\n");
            

        if current_function_name != "gf_log":
            new_line = stack_line + pre_line + line + post_line
        else:
            new_line = line
        stack_line = ""
        target_file.write(new_line)
        number_preceeding = number # remembering this line as the last processed
    else:
        # Writes the original line
        # ========================
        target_file.write(line)
        # ========================

                  
                  
original_file.close()
artefacts.close()
target_file.close()

print "The code contains " + str(v.nb_function) + " functions."
print "Finished."
