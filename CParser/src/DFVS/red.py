# -*- coding:Utf8 -*-
#
# Colors the cycles in graph calls
#
pile = []
original_file = open("out/flot_huffmann.cycles", 'r')
for line in original_file:
    line = line.partition('\n')[0]
    pile.append(line)
    
dotfile = open("out/flot_huffmann.dot", 'r')
dotfile2 = open("out/flot_huffmann_2.dot", 'w')
nb = 0
for line in dotfile:
    linedebut = line.partition('\n')[0]
    trouve = False
    left = linedebut.partition(' ')[0]
    for item in pile:
        #print "left" + left
        left2 = left.split("\t")
        #print left2
        
        if line.find("shape=box") != -1:
            #print "line=" + line
            print "item=" + item    
            print "left2=" + str(left2)
        if len(left2) == 1:
            #print left2[1] + " == " + item
            if item == left2[0] and line.find("label=") != -1:
                trouve = True
                print "Finded !"
                nb += 1
        
    if trouve:
        dotfile2.write(left + " [shape=box,style=filled,color=blue]" + "\n")
    else:
        dotfile2.write(line)
        
print str(nb) + " nodes colored !"
            
        
    