import sys
import re

old_source = ""
old_dest = ""
old_type = ""
line = ""
order = [7,1,6,4,3,5,2]
for line in open(sys.argv[1]):
    split = re.split("([0-9]+) ([0-9]+) ([0-9])", line)
    source = split[1]
    dest = split[2]
    type = split[3]
    if (old_source != source or old_dest != dest) and old_type != "":
        print  old_source + " " + old_dest + " " + old_type
        old_type = ""
    

    
    if old_type != "":
        idx_old_type = order.index(int(old_type))
        idx_type = order.index(int(type))
        if idx_old_type > idx_type:
            #print "old: " + old_source + " " + old_dest + " " + old_type
            #print "line: " + line
            #print "Errasing type " + old_type + " with type " + type
            old_type = type
            
    else:
        old_type = type

    old_source = source
    old_dest = dest
    line = ""

if old_type != "":
    print  old_source + " " + old_dest + " " + old_type
