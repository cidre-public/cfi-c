import os
import re

def perc(nb, total):
    nbf = float(nb)
    ratio = nbf / total * 100
    if ratio == 0:
        return ""
    if ratio < 10:
        #return "(" + ('%.1f' % ratio) + "\\%" + ")"
        return ('%.1f' % ratio) + "\\,\\%" 
    else:
        #return "(" + str(int(ratio)) + "\\%" + ")"
        return str(int(ratio)) + "\\,\\%" 

bad=0
bad1=0
good=0
killcard=0
error=0
killed=0
nott=0
rep = os.listdir("./")
for each_file in rep:
    if re.match("(.*)\.data$", each_file):
        for line in open(each_file):
            linesplit = re.split("^([0-9]+) ([0-9]+) ([0-9])$", line)
            classif = int(linesplit[3])
            if classif == 1:
                bad = bad + 1
            elif classif == 2:
                good = good + 1
            elif classif == 3:
                killed = killed + 1
            elif classif == 4:
                error = error + 1
            elif classif == 5:
                killcard = killcard + 1
            elif classif == 6:
                bad1 = bad1 + 1
            elif classif == 7:
                nott = nott + 1
            else:
                print "Error !!!"
            
total = bad+bad1+killcard+good+error+killed+nott
     
print "BAD jump size>1: " + str(bad)
print "BAD jump size=1: " + str(bad1)
print "KILLCARD: " + str(killcard)
print "Good: " + str(good)
print "Error: " + str(error)
print "killed: " + str(killed)
print "Not triggered: " + str(nott)
print "Total (without nott): " + str(total)

print "HACK for ESORICS:"
print "good = good + nott"
print "error = error + killed"
good = good + nott
error = error + killed

print "LATEX: \n"\
 + str(bad) + " & " \
 + str(perc(bad,total)) + " & " \
 + str(bad1) + " & "\
 + str(perc(bad1,total)) + " & " \
 + str(good) + " & "\
 + str(perc(good,total)) + " & " \
 + str(killcard) + " & "\
 + str(perc(killcard,total)) + " & " \
 + str(error) + " & "\
 + str(perc(error,total)) + " & " ,
# + str(nott) + " & "\
# + str(perc(nott,total)) + " & " \
# + str(killed) + " & "\
# + str(perc(killed    ,total)) + " & " \

print str(total) + " \\\\"
