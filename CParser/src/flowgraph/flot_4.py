# -*- coding:Utf8 -*-
################### LIBRAIRIES ###################################
#import commands
import argparse
import sys
from pycparser import c_parser, c_ast

CPPPATH = '../utils/cpp.exe' if sys.platform == 'win32' else 'cpp'
from pycparser import parse_file

from os import system
import os
from ordereddict import OrderedDict

################### VARIABLES GLOBALES ###########################

global line_number
line_number = 0

global nb
nb = 0

global pile
pile = []

global def_func
def_func = OrderedDict([])     # dictionnaire contenant les définitions de fonctions et ayant pour clé leur numéro de ligne

global df
df = OrderedDict([])    # dictionnaire contenant les appels de fonctions et le nombre def_func'occurrence

global numerotation_func_statements
numerotation_func_statements = OrderedDict([])   # dictionnaire contenant les boucles et les conditions et le nombre def_func'occurrence

global dico
dico = OrderedDict([])  # dictionnaire contenant les boucles et les fonctions et leur coordonnées

global coordonnees
coordonnees = OrderedDict([])        # dictionnaire contenant les coordonnées

##################################################################


############################ TABLEAUX ############################
### COORDONNEES ###
def coordtab(no, num):
    num.append(no)
### COORDONNEES END ###
##################################################################


### OCCURRENCE ###
##################################################################
def write() :
    boucle = ["-> if", "-> \"if", "-> while", "-> \"while", "-> for", "-> \"for", "-> dowhile", "-> \"dowhile", "-> switch", "-> \"switch", "-> ternaryop", "-> \"ternaryop", "-> else", "-> \"else"]
    def_func.clear()

    for i in range(len(dico)):
        g = dico.items()[i]
        g1 = g[1].split('#')
        g2 = g1[0].split('--')
        taille = len(g2)
        for j in range(len(g2)) :
            h = (str(g2[j]).split('"'))
            if len(h) != 1 :
                h = h[1]
            else :
                h = h[0]
            g2[j] = h
        wd = g2[0]
        j = 1
        while taille != 1 and j < len(g2):
            wd = '\"' + g2[j-1] + '\" -> \"'
            wd = wd + g2[j]
            wd = wd + '\"'
            check = str(g2[j]).split(' (')
            check = str(check[0]).split('"')
            if (calllines == 0) :
                if (wd in def_func):
                    nbr = int(def_func[wd]) + 1
                    def_func[wd] = str(nbr)
                else :
                    def_func[wd] = "1"
            # HACK JFL: J'écrase le décompte du nombre de lien avec le numéro de ligne de l'appel
            elif (calllines == 1):
                if (wd not in def_func) :
                    def_func[wd] = int(g1[1])
                else :
                    def_func[wd] = str(def_func[wd]) + " " + str(g1[1])

            taille = taille - 1
            j = j + 1

    for i in range(len(def_func)) :
        for j in range(len(boucle)) :
            d2 = def_func.items()[i]
            key = d2[0]
            value = d2[1]
            
            if boucle[j] in key :
                if (calllines == 0) :
                    def_func[key] = "1"
            # HACK JFL: J'écrase le décompte du nombre de lien avec le numéro de ligne de l'appel
                else :
                    ligne = str(value).split(' ')
                    def_func[key] = str(ligne[0])

    form = True
    for i in range(len(numerotation_func_statements)) :
        d2 = numerotation_func_statements.items()[i]
        key = d2[0]
        for j in range(len(boucle)) :
            if key in boucle[j] :
                form = False
        if form == True :
            f1.write ('\n\t')
            f1.write (key)
            if "FuncPointerCall" in key :
                f1.write(' [shape=polygon, sides=5]')
            else :
                f1.write(' [shape=box]')
        form = True
    
    for i in range(len(def_func)) :
        d2 = def_func.items()[i]
        key = d2[0]
        value = d2[1]
        f1.write('\n\t')
        f1.write(key)
        f2.write(key)
        for j in range(len(boucle)) :
            if (boucle[j] in key and color != 0) :
                f1.write(' [color=blue]')
                form = False
        if calllines == True :
            f1.write(' [label=' + str(value) + ']')
        elif int(value) > 1 :
            if color == 0 :
                f1.write(' [label=' + str(value) + ']')
            elif form == False :
                f1.write(' [label=' + str(value) + ', color=blue, fontcolor=blue]')
            else :
                f1.write(' [label=' + str(value) + ', color=red, fontcolor=red]')
            f2.write('\t' + str(value))
        f2.write('\n')
        form = True


##################################################################
### OCCURRENCE END ###


### FUNCDECL ###
# FuncDecl : détecte les déclarations de fonction.
### FUNCDECL ###
class FuncDeclVisitor(c_ast.NodeVisitor) :
############## Fonction pour obtenir les coordonnées #############
    def ob_coord(self, node) :
        coord = (str(node.coord)).split(':')
        if sys.platform == 'win32' :
            return int(coord[2])
        elif len(coord) == 2 :
            return int(coord[1])
        else :
            return 1
            

####################### La methode FuncDecl ######################
    def FuncDeclTreatment(self, node) :

# Ecriture dans le dictionnaire
        coord = self.ob_coord (node.decl)
        def_func[node.decl.name] = coord
        coordtab (coord, num)
        self.generic_visit(node)

################## La methode visiteur generique #################
    def generic_visit(self, node):
        global line_number

        for c in node.children():
            # HACK JFLPB
            # Solution du pauvre, ca plante donc on ignore...
            # mais il faudrait comprendre pourquoi ca plante et réparer...
            if c == None: # HACK JFLPB: est-ce pertinent ???
                break; # HACK JFLPB: est-ce pertinent ???
            co = self.ob_coord(c)

            if co == 0 :
                coord = line_number
            else :
                coord = co

            if int(coord) < line_number :
                pass
            elif isinstance(c,c_ast.FuncCall) :
                ## DEBUG
                ## print ("FuncCall :"),
                ## print (c.coord),

                # print (c.name.name)
                
                if isinstance(c.name,c_ast.StructRef):
                    #print "WARNING : pointeur de fonction : Ligne : " + str(c.name.coord) + ', pour la fonction : ' + str(c.name.field.name) + "." # c.name.field.name = le nom de la fonction pointée.
                    ind = "FuncPointerCall from " + c.name.field.name + "#" + str(coord)
                    print "Writing " + ind
                elif isinstance(c.name, c_ast.ID) :
                    ind = c.name.name + "#" + str(coord)
                else :
                    ind = str(c.name.expr.name) + "#" + str(coord)
                    print "Writing " + ind
                if nb != 0 and str(ind) in dico[nb-1]:
                    pass
                else :
                    self.FuncCallTreatment(c)

            # JFL
            # Soit on demande a traiter les if,for,while
            # soit non, on veut juste le graphe des fonctions.
            # Si on veut les blocks internes if, for, while
            # il faut traiter les autres types de noeuds avant
            # de faire le parcourt récursif
            if without_blocks == 0:
                if int(coord) < line_number :
                    pass
                elif isinstance(c,c_ast.If) :
                    ## DEBUG
                    ## print ("If :"),
                    ## print (c.coord)
                    self.Treatment(c.cond, "if", c)
                    self.Treatment(c.iftrue, "if", c)
                    if (c.iffalse != None):
                        ## DEBUG
                        ## print "Else :",
                        ## print c.iffalse.coord
                        self.ElseTreatment(c.iffalse, "else", c.iffalse)
                elif isinstance(c,c_ast.While) :
                    ## DEBUG
                    ## print ("While :"),
                    ## print (c.coord)
                    self.Treatment(c, "while", c)
                elif isinstance(c,c_ast.For) :
                    ## DEBUG
                    ## print ("For :"),
                    ## print (c.coord)
                    self.Treatment(c, "for", c)
                elif isinstance(c,c_ast.DoWhile) :
                    ## DEBUG
                    ## print ("DoWhile :"),
                    ## print (c.coord)
                    self.Treatment(c, "dowhile", c)
                elif isinstance(c,c_ast.Switch) :
                    ## DEBUG
                    ## print ("Switch :"),
                    ## print (c.coord)
                    self.Treatment(c, "switch", c)
                elif isinstance(c,c_ast.TernaryOp) :
                    ## DEBUG
                    ## print ("TernaryOp :"),
                    ## print (c.coord)
                    self.Treatment(c, "ternaryop", c)

            self.visit(c)


    def visit(self, node):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)


####################### La methode FuncCall ######################
    def FuncCallTreatment(self, node) :
        global nb
        global line_number

        line_number = self.ob_coord (node)
        if isinstance(node.name,c_ast.StructRef) :
            ind = "\"FuncPointerCall from " + node.name.field.name + "\""# + "#" + str(line_number)
            #pile.append(node.name.field.name)
            pile.append(ind)
        elif isinstance(node.name, c_ast.ID) :
            pile.append (node.name.name)
        else :
            ind = "\"FuncPointerCall from " + str(node.name.expr.name) + "\""
            pile.append (ind)
        taille = len(pile)
        word = pile[0]
        for i in range (len(pile)) :
            if i > 0 and taille > 1 :
                word = word + '--' + str(pile[i]) 
                taille = taille -1

        lg = '--' + str(pile[len(pile)-1]) + '#' + str(line_number)
        if str(lg) in str(dico.values()) :
            pass
        else :
            coordtab (line_number, num)
            dico[nb] = word + '#' + str(line_number)
            nb = nb + 1
            if word in df :
                value = df[word]
                value = int(value) + 1
                df[word] = value
            else :
                df[word] = "1"
            h1 = word.split('--')
            h = str(h1[len(h1)-1])
            
            if h in numerotation_func_statements :
                f = numerotation_func_statements[h]
                f = int(f) + 1
                numerotation_func_statements[h] = f
            else :
                numerotation_func_statements[h] = "1"
        pile.pop()


########## La methode pour les boucles et les conditions #########
    def Treatment(self, node, noun, noeud_courant) :
        global line_number
        global nb
        
        line_number = self.ob_coord (noeud_courant)
        ajout = 0
        ## Ce statement a-t-il déjà été répertorié dans coordonnees ?
        if (noun == "if"):
            if noun in coordonnees :
                value = coordonnees[noun]
                v = (str(value)).split('.')
                if int(v[len(v)-1]) != line_number :
                    ajout = 1
            else :
                ajout = 1
                
        ## On doit ajouter ce if/while/switch dans 
        ## - le dictionnaire des coordonnees
        ## - le dictionnaire de compteur de numerotation
        if (ajout == 1 or (noun != "if")):
            if noun in numerotation_func_statements : 
                f = numerotation_func_statements[noun]
                f = int(f) + 1
                numerotation_func_statements[noun] = f
                val = str(coordonnees[noun]) + '.' + str(line_number)
                coordonnees[noun] = val
            else : 
                ## C'est une fonction ou la premiere fois qu'on voit ce statement
                ## => on le met avec une numérotation à 1 (+ son numéro de ligne)
                numerotation_func_statements[noun] = "1"
                coordonnees[noun] = line_number
        
        ##if numerotation_func_statements[noun] == '-1' :
        ##    mot = "\"" + noun + "\""
        ##else :
        mot = "\"" + noun + " (" + str(numerotation_func_statements[noun]) + ")\""
        pile.append(mot)
        
        word = pile[0]
        taille = len(pile)
        for i in range (len(pile)) :
            if i > 0 and taille > 1 :
                word = word + '--' + pile[i]
                taille = taille -1
        if word in df :
            pass
        else :
            df[word] = "1"
        
        lg = '--' + str(pile[len(pile)-1]) + '#' + str(line_number)
        if str(lg) in str(dico.values()) :
            pass
        else :
            coordtab (line_number, num)
            dico[nb] = word + '#' + str(line_number)
            nb = nb + 1
        
        self.visit(node)
        pile.pop()


#################### La methode pour les else ####################
    def ElseTreatment(self, node, noun, ncoord) :
        global line_number
        global nb

        h = noun
        coord = self.ob_coord (ncoord)
        #f = numerotation_func_statements["if"]
        if h in numerotation_func_statements :
            val = str(coordonnees[h]) + '.' + str(coord)
            coordonnees[h] = val
            g = numerotation_func_statements[h]
            numerotation_func_statements[h] = int(g) + 1
        else :
            numerotation_func_statements[h] = "1"
            coordonnees[h] = coord

        ##if f == '1' :
        ##    mot = "\"" + noun + "\""
        ##else :
        mot = "\"" + noun + " (" + str(numerotation_func_statements["if"]) + ")\""
        pile.append(mot)

        word = pile[0]
        taille = len(pile)
        for i in range (len(pile)) :
            if i > 0 and taille > 1 :
                word = word + '--' + pile[i]
                taille = taille -1
        if word in df :
            pass
        else :
            df[word] = "1"
        
        lg = '--' + str(pile[len(pile)-1]) + '#' + str(line_number)
        if str(lg) in str(dico.values()) :
            pass
        else :
            coordtab (line_number, num)
            dico[nb] = word + '#' + str(line_number)
            nb = nb + 1

        if 'If' in str(node) :
            numerotation_func_statements["if"] = int(numerotation_func_statements["if"]) + 1
            l = self.ob_coord (node.cond)
            val = coordonnees["if"] + '.' + str(l)
            coordonnees["if"] = val
            mot = "\"" + "if" + " (" + str(numerotation_func_statements["if"]) + ")\""
            pile.append(mot)
            
            word = pile[0]
            taille = len(pile)
            for i in range (len(pile)) :
                if i > 0 and taille > 1 :
                    word = word + '--' + pile[i]
                    taille = taille -1
            if word in df :
                pass
            else :
                df[word] = "1"
        
            lg = '--' + str(pile[len(pile)-1]) + '#' + str(line_number)
            if str(lg) in str(dico.values()) :
                pass
            else :
                coordtab (line_number, num)
                dico[nb] = word + '#' + str(line_number)
                nb = nb + 1
            
            ## print "\tIf :",
            ## print node.coord
            self.visit(node.cond)
            self.visit(node.iftrue)
            pile.pop()
            if 'Compound' in str(node.iffalse) :
                ## DEBUG
                ## print "\tElse :",
                ## print node.iffalse.coord
                self.ElseTreatment(node.iffalse, "else", node.iffalse)
                pile.pop()
        elif 'Compound' in str(node) :
            self.visit(node)
            pile.pop()


################### La methode visiteur FuncDef ##################
    def visit_FuncDef(self, node) :
        ## print ("FUNCDEF DETECTE : " + node.decl.name + ".")
#        if node.decl.coord != None :
#            print ("Line : " + str(node.decl.coord) + ".")
#        else :
#            print ("")
        pile.append(node.decl.name)
        self.FuncDeclTreatment(node)
        
        while (len(pile) != 0) :
            pile.pop()


##################################################################
# FUNCDEF END #
# FUNCDEF END #


############################## MAIN ##############################

num = [0]

parser = argparse.ArgumentParser(description='Generates the flow graph of a C file.')
parser.add_argument('--filename', metavar='filename',
                   help='the filename of the C file to parse')
parser.add_argument('--without-blocks',  action='store_true',
                   default=0,
                   help='disable blocks (if, while, for...)')
parser.add_argument('--color', action="store_true", default=0,
                   help='enable color in graph')
parser.add_argument('--calllines', action="store_true", default=0,
                   help='labels arc with the line call number')


args = parser.parse_args()
print args
filename=args.filename
without_blocks=args.without_blocks
color = args.color
calllines = args.calllines

ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH, cpp_args=r'-I./fake_libc_include')

print ('\n######################')
print ('## Generating graph ##')
print ('######################\n')

v = FuncDeclVisitor()
v.visit(ast)

path, filename_only = os.path.split(filename)
dot = './out/flot_' + filename_only + '.dot'
png = './out/flot_' + filename_only + '.png'
txt = './out/flot_' + filename_only + '.txt'


print "Writing " + txt
f2 = open(txt,'w')
f2.write ('// fct1 -> fct2 nbre_appel ou\n// fct1 -> cond ou\n// cond -> fct3 nbre_appel (nbre_appel affiche si superieur a 1).\n\n')


print "Writing " + dot
f1 = open(dot, 'w')
f1.write('digraph G {')
f1.write('\n\trankdir = LR;')
write() # Garder f2 ouvert pour faire ceci
f1.write('\n}')
f1.close()
f2.close()

print "Writing " + png 
command = "dot -Tpng -o " + png + " " + dot
system (command)

del def_func
del df
del numerotation_func_statements
del dico
del coordonnees

print "Finished."

##################################################################
