# -*- coding:Utf8 -*-

#
# Detects the end of functions and returns the line number
#
import argparse
import sys
from pycparser import c_parser, c_ast

CPPPATH = '../utils/cpp.exe' if sys.platform == 'win32' else 'cpp'
from pycparser import parse_file

from os import system
import os


############## Fonction pour obtenir les coordonnées #############
def ob_coord(function_coord) :
    
    coord = (str(function_coord)).split(':')
    
    if len(coord) > 2 :
        chemin = ''
        for i in range(len(coord)-2) :  # construction d'une liste à renvoyer pour n'avoir que 2 éléments dans la liste
            chemin += str(coord[i]) + ':'
        chemin += str(coord[len(coord)-2])
        list_coord = [chemin, coord[len(coord)-1]]
        return list_coord
    
    elif len(coord) == 2 :
        return coord

    else :
        print "ERROR: this object has no coord object !"
        print node


############## Fonction ecrivant dans le fichier txt #############
def write(function_name, function_coord, debut) :
    
    if function_coord == None : # si coord = None
        print "WARNING : ",
        print str(function_name) + ' has no coord object'
        
        if debut == True : # si c'est la ligne de début
            f1.write (' ')
            f1.write (function_name)
            f1.write (' ')
        else :  #si c'est la ligne de fin
            f1.write ('\n')

    else :
        coord = ob_coord(function_coord) # appel pour récupérer les coordonnées
        if debut == True :
            f1.write (coord[0])
            f1.write (' ')
            f1.write (function_name)
            f1.write (' ')
            f1.write (coord[1])

        else :
            f1.write (' ')
            f1.write (coord[1])
            f1.write ('\n')


### FUNCDECLVISITOR ###
##################################################################
class FuncDeclVisitor(c_ast.NodeVisitor) :

######################## Visiteur générique ######################
    def generic_visit(self, node, function_name):
        
        if len(node.children()) == 0 : # si le noeud n'a pas de fils
            if isinstance(node, c_ast.Break) :
                write (function_name, node.coord, False)
            else :
                print "WARNING : le noeud n'a pas de fils",
                print node.coord

        else :
            c = node.children()[len(node.children())-1] # on prend le dernier noeud

            if isinstance(c, c_ast.If) : # si c est un noeud de type if, on analyse si il a un else ou juste un then
                if c.iffalse != None :
                    self.generic_visit(c.iffalse, function_name)
                else :
                    self.generic_visit(c.iftrue, function_name)

            elif ( isinstance(c, c_ast.For) or isinstance(c, c_ast.While) or
                   isinstance(c, c_ast.Switch) or isinstance(c, c_ast.TernaryOp) ):
                self.generic_visit(c.stmt, function_name)
            
            elif isinstance(c, c_ast.Default) :
                self.generic_visit(c.stmt, function_name)

            elif isinstance(c, c_ast.DoWhile) :
                self.generic_visit(c.cond, function_name)
                
            elif isinstance(c, c_ast.Label) : # s'il y a des label dans la fonction
                self.generic_visit(c.stmt, function_name)

            elif isinstance(c, c_ast.Compound) :
                self.generic_visit(c, function_name)

            elif c == None : # si c est un noeud None, on analyse le noeud précédent
                c = node.children()[len(node.children())-2]
                self.generic_visit(c, function_name)

            elif isinstance(c, c_ast.Cast) :  # si c est un cast, on analyse l'expression "casté" 
                self.generic_visit(c.expr, function_name)

            else : # sinon c n'a pas de fils (dernière ligne de la fonction)
                if c.coord == None :
                    if isinstance(c, c_ast.ExprList) :
                        write (function_name, c.exprs[len(c.exprs)-1].coord, False)
                    else :
                        self.generic_visit(c, function_name)
                else : # écriture dans le fichier
                    write (function_name, c.coord, False)


####################### Visiteur func_visit ######################
    def func_visit(self, node) :    # Traitement des définitions de fonctions

        for c in node.children() :
            if isinstance(c, c_ast.FuncDef) :
                self.visit_FuncDef(c)


######################## Visiteur FuncDef ########################
    def visit_FuncDef(self, node) :

        # On détecte le début de la fonction et on le met dans un dictionnaire
        if node.decl.coord != None :
            write(node.decl.name, node.decl.coord, True) # True signifie que l'on est au début de la fonction
        else :
            print "WARNING : the function " + str(node.decl.name) + "doesn't have a number of line"

        # On parcours le corps de chaque fonction à l'aide du visiteur generic_visit()
        for c in node.children() :
            if isinstance(c, c_ast.Compound) :
                if len(node.body.children()) == 0 : # si la fonction est vide
                    write (node.decl.name, node.decl.coord, False)
                    print "WARNING : la fonction " + str(node.decl.name),
                    print "est vide. ligne : " + str(node.decl.coord)
                else :
                    self.generic_visit(node.body, node.decl.name)
                

##################################################################
### END FUNCDECLVISITOR ###


############################## MAIN ##############################

parser = argparse.ArgumentParser(description='Gives a file with the number of line of each function of a C file.')
parser.add_argument('--filename', metavar='filename',
                   help='the filename of the C file to parse')

args = parser.parse_args()
print args
filename=args.filename

print ('\n##############################')
print ('## DEBUT ET FIN DE FONCTION ##')
print ('##############################\n')

path, filename_only = os.path.split(filename)
txt = './../out/fin_' + filename_only + '.txt'

ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH, cpp_args=r'-I./../fake_libc_include')

print "Writing " + txt
f1 = open(txt,'w')
f1.write ('#source_file function_name beginning_function end_function\n\n')

v = FuncDeclVisitor()
v.func_visit(ast)

f1.close()

print "Finished."

##################################################################