# -*- coding:Utf8 -*-
################### LIBRAIRIES ###################################
#
# Statitical counts about a .c file
#
#
import argparse
import sys
from pycparser import c_parser, c_ast

CPPPATH = '../fake_libc_include/cpp.exe' if sys.platform == 'win32' else 'cpp'
from pycparser import parse_file

from os import system
import os
from ordereddict import OrderedDict

global found_goto
found_goto = False

global found_label
found_label = False

global pile
pile = []

global list_of_variables
list_of_variables = []

global list_of_iterators
list_of_iterators = []


### FUNCDECLVISITOR ###
##################################################################
class FuncDeclVisitor(c_ast.NodeVisitor) :

########################## Verbose Mode ##########################
    def verbose_function(self, name, node, RW) :

        # RW = 0, 1, 2 or 3
        # 0 : if is read,
        # 1 : if is written,
        # 2 : the both,
        # 3 : we don't want to print if it's a read variable, ...   

        if verbose == True :
            if RW == 2 :
                print "Read and written variable :",
            elif RW == 1 :
                print "Written variable :",
            elif RW == 0 :
                print "Read variable :",
            elif (RW == 3 and name != "goto" and name != "label") :
                print "FuncCall :",
            
            if (RW == 3 and name == "goto" and calllines == True) :
                print "source of analysis ",
            elif (RW == 3 and name == "label" and calllines == True) :
                print "destination of analysis ",
            else :          
                print name,
                
            print "at line : ",
            print self.ob_coord(node)
            
            if RW == 1 and found_label == True :
                print list_of_variables
            

    def ob_coord(self, node) :
        coord = (str(node.coord)).split(':')
        if sys.platform == 'win32' :
            return int(coord[2])
        elif len(coord) == 2 :
            return int(coord[1])
        else :
            return 1

##################################################################
    def goto_function (self, node, attack_src, attack_dest, function_name, list_cmp) :
        global found_goto
        global found_label
        
        if ( int(attack_src) < int(attack_dest) ) :
            found_goto = True
            found_label = False
            if verbose == True :
                print "\nName of analysed function : ",
                print function_name,
                print "\n"
                print "Forward jump\n"
            self.verbose_function ("goto", node, 3)
            list_cmp = self.analyse(pile, list_cmp)
        elif ( int(attack_src) > int(attack_dest) ) :
            found_goto = False
            found_label = False
            self.verbose_function ("goto", node, 3)
            print "\nList of variables : ",
            print list_of_variables
            
        elif ( int(attack_src) == int(attack_dest) ) :
            print "Warning the source and the destination are the same"
            found_goto = False
            found_label = False
        
        
        
    def label_function (self, node, attack_src, attack_dest, function_name, list_cmp) :
        global found_goto
        global found_label
        
        if ( int(attack_src) < int(attack_dest) ) :
            found_goto = False
            found_label = True
            print "\nList of variables : ",
            print list_of_variables,
            print "\n"
            self.verbose_function ("label", node, 3)
            
        elif ( int(attack_src) > int(attack_dest) ) :
            found_goto = True
            found_label = False
            if verbose == True :
                print "\nName of analysed function : ",
                print function_name,
                print "\n"
                print "Backward jump\n"
            self.verbose_function ("label", node, 3)
            list_cmp = self.analyse(pile, list_cmp)
            

########################## Generic Visitor #######################
    def generic_visit(self, node, function_name, liste):
        global found_goto
        global found_label

        if found_goto == True :
            # analysis if the node is found between the "goto" and the "label"
            liste = self.Goto_analyse(node, function_name, liste)
                
        elif found_label == True :
            # analysis after label
            liste = self.Label_analyse(node, liste)

        for c in node.children() :
            if c == None :
                pass
                
            elif calllines == True :
                if (self.ob_coord (c) == int (src)) :
                    self.goto_function (c, src, dest, function_name, liste)
                    if ( int(src) < int(dest) ) :
                        liste = self.Goto_analyse(c, function_name, liste)
                elif (self.ob_coord (c) == int (dest) and (found_goto == True or (found_goto == False and found_label == False))) :
                    self.label_function (c, src, dest, function_name, liste)
                    if ( int(src) < int(dest) ) :
                        liste = self.visit (c, function_name, liste)
                    elif ( int(src) > int(dest) ) :
                        liste = self.Goto_analyse(c, function_name, liste)
                elif (isinstance(c, c_ast.For) or isinstance(c, c_ast.While)
                   or isinstance(c, c_ast.DoWhile) or isinstance(c, c_ast.Switch)
                   or isinstance(c, c_ast.If)) :
                    if found_goto :
                        #print "for while do while switch"
                        liste = self.function_blocks(liste, "begin", c)
                    pile.append(c)
                    if not(isinstance(c, c_ast.For)) :
                        liste = self.visit(c, function_name, liste)
                    else :
                        liste = self.visit(c.stmt, function_name, liste)
                        if (found_goto and 
                            c.init.lvalue.name in list_of_variables) :
                        # list to have the name of iterators
                            list_of_iterators.append(c.init.lvalue.name)
                    pile.pop()
                    if found_goto :
                        liste = self.function_blocks(liste, "end", c)
                        #print "fin for while do while switch"
                else :
                    liste = self.visit(c, function_name, liste)

            
            elif isinstance(c, c_ast.Goto) :
                if c.name == "attack"+dest :
                    self.goto_function (c, src, dest, function_name, liste)
                    
            elif isinstance (c, c_ast.Label) :
                if c.name == "attack"+dest :
                    # we delete iterators in the list of written variables
                    for i in range(len(list_of_iterators)) :
                        it = list_of_iterators[i]
                        while it in list_of_variables :
                            list_of_variables.remove(it)
      
                    self.label_function (c, src, dest, function_name, liste)
                    liste = self.visit (c, function_name, liste)
                    
    # 2 possibilités : 
    # - c.stmt non vide mais obj Label est le dernier de node.children
    # - l'objet Label n'est pas le dernier de node.children => utilisation de found_label 

            elif (isinstance(c, c_ast.For) or isinstance(c, c_ast.While)
               or isinstance(c, c_ast.DoWhile) or isinstance(c, c_ast.Switch)
               or isinstance(c, c_ast.If)) :
                if found_goto :
                    #print "for while do while switch"
                    liste = self.function_blocks(liste, "begin", c)
                pile.append(c)
                if not(isinstance(c, c_ast.For)) :
                    liste = self.visit(c, function_name, liste)
                else :
                    liste = self.visit(c.stmt, function_name, liste)
                    if (found_goto and 
                        c.init.lvalue.name in list_of_variables) :
                    # list to have the name of iterators
                        list_of_iterators.append(c.init.lvalue.name)
                pile.pop()
                if found_goto :
                    liste = self.function_blocks(liste, "end", c)
                    #print "fin for while do while switch"
            else :
                liste = self.visit(c, function_name, liste)

        return liste


##################################################################
    def visit(self, node, function_name, liste):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node, function_name, liste)
        

####################### Visitor of Do While ######################
    def analyse (self, pile, list_cmp):
        # analysis if "goto" is in statement "do while"
        
        node = pile[len(pile)-1]
        
        if (isinstance(node, c_ast.DoWhile)) :
            list_cmp = self.generic_visit(node.cond, pile[0], list_cmp)

        return list_cmp 


######################## Minimum Function ########################
    def function_min (self, minimum, cmp):
        
        if minimum > cmp :
            minimum = cmp
        else :
            pass
        return minimum


################ Function of counter of statements ###############
    def function_blocks (self, list_cmp, place, node):
        # counter of the beginning and the end of statement
        
        if place == "begin" :
            list_cmp [2] += 1
            list_cmp [4] += 1
            if verbose :
                print "beginnig of block at line : " + str(self.ob_coord(node))
        elif place == "end" :
            list_cmp [2] -= 1
            list_cmp [3] = self.function_min(list_cmp [3], list_cmp[2])
            if list_cmp [4] > 0 :
                list_cmp [4] -= 1
                list_cmp [5] += 1
            if verbose :
                print "end of block at line"
                
        return list_cmp


########################## Goto Visitor ##########################
    def Goto_analyse(self, node, function_name, list_cmp):
        # counter of assignments and funccall
        
        if isinstance (node, c_ast.Assignment) :
            name = self.definition(node.lvalue)
            if (not(name in list_of_variables)) :
                if (not (isinstance (node.lvalue, c_ast.ArrayRef)) or
                    (isinstance (node.lvalue, c_ast.ArrayRef) and
                    not (isinstance (node.lvalue.subscript, c_ast.Constant)))) :
                    list_of_variables.append(name)
                    list_cmp[6] += 1
            if isinstance(node.lvalue, c_ast.ArrayRef) :
                name_tab = self.definition(node.lvalue.name)
                if not(name_tab in list_of_variables) :
                    list_of_variables.append(name_tab)
                if not (isinstance (node.lvalue.subscript, c_ast.Constant)) :
                    name = name_tab + " and " + name
                else :
                    name = name_tab
            self.verbose_function (name, node, 1)
            list_cmp[0] += 1

        elif isinstance (node, c_ast.UnaryOp) :
            # Filtre les opérations unary du type A++
            # Si l'on veut aussi les autres unary op
            # par exemple *(p), il suffit de decommenter ce test.
            if node.op == 'p++' or node.op == 'p--' :
                name = self.definition(node.expr)
                if (not(name in list_of_variables) and
                   not (isinstance (node.expr.subscript, c_ast.Constant))) :
                    list_of_variables.append(name)
                    list_cmp[6] += 1
                if isinstance(node.expr, c_ast.ArrayRef) :
                    name_tab = self.definition(node.expr.name)
                    if not(name_tab in list_of_variables) :
                        list_of_variables.append(name_tab)
                    name = name_tab + " and " + name
                self.verbose_function (name, node, 1)
                list_cmp = self.Goto_analyse(node.expr, function_name, list_cmp)
                list_cmp[0] +=1
        elif isinstance (node, c_ast.FuncCall) :
            name = self.definition(node)
            self.verbose_function (name, node, 3)
            list_cmp[1] +=1
         
        return list_cmp


##################### Definition of Variables ####################
    def definition (self, node):
        # this function must find the name of assignment
        # and puts the result in a list

        if isinstance (node, c_ast.Assignment) :
            name = self.definition (node.lvalue)
            return name
        elif isinstance (node, c_ast.UnaryOp) :
            name = self.definition(node.expr)
            return name
        elif isinstance (node, c_ast.StructRef) :
            name = str(self.definition(node.name)) + str(node.type)
            name += str(self.definition(node.field))
            return name
        elif isinstance (node, c_ast.ID) :
            return node.name
        elif isinstance (node, c_ast.Cast) :
            name = self.definition(node.expr)
            return name
        elif isinstance (node, c_ast.Constant) :
            return node.value
        elif isinstance (node, c_ast.BinaryOp) :
            name = str(self.definition(node.left)) + str(node.op)
            name += str(self.definition(node.right))
            return name
        elif isinstance (node, c_ast.ArrayRef) :
            name = str(self.definition(node.subscript))
            return name
        elif isinstance (node, c_ast.TernaryOp) :
            if str(node.cond.right) in list_of_variables :
                name = self.definition(node.cond.right)
            else :
                name = self.definition(node.cond.left)
            return name
        elif isinstance (node, c_ast.FuncCall) :
            name = self.definition(node.name)
            return name
        else :
            print "Warning ",
            print node,
            print node.coord
            

###################### Assignment's Elements #####################
    def nameLR (self, node, name) :

        # if left side or right side is an array
        # or an ternary operation
        if isinstance (node, c_ast.BinaryOp) :
            name_left = self.definition(node.left)
        elif isinstance (node, c_ast.Assignment) :
            name_left = self.definition(node.lvalue)
            if isinstance (node.lvalue, c_ast.ArrayRef) :
                name_left = self.definition(node.lvalue.subscript)
        elif isinstance (node.lvalue, c_ast.TernaryOp) :
            name_left = self.definition(node.lvalue)
        
        name.append (name_left)
        
        if isinstance (node, c_ast.BinaryOp) :
            name_right = self.definition(node.right)
        elif isinstance (node, c_ast.Assignment) :
            name_right = self.definition(node.rvalue)    
            if isinstance (node.rvalue, c_ast.ArrayRef) :
                name_right = self.definition(node.rvalue.subscript)
        elif isinstance (node.rvalue, c_ast.TernaryOp) :
            name_right = self.definition(node.rvalue)
            
        name.append (name_right)
        
        return name


######################### Read Variables #########################
    def read_variables (self, node, list_cmp, bool_assignment, name_assignment) :
    
        # this function analyse variables
        # bool_assignment : - False, if node is not an assignment
        #                   - True, if node is an assignment
        
        if isinstance (node, c_ast.BinaryOp) :
            name = []
            name = self.nameLR (node, name)
            name_left = name [0]
            name_right = name [1]
            
            list_cmp = self.Array_analyse (node, list_cmp, None)
            
            # if variable(s) is(are) in list of variables
            if (name_left in list_of_variables and 
                name_right in list_of_variables) :
                
                list_cmp [7] += 2
                if bool_assignment == False :
                    name2 = str(name_left) + " and " + str(name_right)
                    self.verbose_function (name2, node, 0)
                    if name_assignment != None :
                        list_of_variables.append (name_assignment)
                        self.verbose_function (name_assignment, node, 1)
                elif name_left == name_assignment :
                    self.verbose_function (name_right, node, 0)
                    self.verbose_function (name_left, node, 2)
                elif name_right == name_assignment :
                    self.verbose_function (name_left, node, 0)
                    self.verbose_function (name_right, node, 2)
                if not(isinstance (node.left, c_ast.ArrayRef) and 
                       isinstance (node.right, c_ast.ArrayRef)) :
                    if not (isinstance(node.right, c_ast.Constant)
                       or isinstance (node.right, c_ast.ID)) :
                        list_cmp [8] += 2
                    else :
                        list_cmp [8] += 1
                    
            elif name_left in list_of_variables :
                list_cmp [7] += 1
                if bool_assignment == False :
                    self.verbose_function (name_left, node, 0)
                    if name_assignment != None :
                        list_of_variables.append (name_assignment)
                        self.verbose_function (name_assignment, node, 1)
                elif name_left == name_assignment :
                    self.verbose_function (name_left, node, 2)
                else :
                    self.verbose_function (name_left, node, 0)
                    self.verbose_function (name_assignment, node, 1)
                if not(isinstance (node.left, c_ast.ArrayRef) and 
                       isinstance (node.right, c_ast.ArrayRef)) :
                    if not (isinstance(node.right, c_ast.Constant)
                       or isinstance (node.right, c_ast.ID)) :
                        list_cmp [8] += 2
                    else :
                        list_cmp [8] += 1
                    
            elif name_right in list_of_variables :
                list_cmp [7] += 1
                if bool_assignment == False :
                    self.verbose_function (name_right, node, 0)
                    if name_assignment != None :
                        list_of_variables.append (name_assignment)
                        self.verbose_function (name_assignment, node, 1)
                elif name_right == name_assignment :
                    self.verbose_function (name_right, node, 2)
                else :
                    self.verbose_function (name_right, node, 0)
                    self.verbose_function (name_assignment, node, 1)
                    
                if not(isinstance (node.left, c_ast.ArrayRef) and 
                       isinstance (node.right, c_ast.ArrayRef)) :
                    if not (isinstance(node.right, c_ast.Constant)
                       or isinstance (node.right, c_ast.ID)) :
                        list_cmp [8] += 2
                    else :
                        list_cmp [8] += 1
                    
            # if the binary operation contains an other one
            else :
                if isinstance (node.left, c_ast.BinaryOp) :
                    list_cmp = self.read_variables(node.left,
                            list_cmp, bool_assignment, name_assignment)
                if isinstance (node.right, c_ast.BinaryOp) :
                    list_cmp = self.read_variables(node.right,
                            list_cmp, bool_assignment, name_assignment)
                            
                if (not(isinstance (node.left, c_ast.ArrayRef)) and
                    not(isinstance (node.right, c_ast.ArrayRef))) :
                    if (not (isinstance(node.left, c_ast.Constant)) and
                        not (isinstance(node.right, c_ast.Constant))) :
                        list_cmp [8] += 2
                    else :
                        list_cmp [8] += 1
                        
                elif not(isinstance (node.left, c_ast.ArrayRef)) : 
                    if (not (isinstance(node.left, c_ast.Constant)) and
                        not(isinstance(node.right.subscript, c_ast.Constant))) :
                        list_cmp [8] += 2
                    elif not (isinstance(node.right.subscript, c_ast.Constant)) :
                        list_cmp [8] += 1
                        
                elif not(isinstance (node.right, c_ast.ArrayRef)) :
                    if (not (isinstance(node.right, c_ast.Constant)) and
                        not (isinstance(node.left.subscript, c_ast.Constant))) :
                        list_cmp [8] += 2
                    elif not (isinstance(node.left.subscript, c_ast.Constant)) :
                        list_cmp [8] += 1
                        
        elif isinstance (node, c_ast.UnaryOp) :
            name = self.definition(node.expr)
            if (node.op == 'p++' or node.op == 'p--') :
            
                if name in list_of_variables :
                    list_cmp [7] += 1
                    if isinstance (node.expr, c_ast.ArrayRef) :
                        self.verbose_function (name, node, 0)
                    else :
                        self.verbose_function (name, node, 2)
                elif isinstance (node.expr, c_ast.ArrayRef) :
                    if not(isinstance (node.expr.subscript, c_ast.Constant)) :
                        list_cmp = self.read_variables (node.expr.subscript, list_cmp, False, None)
            if (not(isinstance(node.expr, c_ast.ArrayRef)) or
               (isinstance(node.expr, c_ast.ArrayRef) and
               not(isinstance (node.expr.subscript, c_ast.Constant)))) :
                list_cmp [8] += 1
            
            if isinstance (node.expr, c_ast.ArrayRef) :
                name_tab = str(self.definition (node.expr.name))
                if ((node.op == 'p++' or node.op == 'p--') and
                     name_tab in list_of_variables) :
                    list_cmp [7] += 1
                    list_cmp [8] += 1
                    self.verbose_function (name_tab, node, 2)
                
        elif isinstance (node, c_ast.FuncCall) :
            for i in range(len(node.args.exprs)) :
                name = self.definition(node.args.exprs[i])
                if name in list_of_variables :
                    list_cmp [7] += 1
                    self.verbose_function (name, node, 0)
                    
                if isinstance (node.args.exprs[i], c_ast.ArrayRef) :
                    name_tab = str(self.definition (node.args.expr[i].name))
                    if name_tab in list_of_variables :
                        list_cmp [7] += 1
                        self.verbose_function (name_tab, node, 2)
                    list_cmp [8] += 1
                list_cmp [8] += 1
        
        return list_cmp
        

##################################################################
    def Array_analyse (self, node, list_cmp, name_assignment) :
        
        if isinstance (node, c_ast.Assignment) :
            node_left = node.lvalue
            node_right = node.rvalue
        elif isinstance (node, c_ast.BinaryOp) :
            node_left = node.left
            node_right = node.right
        else :
            node_left = node.name
            node_right = node.subscript    
        
        if isinstance(node_left, c_ast.UnaryOp) :
            node_left = node_left.expr
        if isinstance(node_right, c_ast.UnaryOp) :
            node_right = node_right.expr
        
        
        # if each part of assignment is an array
        if (isinstance(node_left, c_ast.ArrayRef) and
            isinstance(node_right, c_ast.ArrayRef) and
            isinstance(node, c_ast.Assignment)) :
            
            name_tab = self.definition(node_left.name)
            name_tab2 = self.definition(node_right.name)
            name_left = self.definition(node_left)
            name_right = self.definition(node_right)
            if (name_tab == name_tab2 and name_tab in list_of_variables) :
                list_cmp [7] += 1
                self.verbose_function (name_tab, node, 2)
                if (isinstance (node_left.subscript, c_ast.Constant) and
                    isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 1
                elif (isinstance (node_left.subscript, c_ast.Constant) or
                    isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 2
                else :
                    list_cmp [8] += 3
                
            elif (name_tab in list_of_variables and name_tab2 in list_of_variables) :
                list_cmp [7] += 1
                self.verbose_function (name_tab2, node, 0)
                self.verbose_function (name_tab, node, 1)
                if (isinstance (node_left.subscript, c_ast.Constant) and
                    isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 1
                elif (isinstance (node_left.subscript, c_ast.Constant) or
                    isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 2
                else :
                    list_cmp [8] += 3
                
            else :
                if (name_tab in list_of_variables and 
                    not(name_tab2 in list_of_variables)) :
                    list_of_variables.remove (name_tab)
                    print list_of_variables
                elif name_tab2 in list_of_variables :
                    list_cmp [7] += 1
                    list_of_variables.append(name_tab)
                    self.verbose_function (name_tab, node, 1)
                list_cmp [8] += 3
                
            if name_right in list_of_variables :
                list_cmp [7] += 1
                list_cmp [8] += 1
                self.verbose_function (name_right, node, 0)
            elif not (isinstance(node_right, c_ast.Constant)
                 or isinstance (node_right, c_ast.ID)) :
                list_cmp [8] += 1
                
        # if the left part is an array
        elif isinstance(node_left, c_ast.ArrayRef) :
            name_tab = self.definition(node_left.name)
            name_right = self.definition(node_right)
            
            if (name_tab in list_of_variables and 
                isinstance(node, c_ast.Assignment) and
                name_right in list_of_variables) :
                list_cmp [7] += 1
                list_cmp [8] += 3
                self.verbose_function (name_right, node, 0) 
                self.verbose_function (name_tab, node, 1)
            
            elif isinstance (node_right, c_ast.BinaryOp) :
                list_cmp = self.read_variables (node_right, list_cmp, False, name_tab)
                
            elif (name_tab in list_of_variables and 
                  isinstance(node, c_ast.Assignment) and
                  not(name_right in list_of_variables)) :
                list_cmp [8] += 3
                list_of_variables.remove (name_tab)
                self.verbose_function(name_tab, node, 1)
                
            elif (isinstance(node, c_ast.BinaryOp) and
                  name_tab in list_of_variables) :
                list_cmp [7] += 1
                if not (isinstance(node_left.subscript, c_ast.Constant)) :
                    list_cmp [8] += 3
                else :
                    list_cmp [8] += 2
                self.verbose_function (name_tab, node, 0)
                
            elif not (name_tab in list_of_variables) :
                if name_right in list_of_variables :
                    list_cmp [7] += 1
                    list_of_variables.append (name_tab)
                    self.verbose_function (name_right, node, 0)
                    self.verbose_function (name_tab, node, 1)
                    list_cmp [8] += 3
                elif isinstance (node_right, c_ast.BinaryOp) :
                    list_cmp = self.read_variables (node_right, list_cmp, False, name_tab)
                else :
                    if (isinstance (node_right, c_ast.Constant) and
                        isinstance (node, c_ast.Assignment)) :
                        list_cmp [8] += 2
                    elif isinstance (node, c_ast.Assignment) :
                        list_cmp [8] += 3
            
        # if the right part is an array    
        elif isinstance(node_right, c_ast.ArrayRef) :

            name_tab = self.definition(node_right.name)
            if name_tab in list_of_variables :
                list_cmp [7] += 1
                self.verbose_function (name_tab, node, 0)
                if name_assignment != None :
                    list_of_variables.append(name_assignment)
                    self.verbose_function(name_assignment, node, 1)
                    list_cmp [8] += 1
                if not(isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 1
                    list_cmp = self.Array_analyse (node_right, list_cmp, None)
                else :
                    list_cmp [8] += 1
            elif name_assignment != None :
                if not(isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 2
                    list_cmp = self.Array_analyse (node_right, list_cmp, None)
                else :
                    list_cmp [8] += 2
            else :
                if not(isinstance (node_right.subscript, c_ast.Constant)) :
                    list_cmp [8] += 2
                    list_cmp = self.Array_analyse (node_right, list_cmp, None)
                else :
                    list_cmp [8] += 1
        
        elif ((isinstance (node_left, c_ast.UnaryOp) and
               isinstance (node_right, c_ast.UnaryOp)) or
               isinstance (node_left, c_ast.UnaryOp) or
               isinstance (node_right, c_ast.UnaryOp)) :
            list_cmp [8] += 2

        return list_cmp


##################### Analysis of Assignment #####################
    def Label_analyse (self, node, list_cmp):
        
        if isinstance (node, c_ast.Assignment) :
            # if node is an assignment,
            # we analyse left side and right side
            name = []
            name = self.nameLR (node, name)
            name_left = name [0]
            name_right = name [1]

            
            if node.op in ['+=', '-=', '*=', '/='] :
                if name_left in list_of_variables :
                    list_cmp [7] += 1
                    self.verbose_function (name_left, node, 2)
                if name_right in list_of_variables :
                    list_cmp [7] += 1
                    self.verbose_function (name_right, node, 0)
                    if not (name_left in list_of_variables) :
                        list_of_variables.append (name_left)
                list_cmp [8] += 2
                
            #if the left side is in the list of variables
            elif name_left in list_of_variables :

                if name_right in list_of_variables :
                    # if both parts of assignment is in the list of
                    # affected variables
                    list_cmp [7] += 1
                    if isinstance (node.lvalue, c_ast.ArrayRef) :
                        self.verbose_function (name_left, node, 0)
                        list_cmp = self.Array_analyse (node, list_cmp, None)
                    elif isinstance (node.rvalue, c_ast.ArrayRef) :
                        self.verbose_function (name_right, node, 0)
                        list_cmp = self.Array_analyse (node, list_cmp, None)
                    elif name_left != name_right :
                        self.verbose_function (name_right, node, 0)
                        self.verbose_function (name_left, node, 1)
                        list_cmp [7] += 1
                        list_cmp [8] += 2
                    else :
                        self.verbose_function (name_left, node, 2)
                        list_cmp [8] += 2
                        
                elif isinstance (node.lvalue, c_ast.ArrayRef) :
                    self.verbose_function (name_left, node, 0)
                    list_cmp = self.Array_analyse (node, list_cmp, None)
                    
                elif (isinstance (node.rvalue, c_ast.BinaryOp) or
                      isinstance (node.rvalue, c_ast.TernaryOp)) :
                    list_cmp = self.read_variables (node.rvalue, list_cmp, True, name_left)
                    list_cmp [7] += 1
                    
                elif (isinstance(node.rvalue, c_ast.Constant)
                   or isinstance (node.rvalue, c_ast.ID)) :
                    # if the right side is a constant,
                    # we delete the left side of the list of
                    # variables because this variables is written
                    list_of_variables.remove (name_left)
                    self.verbose_function (name_left, node, 1)
                    list_cmp [8] += 1
                    
                else :
                    list_cmp [8] += 1
                    print node.rvalue,
                    print node.coord
                    
            elif isinstance (node.rvalue, c_ast.BinaryOp) :
                list_cmp [8] += 1
                list_cmp = self.read_variables (node.rvalue, list_cmp, False, name_left)
            
            elif name_right in list_of_variables :
                list_cmp [7] += 1
                if isinstance (node.lvalue, c_ast.ArrayRef) :
                    list_cmp = self.Array_analyse (node, list_cmp, None)
                else :
                    list_of_variables.append(name_left)
                    list_cmp [8] += 2
                    self.verbose_function (name_right, node, 0)
                    self.verbose_function (name_left, node, 1)

            else :
                list_cmp = self.Array_analyse (node, list_cmp, name_left)
                

        elif (isinstance (node, c_ast.UnaryOp)
           or isinstance (node, c_ast.FuncCall)) :
            list_cmp = self.read_variables (node, list_cmp, False, None)
        
        elif (isinstance(node, c_ast.If) or isinstance(node, c_ast.While)
           or isinstance(node, c_ast.DoWhile) or isinstance(node, c_ast.Switch)) :
            list_cmp = self.read_variables (node.cond, list_cmp, False, None)
            
        elif isinstance(node, c_ast.For) :
            list_cmp = self.read_variables (node.cond.left, list_cmp, False, None)
        
        
        return list_cmp
            
            
######################### Visitor FuncDef ########################
    def visit_FuncDef(self, node, liste) :
        global found_label

        if node.decl.coord == None :
            print "WARNING : the function " + str(node.decl.name) + "doesn't have a number of line"
        
        # analysis of each function
        for c in node.children () :
            if len(node.body.children()) == 0 :
                print "WARNING : function " + str(node.decl.name) + "is empty."
            elif isinstance(c, c_ast.Compound) :
                pile.append(node.decl.name)
                liste = self.generic_visit(node.body, node.decl.name, liste)
                pile.pop()
            if found_label == True :
                break;
        return liste


######################## Visitor func_visit ######################
    def func_visit(self, node, liste) :
        # treatment of definitions of function 

        for c in node.children() :
            if isinstance(c, c_ast.FuncDef) :
                liste = self.visit_FuncDef(c, liste)

        return liste


##################################################################
### END FUNCDECLVISITOR ###


############################## MAIN ##############################

parser = argparse.ArgumentParser(description='Counts assignments, functions in a jump of instructions.')
parser.add_argument('--filename', metavar='filename',
                   help='the filename of the C file to parse')
parser.add_argument('--attack_src',
                   help='the source of attack of the C file to parse')
parser.add_argument('--attack_dest',
                   help='the destination of attack of the C file to parse')
parser.add_argument('-v', action='store_true', default=0,
                   help='verbosely list lign analysed')
parser.add_argument('--calllines', action='store_true', default=0,
                   help='True if the arguments attack_src and attack_dest are line numbers, default : if they are attack numbers')

args = parser.parse_args()
print "\n",
print args
filename=args.filename
src = args.attack_src
dest = args.attack_dest
verbose = args.v
calllines = args.calllines

print ('\n############################################################')
print ('## COUNT OF ASSIGNMENTS, CALLS OF FUNCTION AND STATEMENTS ##')
print ('############################################################\n')


if calllines == True :
    ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH,
                     cpp_args=r'-I./../fake_libc_include')
else :
    ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH,
                     cpp_args=[r'-I./../fake_libc_include',
                           r'-D ATTACK_SOURCE'+src+'=attack'+dest,
                           r'-DATTACK_DESTINATION'+dest+'=1'])

# definition of the list of results :
#liste = [nb_assignment, nb_funccall, block_level, min, cmp_block_level,
#         nb_jumped_block, nb_written_var, nb_read_var, total_nb_read_var]
liste = [0, 0, 0, 0, 0, 0, 0, 0, 0]

v = FuncDeclVisitor()
liste = v.func_visit(ast, liste)

print "\nCount between goto and label :\n"
print "Number of assignments : " + str(liste[0])
print "Number of calls of function : " + str(liste[1])
print "Final block level : " + str(liste[2])
print "Minimum block level : " + str(liste[3])
print "Number of jumped blocks : " + str(liste[5])
print "Number of written variables : " + str(liste[6])
print "\nCount after attack :\n"
print "Number of affected variables after label and written between goto and label : " + str(liste[7])
print "Total of read variables : " + str(liste[8])

print "\nFinished.\n"

##################################################################
