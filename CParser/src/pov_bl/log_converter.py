#!/usr/bin/python2

filenameFormat = 'jump\.(\w*)\.log'

def pyConvertLog(*args):
    """Convert logs from :
    fileSrc.c:lineSrc fileDst:lineDst errorType
    to :
    fonctionName nbBad nbError nbGood nbKilled nbNotTriggered nbSigsegv nbTotal
    """
    import os
    import sys
    import re

    #Function name regex extractor
    pat = re.compile(filenameFormat)

    #Sort by alphabetical order
    args = list(args)
    args.sort()

    for filename in args:

        #Get function name
        m = pat.search(filename)
        if not m or not m.group(1):
            sys.stderr.write('error: cannot extract function name from %s.\n' % filename)
            return
        funcname = m.group(1)

        #Open log file
        try:
            f = open(filename, 'r')
        except Exception, e:
            sys.stderr.write('error: cannot open %s.\n' % filename)
            return

        #Set all counters to zero
        killed = 0
        SIGSEGV = 0
        error = 0
        bad = 0
        not_triggered = 0
        good = 0
        other = 0

        #Read source file and count each occurence
        for line in f.readlines():
            act = line.split()[2]
            if act == 'killed':
                killed = killed + 1
            elif act == 'SIGSEGV':
                SIGSEGV = SIGSEGV + 1
            elif act == 'error':
                error = error + 1
            elif act == 'bad':
                bad = bad + 1
            elif act == 'not_triggered':
                not_triggered = not_triggered + 1
            elif act == 'good':
                good = good + 1
            else: #Shouldn't happen
                other = other + 1

        #Write new log
        total = bad + error + good + killed + not_triggered + SIGSEGV + other
        if not other == 0:
            sys.stderr.write('warning: there are %i unknown errors in %s.\n' % (other, funcname) )
        sys.stdout.write('%s %i %i %i %i %i %i %i\n' % (funcname, bad, error, good, killed, not_triggered, SIGSEGV, total) )

        f.close()

if __name__ == '__main__':
    import sys

    #Read cmdline
    if(len(sys.argv) == 1):
        sys.stderr.write('usage: %s logFiles...\n' % sys.argv[0])
    args = tuple(sys.argv[1:])
    pyConvertLog(*args)
