#proc page
  scale: 1


#proc getdata
  file: out.sorted
  filter: 
  ##set sig = $arith(@@3/@@21*100)
  ##set bad = $arith(@@6/@@21*100)
  ##set error = $arith(@@9/@@21*100)
  ##set good = $arith(@@12/@@21*100)
  ##set killed = $arith(@@15/@@21*100)
  ##set not = $arith(@@18/@@21*100)
// Cut des petites fonctions de moins de 7 lignes, total < 7*7
  ##if @@21 < 48
        ##exit
  ##endif
  ##if @@18 = @@21
        ##exit
  ##endif
  ##if @@1 = applySavedFileAttrToOutputFile || @@1 = applySavedTimeInfoToOutputFile || @@1 = bsPutUInt32 || @@1 = countHardLinks || @@1 = fileExists || @@1 = fopen_output_safely || @@1 = mkCell 
        ##exit
  ##endif
  ##if @@1 = mmed3 || @@1 = notAStandardFile || @@1 = BZ2_bsInitWrite || @@1 = bz_config_ok || @@1 = default_bzalloc || @@1 = init_RL || @@1 = default_bzfree
        ##exit
  ##endif
// A enlever pour la CAMERA READY (pourquoi ?????????)
  ##if @@1 = sendMTFValues || @@1 = snocString
        ##exit
  ##endif
  ##print @@1 @@bad @@killed @@sig @@error @@good @@not


#proc categories
  axis: x
  datafield: 1

// Set up plotting area using proc areadef
#proc areadef
  yrange: 0 100
  xscaletype: categories
  xaxis.ticincrement: 1
  rectangle 2 2 5 4


#proc xaxis
  tics: none
  location: 100(s)

#proc yaxis
  tics: yes
  stubs: incremental
  label: Distribution of attacks in ASM
  stubvert: yes
  stubformat: %g%%
  stubdetails: align=left

#proc bars
  lenfield: 7
  legendlabel: not triggered
  color: white
  stackfields: *
  #saveas B

#proc bars
  #clone: B
  lenfield: 6
  legendlabel: good
  color: green

#proc bars
  #clone: B
  lenfield: 5
  legendlabel: error
  color: yellow

#proc bars
  #clone: B
  lenfield: 4
  legendlabel: signal
  color: blue

#proc bars
  #clone: B
  lenfield: 3
  legendlabel: killed
  color: orange

// q1
#proc bars
  #clone: B
  lenfield: 2
  legendlabel: bad
  color: red
  



// render legend using labels defined above (proc legend)
//#proc legend
//  location: min+1 max+0.2
//  reverseorder: no
//  seglen: 0.3
//  format: accross
