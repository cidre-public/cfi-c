#proc page
  pagesize: 30 8
  scale: 1.3 


#proc getdata
  file: out.sorted
  filter: 
  ##set sig = $arith(@@4)
  ##set bad = $arith(@@7)
  ##set error = $arith(@@10)
  ##set good = $arith(@@13)
  ##set killed = $arith(@@16)
  ##set not = $arith(@@19)
  ##print @@1 @@bad @@killed @@sig @@error @@good @@not


#proc categories
  axis: x
  datafield: 1

// Set up plotting area using proc areadef
#proc areadef
  yrange: 0 25
  xscaletype: categories
  xaxis.ticincrement: 1


#proc xaxis
  stubs: usecategories
  tics: none
  stubvert: yes
  stublen: 10


#proc yaxis
  tics: yes
  stubs: incremental 


// q1
#proc bars
  lenfield: 2
  legendlabel: log(bad)
  color: red
  stackfields: *
  #saveas B
  
#proc bars
  #clone: B
  lenfield: 3
  legendlabel: log(killed)
  color: orange

#proc bars
  #clone: B
  lenfield: 4
  legendlabel: log(signal)
  color: blue

#proc bars
  #clone: B
  lenfield: 5
  legendlabel: log(error)
  color: yellow

#proc bars
  #clone: B
  lenfield: 6
  legendlabel: log(good)
  color: green

#proc bars
  #clone: B
  lenfield: 7
  legendlabel: log(not triggered)
  color: white

// render legend using labels defined above (proc legend)
#proc legend
  location: min-0.6 max+0.2
  reverseorder: yes
  seglen: 0.3
