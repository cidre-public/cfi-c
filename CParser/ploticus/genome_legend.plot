// set up some legend entries...
#proc legendentry
  sampletype: color
  label: bad 
  details: red

#proc legendentry
  sampletype: color
  label: killed
  details: orange

#proc legendentry
  sampletype: color
  label: signal
  details: blue

#proc legendentry
  sampletype: color
  label: error
  details: yellow

#proc legendentry
  sampletype: color
  label: good
  details: green

#proc legendentry
  sampletype: color
  label: not triggered
  details: white

#proc legend
  format: across
  extent: 4.0
  location 1 0.2
  frame: no
