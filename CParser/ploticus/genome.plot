#proc page
  pagesize: 30 8
  scale: 1.3 


#proc getdata
  file: out.sorted
  filter: 
  ##set sig = $arith(@@3/@@21*100)
  ##set bad = $arith(@@6/@@21*100)
  ##set error = $arith(@@9/@@21*100)
  ##set good = $arith(@@12/@@21*100)
  ##set killed = $arith(@@15/@@21*100)
  ##set not = $arith(@@18/@@21*100)
  ##print @@1 @@bad @@killed @@sig @@error @@good @@not


#proc categories
  axis: x
  datafield: 1

// Set up plotting area using proc areadef
#proc areadef
  yrange: 0 100
  xscaletype: categories
  xaxis.ticincrement: 1


#proc xaxis
  stubs: usecategories
  tics: none
  stubvert: yes
  stublen: 10


#proc yaxis
  tics: yes
  stubs: incremental 


// q1
#proc bars
  lenfield: 2
  legendlabel: bad
  color: red
  stackfields: *
  #saveas B
  
#proc bars
  #clone: B
  lenfield: 3
  legendlabel: killed
  color: orange

#proc bars
  #clone: B
  lenfield: 4
  legendlabel: signal
  color: blue

#proc bars
  #clone: B
  lenfield: 5
  legendlabel: error
  color: yellow

#proc bars
  #clone: B
  lenfield: 6
  legendlabel: good
  color: green

#proc bars
  #clone: B
  lenfield: 7
  legendlabel: not triggered
  color: white

// render legend using labels defined above (proc legend)
#proc legend
  location: min-0.6 max+0.2
  reverseorder: yes
  seglen: 0.3
