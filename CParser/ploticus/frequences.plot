#proc page
  pagesize: 30 8
  scale: 1.3 


#proc getdata
  file: frequences.data
  filter: 
  ##set v2 = $arith(@@2/@@8*100)
  ##set v3 = $arith(@@3/@@8*100)
  ##set v4 = $arith(@@4/@@8*100)
  ##set v5 = $arith(@@5/@@8*100)
  ##set v6 = $arith(@@6/@@8*100)
  ##set v7 = $arith(@@7/@@8*100)
  ##print @@1 @@v2 @@v3 @@v4 @@v5 @@v6 @@v7



#proc categories
  axis: x
  datafield: 1

// Set up plotting area using proc areadef
#proc areadef
  yrange: 0 100
  xscaletype: categories
  xaxis.ticincrement: 1


#proc xaxis
  stubs: usecategories
  tics: none
  stubvert: yes
  stublen: 10


#proc yaxis
  tics: yes
  stubs: incremental 


// q1
#proc bars
  lenfield: 2
  legendlabel: bad
  color: red
  stackfields: *
  #saveas B
  #
#proc bars
  #clone: B
  lenfield: 5
  legendlabel: killed
  color: orange

#proc bars
  #clone: B
  lenfield: 7
  legendlabel: SIGSEGV
  color: blue

#proc bars
  #clone: B
  lenfield: 3
  legendlabel: error
  color: yellow

#proc bars
  #clone: B
  lenfield: 4
  legendlabel: good
  color: green

#proc bars
  #clone: B
  lenfield: 6
  legendlabel: not triggered
  color: white

// render legend using labels defined above (proc legend)
#proc legend
  location: min-0.6 max+0.2
  reverseorder: yes
  seglen: 0.3
