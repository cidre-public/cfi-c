#!/bin/bash

# A faire un seule fois car main et mainGTU sont mal classés
# pour BZIP2
#cat out.data | sort > out.sorted
#cat out.sorted | cut -d ' ' -f 1 > out.names

export PLOTICUS_PREFABS=`pwd`/../../ploticus/prefabs 
# 7: nott
# 6: bad1
# 5: killcard
# 4: error
# 3: killed
# 2: good
# 1: bad
PARAMETERS="-prefab heatmapAES x=1 y=2 contentfield=3 colorlist=white,orange,black,blue,green,red cutofflist=7,6,5,4,2,1 xbinsize=1 ybinsize=1 title=$datu zerocolor=white -maxrows 1000000 -maxvector 1000000"
for i in out-*.data
do
datu="${i/data/datu}"
echo "Generating $datu..."
python ../../src/tools/unique.py ${i} > ${datu}
ploticus -png -o ${datu}_heatmap.png ${PARAMETERS} data=${datu}
ploticus -eps -o ${datu}_heatmap.eps ${PARAMETERS} data=${datu} 
done 

echo "Achieved:"
echo "Number of .c"
ls *.c|wc -l
echo "Number of .data"
ls *.data|wc -l
echo "STATS"
python ../../src/tools/stats.py


