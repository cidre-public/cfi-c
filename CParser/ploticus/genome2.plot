#proc page
  scale: 1 


#proc getdata
  file: out.sorted
  filter: 
//  ##set sig = $arith(@@3/@@21*100)
//  ##set bad = $arith(@@6/@@21*100)
//  ##set error = $arith(@@9/@@21*100)
//  ##set good = $arith(@@12/@@21*100)
//  ##set killed = $arith(@@15/@@21*100)
//  ##set not = $arith(@@18/@@21*100)
//  ##print @@1 @@bad @@killed @@sig @@error @@good @@not
  ##set v2 = $arith(@@2/@@8*100)
  ##set v3 = $arith(@@3/@@8*100)
  ##set v4 = $arith(@@4/@@8*100)
  ##set v5 = $arith(@@5/@@8*100)
  ##set v6 = $arith(@@6/@@8*100)
  ##set v7 = $arith(@@7/@@8*100)
  ##if @@8 = @@6
        ##exit
  ##endif
  ##if @@1 = applySavedFileAttrToOutputFile || @@1 = applySavedTimeInfoToOutputFile || @@1 = bsPutUInt32 || @@1 = countHardLinks || @@1 = fileExists || @@1 = fopen_output_safely || @@1 = mkCell 
        ##exit
  ##endif
  ##if @@1 = mmed3 || @@1 = notAStandardFile || @@1 = BZ2_bsInitWrite || @@1 = bz_config_ok || @@1 = default_bzalloc || @@1 = init_RL || @@1 = default_bzfree
        ##exit
  ##endif
// A enlever pour la CAMERA READY
  ##if @@1 = sendMTFValues
        ##exit
  ##endif
  ##print @@1 @@v2 @@v5 @@v7 @@v3 @@v4 @@v6


#proc categories
  axis: x
  datafield: 1

// Set up plotting area using proc areadef
#proc areadef
  yrange: 0 100
  xscaletype: categories
  xaxis.ticincrement: 1


#proc xaxis
  stubs: usecategories
  tics: none
  stubvert: yes
  stublen: 10


#proc yaxis
  tics: yes
  stubs: incremental 
  label: Distribution of attacks among categories


// q1
#proc bars
  lenfield: 2
  legendlabel: bad
  color: red
  stackfields: *
  #saveas B
  
#proc bars
  #clone: B
  lenfield: 3
  legendlabel: killed
  color: orange

#proc bars
  #clone: B
  lenfield: 4
  legendlabel: signal
  color: blue

#proc bars
  #clone: B
  lenfield: 5
  legendlabel: error
  color: yellow

#proc bars
  #clone: B
  lenfield: 6
  legendlabel: good
  color: green

#proc bars
  #clone: B
  lenfield: 7
  legendlabel: not triggered
  color: white

// render legend using labels defined above (proc legend)
#proc legend
  location: min+1 max+0.2
  reverseorder: no
  seglen: 0.3
  format: across
