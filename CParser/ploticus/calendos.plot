#proc page
  pagesize: 10 10
  scale: 1 


// Read and process data file using proc getdata
#proc getdata
  file: frequences.data
  filter: 
  ##set v2 = $arith(@@2)
  ##set v3 = $arith(@@3)
  ##set v4 = $arith(@@4)
  ##set v5 = $arith(@@5)
  ##set v6 = $arith(@@6)
  ##set v7 = $arith(@@7)
  ##print @@v2 @@v3 @@v4 @@v5 @@v6 @@v7

// bad error good killed nottriggered SIGSEGV


#proc processdata
  action: accumulate
  fields: 1 2 3 4 5 6

#proc processdata
  action: rotate
  showresults: yes

//  render the pie graph using proc pie
#proc pie
  datafield: 103
  center: 7 3
  radius: 2
  colors: red yellow green orange blue white
  labels: bad
          error 
          good
          killed 
          not triggered
          SIGSEGV

// render legend using labels defined above (proc legend)
#proc legend
  location: 5 5
  reverseorder: yes
  seglen: 0.3

