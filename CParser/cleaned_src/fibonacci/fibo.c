

/* Fibonacci Series c language */
#include<stdio.h>

int Fibonacci(int n)
{
   int res;
   if ( n == 0 )
   {
      res = 0;
   }
   if ( n == 1 )
   {
      res = 1;
   }
   if (n !=0 && n !=1)
   {
      res = Fibonacci(n-1) + Fibonacci(n-2);
   }
   return res;
}  
 
int main(int argc, char ** argv)
{
   int n, i = 0, c;
   
   n = 10;
 
 
   for ( c = 1 ; c <= n ; c++ )
   {
      int f;
      f = Fibonacci(i);
      i++;
      printf("%d ", f);
   }
 
   return 0;
}
 
