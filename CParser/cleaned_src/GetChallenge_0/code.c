/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "interface.h"
#include "types.h"
#include "commons.h"
#include "random.h"
#include "code.h"

extern BOOL g_challengeFlag;
extern UBYTE g_challenge[CHALLENGE_SIZE];
extern UBYTE g_countermeasure;

void generateRandom(UBYTE* buffer, UBYTE size)
{
    UBYTE i;
	
    for ( i = 0; i < size; i++ )
    {
        buffer[i] = randomUBYTE();
    }
    int zr; // artifact
}

BOOL getChallenge()
{
    g_challengeFlag = BOOL_FALSE;
    generateRandom(g_challenge, CHALLENGE_SIZE);
    g_challengeFlag = BOOL_TRUE;
    return g_challengeFlag;
}

UBYTE randomUBYTE()
{
    return (UBYTE)rand();
}

int main()
{
  getChallenge();
  return 0;
}
