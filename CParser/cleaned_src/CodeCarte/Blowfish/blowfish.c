#include <stdio.h>
#include <stdlib.h>
#include "blowfish.h"
#include "bf_locl.h"
#include <string.h>
#include "bf_pi.h"

//#include "stm32f10x.h"
extern int SER_Init(void);

char *BF_version="";

int resultat = 2;

char *BF_options()
{
#ifdef BF_PTR
  return("blowfish(ptr)");
#elif defined(BF_PTR2)
  return("blowfish(ptr2)");
#else
  return("blowfish(idx)");
#endif
}

#if (BF_ROUNDS != 16) && (BF_ROUNDS != 20)
If you set BF_ROUNDS to some value other than 16 or 20, you will have
  to modify the code.
#endif

  void BF_encrypt(BF_LONG *data, BF_KEY *key, int encrypt)
{
  register BF_LONG l,r,*p,*s;

  p=key->P;
  s= &(key->S[0]);
  l=data[0];
  r=data[1];

  if (encrypt)
    {
      l^=p[0];
      BF_ENC(r,l,s,p[ 1]);
      BF_ENC(l,r,s,p[ 2]);
      BF_ENC(r,l,s,p[ 3]);
      BF_ENC(l,r,s,p[ 4]);
      BF_ENC(r,l,s,p[ 5]);
      BF_ENC(l,r,s,p[ 6]);
      BF_ENC(r,l,s,p[ 7]);
      BF_ENC(l,r,s,p[ 8]);
      BF_ENC(r,l,s,p[ 9]);
      BF_ENC(l,r,s,p[10]);
      BF_ENC(r,l,s,p[11]);
      BF_ENC(l,r,s,p[12]);
      BF_ENC(r,l,s,p[13]);
      BF_ENC(l,r,s,p[14]);
      BF_ENC(r,l,s,p[15]);
      BF_ENC(l,r,s,p[16]);
#if BF_ROUNDS == 20
      BF_ENC(r,l,s,p[17]);
      BF_ENC(l,r,s,p[18]);
      BF_ENC(r,l,s,p[19]);
      BF_ENC(l,r,s,p[20]);
#endif
      r^=p[BF_ROUNDS+1];
    }
  else
    {
      l^=p[BF_ROUNDS+1];
#if BF_ROUNDS == 20
      BF_ENC(r,l,s,p[20]);
      BF_ENC(l,r,s,p[19]);
      BF_ENC(r,l,s,p[18]);
      BF_ENC(l,r,s,p[17]);
#endif
      BF_ENC(r,l,s,p[16]);
      BF_ENC(l,r,s,p[15]);
      BF_ENC(r,l,s,p[14]);
      BF_ENC(l,r,s,p[13]);
      BF_ENC(r,l,s,p[12]);
      BF_ENC(l,r,s,p[11]);
      BF_ENC(r,l,s,p[10]);
      BF_ENC(l,r,s,p[ 9]);
      BF_ENC(r,l,s,p[ 8]);
      BF_ENC(l,r,s,p[ 7]);
      BF_ENC(r,l,s,p[ 6]);
      BF_ENC(l,r,s,p[ 5]);
      BF_ENC(r,l,s,p[ 4]);
      BF_ENC(l,r,s,p[ 3]);
      BF_ENC(r,l,s,p[ 2]);
      BF_ENC(l,r,s,p[ 1]);
      r^=p[0];
    }
  data[1]=l&0xffffffff;
  data[0]=r&0xffffffff;
}


/* The input and output encrypted as though 64bit ofb mode is being
 * used.  The extra state information to record how much of the
 * 64bit block we have used is contained in *num;
 */
void BF_ofb64_encrypt(unsigned char *in, unsigned char *out, long length, BF_KEY *schedule, unsigned char *ivec, int *num)
{
  register unsigned long v0,v1,t;
  register int n= *num;
  register long l=length;
  unsigned char d[8];
  register char *dp;
  unsigned long ti[2];
  unsigned char *iv;
  int save=0;

  iv=(unsigned char *)ivec;
  n2l(iv,v0);
  n2l(iv,v1);
  ti[0]=v0;
  ti[1]=v1;
  dp=(char *)d;
  l2n(v0,dp);
  l2n(v1,dp);
  while (l--)
    {
      if (n == 0)
	{
	  BF_encrypt((unsigned long *)ti,schedule,BF_ENCRYPT);
	  dp=(char *)d;
	  t=ti[0]; l2n(t,dp);
	  t=ti[1]; l2n(t,dp);
	  save++;
	}
      *(out++)= *(in++)^d[n];
      n=(n+1)&0x07;
    }
  if (save)
    {
      v0=ti[0];
      v1=ti[1];
      iv=(unsigned char *)ivec;
      l2n(v0,iv);
      l2n(v1,iv);
    }
  t=v0=v1=ti[0]=ti[1]=0;
  *num=n;
}




void BF_set_key(BF_KEY *key, int len, unsigned char *data)
{
  int i;
  BF_LONG *p,ri,in[2];
  unsigned char *d,*end;


  memcpy((char *)key,(char *)&bf_init,sizeof(BF_KEY));
  p=key->P;

  if (len > ((BF_ROUNDS+2)*4)) len=(BF_ROUNDS+2)*4;

  d=data;
  end= &(data[len]);
  for (i=0; i<(BF_ROUNDS+2); i++)
    {
      ri= *(d++);
      if (d >= end) d=data;

      ri<<=8;
      ri|= *(d++);
      if (d >= end) d=data;

      ri<<=8;
      ri|= *(d++);
      if (d >= end) d=data;

      ri<<=8;
      ri|= *(d++);
      if (d >= end) d=data;

      p[i]^=ri;
    }

  in[0]=0L;
  in[1]=0L;
  for (i=0; i<(BF_ROUNDS+2); i+=2)
    {
      BF_encrypt(in,key,BF_ENCRYPT);
      p[i  ]=in[0];
      p[i+1]=in[1];
    }

  p=key->S;
  for (i=0; i<4*256; i+=2)
    {
      BF_encrypt(in,key,BF_ENCRYPT);
      p[i  ]=in[0];
      p[i+1]=in[1];
    }
  int jfl = 1;
}

void BF_cbc_encrypt(unsigned char *in, unsigned char *out, long length, BF_KEY *ks, unsigned char *iv, int encrypt)
{
  register BF_LONG tin0,tin1;
  register BF_LONG tout0,tout1,xor0,xor1;
  register long l=length;
  BF_LONG tin[2];
  if (encrypt)
    {
      n2l(iv,tout0);
      n2l(iv,tout1);
      iv-=8;
      for (l-=8; l>=0; l-=8)
	{
	  n2l(in,tin0);
	  n2l(in,tin1);
	  tin0^=tout0;
	  tin1^=tout1;
	  tin[0]=tin0;
	  tin[1]=tin1;
	  BF_encrypt(tin,ks,BF_ENCRYPT);
	  tout0=tin[0];
	  tout1=tin[1];
	  l2n(tout0,out);
	  l2n(tout1,out);
	}
      if (l != -8)
	{
	  n2ln(in,tin0,tin1,l+8);
	  tin0^=tout0;
	  tin1^=tout1;
	  tin[0]=tin0;
	  tin[1]=tin1;
	  BF_encrypt(tin,ks,BF_ENCRYPT);
	  tout0=tin[0];
	  tout1=tin[1];
	  l2n(tout0,out);
	  l2n(tout1,out);
	}
      l2n(tout0,iv);
      l2n(tout1,iv);
    }
  else
    {
      n2l(iv,xor0);
      n2l(iv,xor1);
      iv-=8;
      for (l-=8; l>=0; l-=8)
	{
	  n2l(in,tin0);
	  n2l(in,tin1);
	  tin[0]=tin0;
	  tin[1]=tin1;
	  BF_encrypt(tin,ks,BF_DECRYPT);
	  tout0=tin[0]^xor0;
	  tout1=tin[1]^xor1;
	  l2n(tout0,out);
	  l2n(tout1,out);
	  xor0=tin0;
	  xor1=tin1;
	}
      if (l != -8)
	{
	  n2l(in,tin0);
	  n2l(in,tin1);
	  tin[0]=tin0;
	  tin[1]=tin1;
	  BF_encrypt(tin,ks,BF_DECRYPT);
	  tout0=tin[0]^xor0;
	  tout1=tin[1]^xor1;
	  l2nn(tout0,tout1,out,l+8);
	  xor0=tin0;
	  xor1=tin1;
	}
      l2n(xor0,iv);
      l2n(xor1,iv);
    }
  tin0=tin1=tout0=tout1=xor0=xor1=0;
  tin[0]=tin[1]=0;
}



/* The input and output encrypted as though 64bit cfb mode is being
 * used.  The extra state information to record how much of the
 * 64bit block we have used is contained in *num;
 */

void BF_cfb64_encrypt(unsigned char *in, unsigned char *out, long length, BF_KEY *schedule, unsigned char *ivec, int *num, int encrypt)
{
  register BF_LONG v0,v1,t;
  register int n= *num;
  register long l=length;
  BF_LONG ti[2];
  unsigned char *iv,c,cc;

  iv=(unsigned char *)ivec;
  if (encrypt)
    {
      while (l--)
	{
	  if (n == 0)
	    {
	      n2l(iv,v0); ti[0]=v0;
	      n2l(iv,v1); ti[1]=v1;
	      BF_encrypt((unsigned long *)ti,schedule,BF_ENCRYPT);
	      iv=(unsigned char *)ivec;
	      t=ti[0]; l2n(t,iv);
	      t=ti[1]; l2n(t,iv);
	      iv=(unsigned char *)ivec;
	    }
	  c= *(in++)^iv[n];
	  *(out++)=c;
	  iv[n]=c;
	  n=(n+1)&0x07;
	}
      int jfl = 1;
    }
  else
    {
      while (l--)
	{
	  if (n == 0)
	    {
	      n2l(iv,v0); ti[0]=v0;
	      n2l(iv,v1); ti[1]=v1;
	      BF_encrypt((unsigned long *)ti,schedule,BF_ENCRYPT);
	      iv=(unsigned char *)ivec;
	      t=ti[0]; l2n(t,iv);
	      t=ti[1]; l2n(t,iv);
	      iv=(unsigned char *)ivec;
	    }
	  cc= *(in++);
	  c=iv[n];
	  iv[n]=cc;
	  *(out++)=c^cc;
	  n=(n+1)&0x07;
	}
      int jfl = 1;
    }
  v0=v1=ti[0]=ti[1]=t=c=cc=0;
  *num=n;
}



/* Blowfish as implemented from 'Blowfish: Springer-Verlag paper'
 * (From LECTURE NOTES IN COIMPUTER SCIENCE 809, FAST SOFTWARE ENCRYPTION,
 * CAMBRIDGE SECURITY WORKSHOP, CAMBRIDGE, U.K., DECEMBER 9-11, 1993)
 */


void BF_ecb_encrypt(unsigned char *in, unsigned char *out, BF_KEY *ks, int encrypt)
{
  BF_LONG l,d[2];

  n2l(in,l); d[0]=l;
  n2l(in,l); d[1]=l;
  BF_encrypt(d,ks,encrypt);
  l=d[0]; l2n(l,out);
  l=d[1]; l2n(l,out);
  l=d[0]=d[1]=0;
}



int main(int argc, char *argv[]) 
{
  unsigned char outdata2[] = {0x9c, 0xe2, 0x98, 0x84, 0x1c, 0xc4, 0x4a, 0xd6};
  BF_KEY key;
  unsigned char ukey[8];
  unsigned char indata[40],outdata[40],ivec[8], ivec2[8];
  int num;
  int by, i, h;
  int encordec=-1;
  char *cp,ch, *src;
		
  SER_Init();
  num = 7;
  by=0,i=0;
  // JFL encode
  encordec = 1;
  cp = "abc";
      
      
  /* Read the key */
  //cp = argv[4];
  while(i < 64 && *cp)    /* the maximum key length is 32 bytes and   */
    {                       /* hence at most 64 hexadecimal digits      */
      ch = toupper(*cp++);            /* process a hexadecimal digit  */
      if(ch >= '0' && ch <= '9')
	{
	  by = (by << 4) + ch - '0';
	}
      else
	{
	  if(ch >= 'A' && ch <= 'F')
	    {
	      by = (by << 4) + ch - 'A' + 10;
	    }
	  else                            /* error if not hexadecimal     */
	    {
	      printf("key must be in hexadecimal notation\n");
	      //_sys_exit(-1);
        exit(-1);
	    }
	  int jfl = 1;
	}
				  
      /* store a key byte for each pair of hexadecimal digits         */
      if(i++ & 1)
	{
	  ukey[i / 2 - 1] = by & 0xff;
	}
      int jfl = 1;
    }
			    
  BF_set_key(&key,8,ukey);
			      
  if(*cp)
    {
      printf("Bad key value.\n");
      _sys_exit(-1);
    }
  src = "abcdefgh";
  /* JFL : modification ci-dessous */
  h = 0;
  i=0;
  while(src[h]!=0)
    {
      int j;
      while(src[h]!=0 && i<40)
	{
	  indata[i++]=src[h++];
	}
      
      BF_cfb64_encrypt(indata,outdata,i,&key,ivec,&num,encordec);
						
      /* JFL : ajout et modification de ci-dessous */ 
      resultat = 2;		
      for(int j=0;j<8;j++)
	{
	  printf("0x%x=",outdata[j]);
	  printf("0x%x\n",outdata2[j]);
	  if (outdata[j] != outdata2[j]) resultat = 1;
							
	}
      printf("\n");
      if (resultat == 2) resultat = 0;
						      
    }
					
					
					
  _sys_exit(0);
}


