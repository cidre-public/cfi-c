(* Frama-C journal generated at 15:37 the 21/03/2011 *)

(* Run the user commands *)
let run () = Parameters.Files.set [ "all.c" ]; File.init_from_cmdline (); () 

(* Main *)
let main () =
  Journal.keep_file "frama_c_journal.ml";
  try run ()
  with e ->
    Kernel.fatal "Journal raised an exception: %s@." (Printexc.to_string e)

(* Registering *)
let main : unit -> unit =
  Dynamic.register
    "Frama_c_journal.main"
    (Type.func Type.unit Type.unit)
    ~journalize:false
    main

(* Hooking *)
let () = Cmdline.run_after_loading_stage main; Cmdline.is_going_to_load ()
