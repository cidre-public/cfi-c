
#include "interface.h"
#include "types.h"
#include "commons.h"
#include "stdio.h"
#include "code.h"

int main()
{
    initialize();
    verifyPIN_1();
    printf("FIN\n");
    return 0;
}


// global variables definition
BOOL g_authenticated;
SBYTE g_ptc;
UBYTE g_countermeasure;
UBYTE g_userPin[PIN_SIZE];
UBYTE g_cardPin[PIN_SIZE];


void initialize()
{
   // local variables
   int i;
   // global variables initialization
   g_authenticated = BOOL_FALSE;
   g_ptc = 3;
   g_countermeasure = 0;
   // card PIN = 1 2 3 4 5...
   for (i = 0; i < PIN_SIZE; ++i) 
   {
       g_cardPin[i] = i+1;
   }
   // user PIN = 0 0 0 0 0...
   for (i = 0 ; i < PIN_SIZE; ++i) 
   {
       g_userPin[i] = 0;
   }
   int jfl1;
}


extern SBYTE g_ptc;
extern BOOL g_authenticated;
extern UBYTE g_userPin[PIN_SIZE];
extern UBYTE g_cardPin[PIN_SIZE];

#ifdef INLINE
inline BOOL byteArrayCompare(UBYTE* a1, UBYTE* a2, UBYTE size) __attribute__((always_inline))
#else
BOOL byteArrayCompare(UBYTE* a1, UBYTE* a2, UBYTE size)
#endif
{
  int i;
  BOOL ret = BOOL_TRUE;
  for(i = 0; i < size; i++) 
  {
    if(a1[i] != a2[i]) 
    {
      ret = BOOL_FALSE;
    }
  }
  return ret;
}

#if defined INLINE && defined PTC
inline BOOL verifyPIN_1() __attribute__((always_inline))
#else
BOOL verifyPIN_1()
#endif
{
  int comp;
  BOOL ret = BOOL_FALSE;
  g_authenticated = BOOL_FALSE;

  if(g_ptc > 0) 
  {
    comp = byteArrayCompare(g_userPin, g_cardPin, PIN_SIZE);
    if(comp == BOOL_TRUE) 
    {
      g_ptc = 3;
      g_authenticated = BOOL_TRUE; // Authentication();
      printf("auth\n");
      ret = BOOL_TRUE;
    }
    else 
    {
    if(comp == BOOL_FALSE) 
    {
      g_ptc--;
      ret = BOOL_FALSE;
    }
    else 
    {
          int jfl1;
    }
    int jfl2;
    }
    int jfl3;
  }

  return ret;
}
