/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/


#include <stdarg.h>
#include "link-includes.h"

static void clear_hash_table(pp_linkset *ls);

/* The functions in this file do several things: () take a linkage
 involving fat links and expand it into a sequence of linkages
 (involving a subset of the given words), one for each way of
 eliminating the conjunctions.  () determine if a linkage involving
 fat links has a structural violation.  () make sure each of the expanded
 linkages has a consistent post-processing behavior.  () compute the
 cost of the linkage. */


static List_o_links *word_links[MAX_SENTENCE]; /* ptr to l.o.l. out of word */
static int structure_violation;
static int dfs_root_word[MAX_SENTENCE]; /* for the depth-first search */
static int dfs_height[MAX_SENTENCE];    /* to determine the order to do the root word dfs */
static int height_perm[MAX_SENTENCE];   /* permute the vertices from highest to lowest */

/* The following three functions are all for computing the cost of and lists */
static int visited[MAX_SENTENCE];
static int and_element_sizes[MAX_SENTENCE];
static int and_element[MAX_SENTENCE];
static int N_and_elements;
static int outside_word[MAX_SENTENCE];
static int N_outside_words;

typedef struct patch_element_struct Patch_element;
struct patch_element_struct 
{
    char used;   /* TRUE if this link is used, else FALSE  */
    char changed;/* TRUE if this link changed, else FALSE  */
    int newl;    /* the new value of the left end          */
    int newr;    /* the new value of the right end         */
};

static Patch_element patch_array[MAX_LINKS];

typedef struct DIS_node_struct DIS_node;
typedef struct CON_node_struct CON_node;
typedef struct CON_list_struct CON_list;
typedef struct DIS_list_struct DIS_list;
typedef struct Links_to_patch_struct Links_to_patch;
typedef void (*void_returning_function)();

struct DIS_node_struct {
    CON_list * cl;     /* the list of children */
    List_o_links * lol;/* the links that comprise this region of the graph */
    int word;          /* the word defining this node */
};

struct CON_node_struct {
    DIS_list * dl;     /* the list of children */
    DIS_list * current;/* defines the current child */
    int word;          /* the word defining this node */
};

struct DIS_list_struct {
    DIS_list * next;
    DIS_node * dn;
};

struct CON_list_struct {
    CON_list * next;
    CON_node * cn;
};

struct Links_to_patch_struct {
    Links_to_patch * next;
    int link;
    char dir;   /* this is 'r' or 'l' depending on which end of the link
		   is to be patched. */
    int new;    /* the new value of the end to be patched */
};


static Sublinkage * x_create_sublinkage(Parse_info * pi) {
    int i;
    Sublinkage *s = (Sublinkage *) xalloc (sizeof(Sublinkage));
    s->link = (Link *) xalloc(MAX_LINKS*sizeof(Link));
    s->pp_info = NULL;
    s->violation = NULL;
    for (i=0; i<MAX_LINKS; i++)	s->link[i] = NULL;
    s->num_links = pi->N_links;   
    assert(pi->N_links < MAX_LINKS, "Too many links");
    return s;
}


static Sublinkage * ex_create_sublinkage(Parse_info * pi) {
    int i;
    Sublinkage *s = (Sublinkage *) exalloc (sizeof(Sublinkage));
    s->link = (Link *) exalloc(pi->N_links*sizeof(Link));
    s->pp_info = NULL;
    s->violation = NULL;
    for (i=0; i<pi->N_links; i++) s->link[i] = NULL;
    s->num_links = pi->N_links;   
    assert(pi->N_links < MAX_LINKS, "Too many links");
    return s;
}


static void free_sublinkage(Sublinkage *s) {
    int i;
    for (i=0; i<MAX_LINKS; i++) {
	if (s->link[i]!=NULL) exfree_link(s->link[i]);
    }
    xfree(s->link, MAX_LINKS*sizeof(Link));
    xfree(s, sizeof(Sublinkage));
}

static void replace_link_name(Link l, char *s) {
    exfree(l->name, sizeof(char)*(strlen(l->name)+1));
    l->name = (char *) exalloc(sizeof(char)*(strlen(s)+1));
    strcpy(l->name, s);
}

static void copy_full_link(Link *dest, Link src) {
    if (*dest != NULL) exfree_link(*dest);
    *dest = excopy_link(src); 
}

/* end new code 9/97 ALB */


static void build_digraph(Parse_info * pi) {
/* Constructs a graph in the word_links array based on the contents of    */
/* the global link_array.  Makes the word_links array point to a list of  */
/* words neighboring each word (actually a list of links).  This is a     */
/* directed graph, constructed for dealing with "and".  For a link in     */
/* which the priorities are UP or DOWN_priority, the edge goes from the   */
/* one labeled DOWN to the one labeled UP.                                */
/* Don't generate links edges for the bogus comma connectors              */
    int i, link, N_fat;
    Link lp;
    List_o_links * lol;
    N_fat = 0;
    for (i=0; i<pi->N_words; i++) {
	word_links[i] = NULL;
    }
    for (link=0; link<pi->N_links; link++) {
	lp = &(pi->link_array[link]);
	i = lp->lc->label;
	if (i < NORMAL_LABEL) {   /* one of those special links for either-or, etc */
	    continue;  
	}
	lol = (List_o_links *) xalloc(sizeof(List_o_links));
	lol->next = word_links[lp->l];
	word_links[lp->l] = lol;
	lol->link = link;
	lol->word = lp->r;
	i = lp->lc->priority;
	if (i == THIN_priority) {
	    lol->dir = 0;
	} else if (i == DOWN_priority) {
	    lol->dir = 1;
	} else {
	    lol->dir = -1;
	}
	lol = (List_o_links *) xalloc(sizeof(List_o_links));
	lol->next = word_links[lp->r];
	word_links[lp->r] = lol;
	lol->link = link;
	lol->word = lp->l;
	i = lp->rc->priority;
	if (i == THIN_priority) {
	    lol->dir = 0;
	} else if (i == DOWN_priority) {
	    lol->dir = 1;
	} else {
	    lol->dir = -1;
	}
    }
}

static int is_CON_word(int w) {
/* Returns TRUE if there is at least one fat link pointing out of this word. */
    List_o_links * lol;
    for (lol = word_links[w]; lol!=NULL; lol = lol->next) {
	if (lol->dir == 1) {
	    return TRUE;
	}
    }
    return FALSE;
}

static DIS_node * build_DIS_node(int);
static CON_list * c_dfs(int, DIS_node *, CON_list *);

static CON_node * build_CON_node(int w) {
/* This word is a CON word (has fat links down).  Build the tree for it.  */
    List_o_links * lol;
    CON_node * a;
    DIS_list * d, *dx;
    d = NULL;
    for (lol = word_links[w]; lol!=NULL; lol = lol->next) {
	if (lol->dir == 1) {
	    dx = (DIS_list *) xalloc (sizeof (DIS_list));
	    dx->next = d;
	    d = dx;
	    d->dn = build_DIS_node(lol->word);
	}
    }
    a = (CON_node *) xalloc(sizeof (CON_node));
    a->dl = a->current = d;
    a->word = w;
    return a;
}

static CON_list * c_dfs(int w, DIS_node * start_dn, CON_list * c) {
/* Does a depth-first-search starting from w.  Puts on the front of the
   list pointed to by c all of the CON nodes it finds, and returns the
   result.  Also construct the list of all edges reached as part of this
   DIS_node search and append it to the lol list of start_dn.

   Both of the structure violations actually occur, and represent
   linkages that have improper structure.  Fortunately, they
   seem to be rather rare.
*/
    CON_list *cx;
    List_o_links * lol, *lolx;
    if (dfs_root_word[w] != -1) {
	if (dfs_root_word[w] != start_dn->word) {
	    structure_violation = TRUE;
	}
	return c;
    }
    dfs_root_word[w] = start_dn->word;
    for (lol=word_links[w]; lol != NULL; lol = lol->next) {
	if (lol->dir < 0) {  /* a backwards link */
	    if (dfs_root_word[lol->word] == -1) {
		structure_violation = TRUE;
	    }
	} else if (lol->dir == 0) {
	    lolx = (List_o_links *) xalloc(sizeof(List_o_links));
	    lolx->next = start_dn->lol;
	    lolx->link = lol->link;
	    start_dn->lol = lolx;
	    c = c_dfs(lol->word, start_dn, c);
	}
    }
    if (is_CON_word(w)) {  /* if the current node is CON, put it first */
	cx = (CON_list *) xalloc(sizeof(CON_list));
	cx->next = c;
	c = cx;
	c->cn = build_CON_node(w);
    }
    return c;
}    

static DIS_node * build_DIS_node(int w) {
/* This node is connected to its parent via a fat link.  Search the
   region reachable via thin links, and put all reachable nodes with fat
   links out of them in its list of children.
*/
    DIS_node * dn;
    dn = (DIS_node *) xalloc(sizeof (DIS_node));
    dn->word = w;   /* must do this before dfs so it knows the start word */
    dn->lol = NULL;
    dn->cl = c_dfs(w, dn, NULL);
    return dn;
}

void height_dfs(int w, int height) {
    List_o_links * lol;
    if (dfs_height[w] != 0) return;
    dfs_height[w] = height;
    for (lol=word_links[w]; lol != NULL; lol = lol->next) {
	/* The dir is 1 for a down link. */
	height_dfs(lol->word, height - lol->dir);
    }
}

int comp_height(int *a, int *b) {
    return dfs_height[*b] - dfs_height[*a];
}

#define COMPARE_TYPE int (*)(const void *, const void *)

static DIS_node * build_DIS_CON_tree(Parse_info * pi) {
    int xw, w;
    DIS_node * dnroot, * dn;
    CON_list * child, * xchild;
    List_o_links * lol, * xlol;

    /* The algorithm used here to build the DIS_CON tree depends on
       the search percolating down from the "top" of the tree.  The
       original version of this started its search at the wall.  This
       was fine because doing a DFS from the wall explore the tree in
       the right order.

       However, in order to handle null links correctly, a more careful
       ordering process must be used to explore the tree.  We use
       dfs_height[] for this.
     */

    for (w=0; w < pi->N_words; w++) dfs_height[w] = 0;
    for (w=0; w < pi->N_words; w++) height_dfs(w, MAX_SENTENCE);

    for (w=0; w < pi->N_words; w++) height_perm[w] = w;
    qsort(height_perm, pi->N_words, sizeof(height_perm[0]), (COMPARE_TYPE) comp_height);

    for (w=0; w<pi->N_words; w++) dfs_root_word[w] = -1;

    dnroot = NULL;
    for (xw=0; xw < pi->N_words; xw++) {
	w = height_perm[xw];
	if (dfs_root_word[w] == -1) {
	    dn = build_DIS_node(w);
	    if (dnroot == NULL) {
		dnroot = dn;
	    } else {
		for (child = dn->cl; child != NULL; child = xchild) {
		    xchild = child->next;
		    child->next = dnroot->cl;
		    dnroot->cl = child;
		}
		for (lol = dn->lol; lol != NULL; lol = xlol) {
		    xlol = lol->next;
		    lol->next = dnroot->lol;
		    dnroot->lol = lol;
		}
		xfree((void *) dn, sizeof(DIS_node));
	    }
	}
    }
    return dnroot;
}

static int advance_CON(CON_node *);
static int advance_DIS(DIS_node * dn) {
/* Cycically advance the current state of this DIS node.
   If it's now at the beginning of its cycle" return FALSE;
   Otherwise return TRUE;
*/
    CON_list * cl;
    for (cl = dn->cl; cl!=NULL; cl=cl->next) {
	if (advance_CON(cl->cn)) {
	    return TRUE;
	}
    }
    return FALSE;
}

static int advance_CON(CON_node * cn) {
/* Cycically advance the current state of this CON node.
   If it's now at the beginning of its cycle return FALSE;
   Otherwise return TRUE;
*/
    if (advance_DIS(cn->current->dn)) {
	return TRUE;
    } else {
	if (cn->current->next == NULL) {
	    cn->current = cn->dl;
	    return FALSE;
	} else {
	    cn->current = cn->current->next;
	    return TRUE;
	}
    }
}

static void fill_patch_array_CON(CON_node *, Links_to_patch *);

static void fill_patch_array_DIS(DIS_node * dn, Links_to_patch * ltp) {
/* Patches up appropriate links in the patch_array for this DIS_node     */
/* and this patch list.                                                  */
    CON_list * cl;
    List_o_links * lol;
    Links_to_patch * ltpx;
    for (lol=dn->lol; lol != NULL; lol=lol->next) {
	patch_array[lol->link].used = TRUE;
    }
    if ((dn->cl == NULL) || (dn->cl->cn->word != dn->word)) {
	for (; ltp != NULL; ltp = ltpx) {
	    ltpx = ltp->next;
	    patch_array[ltp->link].changed = TRUE;
	    if (ltp->dir == 'l') {
		patch_array[ltp->link].newl = dn->word;
	    } else {
		patch_array[ltp->link].newr = dn->word;
	    }
	    xfree((void *) ltp, sizeof(Links_to_patch));
	}
    }
    /* ltp != NULL at this point means that dn has child which is a cn
       which is the same word */
    for (cl=dn->cl; cl!=NULL; cl=cl->next) {
	fill_patch_array_CON(cl->cn, ltp);
	ltp = NULL;
    }
}

static void fill_patch_array_CON(CON_node * cn, Links_to_patch * ltp) {
    List_o_links * lol;
    Links_to_patch *ltpx;
    for (lol=word_links[cn->word]; lol != NULL; lol = lol->next) {
	if (lol->dir == 0) {
	    ltpx = (Links_to_patch *) xalloc(sizeof(Links_to_patch));
	    ltpx->next = ltp;
	    ltp = ltpx;
	    ltp->new = cn->word;
	    ltp->link = lol->link;
	    if (lol->word > cn->word) {
		ltp->dir = 'l';
	    } else {
		ltp->dir = 'r';
	    }
	}
    }
    fill_patch_array_DIS(cn->current->dn, ltp);
}

static void free_digraph(Parse_info * pi) 
{
  List_o_links * lol, *lolx;
  int i;
  for (i=0; i<pi->N_words; i++) 
    {
      for (lol=word_links[i]; lol!=NULL; lol=lolx) 
	{
	  lolx = lol->next;
	  xfree((void *) lol, sizeof(List_o_links));
	}
    }
}

static void free_CON_tree(CON_node *);
static void free_DIS_tree(DIS_node * dn) {
    List_o_links * lol, *lolx;
    CON_list *cl, *clx;
    for (lol=dn->lol; lol!=NULL; lol=lolx) {
	lolx = lol->next;
	xfree((void *) lol, sizeof(List_o_links));
    }
    for (cl = dn->cl; cl!=NULL; cl=clx) {
	clx = cl->next;
	free_CON_tree(cl->cn);
	xfree((void *) cl, sizeof(CON_list));
    }
    xfree((void *) dn, sizeof(DIS_node));
}

static void free_CON_tree(CON_node * cn) {
    DIS_list *dl, *dlx;
    for (dl = cn->dl; dl!=NULL; dl=dlx) {
	dlx = dl->next;
	free_DIS_tree(dl->dn);
	xfree((void *) dl, sizeof(DIS_list));
    }
    xfree((void *) cn, sizeof(CON_node));
}

static void and_dfs_full(int w) {
/* scope out this and element */
    List_o_links *lol;
    if (visited[w]) return;
    visited[w] = TRUE;
    and_element_sizes[N_and_elements]++;

    for (lol = word_links[w]; lol != NULL; lol = lol->next) {
	if (lol->dir >= 0) {
	    and_dfs_full(lol->word);
	}
    }
}

static void and_dfs_commas(Sentence sent, int w) {
/* get down the tree past all the commas */
    List_o_links *lol;
    if (visited[w]) return;
    visited[w] = TRUE;
    for (lol = word_links[w]; lol != NULL; lol = lol->next) {
	if (lol->dir == 1) {
             /* we only consider UP or DOWN priority links here */

	    if (strcmp(sent->word[lol->word].string, ",")==0) {
		    /* pointing to a comma */
		and_dfs_commas(sent, lol->word);
	    } else {
		and_element[N_and_elements]=lol->word;
		and_dfs_full(lol->word);
		N_and_elements++;
	    }
	}
	if (lol->dir == 0) {
	  outside_word[N_outside_words]=lol->word;
	  N_outside_words++;
	}
    }
}

Andlist * build_andlist(Sentence sent) {
/* This function computes the "and cost", resulting from inequalities in the length of 
   and-list elements. It also computes other information used to construct the "andlist"
   structure of linkage_info. */

    int w, i, min, max, j, cost;
    char * s;
    Andlist * new_andlist, * old_andlist;
    Parse_info * pi = sent->parse_info;

    old_andlist = NULL;
    cost = 0;

    for(w = 0; w<pi->N_words; w++) {
	s = sent->word[w].string;
	if (sent->is_conjunction[w]) {
	    N_and_elements = 0;
	    N_outside_words = 0;
	    for(i=0; i<pi->N_words; i++) {
		visited[i] = FALSE;
		and_element_sizes[i] = 0;
	    }
	    if (sent->dict->left_wall_defined) 
		visited[0] = TRUE; 
	    and_dfs_commas(sent, w);
	    if(N_and_elements == 0) continue;
  	    new_andlist = (Andlist *) xalloc(sizeof(Andlist));
	    new_andlist->num_elements = N_and_elements;
	    new_andlist->num_outside_words = N_outside_words;
	    for (i=0; i<N_and_elements; i++) {
	      new_andlist->element[i] = and_element[i];
	    }
	    for (i=0; i<N_outside_words; i++) {
	      new_andlist->outside_word[i] = outside_word[i];
	    }
	    new_andlist->conjunction = w;
	    new_andlist->next = old_andlist;
	    old_andlist = new_andlist;

	    if (N_and_elements > 0) {
		min=MAX_SENTENCE;
		max=0;
		for (i=0; i<N_and_elements; i++) {
		    j = and_element_sizes[i];
		    if (j < min) min=j;
		    if (j > max) max=j;
		}
		cost += max-min;   
	    }	    
	}
    }
    old_andlist->cost = cost;
    return old_andlist;
}

void islands_dfs(int w) {
    List_o_links *lol;
    if (visited[w]) return;
    visited[w] = TRUE;
    for (lol = word_links[w]; lol != NULL; lol = lol->next) {
      islands_dfs(lol->word);
    }
}

static int cost_for_length(int length) {
/* this function defines the cost of a link as a function of its length */
      return length-1;
}

static int link_cost(Parse_info * pi) {
/* computes the cost of the current parse of the current sentence */
/* due to the length of the links                                 */
    int lcost, i;
    lcost =  0;
    for (i=0; i<pi->N_links; i++) {
	lcost += cost_for_length(pi->link_array[i].r - pi->link_array[i].l);
    }
    return lcost;
}

static int null_cost(Parse_info * pi) {
  /* computes the number of null links in the linkage */
  /* No one seems to care about this -- ALB */
  return 0;
}

static int unused_word_cost(Parse_info * pi) {
    int lcost, i;
    lcost =  0;
    for (i=0; i<pi->N_words; i++) 
	lcost += (pi->chosen_disjuncts[i] == NULL);
    return lcost;
}


static int disjunct_cost(Parse_info * pi) {
/* computes the cost of the current parse of the current sentence     */
/* due to the cost of the chosen disjuncts                            */
    int lcost, i;
    lcost =  0;
    for (i=0; i<pi->N_words; i++) {
      if (pi->chosen_disjuncts[i] != NULL)
	lcost += pi->chosen_disjuncts[i]->cost;
    }
    return lcost;
}

static int strictly_smaller_name(char * s, char * t) {
/*
   Returns TRUE if string s represents a strictly smaller match set
   than does t.  An almost identical function appears in and.c.
   The difference is that here we don't require s and t to be the
   same length.
*/
    int strictness, ss, tt;
    strictness = 0;
    while ((*s!='\0') || (*t!='\0')) {
	if (*s == '\0') {
	    ss = '*';
	} else {
	    ss = *s;
	    s++;
	}
	if (*t == '\0') {
	    tt = '*';
	} else {
	    tt = *t;
	    t++;
	}
	if (ss == tt) continue;
	if ((tt == '*') || (ss == '^')) {
	    strictness++;
	} else {
	    return FALSE;
	}
    }
    return (strictness > 0);  
}

static void compute_link_names(Sentence sent) {
/*
   The name of the link is set to be the GCD of the names of
   its two endpoints.
*/
    int i;
    Parse_info * pi = sent->parse_info;

    for (i=0; i<pi->N_links; i++) {
	pi->link_array[i].name = intersect_strings(sent, 
	   pi->link_array[i].lc->string, pi->link_array[i].rc->string);
    }
}

static void compute_pp_link_names(Sentence sent, Sublinkage *sublinkage)
{
/*
   This fills in the sublinkage->link[].name field.  We assume that
   link_array[].name have already been filled in.  As above, in the
   standard case, the name is just the GCD of the two end points.
   If pluralization has occurred, then we want to use the name
   already in link_array[].name.  We detect this in two ways.
   If the endpoints don't match, then we know pluralization
   has occured.  If they do, but the name in link_array[].name
   is *less* restrictive, then pluralization must have occured.
*/
    int i;
    char * s;
    Parse_info * pi = sent->parse_info;

    for (i=0; i<pi->N_links; i++) 
      {
	if (sublinkage->link[i]->l == -1) continue;
	if (!x_match(sublinkage->link[i]->lc, sublinkage->link[i]->rc)) 
	  replace_link_name(sublinkage->link[i], pi->link_array[i].name);
	else 
	  {
	    s = intersect_strings(sent, sublinkage->link[i]->lc->string,
				  sublinkage->link[i]->rc->string);
	    if (strictly_smaller_name(s, pi->link_array[i].name)) 
	      replace_link_name(sublinkage->link[i], pi->link_array[i].name);
	    else replace_link_name(sublinkage->link[i], s); 
	  }
    }
}

/********************** exported functions *****************************/

Linkage_info analyze_fat_linkage(Sentence sent, Parse_Options opts, int analyze_pass) {
  /* This uses link_array.  It enumerates and post-processes
     all the linkages represented by this one.  We know this contains
     at least one fat link. */
    int i;
    Linkage_info li;
    DIS_node *d_root;
    PP_node *pp;
    Postprocessor *postprocessor;
    Sublinkage *sublinkage;
    Parse_info * pi = sent->parse_info;
    PP_node accum;               /* for domain ancestry check */
    D_type_list * dtl0, * dtl1;  /* for domain ancestry check */

    sublinkage = x_create_sublinkage(pi);
    postprocessor = sent->dict->postprocessor;
    build_digraph(pi);
    structure_violation = FALSE;  
    d_root = build_DIS_CON_tree(pi); /* may set structure_violation to TRUE */

    li.N_violations = 0;
    li.improper_fat_linkage = structure_violation;
    li.inconsistent_domains = FALSE;
    li.unused_word_cost = unused_word_cost(sent->parse_info);
    li.disjunct_cost = disjunct_cost(pi);
    li.null_cost = null_cost(pi);
    li.link_cost = link_cost(pi);

    if (structure_violation) {
	li.N_violations++;
	li.and_cost = 0;      /* ? */
	li.andlist = NULL;
	free_sublinkage(sublinkage);
	free_digraph(pi);
	free_DIS_tree(d_root);
	return li;
    }

    if (analyze_pass==PP_SECOND_PASS) {
      li.andlist = build_andlist(sent); 
      li.and_cost = li.andlist->cost; 
    } 
    else li.and_cost = 0; 

    compute_link_names(sent);

    for (i=0; i<pi->N_links; i++) accum.d_type_array[i] = NULL;
    
    for (;;) {	/* loop through all the sub linkages */
	for (i=0; i<pi->N_links; i++) {
	    patch_array[i].used = patch_array[i].changed = FALSE;
	    patch_array[i].newl = pi->link_array[i].l;
	    patch_array[i].newr = pi->link_array[i].r;
	    copy_full_link(&sublinkage->link[i], &(pi->link_array[i]));
	}
	fill_patch_array_DIS(d_root, NULL);

	for (i=0; i<pi->N_links; i++) {
	    if (patch_array[i].changed || patch_array[i].used) {
		sublinkage->link[i]->l = patch_array[i].newl;
		sublinkage->link[i]->r = patch_array[i].newr;
	    } 
	    else if ((dfs_root_word[pi->link_array[i].l] != -1) &&
		     (dfs_root_word[pi->link_array[i].r] != -1)) {
		sublinkage->link[i]->l = -1;
	    }
	}

	compute_pp_link_array_connectors(sent, sublinkage);
	compute_pp_link_names(sent, sublinkage);
	
	/* 'analyze_pass' logic added ALB 1/97 */
	if (analyze_pass==PP_FIRST_PASS) {
	    post_process_scan_linkage(postprocessor,opts,sent,sublinkage);
	    if (!advance_DIS(d_root)) break; 
	    else continue;
	}

	pp = post_process(postprocessor, opts, sent, sublinkage, TRUE);

	if (pp==NULL) {
	    if (postprocessor != NULL) li.N_violations = 1;
	}
	else if (pp->violation == NULL)  {
	    /* the purpose of this stuff is to make sure the domain
	       ancestry for a link in each of its sentences is consistent. */

	    for (i=0; i<pi->N_links; i++) {
		if (sublinkage->link[i]->l == -1) continue;
		if (accum.d_type_array[i] == NULL) {
		    accum.d_type_array[i] = copy_d_type(pp->d_type_array[i]);
		} else {
		    dtl0 = pp->d_type_array[i];
		    dtl1 = accum.d_type_array[i];
		    while((dtl0 != NULL) && (dtl1 != NULL) && (dtl0->type == dtl1->type)) {
			dtl0 = dtl0->next;
			dtl1 = dtl1->next;
		    }
		    if ((dtl0 != NULL) || (dtl1 != NULL)) break;
		}
	    }
	    if (i != pi->N_links) {
		li.N_violations++;
		li.inconsistent_domains = TRUE;
	    }
	}
	else if (pp->violation!=NULL) {
	    li.N_violations++;
	}
	
	if (!advance_DIS(d_root)) break; 
    }

    for (i=0; i<pi->N_links; ++i) {
	free_d_type(accum.d_type_array[i]);
    }

    /* if (display_on && (li.N_violations != 0) && 
       (verbosity > 3) && should_print_messages) 
       printf("P.P. violation in one part of conjunction.\n"); */
    free_sublinkage(sublinkage);
    free_digraph(pi);
    free_DIS_tree(d_root);
    return li;
}

Linkage_info analyze_thin_linkage(Sentence sent, Parse_Options opts, int analyze_pass)
{
    /* This uses link_array.  It post-processes
       this linkage, and prints the appropriate thing.  There are no fat
       links in it. */
    int i;
    Linkage_info li;
    PP_node * pp;
    Postprocessor * postprocessor;
    Sublinkage *sublinkage;
    Parse_info * pi = sent->parse_info;
    build_digraph(pi);

    sublinkage = x_create_sublinkage(pi);
    postprocessor = sent->dict->postprocessor;

    compute_link_names(sent);
    for (i=0; i<pi->N_links; i++) {
      copy_full_link(&(sublinkage->link[i]), &(pi->link_array[i]));
    }

    if (analyze_pass==PP_FIRST_PASS) {                   
        post_process_scan_linkage(postprocessor, opts, sent, sublinkage);
	free_sublinkage(sublinkage);
	free_digraph(pi);
        return li;                             
    }                                          

    li.N_violations = 0;
    li.and_cost = 0;

    /* The code below can be used to generate the "islands" array. For this to work,
       however, you have to call "build_digraph" first (as in analyze_fat_linkage).
       and then "free_digraph". For some reason this causes a space leak. */
    
    pp = post_process(postprocessor, opts, sent, sublinkage, TRUE); 

    li.unused_word_cost = unused_word_cost(sent->parse_info);
    li.improper_fat_linkage = FALSE;
    li.inconsistent_domains = FALSE;
    li.disjunct_cost = disjunct_cost(pi);
    li.null_cost = null_cost(pi);
    li.link_cost = link_cost(pi);
    li.andlist = NULL;

    if (pp==NULL) {
	if (postprocessor != NULL) li.N_violations = 1;
    } else if (pp->violation!=NULL) {
	li.N_violations++;
    }

    free_sublinkage(sublinkage); 
    free_digraph(pi);
    return li;
}

void extract_thin_linkage(Sentence sent, Parse_Options opts, Linkage linkage) 
{
    int i;
    Sublinkage *sublinkage;
    Parse_info * pi = sent->parse_info;

    sublinkage = x_create_sublinkage(pi);
    compute_link_names(sent);
    for (i=0; i<pi->N_links; i++) {
	copy_full_link(&sublinkage->link[i],&(pi->link_array[i]));
    }

    linkage->num_sublinkages = 1;
    linkage->sublinkage = ex_create_sublinkage(pi);

    for (i=0; i<pi->N_links; ++i) {
	linkage->sublinkage->link[i] = excopy_link(sublinkage->link[i]);
    }

    free_sublinkage(sublinkage);
}


void extract_fat_linkage(Sentence sent, Parse_Options opts, Linkage linkage) {
  /* This procedure mimics analyze_fat_linkage in order to 
     extract the sublinkages and copy them to the Linkage
     data structure passed in. 
     */
    int i, j, N_thin_links;
    DIS_node *d_root;
    int num_sublinkages;
    Sublinkage * sublinkage;
    Parse_info * pi = sent->parse_info;

    sublinkage = x_create_sublinkage(pi);
    build_digraph(pi);
    structure_violation = FALSE;
    d_root = build_DIS_CON_tree(pi);
    
    if (structure_violation) {
	compute_link_names(sent);
	for (i=0; i<pi->N_links; i++) {
	    copy_full_link(&sublinkage->link[i],&(pi->link_array[i]));
	}

	linkage->num_sublinkages=1;
	linkage->sublinkage = ex_create_sublinkage(pi);

	/* This will have fat links! */
	for (i=0; i<pi->N_links; ++i) {
	    linkage->sublinkage->link[i] = excopy_link(sublinkage->link[i]);
	}

	free_sublinkage(sublinkage);
	free_digraph(pi);
	free_DIS_tree(d_root);
	return;
    }
    
    /* first get number of sublinkages and allocate space */
    num_sublinkages = 0;
    for (;;) {
	num_sublinkages++;
	if (!advance_DIS(d_root)) break; 
    }
    
    linkage->num_sublinkages = num_sublinkages;
    linkage->sublinkage = 
	(Sublinkage *) exalloc(sizeof(Sublinkage)*num_sublinkages);
    for (i=0; i<num_sublinkages; ++i) {
	linkage->sublinkage[i].pp_info = NULL;
	linkage->sublinkage[i].violation = NULL;
    }
    
    /* now fill out the sublinkage arrays */
    compute_link_names(sent);
    
    num_sublinkages = 0;
    for (;;) {
	for (i=0; i<pi->N_links; i++) {
	    patch_array[i].used = patch_array[i].changed = FALSE;
	    patch_array[i].newl = pi->link_array[i].l;
	    patch_array[i].newr = pi->link_array[i].r;
	    copy_full_link(&sublinkage->link[i], &(pi->link_array[i]));
	}
	fill_patch_array_DIS(d_root, NULL);
	
	for (i=0; i<pi->N_links; i++) {
	    if (patch_array[i].changed || patch_array[i].used) {
		sublinkage->link[i]->l = patch_array[i].newl;
		sublinkage->link[i]->r = patch_array[i].newr;
	    } else if ((dfs_root_word[pi->link_array[i].l] != -1) &&
		       (dfs_root_word[pi->link_array[i].r] != -1)) {
		sublinkage->link[i]->l = -1;
	    }
	}
	
	compute_pp_link_array_connectors(sent, sublinkage);
	compute_pp_link_names(sent, sublinkage);
	
	/* Don't copy the fat links into the linkage */
	N_thin_links = 0;
	for (i= 0; i<pi->N_links; ++i) {
	    if (sublinkage->link[i]->l == -1) continue;
	    N_thin_links++;
	}
	
	linkage->sublinkage[num_sublinkages].num_links = N_thin_links;
	linkage->sublinkage[num_sublinkages].link = 
	    (Link *) exalloc(sizeof(Link)*N_thin_links);
	linkage->sublinkage[num_sublinkages].pp_info = NULL;
	linkage->sublinkage[num_sublinkages].violation = NULL;
	
	for (i=0, j=0; i<pi->N_links; ++i) {
	    if (sublinkage->link[i]->l == -1) continue;
	    linkage->sublinkage[num_sublinkages].link[j++] = 
		excopy_link(sublinkage->link[i]);
	}
	
	
	num_sublinkages++;
	if (!advance_DIS(d_root)) break; 
    }
    
    free_sublinkage(sublinkage);
    free_digraph(pi);
    free_DIS_tree(d_root);
}



/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/*
                             Notes about AND

  A large fraction of the code of this parser seems to deal with handling
  conjunctions.  This comment (combined with reading the paper) should
  give an idea of how it works.

  First of all, we need a more detailed discussion of strings, what they
  match, etc.  (This entire discussion ignores the labels, which are
  semantically the same as the leading upper case letters of the
  connector.)

  We'll deal with infinite strings from an alphabet of three types of
  characters: "*". "^" and ordinary characters (denoted "a" and "b").
  (The end of a string should be thought of as an infinite sequence of
  "*"s).

  Let match(s) be the set of strings that will match the string s.  This
  is defined as follows. A string t is in match(s) if (1) its leading
  upper case letters exactly match those of s.  (2) traversing through
  both strings, from left to right in step, no missmatch is found
  between corresponding letters.  A missmatch is a pair of differing
  ordinary characters, or a "^" and any ordinary letter or two "^"s.
  In other words, a match is exactly a "*" and anything, or two
  identical ordinary letters.

  Alternative definition of the set match(s):
  {t | t is obtained from s by replacing each "^" and any other characters
  by "*"s, and replacing any original "*" in s by any other character
  (or "^").}

  Theorem: if t in match(s) then s in match(t).

  It is also a theorem that given any two strings s and t, there exists a
  unique new string u with the property that:

	      match(u) = match(s) intersect match(t)

  This string is called the GCD of s and t.  Here are some examples.

		  GCD(N*a,Nb) = Nba
		  GCD(Na, Nb) = N^
		  GCD(Nab,Nb) = N^b
		  GCD(N^,N*a) = N^a
		  GCD(N^,  N) = N^
		  GCD(N^^,N^) = N^^

  We need an algorithm for computing the GCD of two strings.  Here is
  one.

  First get by the upper case letters (which must be equal, otherwise
  there is no intersection), issuing them.  Traverse the rest of the
  characters of s and t in lockstep until there is nothing left but
  "*"s.  If the two characters are:

		 "a" and "a", issue "a"
		 "a" and "b", issue "^"
		 "a" and "*", issue "a"
		 "*" and "*", issue "*"
		 "*" and "^", issue "^"
		 "a" and "^", issue "^"
		 "^" and "^", issue "^"

  A simple case analysis suffices to show that any string that matches
  the right side, must match both of the left sides, and any string not
  matching the right side must not match at least one of the left sides.

  This proves that the GCD operator is associative and commutative.
  (There must be a name for a mathematical structure with these properties.)

  To elaborate further on this theory, define the notion of two strings
  matching in the dual sense as follows: s and t dual-match if
  match(s) is contained in match(t) or vice versa---

  Full development of this theory could lead to a more efficient
  algorithm for this problem.  I'll defer this until such time as it
  appears necessary.


  We need a data structure that stores a set of fat links.  Each fat
  link has a number (called its label).  The fat link operates in liu of
  a collection of links.  The particular stuff it is a substitute for is
  defined by a disjunct.  This disjunct is stored in the data structure.

  The type of a disjunct is defined by the sequence of connector types
  (defined by their upper case letters) that comprises it.  Each entry
  of the label_table[] points to a list of disjuncts that have the same
  type (a hash table is uses so that, given a disjunct, we can efficiently
  compute the element of the label table in which it belongs).

  We begin by loading up the label table with all of the possible
  fat links that occur through the words of the sentence.  These are
  obtained by taking every sub-range of the connectors of each disjunct
  (containing the center).  We also compute the closure (under the GCD
  operator) of these disjuncts and store also store these in the
  label_table.  Each disjunct in this table has a string which represents
  the subscripts of all of its connectors (and their multi-connector bits).

  It is possible to generate a fat connector for any one of the
  disjuncts in the label_table.  This connector's label field is given
  the label from the disjunct from which it arose.  It's string field
  is taken from the string of the disjunct (mentioned above).  It will be
  given a priority with a value of UP_priority or DOWN_priority (depending
  on how it will be used).  A connector of UP_priority can match one of
  DOWN_priority, but neither of these can match any other priority.
  (Of course, a fat connector can match only another fat connector with
  the same label.)

  The paper describes in some detail how disjuncts are given to words
  and to "and" and ",", etc.  Each word in the sentence gets many more
  new disjuncts.  For each contiguous set of connectors containing (or
  adjacent to) the center of the disjunct, we generate a fat link, and
  replace these connector in the word by a fat link.  (Actually we do
  this twice.  Once pointing to the right, once to the left.)  These fat
  links have priority UP_priority.

  What do we generate for ","?  For each type of fat link (each label)
  we make a disjunct that has two down connectors (to the right and left)
  and one up connector (to the right).  There will be a unique way of
  hooking together a comma-separated and-list.

  The disjuncts on "and" are more complicated.  Here we have to do just what
  we did for comma (but also include the up link to the left), then
  we also have to allow the process to terminate.  So, there is a disjunct
  with two down fat links, and between them are the original thin links.
  These are said to "blossom" out.  However, this is not all that is
  necessary.  It's possible for an and-list to be part of another and list
  with a different labeled fat connector.  To make this possible, we
  regroup the just blossomed disjuncts (in all possible ways about the center)
  and install them as fat links.  If this sounds like a lot of disjuncts --
  it is!  The program is currently fairly slow on long sentence with and.

  It is slightly non-obvious that the fat-links in a linkage constructed
  from disjuncts defined in this way form a binary tree.  Naturally,
  connectors with UP_priority point up the tree, and those with DOWN_priority
  point down the tree.

  Think of the string x on the connector as representing a set X of strings.
  X = match(x).  So, for example, if x="S^" then match(x) = {"S", "S*a",
  "S*b", etc}.  The matching rules for UP and DOWN priority connectors
  are such that as you go up (the tree of ands) the X sets get no larger.
  So, for example, a "Sb" pointing up can match an "S^" pointing down.
  (Because more stuff can match "Sb" than can match "S^".)
  This guarantees that whatever connector ultimately gets used after the
  fat connector blossoms out (see below), it is a powerful enough connector
  to be able to match to any of the connectors associated with it.

  One problem with the scheme just descibed is that it sometimes generates
  essentially the same linkage several times.  This happens if there is
  a gap in the connective power, and the mismatch can be moved around in
  different ways.  Here is an example of how this happens.

  (Left is DOWN, right is UP)

     Sa <---> S^ <---> S            or             Sa <---> Sa <---> S 
	 fat      thin                                 fat      thin

  Here two of the disjunct types are given by "S^" and "Sa".  Notice that
  the criterion of shrinking the matching set is satisfied by the the fat
  link (traversing from left to right).  How do I eliminate one of these?

  I use the technique of canonization.  I generate all the linkages.  There
  is then a procedure that can check to see of a linkage is canonical.
  If it is, it's used, otherwise it's ignored.  It's claimed that exactly
  one canonical one of each equivalence class will be generated.
  We basically insist that the intermediate fat disjuncts (ones that
  have a fat link pointing down) are all minimal -- that is, that they
  cannot be replaced by by another (with a strictly) smaller match set.
  If one is not minimal, then the linkage is rejected.

  Here's a proof that this is correct.  Consider the set of equivalent
  linkages that are generated.  These Pick a disjunct that is the root of
  its tree.  Consider the set of all disjuncts which occur in that positon
  among the equivalent linkages.  The GCD of all of these can fit in that
  position (it matches down the tree, since its match set has gotten
  smaller, and it also matches to the THIN links.)  Since the GCD is put
  on "and" this particular one will be generated.  Therefore rejecting
  a linkage in which a root fat disjunct can be replaced by a smaller one
  is ok (since the smaller one will be generated separately).  What about
  a fat disjunct that is not the root.  We consider the set of linkages in 
  which the root is minimal (the ones for which it's not have already been
  eliminated).  Now, consider one of the children of the root in precisely
  the way we just considered the root.  The same argument holds.  The only
  difference is that the root node gives another constraint on how small
  you can make the disjunct -- so, within these constraints, if we can go
  smaller, we reject.

  The code to do all of this is fairly ugly, but I think it works.


Problems with this stuff:

  1) There is obviously a combinatorial explosion that takes place.
     As the number of disjuncts (and the number of their subscripts
     increase) the number of disjuncts that get put onto "and" will
     increase tremendously.  When we made the transcript for the tech
     report (Around August 1991) most of the sentence were processed
     in well under 10 seconds.  Now (Jan 1992), some of these sentences
     take ten times longer.  As of this writing I don't really know the
     reason, other than just the fact that the dictionary entries are
     more complex than they used to be.   The number of linkages has also
     increased significantly.
  
  2) Each element of an and list must be attached through only one word.
     This disallows "there is time enough and space enough for both of us", 
     and many other reasonable sounding things.  The combinatorial
     explosion that would occur if you allowed two different connection
     points would be tremendous, and the number of solutions would also
     probably go up by another order of magnitude.  Perhaps if there
     were strong constraints on the type of connectors in which this
     would be allowed, then this would be a conceivable prospect.

  3) A multi-connector must be either all "outside" or all "inside" the and.
     For example, "the big black dog and cat ran" has only two ways to
     linkages (instead of three).

Possible bug: It seems that the following two linkages should be the
same under the canonical linkage test.  Could this have to do with the
pluralization system?

> I am big and the bike and the car were broken
Accepted (4 linkages, 4 with no P.P. violations) at stage 1
  Linkage 1, cost vector = (0, 0, 18)

                                   +------Spx-----+      
       +-----CC-----+------Wd------+-d^^*i^-+     |      
  +-Wd-+Spi+-Pa+    |   +--Ds-+d^^*+   +-Ds-+     +--Pv-+
  |    |   |   |    |   |     |    |   |    |     |     |
///// I.p am big.a and the bike.n and the car.n were broken 

       /////          RW      <---RW---->  RW        /////
       /////          Wd      <---Wd---->  Wd        I.p
       I.p            CC      <---CC---->  CC        and
       I.p            Sp*i    <---Spii-->  Spi       am
       am             Pa      <---Pa---->  Pa        big.a
       and            Wd      <---Wd---->  Wd        and
       bike.n         d^s**  6<---d^^*i->  d^^*i  6  and
       the            D       <---Ds---->  Ds        bike.n
       and            Sp      <---Spx--->  Spx       were
       and            d^^*i  6<---d^^*i->  d^s**  6  car.n
       the            D       <---Ds---->  Ds        car.n
       were           Pv      <---Pv---->  Pv        broken

(press return for another)
> 
  Linkage 2, cost vector = (0, 0, 18)

                                   +------Spx-----+      
       +-----CC-----+------Wd------+-d^s**^-+     |      
  +-Wd-+Spi+-Pa+    |   +--Ds-+d^s*+   +-Ds-+     +--Pv-+
  |    |   |   |    |   |     |    |   |    |     |     |
///// I.p am big.a and the bike.n and the car.n were broken 

       /////          RW      <---RW---->  RW        /////
       /////          Wd      <---Wd---->  Wd        I.p
       I.p            CC      <---CC---->  CC        and
       I.p            Sp*i    <---Spii-->  Spi       am
       am             Pa      <---Pa---->  Pa        big.a
       and            Wd      <---Wd---->  Wd        and
       bike.n         d^s**  6<---d^s**->  d^s**  6  and
       the            D       <---Ds---->  Ds        bike.n
       and            Sp      <---Spx--->  Spx       were
       and            d^s**  6<---d^s**->  d^s**  6  car.n
       the            D       <---Ds---->  Ds        car.n
       were           Pv      <---Pv---->  Pv        broken

*/

static int STAT_N_disjuncts;      /* keeping statistics */
static int STAT_calls_to_equality_test;

void init_LT(Sentence sent) {
    sent->and_data.LT_bound = 20;
    sent->and_data.LT_size = 0;
    sent->and_data.label_table = 
	(Disjunct **) xalloc(sent->and_data.LT_bound * sizeof(Disjunct *));
}

void grow_LT(Sentence sent) {
    space_in_use -= sent->and_data.LT_bound * sizeof(Disjunct *);
    sent->and_data.LT_bound = (3*sent->and_data.LT_bound)/2;
    sent->and_data.label_table = 
	(Disjunct **) realloc((void *)sent->and_data.label_table,
			      sent->and_data.LT_bound * sizeof(Disjunct *));
    space_in_use += sent->and_data.LT_bound * sizeof(Disjunct *);
    if (space_in_use > max_space_in_use) max_space_in_use = space_in_use;
    if (sent->and_data.label_table == NULL) {
	printf("Ran out of space reallocing the label table\n");
	exit(1);
    }
}
    
void init_HT(Sentence sent) {
    int i;
    for (i=0; i<HT_SIZE; i++) {
	sent->and_data.hash_table[i] = NULL;
    }
}

void free_HT(Sentence sent) {
    int i;
    Label_node * la, * la1;
    for (i=0; i<HT_SIZE; i++) {
	for (la=sent->and_data.hash_table[i]; la != NULL; la = la1) {
	    la1 = la->next;
	    xfree((char *)la, sizeof(Label_node));
	}
	sent->and_data.hash_table[i] = NULL;
    }
}

void free_LT(Sentence sent) {
    int i;
    for (i=0; i<sent->and_data.LT_size; i++) {
	free_disjuncts(sent->and_data.label_table[i]);
    }
    xfree((char *) sent->and_data.label_table, 
	  sent->and_data.LT_bound * sizeof(Disjunct*));
    sent->and_data.LT_bound = 0;
    sent->and_data.LT_size = 0;
    sent->and_data.label_table = NULL;
}

void free_AND_tables(Sentence sent) {
    free_LT(sent);
    free_HT(sent);
}

void initialize_conjunction_tables(Sentence sent) {
    int i;
    sent->and_data.LT_bound = 0;
    sent->and_data.LT_size = 0;
    sent->and_data.label_table = NULL;
    for (i=0; i<HT_SIZE; i++) {
	sent->and_data.hash_table[i] = NULL;
    }
}

int and_connector_hash(Connector * c, int i) {
/* This hash function that takes a connector and a seed value i.
   It only looks at the leading upper case letters of
   the string, and the label.  This ensures that if two connectors
   match, then they must hash to the same place. 
*/
    char * s;
    s = c->string;

    i = i + (i<<1) + randtable[(c->label + i) & (RTSIZE-1)];
    while(isupper((int)*s)) {
	i = i + (i<<1) + randtable[(*s + i) & (RTSIZE-1)];
	s++;
    }
    return (i & (HT_SIZE-1));
}

int and_hash_disjunct(Disjunct *d) {
/* This is a hash function for disjuncts */
    int i;
    Connector *e;
    i = 0;
    for (e = d->left ; e != NULL; e = e->next) {
	i = and_connector_hash(e, i);
    }
    i = i + (i<<1) + randtable[i & (RTSIZE-1)];
    for (e = d->right ; e != NULL; e = e->next) {
	i = and_connector_hash(e, i);
    }
    return (i & (HT_SIZE-1));
}


int is_appropriate(Sentence sent, Disjunct * d) {
/* returns TRUE if the disjunct is appropriate to be made into fat links.
   Check here that the connectors are from some small set.
   This will disallow, for example "the and their dog ran".
*/
    Connector * c;

    if (sent->dict->andable_connector_set == NULL) return TRUE;
    /* if no set, then everything is considered andable */
    for (c = d->right; c!=NULL; c=c->next) {
	if (!match_in_connector_set(sent->dict->andable_connector_set, c, '+')) return FALSE;
    }
    for (c = d->left; c!=NULL; c=c->next) {
	if (!match_in_connector_set(sent->dict->andable_connector_set, c, '-')) return FALSE;
    }
    return TRUE;
}

int connector_types_equal(Connector * c1, Connector * c2) {
/* Two connectors are said to be of the same type if they have
   the same label, and the initial upper case letters of their
   strings match.
*/
    char * s, * t;
    if (c1->label != c2->label) return FALSE;
    s = c1->string;
    t = c2->string;
    while(isupper((int)*s) || isupper((int)*t)) {
	if (*s != *t) return FALSE;
	s++;
	t++;
    }
    return TRUE;
}

int disjunct_types_equal(Disjunct * d1, Disjunct * d2) {
/* Two disjuncts are said to be the same type if they're the same
   ignoring the multi fields, the priority fields, and the subscripts
   of the connectors (and the string field of the disjunct of course).
   Disjuncts of the same type are located in the same label_table list.

   This returns TRUE if they are of the same type.
*/
    Connector *e1, *e2;

    e1 = d1->left;
    e2 = d2->left;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connector_types_equal(e1,e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    e1 = d1->right;
    e2 = d2->right;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connector_types_equal(e1,e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    return TRUE;
}

char * intersect_strings(Sentence sent, char * s, char * t) {
/* This returns a string that is the the GCD of the two given strings.
   If the GCD is equal to one of them, a pointer to it is returned.
   Otherwise a new string for the GCD is xalloced and put on the
   "free later" list.
*/
    int len, i, j, d;
    char * u, *u0, *s0;
    if (strcmp(s,t)==0) return s;  /* would work without this */
    i = strlen(s);
    j = strlen(t);
    if (j > i) {
	u = s; s = t; t = u;
	len = j;
    } else {
	len = i;
    }
    /* s is now the longer (at least not the shorter) string */
    /* and len is its length */
    u0 = u = (char *) xalloc(len+1);
    d = 0;
    s0 = s;
    while (*t != '\0') {
	if ((*s == *t) || (*t == '*')) {
	    *u = *s;
	} else {
	    d++;
	    if (*s == '*') *u = *t;
	    else *u = '^';
	}
	s++; t++; u++;
    }
    if (d==0) {
	xfree(u0, len+1);
	return s0;
    } else {
	strcpy(u, s);   /* get the remainder of s */
	u = string_set_add(u0, sent->string_set);
	xfree(u0, len+1);
	return u;
    }
}

int connectors_equal_AND(Connector *c1, Connector *c2) {
/* Two connectors are said to be equal if they are of the same type
   (defined above), they have the same multi field, and they have
   exactly the same connectors (including lower case chars).
   (priorities ignored).
*/
    return (c1->label == c2->label) &&
	   (c1->multi == c2->multi) && 
           (strcmp(c1->string, c2->string) == 0);
}


int disjuncts_equal_AND(Disjunct * d1, Disjunct * d2) {
/* Return true if the disjuncts are equal (ignoring priority fields)
   and the string of the disjunct.
*/
    Connector *e1, *e2;
    STAT_calls_to_equality_test++;
    e1 = d1->left;
    e2 = d2->left;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connectors_equal_AND(e1, e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    e1 = d1->right;
    e2 = d2->right;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connectors_equal_AND(e1, e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    return TRUE;
}

Disjunct * intersect_disjuncts(Sentence sent, Disjunct * d1, Disjunct * d2) {
/* Create a new disjunct that is the GCD of d1 and d2.
   It assumes that the disjuncts are of the same type, so the
   GCD will not be empty.
*/
    Disjunct * d;
    Connector *c1, *c2, *c;
    d = copy_disjunct(d1);
    c = d->left;
    c1 = d1->left;
    c2 = d2->left;
    while (c1!=NULL) {
	c->string = intersect_strings(sent, c1->string, c2->string);
	c->multi = (c1->multi) && (c2->multi);
	c = c->next; c1 = c1->next; c2 = c2->next;
    }
    c = d->right;
    c1 = d1->right;
    c2 = d2->right;
    while (c1!=NULL) {
	c->string = intersect_strings(sent, c1->string, c2->string);
	c->multi = (c1->multi) && (c2->multi);
	c = c->next; c1 = c1->next; c2 = c2->next;
    }
    return d;
}

void put_disjunct_into_table(Sentence sent, Disjunct *d) {
/* (1) look for the given disjunct in the table structures
       if it's already in the table structures, do nothing
   (2) otherwise make a copy of it, and put it into the table structures
   (3) also put all of the GCDs of this disjunct with all of the
       other matching disjuncts into the table.

   The costs are set to zero.
   Note that this has no effect on disjunct d.
*/
    Disjunct *d1=NULL, *d2, *di, *d_copy;
    Label_node * lp;
    int h, k;

    h = and_hash_disjunct(d);

    for (lp = sent->and_data.hash_table[h]; lp != NULL; lp = lp->next) {
	d1 = sent->and_data.label_table[lp->label];
	if (disjunct_types_equal(d,d1)) break;
    }
    if (lp != NULL) {
	/* there is already a label for disjuncts of this type */
	/* d1 points to the list of disjuncts of this type already there */
	while(d1 != NULL) {
	    if (disjuncts_equal_AND(d1, d)) return;
	    d1 = d1->next;
	}
	/* now we must put the d disjunct in there, and all of the GCDs of
           it with the ones already there.

	   This is done as follows.  We scan through the list of disjuncts
           computing the gcd of the new one with each of the others, putting
           the resulting disjuncts onto another list rooted at d2.
	   Now insert d into the the list already there.  Now for each
           one on the d2 list, put it in if it isn't already there.

	   Here we're making use of the following theorem: Given a
           collection of sets s1, s2 ... sn closed under intersection,
	   to if we add a new set s to the collection and also add
           all the intersections between s and s1...sn to the collection,
           then the collection is still closed under intersection.

	   Use a Venn diagram to prove this theorem.

	*/
	d_copy = copy_disjunct(d);
	d_copy->cost = 0;
	k = lp->label;
	d2 = NULL;
	for (d1=sent->and_data.label_table[k]; d1!=NULL; d1 = d1->next) {
	    di = intersect_disjuncts(sent, d_copy, d1);
	    di->next = d2;
	    d2 = di;
	}
	d_copy->next = sent->and_data.label_table[k];
	sent->and_data.label_table[k] = d_copy;
	for (;d2 != NULL; d2 = di) {
	    di = d2->next;
	    for (d1 = sent->and_data.label_table[k]; d1 != NULL; d1 = d1->next) {
		if (disjuncts_equal_AND(d1, d2)) break;
	    }
	    if (d1 == NULL) {
		STAT_N_disjuncts++;
		d2->next = sent->and_data.label_table[k];
		sent->and_data.label_table[k] = d2;
	    } else {
		d2->next = NULL;
		free_disjuncts(d2);
	    }
	}
    } else {
	/* create a new label for disjuncts of this type */
	d_copy = copy_disjunct(d);
	d_copy->cost = 0;
	d_copy->next = NULL;
	if (sent->and_data.LT_size == sent->and_data.LT_bound) grow_LT(sent);
	lp = (Label_node *) xalloc(sizeof(Label_node));
	lp->next = sent->and_data.hash_table[h];
	sent->and_data.hash_table[h] = lp;
	lp->label = sent->and_data.LT_size;
	sent->and_data.label_table[sent->and_data.LT_size] = d_copy;
	sent->and_data.LT_size++;
	STAT_N_disjuncts++;
    }
}


void extract_all_fat_links(Sentence sent, Disjunct * d) {
/*  A sub disjuct of d is any disjunct obtained by killing the tail
    of either connector list at any point.
    Here we go through each sub-disjunct of d, and put it into our
    table data structure.

    The function has no side effects on d.
 */
    Connector * cl, * cr, *tl, *tr;
    tl = d->left;
    d->left = NULL;
    for (cr = d->right; cr!=NULL; cr = cr->next) {
	tr = cr->next;
	cr->next = NULL;
	if (is_appropriate(sent, d)) put_disjunct_into_table(sent, d);
	cr->next = tr;
    }
    d->left = tl;

    tr = d->right;
    d->right = NULL;
    for (cl = d->left; cl!=NULL; cl = cl->next) {
	tl = cl->next;
	cl->next = NULL;
	if (is_appropriate(sent, d)) put_disjunct_into_table(sent, d);
	cl->next = tl;
    }
    d->right = tr;

    for (cl = d->left; cl!=NULL; cl = cl->next) {
	for (cr = d->right; cr!=NULL; cr = cr->next) {
	    tl = cl->next;
	    tr = cr->next;
	    cl->next = cr->next = NULL;

	    if (is_appropriate(sent, d)) put_disjunct_into_table(sent, d);

	    cl->next = tl;
	    cr->next = tr;
	}
    }
}

char * stick_in_one_connector(char *s, Connector *c, int len) {
/* put the next len characters from c->string (skipping upper
   case ones) into s.  If there are fewer than this, pad with '*'s.
   Then put in a character for the multi match bit of c.
   Then put in a '\0', and return a pointer to this place.
*/
    char * t;
    for (t = c->string; isupper((int)*t); t++)
      ;
    while (*t != '\0') {
	*s++ = *t++;
	len--;
    }
    while (len > 0) {
	*s++ = '*';
	len--;
    }
    if (c->multi) *s++ = '*'; else *s++ = '^';  /* check this sometime */
    *s = '\0';
    return s;
}

void compute_matchers_for_a_label(Sentence sent, int k) {
/* This takes a label k, modifies the list of disjuncts with that
   label.  For each such disjunct, it computes the string that
   will be used in the fat connector that represents it.

   The only hard part is finding the length of each of the strings
   so that "*" can be put in.  A better explanation will have to wait.
*/   

    int * lengths;
    int N_connectors, i, j, tot_len;
    Connector * c;
    Disjunct * d;
    char *s, *os;

    d = sent->and_data.label_table[k];

    N_connectors = 0;
    for (c=d->left; c != NULL; c = c->next) N_connectors ++;
    for (c=d->right; c != NULL; c = c->next) N_connectors ++;

    lengths = (int *) xalloc(N_connectors*sizeof(int));
    for (i=0; i<N_connectors; i++) lengths[i] = 0;
    while(d != NULL) {
	i = 0;
	for (c=d->left; c != NULL; c = c->next) {
	    s = c->string;
	    while(isupper((int)*s)) s++;
	    j = strlen(s);
	    if (j > lengths[i]) lengths[i] = j;
	    i++;
	}
	for (c=d->right; c != NULL; c = c->next) {
	    s = c->string;
	    while(isupper((int)*s)) s++;
	    j = strlen(s);
	    if (j > lengths[i]) lengths[i] = j;
	    i++;
	}
	d = d->next;
    }

    tot_len = 0;
    for (i=0; i<N_connectors; i++) tot_len += lengths[i]+1;
                     /* +1 is for the multi-match character */
    for (d = sent->and_data.label_table[k]; d!= NULL; d = d->next) {
	i=0;
	os = s = (char *) xalloc(tot_len+1);
	for (c=d->left; c != NULL; c = c->next) {
	    s = stick_in_one_connector(s, c, lengths[i]);
	    i++;
	}
	for (c=d->right; c != NULL; c = c->next) {
	    s = stick_in_one_connector(s, c, lengths[i]);
	    i++;
	}
	s = string_set_add(os, sent->string_set);
	xfree(os, tot_len+1);
	d->string = s;
    }
    xfree((char *)lengths, N_connectors*sizeof(int));
}

void build_conjunction_tables(Sentence sent) {
/* Goes through the entire sentence and builds the fat link tables
   for all the disjuncts of all the words.
*/
    int w;
    int k;
    Disjunct * d;

    init_HT(sent);
    init_LT(sent);
    STAT_N_disjuncts = STAT_calls_to_equality_test = 0;

    for (w=0; w<sent->length; w++) {
	for (d=sent->word[w].d; d!=NULL; d=d->next) {
	    extract_all_fat_links(sent, d);
	}
    }

    for (k=0; k<sent->and_data.LT_size; k++) {
	compute_matchers_for_a_label(sent, k);
    }
}

void print_AND_statistics(Sentence sent) {
    printf("Number of disjunct types (labels): %d\n", sent->and_data.LT_size);
    printf("Number of disjuncts in the table: %d\n", STAT_N_disjuncts);
    if (sent->and_data.LT_size != 0) {
      printf("average list length: %f\n",
	     (float)STAT_N_disjuncts/sent->and_data.LT_size);
    }
    printf("Number of equality tests: %d\n", STAT_calls_to_equality_test);
}

void connector_for_disjunct(Sentence  sent, Disjunct * d, Connector * c) {
/* Fill in the fields of c for the disjunct.  This must be in
   the table data structures.  The label field and the string field
   are filled in appropriately.  Priority is set to UP_priority.
*/
    int h;
    Disjunct * d1=NULL;
    Label_node * lp;

    h = and_hash_disjunct(d);

    for (lp = sent->and_data.hash_table[h]; lp != NULL; lp = lp->next) {
	d1 = sent->and_data.label_table[lp->label];
	if (disjunct_types_equal(d,d1)) break;
    }
    assert(lp!=NULL, "A disjunct I inserted was not there. (1)");
/*
  I don't know what these lines were for.  I replaced them by
  the above assertion.
    if (lp == NULL) {
	printf("error: A disjunct I inserted was not there\n");
        lp = lp->next;  (to force an error)
    }
*/
    while(d1 != NULL) {
	if (disjuncts_equal_AND(d1, d)) break;
	d1 = d1->next;
    }

    assert(d1!=NULL, "A disjunct I inserted was not there. (2)");
    
    c->label = lp->label;
    c->string = d1->string;
    c->priority = UP_priority;
    c->multi = FALSE;
}


Disjunct * build_fat_link_substitutions(Sentence sent, Disjunct *d) {
/* This function allocates and returns a list of disjuncts.
   This is the one obtained by substituting each contiguous
   non-empty subrange of d (incident on the center) by an appropriate
   fat link, in two possible positions.  Does not effect d.
   The cost of d is inherited by all of the disjuncts in the result.
*/
    Connector * cl, * cr, *tl, *tr, *wc, work_connector;
    Disjunct *d1, *wd, work_disjunct, *d_list;
    if (d==NULL) return NULL;
    wd = & work_disjunct;
    wc = init_connector(& work_connector);
    d_list = NULL;
    *wd = *d;
    tl = d->left;
    d->left = NULL;
    for (cr = d->right; cr!=NULL; cr = cr->next) {
	tr = cr->next;
	cr->next = NULL;
	if (is_appropriate(sent, d)) {
	    connector_for_disjunct(sent, d, wc);
	    wd->left = tl;
	    wd->right = wc;
	    wc->next = tr;
	    d1 = copy_disjunct(wd);
	    d1->next = d_list;
	    d_list = d1;
	    wd->left = wc;
	    wc->next = tl;
	    wd->right = tr;
	    d1 = copy_disjunct(wd);
	    d1->next = d_list;
	    d_list = d1;
	}
	cr->next = tr;
    }
    d->left = tl;
    
    tr = d->right;
    d->right = NULL;
    for (cl = d->left; cl!=NULL; cl = cl->next) {
	tl = cl->next;
	cl->next = NULL;
	if (is_appropriate(sent, d)) {
	    connector_for_disjunct(sent, d, wc);
	    wd->left = tl;
	    wd->right = wc;
	    wc->next = tr;
	    d1 = copy_disjunct(wd);
	    d1->next = d_list;
	    d_list = d1;
	    wd->left = wc;
	    wc->next = tl;
	    wd->right = tr;
	    d1 = copy_disjunct(wd);
	    d1->next = d_list;
	    d_list = d1;
	}
	cl->next = tl;
    }
    d->right = tr;
    
    for (cl = d->left; cl!=NULL; cl = cl->next) {
	for (cr = d->right; cr!=NULL; cr = cr->next) {
	    tl = cl->next;
	    tr = cr->next;
	    cl->next = cr->next = NULL;
	    if (is_appropriate(sent, d)) {
		connector_for_disjunct(sent, d, wc);
		wd->left = tl;
		wd->right = wc;
		wc->next = tr;
		d1 = copy_disjunct(wd);
		d1->next = d_list;
		d_list = d1;
		wd->left = wc;
		wc->next = tl;
		wd->right = tr;
		d1 = copy_disjunct(wd);
		d1->next = d_list;
		d_list = d1;
	    }
	    cl->next = tl;
	    cr->next = tr;
	}
    }
    return d_list;
}

Disjunct * explode_disjunct_list(Sentence sent, Disjunct *d) {
/*  This is basically a "map" function for build_fat_link_substitutions.
     It's applied to the disjuncts for all regular words of the sentence.
*/
   Disjunct *d1;

   d1 = NULL;

   for (; d!=NULL; d = d->next) {
       d1 = catenate_disjuncts(d1, build_fat_link_substitutions(sent, d));
   }
   return d1;
}

Disjunct * build_COMMA_disjunct_list(Sentence sent) {
/*  Builds and returns a disjunct list for the comma.  These are the
    disjuncts that are used when "," operates in conjunction with "and".
    Does not deal with the ", and" issue, nor the other uses
    of comma.
*/
    int lab;
    Disjunct *d1, *d2, *d, work_disjunct, *wd;
    Connector work_connector1, work_connector2, *c1, *c2;
    Connector work_connector3, *c3;
    c1 = init_connector(& work_connector1);
    c2 = init_connector(& work_connector2);
    c3 = init_connector(& work_connector3);
    wd = & work_disjunct;
    
    d1 = NULL;  /* where we put the list we're building */
    
    c1->next = NULL;
    c2->next = c3;
    c3->next = NULL;
    c1->priority = c3->priority = DOWN_priority;
    c2->priority = UP_priority;
    c1->multi = c2->multi = c3->multi = FALSE;
    wd->left = c1;
    wd->right = c2;
    wd->string = ",";  /* *** fix this later?? */
    wd->next = NULL;
    wd->cost = 0;
    for (lab = 0; lab < sent->and_data.LT_size; lab++) {
	for (d = sent->and_data.label_table[lab]; d!=NULL; d=d->next) {
	    c1->string = c2->string = c3->string = d->string;
	    c1->label = c2->label = c3->label = lab;
	    d2 = copy_disjunct(wd);
	    d2->next = d1;
	    d1 = d2;
	}
    }
    return d1;
}

Disjunct * build_AND_disjunct_list(Sentence sent, char * s) {
 /* Builds and returns a disjunct list for "and", "or" and "nor" */
 /* for each disjunct in the label_table, we build three disjuncts */
 /* this means that "Danny and Tycho and Billy" will be parsable in */
 /* two ways.  I don't know an easy way to avoid this */    
 /* the string is either "and", or "or", or "nor" at the moment */

    int lab;
    Disjunct *d_list, *d1, *d3, *d, *d_copy;
    Connector *c1, *c2, *c3;
    
    d_list = NULL;  /* where we put the list we're building */
    
    for (lab = 0; lab < sent->and_data.LT_size; lab++) {
	for (d = sent->and_data.label_table[lab]; d!=NULL; d=d->next) {
	    d1 = build_fat_link_substitutions(sent, d);
	    d_copy = copy_disjunct(d);  /* also include the thing itself! */
	    d_copy->next = d1;
	    d1 = d_copy;
	    for(;d1 != NULL; d1 = d3) {
		d3 = d1->next;

		c1 = init_connector((Connector *) xalloc(sizeof(Connector)));
		c2 = init_connector((Connector *) xalloc(sizeof(Connector)));
		c1->next = NULL;
		c2->next = NULL;
		c1->priority = c2->priority = DOWN_priority;
		c1->multi = c2->multi = FALSE;
		c1->string = c2->string = d->string;
		c1->label = c2->label = lab;
	    
		d1->string = s;
	    
		if (d1->right == NULL) {
		    d1->right = c2;
		} else {
		    for (c3=d1->right; c3->next != NULL; c3 = c3->next)
		      ;
		    c3->next = c2;
		}
		if (d1->left == NULL) {
		    d1->left = c1;
		} else {
		    for (c3=d1->left; c3->next != NULL; c3 = c3->next)
		      ;
		    c3->next = c1;
		}
		d1->next = d_list;
		d_list = d1;
	    }
	}
    }
#if defined(PLURALIZATION)
    /* here is where "and" makes singular into plural. */
    /* must accommodate "he and I are good", "Davy and I are good"
       "Danny and Davy are good", and reject all of these with "is"
       instead of "are".

       The SI connectors must also be modified to accommodate "are John
       and Dave here", but kill "is John and Dave here"
    */
    if (strcmp(s,"and")==0) {
	for (d1 = d_list; d1!=NULL; d1=d1->next) {
	    for (c1=d1->right; c1!=NULL; c1=c1->next) {
		if ((c1->string[0] == 'S') &&
		    ((c1->string[1]=='^') ||
		     (c1->string[1]=='s') ||
		     (c1->string[1]=='p') ||
		     (c1->string[1]=='\0'))) {
		    c1->string = "Sp";
		}
	    }
	    for (c1=d1->left; c1!=NULL; c1=c1->next) {
		if ((c1->string[0] == 'S') && (c1->string[1]=='I') &&
		    ((c1->string[2]=='^') ||
		     (c1->string[2]=='s') ||
		     (c1->string[2]=='p') ||
		     (c1->string[2]=='\0'))) {
		    c1->string = "SIp";
		}
	    }
	}
    } 
/*
  "a cat or a dog is here"  vs  "a cat or a dog are here"
  The first seems right, the second seems wrong.  I'll stick with this.

  That is, "or" has the property that if both parts are the same in
  number,  we use that but if they differ, we use plural.

  The connectors on "I" must be handled specially.  We accept
  "I or the dogs are here" but reject "I or the dogs is here"
*/

/* the code here still does now work "right", rejecting "is John or I invited"
   and accepting "I or my friend know what happened"

   The more generous code for "nor" has been used instead
*/   
/*
    else if (strcmp(s,"or")==0) {
	for (d1 = d_list; d1!=NULL; d1=d1->next) {
	    for (c1=d1->right; c1!=NULL; c1=c1->next) {
		if (c1->string[0] == 'S') {
		    if (c1->string[1]=='^') {
			if (c1->string[2]=='a') {
			    c1->string = "Ss"; 
			} else {
			    c1->string = "Sp";
			}
		    } else if ((c1->string[1]=='p') && (c1->string[2]=='a')){
			c1->string = "Sp";
		    }
		}
	    }
	    for (c1=d1->left; c1!=NULL; c1=c1->next) {
		if ((c1->string[0] == 'S') && (c1->string[1] == 'I')) {
		    if (c1->string[2]=='^') {
			if (c1->string[3]=='a') {
			    c1->string = "Ss"; 
			} else {
			    c1->string = "Sp";
			}
		    } else if ((c1->string[2]=='p') && (c1->string[3]=='a')){
			c1->string = "Sp";
		    }
		}
	    }
	}
    }
*/    
/*
    It appears that the "nor" of two things can be either singular or
    plural.  "neither she nor John likes dogs"
             "neither she nor John like dogs"

*/
    else if ((strcmp(s,"nor")==0) || (strcmp(s,"or")==0)) {
	for (d1 = d_list; d1!=NULL; d1=d1->next) {
	    for (c1=d1->right; c1!=NULL; c1=c1->next) {
		if ((c1->string[0] == 'S') &&
		    ((c1->string[1]=='^') ||
		     (c1->string[1]=='s') ||
		     (c1->string[1]=='p'))) {
		    c1->string = "S";		
		}
	    }
	    for (c1=d1->left; c1!=NULL; c1=c1->next) {
		if ((c1->string[0] == 'S') && (c1->string[1] == 'I') &&
		    ((c1->string[2]=='^') ||
		     (c1->string[2]=='s') ||
		     (c1->string[2]=='p'))) {
		    c1->string = "SI";		
		}
	    }
	}
    }

#endif    
    return d_list;
}


/* The following routines' purpose is to eliminate all but the
   canonical linkage (of a collection of linkages that are identical
   except for fat links).  An example of the problem is
   "I went to a talk and ate lunch".  Without the canonical checker
   this has two linkages with identical structure.

   We restrict our attention to a collection of linkages that are all
   isomorphic.  Consider the set of all disjuncts that are used on one
   word (over the collection of linkages).  This set is closed under GCD,
   since two linkages could both be used in that position, then so could
   their GCD.  The GCD has been constructed and put in the label table.

   The canonical linkage is the one in which the minimal disjunct that
   ever occurrs in a position is used in that position.  It is easy to
   prove that a disjunct is not canonical -- just find one of it's fat
   disjuncts that can be replaced by a smaller one.  If this can not be
   done, then the linkage is canonical.

   The algorithm uses link_array[] and chosen_disjuncts[] as input to
   describe the linkage, and also uses the label_table.

   (1) find all the words with fat disjuncts
   (2) scan all links and build, for each fat disjucnt used,
        an "image" structure that contains what this disjunct must
	connect to in the rest of the linkage.
   (3) For each fat disjunct, run through the label_table for disjuncts
        with the same label, considering only those with strictly more
        restricted match sets (this uses the string fields of the disjuncts
	from the table).
   (4) For each that passes this test, we see if it can replace the chosen
        disjunct.  This is performed by examining how this disjunct
        compares with the image structure for this word.
*/

typedef struct Image_node_struct Image_node;
struct Image_node_struct {
    Image_node * next;
    Connector * c;  /* the connector the place on the disjunct must match */
    int place;      /* Indicates the place in the fat disjunct where this
		       connector must connect.  If 0 then this is a fat
		       connector.  If >0 then go place to the right, if
                       <0 then go -place to the left. */
};

static Image_node * image_array[MAX_SENTENCE];
/* points to the image structure for eacch word.  NULL if not a fat word. */

static char has_fat_down[MAX_SENTENCE];  /* TRUE if this word has a fat down link
				     FALSE otherise */
int set_has_fat_down(Sentence sent) {
/* Fill in the has_fat_down array.  Uses link_array[].
   Returns TRUE if there exists at least one word with a
   fat down label.
*/
    int link, w, N_fat;
    Parse_info * pi = sent->parse_info;

    N_fat = 0;

    for (w=0; w<pi->N_words; w++) {
	has_fat_down[w] = FALSE;
    }
    
    for (link=0; link<pi->N_links; link++) {
	if (pi->link_array[link].lc->priority == DOWN_priority) {
	    N_fat ++;
	    has_fat_down[pi->link_array[link].l] = TRUE;
	} else if (pi->link_array[link].rc->priority == DOWN_priority) {
	    N_fat ++;
	    has_fat_down[pi->link_array[link].r] = TRUE;
	}
    }
    return (N_fat > 0);
}

void free_image_array(Parse_info * pi) {
    int w;
    Image_node * in, * inx;
    for (w=0; w<pi->N_words; w++) {
	for (in=image_array[w]; in!=NULL; in=inx) {
	    inx = in->next;
	    xfree((char *)in, sizeof(Image_node));
	}
    }
}	    
    
void build_image_array(Sentence sent) {
/* uses link_array, chosen_disjuncts, and down_label to construct
   image_array */
    int link, end, word;
    Connector * this_end_con, *other_end_con, * upcon, * updiscon, *clist;
    Disjunct * dis, * updis;
    Image_node * in;
    Parse_info * pi = sent->parse_info;

    for (word=0; word<pi->N_words; word++) {
	image_array[word] = NULL;
    }

    for (end = -1; end <= 1; end += 2) {
	for (link=0; link<pi->N_links; link++) {
	    if (end<0) {
		word = pi->link_array[link].l;
		if (!has_fat_down[word]) continue;
		this_end_con = pi->link_array[link].lc;
		other_end_con = pi->link_array[link].rc;
		dis = pi->chosen_disjuncts[word];
		clist = dis->right;
	    } else {
		word =pi->link_array[link].r;
		if (!has_fat_down[word]) continue;
		this_end_con =pi->link_array[link].rc;
		other_end_con =pi->link_array[link].lc;
		dis = pi->chosen_disjuncts[word];
		clist = dis->left;
	    }

	    if (this_end_con->priority == DOWN_priority) continue;
	    if ((this_end_con->label != NORMAL_LABEL) &&
		(this_end_con->label < 0)) continue;
	    /* no need to construct an image node for down links,
	       or commas links or either/neither links */

	    in = (Image_node *) xalloc(sizeof(Image_node));
	    in->next = image_array[word];
	    image_array[word] = in;
	    in->c = other_end_con;
	    /* the rest of this code is for computing in->place */
	    if (this_end_con->priority == UP_priority) {
		in->place = 0;
	    } else {
		in->place = 1;
		if ((dis->left != NULL) &&
		    (dis->left->priority == UP_priority)) {
		    upcon = dis->left;
		} else if ((dis->right != NULL) &&
			   (dis->right->priority == UP_priority)) {
		    upcon = dis->right;
		} else {
		    upcon = NULL;
		}
		if (upcon != NULL) { /* add on extra for a fat up link */
		    updis = sent->and_data.label_table[upcon->label];
		    if (end > 0) {
			updiscon = updis->left;
		    } else {
			updiscon = updis->right;
		    }
		    for (;updiscon != NULL; updiscon = updiscon->next) {
			in->place ++;
		    }
		}
		for (; clist != this_end_con; clist = clist->next) {
		    if (clist->label < 0) in->place++;
		}
		in->place = in->place * (-end);
	    }
	}
    }
}

int strictly_smaller(char * s, char * t) {
/* returns TRUE if string s represents a strictly smaller match set
   than does t
*/
    int strictness;
    strictness = 0;
    for (;(*s!='\0') && (*t!='\0'); s++,t++) {
	if (*s == *t) continue;
	if ((*t == '*') || (*s == '^')) {
	    strictness++;
	} else {
	    return FALSE;
	}
    }
    assert(! ((*s!='\0') || (*t!='\0')), "s and t should be the same length!");
    return (strictness > 0);  
}

Disjunct * find_subdisjunct(Sentence sent, Disjunct * dis, int label) {
/* dis points to a disjunct in the label_table.  label is the label
   of a different set of disjuncts.  These can be derived from the label
   of dis.  Find the specific disjunct of in label_table[label]
   which corresponds to dis.
*/
    Disjunct * d;
    Connector * cx, *cy;
    for (d=sent->and_data.label_table[label]; d!=NULL; d=d->next) {
	for (cx=d->left, cy=dis->left; cx!=NULL; cx=cx->next,cy=cy->next) {
/*	    if ((cx->string != cy->string) || */
	    if ((strcmp(cx->string, cy->string) != 0) ||
		(cx->multi != cy->multi)) break;/* have to check multi? */
	}
	if (cx!=NULL) continue;
	for (cx=d->right, cy=dis->right; cx!=NULL; cx=cx->next,cy=cy->next) {
/*	    if ((cx->string != cy->string) || */
	    if ((strcmp(cx->string, cy->string) != 0) ||
		(cx->multi != cy->multi)) break;
	}
	if (cx==NULL) break;
    }
    assert(d!=NULL, "Never found subdisjunct");
    return d;
}

int is_canonical_linkage(Sentence sent) {
/*
   This uses link_array[], chosen_disjuncts[], has_fat_down[].
   It assumes that there is a fat link in the current linkage.
   See the comments above for more information about how it works
*/

    int w, d_label=0, place;
    Connector *d_c, *c, dummy_connector, *upcon;
    Disjunct *dis, *chosen_d;
    Image_node * in;
    Parse_info * pi = sent->parse_info;

    dummy_connector.priority = UP_priority;
    init_connector(&dummy_connector);

    build_image_array(sent);

    for (w=0; w<pi->N_words; w++) {
	if (!has_fat_down[w]) continue;
	chosen_d = pi->chosen_disjuncts[w];

        /* there must be a down connector in both the left and right list */
	for (d_c = chosen_d->left; d_c!=NULL; d_c=d_c->next) {
	    if (d_c->priority == DOWN_priority) {
		d_label = d_c->label;
		break;
	    }
	}
	assert(d_c != NULL, "Should have found the down link.");

	if ((chosen_d->left != NULL) &&
	    (chosen_d->left->priority == UP_priority)) {
	    upcon = chosen_d->left;
	} else if ((chosen_d->right != NULL) &&
		   (chosen_d->right->priority == UP_priority)) {
	    upcon = chosen_d->right;
	} else {
	    upcon = NULL;
	}
	
	/* check that the disjunct on w is minimal (canonical) */
	
	for (dis=sent->and_data.label_table[d_label]; dis!=NULL; dis=dis->next) {
	    
	    /* now, reject a disjunct if it's not strictly below the old */
	    if(!strictly_smaller(dis->string, d_c->string)) continue;

	    /* Now, it has to match the image connectors */

	    for (in=image_array[w]; in!=NULL; in=in->next) {

		place = in->place;
		if (place == 0) {
	
		    assert(upcon != NULL, "Should have found an up link");
		    dummy_connector.label = upcon->label;

		    /* now we have to compute the string of the
		       disjunct with upcon->label that corresponds
		       to dis  */

		    if (upcon->label == d_label) {
			dummy_connector.string = dis->string;
		    } else {
			dummy_connector.string =
			  find_subdisjunct(sent, dis, upcon->label)->string;
		    }
		    if (!x_match(&dummy_connector, in->c)) break;  /* I hope using x_match here is right */
		} else if (place > 0) {
		    for (c=dis->right; place > 1; place--) {
			c = c->next;
		    }
		    if (!x_match(c, in->c)) break;    /* Ditto above comment  --DS 07/97*/
		} else {
		    for (c=dis->left; place < -1; place++) {
			c = c->next;
		    }
		    if (!x_match(c, in->c)) break;  /* Ditto Ditto */
		}
	    }

	    if (in == NULL) break;
	}
	if (dis != NULL) break;
	/* there is a better disjunct that the one we're using, so this
	   word is bad, so we're done */
    }
    free_image_array(pi);
    return (w == pi->N_words);
}

void compute_pp_link_array_connectors(Sentence sent, Sublinkage *sublinkage)
{
/*
   This takes as input link_array[], sublinkage->link[]->l and
   sublinkage->link[]->r (and also has_fat_down[word], which has been
   computed in a prior call to is_canonical()), and from these
   computes sublinkage->link[].lc and .rc.  We assume these have
   been initialized with the values from link_array.  We also assume
   that there are fat links.
*/
    int link, end, word, place;
    Connector * this_end_con, * upcon, * updiscon, *clist, *con, *mycon;
    Disjunct * dis, * updis, *mydis;
    Parse_info * pi = sent->parse_info;

    for (end = -1; end <= 1; end += 2) {
	for (link=0; link<pi->N_links; link++) {
	    if (sublinkage->link[link]->l == -1) continue;
	    if (end<0) {
		word = pi->link_array[link].l;
		if (!has_fat_down[word]) continue;
		this_end_con = pi->link_array[link].lc;
		dis = pi->chosen_disjuncts[word];
		mydis = pi->chosen_disjuncts[sublinkage->link[link]->l];
		clist = dis->right;
	    } else {
		word = pi->link_array[link].r;
		if (!has_fat_down[word]) continue;
		this_end_con = pi->link_array[link].rc;
		dis = pi->chosen_disjuncts[word];
		mydis = pi->chosen_disjuncts[sublinkage->link[link]->r];
		clist = dis->left;
	    }

	    if (this_end_con->label != NORMAL_LABEL) continue;
	    /* no need to construct a connector for up links,
	       or commas links or either/neither links */

	    /* Now compute the place */
	    place = 0;
	    if ((dis->left != NULL) &&
		(dis->left->priority == UP_priority)) {
		upcon = dis->left;
	    } else if ((dis->right != NULL) &&
		       (dis->right->priority == UP_priority)) {
		upcon = dis->right;
	    } else {
		upcon = NULL;
	    }
	    if (upcon != NULL) { /* add on extra for a fat up link */
		updis = sent->and_data.label_table[upcon->label];
		if (end > 0) {
		    updiscon = updis->left;
		} else {
		    updiscon = updis->right;
		}
		for (;updiscon != NULL; updiscon = updiscon->next) {
		    place ++;
		}
	    }
	    for (; clist != this_end_con; clist = clist->next) {
		if (clist->label < 0) place++;
	    }
	    /* place has just been computed */

	    /* now find the right disjunct in the table */
	    if ((mydis->left != NULL) &&
		(mydis->left->priority == UP_priority)) {
		mycon = mydis->left;
	    } else if ((mydis->right != NULL) &&
		       (mydis->right->priority == UP_priority)) {
		mycon = mydis->right;
	    } else {
		printf("word = %d\n", word);
		printf("fat link: [%d, %d]\n", 
		       pi->link_array[link].l, pi->link_array[link].r);
		printf("thin link: [%d, %d]\n", 
		       sublinkage->link[link]->l, sublinkage->link[link]->r);
		assert(FALSE, "There should be a fat UP link here");
	    }

	    for (dis=sent->and_data.label_table[mycon->label]; 
		 dis != NULL; dis=dis->next) {
		if (dis->string == mycon->string) break;
	    }
	    assert(dis!=NULL, "Should have found this connector string");
	    /* the disjunct in the table has just been found */

	    if (end < 0) {
	      for (con = dis->right; place > 0; place--, con=con->next);
		/* sublinkage->link[link]->lc = con; OLD CODE */
	      exfree_connectors(sublinkage->link[link]->lc);
	      sublinkage->link[link]->lc = excopy_connectors(con);
	    } else {
		for (con = dis->left; place > 0; place--, con=con->next);
		/* sublinkage->link[link]->rc = con; OLD CODE */
		exfree_connectors(sublinkage->link[link]->rc);
		sublinkage->link[link]->rc = excopy_connectors(con);
	    }
	}
    }
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/***************************************************************
*
* Routines for setting Parse_Options
*
****************************************************************/

void          free_sentence_disjuncts(Sentence sent);
void          free_post_processing(Sentence sent);


int VDAL_compare_parse(Linkage_info * p1, Linkage_info * p2) {
    /* for sorting the linkages in postprocessing */
    if (p1->N_violations != p2->N_violations) {
	return (p1->N_violations - p2->N_violations);
    } 
    else if (p1->unused_word_cost != p2->unused_word_cost) {
	return (p1->unused_word_cost - p2->unused_word_cost);      
    }
    else if (p1->disjunct_cost != p2->disjunct_cost) {
	return (p1->disjunct_cost - p2->disjunct_cost);
    } 
    else if (p1->and_cost != p2->and_cost) {
	return (p1->and_cost - p2->and_cost);
    } 
    else {
	return (p1->link_cost - p2->link_cost);
    }
}

Parse_Options parse_options_create() {
  /* create and initialize a Parse_Options object */
    Parse_Options po;

    po = (Parse_Options) xalloc(sizeof(struct Parse_Options_s));

    /* Here's where the values are initialized */
    po->verbosity     = 1;
    po->linkage_limit = 100;
    po->disjunct_cost = MAX_DISJUNCT_COST;
    po->min_null_count = 0;
    po->max_null_count = 0;
    po->null_block = 1;
    po->islands_ok = FALSE;
    po->cost_model.compare_fn = &VDAL_compare_parse;
    po->cost_model.type       = VDAL;
    po->short_length = 6;
    po->all_short = FALSE;
    po->twopass_length = 30;
    po->max_sentence_length = 70;
    po->resources = resources_create();
    po->display_short = TRUE;
    po->display_word_subscripts = TRUE;
    po->display_link_subscripts = TRUE;
    po->display_walls = FALSE;
    po->display_union = FALSE;
    po->allow_null = TRUE;
    po->echo_on = FALSE;
    po->batch_mode = FALSE;
    po->panic_mode = FALSE;
    po->screen_width = 79;
    po->display_on = TRUE;
    po->display_postscript = FALSE;
    po->display_constituents = 0;
    po->display_bad = FALSE;
    po->display_links = FALSE;

    return po;
}

int parse_options_delete(Parse_Options  opts) {
    resources_delete(opts->resources);
    xfree(opts, sizeof(struct Parse_Options_s));
    return 0;
}

void parse_options_set_cost_model_type(Parse_Options opts, int cm) {
    switch(cm) {
    case VDAL:
	opts->cost_model.type = VDAL;
	opts->cost_model.compare_fn = &VDAL_compare_parse;
	break;
    default:
	error("Illegal cost model: %d\n", cm);
    }
}

void parse_options_set_verbosity(Parse_Options opts, int dummy) {
    opts->verbosity = dummy;
    verbosity = opts->verbosity;  
    /* this is one of the only global variables. */
}

int parse_options_get_verbosity(Parse_Options opts) {
    return opts->verbosity;
}

void parse_options_set_linkage_limit(Parse_Options opts, int dummy) {
    opts->linkage_limit = dummy;
}
int parse_options_get_linkage_limit(Parse_Options opts) {
    return opts->linkage_limit;
}

void parse_options_set_disjunct_cost(Parse_Options opts, int dummy) {
    opts->disjunct_cost = dummy;
}
int parse_options_get_disjunct_cost(Parse_Options opts) {
    return opts->disjunct_cost;
}

void parse_options_set_min_null_count(Parse_Options opts, int val) {
    opts->min_null_count = val;
}
int parse_options_get_min_null_count(Parse_Options opts) {
    return opts->min_null_count;
}

void parse_options_set_max_null_count(Parse_Options opts, int val) {
    opts->max_null_count = val;
}
int parse_options_get_max_null_count(Parse_Options opts) {
    return opts->max_null_count;
}


void parse_options_set_null_block(Parse_Options opts, int dummy) {
    opts->null_block = dummy;
}
int parse_options_get_null_block(Parse_Options opts) {
    return opts->null_block;
}

void parse_options_set_islands_ok(Parse_Options opts, int dummy) {
    opts->islands_ok = dummy;
}

int parse_options_get_islands_ok(Parse_Options opts) {
    return opts->islands_ok;
}

void parse_options_set_short_length(Parse_Options opts, int short_length) {
    opts->short_length = short_length;
}

int parse_options_get_short_length(Parse_Options opts) {
    return opts->short_length;
}

void parse_options_set_all_short_connectors(Parse_Options opts, int val) {
    opts->all_short = val;
}

int parse_options_get_all_short_connectors(Parse_Options opts) {
    return opts->all_short;
}

void parse_options_set_max_parse_time(Parse_Options opts, int dummy) {
    opts->resources->max_parse_time = dummy;
}

int parse_options_get_max_parse_time(Parse_Options opts) {
    return opts->resources->max_parse_time;
}

void parse_options_set_max_memory(Parse_Options opts, int dummy) {
    opts->resources->max_memory = dummy;
}

int parse_options_get_max_memory(Parse_Options opts) {
    return opts->resources->max_memory;
}

void parse_options_set_max_sentence_length(Parse_Options opts, int dummy) {
    opts->max_sentence_length = dummy;
}

int parse_options_get_max_sentence_length(Parse_Options opts) {
    return opts->max_sentence_length;
}

void parse_options_set_echo_on(Parse_Options opts, int dummy) {
    opts->echo_on = dummy;
}

int parse_options_get_echo_on(Parse_Options opts) {
    return opts->echo_on;
}

void parse_options_set_batch_mode(Parse_Options opts, int dummy) {
    opts->batch_mode = dummy;
}

int parse_options_get_batch_mode(Parse_Options opts) {
    return opts->batch_mode;
}

void parse_options_set_panic_mode(Parse_Options opts, int dummy) {
    opts->panic_mode = dummy;
}

int parse_options_get_panic_mode(Parse_Options opts) {
    return opts->panic_mode;
}

void parse_options_set_allow_null(Parse_Options opts, int dummy) {
    opts->allow_null = dummy;
}

int parse_options_get_allow_null(Parse_Options opts) {
    return opts->allow_null;
}

void parse_options_set_screen_width(Parse_Options opts, int dummy) {
    opts->screen_width = dummy;
}

int parse_options_get_screen_width(Parse_Options opts) {
    return opts->screen_width;
}


void parse_options_set_display_on(Parse_Options opts, int dummy) {
    opts->display_on = dummy;
}

int parse_options_get_display_on(Parse_Options opts) {
    return opts->display_on;
}

void parse_options_set_display_postscript(Parse_Options opts, int dummy) {
    opts->display_postscript = dummy;
}

int parse_options_get_display_postscript(Parse_Options opts) {
    return opts->display_postscript;
}

void parse_options_set_display_constituents(Parse_Options opts, int dummy) {
    if ((dummy < 0) || (dummy > 2)) {
       fprintf(stderr, "Possible values for constituents: \n");
       fprintf(stderr, "   0 (no display) 1 (treebank style) or 2 (flat tree)\n");
       opts->display_constituents = 0;
    }    
    else opts->display_constituents = dummy;
}

int parse_options_get_display_constituents(Parse_Options opts) {
    return opts->display_constituents;
}

void parse_options_set_display_bad(Parse_Options opts, int dummy) {
    opts->display_bad = dummy;
}

int parse_options_get_display_bad(Parse_Options opts) {
    return opts->display_bad;
}

void parse_options_set_display_links(Parse_Options opts, int dummy) {
    opts->display_links = dummy;
}

int parse_options_get_display_links(Parse_Options opts) {
    return opts->display_links;
}

void parse_options_set_display_walls(Parse_Options opts, int dummy) {
    opts->display_walls = dummy;
}

int parse_options_get_display_walls(Parse_Options opts) {
    return opts->display_walls;
}

int parse_options_get_display_union(Parse_Options opts) {
    return opts->display_union;
}

void parse_options_set_display_union(Parse_Options opts, int dummy) {
    opts->display_union = dummy;
}

int parse_options_timer_expired(Parse_Options opts) {
    return resources_timer_expired(opts->resources);
}

int parse_options_memory_exhausted(Parse_Options opts) {
    return resources_memory_exhausted(opts->resources);
}

int parse_options_resources_exhausted(Parse_Options opts) {
    return (resources_timer_expired(opts->resources) || resources_memory_exhausted(opts->resources));
}

void parse_options_reset_resources(Parse_Options opts) {
    resources_reset(opts->resources);
}


/***************************************************************
*
* Routines for manipulating Dictionary 
*
****************************************************************/

/* The following function is dictionary_create with an extra paramater called "path".
   If this is non-null, then the path used to find the file is taken from that path.
   Otherwise the path is taken from the dict_name.  This is only needed because
   an affix_file is opened by a recursive call to this function.
 */
static Dictionary internal_dictionary_create(char * dict_name, char * pp_name, char * cons_name, char * affix_name, char * path) {
    Dictionary dict;
    static int rand_table_inited=FALSE;
    Dict_node *dict_node;
    char * dictionary_path_name;

    dict = (Dictionary) xalloc(sizeof(struct Dictionary_s));

    if (!rand_table_inited) {
        init_randtable();
	rand_table_inited=TRUE;
    }

    dict->string_set = string_set_create();
    dict->name = string_set_add(dict_name, dict->string_set);
    dict->num_entries = 0;
    dict->is_special = FALSE;
    dict->already_got_it = '\0';
    dict->line_number = 1;
    dict->root = NULL;
    dict->word_file_header = NULL;
    dict->exp_list = NULL;
    dict->affix_table = NULL;

    /*  *DS*  remove this
    if (pp_name != NULL) {
	dict->post_process_filename = string_set_add(pp_name, dict->string_set);
    }
    else {
	dict->post_process_filename = NULL;
    }
    */
    
    if (path != NULL) dictionary_path_name = path; else dictionary_path_name = dict_name;

    if (!open_dictionary(dictionary_path_name, dict)) {
	lperror(NODICT, dict_name);
	string_set_delete(dict->string_set);
	xfree(dict, sizeof(struct Dictionary_s));
	return NULL;
    }

    if (!read_dictionary(dict)) {
	string_set_delete(dict->string_set);
	xfree(dict, sizeof(struct Dictionary_s));
	return NULL;
    }

    dict->left_wall_defined  = boolean_dictionary_lookup(dict, LEFT_WALL_WORD);
    dict->right_wall_defined = boolean_dictionary_lookup(dict, RIGHT_WALL_WORD);
    dict->postprocessor      = post_process_open(dict->name, pp_name);
    dict->constituent_pp     = post_process_open(dict->name, cons_name);
    
    dict->affix_table = NULL;
    if (affix_name != NULL) {
	dict->affix_table = internal_dictionary_create(affix_name, NULL, NULL, NULL, dict_name);
	if (dict->affix_table == NULL) {
	    fprintf(stderr, "%s\n", lperrmsg);
	    exit(-1);
	}
    }
    
    dict->unknown_word_defined = boolean_dictionary_lookup(dict, UNKNOWN_WORD);
    dict->use_unknown_word = TRUE;
    dict->capitalized_word_defined = boolean_dictionary_lookup(dict, PROPER_WORD);
    dict->pl_capitalized_word_defined = boolean_dictionary_lookup(dict, PL_PROPER_WORD);
    dict->hyphenated_word_defined = boolean_dictionary_lookup(dict, HYPHENATED_WORD);
    dict->number_word_defined = boolean_dictionary_lookup(dict, NUMBER_WORD);
    dict->ing_word_defined = boolean_dictionary_lookup(dict, ING_WORD);
    dict->s_word_defined = boolean_dictionary_lookup(dict, S_WORD);
    dict->ed_word_defined = boolean_dictionary_lookup(dict, ED_WORD);
    dict->ly_word_defined = boolean_dictionary_lookup(dict, LY_WORD);
    dict->max_cost = 1000;

    if ((dict_node = dictionary_lookup(dict, ANDABLE_CONNECTORS_WORD)) != NULL) {
	dict->andable_connector_set = connector_set_create(dict_node->exp);
    } else {
	dict->andable_connector_set = NULL;
    }

    if ((dict_node = dictionary_lookup(dict, UNLIMITED_CONNECTORS_WORD)) != NULL) {
	dict->unlimited_connector_set = connector_set_create(dict_node->exp);
    } else {
	dict->unlimited_connector_set = NULL;
    }

    free_lookup_list();
    return dict;
}

Dictionary dictionary_create(char * dict_name, char * pp_name, char * cons_name, char * affix_name) {
    return internal_dictionary_create(dict_name, pp_name, cons_name, affix_name, NULL);
}

/* obsolete *DS*
void dictionary_open_affix_file(Dictionary dict, char * affix_file) {
    dict->affix_table = dictionary_create(affix_file, NULL);
    if (dict->affix_table == NULL) {
        fprintf(stderr, "%s\n", lperrmsg);
	exit(-1);
    }
}
*/

/* obsolete *DS*
void dictionary_open_constituent_knowledge(Dictionary dict, char * cons_file) {
    dict->constituent_pp = post_process_open(dict->name, cons_file);
}
*/

int dictionary_delete(Dictionary dict) {

    if (verbosity > 0) {
	fprintf(stderr, "Freeing dictionary %s\n", dict->name);
    }

    if (dict->affix_table != NULL) {
        dictionary_delete(dict->affix_table);
    }

    connector_set_delete(dict->andable_connector_set);
    connector_set_delete(dict->unlimited_connector_set);

    post_process_close(dict->postprocessor);
    post_process_close(dict->constituent_pp);
    string_set_delete(dict->string_set);
    free_dictionary(dict);
    xfree(dict, sizeof(struct Dictionary_s));

    return 0;
}

int dictionary_get_max_cost(Dictionary dict) {
    return dict->max_cost;
}


/***************************************************************
*
* Routines for creating and destroying processing Sentences
*
****************************************************************/

Sentence sentence_create(char *input_string, Dictionary dict) {
    Sentence sent;
    int i;

    free_lookup_list();

    sent = (Sentence) xalloc(sizeof(struct Sentence_s));
    sent->dict = dict;
    sent->length = 0;
    sent->num_linkages_found = 0;
    sent->num_linkages_alloced = 0;
    sent->num_linkages_post_processed = 0;
    sent->num_valid_linkages = 0;
    sent->link_info = NULL;
    sent->deletable = NULL;
    sent->effective_dist = NULL;
    sent->num_valid_linkages = 0;
    sent->null_count = 0;
    sent->parse_info = NULL;
    sent->string_set = string_set_create();

    if (!separate_sentence(input_string, sent)) {
	string_set_delete(sent->string_set);
	xfree(sent, sizeof(struct Sentence_s));
	return NULL;
    }
   
    sent->q_pruned_rules = FALSE; /* for post processing */
    sent->is_conjunction = (char *) xalloc(sizeof(char)*sent->length);
    set_is_conjunction(sent);
    initialize_conjunction_tables(sent);

    for (i=0; i<sent->length; i++) {
	/* in case we free these before they set to anything else */
	sent->word[i].x = NULL;
	sent->word[i].d = NULL;
    }
    
    if (!(dict->unknown_word_defined && dict->use_unknown_word)) {
	if (!sentence_in_dictionary(sent)) {
	    sentence_delete(sent);
	    return NULL;
	}
    }
    
    if (!build_sentence_expressions(sent)) {
	sentence_delete(sent);
	return NULL;
    }

    return sent;
}

void sentence_delete(Sentence sent) {

  /*free_andlists(sent); */
    free_sentence_disjuncts(sent);      
    free_sentence_expressions(sent);
    string_set_delete(sent->string_set);
    free_parse_set(sent);
    free_post_processing(sent);
    post_process_close_sentence(sent->dict->postprocessor);
    free_lookup_list();
    free_deletable(sent);
    free_effective_dist(sent);
    xfree(sent->is_conjunction, sizeof(char)*sent->length);
    xfree((char *) sent, sizeof(struct Sentence_s));
}

void free_andlists(Sentence sent) {

  int L;
  Andlist * andlist, * next;
  for(L=0; L<sent->num_linkages_post_processed; L++) {
    /* printf("%d ", sent->link_info[L].canonical);  */
    /* if (sent->link_info[L].canonical==0) continue; */
    andlist = sent->link_info[L].andlist; 
    while(1) {
      if(andlist == NULL) break;
      next = andlist->next;
      xfree((char *) andlist, sizeof(Andlist));
      andlist = next;
    }
  }
  /* printf("\n"); */
}

int sentence_length(Sentence sent) {
    return sent->length;
}

char * sentence_get_word(Sentence sent, int index) {
    return sent->word[index].string;
}

int sentence_null_count(Sentence sent) {
    return sent->null_count;
}

int sentence_num_linkages_found(Sentence sent) {
    return sent->num_linkages_found;
}

int sentence_num_valid_linkages(Sentence sent) {
    return sent->num_valid_linkages;
}

int sentence_num_linkages_post_processed(Sentence sent) {
    return sent->num_linkages_post_processed;
}

int sentence_num_violations(Sentence sent, int i) {
    return sent->link_info[i].N_violations;
}

int sentence_disjunct_cost(Sentence sent, int i) {
    return sent->link_info[i].disjunct_cost;
}

/***************************************************************
*
* Routines for postprocessing
*
****************************************************************/


void post_process_linkages(Sentence sent, Parse_Options opts) {

    int *indices;
    int in, block_bottom, block_top;
    int N_linkages_found, N_linkages_alloced;
    int N_linkages_post_processed, N_valid_linkages;
    int overflowed, only_canonical_allowed;
    double denom;
    Linkage_info *link_info;
    int canonical;
    
    free_post_processing(sent);   

    overflowed = build_parse_set(sent, sent->null_count, opts);
    print_time(opts, "Built parse set");

    if (overflowed) {
	/* We know that sent->num_linkages_found is bogus, possibly negative */
        sent->num_linkages_found = opts->linkage_limit;
	if (opts->verbosity > 1) 
	  fprintf(stdout,
		  "Warning: Count overflow.\n"
		  "Considering a random subset of %d of an unknown and large number of linkages\n",
		  opts->linkage_limit);
    }
    N_linkages_found = sent->num_linkages_found;
    
    if (sent->num_linkages_found == 0) {
	sent->num_linkages_alloced = 0;
	sent->num_linkages_post_processed = 0;
	sent->num_valid_linkages = 0;
	sent->link_info = NULL;
	return;
    }
    
    if (N_linkages_found > opts->linkage_limit) 
      {
	N_linkages_alloced = opts->linkage_limit;
	if (opts->verbosity > 1) {
	  fprintf(stdout,
		  "Warning: Considering a random subset of %d of %d linkages\n",
		  N_linkages_alloced, N_linkages_found);
	}
      } 
    else N_linkages_alloced = N_linkages_found;
    
    link_info=(Linkage_info *)xalloc(N_linkages_alloced * sizeof(Linkage_info));
    N_linkages_post_processed = N_valid_linkages = 0;

    /* generate an array of linkage indices to examine */
    indices = (int *) xalloc(N_linkages_alloced * sizeof(int));
    if (overflowed) {
	for (in=0; in<N_linkages_alloced; in++) {
	    indices[in] = -(in+1); 
	}
    }
    else {
	my_random_initialize(N_linkages_found + sent->length);
	for (in=0; in<N_linkages_alloced; in++) {
	    denom = (double) N_linkages_alloced;
	    block_bottom = (int) (((double)in*(double) N_linkages_found)/denom);
	    block_top = (int) (((double)(in+1)*(double)N_linkages_found)/denom);
	    indices[in] = block_bottom + (my_random() % (block_top-block_bottom));
	}
	my_random_finalize();
    }

    only_canonical_allowed = (!(overflowed || (N_linkages_found > 2*opts->linkage_limit)));
    /* When we're processing only a small subset of the linkages, don't worry
       about restricting the set we consider to be canonical ones.  In the extreme
       case where we are only generating 1 in a million linkages, it's very unlikely
       that we'll hit two symmetric variants of the same linkage anyway. */
    
    /* (optional) first pass: just visit the linkages */ 
    /* The purpose of these two passes is to make the post-processing more
       efficient.  Because (hopefully) by the time you do the real work
       in the 2nd pass you've pruned the relevant rule set in the first pass. */
    if (sent->length >= opts->twopass_length) {
	for (in=0; (in < N_linkages_alloced) && 
	           (!resources_exhausted(opts->resources)); in++) {
	    extract_links(indices[in], sent->null_count, sent->parse_info);
	    if (set_has_fat_down(sent)) {
		if (only_canonical_allowed && !is_canonical_linkage(sent)) continue;
		analyze_fat_linkage(sent, opts, PP_FIRST_PASS);
	    } 
	    else {
		analyze_thin_linkage(sent, opts, PP_FIRST_PASS);
	    }
	}
    }
    
    /* second pass: actually perform post-processing */
    for (in=0; (in < N_linkages_alloced) && 
	       (!resources_exhausted(opts->resources)); in++) {
	extract_links(indices[in], sent->null_count, sent->parse_info);
	if (set_has_fat_down(sent)) {
	    canonical = is_canonical_linkage(sent);
	    if (only_canonical_allowed && !canonical) continue;
	    link_info[N_linkages_post_processed] = 
		analyze_fat_linkage(sent, opts, PP_SECOND_PASS);
	    link_info[N_linkages_post_processed].fat = TRUE;
	    link_info[N_linkages_post_processed].canonical = canonical;
	} 
	else {
	    link_info[N_linkages_post_processed] = 
		analyze_thin_linkage(sent, opts, PP_SECOND_PASS);
	    link_info[N_linkages_post_processed].fat = FALSE;
	    link_info[N_linkages_post_processed].canonical = TRUE;
	}
	if (link_info[N_linkages_post_processed].N_violations==0)
	    N_valid_linkages++;
	link_info[N_linkages_post_processed].index = indices[in];
	N_linkages_post_processed++;
    }

    print_time(opts, "Postprocessed all linkages"); 
    qsort((void *)link_info, N_linkages_post_processed, sizeof(Linkage_info),
	  (int (*)(const void *, const void *)) opts->cost_model.compare_fn);
    
    if (!resources_exhausted(opts->resources)) {
	assert(! ((N_linkages_post_processed == 0) && 
		  (N_linkages_found > 0) && 
		  (N_linkages_found < opts->linkage_limit)),
	       "None of the linkages is canonical");
    }

    if (opts->verbosity > 1) {
	fprintf(stdout, "%d of %d linkages with no P.P. violations\n", 
		N_valid_linkages, N_linkages_post_processed);
    }
	
    print_time(opts, "Sorted all linkages");

    sent->num_linkages_alloced = N_linkages_alloced;
    sent->num_linkages_post_processed = N_linkages_post_processed;
    sent->num_valid_linkages = N_valid_linkages;
    sent->link_info = link_info;

    xfree(indices, N_linkages_alloced * sizeof(int));
    /*if(N_valid_linkages == 0) free_andlists(sent); */
}

void free_post_processing(Sentence sent) {
    if (sent->link_info != NULL) {
	/* postprocessing must have been done */
        free_andlists(sent);
	xfree((char *) sent->link_info, sent->num_linkages_alloced*sizeof(Linkage_info));
	sent->link_info = NULL;
    }
}

int sentence_parse(Sentence sent, Parse_Options opts) {
    int nl;

    verbosity = opts->verbosity;
 
    free_sentence_disjuncts(sent);
    resources_reset_space(opts->resources);

    if (resources_exhausted(opts->resources)) {
	sent->num_valid_linkages = 0;
	return 0;
    }

    expression_prune(sent); 
    print_time(opts, "Finished expression pruning");
    prepare_to_parse(sent, opts);

    init_fast_matcher(sent);
    init_table(sent);

    /* A parse set may have been already been built for this sentence,
       if it was previously parsed.  If so we free it up before building another.  */
    free_parse_set(sent);
    init_x_table(sent);

    for (nl = opts->min_null_count; 
	 (nl<=opts->max_null_count) && (!resources_exhausted(opts->resources)); ++nl) {
	sent->null_count = nl;
	sent->num_linkages_found = parse(sent, sent->null_count, opts);
	print_time(opts, "Counted parses");
	post_process_linkages(sent, opts);
	if (sent->num_valid_linkages > 0) break;
    }

    free_table(sent);
    free_fast_matcher(sent);
    print_time(opts, "Finished parse");

    return sent->num_valid_linkages;
}

/***************************************************************
*
* Routines which allow user access to Linkages.
*
****************************************************************/

Linkage linkage_create(int k, Sentence sent, Parse_Options opts) {
    Linkage linkage;

    assert((k < sent->num_linkages_post_processed) && (k >= 0), "index out of range");
    
    /* Using exalloc since this is external to the parser itself. */
    linkage = (Linkage) exalloc(sizeof(struct Linkage_s));
    
    linkage->num_words = sent->length;
    linkage->word = (char **) exalloc(linkage->num_words*sizeof(char *));
    linkage->current = 0;
    linkage->num_sublinkages=0;
    linkage->sublinkage = NULL;
    linkage->unionized = FALSE;
    linkage->sent = sent;
    linkage->opts = opts;
    linkage->info = sent->link_info[k];

    extract_links(sent->link_info[k].index, sent->null_count, sent->parse_info);
    compute_chosen_words(sent, linkage);
    
    if (set_has_fat_down(sent)) {
	extract_fat_linkage(sent, opts, linkage);
    }
    else {
	extract_thin_linkage(sent, opts, linkage);
    }

    if (sent->dict->postprocessor != NULL) {
       linkage_post_process(linkage, sent->dict->postprocessor);
    }

    return linkage;
}

int linkage_set_current_sublinkage(Linkage linkage, int index) {
    int errno;
    if ((index < 0) ||
	(index >= linkage->num_sublinkages)) {
	errno = -1;
	return 0;
    }
    linkage->current = index;
    return 1;
}

void exfree_pp_info(PP_info ppi) {
    int i;
    for (i=0; i<ppi.num_domains; ++i) {
	exfree(ppi.domain_name[i], strlen(ppi.domain_name[i])+1);
    }
    if (ppi.num_domains > 0) 
	exfree(ppi.domain_name, sizeof(char *)*ppi.num_domains);
}

void xfree_pp_info(PP_info ppi) {
    int i;
    for (i=0; i<ppi.num_domains; ++i) {
	xfree(ppi.domain_name[i], strlen(ppi.domain_name[i])+1);
    }
    xfree(ppi.domain_name, sizeof(char *)*ppi.num_domains);
}

void linkage_delete(Linkage linkage) {
    int i, j;
    Sublinkage *s;

    for (i=0; i<linkage->num_words; ++i) {
	exfree(linkage->word[i], strlen(linkage->word[i])+1);
    }
    exfree(linkage->word, sizeof(char *)*linkage->num_words);
    for (i=0; i<linkage->num_sublinkages; ++i) {
	s = &(linkage->sublinkage[i]);
	for (j=0; j<s->num_links; ++j) {
	  exfree_link(s->link[j]); 
	}
	exfree(s->link, sizeof(Link)*s->num_links);
	if (s->pp_info != NULL) {
	    for (j=0; j<s->num_links; ++j) {
		exfree_pp_info(s->pp_info[j]);
	    }
	    exfree(s->pp_info, sizeof(PP_info)*s->num_links);
	    post_process_free_data(&s->pp_data); 
	}
	if (s->violation != NULL) {
	    exfree(s->violation, sizeof(char)*(strlen(s->violation)+1));
	}
    } 
    exfree(linkage->sublinkage, sizeof(Sublinkage)*linkage->num_sublinkages);
    exfree(linkage, sizeof(struct Linkage_s));
}

int links_are_equal(Link l, Link m) {
    return ((l->l == m->l) && (l->r == m->r) && (strcmp(l->name, m->name)==0));
}

int link_already_appears(Linkage linkage, Link link, int a) {
    int i, j;

    for (i=0; i<a; ++i) {
	for (j=0; j<linkage->sublinkage[i].num_links; ++j) {
	    if (links_are_equal(linkage->sublinkage[i].link[j], link)) return TRUE;
	}
    }
    return FALSE;
}

PP_info excopy_pp_info(PP_info ppi) {
     static PP_info newppi;
     int i;

     newppi.num_domains = ppi.num_domains;
     newppi.domain_name = (char **) exalloc(sizeof(char *)*ppi.num_domains);
     for (i=0; i<newppi.num_domains; ++i) {
         newppi.domain_name[i] = (char *) exalloc(sizeof(char)*(strlen(ppi.domain_name[i])+1));
	 strcpy(newppi.domain_name[i], ppi.domain_name[i]);
     }
     return newppi;
}


Sublinkage unionize_linkage(Linkage linkage) {
    int i, j, num_in_union=0;
    Sublinkage u;
    Link link;
    char *p;

    for (i=0; i<linkage->num_sublinkages; ++i) {
	for (j=0; j<linkage->sublinkage[i].num_links; ++j) {
	    link = linkage->sublinkage[i].link[j];
	    if (!link_already_appears(linkage, link, i)) num_in_union++;
	}
    }

    u.num_links = num_in_union;
    u.link = (Link *) exalloc(sizeof(Link)*num_in_union);
    u.pp_info = (PP_info *) exalloc(sizeof(PP_info)*num_in_union);
    u.violation = NULL;
    
    num_in_union = 0;

    for (i=0; i<linkage->num_sublinkages; ++i) {
	for (j=0; j<linkage->sublinkage[i].num_links; ++j) {
	    link = linkage->sublinkage[i].link[j];
	    if (!link_already_appears(linkage, link, i)) {
		u.link[num_in_union] = excopy_link(link);
		u.pp_info[num_in_union] = excopy_pp_info(linkage->sublinkage[i].pp_info[j]);
		if (((p=linkage->sublinkage[i].violation) != NULL) &&
		    (u.violation == NULL)) {
		    u.violation = (char *) exalloc((strlen(p)+1)*sizeof(char));
		    strcpy(u.violation, p);
		}
		num_in_union++;
	    }
	}
    }

    return u;
}

int linkage_compute_union(Linkage linkage) {
    int i, num_subs=linkage->num_sublinkages;
    Sublinkage * new_sublinkage;

    if (linkage->unionized) {
	linkage->current = linkage->num_sublinkages-1;
	return 0;
    }
    if (num_subs == 1) {
	linkage->unionized = TRUE;
	return 1;
    }
    
    new_sublinkage = 
	(Sublinkage *) exalloc(sizeof(Sublinkage)*(num_subs+1));

    for (i=0; i<num_subs; ++i) {
	new_sublinkage[i] = linkage->sublinkage[i];
    }
    exfree(linkage->sublinkage, sizeof(Sublinkage)*num_subs);
    linkage->sublinkage = new_sublinkage;
    linkage->sublinkage[num_subs] = unionize_linkage(linkage);

    /* The domain data will not be needed for the union -- zero it out */
    linkage->sublinkage[num_subs].pp_data.N_domains=0;
    linkage->sublinkage[num_subs].pp_data.length=0;
    linkage->sublinkage[num_subs].pp_data.links_to_ignore=NULL;
    for (i=0; i<MAX_SENTENCE; ++i) {
      linkage->sublinkage[num_subs].pp_data.word_links[i] = NULL;
    }

    linkage->num_sublinkages++;

    linkage->unionized = TRUE;
    linkage->current = linkage->num_sublinkages-1;
    return 1;
}

int linkage_get_num_sublinkages(Linkage linkage) {
    return linkage->num_sublinkages;
}

int linkage_get_num_words(Linkage linkage) {
    return linkage->num_words;
}

int linkage_get_num_links(Linkage linkage) {
    int current = linkage->current;
    return linkage->sublinkage[current].num_links;
}

static int verify_link_index(Linkage linkage, int index) {
    int errno;
    if ((index < 0) ||
	(index >= linkage->sublinkage[linkage->current].num_links)) {
	errno = -1;
	return 0;
    }
    return 1;
}

int linkage_get_link_length(Linkage linkage, int index) {
    Link link;
    int word_has_link[MAX_SENTENCE];
    int i, length;
    int current = linkage->current;

    if (!verify_link_index(linkage, index)) return -1;

    for (i=0; i<linkage->num_words+1; ++i) {
        word_has_link[i] = FALSE;
    }

    for (i=0; i<linkage->sublinkage[current].num_links; ++i) {
	link = linkage->sublinkage[current].link[i];
	word_has_link[link->l] = TRUE;
	word_has_link[link->r] = TRUE;
    }

    link = linkage->sublinkage[current].link[index];
    length = link->r - link->l;
    for (i= link->l+1; i < link->r; ++i) {
	if (!word_has_link[i]) length--;
    }
    return length;
}

int linkage_get_link_lword(Linkage linkage, int index) {
    Link link;
    if (!verify_link_index(linkage, index)) return -1;
    link = linkage->sublinkage[linkage->current].link[index];
    return link->l;
}

int linkage_get_link_rword(Linkage linkage, int index) {
    Link link;
    if (!verify_link_index(linkage, index)) return -1;
    link = linkage->sublinkage[linkage->current].link[index];
    return link->r;
}

char * linkage_get_link_label(Linkage linkage, int index) {
    Link link;
    if (!verify_link_index(linkage, index)) return NULL;
    link = linkage->sublinkage[linkage->current].link[index];
    return link->name;
}

char * linkage_get_link_llabel(Linkage linkage, int index) {
    Link link;
    if (!verify_link_index(linkage, index)) return NULL;
    link = linkage->sublinkage[linkage->current].link[index];
    return link->lc->string;
}

char * linkage_get_link_rlabel(Linkage linkage, int index) {
    Link link;
    if (!verify_link_index(linkage, index)) return NULL;
    link = linkage->sublinkage[linkage->current].link[index];
    return link->rc->string;
}

char ** linkage_get_words(Linkage linkage) {
    return linkage->word;
}

Sentence linkage_get_sentence(Linkage linkage) {
    return linkage->sent;
}

char * linkage_get_word(Linkage linkage, int w) {
    return linkage->word[w];
}

int linkage_unused_word_cost(Linkage linkage) {
    return linkage->info.unused_word_cost;
}

int linkage_disjunct_cost(Linkage linkage) {
    return linkage->info.disjunct_cost;
}

int linkage_and_cost(Linkage linkage) {
    return linkage->info.and_cost;
}

int linkage_link_cost(Linkage linkage) {
    return linkage->info.link_cost;
}

int linkage_get_link_num_domains(Linkage linkage, int index) {
    PP_info pp_info;
    if (!verify_link_index(linkage, index)) return -1;
    pp_info = linkage->sublinkage[linkage->current].pp_info[index];
    return pp_info.num_domains;
}

char ** linkage_get_link_domain_names(Linkage linkage, int index) {
    PP_info pp_info;
    if (!verify_link_index(linkage, index)) return NULL;
    pp_info = linkage->sublinkage[linkage->current].pp_info[index];
    return pp_info.domain_name;
}

char * linkage_get_violation_name(Linkage linkage) {
    return linkage->sublinkage[linkage->current].violation;
}

int linkage_is_canonical(Linkage linkage) {
    return linkage->info.canonical;
}

int linkage_is_improper(Linkage linkage) {
    return linkage->info.improper_fat_linkage;
}

int linkage_has_inconsistent_domains(Linkage linkage) {
    return linkage->info.inconsistent_domains;
}

void linkage_post_process(Linkage linkage, Postprocessor * postprocessor) {
    int N_sublinkages = linkage_get_num_sublinkages(linkage);
    Parse_Options opts = linkage->opts;
    Sentence sent = linkage->sent;
    Sublinkage * subl;
    PP_node * pp;
    int i, j, k;
    D_type_list * d;

    for (i=0; i<N_sublinkages; ++i) {
	
	subl = &linkage->sublinkage[i];
        if (subl->pp_info != NULL) {
	    for (j=0; j<subl->num_links; ++j) {
		exfree_pp_info(subl->pp_info[j]);
	    }
	    post_process_free_data(&subl->pp_data);
	    exfree(subl->pp_info, sizeof(PP_info)*subl->num_links);
	}
	subl->pp_info = (PP_info *) exalloc(sizeof(PP_info)*subl->num_links);
	for (j=0; j<subl->num_links; ++j) {
	    subl->pp_info[j].num_domains = 0;
	    subl->pp_info[j].domain_name = NULL;
	}
	if (subl->violation != NULL) {
	    exfree(subl->violation, sizeof(char)*(strlen(subl->violation)+1));
	    subl->violation = NULL;
	}

        if (linkage->info.improper_fat_linkage) {
            pp = NULL;
        } else {
            pp = post_process(postprocessor, opts, sent, subl, FALSE);
	    /* This can return NULL, for example if there is no
	       post-processor */
        }

	if (pp == NULL) {
	    for (j=0; j<subl->num_links; ++j) {
		subl->pp_info[j].num_domains = 0;
		subl->pp_info[j].domain_name = NULL;
	    }
	}
	else {
	    for (j=0; j<subl->num_links; ++j) {
		k=0;
		for (d = pp->d_type_array[j]; d!=NULL; d=d->next) k++;
		subl->pp_info[j].num_domains = k;
		if (k > 0) {
		    subl->pp_info[j].domain_name = (char **) exalloc(sizeof(char *)*k);
		}
		k = 0;
		for (d = pp->d_type_array[j]; d!=NULL; d=d->next) {
		    subl->pp_info[j].domain_name[k] = (char *) exalloc(sizeof(char)*2);
		    sprintf(subl->pp_info[j].domain_name[k], "%c", d->type);
		    k++;
		}
	    }
	    subl->pp_data = postprocessor->pp_data;
	    if (pp->violation != NULL) {
		subl->violation = 
		    (char *) exalloc(sizeof(char)*(strlen(pp->violation)+1));
		strcpy(subl->violation, pp->violation);
	    }
	}
    }
    post_process_close_sentence(postprocessor);
}

/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

/* stuff for transforming a dictionary entry into a disjunct list */

#include "link-includes.h"

/*Temporary connectors used while converting expressions into disjunct lists */
typedef struct Tconnector_struct Tconnector;
struct Tconnector_struct{
    char multi;   /* TRUE if this is a multi-connector */
    char dir;     /* '-' for left and '+' for right */
    Tconnector * next;
    char * string;
};

typedef struct clause_struct Clause;
struct clause_struct {
    Clause * next;
    int cost;
    int maxcost;
    Tconnector * c;
};

Tconnector * copy_Tconnectors(Tconnector * c) {
/* This builds a new copy of the connector list pointed to by c.
   Strings, as usual, are not copied.
*/
    Tconnector *c1;
    if (c == NULL) return NULL;
    c1 = (Tconnector *) xalloc(sizeof(Tconnector));
    *c1 = *c;
    c1->next = copy_Tconnectors(c->next);
    return c1;
}

void free_Tconnectors(Tconnector *e) {
    Tconnector * n;
    for(;e != NULL; e=n) {
	n = e->next;
	xfree((char *)e, sizeof(Tconnector));
    }
}

void free_clause_list(Clause *c) {
    Clause *c1;
    while (c != NULL) {
	c1 = c->next;
	free_Tconnectors(c->c);
	xfree((char *)c, sizeof(Clause));
	c = c1;
    }
}

Clause * copy_clause(Clause * d) {
/* This builds a new copy of the clause pointed to by d (except for the
   next field which is set to NULL).  Strings, as usual, are not copied.
*/
    Clause * d1;
    if (d == NULL) return NULL;
    d1 = (Clause *) xalloc(sizeof(Clause));
    *d1 = *d;
    d1->next = NULL;
    d1->c = copy_Tconnectors(d->c);
    return d1;
}

Tconnector * Treverse(Tconnector *e) {
/* reverse the order of the list e.  destructive */
    Tconnector * head, *x;
    head = NULL;
    while (e != NULL) {
	x = e->next;
	e->next = head;
	head = e;
	e = x;
    }
    return head;
}    

Connector * reverse(Connector *e) {
/* reverse the order of the list e.  destructive */
    Connector * head, *x;
    head = NULL;
    while (e != NULL) {
	x = e->next;
	e->next = head;
	head = e;
	e = x;
    }
    return head;
}    

Tconnector * catenate(Tconnector * e1, Tconnector * e2) {
/* Builds a new list of connectors that is the catenation of e1 with e2.
   does not effect lists e1 or e2.   Order is maintained. */

    Tconnector * e, * head;
    head = NULL;
    for (;e1 != NULL; e1 = e1->next) {
	e = (Tconnector *) xalloc(sizeof(Tconnector));
	*e = *e1;
	e->next = head;
	head = e;
    }
    for (;e2 != NULL; e2 = e2->next) {
	e = (Tconnector *) xalloc(sizeof(Tconnector));
	*e = *e2;
	e->next = head;
	head = e;
    }
    return Treverse(head);
}

Tconnector * build_terminal(Exp * e) {
    /* build the connector for the terminal node n */
    Tconnector * c;
    c = (Tconnector *) xalloc(sizeof(Tconnector));
    c->string = e->u.string;
    c->multi = e->multi;
    c->dir = e->dir;
    c->next = NULL;
    return c;
}

int maxcost_of_expression(Exp *e) {
    E_list * e_list;
    int m, m1;

    m = 0;
    
    if ((e->type == AND_type) || (e->type == OR_type)) {
	for (e_list = e->u.l; e_list != NULL; e_list = e_list->next) {
	    m1 = maxcost_of_expression(e_list->e);
	    m = MAX(m, m1);
	}
    }
    return (m + e->cost);
}

int maxcost_of_sentence(Sentence sent) {
/* This returns the maximum maxcost of any disjunct in the sentence */
/* assumes the sentence expressions have been constructed */
    X_node * x;
    int w, m, m1;
    m = 0;

    for (w=0; w<sent->length; w++) {
	for (x=sent->word[w].x; x!=NULL; x = x->next){
	    m1 = maxcost_of_expression(x->exp),
	    m = MAX(m, m1);
	}
    }
    return m;
}


Clause * build_clause(Exp *e, int cost_cutoff) {
/* Build the clause for the expression e.  Does not change e */
    Clause *c=NULL, *c1, *c2, *c3, *c4, *c_head;
    E_list * e_list;

    assert(e != NULL, "build_clause called with null parameter");
    if (e->type == AND_type) {
	c1 = (Clause *) xalloc(sizeof (Clause));
	c1->c = NULL;
	c1->next = NULL;
	c1->cost = 0;
	c1->maxcost = 0 ;
	for (e_list = e->u.l; e_list != NULL; e_list = e_list->next) {
	    c2 = build_clause(e_list->e, cost_cutoff);
	    c_head = NULL;
	    for (c3 = c1; c3 != NULL; c3 = c3->next) {
		for (c4 = c2; c4 != NULL; c4 = c4->next) {
		    c = (Clause *) xalloc(sizeof (Clause));
		    c->cost = c3->cost + c4->cost;
		    c->maxcost = MAX(c3->maxcost,c4->maxcost);
		    c->c = catenate(c3->c, c4->c);
		    c->next = c_head;
		    c_head = c;
		}
	    }
	    free_clause_list(c1);
	    free_clause_list(c2);
	    c1 = c_head;
	}
	c = c1;
    } else if (e->type == OR_type) {
	/* we'll catenate the lists of clauses */
	c = NULL;
	for (e_list = e->u.l; e_list != NULL; e_list = e_list->next) {
	    c1 = build_clause(e_list->e, cost_cutoff);
	    while(c1 != NULL) {
		c3 = c1->next;
		c1->next = c;
		c = c1;
		c1 = c3;
	    }
	}
    } else if (e->type == CONNECTOR_type) {
	c = (Clause *) xalloc(sizeof(Clause));
	c->c = build_terminal(e);
	c->cost = 0;
	c->maxcost = 0;
	c->next = NULL;
    } else {
	assert(FALSE, "an expression node with no type");
    }

    /* c now points to the list of clauses */

    for (c1=c; c1!=NULL; c1 = c1->next) {
	c1->cost += e->cost;
/*	c1->maxcost = MAX(c1->maxcost,e->cost);  */  /* <---- This is how Dennis had it.
	                                                I prefer the line below */
	c1->maxcost += e->cost;
    }
    return c;
}

void print_connector_list(Connector * e) {
    for (;e != NULL; e=e->next) {
	printf("%s",e->string);
	if (e->label != NORMAL_LABEL) {
	    printf("%3d", e->label);
	} else {
	    printf("   ");
	}
	if (e->next != NULL) printf(" ");
    }
}

void print_Tconnector_list(Tconnector * e) {
    for (;e != NULL; e=e->next) {
	if (e->multi) printf("@");
	printf("%s",e->string);
	printf("%c", e->dir);
	if (e->next != NULL) printf(" ");
    }
}

void print_clause_list(Clause * c) {
    for (;c != NULL; c=c->next) {
	printf("  Clause: ");
	printf("(%2d, %2d)", c->cost, c->maxcost);
	print_Tconnector_list(c->c);
	printf("\n");
    }
}

void print_disjunct_list(Disjunct * c) {
    for (;c != NULL; c=c->next) {
	printf("%10s: ", c->string);
	printf("(%2d)", c->cost);
	print_connector_list(c->left);
	printf(" <--> ");
	print_connector_list(c->right);
	printf("\n");
    }
}

Connector * extract_connectors(Tconnector *e, int c) {
/* Build a new list of connectors starting from the Tconnectors
   in the list pointed to by e.  Keep only those whose strings whose
   direction has the value c.
*/
    Connector *e1;
    if (e == NULL) return NULL;
    if (e->dir == c) {
	e1 = init_connector((Connector *) xalloc(sizeof(Connector)));
	e1->next = extract_connectors(e->next,c);
	e1->multi = e->multi;
	e1->string = e->string;
	e1->label = NORMAL_LABEL;
	e1->priority = THIN_priority;
	e1->word = 0;
	return e1;
    } else {
	return extract_connectors(e->next,c);
    }
}	

Disjunct * build_disjunct(Clause * cl, char * string, int cost_cutoff) {
/* build a disjunct list out of the clause list c */
/* string is the print name of word that generated this disjunct */

    Disjunct *dis, *ndis;
    dis = NULL;
    for (;cl != NULL; cl=cl->next) {
	if (cl->maxcost <= cost_cutoff) {
	    ndis = (Disjunct *) xalloc(sizeof(Disjunct));
	    ndis->left = reverse(extract_connectors(cl->c, '-'));
	    ndis->right = reverse(extract_connectors(cl->c, '+'));
	    ndis->string = string;
	    ndis->cost = cl->cost;
	    ndis->next = dis;
	    dis = ndis;
	}
    }
    return dis;
}

Disjunct * build_disjuncts_for_X_node(X_node * x, int cost_cutoff) {
    Clause *c ;
    Disjunct * dis;
    c = build_clause(x->exp, cost_cutoff);
    dis = build_disjunct(c, x->string, cost_cutoff);
    free_clause_list(c);
    return dis;
}

Disjunct * build_disjuncts_for_dict_node(Dict_node *dn) {
/* still need this for counting the number of disjuncts */
    Clause *c ;
    Disjunct * dis;
/*                 print_expression(dn->exp);   */
/*                 printf("\n");                */
    c = build_clause(dn->exp, NOCUTOFF);
/*                 print_clause_list(c);        */
    dis = build_disjunct(c, dn->string, NOCUTOFF);
    free_clause_list(c);
    return dis;
}

#if 0
OBS 
OBS Disjunct * build_word_disjuncts(char * s) {
OBS /* Looks up the word s in the dictionary.  Returns NULL if it's not there.
OBS    If there, it builds the disjunct structure for the word, and returns
OBS    a pointer to it.
OBS */
OBS     Dict_node * dn;
OBS     Disjunct * dis;
OBS 
OBS     dn = dictionary_lookup(s);
OBS 
OBS /*
OBS     x = dn;
OBS     printf("%s :", s);
OBS     while (dn != NULL) {
OBS        printf("%s \n", dn->string);
OBS        print_expression(dn->node);
OBS        dn = dn->right;
OBS     }
OBS     dn = x;
OBS */
OBS     dis = NULL;
OBS 
OBS     while (dn != NULL) {
OBS 	dis = catenate_disjuncts(build_disjuncts_for_dict_node(dn), dis);
OBS 	dn = dn->right;
OBS     }
OBS                 /*    print_disjunct_list(dis); */
OBS     return dis;
OBS }
#endif

X_node * build_word_expressions(Sentence sent, char * s) {
/* Looks up the word s in the dictionary.  Returns NULL if it's not there.
   If there, it builds the list of expressions for the word, and returns
   a pointer to it.
*/
    Dict_node * dn;
    X_node * x, * y;

    dn = dictionary_lookup(sent->dict, s);

    x = NULL;
    while (dn != NULL) {
	y = (X_node *) xalloc(sizeof(X_node));
	y->next = x;
	x = y;
	x->exp = copy_Exp(dn->exp);
	x->string = dn->string;
	dn = dn->right;
    }
    return x;
}

void build_sentence_disjuncts(Sentence sent, int cost_cutoff) {
/* We've already built the sentence expressions.  This turns them into
   disjuncts.  
*/
    Disjunct * d;
    X_node * x;
    int w;
    for (w=0; w<sent->length; w++) {
	d = NULL;
	for (x=sent->word[w].x; x!=NULL; x = x->next){
	    d = catenate_disjuncts(build_disjuncts_for_X_node(x, cost_cutoff),d);
	}
	sent->word[w].d = d;
    }
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

static struct {
    int verbosity;
    int timeout;
    int memory;
    int linkage_limit;
    int null_block;
    int islands_ok;
    int short_length;
    int batch_mode;  
    int panic_mode;  
    int allow_null;
    int echo_on; 
    int screen_width;
    int display_on;
    int display_constituents;
    int max_sentence_length;
    int display_postscript;
    int display_bad;
    int display_links;
    int display_walls;
    int display_union;
} local;

typedef struct {
    char * string;
    int    isboolean;
    char * description;
    int  * p;
} Switch;

Switch default_switches[] = {
    {"verbosity",    0, "Level of detail in output",        &local.verbosity},
    {"timeout",      0, "Abort parsing after this long",    &local.timeout},
    {"memory",       0, "Max memory allowed",               &local.memory},
    {"limit",        0, "The maximum linkages processed",   &local.linkage_limit},
    {"null-block",   0, "Size of blocks with null cost 1",  &local.null_block},
    {"islands-ok",   1, "Use of null-linked islands",       &local.islands_ok},
    {"short",        0, "Max length of short links",        &local.short_length},    
    {"batch",        1, "Batch mode",                       &local.batch_mode},
    {"panic",        1, "Use of \"panic mode\"",            &local.panic_mode},
    {"null",         1, "Null links",                       &local.allow_null},
    {"width",        0, "The width of the display",         &local.screen_width},
    {"echo",         1, "Echoing of input sentence",        &local.echo_on},
    {"graphics",     1, "Graphical display of linkage",     &local.display_on},
    {"postscript",   1, "Generate postscript output",       &local.display_postscript},
    {"constituents", 0, "Generate constituent output",      &local.display_constituents},
    {"max-length",   0, "Maximum sentence length",          &local.max_sentence_length},
    {"bad",          1, "Display of bad linkages",          &local.display_bad},
    {"links",        1, "Showing of complete link data",    &local.display_links},
    {"walls",        1, "Showing of wall words",            &local.display_walls},
    {"union",        1, "Showing of 'union' linkage",       &local.display_union},
    {NULL,           1,  NULL,                              NULL}
};

struct {char * s; char * str;} user_command[] = {
    {"variables",    "List user-settable variables and their functions"},
    {"help",         "List the commands and what they do"},
    {NULL,           NULL}
};

void clean_up_string(char * s) {
    /* gets rid of all the white space in the string s.  Changes s */
    char * x, * y;
    y = x = s;
    while(*x != '\0') {
	if (!isspace((int)*x)) {
	    *y = *x; x++; y++;
	} else {
	    x++;
	}
    }
    *y = '\0';
}

int is_numerical_rhs(char *s) {
    /* return TRUE if s points to a number:
     optional + or - followed by 1 or more
     digits.
     */
    if (*s=='+' || *s == '-') s++;
    if (*s == '\0') return FALSE;
    for (; *s != '\0'; s++) if (!isdigit((int)*s)) return FALSE;
    return TRUE;
}

void x_issue_special_command(char * line, Parse_Options opts, Dictionary dict) {
    char *s, myline[1000], *x, *y;
    int i, count, j, k;
    Switch * as = default_switches;

    strncpy(myline, line, sizeof(myline));
    myline[sizeof(myline)-1] = '\0';
    clean_up_string(myline);

    s = myline;
    j = k = -1;
    count = 0;
    for (i=0; as[i].string != NULL; i++) {
	if (as[i].isboolean && strncasecmp(s, as[i].string, strlen(s)) == 0) {
	    count++;
	    j = i;
	}
    }
    for (i=0; user_command[i].s != NULL; i++) {
	if (strncasecmp(s, user_command[i].s, strlen(s)) == 0) {
	    count++;
	    k = i;
	}
    }

    if (count > 1) {
	printf("Ambiguous command.  Type \"!help\" or \"!variables\"\n");
	return;
    } 
    else if (count == 1) {
	if (j >= 0) {
	    *as[j].p = !(*as[j].p);
	    printf("%s turned %s.\n", as[j].description, (*as[j].p)? "on" : "off");
	    return;
	} 
	else {
	    /* replace the abbreviated command by the full one */
	    strcpy(s, user_command[k].s);  
	}
    }



    if (strcmp(s, "variables")==0) {
	printf(" Variable     Controls                                      Value\n");
	printf(" --------     --------                                      -----\n");
	for (i=0; as[i].string != NULL; i++) {
	    printf(" ");
	    left_print_string(stdout, as[i].string, "             ");
	    left_print_string(stdout, as[i].description, "                                              ");
	    printf("%5d", *as[i].p);
	    if (as[i].isboolean) {
		if (*as[i].p) printf(" (On)"); else printf(" (Off)");
	    }
	    printf("\n");
	}
	printf("\n");
	printf("Toggle a boolean variable as in \"!batch\"; ");
	printf("set a variable as in \"!width=100\".\n");
	return;
    }
    if (strcmp(s, "help")==0) {
	printf("Special commands always begin with \"!\".  Command and variable names\n");
	printf("can be abbreviated.  Here is a list of the commands:\n\n");
	for (i=0; user_command[i].s != NULL; i++) {
	    printf(" !");
	    left_print_string(stdout, user_command[i].s, "                  ");
	    left_print_string(stdout, user_command[i].str, "                                                    ");
	    printf("\n");
	}
	printf(" !!<string>         Print all the dictionary words matching <string>.\n");
	printf("                    Also print the number of disjuncts of each.\n");
	printf("\n");
	printf(" !<var>             Toggle the specified boolean variable.\n");
	printf(" !<var>=<val>       Assign that value to that variable.\n");
	return;
    }

    if(s[0] == '!') {
	dict_display_word_info(dict, s+1);
	return;
    }
    
    /* test here for an equation */
    for (x=s; (*x != '=') && (*x != '\0') ; x++)
      ;
    if (*x == '=') {
	*x = '\0';
	y = x+1;
	x = s;
	/* now x is the first word and y is the rest */

	if (is_numerical_rhs(y)) {
	    for (i=0; as[i].string != NULL; i++) {
		if (strcmp(x, as[i].string) == 0) break;
	    }
	    if (as[i].string  == NULL) {
		printf("There is no user variable called \"%s\".\n", x);
	    } else {
		*(as[i].p) = atoi(y);
		printf("%s set to %d\n", x, atoi(y));
	    }
	    return;
	}
    }

    printf("I can't interpret \"%s\" as a command.  Try \"!help\".\n", myline);
}

void put_opts_in_local_vars(Parse_Options opts) {
    local.verbosity = parse_options_get_verbosity(opts);
    local.timeout = parse_options_get_max_parse_time(opts);;
    local.memory = parse_options_get_max_memory(opts);;
    local.linkage_limit = parse_options_get_linkage_limit(opts);
    local.null_block = parse_options_get_null_block(opts);
    local.islands_ok = parse_options_get_islands_ok(opts);
    local.short_length = parse_options_get_short_length(opts);
    local.echo_on = parse_options_get_echo_on(opts);
    local.batch_mode = parse_options_get_batch_mode(opts);
    local.panic_mode = parse_options_get_panic_mode(opts);
    local.screen_width = parse_options_get_screen_width(opts);
    local.allow_null = parse_options_get_allow_null(opts);
    local.screen_width = parse_options_get_screen_width(opts);
    local.display_on = parse_options_get_display_on(opts);
    local.display_postscript = parse_options_get_display_postscript(opts);
    local.display_constituents = parse_options_get_display_constituents(opts);
    local.max_sentence_length = parse_options_get_max_sentence_length(opts);
    local.display_bad = parse_options_get_display_bad(opts);
    local.display_links = parse_options_get_display_links(opts);
    local.display_walls = parse_options_get_display_walls(opts);
    local.display_union = parse_options_get_display_union(opts);
}

void put_local_vars_in_opts(Parse_Options opts) {
    parse_options_set_verbosity(opts, local.verbosity);
    parse_options_set_max_parse_time(opts, local.timeout);
    parse_options_set_max_memory(opts, local.memory);
    parse_options_set_linkage_limit(opts, local.linkage_limit);
    parse_options_set_null_block(opts, local.null_block);
    parse_options_set_islands_ok(opts, local.islands_ok);
    parse_options_set_short_length(opts, local.short_length);
    parse_options_set_echo_on(opts, local.echo_on);
    parse_options_set_batch_mode(opts, local.batch_mode);
    parse_options_set_panic_mode(opts, local.panic_mode);
    parse_options_set_screen_width(opts, local.screen_width);
    parse_options_set_allow_null(opts, local.allow_null);
    parse_options_set_screen_width(opts, local.screen_width);
    parse_options_set_display_on(opts, local.display_on);
    parse_options_set_display_postscript(opts, local.display_postscript);
    parse_options_set_display_constituents(opts, local.display_constituents);
    parse_options_set_max_sentence_length(opts, local.max_sentence_length);
    parse_options_set_display_bad(opts, local.display_bad);
    parse_options_set_display_links(opts, local.display_links);
    parse_options_set_display_walls(opts, local.display_walls);
    parse_options_set_display_union(opts, local.display_union);
}

void issue_special_command(char * line, Parse_Options opts, Dictionary dict) {
    put_opts_in_local_vars(opts);
    x_issue_special_command(line, opts, dict);
    put_local_vars_in_opts(opts);
}


/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include <stdarg.h>
#include "link-includes.h"
#include "constituents.h"

#define MAXCONSTITUENTS 1024
#define MAXSUBL 16
#define OPEN_BRACKET '['
#define CLOSE_BRACKET ']'

typedef enum {OPEN, CLOSE, WORD} CType;
typedef enum {NONE, STYPE, PTYPE, QTYPE, QDTYPE} WType;
String_set * phrase_ss;

struct {
  int left;
  int right;
  char * type;
  char domain_type;
  char * start_link;
  int start_num;
  int subl; 
  int canon;
  int valid;
  int aux;      
  /* 0: it's an ordinary VP (or other type); 
     1: it's an AUX, don't print it; 
     2: it's an AUX, and print it */
} constituent[MAXCONSTITUENTS];

/*PV* made global vars static */
static WType wordtype[MAX_SENTENCE];
static int word_used[MAXSUBL][MAX_SENTENCE];
static int templist[100];
static struct {
  int num;
  int e[10];
  int valid;
} andlist[1024];

/* forward declarations */
static void print_constituent(Linkage linkage, int c);
static void adjust_for_left_comma(Linkage linkage, int c);
static void adjust_for_right_comma(Linkage linkage, int c);
static int  find_next_element(Linkage linkage, 
			      int start, 
			      int numcon_total, 
			      int num_elements, 
			      int num_lists);

/*********************************************************/
/*        These functions do the bulk of the actual      */
/* constituent-generating; they're called once for each  */
/*                      sublinkage                       */
/*********************************************************/

static int uppercompare(char * s, char * t) {
  while(isupper((int) *s) || isupper((int) *t)) {
      if (*s != *t) return 1;
      s++;
      t++;
  }
  return 0;
}  

static int gen_comp(Linkage linkage, int numcon_total, int numcon_subl, 
		    char * ctype1, char * ctype2, char * ctype3, int x) {

    /* This function looks for constituents of type ctype1. Say it finds 
       one, call it c1. It searches for the next larger constituent of 
       type ctype2, call it c2. It then generates a new constituent of 
       ctype3, containing all the words in c2 but not c1. */

    int w, w2, w3, c, c1, c2, done;
    c = numcon_total + numcon_subl;

    for (c1=numcon_total; c1<numcon_total + numcon_subl; c1++) {    

	/* If ctype1 is NP, it has to be an appositive to continue */
	if ((x==4) && (post_process_match("MX#*", constituent[c1].start_link)==0)) 
	    continue; 

	/* If ctype1 is X, and domain_type is t, it's an infinitive - skip it */
	if ((x==2) && (constituent[c1].domain_type=='t')) 
	    continue;

	/* If it's domain-type z, it's a subject-relative clause; 
	   the VP doesn't need an NP */
	if (constituent[c1].domain_type=='z') 
	    continue;

	/* If ctype1 is X or VP, and it's not started by an S, don't generate an NP
	 (Neither of the two previous checks are necessary now, right?) */
	if ((x==1 || x==2) && 
	    (((post_process_match("S", constituent[c1].start_link)==0) &&
	      (post_process_match("SX", constituent[c1].start_link)==0) &&
	      (post_process_match("SF", constituent[c1].start_link)==0)) ||
	     (post_process_match("S##w", constituent[c1].start_link)!=0))) 
	    continue;

	/* If it's an SBAR (relative clause case), it has to be a relative clause */
	if ((x==3) && 
	    ((post_process_match("Rn", constituent[c1].start_link)==0) &&
	     (post_process_match("R*", constituent[c1].start_link)==0) &&
	     (post_process_match("MX#r", constituent[c1].start_link)==0) &&
	     (post_process_match("Mr", constituent[c1].start_link)==0) &&
	     (post_process_match("MX#d", constituent[c1].start_link)==0)))
	    continue;

	/* If ctype1 is SBAR (clause opener case), it has to be an f domain */
	if ((x==5) && (constituent[c1].domain_type!='f'))
	    continue;

	/* If ctype1 is SBAR (pp opener case), it has to be a g domain */
	if ((x==6) && (constituent[c1].domain_type!='g')) 
	    continue;

	/* If ctype1 is NP (paraphrase case), it has to be started by an SI */
	if ((x==7) && (post_process_match("SI", constituent[c1].start_link)==0)) 
	    continue;

	/* If ctype1 is VP (participle modifier case), it has to be 
	   started by an Mv or Mg */
	if ((x==8) && (post_process_match("M", constituent[c1].start_link)==0)) 
	    continue;

	/* If ctype1 is VP (participle opener case), it has 
	   to be started by a COp */
	if ((x==9) && (post_process_match("COp", constituent[c1].start_link)==0)) 
	    continue;

	/* Now start at the bounds of c1, and work outwards until you 
	   find a larger constituent of type ctype2 */
	if (!(strcmp(constituent[c1].type, ctype1)==0)) 
	    continue;

	if (verbosity>=2) 
	    printf("Generating complement constituent for c %d of type %s\n", 
		   c1, ctype1);
	done=0;
	for (w2=constituent[c1].left; (done==0) && (w2>=0); w2--) {
	    for (w3=constituent[c1].right; w3<linkage->num_words; w3++) {
		for (c2=numcon_total; (done==0) && 
			 (c2 < numcon_total + numcon_subl); c2++) {
		    if (!((constituent[c2].left==w2) && 
			  (constituent[c2].right==w3)) || (c2==c1))
			continue;
		    if (!(strcmp(constituent[c2].type, ctype2)==0)) 
			continue;
		    
		    /* if the new constituent (c) is to the left 
		       of c1, its right edge should be adjacent to the 
		       left edge of c1 - or as close as possible 
		       without going outside the current sublinkage. 
		       (Or substituting right and left as necessary.) */
		    
		    if ((x==5) || (x==6) || (x==9)) {    
				/* This is the case where c is to the 
				   RIGHT of c1 */
			w=constituent[c1].right+1;
			while(1) {
			    if (word_used[linkage->current][w]==1) 
				break;
			    w++;
			}
			if (w>constituent[c2].right) {
			    done=1;
			    continue;
			}
			constituent[c].left=w;
			constituent[c].right=constituent[c2].right;
		    }
		    else {
			w=constituent[c1].left-1;
			while(1) {
			    if (word_used[linkage->current][w]==1) 
				break;
			    w--;
			}
			if (w<constituent[c2].left) {
			    done=1;
			    continue;
			}
			constituent[c].right=w;
			constituent[c].left=constituent[c2].left;
		    }
		    
		    adjust_for_left_comma(linkage, c1);
		    adjust_for_right_comma(linkage, c1);
		    
		    constituent[c].type = 
			string_set_add(ctype3, phrase_ss);
		    constituent[c].domain_type = 'x';
		    constituent[c].start_link = 
			string_set_add("XX", phrase_ss);
		    constituent[c].start_num = 
			constituent[c1].start_num; /* bogus */
		    if (verbosity>=2) {
			printf("Larger c found: c %d (%s); ", 
			       c2, ctype2); 
			printf("Adding constituent:\n");
			print_constituent(linkage, c); 
		    }
		    c++;
		    assert(c < MAXCONSTITUENTS, "Too many constituents");
		    done=1;
		}
	    }
	}
	if (verbosity>=2) {
	  if (done==0) 
	    printf("No constituent added, because no larger %s " \
		   " was found\n", ctype2);
	}
    }
    numcon_subl = c - numcon_total;
    return numcon_subl;
}

void adjust_subordinate_clauses(Linkage linkage, 
				int numcon_total, 
				int numcon_subl) {

  /* Look for a constituent started by an MVs or MVg. 
     Find any VP's or ADJP's that contain it (without going 
     beyond a larger S or NP). Adjust them so that 
     they end right before the m domain starts. */

    int c, w, c2, w2, done;

    for (c=numcon_total; c<numcon_total + numcon_subl; c++) {
	if ((post_process_match("MVs", constituent[c].start_link)==1) ||
	    (post_process_match("MVg", constituent[c].start_link)==1)) {
	    done=0;
	    for (w2=constituent[c].left-1; (done==0) && w2>=0; w2--) {
		for (c2=numcon_total; c2<numcon_total + numcon_subl; c2++) {
		    if (!((constituent[c2].left==w2) && 
			  (constituent[c2].right >= constituent[c].right)))
			continue;
		    if ((strcmp(constituent[c2].type, "S")==0) || 
			(strcmp(constituent[c2].type, "NP")==0)) {
			done=1;
			break;
		    }
		    if ((constituent[c2].domain_type=='v') || 
			(constituent[c2].domain_type=='a')) {
			w = constituent[c].left-1;
			while (1) {
			    if (word_used[linkage->current][w]==1) break;
			    w--;
			}
			constituent[c2].right = w;
			
			if (verbosity>=2) 
			    printf("Adjusting constituent %d:\n", c2);
			print_constituent(linkage, c2);
		    }
		}
	    }
	    if (strcmp(linkage->word[constituent[c].left], ",")==0) 
		constituent[c].left++;
	}      
    }
}

static void print_constituent(Linkage linkage, int c) {
    int w;
    /* Sentence sent;
       sent = linkage_get_sentence(linkage); **PV* using linkage->word not sent->word */
    if (verbosity<2) return;
    printf("  c %2d %4s [%c] (%2d-%2d): ", 
	   c, constituent[c].type, constituent[c].domain_type, 
	   constituent[c].left, constituent[c].right);
    for (w=constituent[c].left; w<=constituent[c].right; w++) {
	printf("%s ", linkage->word[w]); /**PV**/
    }
    printf("\n");
}

static void adjust_for_left_comma(Linkage linkage, int c) {
 
    /* If a constituent c has a comma at either end, we exclude the
       comma. (We continue to shift the boundary until we get to
       something inside the current sublinkage) */
    int w;
    
    w=constituent[c].left;
    if (strcmp(linkage->word[constituent[c].left], ",")==0) {
	w++;
	while (1) {
	    if (word_used[linkage->current][w]==1) break;
	    w++;
	}
    }
    constituent[c].left = w;
}

static void adjust_for_right_comma(Linkage linkage, int c) {

    int w;
    w=constituent[c].right;
    if ((strcmp(linkage->word[constituent[c].right], ",")==0) || 
	(strcmp(linkage->word[constituent[c].right], "RIGHT-WALL")==0)) {
	w--;
	while (1) {
	    if (word_used[linkage->current][w]==1) break;
	    w--;
	}
    }
    constituent[c].right = w;
}

/**********************************************************/
/* These functions are called once, after constituents    */
/* for each sublinkage have been generated, to merge them */
/*        together and fix up some other things           */
/*                                                        */
/**********************************************************/

static int merge_constituents(Linkage linkage, int numcon_total) {

    int c1, c2=0, c3, ok, a, n, a2, n2, match, listmatch, a3;
    int num_lists, num_elements;
    int leftend, rightend;


    for (c1=0; c1<numcon_total; c1++) {
	constituent[c1].valid = 1;
	/* Find and invalidate any constituents with negative length */
	if(constituent[c1].right<constituent[c1].left) {
	  if(verbosity>=2) printf("WARNING: Constituent %d has negative length. Deleting it.\n", c1);
	  constituent[c1].valid=0;
	}
	constituent[c1].canon=c1;
    }

    /* First go through and give each constituent a canonical number 
       (the index number of the lowest-numbered constituent 
       identical to it) */

    for (c1=0; c1<numcon_total; c1++) {
	if (constituent[c1].canon!=c1) continue;
	for (c2=c1+1; c2<numcon_total; c2++) {
	    if ((constituent[c1].left == constituent[c2].left) && 
		(constituent[c1].right == constituent[c2].right) && 
		(strcmp(constituent[c1].type, constituent[c2].type)==0)) {
		constituent[c2].canon=c1;
	    }
	}
    }

    /* If constituents A and B in different sublinkages X and Y 
       have one endpoint in common, but A is larger at the other end, 
       and B has no duplicate in X, then declare B invalid. (Example: 
       " [A [B We saw the cat B] and the dog A] " */

    for (c1=0; c1<numcon_total; c1++) {
	if (constituent[c1].valid==0) continue;
	for (c2=0; c2<numcon_total; c2++) {
	    if (constituent[c2].subl == constituent[c1].subl) continue;
	    ok=1;
	/* Does c2 have a duplicate in the sublinkage containing c1? 
	   If so, bag it */
	    for (c3=0; c3<numcon_total; c3++) {
		if ((constituent[c2].canon == constituent[c3].canon) && 
		    (constituent[c3].subl == constituent[c1].subl))
		    ok=0;
	    }
	    for (c3=0; c3<numcon_total; c3++) {
		if ((constituent[c1].canon == constituent[c3].canon) && 
		    (constituent[c3].subl == constituent[c2].subl)) 
		    ok=0;
	    }
	    if (ok==0) continue;
	    if ((constituent[c1].left == constituent[c2].left) && 
		(constituent[c1].right > constituent[c2].right) && 
		(strcmp(constituent[c1].type, constituent[c2].type)==0)) {
		constituent[c2].valid=0;
	    }

	    if ((constituent[c1].left < constituent[c2].left) && 
		(constituent[c1].right == constituent[c2].right) && 
		(strcmp(constituent[c1].type, constituent[c2].type)==0)) {
		constituent[c2].valid=0;
	    }
	}
    }

    /* Now go through and find duplicates; if a pair is found, 
       mark one as invalid. (It doesn't matter if they're in the 
       same sublinkage or not) */

    for (c1=0; c1<numcon_total; c1++) {
	if (constituent[c1].valid==0) continue;
	for (c2=c1+1; c2<numcon_total; c2++) {
	    if (constituent[c2].canon == constituent[c1].canon) 
		constituent[c2].valid=0;
	}
    }

    /* Now we generate the and-lists. An and-list is a set of mutually 
       exclusive constituents. Each constituent in the list may not 
       be present in the same sublinkage as any of the others. */

    num_lists=0;
    for (c1=0; c1<numcon_total; c1++) {
	if (constituent[c1].valid==0) continue;
	num_elements=1;
	templist[0]=c1;
	num_lists = 
	    find_next_element(linkage, c1, numcon_total, 
			      num_elements, num_lists);
    }

    if (verbosity>=2) {
	printf("And-lists:\n");
	for (n=0; n<num_lists; n++) {
	    printf("  %d: ", n);
	    for (a=0; a<andlist[n].num; a++) {
		printf("%d ", andlist[n].e[a]);
	    }
	    printf("\n");
	}
    } 

    /* Now we prune out any andlists that are subsumed by other 
       andlists--e.g. if andlist X contains constituents A and B, 
       and Y contains A B and C, we throw out X */

    for (n=0; n<num_lists; n++) {
	andlist[n].valid=1;
	for (n2=0; n2<num_lists; n2++) {
	    if (n2==n) continue;
	    if (andlist[n2].num < andlist[n].num) 
		continue;
	    listmatch=1;
	    for (a=0; a<andlist[n].num; a++) {
		match=0;
		for (a2=0; a2<andlist[n2].num; a2++) {
		    if (andlist[n2].e[a2]==andlist[n].e[a]) 
			match=1;
		}
		if (match==0) listmatch=0; 
		/* At least one element was not matched by n2 */
	    }
	    if (listmatch==1) andlist[n].valid=0;
	}
    } 

    /* If an element of an andlist contains an element of another 
       andlist, it must contain the entire andlist. */

    for (n=0; n<num_lists; n++) {
	if (andlist[n].valid==0) 
	    continue;
	for (a=0; (a<andlist[n].num) && (andlist[n].valid); a++) {
	    for (n2=0; (n2<num_lists) && (andlist[n].valid); n2++) {
		if ((n2==n) || (andlist[n2].valid==0)) 
		    continue;
		for (a2=0; (a2<andlist[n2].num) && (andlist[n].valid); a2++) {
		    c1=andlist[n].e[a];
		    c2=andlist[n2].e[a2];
		    if (c1==c2) 
			continue;
		    if (!((constituent[c2].left<=constituent[c1].left) && 
			  (constituent[c2].right>=constituent[c1].right))) 
			continue;
		    if (verbosity>=2) 
			printf("Found that c%d in list %d is bigger " \
			       "than c%d in list %d\n", c2, n2, c1, n);
		    ok=1;

		    /* An element of n2 contains an element of n. 
		       Now, we check to see if that element of n2 
		       contains ALL the elements of n. 
		       If not, n is invalid. */
		    
		    for (a3=0; a3<andlist[n].num; a3++) {
			c3=andlist[n].e[a3];
			if ((constituent[c2].left>constituent[c3].left) || 
			    (constituent[c2].right<constituent[c3].right)) 
			    ok=0;
		    } 
		    if (ok != 0) 
			continue;
		    andlist[n].valid=0;
		    if (verbosity>=2) {
			printf("Eliminating andlist, " \
			       "n=%d, a=%d, n2=%d, a2=%d: ", 
			       n, a, n2, a2);
			for (a3=0; a3<andlist[n].num; a3++) {
			    printf("%d ", andlist[n].e[a3]);
			}
			printf("\n"); 
		    }
		}
	    }
	}
    }


    if (verbosity>=2) {
	printf("And-lists after pruning:\n");
	for (n=0; n<num_lists; n++) {
	    if (andlist[n].valid==0) 
		continue; 
	    printf("  %d: ", n);
	    for (a=0; a<andlist[n].num; a++) {
		printf("%d ", andlist[n].e[a]);
	    }
	    printf("\n");
	}
    } 

    c1=numcon_total;
    for (n=0; n<num_lists; n++) {
	if (andlist[n].valid==0) continue;
	leftend=256;
	rightend=-1;
	for (a=0; a<andlist[n].num; a++) {
	    c2=andlist[n].e[a];
	    if (constituent[c2].left<leftend) {
		leftend=constituent[c2].left;
	    }
	    if (constituent[c2].right>rightend) {
		rightend=constituent[c2].right;
	    }
	}
	
	constituent[c1].left=leftend;
	constituent[c1].right=rightend;
	constituent[c1].type = constituent[c2].type;
	constituent[c1].domain_type = 'x';
	constituent[c1].valid=1;
	constituent[c1].start_link = constituent[c2].start_link;  /* bogus */
	constituent[c1].start_num = constituent[c2].start_num;    /* bogus */

	/* If a constituent within the andlist is an aux (aux==1), 
	   set aux for the whole-list constituent to 2, also set 
	   aux for the smaller constituent to 2, meaning they'll both
	   be printed (as an "X"). (If aux is 2 for the smaller 
	   constituent going in, the same thing should be done, 
	   though I doubt this ever happens.) */

	for (a=0; a<andlist[n].num; a++) {
	    c2=andlist[n].e[a];
	    if ((constituent[c2].aux==1) || (constituent[c2].aux==2)) { 
		constituent[c1].aux=2;
		constituent[c2].aux=2;
	    }
	}
	
	if (verbosity>=2) 
	    printf("Adding constituent:\n");
	print_constituent(linkage, c1);
	c1++;
    }
    numcon_total=c1;
    return numcon_total;
}

static int find_next_element(Linkage linkage, 
			     int start, 
			     int numcon_total, 
			     int num_elements, 
			     int num_lists) {
    /* Here we're looking for the next andlist element to add on 
       to a conjectural andlist, stored in the array templist.
       We go through the constituents, starting at "start". */

    int c, a, ok, c2, c3, addedone=0, n;

    n = num_lists;
    for (c=start+1; c<numcon_total; c++) {
	if (constituent[c].valid==0) 
	    continue;
	if (strcmp(constituent[templist[0]].type, constituent[c].type)!=0) 
	    continue; 
	ok=1;

	/* We're considering adding constituent c to the andlist. 
	   If c is in the same sublinkage as one of the other andlist 
	   elements, don't add it. If it overlaps with one of the other 
	   constituents, don't add it. If there's a constituent
	   identical to c that occurs in a sublinkage in which one of 
	   the other elements occurs, don't add it. */

	for (a=0; a<num_elements; a++) {
	    if (constituent[c].subl==constituent[templist[a]].subl) 
		ok=0;
	    if (((constituent[c].left<constituent[templist[a]].left) && 
		 (constituent[c].right>constituent[templist[a]].left))
		|| 
		((constituent[c].right>constituent[templist[a]].right) && 
		 (constituent[c].left<constituent[templist[a]].right))
		|| 
		((constituent[c].right>constituent[templist[a]].right) && 
		 (constituent[c].left<constituent[templist[a]].right))
		|| 
		((constituent[c].left>constituent[templist[a]].left) && 
		 (constituent[c].right<constituent[templist[a]].right)))
		ok=0;
	    for (c2=0; c2<numcon_total; c2++) {
		if (constituent[c2].canon != constituent[c].canon) 
		    continue;
		for (c3=0; c3<numcon_total; c3++) {
		    if ((constituent[c3].canon==constituent[templist[a]].canon)
			&& (constituent[c3].subl==constituent[c2].subl))
			ok=0;
		}
	    }
	}
	if (ok==0) continue;
	templist[num_elements]=c;
	addedone=1;
	num_lists = find_next_element(linkage, c, numcon_total, 
				      num_elements+1, num_lists);
    }
    if (addedone==0 && num_elements>1) {
	for (a=0; a<num_elements; a++) {
	    andlist[num_lists].e[a]=templist[a];
	    andlist[num_lists].num=num_elements;
	}
	num_lists++;
    }
    return num_lists;
}

static void generate_misc_word_info(Linkage linkage) {

    /* Go through all the words. If a word is on the right end of 
       an S (or SF or SX), wordtype[w]=STYPE.  If it's also on the left end of a 
       Pg*b, I, PP, or Pv, wordtype[w]=PTYPE. If it's a question-word 
       used in an indirect question, wordtype[w]=QTYPE. If it's a 
       question-word determiner,  wordtype[w]=QDTYPE. Else wordtype[w]=NONE. 
       (This function is called once for each sublinkage.) */
     
    int l1, l2, w1, w2;
    char * label1, * label2;

    for (w1=0; w1<linkage->num_words; w1++) 
	wordtype[w1]=NONE;

    for (l1=0; l1<linkage_get_num_links(linkage); l1++) {      
	w1=linkage_get_link_rword(linkage, l1);
	label1 = linkage_get_link_label(linkage, l1);
	if ((uppercompare(label1, "S")==0) || 
	    (uppercompare(label1, "SX")==0) || 
	    (uppercompare(label1, "SF")==0)) {
	    wordtype[w1] = STYPE;
	    for (l2=0; l2<linkage_get_num_links(linkage); l2++) {
		w2=linkage_get_link_lword(linkage, l2);
		label2 = linkage_get_link_label(linkage, l2);
		if ((w1==w2) && 
		    ((post_process_match("Pg#b", label2)==1) || 
		     (uppercompare(label2, "I")==0) || 
		     (uppercompare(label2, "PP")==0) || 
		     (post_process_match("Pv", label2)==1))) {
		    /* Pvf, Pgf? */
		    wordtype[w1] = PTYPE;
		}
	    }
	}
	if (post_process_match("QI#d", label1)==1) {
	    wordtype[w1] = QTYPE;
	    for (l2=0; l2<linkage_get_num_links(linkage); l2++) {
		w2=linkage_get_link_lword(linkage, l2);
		label2 = linkage_get_link_label(linkage, l2);
		if ((w1==w2) && (post_process_match("D##w", label2)==1)) {
		    wordtype[w1] = QDTYPE;
		}
	    }
	}
	if (post_process_match("Mr", label1)==1) wordtype[w1] = QDTYPE;
	if (post_process_match("MX#d", label1)==1) wordtype[w1] = QDTYPE;
    }
}

static int last_minute_fixes(Linkage linkage, int numcon_total) {

    int c, c2, global_leftend_found, adjustment_made,
	global_rightend_found, lastword, newcon_total=0;
    Sentence sent;
    sent = linkage_get_sentence(linkage);

    for (c=0; c<numcon_total; c++) {

	/* In a paraphrase construction ("John ran, he said"), 
	   the paraphrasing clause doesn't get
	   an S. (This is true in Treebank II, not Treebank I) */

	if (uppercompare(constituent[c].start_link, "CP")==0) {
	    constituent[c].valid = 0;
	}

	/* If it's a possessive with an "'s", the NP on the left 
	   should be extended to include the "'s". */
	if ((uppercompare(constituent[c].start_link, "YS")==0) || 
	    (uppercompare(constituent[c].start_link, "YP")==0)) {
	    constituent[c].right++;
	}

	/* If a constituent has starting link MVpn, it's a time 
	   expression like "last week"; label it as a noun phrase 
	   (incorrectly) */

	if (strcmp(constituent[c].start_link, "MVpn")==0) {
	    constituent[c].type = string_set_add("NP", phrase_ss);
	}
	if (strcmp(constituent[c].start_link, "COn")==0) {
	    constituent[c].type = string_set_add("NP", phrase_ss);
	}
	if (strcmp(constituent[c].start_link, "Mpn")==0) {
	    constituent[c].type = string_set_add("NP", phrase_ss);
	}

	/* If the constituent is an S started by "but" or "and" at 
	   the beginning of the sentence, it should be ignored. */

	if ((strcmp(constituent[c].start_link, "Wdc")==0) && 
	    (constituent[c].left==2)) {
	    constituent[c].valid = 0;
	}

	/* For prenominal adjectives, an ADJP constituent is assigned 
	   if it's a hyphenated (Ah) or comparative (Am) adjective; 
	   otherwise no ADJP is assigned, unless the phrase is more
	   than one word long (e.g. "very big"). The same with certain 
	   types of adverbs. */
	/* That was for Treebank I. For Treebank II, the rule only 
	   seems to apply to prenominal adjectives (of all kinds). 
	   However, it also applies to number expressions ("QP"). */

	if ((post_process_match("A", constituent[c].start_link)==1) || 
	    (constituent[c].domain_type=='d') || 
	    (constituent[c].domain_type=='h')) {
	    if (constituent[c].right-constituent[c].left==0) {
		constituent[c].valid=0;
	    }
	}

	if ((constituent[c].domain_type=='h') && 
	    (strcmp(linkage->word[constituent[c].left-1], "$")==0)) {
	    constituent[c].left--;
	}

	/* If a constituent has type VP and its aux value is 2, 
	   this means it's an aux that should be printed; change its 
	   type to "X". If its aux value is 1, set "valid" to 0. (This
	   applies to Treebank I only) */

	if (constituent[c].aux==2) {
	    constituent[c].type = string_set_add("X", phrase_ss);
	}
	if (constituent[c].aux==1) {
	    constituent[c].valid=0;
	}
    }

    numcon_total = numcon_total + newcon_total;

    /* If there's a global S constituent that includes everything 
       except a final period or question mark, extend it by one word */

    for (c=0; c<numcon_total; c++) {
	if ((constituent[c].right==(linkage->num_words)-3) && 
	    (constituent[c].left==1) && 
	    (strcmp(constituent[c].type, "S")==0) && 
	    (strcmp(sent->word[(linkage->num_words)-2].string, ".")==0)) 
	    constituent[c].right++;
    }

    /* If there's no S boundary at the very left end of the sentence, 
       or the very right end, create a new S spanning the entire sentence */

    lastword=(linkage->num_words)-2;
    global_leftend_found = 0;
    global_rightend_found = 0;
    for (c=0; c<numcon_total; c++) {
	if ((constituent[c].left==1) && (strcmp(constituent[c].type, "S")==0) && 
	    (constituent[c].valid==1))
	    global_leftend_found=1;
    }
    for (c=0; c<numcon_total; c++) {
	if ((constituent[c].right>=lastword) && 
	    (strcmp(constituent[c].type, "S")==0) && (constituent[c].valid==1)) 
	    global_rightend_found=1;
    }
    if ((global_leftend_found==0) || (global_rightend_found==0)) {
	c=numcon_total;
	constituent[c].left=1;
	constituent[c].right=linkage->num_words-1;
	constituent[c].type = string_set_add("S", phrase_ss);
	constituent[c].valid=1;
	constituent[c].domain_type = 'x';
	numcon_total++;
	if (verbosity>=2) 
	    printf("Adding global sentence constituent:\n");
	print_constituent(linkage, c);
    }

    /* Check once more to see if constituents are nested (checking BETWEEN sublinkages
       this time) */

    while (1) {
	adjustment_made=0;
	for (c=0; c<numcon_total; c++) {
	    if(constituent[c].valid==0) continue;
	    for (c2=0; c2<numcon_total; c2++) {
                if(constituent[c2].valid==0) continue;
		if ((constituent[c].left < constituent[c2].left) && 
		    (constituent[c].right < constituent[c2].right) && 
		    (constituent[c].right >= constituent[c2].left)) {

		    if (verbosity>=2) {
		      printf("WARNING: the constituents aren't nested! Adjusting them." \
			       "(%d, %d)\n", c, c2);
		      }
		    constituent[c].left = constituent[c2].left;
		}
	    }
	}
	if (adjustment_made==0) break;
    }
    return numcon_total;
}

static void count_words_used(Linkage linkage) {

    /*This function generates a table, word_used[i][w], showing 
      whether each word w is used in each sublinkage i; if so, 
      the value for that cell of the table is 1 */

    int i, w, link, num_subl;

    num_subl = linkage->num_sublinkages;
    if(linkage->unionized==1 && num_subl>1) num_subl--;

    if (verbosity>=2) 
	printf("Number of sublinkages = %d\n", num_subl); 
    for (i=0; i<num_subl; i++) {
	for (w=0; w<linkage->num_words; w++) word_used[i][w]=0;
	linkage->current=i;
	for (link=0; link<linkage_get_num_links(linkage); link++) {
	    word_used[i][linkage_get_link_lword(linkage, link)]=1;
	    word_used[i][linkage_get_link_rword(linkage, link)]=1;
	}
	if (verbosity>=2) {
	    printf("Sublinkage %d: ", i);
	    for (w=0; w<linkage->num_words; w++) {
		if (word_used[i][w]==0) printf("0 ");
		if (word_used[i][w]==1) printf("1 ");
	    }
	    printf("\n");
	}
    }
}

static int r_limit=0;

static void add_constituent(int * cons, Linkage linkage, Domain domain, 
		     int l, int r, char * name) {
    int c = *cons;
    c++;

    /* Avoid running off end to walls **PV**/
    if( l<1 ) l=1; 
    if( r>r_limit ) r=r_limit;
    assert( l<=r, "negative constituent length!" );

    constituent[c].left = l;
    constituent[c].right = r;
    constituent[c].domain_type = domain.type;
    constituent[c].start_link = 
	linkage_get_link_label(linkage, domain.start_link);
    constituent[c].start_num = domain.start_link;
    constituent[c].type = string_set_add(name, phrase_ss);
    *cons = c;
}

static char * cons_of_domain(char domain_type) {
    switch (domain_type) {
    case 'a':
	return "ADJP";
    case 'b':
	return "SBAR";
    case 'c':
	return "VP";
    case 'd':
	return "QP";
    case 'e':
	return "ADVP";
    case 'f':
	return "SBAR";
    case 'g':
	return "PP";
    case 'h':
	return "QP";
    case 'i':
	return "ADVP";
    case 'k':
	return "PRT";
    case 'n':
	return "NP";
    case 'p':
	return "PP";
    case 'q':
	return "SINV";
    case 's':
	return "S";
    case 't':
	return "VP";
    case 'u':
	return "ADJP";
    case 'v':
	return "VP";
    case 'y':
	return "NP";
    case 'z':
	return "VP";
    default:
	printf("Illegal domain: %c\n", domain_type);
	assert(0, "Illegal domain");
    }
}

static int read_constituents_from_domains(Linkage linkage, 
					  int numcon_total, int s) {

    int d, c, leftlimit, l, leftmost, rightmost, w, c2, numcon_subl=0, w2;
    List_o_links * dlink;
    int rootright, rootleft, adjustment_made;
    Sublinkage * subl;
    char * name;
    Domain domain;

    r_limit = linkage->num_words-2; /**PV**/

    subl = &linkage->sublinkage[s];

    for (d=0, c=numcon_total; d<subl->pp_data.N_domains; d++, c++) {    
	domain = subl->pp_data.domain_array[d];
	rootright = linkage_get_link_rword(linkage, domain.start_link);
	rootleft =  linkage_get_link_lword(linkage, domain.start_link);

	if ((domain.type=='c') ||
	    (domain.type=='d') ||
	    (domain.type=='e') || 
	    (domain.type=='f') || 
	    (domain.type=='g') || 
	    (domain.type=='u') || 
	    (domain.type=='y')) {
	    leftlimit = 0;
	    leftmost = linkage_get_link_lword(linkage, domain.start_link);
	    rightmost = linkage_get_link_lword(linkage, domain.start_link);
	}
	else {
	    leftlimit = linkage_get_link_lword(linkage, domain.start_link)+1;
	    leftmost = linkage_get_link_rword(linkage, domain.start_link);
	    rightmost = linkage_get_link_rword(linkage, domain.start_link);
	}

	/* Start by assigning both left and right limits to the 
	   right word of the start link. This will always be contained 
	   in the constituent. This will also handle the case 
	   where the domain contains no links. */

	for (dlink = domain.lol; dlink!=NULL; dlink=dlink->next) {
	    l=dlink->link;

	    if ((linkage_get_link_lword(linkage, l) < leftmost) && 
		(linkage_get_link_lword(linkage, l) >= leftlimit)) 
		leftmost = linkage_get_link_lword(linkage, l);

	    if (linkage_get_link_rword(linkage, l) > rightmost) 
		rightmost = linkage_get_link_rword(linkage, l);
	}

	c--;
	add_constituent(&c, linkage, domain, leftmost, rightmost, 
			cons_of_domain(domain.type)); 

	if (domain.type=='z') {
	    add_constituent(&c, linkage, domain, leftmost, rightmost, "S");
	}
	if (domain.type=='c') {
	    add_constituent(&c, linkage, domain, leftmost, rightmost, "S");
	}
	if ((post_process_match("Ce*", constituent[c].start_link)==1) ||
	    (post_process_match("Rn", constituent[c].start_link)==1)) {
	    add_constituent(&c, linkage, domain, leftmost, rightmost, "SBAR");
	}
	if ((post_process_match("R*", constituent[c].start_link)==1) ||
	    (post_process_match("MX#r", constituent[c].start_link)==1)) {
	    w=leftmost;
	    if (strcmp(linkage->word[w], ",")==0) w++;
	    add_constituent(&c, linkage, domain, w, w, "WHNP");
	}
	if (post_process_match("Mj", constituent[c].start_link)==1) {
	    w=leftmost;
	    if (strcmp(linkage->word[w], ",")==0) w++;
	    add_constituent(&c, linkage, domain, w, w+1, "WHPP");
	    add_constituent(&c, linkage, domain, w+1, w+1, "WHNP");
	}
	if ((post_process_match("Ss#d", constituent[c].start_link)==1) ||
	    (post_process_match("B#d", constituent[c].start_link)==1)) {
	    add_constituent(&c, linkage, domain, rootleft, rootleft, "WHNP");
	    add_constituent(&c, linkage, domain, 
			    rootleft, constituent[c-1].right, "SBAR");
	}
	if (post_process_match("CP", constituent[c].start_link)==1) {
	    if (strcmp(linkage->word[leftmost], ",")==0) 
		constituent[c].left++; 
	    add_constituent(&c, linkage, domain, 1, linkage->num_words-1, "S");
	}
	if ((post_process_match("MVs", constituent[c].start_link)==1) || 
	    (domain.type=='f')) {
	    w=constituent[c].left;
	    if (strcmp(linkage->word[w], ",")==0) 
		w++;
	    if (strcmp(linkage->word[w], "when")==0) {
		add_constituent(&c, linkage, domain, w, w, "WHADVP");
	    }
	}
	if (domain.type=='t') {
	    add_constituent(&c, linkage, domain, leftmost, rightmost, "S");
	} 
	if ((post_process_match("QI", constituent[c].start_link)==1) ||
	    (post_process_match("Mr", constituent[c].start_link)==1) ||
	    (post_process_match("MX#d", constituent[c].start_link)==1)) {
	    w=leftmost;
	    if (strcmp(linkage->word[w], ",")==0) w++;
	    if (wordtype[w] == NONE) 
		name = "WHADVP";
	    else if (wordtype[w] == QTYPE) 
		name = "WHNP";
	    else if (wordtype[w] == QDTYPE) 
		name = "WHNP";
	    else 
		assert(0, "Unexpected word type");
	    add_constituent(&c, linkage, domain, w, w, name);

	    if (wordtype[w] == QDTYPE) {
	        /* Now find the finite verb to the right, start an S */
	        /*PV* limited w2 to sentence len*/
		for( w2=w+1; w2 < r_limit-1; w2++ ) 
		  if ((wordtype[w2] == STYPE) || (wordtype[w2] == PTYPE)) break;
		/* adjust the right boundary of previous constituent */
		constituent[c].right = w2-1;
		add_constituent(&c, linkage, domain, w2, rightmost, "S");
	      }
	} 

	if (constituent[c].domain_type=='\0') {
	    error("Error: no domain type assigned to constituent\n");
	}
	if (constituent[c].start_link==NULL) {
	    error("Error: no type assigned to constituent\n");
	}
    }

    numcon_subl = c - numcon_total;
    /* numcon_subl = handle_islands(linkage, numcon_total, numcon_subl);  */

    if (verbosity>=2) 
	printf("Constituents added at first stage for subl %d:\n", 
	       linkage->current);
    for (c=numcon_total; c<numcon_total + numcon_subl; c++) {
	print_constituent(linkage, c);
    }

    /* Opener case - generates S around main clause. 
       (This must be done first; the S generated will be needed for 
       later cases.) */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "SBAR", "S", "S", 5);

    /* pp opener case */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "PP", "S", "S", 6);

    /* participle opener case */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "S", "S", "S", 9);

    /* Subject-phrase case; every main VP generates an S */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "VP", "S", "NP", 1);

    /* Relative clause case; an SBAR generates a complement NP */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "SBAR", "NP", "NP", 3);

    /* Participle modifier case */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "VP", "NP", "NP", 8);

    /* PP modifying NP */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "PP", "NP", "NP", 8);

    /* Appositive case */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "NP", "NP", "NP", 4);

    /* S-V inversion case; an NP generates a complement VP */
    numcon_subl = 
	gen_comp(linkage, numcon_total, numcon_subl, "NP", "SINV", "VP", 7);

    adjust_subordinate_clauses(linkage, numcon_total, numcon_subl);
    for (c=numcon_total; c<numcon_total + numcon_subl; c++) {
	if ((constituent[c].domain_type=='p') && 
	    (strcmp(linkage->word[constituent[c].left], ",")==0)) {
	    constituent[c].left++;
	}
    }

    /* Make sure the constituents are nested. If two constituents are not nested: whichever 
       constituent has the furthest left boundary, shift that boundary rightwards to the left 
       boundary of the other one */

    while (1) {
	adjustment_made=0;
	for (c=numcon_total; c<numcon_total + numcon_subl; c++) {
	    for (c2=numcon_total; c2<numcon_total + numcon_subl; c2++) {
		if ((constituent[c].left < constituent[c2].left) && 
		    (constituent[c].right < constituent[c2].right) && 
		    (constituent[c].right >= constituent[c2].left)) {
		    
		    /* We've found two overlapping constituents. 
		       If one is larger, except the smaller one
		       includes an extra comma, adjust the smaller one 
		       to exclude the comma */
		    
		    if ((strcmp(linkage->word[constituent[c2].right], ",")==0) || 
			(strcmp(linkage->word[constituent[c2].right], 
				"RIGHT-WALL")==0)) {
			if (verbosity>=2) 
			    printf("Adjusting %d to fix comma overlap\n", c2);
			adjust_for_right_comma(linkage, c2);
			adjustment_made=1;
		    }
		    else if (strcmp(linkage->word[constituent[c].left], ",")==0) {
			if (verbosity>=2) 
			    printf("Adjusting c %d to fix comma overlap\n", c);
			adjust_for_left_comma(linkage, c);
			adjustment_made=1;
		    }
		    else {
		      if (verbosity>=2) {
			printf("WARNING: the constituents aren't nested! Adjusting them." \
			       "(%d, %d)\n", c, c2);
		      }
		      constituent[c].left = constituent[c2].left;
		    }
		}
	    }
	}
	if (adjustment_made==0) break;
    }

    /* This labels certain words as auxiliaries (such as forms of "be" 
       with passives, forms of "have" wth past participles, 
       "to" with infinitives). These words start VP's which include
       them. In Treebank I, these don't get printed unless they're part of an 
       andlist, in which case they get labeled "X". (this is why we need to 
       label them as "aux".) In Treebank II, however, they seem to be treated 
       just like other verbs, so the "aux" stuff isn't needed. */


    for (c=numcon_total; c<numcon_total + numcon_subl; c++) {
	constituent[c].subl = linkage->current;
	if (((constituent[c].domain_type == 'v') && 
	    (wordtype[linkage_get_link_rword(linkage, 
					     constituent[c].start_num)]==PTYPE)) 
	   ||
	   ((constituent[c].domain_type == 't') && 
	    (strcmp(constituent[c].type, "VP")==0))) {
	    constituent[c].aux=1;
	}
	else constituent[c].aux=0;
    } 

    for (c=numcon_total; c<numcon_total + numcon_subl; c++) {
	constituent[c].subl = linkage->current;
	constituent[c].aux=0; 
    }
    
    return numcon_subl;
}

static char * exprint_constituent_structure(Linkage linkage, int numcon_total) {
    int c, w;
    int leftdone[MAXCONSTITUENTS];
    int rightdone[MAXCONSTITUENTS];
    int best, bestright, bestleft;
    Sentence sent; 
    char s[100], * p;
    String * cs = String_create();

    assert (numcon_total < MAXCONSTITUENTS, "Too many constituents");
    sent = linkage_get_sentence(linkage); 

    for(c=0; c<numcon_total; c++) {
	leftdone[c]=0;
	rightdone[c]=0;
    }

    if(verbosity>=2) 
	printf("\n");	      
	
    for(w=1; w<linkage->num_words; w++) {      
	/* Skip left wall; don't skip right wall, since it may 
	   have constituent boundaries */

	while(1) {
	    best = -1;
	    bestright = -1;
	    for(c=0; c<numcon_total; c++) {    
		if ((constituent[c].left==w) && 
		    (leftdone[c]==0) && (constituent[c].valid==1) &&
		    (constituent[c].right >= bestright)) { 
		    best = c;
		    bestright = constituent[c].right;
		}
	    }
	    if (best==-1) 
		break;
	    leftdone[best]=1;
	    if(constituent[best].aux==1) continue;
	    append_string(cs, "%c%s ", OPEN_BRACKET, constituent[best].type);
	}

	if (w<linkage->num_words-1) {  
	    /* Don't print out right wall */
	    strcpy(s, sent->word[w].string);
	    
	    /* Now, if the first character of the word was 
	       originally uppercase, we put it back that way */
	    if (sent->word[w].firstupper ==1 ) 
		s[0]=toupper(s[0]);
	    append_string(cs, "%s ", s);
	}
	
	while(1) {
	    best = -1;
	    bestleft = -1;
	    for(c=0; c<numcon_total; c++) {    
		if ((constituent[c].right==w) && 
		    (rightdone[c]==0) && (constituent[c].valid==1) &&
		    (constituent[c].left > bestleft)) {
		    best = c;
		    bestleft = constituent[c].left;
		}
	    }
	    if (best==-1) 
		break;
	    rightdone[best]=1;
	    if (constituent[best].aux==1) 
		continue;
	    append_string(cs, "%s%c ", constituent[best].type, CLOSE_BRACKET);
	}
    }
    
    append_string(cs, "\n"); 
    p = exalloc(strlen(cs->p)+1);
    strcpy(p, cs->p);
    exfree(cs->p, sizeof(char)*cs->allocated);
    exfree(cs, sizeof(String));
    return p;
}

static char * print_flat_constituents(Linkage linkage) {
    int num_words;
    Sentence sent;
    Postprocessor * pp;
    int s, numcon_total, numcon_subl, num_subl;
    char * q;

    sent = linkage_get_sentence(linkage); 
    phrase_ss = string_set_create();
    pp = linkage->sent->dict->constituent_pp;
    numcon_total = 0;

    count_words_used(linkage);

    num_subl = linkage->num_sublinkages;
    if(num_subl > MAXSUBL) {
      num_subl=MAXSUBL;
      if(verbosity>=2) printf("Number of sublinkages exceeds maximum: only considering first %d sublinkages\n", MAXSUBL);
    }
    if(linkage->unionized==1 && num_subl>1) num_subl--;
    for (s=0; s<num_subl; s++) {
	linkage_set_current_sublinkage(linkage, s);
	linkage_post_process(linkage, pp);
	num_words = linkage_get_num_words(linkage);
	generate_misc_word_info(linkage);
	numcon_subl = read_constituents_from_domains(linkage, numcon_total, s); 
	numcon_total = numcon_total + numcon_subl;
    }
    numcon_total = merge_constituents(linkage, numcon_total);
    numcon_total = last_minute_fixes(linkage, numcon_total); 
    q = exprint_constituent_structure(linkage, numcon_total);
    string_set_delete(phrase_ss);
    return q;
}

CType token_type (char *token) {
    if ((token[0]==OPEN_BRACKET) && (strlen(token)>1)) 
	return OPEN;
    if ((strlen(token)>1) && (token[strlen(token)-1]==CLOSE_BRACKET)) 
	return CLOSE;
    return WORD;
}

CNode * make_CNode(char *q) {
    CNode * cn;
    cn = exalloc(sizeof(CNode));
    cn->label = (char *) exalloc(sizeof(char)*(strlen(q)+1));
    strcpy(cn->label, q);
    cn->child = cn->next = (CNode *) NULL;
    cn->next = (CNode *) NULL;
    cn->start = cn->end = -1;
    return cn;
}

CNode * parse_string(CNode * n) {
    char *q;
    CNode *m, *last_child=NULL;

    while ((q=strtok(NULL, " "))) {
	switch (token_type(q)) {
	case CLOSE :
	    q[strlen(q)-1]='\0';
	    assert(strcmp(q, n->label)==0, 
		   "Constituent tree: Labels do not match.");
	    return n;
	    break;
	case OPEN:
	    m = make_CNode(q+1);
	    m = parse_string(m);
	    break;
	case WORD:
	    m = make_CNode(q);
	    break;
	default:
	    assert(0, "Constituent tree: Illegal token type");
	}
	if (n->child == NULL) {
	    last_child = n->child = m;
	}
	else {
	    last_child->next = m;
	    last_child = m;
	}
    }
    assert(0, "Constituent tree: Constituent did not close");
    return NULL;
}

static void print_tree(String * cs, int indent, CNode * n, int o1, int o2) {
    int i, child_offset;
    CNode * m;

    if (n==NULL) return;

    if (indent) 
	for (i=0; i<o1; ++i) 
	    append_string(cs, " ");
    append_string(cs, "(%s ", n->label);
    child_offset = o2 + strlen(n->label)+2;

    for (m=n->child; m!=NULL; m=m->next) {
	if (m->child == NULL) {
	    append_string(cs, "%s", m->label);
	    if ((m->next != NULL) && (m->next->child == NULL))
		append_string(cs, " ");
	}
	else {
	    if (m != n->child) {
		if (indent) append_string(cs, "\n");
		else append_string(cs, " ");
		print_tree(cs, indent, m, child_offset, child_offset);
	    }
	    else {
		print_tree(cs, indent, m, 0, child_offset);
	    }
	    if ((m->next != NULL) && (m->next->child == NULL)) {
		if (indent) {
		    append_string(cs, "\n");
		    for (i=0; i<child_offset; ++i) 
			append_string(cs, " ");
		}
		else append_string(cs, " ");
	    }
	}
    }
    append_string(cs, ")");
}

static int assign_spans(CNode * n, int start) {
    int num_words=0;
    CNode * m=NULL;
    if (n==NULL) return 0;
    n->start = start;
    if (n->child == NULL) {
	n->end = start;
	return 1;
    }
    else {
	for (m=n->child; m!=NULL; m=m->next) {
	    num_words += assign_spans(m, start+num_words);
	}
	n->end = start+num_words-1;
    }
    return num_words;
}

CNode * linkage_constituent_tree(Linkage linkage) {
    static char *p, *q;
    int len;
    CNode * root;
    p = print_flat_constituents(linkage);
    len = strlen(p);
    q = strtok(p, " ");
    assert(token_type(q) == OPEN, "Illegal beginning of string");
    root = make_CNode(q+1);
    root = parse_string(root);
    assign_spans(root, 0);
    exfree(p, sizeof(char)*(len+1));
    return root;
}

void linkage_free_constituent_tree(CNode * n) {
    CNode *m, *x;
    for (m=n->child; m!=NULL; m=x) {
	x=m->next;
	linkage_free_constituent_tree(m);
    }
    exfree(n->label, sizeof(char)*(strlen(n->label)+1));
    exfree(n, sizeof(CNode));
}


/* Print out the constituent tree.  
   mode 1: treebank-style constituent tree
   mode 2: flat, bracketed tree [A like [B this B] A] 
   mode 3: flat, treebank-style tree (A like (B this) ) */

char * linkage_print_constituent_tree(Linkage linkage, int mode) {
    String * cs;
    CNode * root;
    char * p;
    
    if ((mode == 0) || (linkage->sent->dict->constituent_pp == NULL)) {
	return NULL;
    } 
    else if (mode==1 || mode==3) {
	cs = String_create();
	root = linkage_constituent_tree(linkage);
	print_tree(cs, (mode==1), root, 0, 0);
	linkage_free_constituent_tree(root);
	append_string(cs, "\n");
	p = exalloc(strlen(cs->p)+1);
	strcpy(p, cs->p);
	exfree(cs->p, sizeof(char)*cs->allocated);
	exfree(cs, sizeof(String));
	return p;
    }
    else if (mode == 2) {
	return print_flat_constituents(linkage);
    }
    assert(0, "Illegal mode in linkage_print_constituent_tree");
    return NULL;
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/* This file contains the exhaustive search algorithm. */

static char ** deletable;
static char ** effective_dist;
static Word *  local_sent;
static int     null_block, islands_ok, null_links;
static Resources current_resources;

int x_match(Connector *a, Connector *b) {
    return match(a, b, 0, 0);
}

void count_set_effective_distance(Sentence sent) {
    effective_dist = sent->effective_dist;
}

void count_unset_effective_distance(Sentence sent) {
    effective_dist = NULL;
}

int match(Connector *a, Connector *b, int aw, int bw) {
  /* 
     Returns TRUE if s and t match according to the connector matching
     rules.  The connector strings must be properly formed, starting with
     zero or more upper case letters, followed by some other letters, and
     The algorithm is symmetric with respect to a and b.
     
     It works as follows:  The labels must match.  The priorities must be
     compatible (both THIN_priority, or one UP_priority and one DOWN_priority).
     The sequence of upper case letters must match exactly.  After these comes
     a sequence of lower case letters "*"s or "^"s.  The matching algorithm
     is different depending on which of the two priority cases is being
     considered.  See the comments below.  */
    char *s, *t;
    int x, y, dist;
    if (a->label != b->label) return FALSE;
    x = a->priority;
    y = b->priority;
    
    s = a->string;
    t = b->string;
    
    while(isupper((int)*s) || isupper((int)*t)) {
	if (*s != *t) return FALSE;
	s++;
	t++;
    }

    if (aw==0 && bw==0) {  /* probably not necessary, as long as effective_dist[0][0]=0 and is defined */
	dist = 0;
    } else {
	assert(aw < bw, "match() did not receive params in the natural order.");
	dist = effective_dist[aw][bw];
    }
    /*    printf("M: a=%4s b=%4s  ap=%d bp=%d  aw=%d  bw=%d  a->ll=%d b->ll=%d  dist=%d\n",
	   s, t, x, y, aw, bw, a->length_limit, b->length_limit, dist); */
    if (dist > a->length_limit || dist > b->length_limit) return FALSE;
    
    if ((x==THIN_priority) && (y==THIN_priority)) {
	/*
	   Remember that "*" matches anything, and "^" matches nothing
	   (except "*").  Otherwise two characters match if and only if
	   they're equal.  ("^" can be used in the dictionary just like
	   any other connector.)
	   */
	while ((*s!='\0') && (*t!='\0')) {
	    if ((*s == '*') || (*t == '*') ||
		((*s == *t) && (*s != '^'))) {
		s++;
		t++;
	    } else return FALSE;
	}
	return TRUE;
    } else if ((x==UP_priority) && (y==DOWN_priority)) {
	/*
	   As you go up (namely from x to y) the set of strings that
	   match (in the normal THIN sense above) should get no larger.
	   Read the comment in and.c to understand this.
	   In other words, the y string (t) must be weaker (or at least
	   no stronger) that the x string (s).
	   
	   This code is only correct if the strings are the same
	   length.  This is currently true, but perhaps for safty
	   this assumption should be removed.
	   */
	while ((*s!='\0') && (*t!='\0')) {
	    if ((*s == *t) || (*s == '*') || (*t == '^')) {
		s++;
		t++;
	    } else return FALSE;
	}
	return TRUE;
    } else if ((y==UP_priority) && (x==DOWN_priority)) {
	while ((*s!='\0') && (*t!='\0')) {
	    if ((*s == *t) || (*t == '*') || (*s == '^')) {
		s++;
		t++;
	    } else return FALSE;
	}
	return TRUE;
    } else return FALSE;
}

typedef struct Table_connector Table_connector;
struct Table_connector {
    short              lw, rw;
    Connector         *le, *re;
    short              cost;
    int                count;
    Table_connector   *next;
};

static int table_size;
static Table_connector ** table;

void init_table(Sentence sent) {
    /* A piecewise exponential function determines the size of the hash table.      */
    /* Probably should make use of the actual number of disjuncts, rather than just */
    /* the number of words                                                          */
    int i;
    if (sent->length >= 10) {
	table_size = (1<<16);
	/*  } else if (sent->length >= 10) {
	    table_size = (1 << (((6*(sent->length-10))/30) + 10)); */
    } else if (sent->length >= 4) {
	table_size = (1 << (((6*(sent->length-4))/6) + 4));
    } else {
	table_size = (1 << 4);
    }
    table = (Table_connector**) xalloc(table_size * sizeof(Table_connector*));
    for (i=0; i<table_size; i++) {
	table[i] = NULL;
    }
}

int hash(int lw, int rw, Connector *le, Connector *re, int cost) {
    int i;
    i = 0;
    
    i = i + (i<<1) + randtable[(lw + i) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(rw + i) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(((long) le + i) % (table_size+1)) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(((long) re + i) % (table_size+1)) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(cost+i) & (RTSIZE - 1)];
    return i & (table_size-1);
}

void free_table(Sentence sent) {
    int i;
    Table_connector *t, *x;
    
    for (i=0; i<table_size; i++) {
	for(t = table[i]; t!= NULL; t=x) {
	    x = t->next;
	    xfree((void *) t, sizeof(Table_connector));
	}
    }
    xfree((void *) table, table_size * sizeof(Table_connector*));
}

Table_connector * table_store(int lw, int rw, Connector *le, Connector *re, int cost, int count){
    /* Stores the value in the table.  Assumes it's not already there */
    Table_connector *t, *n;
    int h;
    
    n = (Table_connector *) xalloc(sizeof(Table_connector));
    n->count = count; 
    n->lw = lw; n->rw = rw; n->le = le; n->re = re; n->cost = cost;
    h = hash(lw, rw, le, re, cost);
    t = table[h];
    n->next = t;
    table[h] = n;
    return n;
}


Table_connector * table_pointer(int lw, int rw, Connector *le, Connector *re, int cost) {
    /* returns the pointer to this info, NULL if not there */
    Table_connector *t;
    t = table[hash(lw, rw, le, re, cost)];
    for (; t != NULL; t = t->next) {
	if ((t->lw == lw) && (t->rw == rw) && (t->le == le) && (t->re == re) 
	    && (t->cost == cost))  return t;
    }
    
    if ((current_resources != NULL) && 	resources_exhausted(current_resources)) {
	return table_store(lw, rw, le, re, cost, 0);
    }
    else return NULL;
}

int table_lookup(int lw, int rw, Connector *le, Connector *re, int cost) {
    /* returns the count for this quintuple if there, -1 otherwise */    
    Table_connector *t = table_pointer(lw, rw, le, re, cost);

    if (t == NULL) return -1; else return t->count;
}

void table_update(int lw, int rw, Connector *le, Connector *re, int cost, int count) {
    /* Stores the value in the table.  Unlike table_store, it assumes it's already there */
    Table_connector *t = table_pointer(lw, rw, le, re, cost);
    
    assert(t != NULL, "This entry is supposed to be in the table.");
    t->count = count;
}


int pseudocount(int lw, int rw, Connector *le, Connector *re, int cost) {
/* returns 0 if and only if this entry is in the hash table with a count value of 0 */
    int count;
    count = table_lookup(lw, rw, le, re, cost);
    if (count == 0) return 0; else return 1;
}

int count(int lw, int rw, Connector *le, Connector *re, int cost) {
    Disjunct * d;
    int total, pseudototal;
    int start_word, end_word, w;
    int leftcount, rightcount;
    int lcost, rcost, Lmatch, Rmatch;

    Match_node * m, *m1;
    Table_connector *t;

    if (cost < 0) return 0;  /* will we ever call it with cost<0 ? */

    t = table_pointer(lw, rw, le, re, cost);

    if (t == NULL) {
	t = table_store(lw, rw, le, re, cost, 0);  /* create the table entry with a tentative cost of 0 */
	                                           /* this cost must be updated before we return */
    } else {
	return t->count;
    }
    
    if (rw == 1+lw) {
	/* lw and rw are neighboring words */
	/* you can't have a linkage here with cost > 0 */
	if ((le == NULL) && (re == NULL) && (cost == 0)) {
	    t->count = 1;
	} else {
	    t->count = 0;
	}
	return t->count;
    }
    
    if ((le == NULL) && (re == NULL)) {
	if (!islands_ok && (lw != -1)) {
	  /* if we don't allow islands (a set of words linked together but
	     separate from the rest of the sentence) then  the cost of skipping
	     n words is just n */
	    if (cost == ((rw-lw-1)+null_block-1)/null_block) {  
		/* if null_block=4 then the cost of 
		   1,2,3,4 nulls is 1, 5,6,7,8 is 2 etc. */
		t->count = 1;
	    } else {
		t->count = 0;
	    }
	    return t->count;
	}
	if (cost == 0) {
	    /* there is no zero-cost solution in this case */
	    /* slight efficiency hack to separate this cost=0 case out */
	    /* but not necessary for correctness */
	    t->count = 0;
	} else {
	    total = 0;
	    w = lw+1;
	    for (d = local_sent[w].d; d != NULL; d = d->next) {
		if (d->left == NULL) {
		    total += count(w, rw, d->right, NULL, cost-1);
		}
	    }
	    total += count(w, rw, NULL, NULL, cost-1);
	    t->count = total;
	}
	return t->count;
    }
    
    if (le == NULL) {
	start_word = lw+1;
    } else {
	start_word = le->word;
    }

    if (re == NULL) {
	end_word = rw-1;
    } else {
	end_word = re->word;
    }

    total = 0;
    
    for (w=start_word; w <= end_word; w++) {
	m1 = m = form_match_list(w, le, lw, re, rw); 
	for (; m!=NULL; m=m->next) {
	    d = m->d;
	    for (lcost = 0; lcost <= cost; lcost++) {
		rcost = cost-lcost;
		/* now lcost and rcost are the costs we're assigning to those parts respectively */


		/* Now, we determine if (based on table only) we can see that
		   the current range is not parsable. */

		Lmatch = (le != NULL) && (d->left != NULL) && match(le, d->left, lw, w);
		Rmatch = (d->right != NULL) && (re != NULL) && match(d->right, re, w, rw);

		rightcount = leftcount = 0;
		if (Lmatch) {
		    leftcount = pseudocount(lw, w, le->next, d->left->next, lcost);
		    if (le->multi) leftcount += pseudocount(lw, w, le, d->left->next, lcost);
		    if (d->left->multi) leftcount += pseudocount(lw, w, le->next, d->left, lcost);
		    if (le->multi && d->left->multi) leftcount += pseudocount(lw, w, le, d->left, lcost);
		}
		
		if (Rmatch) {
		    rightcount = pseudocount(w, rw, d->right->next, re->next, rcost);
		    if (d->right->multi) rightcount += pseudocount(w,rw,d->right,re->next, rcost);
		    if (re->multi) rightcount += pseudocount(w, rw, d->right->next, re, rcost);
		    if (d->right->multi && re->multi) rightcount += pseudocount(w, rw, d->right, re, rcost);
		}

		pseudototal = leftcount*rightcount;  /* total number where links are used on both sides */

		if (leftcount > 0) {
		    /* evaluate using the left match, but not the right */
		    pseudototal += leftcount * pseudocount(w, rw, d->right, re, rcost);
		}
		if ((le == NULL) && (rightcount > 0)) {
		    /* evaluate using the right match, but not the left */
		    pseudototal += rightcount * pseudocount(lw, w, le, d->left, lcost);
		}

		/* now pseudototal is 0 implies that we know that the true total is 0 */
		if (pseudototal != 0) {
		    rightcount = leftcount = 0;
		    if (Lmatch) {
			leftcount = count(lw, w, le->next, d->left->next, lcost);
			if (le->multi) leftcount += count(lw, w, le, d->left->next, lcost);
			if (d->left->multi) leftcount += count(lw, w, le->next, d->left, lcost);
			if (le->multi && d->left->multi) leftcount += count(lw, w, le, d->left, lcost);
		    }

		    if (Rmatch) {
			rightcount = count(w, rw, d->right->next, re->next, rcost);
			if (d->right->multi) rightcount += count(w,rw,d->right,re->next, rcost);
			if (re->multi) rightcount += count(w, rw, d->right->next, re, rcost);
			if (d->right->multi && re->multi) rightcount += count(w, rw, d->right, re, rcost);
		    }

		    total += leftcount*rightcount;  /* total number where links are used on both sides */

		    if (leftcount > 0) {
			/* evaluate using the left match, but not the right */
			total += leftcount * count(w, rw, d->right, re, rcost);
		    }
		    if ((le == NULL) && (rightcount > 0)) {
			/* evaluate using the right match, but not the left */
			total += rightcount * count(lw, w, le, d->left, lcost);
		    }
		}
	    }
	}
	    
	put_match_list(m1);
    }
    t->count = total;
    return total;
}

int parse(Sentence sent, int cost, Parse_Options opts) {
    /* Returns the number of ways the sentence can be parsed with the
       specified cost Assumes that the hash table has already been
       initialized, and is freed later.  */
    int total;
    
    count_set_effective_distance(sent);
    current_resources = opts->resources;
    local_sent = sent->word;
    deletable = sent->deletable;
    null_block = opts->null_block;
    islands_ok = opts->islands_ok;

    total = count(-1, sent->length, NULL, NULL, cost+1);
    if (verbosity > 1) {
	printf("Total count with %d null links:   %d\n", cost, total);
    }
    if (total < 0) {
        printf("WARNING: Overflow in count!\n");
    }

    local_sent = NULL;
    current_resources = NULL;
    return total;
}

/*
   
   CONJUNCTION PRUNING.
   
   The basic idea is this.  Before creating the fat disjuncts,
   we run a modified version of the exhaustive search procedure.
   Its purpose is to mark the disjuncts that can be used in any
   linkage.  It's just like the normal exhaustive search, except that
   if a subrange of words are deletable, then we treat them as though
   they were not even there.  So, if we call the function in the
   situation where the set of words between the left and right one
   are deletable, and the left and right connector pointers
   are NULL, then that range is considered to have a solution.
   
   There are actually two procedures to implement this.  One is
   mark_region() and the other is region_valid().  The latter just
   checks to see if the given region can be completed (within it).
   The former actually marks those disjuncts that can be used in
   any valid linkage of the given region.
   
   As in the standard search procedure, we make use of the fast-match
   data structure (which requires power pruning to have been done), and
   we also use a hash table.  The table is used differently in this case.
   The meaning of values stored in the table are as follows:
   
   -1  Nothing known (Actually, this is not stored.  It's returned
   by table_lookup when nothing is known.)
   0  This region can't be completed (marking is therefore irrelevant)
   1  This region can be completed, but it's not yet marked
   2  This region can be completed, and it's been marked.
   */  

int region_valid(int lw, int rw, Connector *le, Connector *re) {
    /* Returns 0 if this range cannot be successfully filled in with           */
    /* links.  Returns 1 if it can, and it's not been marked, and returns      */
    /* 2 if it can and it has been marked.                                     */
    
    Disjunct * d;
    int left_valid, right_valid, found;
    int i, start_word, end_word;
    int w;
    Match_node * m, *m1;
    
    i = table_lookup(lw, rw, le, re, 0);
    if (i >= 0) return i;
    
    if ((le == NULL) && (re == NULL) && deletable[lw][rw]) {
	table_store(lw, rw, le, re, 0, 1);
	return 1;
    }
    
    if (le == NULL) {
	start_word = lw+1;
    } else {
	start_word = le->word;
    }
    if (re == NULL) {
	end_word = rw-1;
    } else {
	end_word = re->word;
    }
    
    found = 0;
    
    for (w=start_word; w <= end_word; w++) {
	m1 = m = form_match_list(w, le, lw, re, rw); 
	for (; m!=NULL; m=m->next) {
	    d = m->d;
	    /* mark_cost++;*/
	    /* in the following expressions we use the fact that 0=FALSE. Could eliminate
	       by always saying "region_valid(...) != 0"  */
	    left_valid = (((le != NULL) && (d->left != NULL) && prune_match(le, d->left, lw, w)) &&
			  ((region_valid(lw, w, le->next, d->left->next)) ||
			   ((le->multi) && region_valid(lw, w, le, d->left->next)) ||
			   ((d->left->multi) && region_valid(lw, w, le->next, d->left)) ||
			   ((le->multi && d->left->multi) && region_valid(lw, w, le, d->left))));
	    if (left_valid && region_valid(w, rw, d->right, re)) {
		found = 1;
		break;
	    }
	    right_valid = (((d->right != NULL) && (re != NULL) && prune_match(d->right, re, w, rw)) &&
			   ((region_valid(w, rw, d->right->next,re->next))    ||
			    ((d->right->multi) && region_valid(w,rw,d->right,re->next))  ||
			    ((re->multi) && region_valid(w, rw, d->right->next, re))  ||
			    ((d->right->multi && re->multi) && region_valid(w, rw, d->right, re))));
	    if ((left_valid && right_valid) || (right_valid && region_valid(lw, w, le, d->left))) {
		found = 1;
		break;
	    }
	}
	put_match_list(m1);
	if (found != 0) break;
    }
    table_store(lw, rw, le, re, 0, found);
    return found;
}

void mark_region(int lw, int rw, Connector *le, Connector *re) {
    /* Mark as useful all disjuncts involved in some way to complete the structure  */
    /* within the current region.  Note that only disjuncts strictly between        */
    /* lw and rw will be marked.  If it so happens that this region itself is not   */
    /* valid, then this fact will be recorded in the table, and nothing else happens*/    
    
    Disjunct * d;
    int left_valid, right_valid, i;
    int start_word, end_word;
    int w;
    Match_node * m, *m1;
    
    i = region_valid(lw, rw, le, re);
    if ((i==0) || (i==2)) return;
    /* we only reach this point if it's a valid unmarked region, i=1 */
    table_update(lw, rw, le, re, 0, 2);
    
    if ((le == NULL) && (re == NULL) && (null_links) && (rw != 1+lw)) {
	w = lw+1;
	for (d = local_sent[w].d; d != NULL; d = d->next) {
	    if ((d->left == NULL) && region_valid(w, rw, d->right, NULL)) {
		d->marked = TRUE;
		mark_region(w, rw, d->right, NULL);
	    }
	}
	mark_region(w, rw, NULL, NULL);
	return;
    }
    
    if (le == NULL) {
	start_word = lw+1;
    } else {
	start_word = le->word;
    }
    if (re == NULL) {
	end_word = rw-1;
    } else {
	end_word = re->word;
    }
    
    for (w=start_word; w <= end_word; w++) {
	m1 = m = form_match_list(w, le, lw, re, rw); 
	for (; m!=NULL; m=m->next) {
	    d = m->d;
	    /* mark_cost++;*/
	    left_valid = (((le != NULL) && (d->left != NULL) && prune_match(le, d->left, lw, w)) &&
			  ((region_valid(lw, w, le->next, d->left->next)) ||
			   ((le->multi) && region_valid(lw, w, le, d->left->next)) ||
			   ((d->left->multi) && region_valid(lw, w, le->next, d->left)) ||
			   ((le->multi && d->left->multi) && region_valid(lw, w, le, d->left))));
	    right_valid = (((d->right != NULL) && (re != NULL) && prune_match(d->right, re, w, rw)) &&
			   ((region_valid(w, rw, d->right->next,re->next)) ||
			    ((d->right->multi) && region_valid(w,rw,d->right,re->next))  ||
			    ((re->multi) && region_valid(w, rw, d->right->next, re)) ||
			    ((d->right->multi && re->multi) && region_valid(w, rw, d->right, re))));
	    
	    /* The following if statements could be restructured to avoid superfluous calls
	       to mark_region.  It didn't seem a high priority, so I didn't optimize this.
	       */
	    
	    if (left_valid && region_valid(w, rw, d->right, re)) {
		d->marked = TRUE;
		mark_region(w, rw, d->right, re);
		mark_region(lw, w, le->next, d->left->next);
		if (le->multi) mark_region(lw, w, le, d->left->next);
		if (d->left->multi) mark_region(lw, w, le->next, d->left);
		if (le->multi && d->left->multi) mark_region(lw, w, le, d->left);
	    }
	    
	    if (right_valid && region_valid(lw, w, le, d->left)) {
		d->marked = TRUE;
		mark_region(lw, w, le, d->left);
		mark_region(w, rw, d->right->next,re->next);
		if (d->right->multi) mark_region(w,rw,d->right,re->next);
		if (re->multi) mark_region(w, rw, d->right->next, re);
		if (d->right->multi && re->multi) mark_region(w, rw, d->right, re);
	    }
	    
	    if (left_valid && right_valid) {
		d->marked = TRUE;
		mark_region(lw, w, le->next, d->left->next);
		if (le->multi) mark_region(lw, w, le, d->left->next);
		if (d->left->multi) mark_region(lw, w, le->next, d->left);
		if (le->multi && d->left->multi) mark_region(lw, w, le, d->left);
		mark_region(w, rw, d->right->next,re->next);
		if (d->right->multi) mark_region(w,rw,d->right,re->next);
		if (re->multi) mark_region(w, rw, d->right->next, re);
		if (d->right->multi && re->multi) mark_region(w, rw, d->right, re);
	    }
	}
	put_match_list(m1);
    }
}

void delete_unmarked_disjuncts(Sentence sent){
    int w;
    Disjunct *d_head, *d, *dx;

    for (w=0; w<sent->length; w++) {
	d_head = NULL;
	for (d=sent->word[w].d; d != NULL; d=dx) {
	    dx = d->next;
	    if (d->marked) {
		d->next = d_head;
		d_head = d;
	    } else {
		d->next = NULL;
		free_disjuncts(d);
	    }
	}
	sent->word[w].d = d_head;
    }
}

void conjunction_prune(Sentence sent, Parse_Options opts) {
    /*
       We've already built the sentence disjuncts, and we've pruned them
       and power_pruned(GENTLE) them also.  The sentence contains a
       conjunction.  deletable[][] has been initialized to indicate the
       ranges which may be deleted in the final linkage.
       
       This routine deletes irrelevant disjuncts.  It finds them by first
       marking them all as irrelevant, and then marking the ones that
       might be useable.  Finally, the unmarked ones are removed.
       
       */
    Disjunct * d;
    int w;
    
    current_resources = opts->resources;
    deletable = sent->deletable;
    count_set_effective_distance(sent);

    /* we begin by unmarking all disjuncts.  This would not be necessary if
       whenever we created a disjunct we cleared its marked field.
       I didn't want to search the program for all such places, so
       I did this way.
       */
    for (w=0; w<sent->length; w++) {
	for (d=sent->word[w].d; d != NULL; d=d->next) {
	    d->marked = FALSE;
	}
    }
    
    init_fast_matcher(sent);
    init_table(sent);
    local_sent = sent->word;
    null_links = (opts->min_null_count > 0);
    /*
    for (d = sent->word[0].d; d != NULL; d = d->next) {
	if ((d->left == NULL) && region_valid(0, sent->length, d->right, NULL)) {
	    mark_region(0, sent->length, d->right, NULL);
	    d->marked = TRUE;
	}
    }
    mark_region(0, sent->length, NULL, NULL);
    */

    if (null_links) {
        mark_region(-1, sent->length, NULL, NULL);        
    } else {
	for (w=0; w<sent->length; w++) {
	  /* consider removing the words [0,w-1] from the beginning
	     of the sentence */
	    if (deletable[-1][w]) {
	        for (d = sent->word[w].d; d != NULL; d = d->next) {
		    if ((d->left == NULL) && region_valid(w, sent->length, d->right, NULL)) {
		        mark_region(w, sent->length, d->right, NULL);
		        d->marked = TRUE;
		    }  
		}
	    }
	}
    }
    
    delete_unmarked_disjuncts(sent);

    free_fast_matcher(sent);
    free_table(sent);    

    local_sent = NULL;
    current_resources = NULL;
    deletable = NULL;
    count_unset_effective_distance(sent);
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>

#include "error.h"

static char   buf[1024];
#define CRLF  printf("\n")

int    lperrno;
char   lperrmsg[1024];

const char * msg_of_lperror(int lperr) {
    switch(lperr) {
    case NODICT:
	return "Could not open dictionary ";
	break;
    case DICTPARSE:
	return "Error parsing dictionary ";
	break;
    case WORDFILE:
	return "Error opening word file ";
	break;
    case SEPARATE:
	return "Error separating sentence ";
	break;
    case NOTINDICT:
	return "Sentence not in dictionary ";
	break;
    case BUILDEXPR:
	return "Could not build sentence expressions ";
	break;
    case INTERNALERROR:
	return "Internal error.  Send mail to link@juno.com ";
	break;
    default:
	return "";
	break;
    }
    return NULL;
}


void lperror(int lperr, char *fmt, ...) {
    char temp[1024];
    va_list args;

    va_start(args, fmt);
    sprintf(lperrmsg, "PARSER-API: %s", msg_of_lperror(lperr));
    vsprintf(temp, fmt, args); 
    strcat(lperrmsg, temp);
    va_end(args);
    lperrno = lperr;
    fflush(stderr);
}

void error(char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args); CRLF;
    va_end(args);
    fprintf(stderr, "\n");
    if (errno > 0) {
	perror(buf);
	fprintf(stderr, "errno=%d\n", errno);
	fprintf(stderr, buf);
	fprintf(stderr, "\n");
    }
    fflush(stderr);
    fflush(stdout);
    fprintf(stderr, "Parser quitting.\n");
    exit(1); /* Always fail and print out this file name */
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/* The first thing we do is we build a data structure to represent the
   result of the entire parse search.  There will be a set of nodes
   built for each call to the count() function that returned a non-zero
   value, AND which is part of a valid linkage.  Each of these nodes
   represents a valid continuation, and contains pointers to two other
   sets (one for the left continuation and one for the righ
   continuation).

 */

static Word * local_sent;
static int    islands_ok;

Parse_set * dummy_set() {
    static Parse_set ds;
    ds.first = ds.current = NULL;
    ds.count = 1;
    return &ds;
}

Parse_set * empty_set(void) {
    /* returns an empty set of parses */
    Parse_set *s;
    s = (Parse_set *) xalloc(sizeof(Parse_set));
    s->first = s->current = NULL;
    s->count = 0;
    return s;
}

void free_set(Parse_set *s) {
    Parse_choice *p, *xp;
    if (s == NULL) return;
    for (p=s->first; p != NULL; p = xp) {
	xp = p->next;
	xfree((void *)p, sizeof(*p));
    }
    xfree((void *)s, sizeof(*s));
}

Parse_choice * make_choice(Parse_set *lset, int llw, int lrw, Connector * llc, Connector * lrc,
			   Parse_set *rset, int rlw, int rrw, Connector * rlc, Connector * rrc,
			   Disjunct *ld, Disjunct *md, Disjunct *rd) {
    Parse_choice *pc;
    pc = (Parse_choice *) xalloc(sizeof(*pc));
    pc->next = NULL;
    pc->set[0] = lset;
    pc->link[0].l = llw;
    pc->link[0].r = lrw;
    pc->link[0].lc = llc;
    pc->link[0].rc = lrc;
    pc->set[1] = rset;
    pc->link[1].l = rlw;
    pc->link[1].r = rrw;
    pc->link[1].lc = rlc;
    pc->link[1].rc = rrc;
    pc->ld = ld;
    pc->md = md;
    pc->rd = rd;
    return pc;
}

void put_choice_in_set(Parse_set *s, Parse_choice *pc) {
/* Put this parse_choice into a given set.  The current pointer is always
   left pointing to the end of the list. */
    if (s->first == NULL) {
	s->first = pc;
    } else {
	s->current->next = pc;
    }
    s->current = pc;
    pc->next = NULL;
}

int x_hash(int lw, int rw, Connector *le, Connector *re, int cost, Parse_info * pi) {
    int i;
    i = 0;
    
    i = i + (i<<1) + randtable[(lw + i) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(rw + i) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(((long) le + i) % (pi->x_table_size+1)) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(((long) re + i) % (pi->x_table_size+1)) & (RTSIZE - 1)];
    i = i + (i<<1) + randtable[(cost+i) & (RTSIZE - 1)];
    return i & (pi->x_table_size-1);
}

void init_x_table(Sentence sent) {
    /* A piecewise exponential function determines the size of the hash table.      */
    /* Probably should make use of the actual number of disjuncts, rather than just */
    /* the number of words                                                          */
    int i, x_table_size;
    Parse_info * pi;

    assert(sent->parse_info == NULL, "Parse_info is not NULL");

    pi = sent->parse_info = (Parse_info *) xalloc(sizeof(Parse_info));
    pi->N_words = sent->length;

    if (pi->N_words >= 10) {
	x_table_size = (1<<14);
    } else if (pi->N_words >= 4) {
	x_table_size = (1 << (pi->N_words));
    } else {
	x_table_size = (1 << 4);
    }

    /*printf("Allocating x_table of size %d\n", x_table_size);*/
    pi->x_table_size = x_table_size;
    pi->x_table = (X_table_connector**) xalloc(x_table_size * sizeof(X_table_connector*));
    for (i=0; i<x_table_size; i++) {
	pi->x_table[i] = NULL;
    }
}
    
void free_x_table(Parse_info * pi) {
/* This is the function that should be used to free tha set structure. Since
   it's a dag, a recursive free function won't work.  Every time we create
   a set element, we put it in the hash table, so this is OK.*/
    int i;
    X_table_connector *t, *x;

    if (pi->x_table == NULL) {
	/*fprintf(stderr, "Warning: Tried to free a NULL x_table\n");*/
	return;
    }
    
    for (i=0; i<pi->x_table_size; i++) {
	for(t = pi->x_table[i]; t!= NULL; t=x) {
	    x = t->next;
	    free_set(t->set);
	    xfree((void *) t, sizeof(X_table_connector));
	}
    }
    /*printf("Freeing x_table of size %d\n", x_table_size);*/
    xfree((void *) pi->x_table, pi->x_table_size * sizeof(X_table_connector*));
    pi->x_table_size = 0;
    pi->x_table = NULL;
}

X_table_connector * x_table_pointer(int lw, int rw, Connector *le, Connector *re, 
				    int cost, Parse_info * pi) {
    /* returns the pointer to this info, NULL if not there */
    X_table_connector *t;
    t = pi->x_table[x_hash(lw, rw, le, re, cost, pi)];
    for (; t != NULL; t = t->next) {
	if ((t->lw == lw) && (t->rw == rw) && (t->le == le) && (t->re == re) && (t->cost == cost))  return t;
    }
    return NULL;
}

#if 0
Parse_set * x_table_lookup(int lw, int rw, Connector *le, Connector *re, 
			   int cost, Parse_info * pi) {
    /* returns the count for this quintuple if there, -1 otherwise */    
    X_table_connector *t = x_table_pointer(lw, rw, le, re, cost, pi);

    if (t == NULL) return -1; else return t->set;
}
#endif

X_table_connector * x_table_store(int lw, int rw, Connector *le, Connector *re, 
				  int cost, Parse_set * set, Parse_info * pi) {
    /* Stores the value in the x_table.  Assumes it's not already there */
    X_table_connector *t, *n;
    int h;
    
    n = (X_table_connector *) xalloc(sizeof(X_table_connector));
    n->set = set; 
    n->lw = lw; n->rw = rw; n->le = le; n->re = re; n->cost = cost;
    h = x_hash(lw, rw, le, re, cost, pi);
    t = pi->x_table[h];
    n->next = t;
    pi->x_table[h] = n;
    return n;
}

void x_table_update(int lw, int rw, Connector *le, Connector *re, 
		    int cost, Parse_set * set, Parse_info * pi) {
    /* Stores the value in the x_table.  Unlike x_table_store, it assumes it's already there */
    X_table_connector *t = x_table_pointer(lw, rw, le, re, cost, pi);
    
    assert(t != NULL, "This entry is supposed to be in the x_table.");
    t->set = set;
}


Parse_set * parse_set(Disjunct *ld, Disjunct *rd, int lw, int rw, 
		      Connector *le, Connector *re, int cost, Parse_info * pi) {
    /* returns NULL if there are no ways to parse, or returns a pointer
       to a set structure representing all the ways to parse */

    Disjunct * d, * dis;
    int start_word, end_word, w;
    int lcost, rcost, Lmatch, Rmatch;
    int i, j;
    Parse_set *ls[4], *rs[4], *lset, *rset;
    Parse_choice * a_choice;

    Match_node * m, *m1;
    X_table_connector *xt;
    int count;

    assert(cost >= 0, "parse_set() called with cost < 0.");

    count = table_lookup(lw, rw, le, re, cost);

    /*
      assert(count >= 0, "parse_set() called on params that were not in the table.");
      Actually, we can't assert this, because of the pseudocount technique that's
      used in count().  It's not the case that every call to parse_set() has already
      been put into the table.
     */

    if ((count == 0) || (count == -1)) return NULL;
    
    xt = x_table_pointer(lw, rw, le, re, cost, pi);

    if (xt == NULL) {
	xt = x_table_store(lw, rw, le, re, cost, empty_set(), pi);
	/* start it out with the empty set of options */
	/* this entry must be updated before we return */
    } else {
	return xt->set;  /* we've already computed it */
    }

    xt->set->count = count;  /* the count we already computed */
    /* this count is non-zero */
    
    if (rw == 1+lw) return xt->set;
    if ((le == NULL) && (re == NULL)) {
	if (!islands_ok && (lw != -1)) {
	    return xt->set;
	}
	if (cost == 0) {
	    return xt->set;
	} else {
	    w = lw+1;
	    for (dis = local_sent[w].d; dis != NULL; dis = dis->next) {
		if (dis->left == NULL) {
		    rs[0] = parse_set(dis, NULL, w, rw, dis->right, NULL, cost-1, pi);
		    if (rs[0] == NULL) continue;
		    a_choice = make_choice(dummy_set(), lw, w, NULL, NULL,
					   rs[0], w, rw, NULL, NULL,
					   NULL, NULL, NULL);
		    put_choice_in_set(xt->set, a_choice);
		}
	    }
	    rs[0] = parse_set(NULL, NULL, w, rw, NULL, NULL, cost-1, pi); 
	    if (rs[0] != NULL) {
		a_choice = make_choice(dummy_set(), lw, w, NULL, NULL,
				       rs[0], w, rw, NULL, NULL,
				       NULL, NULL, NULL);
		put_choice_in_set(xt->set, a_choice);
	    }
	    return xt->set;
	}
    }
    
    if (le == NULL) {
	start_word = lw+1;
    } else {
	start_word = le->word;

    }

    if (re == NULL) {
	end_word = rw-1;
    } else {
	end_word = re->word;
    }
    
    for (w=start_word; w <= end_word; w++) {
	m1 = m = form_match_list(w, le, lw, re, rw); 
	for (; m!=NULL; m=m->next) {
	    d = m->d;
	    for (lcost = 0; lcost <= cost; lcost++) {
		rcost = cost-lcost;
		/* now lcost and rcost are the costs we're assigning to those parts respectively */

		/* Now, we determine if (based on table only) we can see that
		   the current range is not parsable. */

		Lmatch = (le != NULL) && (d->left != NULL) && match(le, d->left, lw, w);
		Rmatch = (d->right != NULL) && (re != NULL) && match(d->right, re, w, rw);
		for (i=0; i<4; i++) {ls[i] = rs[i] = NULL;}
		if (Lmatch) {
		    ls[0] = parse_set(ld, d, lw, w, le->next, d->left->next, lcost, pi);
		    if (le->multi) ls[1] = parse_set(ld, d, lw, w, le, d->left->next, lcost, pi);
		    if (d->left->multi) ls[2] = parse_set(ld, d, lw, w, le->next, d->left, lcost, pi);
		    if (le->multi && d->left->multi) ls[3] = parse_set(ld, d, lw, w, le, d->left, lcost, pi);
		}
		if (Rmatch) {
		    rs[0] = parse_set(d, rd, w, rw, d->right->next, re->next, rcost, pi);
		    if (d->right->multi) rs[1] = parse_set(d, rd, w,rw,d->right,re->next, rcost, pi);
		    if (re->multi) rs[2] = parse_set(d, rd, w, rw, d->right->next, re, rcost, pi);
		    if (d->right->multi && re->multi) rs[3] = parse_set(d, rd, w, rw, d->right, re, rcost, pi);
		}

		for (i=0; i<4; i++) {
		    /* this ordering is probably not consistent with that needed to use list_links */
		    if (ls[i] == NULL) continue;
		    for (j=0; j<4; j++) {
			if (rs[j] == NULL) continue;
			a_choice = make_choice(ls[i], lw, w, le, d->left,
					       rs[j], w, rw, d->right, re,
					       ld, d, rd);
			put_choice_in_set(xt->set, a_choice);
		    }
		}
		
		if (ls[0] != NULL || ls[1] != NULL || ls[2] != NULL || ls[3] != NULL) {
		    /* evaluate using the left match, but not the right */
		    rset = parse_set(d, rd, w, rw, d->right, re, rcost, pi);
		    if (rset != NULL) {
			for (i=0; i<4; i++) {
			    if (ls[i] == NULL) continue;
			    /* this ordering is probably not consistent with that needed to use list_links */
			    a_choice = make_choice(ls[i], lw, w, le, d->left,
						   rset, w, rw, NULL /* d->right */, re,  /* the NULL indicates no link*/
						   ld, d, rd);
			    put_choice_in_set(xt->set, a_choice);
			}
		    }
		}
		if ((le == NULL) && (rs[0] != NULL || rs[1] != NULL || rs[2] != NULL || rs[3] != NULL)) {
		    /* evaluate using the right match, but not the left */
		    lset = parse_set(ld, d, lw, w, le, d->left, lcost, pi);

		    if (lset != NULL) {
			for (i=0; i<4; i++) {
			    if (rs[i] == NULL) continue;
			    /* this ordering is probably not consistent with that needed to use list_links */
			    a_choice = make_choice(lset, lw, w, NULL /* le */, d->left,  /* NULL indicates no link */
						   rs[i], w, rw, d->right, re,
						   ld, d, rd);
			    put_choice_in_set(xt->set, a_choice);
			}
		    }
		}
	    }
	}
	put_match_list(m1);
    }
    xt->set->current = xt->set->first;
    return xt->set;
}

int verify_set_node(Parse_set *set) {
    Parse_choice *pc;
    double dn;
    int n;
    if (set == NULL || set->first == NULL) return FALSE;
    dn = n = 0;
    for (pc = set->first; pc != NULL; pc = pc->next) {
	n  += pc->set[0]->count * pc->set[1]->count;
	dn += ((double) pc->set[0]->count) * ((double) pc->set[1]->count);
    }
    assert(n == set->count, "verify_set failed");
    return (n < 0) || (n != (int) dn);
}

int verify_set(Parse_info * pi) {
    X_table_connector *t;
    int i;
    int overflowed;

    overflowed=FALSE;
    assert(pi->x_table != NULL, "called verify_set when x_table==NULL");
    for (i=0; i<pi->x_table_size; i++) {
	for(t = pi->x_table[i]; t!= NULL; t=t->next) {
	    overflowed = (overflowed || verify_set_node(t->set));
	}
    }
    return overflowed;
}

int build_parse_set(Sentence sent, int cost, Parse_Options opts) {
    /* This is the top level call that computes the whole parse_set.  It
       points whole_set at the result.  It creates the necessary hash
       table (x_table) which will be freed at the same time the
       whole_set is freed.

       It also assumes that count() has been run, and that hash table is
       filled with the values thus computed.  Therefore this function
       must be structured just like parse() (the main function for
       count()).  
       
       If the number of linkages gets huge, then the counts can overflow.
       We check if this has happened when verifying the parse set.
       This routine returns TRUE iff overflowed occurred.
    */

    Parse_set * whole_set;

    local_sent = sent->word;
    islands_ok = opts->islands_ok;

    whole_set = 
        parse_set(NULL, NULL, -1, sent->length, NULL, NULL, cost+1, sent->parse_info);

    if ((whole_set != NULL) && (whole_set->current != NULL)) {
	whole_set->current = whole_set->first;
    }

    sent->parse_info->parse_set = whole_set;

    local_sent = NULL;
    return verify_set(sent->parse_info);  
}

void free_parse_set(Sentence sent) {
    /* This uses the x_table to free the whole parse set (the set itself
       cannot be used cause it's a dag).  called from the outside world */
    if (sent->parse_info != NULL) {
	free_x_table(sent->parse_info);
	sent->parse_info->parse_set = NULL;
	xfree((void *) sent->parse_info, sizeof(Parse_info));
	sent->parse_info = NULL;
    }
}

void initialize_links(Parse_info * pi) {
    int i;
    pi->N_links = 0;
    for (i=0; i<pi->N_words; ++i) {
	pi->chosen_disjuncts[i] = NULL;
    }
}

void issue_link(Parse_info * pi, Disjunct * ld, Disjunct * rd, struct Link_s link) {
    assert(pi->N_links <= MAX_LINKS-1, "Too many links"); 
    pi->link_array[pi->N_links] = link;
    pi->N_links++;

    pi->chosen_disjuncts[link.l] = ld;
    pi->chosen_disjuncts[link.r] = rd;
}

void issue_links_for_choice(Parse_info * pi, Parse_choice *pc) {
    if (pc->link[0].lc != NULL) { /* there is a link to generate */
	issue_link(pi, pc->ld, pc->md, pc->link[0]);
    }
    if (pc->link[1].lc != NULL) {
	issue_link(pi, pc->md, pc->rd, pc->link[1]);
    }
}

void build_current_linkage_recursive(Parse_info * pi, Parse_set *set) {
    if (set == NULL) return;
    if (set->current == NULL) return;

    issue_links_for_choice(pi, set->current);
    build_current_linkage_recursive(pi, set->current->set[0]);
    build_current_linkage_recursive(pi, set->current->set[1]);
}

void build_current_linkage(Parse_info * pi) {
    /* This function takes the "current" point in the given set and
       generates the linkage that it represents.
     */
    initialize_links(pi);
    build_current_linkage_recursive(pi, pi->parse_set);
}

int advance_linkage(Parse_info * pi, Parse_set * set) {
    /* Advance the "current" linkage to the next one
       return 1 if there's a "carry" from this node,
       which indicates that the scan of this node has
       just been completed, and it's now back to it's
       starting state. */
    if (set == NULL) return 1;  /* probably can't happen */
    if (set->first == NULL) return 1;  /* the empty set */
    if (advance_linkage(pi, set->current->set[0]) == 1) {
	if (advance_linkage(pi, set->current->set[1]) == 1) {
	    if (set->current->next == NULL) {
		set->current = set->first;
		return 1;
	    }
	    set->current = set->current->next;
	}
    }
    return 0;
}

void advance_parse_set(Parse_info * pi) {
     advance_linkage(pi, pi->parse_set);
}

void list_links(Parse_info * pi, Parse_set * set, int index) {
     Parse_choice *pc;
     int n;

     if (set == NULL || set->first == NULL) return;
     for (pc = set->first; pc != NULL; pc = pc->next) {
	  n = pc->set[0]->count * pc->set[1]->count;
	  if (index < n) break;
	  index -= n;
     }
     assert(pc != NULL, "walked off the end in list_links");
     issue_links_for_choice(pi, pc);
     list_links(pi, pc->set[0], index % pc->set[0]->count);
     list_links(pi, pc->set[1], index / pc->set[0]->count);
}

void list_random_links(Parse_info * pi, Parse_set * set) {
     Parse_choice *pc;
     int num_pc, new_index;

     if (set == NULL || set->first == NULL) return;
     num_pc = 0;
     for (pc = set->first; pc != NULL; pc = pc->next) {
	  num_pc++;
     }

     new_index = my_random() % num_pc;
     
     num_pc = 0;
     for (pc = set->first; pc != NULL; pc = pc->next) {
	  if (new_index == num_pc) break;
	  num_pc++;
     }

     assert(pc != NULL, "Couldn't get a random parse choice");
     issue_links_for_choice(pi, pc);
     list_random_links(pi, pc->set[0]);
     list_random_links(pi, pc->set[1]);
}

void extract_links(int index, int cost, Parse_info * pi) {
/* Generate the list of all links of the indexth parsing of the
   sentence.  For this to work, you must have already called parse, and
   already built the whole_set. */
    initialize_links(pi);
    if (index < 0) {
	my_random_initialize(index);
	list_random_links(pi, pi->parse_set);
	my_random_finalize();
    }
    else {
	list_links(pi, pi->parse_set, index);
    }
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

int left_disjunct_list_length(Disjunct * d) {
/* returns the number of disjuncts in the list that have non-null
   left connector lists */
    int i;
    for (i=0; d!=NULL; d=d->next) {
	if (d->left != NULL) i++;
    }
    return i;
}
int right_disjunct_list_length(Disjunct * d) {
    int i;
    for (i=0; d!=NULL; d=d->next) {
	if (d->right != NULL) i++;
    }
    return i;
}

static int match_cost;

static int l_table_size[MAX_SENTENCE];  /* the sizes of the hash tables */
static int r_table_size[MAX_SENTENCE];

static Match_node ** l_table[MAX_SENTENCE]; 
                 /* the beginnings of the hash tables */
static Match_node ** r_table[MAX_SENTENCE];

static Match_node * mn_free_list = NULL;
   /* I'll pedantically maintain my own list of these cells */

Match_node * get_match_node(void) {
/* return a match node to be used by the caller */
    Match_node * m;
    if (mn_free_list != NULL) {
	m = mn_free_list;
	mn_free_list = m->next;
    } else {
	m = (Match_node *) xalloc(sizeof(Match_node));
    }
    return m;
}

void put_match_list(Match_node *m) {
/* put these nodes back onto my free list */    
    Match_node * xm;
    for (; m != NULL; m = xm) {
	xm = m->next;
	m->next = mn_free_list;
	mn_free_list = m;
    }
}

void free_match_list(Match_node * t) {
    Match_node *xt;
    for (; t!=NULL; t=xt) {
	xt = t->next;
	xfree((char *)t, sizeof(Match_node));
    }
}

void free_fast_matcher(Sentence sent) {
/* free all of the hash tables and Match_nodes */
    int w;
    int i;
    if (verbosity > 1) printf("%d Match cost\n", match_cost);
    for (w=0; w<sent->length; w++) {
	for (i=0; i<l_table_size[w]; i++) {
	    free_match_list(l_table[w][i]);
	}
	xfree((char *)l_table[w], l_table_size[w] * sizeof (Match_node *));
	for (i=0; i<r_table_size[w]; i++) {
	    free_match_list(r_table[w][i]);
	}
	xfree((char *)r_table[w], r_table_size[w] * sizeof (Match_node *));
    }
    free_match_list(mn_free_list);
    mn_free_list = NULL;
}

int fast_match_hash(Connector * c) {
/* This hash function only looks at the leading upper case letters of
   the connector string, and the label fields.  This ensures that if two
   strings match (formally), then they must hash to the same place.
   The answer must be masked to the appropriate table size.
*/
    char *s;
    int i;
    i = randtable[c->label & (RTSIZE-1)];
    s = c->string;
    while(isupper((int)*s)) {
	i = i + (i<<1) + randtable[((*s) + i) & (RTSIZE-1)];
	s++;
    }
    return i;
}

Match_node * add_to_right_table_list(Match_node * m, Match_node * l) {
/* Adds the match node m to the sorted list of match nodes l.
   The parameter dir determines the order of the sorting to be used.
   Makes the list sorted from smallest to largest.
*/
    if (l==NULL) return m;
    if ((m->d->right->word) <= (l->d->right->word)) {
	m->next = l;
	return m;
    } else {
	l->next = add_to_right_table_list(m, l->next);
	return l;
    }
}

Match_node * add_to_left_table_list(Match_node * m, Match_node * l) {
/* Adds the match node m to the sorted list of match nodes l.
   The parameter dir determines the order of the sorting to be used.
   Makes the list sorted from largest to smallest
*/
    if (l==NULL) return m;
    if ((m->d->left->word) >= (l->d->left->word)) {
	m->next = l;
	return m;
    } else {
	l->next = add_to_left_table_list(m, l->next);
	return l;
    }
}

void put_into_match_table(int size, Match_node ** t,
			  Disjunct * d, Connector * c, int dir ) {
/* The disjunct d (whose left or right pointer points to c) is put
   into the appropriate hash table
   dir =  1, we're putting this into a right table.
   dir = -1, we're putting this into a left table.
*/
    int h;
    Match_node * m;
    h = fast_match_hash(c) & (size-1);
    m = (Match_node *) xalloc (sizeof(Match_node));
    m->next = NULL;
    m->d = d;
    if (dir == 1) {
	t[h] = add_to_right_table_list(m, t[h]);
    } else {
	t[h] = add_to_left_table_list(m, t[h]);
    }
}

void init_fast_matcher(Sentence sent) {
    int w, len, size, i;
    Match_node ** t;
    Disjunct * d;
    match_cost = 0;
    for (w=0; w<sent->length; w++) {
	len = left_disjunct_list_length(sent->word[w].d);
	size = next_power_of_two_up(len);
	l_table_size[w] = size;
	t = l_table[w] = (Match_node **) xalloc(size * sizeof(Match_node *));
	for (i=0; i<size; i++) t[i] = NULL;

	for (d=sent->word[w].d; d!=NULL; d=d->next) {
	    if (d->left != NULL) {
		put_into_match_table(size, t, d, d->left, -1);
	    }
	}

	len = right_disjunct_list_length(sent->word[w].d);
	size = next_power_of_two_up(len);
	r_table_size[w] = size;
	t = r_table[w] = (Match_node **) xalloc(size * sizeof(Match_node *));
	for (i=0; i<size; i++) t[i] = NULL;

	for (d=sent->word[w].d; d!=NULL; d=d->next) {
	    if (d->right != NULL) {
		put_into_match_table(size, t, d, d->right, 1);
	    }
	}
    }
}

Match_node * form_match_list
      (int w, Connector *lc, int lw, Connector *rc, int rw) {
/* Forms and returns a list of disjuncts that might match lc or rc or both.
   lw and rw are the words from which lc and rc came respectively.
   The list is formed by the link pointers of Match_nodes.
   The list contains no duplicates.  A quadratic algorithm is used to
   eliminate duplicates.  In practice the match_cost is less than the
   parse_cost (and the loop is tiny), so there's no reason to bother
   to fix this.
*/
    Match_node *ml, *mr, *mx, *my, * mz, *front, *free_later;

    if (lc!=NULL) {
	ml = l_table[w][fast_match_hash(lc) & (l_table_size[w]-1)];
    } else {
	ml = NULL;
    }
    if (rc!=NULL) {
	mr = r_table[w][fast_match_hash(rc) & (r_table_size[w]-1)];
    } else {
	mr = NULL;
    }

    front = NULL;
    for (mx = ml; mx!=NULL; mx=mx->next) {
	if (mx->d->left->word < lw) break;
	my = get_match_node();
	my->d = mx->d;
	my->next = front;
	front = my;
    }
    ml = front;   /* ml is now the list of things that could match the left */

    front = NULL;
    for (mx = mr; mx!=NULL; mx=mx->next) {
	if (mx->d->right->word > rw) break;
	my = get_match_node();
	my->d = mx->d;
	my->next = front;
	front = my;
    }
    mr = front;   /* mr is now the list of things that could match the right */

    /* now we want to eliminate duplicates from the lists */

    free_later = NULL;
    front = NULL;
    for(mx = mr; mx != NULL; mx=mz) {
	/* see if mx in first list, put it in if its not */
	mz = mx->next;
	match_cost++;
	for (my=ml; my!=NULL; my=my->next) {
	    match_cost++;
	    if (mx->d == my->d) break;
	}
	if (my != NULL) { /* mx was in the l list */
	    mx->next = free_later;
	    free_later = mx;
	}
	if (my==NULL) {  /* it was not there */
	    mx->next = front;
	    front = mx;
	}
    }
    mr = front;  /* mr is now the abbreviated right list */
    put_match_list(free_later);

    /* now catenate the two lists */
    if (mr == NULL) return ml;
    for (mx = mr; mx->next != NULL; mx = mx->next)
      ;
    mx->next = ml;
    return mr;
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"
     
int contains_underbar(char * s) {
    /* Returns TRUE if the string contains an underbar character.
     */
    while(*s != '\0') {
	if (*s == '_') return TRUE;
	s++;
    }
    return FALSE;
}

int is_idiom_string(char * s) {
    /* Returns FALSE if it is not a correctly formed idiom string.
       correct such string:
       () contains no "."
       () non-empty strings separated by _
       */
    char * t;
    for (t=s; *t != '\0'; t++) {
	if (*t == '.') {
	    return FALSE;
	}
    }
    if ((s[0] == '_') || (s[strlen(s)-1] == '_')) {
	return FALSE;
    }
    for (t=s; *t != '\0'; t++) {
	if ((*t == '_') && (*(t+1) == '_')) {
	    return FALSE;
	}
    }
    return TRUE;
}    

int is_number(char *s) {
    /* return TRUE if the string s is a sequence of digits. */
    while(*s != '\0') {
	if (!isdigit((int)*s)) return FALSE;
	s++;
    }
    return TRUE;
}

int numberfy(char * s) {
    /* if the string contains a single ".", and ends in ".Ix" where
       x is a number, return x.  Return -1 if not of this form.
       */
    for (; (*s != '\0') && (*s != '.'); s++)
      ;
    if (*s++ != '.') return -1;
    if (*s++ != 'I') return -1;
    if (!is_number(s)) return -1;
    return atoi(s);
}

int max_postfix_found(Dict_node * d) {
    /* Look for words that end in ".Ix" where x is a number.
       Return the largest x found.
       */
    int i, j;
    i = 0;
    while(d != NULL) {
	j = numberfy(d->string);
	if (j > i) i = j;
	d = d->right;
    }
    return i;
}

char * build_idiom_word_name(Dictionary dict, char * s) {
    /* Allocates string space and returns a pointer to it.
       In this string is placed the idiomized name of the given string s.
       This is the same as s, but with a postfix of ".Ix", where x is an
       appropriate number.  x is the minimum number that distinguishes
       this word from others in the dictionary.
       */
    char * new_s, * x, *id;
    int count, sz;

    count = max_postfix_found(dictionary_lookup(dict, s))+1;

    sz = strlen(s)+10;
    new_s = x = (char *) xalloc(sz); /* fails if > 10**10 idioms */
    while((*s != '\0') && (*s != '.')) {
	*x = *s;
	x++;
	s++;
    }
    sprintf(x,".I%d",count);

    id = string_set_add(new_s, dict->string_set);
    xfree(new_s, sz);
    return id;
}

Dict_node * make_idiom_Dict_nodes(Dictionary dict, char * string) {
    /* Tear the idiom string apart.
       Destroys the string s, but does not free it.
       Put the parts into a list of Dict_nodes (connected by their right pointers)
       Sets the string fields of these Dict_nodes pointing to the
       fragments of the string s.  Later these will be replaced by
       correct names (with .Ix suffixes).
       The list is reversed from the way they occur in the string.
       A pointer to this list is returned.
       */
    Dict_node * dn, * dn_new;
    char * t, *s, *p;
    int more, sz;
    dn = NULL;

    sz = strlen(string)+1;
    p = s = xalloc(sz);
    strcpy(s, string);

    while (*s != '\0') {
	t = s;
    	while((*s != '\0') && (*s != '_')) s++;
	if (*s == '_') {
	    more = TRUE;
	    *s = '\0';
	} else {
	    more = FALSE;
	}
	dn_new = (Dict_node *) xalloc(sizeof (Dict_node));
	dn_new->right = dn;
	dn = dn_new;
	dn->string = string_set_add(t, dict->string_set);
	dn->file = NULL;
	if (more) s++;
    }

    xfree(p, sz);
    return dn;
}

static char current_name[] = "AAAAAAAA";
#define CN_size (sizeof(current_name)-1)

void increment_current_name(void) {
    int i, carry;
    i = CN_size-1;
    carry = 1;
    while (carry == 1) {
	current_name[i]++;
	if (current_name[i] == 'Z'+1) {
	    current_name[i] = 'A';
	    carry = 1;
	} else {
	    carry = 0;
	}
	i--;
    }
}

char * generate_id_connector(Dictionary dict) {
    /* generate a new connector name
       obtained from the current_name
       allocate string space for it.
       return a pointer to it.
     */
    int i, sz;
    char * t, * s, *id;
    
    for (i=0; current_name[i] == 'A'; i++)
      ;
    /* i is now the number of characters of current_name to skip */
    sz = CN_size - i + 2 + 1 + 1;
    s = t = (char *) xalloc(sz);
    *t++ = 'I';
    *t++ = 'D';
    for (; i < CN_size; i++ ) {
	*t++ = current_name[i] ;
    }
    *t++ = '\0';
    id = string_set_add(s, dict->string_set);
    xfree(s, sz);
    return id;
}

void insert_idiom(Dictionary dict, Dict_node * dn) {
    /* Takes as input a pointer to a Dict_node.
       The string of this Dict_node is an idiom string.
       This string is torn apart, and its components are inserted into the
       dictionary as special idiom words (ending in .I*, where * is a number).
       The expression of this Dict_node (its node field) has already been
       read and constructed.  This will be used to construct the special idiom
       expressions.
       The given dict node is freed.  The string is also freed.
       */
    Exp * nc, * no, * n1;
    E_list *ell, *elr;
    char * s;
    int s_length;
    Dict_node * dn_list, * xdn, * start_dn_list;
    
    no = dn->exp;
    s = dn->string;
    s_length = strlen(s);
    
    if (!is_idiom_string(s)) {
	printf("*** Word \"%s\" on line %d is not",s, dict->line_number);
	printf(" a correctly formed idiom string.\n");
	printf("    This word will be ignored\n");
	/* xfree((char *)s, s_length+1);  strings are handled now by string_set */
	xfree((char *)dn, sizeof (Dict_node));
	return;
    }
    
    dn_list = start_dn_list = make_idiom_Dict_nodes(dict, s);
    xfree((char *)dn, sizeof (Dict_node));
    
    if (dn_list->right == NULL) {
      error("Idiom string with only one connector -- should have been caught");
    }
    
    /* first make the nodes for the base word of the idiom (last word) */
    /* note that the last word of the idiom is first in our list */
    
    /* ----- this code just sets up the node fields of the dn_list ----*/
    nc = Exp_create(dict);
    nc->u.string = generate_id_connector(dict);
    nc->dir = '-';
    nc->multi = FALSE;
    nc->type = CONNECTOR_type;
    nc->cost = 0;
    
    n1 = Exp_create(dict);
    n1->u.l = ell = (E_list *) xalloc(sizeof(E_list));
    ell->next = elr = (E_list *) xalloc(sizeof(E_list));
    elr->next = NULL;
    ell->e = nc;
    elr->e = no;
    n1->type = AND_type;
    n1->cost = 0;

    dn_list->exp = n1;
    
    dn_list = dn_list->right;
    
    while(dn_list->right != NULL) {
	/* generate the expression for a middle idiom word */
	
	n1 = Exp_create(dict);
	n1->u.string = NULL;
	n1->type = AND_type;
	n1->cost = 0;
	n1->u.l = ell = (E_list *) xalloc(sizeof(E_list));
	ell->next = elr = (E_list *) xalloc(sizeof(E_list));
	elr->next = NULL;
	
	nc = Exp_create(dict);
	nc->u.string = generate_id_connector(dict);
	nc->dir = '+';
	nc->multi = FALSE;
	nc->type = CONNECTOR_type;
	nc->cost = 0;
	elr->e = nc;
	
	increment_current_name();
	
	nc = Exp_create(dict);
	nc->u.string = generate_id_connector(dict);
	nc->dir = '-';
	nc->multi = FALSE;
	nc->type = CONNECTOR_type;
	nc->cost = 0;

	ell->e = nc;
	
	dn_list->exp = n1;
	
	dn_list = dn_list->right;
    }
    /* now generate the last one */
    
    nc = Exp_create(dict);
    nc->u.string = generate_id_connector(dict);
    nc->dir = '+';
    nc->multi = FALSE;
    nc->type = CONNECTOR_type; 
    nc->cost = 0;
    
    dn_list->exp = nc;
    
    increment_current_name();
    
    /* ---- end of the code alluded to above ---- */
    
    /* now its time to insert them into the dictionary */
    
    dn_list = start_dn_list;
    
    while (dn_list != NULL) {
	xdn = dn_list->right;
	dn_list->left = dn_list->right = NULL;
        dn_list->string = build_idiom_word_name(dict, dn_list->string);
	dict->root = insert_dict(dict, dict->root, dn_list);
	dict->num_entries++;
	dn_list = xdn;
    }
    /* xfree((char *)s, s_length+1); strings are handled by string_set */
}

int is_idiom_word(char * s) {
    /* returns TRUE if this is a word ending in ".Ix", where x is a number. */
    return (numberfy(s) != -1) ;
}

/*
  int only_idiom_words(Dict_node * dn) {
  returns TRUE if the list of words contains only words that are
  idiom words.  This is useful, because under this condition you want
   to be able to insert the word anyway, as long as it doesn't match
   exactly.

    while(dn != NULL) {
	if (!is_idiom_word(dn->string)) return FALSE;
	dn = dn->right;
    }
    return TRUE;
}
*/
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"
#include <memory.h>

static LINKSET_SET ss[LINKSET_MAX_SETS];
static char q_unit_is_used[LINKSET_MAX_SETS];

/* delcarations of non-exported functions */
static void clear_hash_table2(const int unit);
static void initialize_unit(const int unit, const int size);
static LINKSET_NODE *linkset_add_internal(const int unit, char *str);
static int  take_a_unit();
static int  compute_hash2(const int unit, const char *str);
static char *local_alloc (int nbytes);
 
int linkset_open(const int size)
{
  int unit = take_a_unit();
  initialize_unit(unit, size);
  return unit;
}

void linkset_close(const int unit)
{
  if (!q_unit_is_used[unit]) return;
  linkset_clear(unit);
  free (ss[unit].hash_table);
  q_unit_is_used[unit] = 0;
}

void linkset_clear(const int unit)
{
  int i;
  if (!q_unit_is_used[unit]) return;
  for (i=0; i<ss[unit].hash_table_size; i++)
    {
      LINKSET_NODE *p=ss[unit].hash_table[i];
      while (p!=0)
	{
	  LINKSET_NODE *q = p;
	  p=p->next;
	  if (q->solid) free (q->str);
	  free(q);
	}
    }
  clear_hash_table2(unit);
}

int linkset_add(const int unit, char *str)
{
    /* returns 0 if already there, 1 if new. Stores only the pointer. */
    LINKSET_NODE *sn = linkset_add_internal(unit, str);
    if (sn==NULL) return 0;
    sn->solid = 0;
    return 1;
}

int linkset_add_solid(const int unit, char *str)
{
    /* returns 0 if already there, 1 if new. Copies string. */
    LINKSET_NODE *sn = linkset_add_internal(unit, str);
    if (sn==NULL) return 0;
    sn->str = (char *) malloc ((1+strlen(str))*sizeof(char));
    if (!sn->str) error("linkset: out of memory!");
    strcpy(sn->str,str);
    sn->solid = 1;
    return 1;
}

int linkset_remove(const int unit, char *str) 
{
  /* returns 1 if removed, 0 if not found */
  int hashval;
  LINKSET_NODE *p, *last;
  hashval = compute_hash2(unit, str);
  last = ss[unit].hash_table[hashval];
  if (!last) return 0;
  if (!strcmp(last->str,str)) 
    {
	ss[unit].hash_table[hashval] = last->next;
	if (last->solid) free(last->str);
	free(last);
	return 1;
    }
  p = last->next;
  while (p)
    {
	if (!strcmp(p->str,str)) 
	  {
	      last->next = p->next;
	      if (last->solid) free(last->str);
	      free(p);
	      return 1;
	  }
	p=p->next;
	last = last->next;
    }
  return 0;
}


int linkset_match(const int unit, char *str) {
    int hashval;
    LINKSET_NODE *p;
    hashval = compute_hash2(unit, str);
    p = ss[unit].hash_table[hashval];
    while(p!=0) 
      {
	  if (post_process_match(p->str,str)) return 1;
	  p=p->next;
      }
    return 0;
}

int linkset_match_bw(const int unit, char *str) {
    int hashval;
    LINKSET_NODE *p;
    hashval = compute_hash2(unit, str);
    p = ss[unit].hash_table[hashval];
    while(p!=0)
      {
	  if (post_process_match(str,p->str)) return 1;
	  p=p->next;
      }
    return 0;
}

/***********************************************************************/
static void clear_hash_table2(const int unit)
{
  memset(ss[unit].hash_table, 0, 
	 ss[unit].hash_table_size*sizeof(LINKSET_NODE *));
}

static void initialize_unit(const int unit, const int size) {
  if(size<=0) {
     printf("size too small!");
     abort();
  }
  ss[unit].hash_table_size = (int) ((float) size*LINKSET_SPARSENESS);
  ss[unit].hash_table = (LINKSET_NODE**) 
	local_alloc (ss[unit].hash_table_size*sizeof(LINKSET_NODE *));
  clear_hash_table2(unit);
}

static LINKSET_NODE *linkset_add_internal(const int unit, char *str)
{
  LINKSET_NODE *p, *n;
  int hashval;

  /* look for str in set */
  hashval = compute_hash2(unit, str);
  for (p=ss[unit].hash_table[hashval]; p!=0; p=p->next)
    if (!strcmp(p->str,str)) return NULL;  /* already present */
  
  /* create a new node for u; stick it at head of linked list */
  n = (LINKSET_NODE *) local_alloc (sizeof(LINKSET_NODE));      
  n->next = ss[unit].hash_table[hashval];
  n->str = str;
  ss[unit].hash_table[hashval] = n;
  return n;
}

static int compute_hash2(const int unit, const char *str)
 {
   /* hash is computed from capitalized prefix only */
  int i, hashval;
  hashval=LINKSET_DEFAULT_SEED;
  for (i=0; isupper((int)str[i]); i++)
    hashval = str[i] + 31*hashval;
  hashval = hashval % ss[unit].hash_table_size;
  if (hashval<0) hashval*=-1;
  return hashval;
}

static int take_a_unit() {
  /* hands out free units */
  int i;
  static int q_first = 1;
  if (q_first) {
    memset(q_unit_is_used, 0, LINKSET_MAX_SETS*sizeof(char));
    q_first = 0;
  }
  for (i=0; i<LINKSET_MAX_SETS; i++)
    if (!q_unit_is_used[i]) break;
  if (i==LINKSET_MAX_SETS) {
    printf("linkset.h: No more free units");   
    abort();
  }
  q_unit_is_used[i] = 1;
  return i;
}

static char *local_alloc (int nbytes)  {
   char * p;
   p = (char *) malloc (nbytes);
   if (!p) { 
        printf("linkset: out of memory");
       abort();
    }
   return p;
}

/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/* This file contains the functions for massaging disjuncts of the 
   sentence in special ways having to do with conjunctions.
   The only function called from the outside world is 
   install_special_conjunctive_connectors()

   It would be nice if this code was written more transparently.  In
   other words, there should be some fairly general functions that
   manipulate disjuncts, and take words like "neither" etc as input
   parameters, so as to encapsulate the changes being made for special
   words.  This would not be too hard to do, but it's not a high priority.  
       -DS 3/98
 */

#define COMMA_LABEL   (-2) /* to hook the comma to the following "and" */
#define EITHER_LABEL  (-3) /* to connect the "either" to the following "or" */
#define NEITHER_LABEL (-4) /* to connect the "neither" to the following "nor"*/
#define NOT_LABEL     (-5) /* to connect the "not" to the following "but"*/
#define NOTONLY_LABEL (-6) /* to connect the "not" to the following "only"*/
#define BOTH_LABEL    (-7) /* to connect the "both" to the following "and"*/

/* There's a problem with installing "...but...", "not only...but...", and
   "not...but...", which is that the current comma mechanism will allow
   a list separated by commas.  "Not only John, Mary but Jim came"
   The best way to prevent this is to make it impossible for the comma
   to attach to the "but", via some sort of additional subscript on commas.
   
   I can't think of a good way to prevent this.
*/

/* The following functions all do slightly different variants of the
   following thing:

   Catenate to the disjunct list pointed to by d, a new disjunct list.
   The new list is formed by copying the old list, and adding the new
   connector somewhere in the old disjunct, for disjuncts that satisfy
   certain conditions
*/

static Disjunct * glom_comma_connector(Disjunct * d) {
/* In this case the connector is to connect to the comma to the
   left of an "and" or an "or".  Only gets added next to a fat link
*/
    Disjunct * d_list, * d1, * d2;
    Connector * c, * c1;
    d_list = NULL;
    for (d1 = d; d1!=NULL; d1=d1->next) {
	if (d1->left == NULL) continue; 
	for (c = d1->left; c->next != NULL; c = c->next)
	  ;
	if (c->label < 0) continue;   /* last one must be a fat link */

	d2 = copy_disjunct(d1);
	d2->next = d_list;
	d_list = d2;

	c1 = init_connector((Connector *)xalloc(sizeof(Connector)));
	c1->string="";
	c1->label = COMMA_LABEL;
	c1->priority = THIN_priority;
	c1->multi=FALSE;	
	c1->next = NULL;

	c->next = c1;
    }
    return catenate_disjuncts(d, d_list);
}

static Disjunct * glom_aux_connector(Disjunct * d, int label, int necessary) {
/* In this case the connector is to connect to the "either", "neither",
   "not", or some auxilliary d to the current which is a conjunction.
   Only gets added next to a fat link, but before it (not after it)
   In the case of "nor", we don't create new disjuncts, we merely modify
   existing ones.  This forces the fat link uses of "nor" to
   use a neither.  (Not the case with "or".)  If necessary=FALSE, then
   duplication is done, otherwise it isn't
*/
    Disjunct * d_list, * d1, * d2;
    Connector * c, * c1, *c2;
    d_list = NULL;
    for (d1 = d; d1!=NULL; d1=d1->next) {
	if (d1->left == NULL) continue; 
	for (c = d1->left; c->next != NULL; c = c->next)
	  ;
	if (c->label < 0) continue;   /* last one must be a fat link */

	if (!necessary) {
	    d2 = copy_disjunct(d1);
	    d2->next = d_list;
	    d_list = d2;
	}

	c1 = init_connector((Connector *)xalloc(sizeof(Connector)));
	c1->string="";
	c1->label = label;
	c1->priority = THIN_priority;
	c1->multi=FALSE;
	c1->next = c;

	if (d1->left == c) {
	    d1->left = c1;
	} else {
	    for (c2 = d1->left; c2->next != c; c2 = c2->next)
	      ;
	    c2->next = c1;
	}
    }
    return catenate_disjuncts(d, d_list);
}

static Disjunct * add_one_connector(int label, int dir, char *cs, Disjunct * d) {
/* This adds one connector onto the beginning of the left (or right)
   connector list of d.  The label and string of the connector are
   specified
*/
    Connector * c;

    c = init_connector((Connector *)xalloc(sizeof(Connector)));
    c->string= cs;
    c->label = label;
    c->priority = THIN_priority;
    c->multi=FALSE;
    c->next = NULL;

    if (dir == '+') {
	c->next = d->right;
	d->right = c;
    } else {
	c->next = d->left;
	d->left = c;
    }
    return d;
}    

static Disjunct * special_disjunct(int label, int dir, char *cs, char * ds) {
/* Builds a new disjunct with one connector pointing in direction dir
   (which is '+' or '-').  The label and string of the connector
   are specified, as well as the string of the disjunct.
   The next pointer of the new disjunct set to NULL, so it can be
   regarded as a list.
*/
    Disjunct * d1;
    Connector * c;
    d1 = (Disjunct *) xalloc(sizeof(Disjunct));
    d1->cost = 0;
    d1->string = ds;
    d1->next = NULL;

    c = init_connector((Connector *)xalloc(sizeof(Connector)));
    c->string= cs;
    c->label = label;
    c->priority = THIN_priority;
    c->multi=FALSE;
    c->next = NULL;

    if (dir == '+') {
	d1->left = NULL;
	d1->right = c;
    } else {
	d1->right = NULL;
	d1->left = c;
    }
    return d1;
}

static void construct_comma(Sentence sent) {
/* Finds all places in the sentence where a comma is followed by
   a conjunction ("and", "or", "but", or "nor").  It modifies these comma
   disjuncts, and those of the following word, to allow the following
   word to absorb the comma (if used as a conjunction).
*/
    int w;
    for (w=0; w<sent->length-1; w++) {
	if ((strcmp(sent->word[w].string, ",")==0) && sent->is_conjunction[w+1]) {
	    sent->word[w].d = catenate_disjuncts(special_disjunct(COMMA_LABEL,'+',"", ","), sent->word[w].d);
	    sent->word[w+1].d = glom_comma_connector(sent->word[w+1].d);
	}
    }
}

/* The functions below put the special connectors on certain auxilliary
   words to be used with conjunctions.  Examples: either, neither, 
   both...and..., not only...but...
*/
static void construct_either(Sentence sent) {
    int w;
    if (!sentence_contains(sent, "either")) return;
    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "either") != 0) continue;
	sent->word[w].d = catenate_disjuncts(
                   special_disjunct(EITHER_LABEL,'+',"", "either"),
                   sent->word[w].d);
    }

    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "or") != 0) continue;
	sent->word[w].d = glom_aux_connector
	                  (sent->word[w].d, EITHER_LABEL, FALSE);
    }
}

static void construct_neither(Sentence sent) {
    int w;
    if (!sentence_contains(sent, "neither")) {
	/* I don't see the point removing disjuncts on "nor".  I
	   Don't know why I did this.  What's the problem keeping the
	   stuff explicitely defined for "nor" in the dictionary?  --DS 3/98 */
#if 0 
	    for (w=0; w<sent->length; w++) {
	    if (strcmp(sent->word[w].string, "nor") != 0) continue;
	    free_disjuncts(sent->word[w].d);
	    sent->word[w].d = NULL;  /* a nor with no neither is dead */
	}
#endif
	return;
    }
    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "neither") != 0) continue;
	sent->word[w].d = catenate_disjuncts(
		   special_disjunct(NEITHER_LABEL,'+',"", "neither"),
		   sent->word[w].d);
    }

    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "nor") != 0) continue;
	sent->word[w].d = glom_aux_connector
	                  (sent->word[w].d, NEITHER_LABEL, TRUE);
    }
}

static void construct_notonlybut(Sentence sent) {
    int w;
    Disjunct *d;
    if (!sentence_contains(sent, "not")) {
	return;
    }
    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "not") != 0) continue;
	sent->word[w].d = catenate_disjuncts(
	     special_disjunct(NOT_LABEL,'+',"", "not"),
	     sent->word[w].d);
	if (w<sent->length-1 &&  strcmp(sent->word[w+1].string, "only")==0) {
	    sent->word[w+1].d = catenate_disjuncts(
                          special_disjunct(NOTONLY_LABEL, '-',"","only"),
                          sent->word[w+1].d);
	    d = special_disjunct(NOTONLY_LABEL, '+', "","not");
	    d = add_one_connector(NOT_LABEL,'+',"", d);
	    sent->word[w].d = catenate_disjuncts(d, sent->word[w].d);
	}
    }
    /* The code below prevents sentences such as the following from
       parsing:
       it was not carried out by Serbs but by Croats */


    /* We decided that this is a silly thing to.  Here's the bug report
       caused by this:

      Bug with conjunctions.  Some that work with "and" but they don't work
      with "but".  "He was not hit by John and by Fred".
      (Try replacing "and" by "but" and it does not work.
      It's getting confused by the "not".)
     */
    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "but") != 0) continue;
	sent->word[w].d = glom_aux_connector
	                  (sent->word[w].d, NOT_LABEL, FALSE);
	/* The above line use to have a TRUE in it */
    }
}

static void construct_both(Sentence sent) {
    int w;
    if (!sentence_contains(sent, "both")) return;
    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "both") != 0) continue;
	sent->word[w].d = catenate_disjuncts(
                   special_disjunct(BOTH_LABEL,'+',"", "both"),
                   sent->word[w].d);
    }

    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, "and") != 0) continue;
	sent->word[w].d = glom_aux_connector(sent->word[w].d, BOTH_LABEL, FALSE);
    }
}

void install_special_conjunctive_connectors(Sentence sent) {
    construct_either(sent);      /* special connectors for "either" */
    construct_neither(sent);     /* special connectors for "neither" */
    construct_notonlybut(sent);  /* special connectors for "not..but.." */
                                 /* and               "not only..but.." */
    construct_both(sent);        /* special connectors for "both..and.." */
    construct_comma(sent);       /* special connectors for extra comma */
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

 /****************************************************************************
 *  
 *   This is a simple example of the link parser API.  It similates most of
 *   the functionality of the original link grammar parser, allowing sentences
 *   to be typed in either interactively or in "batch" mode (if -batch is
 *   specified on the command line, and stdin is redirected to a file).
 *   The program:
 *     Opens up a dictionary
 *     Iterates:
 *        1. Reads from stdin to get an input string to parse
 *        2. Tokenizes the string to form a Sentence
 *        3. Tries to parse it with cost 0
 *        4. Tries to parse with increasing cost
 *     When a parse is found:
 *        1. Extracts each Linkage
 *        2. Passes it to process_some_linkages()
 *        3. Deletes linkage
 *     After parsing each Sentence is deleted by making a call to 
 *     sentence_delete.
 *     
 ****************************************************************************/


#include "link-includes.h"
#include "command-line.h"

#define MAXINPUT 1024
#define DISPLAY_MAX 1024
#define COMMENT_CHAR '%'  /* input lines beginning with this are ignored */

static int batch_errors = 0;
static int input_pending=FALSE;
static Parse_Options  opts;
static Parse_Options  panic_parse_opts;

typedef enum {UNGRAMMATICAL='*', 
	      PARSE_WITH_DISJUNCT_COST_GT_0=':',
	      NO_LABEL=' '} Label;

int fget_input_string(char *input_string, FILE *in, FILE *out, 
		      Parse_Options opts) {
    if ((!parse_options_get_batch_mode(opts)) && (verbosity > 0)
	&& (!input_pending)) fprintf(out, "linkparser> ");
    fflush(out);
    input_pending = FALSE;
    if (fgets(input_string, MAXINPUT, in)) return 1;
    else return 0;
}

int fget_input_char(FILE *in, FILE *out, Parse_Options opts) {
    if (!parse_options_get_batch_mode(opts) && (verbosity > 0)) 
      fprintf(out, "linkparser> ");
    fflush(out);
    return getc(in);
}

/**************************************************************************
*  
*  This procedure displays a linkage graphically.  Since the diagrams 
*  are passed as character strings, they need to be deleted with a 
*  call to string_delete.
*
**************************************************************************/

void process_linkage(Linkage linkage, Parse_Options opts) {
    char * string;
    int    j, mode, first_sublinkage;

    if (parse_options_get_display_union(opts)) {
	linkage_compute_union(linkage);
	first_sublinkage = linkage_get_num_sublinkages(linkage)-1;
    }
    else {
	first_sublinkage = 0;
    }

    for (j=first_sublinkage; j<linkage_get_num_sublinkages(linkage); ++j) {
	linkage_set_current_sublinkage(linkage, j);
	if (parse_options_get_display_on(opts)) {
	    string = linkage_print_diagram(linkage);
	    fprintf(stdout, "%s", string);
	    string_delete(string);
	}
	if (parse_options_get_display_links(opts)) {
	    string = linkage_print_links_and_domains(linkage);
	    fprintf(stdout, "%s", string);
	    string_delete(string);
	}
	if (parse_options_get_display_postscript(opts)) {
	    string = linkage_print_postscript(linkage, FALSE);
	    fprintf(stdout, "%s\n", string);
	    string_delete(string);
	}
    }
    if ((mode=parse_options_get_display_constituents(opts))) {
	string = linkage_print_constituent_tree(linkage, mode);
	if (string != NULL) {
	    fprintf(stdout, "%s\n", string);
	    string_delete(string);
	} else {
	    fprintf(stderr, "Can't generate constituents.\n");
	    fprintf(stderr, "Constituent processing has been turned off.\n");
	}
    }
}

void print_parse_statistics(Sentence sent, Parse_Options opts) {
    if (sentence_num_linkages_found(sent) > 0) {
	if (sentence_num_linkages_found(sent) > 
	    parse_options_get_linkage_limit(opts)) {
	    fprintf(stdout, "Found %d linkage%s (%d of %d random " \
		    "linkages had no P.P. violations)", 
		    sentence_num_linkages_found(sent), 
		    sentence_num_linkages_found(sent) == 1 ? "" : "s",
		    sentence_num_valid_linkages(sent),
		    sentence_num_linkages_post_processed(sent));
	}
	else {
	    fprintf(stdout, "Found %d linkage%s (%d had no P.P. violations)", 
		    sentence_num_linkages_post_processed(sent), 
		    sentence_num_linkages_found(sent) == 1 ? "" : "s",
		    sentence_num_valid_linkages(sent));
	}
	if (sentence_null_count(sent) > 0) {
	    fprintf(stdout, " at null count %d", sentence_null_count(sent));
	}
	fprintf(stdout, "\n");
    }
}


void process_some_linkages(Sentence sent, Parse_Options opts) {
    int i, c, num_displayed, num_to_query;
    Linkage linkage;
   
    if (verbosity > 0) print_parse_statistics(sent, opts);
    if (!parse_options_get_display_bad(opts)) {
	num_to_query = MIN(sentence_num_valid_linkages(sent), DISPLAY_MAX);
    }
    else {
	num_to_query = MIN(sentence_num_linkages_post_processed(sent), 
			   DISPLAY_MAX);
    }

    for (i=0, num_displayed=0; i<num_to_query; ++i) {

	if ((sentence_num_violations(sent, i) > 0) &&
	    (!parse_options_get_display_bad(opts))) {
	    continue;
	}

	linkage = linkage_create(i, sent, opts);

	if (verbosity > 0) {
	  if ((sentence_num_valid_linkages(sent) == 1) &&
	      (!parse_options_get_display_bad(opts))) {
	    fprintf(stdout, "  Unique linkage, ");
	  }
	  else if ((parse_options_get_display_bad(opts)) &&
		   (sentence_num_violations(sent, i) > 0)) {
	    fprintf(stdout, "  Linkage %d (bad), ", i+1);
	  }
	  else {
	    fprintf(stdout, "  Linkage %d, ", i+1);
	  }
	  
	  if (!linkage_is_canonical(linkage)) {
	    fprintf(stdout, "non-canonical, ");
	  }
	  if (linkage_is_improper(linkage)) {
	    fprintf(stdout, "improper fat linkage, ");
	  }
	  if (linkage_has_inconsistent_domains(linkage)) {
	    fprintf(stdout, "inconsistent domains, ");
	  }
	  
	  fprintf(stdout, "cost vector = (UNUSED=%d DIS=%d AND=%d LEN=%d)\n",
		  linkage_unused_word_cost(linkage),
		  linkage_disjunct_cost(linkage),
		  linkage_and_cost(linkage),
		  linkage_link_cost(linkage));
	}

	process_linkage(linkage, opts);
	linkage_delete(linkage);

	if (++num_displayed < num_to_query) {
	    if (verbosity > 0) {
	        fprintf(stdout, "Press RETURN for the next linkage.\n");
	    }
	    if ((c=fget_input_char(stdin, stdout, opts)) != '\n') {
		ungetc(c, stdin);
		input_pending = TRUE;
		break;
	    }
	}
    }
}

int there_was_an_error(Label label, Sentence sent, Parse_Options opts) {

    if (sentence_num_valid_linkages(sent) > 0) {
	if (label == UNGRAMMATICAL) {
	    batch_errors++;
	    return UNGRAMMATICAL;
	}
	if ((sentence_disjunct_cost(sent, 0) == 0) && 
	    (label == PARSE_WITH_DISJUNCT_COST_GT_0)) {
	    batch_errors++;
	    return PARSE_WITH_DISJUNCT_COST_GT_0;
	}
    } else {
	if  (label != UNGRAMMATICAL) {
	    batch_errors++;
	    return UNGRAMMATICAL;
	}
    }
    return FALSE;
}

void batch_process_some_linkages(Label label, Sentence sent, Parse_Options opts) {
    Linkage linkage;
   
    if (there_was_an_error(label, sent, opts)) {
	if (sentence_num_linkages_found(sent) > 0) {
	    linkage = linkage_create(0, sent, opts);
	    process_linkage(linkage, opts);
	    linkage_delete(linkage);
	}
	fprintf(stdout, "+++++ error %d\n", batch_errors);
    }
}

int special_command(char *input_string, Dictionary dict) {

    if (input_string[0] == '\n') return TRUE;
    if (input_string[0] == COMMENT_CHAR) return TRUE;
    if (input_string[0] == '!') {
        if (strncmp(input_string, "!panic_", 7)==0) {
	    issue_special_command(input_string+7, panic_parse_opts, dict);
	}
	else {
	    issue_special_command(input_string+1, opts, dict);
	}
	return TRUE;
    }
    return FALSE;
}

Label strip_off_label(char * input_string) {
    int c;

    c = input_string[0];
    switch(c) {
    case UNGRAMMATICAL:
    case PARSE_WITH_DISJUNCT_COST_GT_0:
	input_string[0] = ' ';
	return c;
	break;
    default:
	return NO_LABEL;
    }	
}

void setup_panic_parse_options(Parse_Options opts) {
    parse_options_set_disjunct_cost(opts, 3);
    parse_options_set_min_null_count(opts, 1);
    parse_options_set_max_null_count(opts, MAX_SENTENCE);
    parse_options_set_max_parse_time(opts, 60);
    parse_options_set_islands_ok(opts, 1);
    parse_options_set_short_length(opts, 6);
    parse_options_set_all_short_connectors(opts, 1);
    parse_options_set_linkage_limit(opts, 100);
}

void print_usage(char *str) {
    fprintf(stderr, 
	    "Usage: %s [dict_file] [-pp pp_knowledge_file]\n"
	    "          [-c constituent_knowledge_file] [-a affix_file]\n"
	    "          [-ppoff] [-coff] [-aoff] [-batch] [-<special \"!\" command>]\n", str);
    exit(-1);
}

int main(int argc, char * argv[]) {

    Dictionary      dict;
    Sentence        sent;
    char            *dictionary_file=NULL;
    char            *post_process_knowledge_file=NULL;
    char            *constituent_knowledge_file=NULL;
    char            *affix_file=NULL;
    int             pp_on=TRUE;
    int             af_on=TRUE;
    int             cons_on=TRUE;
    int             num_linkages, i;
    char            input_string[MAXINPUT];
    Label           label = NO_LABEL;  
    int             parsing_space_leaked, reported_leak, dictionary_and_option_space;


    i = 1;
    if ((argc > 1) && (argv[1][0] != '-')) {
	/* the dictionary is the first argument if it doesn't begin with "-" */
	dictionary_file = argv[1];	
	i++;
    }

    for (; i<argc; i++) {
	if (argv[i][0] == '-') {
	    if (strcmp("-pp", argv[i])==0) {
		if ((post_process_knowledge_file != NULL) || (i+1 == argc)) 
		  print_usage(argv[0]);
		post_process_knowledge_file = argv[i+1];
		i++;
	    } else 
	    if (strcmp("-c", argv[i])==0) {
		if ((constituent_knowledge_file != NULL) || (i+1 == argc)) 
		  print_usage(argv[0]);
		constituent_knowledge_file = argv[i+1];
		i++;
	    } else 
	    if (strcmp("-a", argv[i])==0) {
		if ((affix_file != NULL) || (i+1 == argc)) print_usage(argv[0]);
		affix_file = argv[i+1];
		i++;
	    } else if (strcmp("-ppoff", argv[i])==0) {
		pp_on = FALSE;
	    } else if (strcmp("-coff", argv[i])==0) {
		cons_on = FALSE;
	    } else if (strcmp("-aoff", argv[i])==0) {
		af_on = FALSE;
	    } else if (strcmp("-batch", argv[i])==0) {
	    } else if (strncmp("-!", argv[i],2)==0) {
	    } else {
		print_usage(argv[0]);		
	    }
	} else {
	    print_usage(argv[0]);
	}
    }

    if (!pp_on && post_process_knowledge_file != NULL) print_usage(argv[0]);

    if (dictionary_file == NULL) {
	dictionary_file = "4.0.dict";
        fprintf(stderr, "No dictionary file specified.  Using %s.\n", 
		dictionary_file);
    }

    if (af_on && affix_file == NULL) {
	affix_file = "4.0.affix";
        fprintf(stderr, "No affix file specified.  Using %s.\n", affix_file);
    }

    if (pp_on && post_process_knowledge_file == NULL) {
	post_process_knowledge_file = "4.0.knowledge";
        fprintf(stderr, "No post process knowledge file specified.  Using %s.\n",
		post_process_knowledge_file);
    }

    if (cons_on && constituent_knowledge_file == NULL) {
        constituent_knowledge_file = "4.0.constituent-knowledge"; 
	fprintf(stderr, "No constituent knowledge file specified.  Using %s.\n", 
		constituent_knowledge_file);
    }

    opts = parse_options_create();
    if (opts == NULL) {
	fprintf(stderr, "%s\n", lperrmsg);
	exit(-1);
    }

    panic_parse_opts = parse_options_create();
    if (panic_parse_opts == NULL) {
	fprintf(stderr, "%s\n", lperrmsg);
	exit(-1);
    }
    setup_panic_parse_options(panic_parse_opts);
    parse_options_set_max_sentence_length(opts, 70);
    parse_options_set_panic_mode(opts, TRUE);
    parse_options_set_max_parse_time(opts, 30);
    parse_options_set_linkage_limit(opts, 1000);
    parse_options_set_short_length(opts, 10);

    dict = dictionary_create(dictionary_file, 
			     post_process_knowledge_file,
			     constituent_knowledge_file,
			     affix_file);
    if (dict == NULL) {
	fprintf(stderr, "%s\n", lperrmsg);
	exit(-1);
    }

    /* process the command line like commands */
    for (i=1; i<argc; i++) {
	if ((strcmp("-pp", argv[i])==0) || 
	    (strcmp("-c", argv[i])==0) || 
	    (strcmp("-a", argv[i])==0)) {
	  i++;
	} else if ((argv[i][0] == '-') && (strcmp("-ppoff", argv[i])!=0) &&
		   (argv[i][0] == '-') && (strcmp("-coff", argv[i])!=0) &&
		   (argv[i][0] == '-') && (strcmp("-aoff", argv[i])!=0)) {
	  issue_special_command(argv[i]+1, opts, dict);
	}
    }

    dictionary_and_option_space = space_in_use;  
    reported_leak = external_space_in_use = 0;
    verbosity = parse_options_get_verbosity(opts);

    while (fget_input_string(input_string, stdin, stdout, opts)) {

	if (space_in_use != dictionary_and_option_space + reported_leak) {
	    fprintf(stderr, "Warning: %d bytes of space leaked.\n",
		    space_in_use-dictionary_and_option_space-reported_leak);
	    reported_leak = space_in_use - dictionary_and_option_space;
	}

	if ((strcmp(input_string, "quit\n")==0) ||
	    (strcmp(input_string, "exit\n")==0)) break;

	if (special_command(input_string, dict)) continue;
	if (parse_options_get_echo_on(opts)) {
	    printf("%s", input_string);
	}

	if (parse_options_get_batch_mode(opts)) {
	    label = strip_off_label(input_string);
	}

	sent = sentence_create(input_string, dict);

	if (sent == NULL) {
	    if (verbosity > 0) fprintf(stderr, "%s\n", lperrmsg);
	    if (lperrno != NOTINDICT) exit(-1);
	    else continue;
	} 
	if (sentence_length(sent) > parse_options_get_max_sentence_length(opts)) {
	    sentence_delete(sent);
	    if (verbosity > 0) {
	      fprintf(stdout, 
		      "Sentence length (%d words) exceeds maximum allowable (%d words)\n",
		    sentence_length(sent), parse_options_get_max_sentence_length(opts));
	    }
	    continue;
	}

	/* First parse with cost 0 or 1 and no null links */
	parse_options_set_disjunct_cost(opts, 2);
	parse_options_set_min_null_count(opts, 0);
	parse_options_set_max_null_count(opts, 0);
	parse_options_reset_resources(opts);

	num_linkages = sentence_parse(sent, opts);

	/* Now parse with null links */
	if ((num_linkages == 0) && (!parse_options_get_batch_mode(opts))) {
	    if (verbosity > 0) fprintf(stdout, "No complete linkages found.\n");
	    if (parse_options_get_allow_null(opts)) {
		parse_options_set_min_null_count(opts, 1);
		parse_options_set_max_null_count(opts, sentence_length(sent));
		num_linkages = sentence_parse(sent, opts);
	    }
	}

	if (parse_options_timer_expired(opts)) {
	    if (verbosity > 0) fprintf(stdout, "Timer is expired!\n");
	}
	if (parse_options_memory_exhausted(opts)) {
	    if (verbosity > 0) fprintf(stdout, "Memory is exhausted!\n");
	}

	if ((num_linkages == 0) && 
	    parse_options_resources_exhausted(opts) &&
	    parse_options_get_panic_mode(opts)) {
	    print_total_time(opts);
	    if (verbosity > 0) fprintf(stdout, "Entering \"panic\" mode...\n");
	    parse_options_reset_resources(panic_parse_opts);
	    parse_options_set_verbosity(panic_parse_opts, verbosity);
	    num_linkages = sentence_parse(sent, panic_parse_opts);
	    if (parse_options_timer_expired(panic_parse_opts)) {
		if (verbosity > 0) fprintf(stdout, "Timer is expired!\n");
	    }
	}

	print_total_time(opts);

	if (parse_options_get_batch_mode(opts)) {
	    batch_process_some_linkages(label, sent, opts);
	}
	else {
	    process_some_linkages(sent, opts);
	}

	sentence_delete(sent);
	if (external_space_in_use != 0) {
	    fprintf(stderr, "Warning: %d bytes of external space leaked.\n", 
		    external_space_in_use);
	}
    }

    if (parse_options_get_batch_mode(opts)) {
	print_time(opts, "Total");
	fprintf(stderr, 
		"%d error%s.\n", batch_errors, (batch_errors==1) ? "" : "s");
    }

    parsing_space_leaked = space_in_use - dictionary_and_option_space;
    if (parsing_space_leaked != 0) {
        fprintf(stderr, "Warning: %d bytes of space leaked during parsing.\n", 
		parsing_space_leaked);
    }

    parse_options_delete(panic_parse_opts);
    parse_options_delete(opts);
    dictionary_delete(dict);

    if (space_in_use != parsing_space_leaked) {
        fprintf(stderr, 
		"Warning: %d bytes of dictionary and option space leaked.\n", 
		space_in_use - parsing_space_leaked);
    } 
    else if (parsing_space_leaked == 0) {
        fprintf(stderr, "Good news: no space leaked.\n");
    }

    if (external_space_in_use != 0) {
        fprintf(stderr, "Warning: %d bytes of external space leaked.\n", 
		external_space_in_use);
    }

    return 0;
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

 /* see bottom of file for comments on post processing                      */

#include <stdarg.h>
#include <memory.h>
#include "link-includes.h"

#define PP_MAX_DOMAINS 128

/***************** utility routines (not exported) ***********************/

static int string_in_list(char * s, char * a[]) 
{
  /* returns FALSE if the string s does not match anything in 
     the array.  The array elements are post-processing symbols */
  int i;
  for (i=0; a[i] != NULL; i++) 
    if (post_process_match(a[i], s)) return TRUE;
  return FALSE;
}

static int find_domain_name(Postprocessor *pp, char *link) 
{
  /* Return the name of the domain associated with the provided starting 
     link. Return -1 if link isn't associated with a domain. */
  int i,domain;
  StartingLinkAndDomain *sllt = pp->knowledge->starting_link_lookup_table;
  for (i=0;;i++) 
    {
      domain = sllt[i].domain;
      if (domain==-1) return -1;          /* hit the end-of-list sentinel */
      if (post_process_match(sllt[i].starting_link, link)) return domain;
    }
}

static int contained_in(Domain * d1, Domain * d2, Sublinkage *sublinkage)
{
  /* returns TRUE if domain d1 is contained in domain d2 */
  char mark[MAX_LINKS];
  List_o_links * lol;
  memset(mark, 0, sublinkage->num_links*(sizeof mark[0]));
  for (lol=d2->lol; lol != NULL; lol = lol->next) 
    mark[lol->link] = TRUE;
  for (lol=d1->lol; lol != NULL; lol = lol->next) 
    if (!mark[lol->link]) return FALSE;
  return TRUE;
}

static int link_in_domain(int link, Domain * d) 
{
  /* returns the predicate "the given link is in the given domain" */    
  List_o_links * lol;
  for (lol = d->lol; lol != NULL; lol = lol->next) 
    if (lol->link == link) return TRUE;
  return FALSE;
}

/* #define CHECK_DOMAIN_NESTING */

#if defined(CHECK_DOMAIN_NESTING)
/* Although this is no longer used, I'm leaving the code here for future reference --DS 3/98 */

static int check_domain_nesting(Postprocessor *pp, int num_links)
{
  /* returns TRUE if the domains actually form a properly nested structure */
  Domain * d1, * d2;
  int counts[4];
  char mark[MAX_LINKS];
  List_o_links * lol;
  int i;
  for (d1=pp->pp_data.domain_array; d1 < pp->pp_data.domain_array + pp->pp_data.N_domains; d1++) {
    for (d2=d1+1; d2 < pp->pp_data.domain_array + pp->pp_data.N_domains; d2++) {
      memset(mark, 0, num_links*(sizeof mark[0]));
      for (lol=d2->lol; lol != NULL; lol = lol->next) {
	mark[lol->link] = 1;
      }
      for (lol=d1->lol; lol != NULL; lol = lol->next) {
	mark[lol->link] += 2;
      }
      counts[0] = counts[1] = counts[2] = counts[3] = 0;
      for (i=0; i<num_links; i++) 
	counts[(int)mark[i]]++;/* (int) cast avoids compiler warning DS 7/97 */
      if ((counts[1] > 0) && (counts[2] > 0) && (counts[3] > 0)) 
	return FALSE;
    }
  }
  return TRUE;
}
#endif

static void free_List_o_links(List_o_links *lol) 
{
  /* free the list of links pointed to by lol
     (does not free any strings) */
  List_o_links * xlol;
  while(lol != NULL) {
    xlol = lol->next;
    xfree(lol, sizeof(List_o_links));
    lol = xlol;
  }
}

static void free_D_tree_leaves(DTreeLeaf *dtl) 
{
  DTreeLeaf * xdtl;
  while(dtl != NULL) {
    xdtl = dtl->next;
    xfree(dtl, sizeof(DTreeLeaf));
    dtl = xdtl;
  }
}

void post_process_free_data(PP_data * ppd)
{
  /* gets called after every invocation of post_process() */
  int w, d;
  for (w=0; w<ppd->length; w++) 
    free_List_o_links(ppd->word_links[w]);
  for (d=0; d<ppd->N_domains; d++) 
    {
      free_List_o_links(ppd->domain_array[d].lol);
      free_D_tree_leaves(ppd->domain_array[d].child);
    }
  free_List_o_links(ppd->links_to_ignore);
  ppd->links_to_ignore = NULL;
}

static void connectivity_dfs(Postprocessor *pp, Sublinkage *sublinkage,
			     int w, pp_linkset *ls)
{
  List_o_links *lol;
  pp->visited[w] = TRUE;
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if (!pp->visited[lol->word] &&
        !pp_linkset_match(ls, sublinkage->link[lol->link]->name)) 
      connectivity_dfs(pp, sublinkage, lol->word, ls);
  }
}

static void mark_reachable_words(Postprocessor *pp, int w)
{
  List_o_links *lol;
  if (pp->visited[w]) return;
  pp->visited[w] = TRUE;
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) 
    mark_reachable_words(pp, lol->word);
}

static int is_connected(Postprocessor *pp)
{
  /* Returns true if the linkage is connected, considering words
     that have at least one edge....this allows conjunctive sentences
     not to be thrown out. */
  int i;
  for (i=0; i<pp->pp_data.length; i++) 
    pp->visited[i] = (pp->pp_data.word_links[i] == NULL);
  mark_reachable_words(pp, 0);
  for (i=0; i<pp->pp_data.length; i++) 
    if (!pp->visited[i]) return FALSE;
  return TRUE;
}


static void build_type_array(Postprocessor *pp)
{
  D_type_list * dtl;
  int d;
  List_o_links * lol;
  for (d=0; d<pp->pp_data.N_domains; d++) 
    {
      for (lol=pp->pp_data.domain_array[d].lol; lol != NULL; lol = lol->next) 
	{
	  dtl = (D_type_list *) xalloc(sizeof(D_type_list));
	  dtl->next = pp->pp_node->d_type_array[lol->link];
	  pp->pp_node->d_type_array[lol->link] = dtl;
	  dtl->type = pp->pp_data.domain_array[d].type;
	}
    }
}

void free_d_type(D_type_list * dtl) {
    D_type_list * dtlx;
    for (; dtl!=NULL; dtl=dtlx) {
	dtlx = dtl->next;
	xfree((void*) dtl, sizeof(D_type_list));
    }
}

D_type_list * copy_d_type(D_type_list * dtl) {
    D_type_list *dtlx, *dtlcurr=NULL, *dtlhead=NULL;
    for (; dtl!=NULL; dtl=dtl->next) {
	dtlx = (D_type_list *) xalloc(sizeof(D_type_list));
	*dtlx = *dtl;
	if (dtlhead == NULL) {
	    dtlhead = dtlx;
	    dtlcurr = dtlx;
	}
	else {
	    dtlcurr->next = dtlx;
	    dtlcurr = dtlx;
	}
    }
    return dtlhead;
}

static void free_pp_node(Postprocessor *pp)
{
  /* free the pp node from last time */
  int i;
  if (pp->pp_node==NULL) return;
  for (i=0; i<MAX_LINKS; i++) {
      free_d_type(pp->pp_node->d_type_array[i]);
  }
  xfree((void*) pp->pp_node, sizeof(PP_node));
  pp->pp_node = NULL;
}


static void alloc_pp_node(Postprocessor *pp)
{
  /* set up a fresh pp_node for later use */
  int i;
  pp->pp_node=(PP_node *) xalloc(sizeof(PP_node));
  pp->pp_node->violation = NULL;
  for (i=0; i<MAX_LINKS; i++) 
    pp->pp_node->d_type_array[i] = NULL; 
}

static void reset_pp_node(Postprocessor *pp)
{
  free_pp_node(pp);
  alloc_pp_node(pp);
}

/************************ rule application *******************************/

static int apply_rules(Postprocessor *pp,
		       int (applyfn) (Postprocessor *,Sublinkage *,pp_rule *),
		       Sublinkage *sublinkage,
		       pp_rule *rule_array,   
		       char **msg) 
{
  int i;
  for (i=0; (*msg=rule_array[i].msg)!=NULL; i++) 
    if (!applyfn(pp, sublinkage, &(rule_array[i]))) return 0;  
  return 1;
}

static int 
apply_relevant_rules(Postprocessor *pp,
		     int(applyfn)(Postprocessor *pp,Sublinkage*,pp_rule *rule),
		     Sublinkage *sublinkage,
		     pp_rule *rule_array,   
		     int *relevant_rules,
		     char **msg) 
{
  int i, idx;

  /* if we didn't accumulate link names for this sentence, we need to apply
     all rules */
  if (pp_linkset_population(pp->set_of_links_of_sentence)==0) {
      return apply_rules(pp, applyfn, sublinkage, rule_array, msg);
  }
  
  /* we did, and we don't */
  for (i=0; (idx=relevant_rules[i])!=-1; i++) {
      *msg = rule_array[idx].msg;   /* Adam had forgotten this -- DS 4/9/98 */
      if (!applyfn(pp, sublinkage, &(rule_array[idx]))) return 0;
  }      
  return 1;
}

static int 
apply_contains_one(Postprocessor *pp, Sublinkage *sublinkage, pp_rule *rule) 
{
  /* returns TRUE if and only if all groups containing the specified link 
     contain at least one from the required list.  (as determined by exact
     string matching) */    
  DTreeLeaf * dtl;
  int d, count;
  for (d=0; d<pp->pp_data.N_domains; d++) 
    {
      for (dtl = pp->pp_data.domain_array[d].child; 
	   dtl != NULL && 
	     !post_process_match(rule->selector,
				 sublinkage->link[dtl->link]->name);
	   dtl = dtl->next);
      if (dtl != NULL) 
	{
	  /* selector link of rule appears in this domain */
	  count=0;
	  for (dtl = pp->pp_data.domain_array[d].child; dtl != NULL; dtl = dtl->next) 
	    if (string_in_list(sublinkage->link[dtl->link]->name,
			       rule->link_array))
	      {
		count=1;
		break;
	      }
	  if (count == 0) return FALSE;
	}
    }
  return TRUE;
}


static int 
apply_contains_none(Postprocessor *pp,Sublinkage *sublinkage,pp_rule *rule) 
{
  /* returns TRUE if and only if:
     all groups containing the selector link do not contain anything
     from the link_array contained in the rule. Uses exact string matching. */
  DTreeLeaf * dtl;
  int d;
  for (d=0; d<pp->pp_data.N_domains; d++) 
    {
      for (dtl = pp->pp_data.domain_array[d].child; 
	   dtl != NULL &&
	     !post_process_match(rule->selector,
				 sublinkage->link[dtl->link]->name);
	   dtl = dtl->next); 
      if (dtl != NULL) 
	{
	  /* selector link of rule appears in this domain */
	  for (dtl = pp->pp_data.domain_array[d].child; dtl != NULL; dtl = dtl->next) 
	    if (string_in_list(sublinkage->link[dtl->link]->name, 
			       rule->link_array)) 
	      return FALSE;
	}
    }
  return TRUE;
}

static int 
apply_contains_one_globally(Postprocessor *pp,Sublinkage *sublinkage,pp_rule *rule)
{
  /* returns TRUE if and only if 
     (1) the sentence doesn't contain the selector link for the rule, or 
     (2) it does, and it also contains one or more from the rule's link set */

  int i,j,count;
  for (i=0; i<sublinkage->num_links; i++) {
    if (sublinkage->link[i]->l == -1) continue;
    if (post_process_match(rule->selector,sublinkage->link[i]->name)) break;
  }
  if (i==sublinkage->num_links) return TRUE;
  
  /* selector link of rule appears in sentence */
  count=0;
  for (j=0; j<sublinkage->num_links && count==0; j++) {
    if (sublinkage->link[j]->l == -1) continue;
    if (string_in_list(sublinkage->link[j]->name, rule->link_array)) 
      {
	count=1;
	break;
      }
  }
  if (count==0) return FALSE; else return TRUE;
}

static int 
apply_connected(Postprocessor *pp, Sublinkage *sublinkage, pp_rule *rule)
{
  /* There is actually just one (or none, if user didn't specify it)
     rule asserting that linkage is connected. */
  if (!is_connected(pp)) return 0;  
  return 1;                     
}

#if 0
/* replaced in 3/98 with a slightly different algorithm shown below  ---DS*/
static int 
apply_connected_without(Postprocessor *pp,Sublinkage *sublinkage,pp_rule *rule)
{
  /* Returns true if the linkage is connected when ignoring the links
     whose names are in the given list of link names.
     Actually, what it does is this: it returns FALSE if the connectivity
     of the subgraph reachable from word 0 changes as a result of deleting
     these links. */
  int i;
  memset(pp->visited, 0, pp->pp_data.length*(sizeof pp->visited[0]));
  mark_reachable_words(pp, 0);
  for (i=0; i<pp->pp_data.length; i++) 
    pp->visited[i] = !pp->visited[i];
  connectivity_dfs(pp, sublinkage, 0, rule->link_set);
  for (i=0; i<pp->pp_data.length; i++) 
    if (pp->visited[i] == FALSE) return FALSE;
  return TRUE;
}
#else 

/* Here's the new algorithm: For each link in the linkage that is in the
   must_form_a_cycle list, we want to make sure that that link
   is in a cycle.  We do this simply by deleting the link, then seeing if the
   end points of that link are still connected.
*/

static void reachable_without_dfs(Postprocessor *pp, Sublinkage *sublinkage, int a, int b, int w) {
     /* This is a depth first search of words reachable from w, excluding any direct edge
	between word a and word b. */
    List_o_links *lol;
    pp->visited[w] = TRUE;
    for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
	if (!pp->visited[lol->word] && !(w == a && lol->word == b) && ! (w == b && lol->word == a)) {
	    reachable_without_dfs(pp, sublinkage, a, b, lol->word);
	}
    }
}

static int
apply_must_form_a_cycle(Postprocessor *pp,Sublinkage *sublinkage,pp_rule *rule)
{
  /* Returns TRUE if the linkage is connected when ignoring the links
     whose names are in the given list of link names.
     Actually, what it does is this: it returns FALSE if the connectivity
     of the subgraph reachable from word 0 changes as a result of deleting
     these links. */

    List_o_links *lol;
    int w;
    for (w=0; w<pp->pp_data.length; w++) {
	for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
	    if (w > lol->word) continue;  /* only consider each edge once */
	    if (!pp_linkset_match(rule->link_set, sublinkage->link[lol->link]->name)) continue;
	    memset(pp->visited, 0, pp->pp_data.length*(sizeof pp->visited[0]));
	    reachable_without_dfs(pp, sublinkage, w, lol->word, w);
	    if (!pp->visited[lol->word]) return FALSE;
	}
    }

    for (lol = pp->pp_data.links_to_ignore; lol != NULL; lol = lol->next) {
	w = sublinkage->link[lol->link]->l;
	/* (w, lol->word) are the left and right ends of the edge we're considering */
	if (!pp_linkset_match(rule->link_set, sublinkage->link[lol->link]->name)) continue;
	memset(pp->visited, 0, pp->pp_data.length*(sizeof pp->visited[0]));
	reachable_without_dfs(pp, sublinkage, w, lol->word, w);
	if (!pp->visited[lol->word]) return FALSE;
    }

    return TRUE;
}

#endif

static int 
apply_bounded(Postprocessor *pp,Sublinkage *sublinkage,pp_rule *rule)
{
  /* Checks to see that all domains with this name have the property that
     all of the words that touch a link in the domain are not to the left
     of the root word of the domain. */
  int d, lw, d_type;
  List_o_links * lol;    
  d_type = rule->domain;
  for (d=0; d<pp->pp_data.N_domains; d++) {
    if (pp->pp_data.domain_array[d].type != d_type) continue;
    lw = sublinkage->link[pp->pp_data.domain_array[d].start_link]->l;
    for (lol = pp->pp_data.domain_array[d].lol; lol != NULL; lol = lol->next) {
      if (sublinkage->link[lol->link]->l < lw) return FALSE;
    }
  }
  return TRUE;
}

/********************* various non-exported functions ***********************/

static void build_graph(Postprocessor *pp, Sublinkage *sublinkage)
{
  /* fill in the pp->pp_data.word_links array with a list of words neighboring each
     word (actually a list of links).  The dir fields are not set, since this
     (after fat-link-extraction) is an undirected graph. */ 
  int i, link;
  List_o_links * lol;
  
  for (i=0; i<pp->pp_data.length; i++) 
    pp->pp_data.word_links[i] = NULL;
  
  for (link=0; link<sublinkage->num_links; link++) 
    {
      if (sublinkage->link[link]->l == -1) continue;	
      if (pp_linkset_match(pp->knowledge->ignore_these_links, sublinkage->link[link]->name)) {
	  lol = (List_o_links *) xalloc(sizeof(List_o_links));
	  lol->next = pp->pp_data.links_to_ignore;
	  pp->pp_data.links_to_ignore = lol;
	  lol->link = link;
	  lol->word = sublinkage->link[link]->r;
	  continue;
      }
      
      lol = (List_o_links *) xalloc(sizeof(List_o_links));
      lol->next = pp->pp_data.word_links[sublinkage->link[link]->l];
      pp->pp_data.word_links[sublinkage->link[link]->l] = lol;
      lol->link = link;
      lol->word = sublinkage->link[link]->r;
      
      lol = (List_o_links *) xalloc(sizeof(List_o_links));
      lol->next = pp->pp_data.word_links[sublinkage->link[link]->r];
      pp->pp_data.word_links[sublinkage->link[link]->r] = lol;
      lol->link = link;
      lol->word = sublinkage->link[link]->l;
    }
}

static void setup_domain_array(Postprocessor *pp,
			       int n, char *string, int start_link) 
{
  memset(pp->visited, 0, pp->pp_data.length*(sizeof pp->visited[0]));/* set pp->visited[i] to FALSE */
  pp->pp_data.domain_array[n].string = string;
  pp->pp_data.domain_array[n].lol        = NULL;
  pp->pp_data.domain_array[n].size       = 0;
  pp->pp_data.domain_array[n].start_link = start_link;
}

static void add_link_to_domain(Postprocessor *pp, int link) 
{
  List_o_links *lol;
  lol = (List_o_links *) xalloc(sizeof(List_o_links));
  lol->next = pp->pp_data.domain_array[pp->pp_data.N_domains].lol;
  pp->pp_data.domain_array[pp->pp_data.N_domains].lol = lol;
  pp->pp_data.domain_array[pp->pp_data.N_domains].size++;
  lol->link = link;
}

static void depth_first_search(Postprocessor *pp, Sublinkage *sublinkage, 
			       int w, int root,int start_link) 
{
  List_o_links *lol;
  pp->visited[w] = TRUE;
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if (lol->word < w && lol->link != start_link) {
      add_link_to_domain(pp, lol->link);
    }
  }
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if (!pp->visited[lol->word] && (lol->word != root) &&
	!(lol->word < root && lol->word < w &&
          pp_linkset_match(pp->knowledge->restricted_links,
			   sublinkage->link[lol->link]->name)))
      depth_first_search(pp, sublinkage, lol->word, root, start_link);
  }
}

static void bad_depth_first_search(Postprocessor *pp, Sublinkage *sublinkage, 
				   int w, int root, int start_link) 
{
  List_o_links * lol;
  pp->visited[w] = TRUE;
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if ((lol->word < w)  && (lol->link != start_link) && (w != root)) {
      add_link_to_domain(pp, lol->link);
    }
  }
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if ((!pp->visited[lol->word]) && !(w == root && lol->word < w) &&
	!(lol->word < root && lol->word < w && 
          pp_linkset_match(pp->knowledge->restricted_links,
			   sublinkage->link[lol->link]->name)))
      bad_depth_first_search(pp, sublinkage, lol->word, root, start_link);
  }
}

static void d_depth_first_search(Postprocessor *pp, Sublinkage *sublinkage,
				 int w, int root, int right, int start_link) 
{
  List_o_links * lol;
  pp->visited[w] = TRUE;
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if ((lol->word < w) && (lol->link != start_link) && (w != root)) {
      add_link_to_domain(pp, lol->link);
    }
  }
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if (!pp->visited[lol->word] && !(w == root && lol->word >= right) &&
	!(w == root && lol->word < root) &&
	!(lol->word < root && lol->word < w && 
          pp_linkset_match(pp->knowledge->restricted_links,
			   sublinkage->link[lol->link]->name)))
      d_depth_first_search(pp,sublinkage,lol->word,root,right,start_link);
  }
}    

static void left_depth_first_search(Postprocessor *pp, Sublinkage *sublinkage,
                               int w, int right,int start_link)
{
  List_o_links *lol;
  pp->visited[w] = TRUE;
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if (lol->word < w && lol->link != start_link) {
      add_link_to_domain(pp, lol->link);
    }
  }
  for (lol = pp->pp_data.word_links[w]; lol != NULL; lol = lol->next) {
    if (!pp->visited[lol->word] && (lol->word != right))
      depth_first_search(pp, sublinkage, lol->word, right, start_link);
  }
}

static int domain_compare(const Domain * d1, const Domain * d2) 
{ return (d1->size-d2->size); /* for sorting the domains by size */ }
    
static void build_domains(Postprocessor *pp, Sublinkage *sublinkage)
{
  int link, i, d;
  char *s;
  pp->pp_data.N_domains = 0;

  for (link = 0; link<sublinkage->num_links; link++) {
    if (sublinkage->link[link]->l == -1) continue;	
    s = sublinkage->link[link]->name;
    
    if (pp_linkset_match(pp->knowledge->ignore_these_links, s)) continue;
    if (pp_linkset_match(pp->knowledge->domain_starter_links, s)) 
      {
	setup_domain_array(pp, pp->pp_data.N_domains, s, link);
        if (pp_linkset_match(pp->knowledge->domain_contains_links, s)) 
	  add_link_to_domain(pp, link);
	depth_first_search(pp,sublinkage,sublinkage->link[link]->r,
			   sublinkage->link[link]->l, link);

	pp->pp_data.N_domains++;
        assert(pp->pp_data.N_domains<PP_MAX_DOMAINS, "raise value of PP_MAX_DOMAINS");
      } 
    else {
      if (pp_linkset_match(pp->knowledge->urfl_domain_starter_links,s)) 
	{
	  setup_domain_array(pp, pp->pp_data.N_domains, s, link);
	  /* always add the starter link to its urfl domain */
	  add_link_to_domain(pp, link);
	  bad_depth_first_search(pp,sublinkage,sublinkage->link[link]->r,
				 sublinkage->link[link]->l,link);
	  pp->pp_data.N_domains++;
	  assert(pp->pp_data.N_domains<PP_MAX_DOMAINS,"raise PP_MAX_DOMAINS value");
	}
      else 
	if (pp_linkset_match(pp->knowledge->urfl_only_domain_starter_links,s))
	  {
	    setup_domain_array(pp, pp->pp_data.N_domains, s, link);
	    /* do not add the starter link to its urfl_only domain */
	    d_depth_first_search(pp,sublinkage, sublinkage->link[link]->l,
				 sublinkage->link[link]->l,
				 sublinkage->link[link]->r,link);
	    pp->pp_data.N_domains++;
	    assert(pp->pp_data.N_domains<PP_MAX_DOMAINS,"raise PP_MAX_DOMAINS value");
	  }
	else 
	  if (pp_linkset_match(pp->knowledge->left_domain_starter_links,s))
	    {
	      setup_domain_array(pp, pp->pp_data.N_domains, s, link);
	      /* do not add the starter link to a left domain */
	      left_depth_first_search(pp,sublinkage, sublinkage->link[link]->l,
				   sublinkage->link[link]->r,link);
	      pp->pp_data.N_domains++;
	      assert(pp->pp_data.N_domains<PP_MAX_DOMAINS,"raise PP_MAX_DOMAINS value");
	    }
    }
  }
  
  /* sort the domains by size */
  qsort((void *) pp->pp_data.domain_array, 
	pp->pp_data.N_domains, 
	sizeof(Domain),
	(int (*)(const void *, const void *)) domain_compare);
  
  /* sanity check: all links in all domains have a legal domain name */
  for (d=0; d<pp->pp_data.N_domains; d++) {
    i = find_domain_name(pp, pp->pp_data.domain_array[d].string);
    if (i==-1) 
       error("\tpost_process: Need an entry for %s in LINK_TYPE_TABLE",
           pp->pp_data.domain_array[d].string);
    pp->pp_data.domain_array[d].type = i;
  }
}

static void build_domain_forest(Postprocessor *pp, Sublinkage *sublinkage)
{
  int d, d1, link;
  DTreeLeaf * dtl;
  if (pp->pp_data.N_domains > 0) 
    pp->pp_data.domain_array[pp->pp_data.N_domains-1].parent = NULL;
  for (d=0; d < pp->pp_data.N_domains-1; d++) {
    for (d1 = d+1; d1 < pp->pp_data.N_domains; d1++) {
      if (contained_in(&pp->pp_data.domain_array[d],&pp->pp_data.domain_array[d1],sublinkage))
	{
	  pp->pp_data.domain_array[d].parent = &pp->pp_data.domain_array[d1];
	  break;
	}
    }
    if (d1 == pp->pp_data.N_domains) {
      /* we know this domain is a root of a new tree */
      pp->pp_data.domain_array[d].parent = NULL;
      /* It's now ok for this to happen.  It used to do:
	 printf("I can't find a parent domain for this domain\n");
	 print_domain(d);
	 exit(1); */
    }
  }
  /* the parent links of domain nodes have been established.
     now do the leaves */
  for (d=0; d < pp->pp_data.N_domains; d++) {
    pp->pp_data.domain_array[d].child = NULL;
  }
  for (link=0; link < sublinkage->num_links; link++) {
    if (sublinkage->link[link]->l == -1) continue; /* probably not necessary */
    for (d=0; d<pp->pp_data.N_domains; d++) {
      if (link_in_domain(link, &pp->pp_data.domain_array[d])) {
	dtl = (DTreeLeaf *) xalloc(sizeof(DTreeLeaf));
	dtl->link = link;
	dtl->parent = &pp->pp_data.domain_array[d];
	dtl->next = pp->pp_data.domain_array[d].child;
	pp->pp_data.domain_array[d].child = dtl;
	break;
      }
    }
  }
}

static int 
internal_process(Postprocessor *pp,Sublinkage *sublinkage,char **msg) 
{
  int i;
  /* quick test: try applying just the relevant global rules */
  if (!apply_relevant_rules(pp,apply_contains_one_globally,
			    sublinkage,
			    pp->knowledge->contains_one_rules,
			    pp->relevant_contains_one_rules, msg)) {
    for (i=0; i<pp->pp_data.length; i++) 
      pp->pp_data.word_links[i] = NULL;
    pp->pp_data.N_domains = 0;
    return -1;
  } 

  /* build graph; confirm that it's legally connected */
  build_graph(pp, sublinkage);  
  build_domains(pp, sublinkage);  
  build_domain_forest(pp, sublinkage);
 
#if defined(CHECK_DOMAIN_NESTING)
  /* These messages were deemed to not be useful, so
     this code is commented out.  See comment above. */
  if(!check_domain_nesting(pp, sublinkage->num_links)) 
      printf("WARNING: The domains are not nested.\n");
#endif
  
  /* The order below should be optimal for most cases */
  if (!apply_relevant_rules(pp,apply_contains_one, sublinkage,
			    pp->knowledge->contains_one_rules,
			    pp->relevant_contains_one_rules, msg))  return 1;
  if (!apply_relevant_rules(pp,apply_contains_none, sublinkage,
			    pp->knowledge->contains_none_rules,
	 	 	    pp->relevant_contains_none_rules, msg)) return 1;
  if (!apply_rules(pp,apply_must_form_a_cycle, sublinkage,
		   pp->knowledge->form_a_cycle_rules,msg))     return 1;
  if (!apply_rules(pp,apply_connected, sublinkage,
		   pp->knowledge->connected_rules, msg))            return 1;
  if (!apply_rules(pp,apply_bounded, sublinkage,
		   pp->knowledge->bounded_rules, msg))              return 1;
  return 0; /* This linkage satisfied all the rules */
}

  
void prune_irrelevant_rules(Postprocessor *pp)
{
  /* call this (a) after having called post_process_scan_linkage() on all
     generated linkages, but (b) before calling post_process() on any
     particular linkage. Here we mark all rules which we know (from having
     accumulated a set of link names appearing in *any* linkage) won't
     ever be needed. */
   pp_rule *rule;
   int coIDX, cnIDX, rcoIDX=0, rcnIDX=0;

  /* If we didn't scan any linkages, there's no pruning to be done. */
  if (pp_linkset_population(pp->set_of_links_of_sentence)==0) return;

  for (coIDX=0;;coIDX++) 
     {
       rule = &(pp->knowledge->contains_one_rules[coIDX]); 
       if (rule->msg==NULL) break;
       if (pp_linkset_match_bw(pp->set_of_links_of_sentence, rule->selector))
	 {
	   /* mark rule as being relevant to this sentence */
	   pp->relevant_contains_one_rules[rcoIDX++] = coIDX; 
	   pp_linkset_add(pp->set_of_links_in_an_active_rule, rule->selector); 
	 }
     }
   pp->relevant_contains_one_rules[rcoIDX] = -1;  /* end sentinel */
   
   for (cnIDX=0;;cnIDX++) 
     {
       rule = &(pp->knowledge->contains_none_rules[cnIDX]);
       if (rule->msg==NULL) break;
       if (pp_linkset_match_bw(pp->set_of_links_of_sentence, rule->selector)) 
	 {
	   pp->relevant_contains_none_rules[rcnIDX++] = cnIDX; 
	   pp_linkset_add(pp->set_of_links_in_an_active_rule, rule->selector); 
	 }
   }
   pp->relevant_contains_none_rules[rcnIDX] = -1;  

   if (verbosity>1) {
     printf("Saw %i unique link names in all linkages.\n", 
	    pp_linkset_population(pp->set_of_links_of_sentence));
     printf("Using %i 'contains one' rules and %i 'contains none' rules\n", 
	    rcoIDX, rcnIDX);
   } 
}


/***************** definitions of exported functions ***********************/

Postprocessor * post_process_open(char *dictname, char *path)
{
  /* read rules from path and initialize the appropriate fields in 
     a postprocessor structure, a pointer to which is returned.
     The only reason we need the dictname is to used it for the
     path, in case there is no DICTPATH set up.  If the dictname
     is null, and there is no DICTPATH, it just uses the filename
     as the full path.
  */
  Postprocessor *pp;
  if (path==NULL) return NULL;

  pp = (Postprocessor *) xalloc (sizeof(Postprocessor));
  pp->knowledge  = pp_knowledge_open(dictname, path);
  pp->sentence_link_name_set = string_set_create();
  pp->set_of_links_of_sentence = pp_linkset_open(1024);
  pp->set_of_links_in_an_active_rule=pp_linkset_open(1024);
  pp->relevant_contains_one_rules = 
      (int *) xalloc ((pp->knowledge->n_contains_one_rules+1)
		      *(sizeof pp->relevant_contains_one_rules[0]));
  pp->relevant_contains_none_rules = 
      (int *) xalloc ((pp->knowledge->n_contains_none_rules+1)
		      *(sizeof pp->relevant_contains_none_rules[0]));
  pp->relevant_contains_one_rules[0]  = -1;    
  pp->relevant_contains_none_rules[0] = -1;   
  pp->pp_node = NULL;
  pp->pp_data.links_to_ignore = NULL;
  pp->n_local_rules_firing  = 0;
  pp->n_global_rules_firing = 0;
  return pp;
}

void post_process_close(Postprocessor *pp)
{
  /* frees up memory associated with pp, previously allocated by open */
  if (pp==NULL) return;
  string_set_delete(pp->sentence_link_name_set);
  pp_linkset_close(pp->set_of_links_of_sentence);
  pp_linkset_close(pp->set_of_links_in_an_active_rule);
  xfree(pp->relevant_contains_one_rules, 
	(1+pp->knowledge->n_contains_one_rules)
	*(sizeof pp->relevant_contains_one_rules[0]));
  xfree(pp->relevant_contains_none_rules,
	(1+pp->knowledge->n_contains_none_rules)
	*(sizeof pp->relevant_contains_none_rules[0]));
  pp_knowledge_close(pp->knowledge);
  free_pp_node(pp);
  xfree(pp, sizeof(Postprocessor));
}

void post_process_close_sentence(Postprocessor *pp)
{
  if (pp==NULL) return;
  pp_linkset_clear(pp->set_of_links_of_sentence);
  pp_linkset_clear(pp->set_of_links_in_an_active_rule);
  string_set_delete(pp->sentence_link_name_set);
  pp->sentence_link_name_set = string_set_create(); 
  pp->n_local_rules_firing  = 0;
  pp->n_global_rules_firing = 0;
  pp->relevant_contains_one_rules[0]  = -1;    
  pp->relevant_contains_none_rules[0] = -1;   
  free_pp_node(pp);
}

void post_process_scan_linkage(Postprocessor *pp, Parse_Options opts,
			       Sentence sent, Sublinkage *sublinkage)
{
  /* During a first pass (prior to actual post-processing of the linkages 
     of a sentence), call this once for every generated linkage. Here we
     simply maintain a set of "seen" link names for rule pruning later on */
  char *p;
  int i;
  if (pp==NULL) return;
  if (sent->length < opts->twopass_length) return;
  for (i=0; i<sublinkage->num_links; i++) 
    {
      if (sublinkage->link[i]->l == -1) continue;
      p=string_set_add(sublinkage->link[i]->name,pp->sentence_link_name_set);
      pp_linkset_add(pp->set_of_links_of_sentence, p);
    }
}

PP_node *post_process(Postprocessor *pp, Parse_Options opts,
		      Sentence sent, Sublinkage *sublinkage, int cleanup) 
{
  /* Takes a sublinkage and returns:
     . for each link, the domain structure of that link
     . a list of the violation strings
     NB: sublinkage->link[i]->l=-1 means that this connector is to be ignored*/
  
  char *msg;

  if (pp==NULL) return NULL;

  pp->pp_data.links_to_ignore = NULL;
  pp->pp_data.length = sent->length;

  /* In the name of responsible memory management, we retain a copy of the 
     returned data structure pp_node as a field in pp, so that we can clear
     it out after every call, without relying on the user to do so. */
  reset_pp_node(pp);

  /* The first time we see a sentence, prune the rules which we won't be 
     needing during postprocessing the linkages of this sentence */
  if (sent->q_pruned_rules==FALSE && sent->length >= opts->twopass_length)
    prune_irrelevant_rules(pp);
  sent->q_pruned_rules=TRUE;

  switch(internal_process(pp, sublinkage, &msg))
    {
    case -1:
      /* some global test failed even before we had to build the domains */
      pp->n_global_rules_firing++;
      pp->pp_node->violation = msg;
      return pp->pp_node;
      break;
    case 1:
      /* one of the "normal" post processing tests failed */
      pp->n_local_rules_firing++;
      pp->pp_node->violation = msg;
      break; 
    case 0:
      /* This linkage is legal according to the post processing rules */
      pp->pp_node->violation = NULL;
      break;
    }

  build_type_array(pp);
  if (cleanup) post_process_free_data(&pp->pp_data);  
  return pp->pp_node;
}

/*
  string comparison in postprocessing. The first parameter is a
  post-processing symbol. The second one is a connector name from a link. The
  upper case parts must match. We imagine that the first arg is padded with an
  infinite sequence of "#" and that the 2nd one is padded with "*". "#"
  matches anything, but "*" is just like an ordinary char for matching 
  purposes. For efficiency sake there are several different versions of these 
  functions 
  */

int post_process_match(char *s, char *t) 
{
  char c;
  while(isupper((int)*s) || isupper((int)*t)) 
    {
      if (*s != *t) return FALSE;
      s++;
      t++;
    }
  while (*s != '\0') 
    {
      if (*s != '#') 
	{
	  if (*t == '\0') c = '*'; else c = *t;
	  if (*s != c) return FALSE;
	}
      s++;
      if (*t != '\0') t++;
    }
  return TRUE;
}

/* OLD COMMENTS (OUT OF DATE):
  This file does the post-processing. 
  The main routine is "post_process()".  It uses the link names only,
  and not the connectors.  
  
  A domain is a set of links.  Each domain has a defining link.
  Only certain types of links serve to define a domain.  These
  parameters are set by the lists of link names in a separate,
  human-readable file referred to herein as the 'knowledge file.'
  
  The domains are nested: given two domains, either they're disjoint,
  or one contains the other, i.e. they're tree structured.  The set of links
  in a domain (but in no smaller domain) are called the "group" of the
  domain.  Data structures are built to store all this stuff.
  The tree structured property is not mathematically guaranteed by
  the domain construction algorithm.  Davy simply claims that because
  of how he built the dictionary, the domains will always be so
  structured.  The program checks this and gives an error message
  if it's violated.
  
  Define the "root word" of a link (or domain) to be the word at the
  left end of the link.  The other end of the defining link is called
  the "right word".
  
  The domain corresponding to a link is defined to be the set of links
  reachable by starting from the right word, following links and never
  using the root word or any word to its left.
  
  There are some minor exceptions to this.  The "restricted_link" lists
  those connectors that, even if they point back before the root word,
  are included in the domain.  Some of the starting links are included
  in their domain, these are listed in the "domain_contains_links" list.
  
  Such was the way it was.  Now Davy tells me there should be another type
  of domain that's quite different.  Let's call these "urfl" domains.
  Certain type of connectors start urfl domains.  They're listed below.
  In a urfl domain, the search includes the root word.  It does a separate
  search to find urfl domains.
  
  Restricted links should work just as they do with ordinary domains. If they
  come out of the right word, or anything to the right of it (that's
  in the domain), they should be included but should not be traced
  further. If they come out of the root word, they should not be
  included. 
  */   

/*
  I also, unfortunately, want to propose a new type of domain. These
  would include everything that can be reached from the root word of the
  link, to the right, that is closer than the right word of the link.
  (They would not include the link itself.)
  
  In the following sentence, then, the "Urfl_Only Domain" of the G link
  would include only the "O" link:
  
  +-----G----+    
  +---O--+   +-AI+
  |      |   |   |
  hitting dogs is fun.a 
  
  In the following sentence it would include the "O", the "TT", the "I",
  the second "O", and the "A".
  
  +----------------G---------------+    
  +-----TT-----+  +-----O-----+    |    
  +---O---+    +-I+    +---A--+    +-AI+
  |       |    |  |    |      |    |   |
  telling people to do stupid things is fun.a 
  
  This would allow us to judge the following:
  
  kicking dogs bores me
  *kicking dogs kicks dogs
  explaining the program is easy
  *explaining the program is running
  
  (These are distinctions that I thought we would never be able to make,
  so I told myself they were semantic rather than syntactic. But with
  domains, they should be easy.)
  */
 
  /* Modifications, 6/96 ALB: 
     1) Rules and link sets are relegated to a separate, user-written
        file(s), herein referred to as the 'knowledge file'
     2) This information is read by a lexer, in pp_lexer.l (lex code)
        whose exported routines are all prefixed by 'pp_lexer'
     3) when postprocessing a sentence, the links of each domain are
        placed in a set for quick lookup, ('contains one' and 'contains none')
     4) Functions which were never called have been eliminated:
        link_inhabits(), match_in_list(), group_type_contains(),
        group_type_contains_one(),  group_type_contains_all()
     5) Some 'one-by-one' initializations have been replaced by faster
        block memory operations (memset etc.)
     6) The above comments are correct but incomplete! (1/97)
     7) observation: the 'contains one' is, empirically, by far the most 
        violated rule, so it should come first in applying the rules.

    Modifications, 9/97 ALB:
     Deglobalization. Made code constistent with api. 	
   */
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

/***********************************************************************
 pp_knowledge.c
 7/97
 Contains rules and associated information for post processing. This
 information is supplied in a human-readable file and is parsed by
 pp_lexer.h
***********************************************************************/

#include "link-includes.h"

#define PP_MAX_UNIQUE_LINK_NAMES 1024  /* just needs to be approximate */

/****************** non-exported functions ***************************/

static void check_domain_is_legal(char *p) 
{
  if (strlen(p)>1)
    error("post_process: Domain (%s) must be a single character", p);
}

static void initialize_set_of_links_starting_bounded_domain(pp_knowledge *k)
{
  int i,j,d,domain_of_rule;
  k->set_of_links_starting_bounded_domain = 
    pp_linkset_open(PP_MAX_UNIQUE_LINK_NAMES);
  for (i=0; k->bounded_rules[i].msg!=0; i++) 
    {
      domain_of_rule = k->bounded_rules[i].domain;
      for (j=0; (d=(k->starting_link_lookup_table[j].domain))!=-1; j++) 
	if (d==domain_of_rule) 
	  pp_linkset_add(k->set_of_links_starting_bounded_domain,
		      k->starting_link_lookup_table[j].starting_link);
    }
}

static void read_starting_link_table(pp_knowledge *k)
{
  /* read table of [link, domain type]. 
     This tells us what domain type each link belongs to. 
     This lookup table *must* be defined in the knowledge file. */
  char *p;
  const char label[] = "STARTING_LINK_TYPE_TABLE";
  int i, n_tokens;
  if (!pp_lexer_set_label(k->lt, label))
    error("post_process: Couldn't find starting link table %s",label);
  n_tokens = pp_lexer_count_tokens_of_label(k->lt); 
  if (n_tokens %2) 
    error("post_process: Link table must have format [<link> <domain name>]+");
  k->nStartingLinks = n_tokens/2;
  k->starting_link_lookup_table = (StartingLinkAndDomain*) 
    xalloc((1+k->nStartingLinks)*sizeof(StartingLinkAndDomain));
  for (i=0; i<k->nStartingLinks; i++)
    {
      /* read the starting link itself */
      k->starting_link_lookup_table[i].starting_link = 
	string_set_add(pp_lexer_get_next_token_of_label(k->lt),k->string_set);
      
      /* read the domain type of the link */
      p = pp_lexer_get_next_token_of_label(k->lt);   
      check_domain_is_legal(p);
      k->starting_link_lookup_table[i].domain = (int) p[0];
    }

  /* end sentinel */
  k->starting_link_lookup_table[k->nStartingLinks].domain = -1;
}

static pp_linkset *read_link_set(pp_knowledge *k,
				 const char *label, String_set *ss)
{
  /* read link set, marked by label in knowledge file, into a set of links
     whose handle is returned. Return NULL if link set not defined in file,
     in which case the set is taken to be empty. */
  int n_strings,i;
  pp_linkset *ls;
  if (!pp_lexer_set_label(k->lt, label)) {
    if (verbosity>0)
      printf("PP warning: Link set %s not defined: assuming empty.\n",label);
    n_strings = 0;
  }
  else n_strings = pp_lexer_count_tokens_of_label(k->lt); 
  ls = pp_linkset_open(n_strings); 
  for (i=0; i<n_strings; i++) 
    pp_linkset_add(ls,
		   string_set_add(pp_lexer_get_next_token_of_label(k->lt),ss));
  return ls;
}

static void read_link_sets(pp_knowledge *k)
{
  String_set *ss = k->string_set;   /* shorthand */
  k->domain_starter_links     =read_link_set(k,"DOMAIN_STARTER_LINKS",ss);
  k->urfl_domain_starter_links=read_link_set(k,"URFL_DOMAIN_STARTER_LINKS",ss);
  k->domain_contains_links    =read_link_set(k,"DOMAIN_CONTAINS_LINKS",ss);
  k->ignore_these_links       =read_link_set(k,"IGNORE_THESE_LINKS",ss);
  k->restricted_links         =read_link_set(k,"RESTRICTED_LINKS",ss);
  k->must_form_a_cycle_links  =read_link_set(k,"MUST_FORM_A_CYCLE_LINKS",ss); 
  k->urfl_only_domain_starter_links=
      read_link_set(k,"URFL_ONLY_DOMAIN_STARTER_LINKS",ss);
  k->left_domain_starter_links=read_link_set(k,"LEFT_DOMAIN_STARTER_LINKS",ss);
}

static void free_link_sets(pp_knowledge *k)
{
  pp_linkset_close(k->domain_starter_links);
  pp_linkset_close(k->urfl_domain_starter_links);
  pp_linkset_close(k->domain_contains_links);
  pp_linkset_close(k->ignore_these_links);
  pp_linkset_close(k->restricted_links);
  pp_linkset_close(k->must_form_a_cycle_links);
  pp_linkset_close(k->urfl_only_domain_starter_links);
  pp_linkset_close(k->left_domain_starter_links);
}

static void read_connected_rule(pp_knowledge *k, const char *label)
{
  /* This is a degenerate class of rules: either a single rule asserting
     connectivity is there, or it isn't. The only information in the
     rule (besides its presence) is the error message to display if 
     the rule is violated */
  k->connected_rules = (pp_rule *) xalloc (sizeof(pp_rule));
  if (!pp_lexer_set_label(k->lt, label))
    {
      k->connected_rules[0].msg=0;  /* rule not there */
      if (verbosity>0) printf("PP warning: Not using 'link is connected' rule\n");
      return;
    }
  if (pp_lexer_count_tokens_of_label(k->lt)>1) 
    error("post_process: Invalid syntax in %s", label);
  k->connected_rules[0].msg =
    string_set_add(pp_lexer_get_next_token_of_label(k->lt), k->string_set);
}


static void read_form_a_cycle_rules(pp_knowledge *k, const char *label)
{
  int n_commas, n_tokens, r, i;
  pp_linkset *lsHandle;
  char **tokens;
  if (!pp_lexer_set_label(k->lt, label)) {
      k->n_form_a_cycle_rules = 0;             
      if (verbosity>0)
	printf("PP warning: Not using any 'form a cycle' rules\n");
  }
  else {
    n_commas = pp_lexer_count_commas_of_label(k->lt); 
    k->n_form_a_cycle_rules = (n_commas + 1)/2;
  }
  k->form_a_cycle_rules=
    (pp_rule*) xalloc ((1+k->n_form_a_cycle_rules)*sizeof(pp_rule));
  for (r=0; r<k->n_form_a_cycle_rules; r++)
    {
      /* read link set */
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens); 
      if (n_tokens <= 0) error("syntax error in knowledge file");
      lsHandle = pp_linkset_open(n_tokens);
      for (i=0; i<n_tokens; i++) 
	pp_linkset_add(lsHandle,string_set_add(tokens[i], k->string_set));
      k->form_a_cycle_rules[r].link_set=lsHandle;
      
      /* read error message */
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens);
      if (n_tokens>1)
	error("post_process: Invalid syntax (rule %i of %s)",r+1,label);
      k->form_a_cycle_rules[r].msg=string_set_add(tokens[0],k->string_set);
    }

  /* sentinel entry */
  k->form_a_cycle_rules[k->n_form_a_cycle_rules].msg = 0;
}

static void read_bounded_rules(pp_knowledge *k, const char *label)
{
  char **tokens;
  int n_commas, n_tokens, r;
  if (!pp_lexer_set_label(k->lt, label)) {
      k->n_bounded_rules = 0;
      if (verbosity>0) printf("PP warning: Not using any 'bounded' rules\n");
  }
  else {
    n_commas = pp_lexer_count_commas_of_label(k->lt); 
    k->n_bounded_rules = (n_commas + 1)/2;
  }
  k->bounded_rules = (pp_rule*) xalloc ((1+k->n_bounded_rules)*sizeof(pp_rule));
  for (r=0; r<k->n_bounded_rules; r++)
    {
      /* read domain */	
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens); 
      if (n_tokens!=1)
	error("post_process: Invalid syntax: rule %i of %s",r+1,label);
      k->bounded_rules[r].domain = (int) tokens[0][0];
      
      /* read error message */
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens); 
      if (n_tokens!=1) 
	error("post_process: Invalid syntax: rule %i of %s",r+1,label);
      k->bounded_rules[r].msg = string_set_add(tokens[0], k->string_set);
    }
  
  /* sentinel entry */
  k->bounded_rules[k->n_bounded_rules].msg = 0;
}

static void read_contains_rules(pp_knowledge *k, char *label, pp_rule **rules, 
				int *nRules)
{
  /* Reading the 'contains_one_rules' and reading the 
     'contains_none_rules' into their respective arrays */
  int n_commas, n_tokens, i, r;
  char *p, **tokens;
  if (!pp_lexer_set_label(k->lt, label)) {
      *nRules = 0;
      if (verbosity>0) printf("PP warning: Not using any %s rules\n", label);
  }
  else {
    n_commas = pp_lexer_count_commas_of_label(k->lt);
    *nRules = (n_commas + 1)/3;
  }
  *rules = (pp_rule*) xalloc ((1+*nRules)*sizeof(pp_rule));
  for (r=0; r<*nRules; r++)
    {
      /* first read link */
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens);
      if (n_tokens>1) 
	error("post_process: Invalid syntax in %s (rule %i)",label,r+1);
      (*rules)[r].selector = string_set_add(tokens[0], k->string_set);
      
      /* read link set */
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens);
      (*rules)[r].link_set = pp_linkset_open(n_tokens);
      (*rules)[r].link_set_size = n_tokens;
      (*rules)[r].link_array = (char **) xalloc((1+n_tokens)*sizeof(char*));
      for (i=0; i<n_tokens; i++) 
	{
	  p=string_set_add(tokens[i], k->string_set);
	  pp_linkset_add((*rules)[r].link_set, p); 
	  (*rules)[r].link_array[i]=p;
	}
      (*rules)[r].link_array[i]=0; /* NULL-terminator */

      /* read error message */
      tokens = pp_lexer_get_next_group_of_tokens_of_label(k->lt, &n_tokens);
      if (n_tokens>1) 
	error("post_process: Invalid syntax in %s (rule %i)",label,r+1);
      (*rules)[r].msg = string_set_add(tokens[0], k->string_set);
    }
  
  /* sentinel entry */
  (*rules)[*nRules].msg = 0;
}


static void read_rules(pp_knowledge *k)
{
  read_form_a_cycle_rules(k, "FORM_A_CYCLE_RULES");
  read_connected_rule(k, "CONNECTED_RULES");
  read_bounded_rules(k,  "BOUNDED_RULES");
  read_contains_rules(k, "CONTAINS_ONE_RULES" ,
		      &(k->contains_one_rules), &(k->n_contains_one_rules)); 
  read_contains_rules(k, "CONTAINS_NONE_RULES",
		      &(k->contains_none_rules), &(k->n_contains_none_rules)); 
}

static void free_rules(pp_knowledge *k)
{
  int r;
  int rs=sizeof(pp_rule);
  pp_rule *rule;
  for (r=0; k->contains_one_rules[r].msg!=0; r++) {
    rule = &(k->contains_one_rules[r]);    /* shorthand */
    xfree((void*) rule->link_array, (1+rule->link_set_size)*sizeof(char*));
    pp_linkset_close(rule->link_set);
  }
  for (r=0; k->contains_none_rules[r].msg!=0; r++) {
    rule = &(k->contains_none_rules[r]);   /* shorthand */
    xfree((void *)rule->link_array, (1+rule->link_set_size)*sizeof(char*));
    pp_linkset_close(rule->link_set);
  }

  for (r=0; r<k->n_form_a_cycle_rules; r++) 
    pp_linkset_close(k->form_a_cycle_rules[r].link_set);
  xfree((void*)k->bounded_rules,           rs*(1+k->n_bounded_rules));
  xfree((void*)k->connected_rules,         rs);
  xfree((void*)k->form_a_cycle_rules, rs*(1+k->n_form_a_cycle_rules));
  xfree((void*)k->contains_one_rules,      rs*(1+k->n_contains_one_rules));
  xfree((void*)k->contains_none_rules,     rs*(1+k->n_contains_none_rules));
}

/********************* exported functions ***************************/

pp_knowledge *pp_knowledge_open(char *dictname, char *path)
{
  /* read knowledge from disk into pp_knowledge */
  FILE *f=dictopen(dictname, path, "r");
  pp_knowledge *k = (pp_knowledge *) xalloc (sizeof(pp_knowledge));
  if (!f) error("Couldn't find post-process knowledge file %s", path);
  k->lt = pp_lexer_open(f);
  fclose(f);
  k->string_set = string_set_create();
  k->path = string_set_add(path, k->string_set);
  read_starting_link_table(k);
  read_link_sets(k);
  read_rules(k);
  initialize_set_of_links_starting_bounded_domain(k);
  return k;
}

void pp_knowledge_close(pp_knowledge *k)
{
  /* clear the memory taken up by k */
  xfree((void*)k->starting_link_lookup_table,
	((1+k->nStartingLinks)*sizeof(StartingLinkAndDomain)));
  free_link_sets(k);
  free_rules(k);
  pp_linkset_close(k->set_of_links_starting_bounded_domain);
  string_set_delete(k->string_set);
  pp_lexer_close(k->lt);
  xfree((void*)k, sizeof(pp_knowledge));
}




/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

/* I edited this file to eliminate compiler warnings.  I've documented here
   all the changes.  The .fl file from which this is derived is in
   pp_lexer-flex-file.  Here are all the warnings I got:

     pp_lexer.c: In function `pp_lexer_lex':
     pp_lexer.c:841: warning: implicit declaration of function `yywrap'
     pp_lexer.c:689: warning: label `find_rule' defined but not used
     pp_lexer.c: At top level:
     pp_lexer.c:1590: warning: `yy_flex_realloc' defined but not used
     pp_lexer.c:1099: warning: `yyunput' defined but not used
     pp_lexer.c:1814: warning: `show_bindings' defined but not used


   Here are the changes I made.  All of them are labeled with "--DS" in
   the code.

     Got rid of #line directives.
     Added the prototype of yywrpa() right below this comment.
     Commented out code and prototype declarations for the unused functions
 */

int yywrap(void);  /* --DS */

#define yy_create_buffer pp_lexer__create_buffer
#define yy_delete_buffer pp_lexer__delete_buffer
#define yy_scan_buffer pp_lexer__scan_buffer
#define yy_scan_string pp_lexer__scan_string
#define yy_scan_bytes pp_lexer__scan_bytes
#define yy_flex_debug pp_lexer__flex_debug
#define yy_init_buffer pp_lexer__init_buffer
#define yy_flush_buffer pp_lexer__flush_buffer
#define yy_load_buffer_state pp_lexer__load_buffer_state
#define yy_switch_to_buffer pp_lexer__switch_to_buffer
#define yyin pp_lexer_in
#define yyleng pp_lexer_leng
#define yylex pp_lexer_lex
#define yyout pp_lexer_out
#define yyrestart pp_lexer_restart
#define yytext pp_lexer_text
#define yylineno pp_lexer_lineno
#define yywrap pp_lexer_wrap

/* #line 21 "pp_lexer.c" --DS */
/* A lexical scanner generated by flex */

/* Scanner skeleton version:
 * $Header: /usr4/sleator/link-6/041/src/RCS/pp_lexer.c,v 1.7 98/04/10 19:05:57 sleator Exp Locker: sleator $
 */

#define FLEX_SCANNER
#define YY_FLEX_MAJOR_VERSION 2
#define YY_FLEX_MINOR_VERSION 5

#include <stdio.h>

#if 0
     /* for some reason I needed to include these with linux
	and sun4_55 when using gcc -ansi.  Not necessary
	without the -ansi.
      */
char *strdup(const char *s);
int fileno( FILE *stream); 
#endif

/* cfront 1.2 defines "c_plusplus" instead of "__cplusplus" */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif


#ifdef __cplusplus

#include <stdlib.h>
#include <unistd.h>

/* Use prototypes in function declarations. */
#define YY_USE_PROTOS

/* The "const" storage-class-modifier is valid. */
#define YY_USE_CONST

#else	/* ! __cplusplus */

#if __STDC__

#define YY_USE_PROTOS
#define YY_USE_CONST

#endif	/* __STDC__ */
#endif	/* ! __cplusplus */

#ifdef __TURBOC__
 #pragma warn -rch
 #pragma warn -use
#include <io.h>
#include <stdlib.h>
#define YY_USE_CONST
#define YY_USE_PROTOS
#endif

#ifndef YY_USE_CONST
#ifndef const
#define const
#endif
#endif


#ifdef YY_USE_PROTOS
#define YY_PROTO(proto) proto
#else
#define YY_PROTO(proto) ()
#endif

/* Returned upon end-of-file. */
#define YY_NULL 0

/* Promotes a possibly negative, possibly signed char to an unsigned
 * integer for use as an array index.  If the signed char is negative,
 * we want to instead treat it as an 8-bit unsigned char, hence the
 * double cast.
 */
#define YY_SC_TO_UI(c) ((unsigned int) (unsigned char) c)

/* Enter a start condition.  This macro really ought to take a parameter,
 * but we do it the disgusting crufty way forced on us by the ()-less
 * definition of BEGIN.
 */
#define BEGIN yy_start = 1 + 2 *

/* Translate the current start state into a value that can be later handed
 * to BEGIN to return to the state.  The YYSTATE alias is for lex
 * compatibility.
 */
#define YY_START ((yy_start - 1) / 2)
#define YYSTATE YY_START

/* Action number for EOF rule of a given start state. */
#define YY_STATE_EOF(state) (YY_END_OF_BUFFER + state + 1)

/* Special action meaning "start processing a new file". */
#define YY_NEW_FILE yyrestart( yyin )

#define YY_END_OF_BUFFER_CHAR 0

/* Size of default input buffer. */
#define YY_BUF_SIZE 16384

typedef struct yy_buffer_state *YY_BUFFER_STATE;

extern int yyleng;
extern FILE *yyin, *yyout;

#define EOB_ACT_CONTINUE_SCAN 0
#define EOB_ACT_END_OF_FILE 1
#define EOB_ACT_LAST_MATCH 2

/* The funky do-while in the following #define is used to turn the definition
 * int a single C statement (which needs a semi-colon terminator).  This
 * avoids problems with code like:
 *
 * 	if ( condition_holds )
 *		yyless( 5 );
 *	else
 *		do_something_else();
 *
 * Prior to using the do-while the compiler would get upset at the
 * "else" because it interpreted the "if" statement as being all
 * done when it reached the ';' after the yyless() call.
 */

/* Return all but the first 'n' matched characters back to the input stream. */

#define yyless(n) \
	do \
		{ \
		/* Undo effects of setting up yytext. */ \
		*yy_cp = yy_hold_char; \
		yy_c_buf_p = yy_cp = yy_bp + n - YY_MORE_ADJ; \
		YY_DO_BEFORE_ACTION; /* set up yytext again */ \
		} \
	while ( 0 )

 /* #define unput(c) yyunput( c, yytext_ptr ) --DS */

/* The following is because we cannot portably get our hands on size_t
 * (without autoconf's help, which isn't available because we want
 * flex-generated scanners to compile on their own).
 */
typedef unsigned int yy_size_t;


struct yy_buffer_state
	{
	FILE *yy_input_file;

	char *yy_ch_buf;		/* input buffer */
	char *yy_buf_pos;		/* current position in input buffer */

	/* Size of input buffer in bytes, not including room for EOB
	 * characters.
	 */
	yy_size_t yy_buf_size;

	/* Number of characters read into yy_ch_buf, not including EOB
	 * characters.
	 */
	int yy_n_chars;

	/* Whether we "own" the buffer - i.e., we know we created it,
	 * and can realloc() it to grow it, and should free() it to
	 * delete it.
	 */
	int yy_is_our_buffer;

	/* Whether this is an "interactive" input source; if so, and
	 * if we're using stdio for input, then we want to use getc()
	 * instead of fread(), to make sure we stop fetching input after
	 * each newline.
	 */
	int yy_is_interactive;

	/* Whether we're considered to be at the beginning of a line.
	 * If so, '^' rules will be active on the next match, otherwise
	 * not.
	 */
	int yy_at_bol;

	/* Whether to try to fill the input buffer when we reach the
	 * end of it.
	 */
	int yy_fill_buffer;

	int yy_buffer_status;
#define YY_BUFFER_NEW 0
#define YY_BUFFER_NORMAL 1
	/* When an EOF's been seen but there's still some text to process
	 * then we mark the buffer as YY_EOF_PENDING, to indicate that we
	 * shouldn't try reading from the input source any more.  We might
	 * still have a bunch of tokens to match, though, because of
	 * possible backing-up.
	 *
	 * When we actually see the EOF, we change the status to "new"
	 * (via yyrestart()), so that the user can continue scanning by
	 * just pointing yyin at a new input file.
	 */
#define YY_BUFFER_EOF_PENDING 2
	};

static YY_BUFFER_STATE yy_current_buffer = 0;

/* We provide macros for accessing buffer states in case in the
 * future we want to put the buffer states in a more general
 * "scanner state".
 */
#define YY_CURRENT_BUFFER yy_current_buffer


/* yy_hold_char holds the character lost when yytext is formed. */
static char yy_hold_char;

static int yy_n_chars;		/* number of characters read into yy_ch_buf */


int yyleng;

/* Points to current character in buffer. */
static char *yy_c_buf_p = (char *) 0;
static int yy_init = 1;		/* whether we need to initialize */
static int yy_start = 0;	/* start state number */

/* Flag which is used to allow yywrap()'s to do buffer switches
 * instead of setting up a fresh yyin.  A bit of a hack ...
 */
static int yy_did_buffer_switch_on_eof;

void yyrestart YY_PROTO(( FILE *input_file ));

void yy_switch_to_buffer YY_PROTO(( YY_BUFFER_STATE new_buffer ));
void yy_load_buffer_state YY_PROTO(( void ));
YY_BUFFER_STATE yy_create_buffer YY_PROTO(( FILE *file, int size ));
void yy_delete_buffer YY_PROTO(( YY_BUFFER_STATE b ));
void yy_init_buffer YY_PROTO(( YY_BUFFER_STATE b, FILE *file ));
void yy_flush_buffer YY_PROTO(( YY_BUFFER_STATE b ));
#define YY_FLUSH_BUFFER yy_flush_buffer( yy_current_buffer )

YY_BUFFER_STATE yy_scan_buffer YY_PROTO(( char *base, yy_size_t size ));
YY_BUFFER_STATE yy_scan_string YY_PROTO(( const char *str ));
YY_BUFFER_STATE yy_scan_bytes YY_PROTO(( const char *bytes, int len ));

static void *yy_flex_alloc YY_PROTO(( yy_size_t ));
/* static void *yy_flex_realloc YY_PROTO(( void *, yy_size_t )); --DS */
static void yy_flex_free YY_PROTO(( void * ));

#define yy_new_buffer yy_create_buffer

#define yy_set_interactive(is_interactive) \
	{ \
	if ( ! yy_current_buffer ) \
		yy_current_buffer = yy_create_buffer( yyin, YY_BUF_SIZE ); \
	yy_current_buffer->yy_is_interactive = is_interactive; \
	}

#define yy_set_bol(at_bol) \
	{ \
	if ( ! yy_current_buffer ) \
		yy_current_buffer = yy_create_buffer( yyin, YY_BUF_SIZE ); \
	yy_current_buffer->yy_at_bol = at_bol; \
	}

#define YY_AT_BOL() (yy_current_buffer->yy_at_bol)


#define YY_USES_REJECT
typedef unsigned char YY_CHAR;
#ifdef VMS
#ifndef __VMS_POSIX
FILE *yyin = (FILE *) 0, *yyout = (FILE *) 0;
#else
FILE *yyin = stdin, *yyout = stdout;
#endif
#else
FILE *yyin = (FILE *) 0, *yyout = (FILE *) 0;
#endif
typedef int yy_state_type;
extern int yylineno;
int yylineno = 1;
extern char *yytext;
#define yytext_ptr yytext

#ifndef YY_SKIP_YYWRAP
#ifdef __cplusplus
extern "C" int yywrap YY_PROTO(( void ));
#else
extern int yywrap YY_PROTO(( void ));
#endif
#endif

#if 0  /* --DS */
#ifndef YY_NO_UNPUT
static void yyunput YY_PROTO(( int c, char *buf_ptr ));
#endif
#endif /* --DS */

#ifndef yytext_ptr
static void yy_flex_strncpy YY_PROTO(( char *, const char *, int ));
#endif

#ifndef YY_NO_INPUT
#ifdef __cplusplus
static int yyinput YY_PROTO(( void ));
#else
static int input YY_PROTO(( void ));
#endif
#endif

static yy_state_type yy_get_previous_state YY_PROTO(( void ));
static yy_state_type yy_try_NUL_trans YY_PROTO(( yy_state_type current_state ));
static int yy_get_next_buffer YY_PROTO(( void ));
static void yy_fatal_error YY_PROTO(( const char msg[] ));

/* Done after the current pattern has been matched and before the
 * corresponding action - sets up yytext.
 */
#define YY_DO_BEFORE_ACTION \
	yytext_ptr = yy_bp; \
	yyleng = (int) (yy_cp - yy_bp); \
	yy_hold_char = *yy_cp; \
	*yy_cp = '\0'; \
	yy_c_buf_p = yy_cp;

#define YY_NUM_RULES 8
#define YY_END_OF_BUFFER 9
static const short int yy_acclist[39] =
    {   0,
        2,    2,    2,    2,    9,    5,    7,    8,    2,    7,
        8,    2,    8,    7,    8,    7,    8,    5,    7,    8,
        2,    7,    8,    5,    7,    8,    7,    8,    5,    3,
        2,    4,    5,    2,    5,    1,    3,    6
    } ;

static const short int yy_accept[29] =
    {   0,
        1,    2,    3,    4,    5,    6,    9,   12,   14,   16,
       18,   21,   24,   27,   29,   30,   31,   32,   32,   34,
       35,   35,   36,   36,   37,   38,   39,   39
    } ;

static const int yy_ec[256] =
    {   0,
        1,    1,    1,    1,    1,    1,    1,    1,    2,    3,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    4,    1,    5,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    6,    7,    1,
        1,    1,    1,    8,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,

        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,

        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1
    } ;

static const int yy_meta[9] =
    {   0,
        1,    2,    2,    3,    4,    5,    1,    1
    } ;

static const short int yy_base[32] =
    {   0,
        0,    7,   14,   21,   18,   11,    0,    0,    0,   66,
       10,   27,   34,   42,    7,   66,    0,    7,    4,    0,
        0,    0,    0,   66,    0,   66,   66,   50,   54,   57,
       60
    } ;

static const short int yy_def[32] =
    {   0,
       27,    1,    1,    3,   27,   28,   29,   29,   30,   27,
       31,   27,   27,   13,   28,   27,   29,   30,   31,   12,
       14,   13,   14,   27,   14,   27,    0,   27,   27,   27,
       27
    } ;

static const short int yy_nxt[75] =
    {   0,
        6,    7,    8,    7,    9,   10,    6,   11,   12,   16,
       12,   26,   16,   13,   10,   16,   16,   27,   10,   27,
       10,   10,   12,   27,   12,   27,   27,   14,   20,   17,
       20,   27,   27,   21,   22,   23,   24,   23,   23,   25,
       22,   22,   23,   27,   27,   27,   27,   23,   23,   23,
       15,   27,   27,   27,   15,   17,   17,   18,   27,   18,
       19,   27,   27,   27,   19,    5,   27,   27,   27,   27,
       27,   27,   27,   27
    } ;

static const short int yy_chk[75] =
    {   0,
        1,    1,    1,    1,    1,    1,    1,    1,    2,   19,
        2,   18,   15,    2,    3,   11,    6,    5,    3,    0,
        3,    3,    4,    0,    4,    0,    0,    4,   12,   12,
       12,    0,    0,   12,   13,   13,   13,   13,   13,   13,
       13,   13,   14,    0,    0,    0,    0,   14,   14,   14,
       28,    0,    0,    0,   28,   29,   29,   30,    0,   30,
       31,    0,    0,    0,   31,   27,   27,   27,   27,   27,
       27,   27,   27,   27
    } ;

static yy_state_type yy_state_buf[YY_BUF_SIZE + 2], *yy_state_ptr;
static char *yy_full_match;
static int yy_lp;
#define REJECT \
{ \
*yy_cp = yy_hold_char; /* undo effects of setting up yytext */ \
yy_cp = yy_full_match; /* restore poss. backed-over text */ \
++yy_lp; \
goto find_rule; \
}
#define yymore() yymore_used_but_not_detected
#define YY_MORE_ADJ 0
char *yytext;
/* #line 1 "pp_lexer.fl" --DS */
#define INITIAL 0
/* #line 2 "pp_lexer.fl" --DS */
/**************************************************************************
    Lex specification for post-process knowledge file 
    6/96 ALB
    Updated 8/97 to allow multiple instances
    Compile with either  
      1) flex pp_lexer.fl (on systems which support %option prefix) OR
      2) flex pp_lexer.fl
         mv lex.yy.c pp_lexer.tmp.c
         cat pp_lexer.tmp.c | sed "s/yy/pp_lexer_/g" > pp_lexer.c
         rm -f pp_lexer.tmp.c
             (on systems which do not)

 In the case of (1), uncomment the three %option lines below. 
**************************************************************************/

#include <stdarg.h>
#include "link-includes.h"

#undef yywrap

extern void error(char *fmt, ...);

/* forward references for non-exported functions (and static variable) */
static void check_string(const char *str);
static void setup(PPLexTable *lt);
static void set_label(PPLexTable *lt, const char *label);
static void add_string_to_label(PPLexTable *lt, char *str);
static void add_set_of_strings_to_label(PPLexTable *lt,const char *label_of_set);
/* static void show_bindings(PPLexTable *lt); --DS */
static int  get_index_of_label(PPLexTable *lt, const char *label);
static PPLexTable *clt=NULL; /* ptr to lex table we're currently filling in */
/* see above */
#define INCLUDE 1

/* #line 490 "pp_lexer.c" --DS */

/* Macros after this point can all be overridden by user definitions in
 * section 1.
 */

#if YY_STACK_USED
static int yy_start_stack_ptr = 0;
static int yy_start_stack_depth = 0;
static int *yy_start_stack = 0;
#ifndef YY_NO_PUSH_STATE
static void yy_push_state YY_PROTO(( int new_state ));
#endif
#ifndef YY_NO_POP_STATE
static void yy_pop_state YY_PROTO(( void ));
#endif
#ifndef YY_NO_TOP_STATE
static int yy_top_state YY_PROTO(( void ));
#endif

#else
#define YY_NO_PUSH_STATE 1
#define YY_NO_POP_STATE 1
#define YY_NO_TOP_STATE 1
#endif

#ifdef YY_MALLOC_DECL
YY_MALLOC_DECL
#else
#if __STDC__
#ifndef __cplusplus
#include <stdlib.h>
#endif
#else
/* Just try to get by without declaring the routines.  This will fail
 * miserably on non-ANSI systems for which sizeof(size_t) != sizeof(int)
 * or sizeof(void*) != sizeof(int).
 */
#endif
#endif

/* Amount of stuff to slurp up with each read. */
#ifndef YY_READ_BUF_SIZE
#define YY_READ_BUF_SIZE 8192
#endif

/* Copy whatever the last rule matched to the standard output. */

#ifndef ECHO
/* This used to be an fputs(), but since the string might contain NUL's,
 * we now use fwrite().
 */
#define ECHO (void) fwrite( yytext, yyleng, 1, yyout )
#endif

/* Gets input and stuffs it into "buf".  number of characters read, or YY_NULL,
 * is returned in "result".
 */
#ifndef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( yy_current_buffer->yy_is_interactive ) \
		{ \
		int c = '*', n; \
		for ( n = 0; n < max_size && \
			     (c = getc( yyin )) != EOF && c != '\n'; ++n ) \
			buf[n] = (char) c; \
		if ( c == '\n' ) \
			buf[n++] = (char) c; \
		if ( c == EOF && ferror( yyin ) ) \
			YY_FATAL_ERROR( "input in flex scanner failed" ); \
		result = n; \
		} \
	else if ( ((result = fread( buf, 1, max_size, yyin )) == 0) \
		  && ferror( yyin ) ) \
		YY_FATAL_ERROR( "input in flex scanner failed" );
#endif

/* No semi-colon after return; correct usage is to write "yyterminate();" -
 * we don't want an extra ';' after the "return" because that will cause
 * some compilers to complain about unreachable statements.
 */
#ifndef yyterminate
#define yyterminate() return YY_NULL
#endif

/* Number of entries by which start-condition stack grows. */
#ifndef YY_START_STACK_INCR
#define YY_START_STACK_INCR 25
#endif

/* Report a fatal error. */
#ifndef YY_FATAL_ERROR
#define YY_FATAL_ERROR(msg) yy_fatal_error( msg )
#endif

/* Default declaration of generated scanner - a define so the user can
 * easily add parameters.
 */
#ifndef YY_DECL
#define YY_DECL int yylex YY_PROTO(( void ))
#endif

/* Code executed at the beginning of each rule, after yytext and yyleng
 * have been set up.
 */
#ifndef YY_USER_ACTION
#define YY_USER_ACTION
#endif

/* Code executed at the end of each rule. */
#ifndef YY_BREAK
#define YY_BREAK break;
#endif

#define YY_RULE_SETUP \
	if ( yyleng > 0 ) \
		yy_current_buffer->yy_at_bol = \
				(yytext[yyleng - 1] == '\n'); \
	YY_USER_ACTION

YY_DECL
	{
	register yy_state_type yy_current_state;
	register char *yy_cp, *yy_bp;
	register int yy_act;

	/* #line 56 "pp_lexer.fl" --DS */


	/* #line 619 "pp_lexer.c" --DS */

	if ( yy_init )
		{
		yy_init = 0;

#ifdef YY_USER_INIT
		YY_USER_INIT;
#endif

		if ( ! yy_start )
			yy_start = 1;	/* first start state */

		if ( ! yyin )
			yyin = stdin;

		if ( ! yyout )
			yyout = stdout;

		if ( ! yy_current_buffer )
			yy_current_buffer =
				yy_create_buffer( yyin, YY_BUF_SIZE );

		yy_load_buffer_state();
		}

	while ( 1 )		/* loops until end-of-file is reached */
		{
		yy_cp = yy_c_buf_p;

		/* Support of yytext. */
		*yy_cp = yy_hold_char;

		/* yy_bp points to the position in yy_ch_buf of the start of
		 * the current run.
		 */
		yy_bp = yy_cp;

		yy_current_state = yy_start;
		yy_current_state += YY_AT_BOL();
		yy_state_ptr = yy_state_buf;
		*yy_state_ptr++ = yy_current_state;
yy_match:
		do
			{
			register YY_CHAR yy_c = yy_ec[YY_SC_TO_UI(*yy_cp)];
			while ( yy_chk[yy_base[yy_current_state] + yy_c] != yy_current_state )
				{
				yy_current_state = (int) yy_def[yy_current_state];
				if ( yy_current_state >= 28 )
					yy_c = yy_meta[(unsigned int) yy_c];
				}
			yy_current_state = yy_nxt[yy_base[yy_current_state] + (unsigned int) yy_c];
			*yy_state_ptr++ = yy_current_state;
			++yy_cp;
			}
		while ( yy_base[yy_current_state] != 66 );

yy_find_action:
		yy_current_state = *--yy_state_ptr;
		yy_lp = yy_accept[yy_current_state];
/* find_rule: --DS */ /* we branch to this label when backing up */
		for ( ; ; ) /* until we find what rule we matched */
			{
			if ( yy_lp && yy_lp < yy_accept[yy_current_state + 1] )
				{
				yy_act = yy_acclist[yy_lp];
					{
					yy_full_match = yy_cp;
					break;
					}
				}
			--yy_cp;
			yy_current_state = *--yy_state_ptr;
			yy_lp = yy_accept[yy_current_state];
			}

		YY_DO_BEFORE_ACTION;

		if ( yy_act != YY_END_OF_BUFFER )
			{
			int yyl;
			for ( yyl = 0; yyl < yyleng; ++yyl )
				if ( yytext[yyl] == '\n' )
					++yylineno;
			}

do_action:	/* This label is used only to access EOF actions. */


		switch ( yy_act )
	{ /* beginning of action switch */
case 1:
*yy_cp = yy_hold_char; /* undo effects of setting up yytext */
yy_c_buf_p = yy_cp -= 1;
YY_DO_BEFORE_ACTION; /* set up yytext again */
YY_RULE_SETUP
    /* #line 58 "pp_lexer.fl" --DS */
; 
	YY_BREAK
case 2:
YY_RULE_SETUP
	    /* #line 59 "pp_lexer.fl" --DS */
; 
	YY_BREAK
case 3:
YY_RULE_SETUP
	    /* #line 61 "pp_lexer.fl" --DS */
{ set_label(clt, yytext); }
	YY_BREAK
case 4:
YY_RULE_SETUP
	    /* #line 62 "pp_lexer.fl" --DS */
{ add_set_of_strings_to_label(clt, &(yytext[1])); }
	YY_BREAK
case 5:
YY_RULE_SETUP
	/* #line 63 "pp_lexer.fl" --DS */
{ add_string_to_label(clt, yytext); }
	YY_BREAK
case 6:
YY_RULE_SETUP
	/* #line 64 "pp_lexer.fl" --DS */
{ add_string_to_label(clt, yytext); }
	YY_BREAK
case 7:
YY_RULE_SETUP
	/* #line 66 "pp_lexer.fl" --DS */
{ error("pp_lexer: unable to parse knowledge file (line %i).\n",yylineno); }
	YY_BREAK
case 8:
YY_RULE_SETUP
	/* #line 68 "pp_lexer.fl" --DS */
ECHO;
	YY_BREAK
	/* #line 754 "pp_lexer.c" --DS */
			case YY_STATE_EOF(INITIAL):
			case YY_STATE_EOF(INCLUDE):
				yyterminate();

	case YY_END_OF_BUFFER:
		{
		/* Amount of text matched not including the EOB char. */
		int yy_amount_of_matched_text = (int) (yy_cp - yytext_ptr) - 1;

		/* Undo the effects of YY_DO_BEFORE_ACTION. */
		*yy_cp = yy_hold_char;

		if ( yy_current_buffer->yy_buffer_status == YY_BUFFER_NEW )
			{
			/* We're scanning a new file or input source.  It's
			 * possible that this happened because the user
			 * just pointed yyin at a new source and called
			 * yylex().  If so, then we have to assure
			 * consistency between yy_current_buffer and our
			 * globals.  Here is the right place to do so, because
			 * this is the first action (other than possibly a
			 * back-up) that will match for the new input source.
			 */
			yy_n_chars = yy_current_buffer->yy_n_chars;
			yy_current_buffer->yy_input_file = yyin;
			yy_current_buffer->yy_buffer_status = YY_BUFFER_NORMAL;
			}

		/* Note that here we test for yy_c_buf_p "<=" to the position
		 * of the first EOB in the buffer, since yy_c_buf_p will
		 * already have been incremented past the NUL character
		 * (since all states make transitions on EOB to the
		 * end-of-buffer state).  Contrast this with the test
		 * in input().
		 */
		if ( yy_c_buf_p <= &yy_current_buffer->yy_ch_buf[yy_n_chars] )
			{ /* This was really a NUL. */
			yy_state_type yy_next_state;

			yy_c_buf_p = yytext_ptr + yy_amount_of_matched_text;

			yy_current_state = yy_get_previous_state();

			/* Okay, we're now positioned to make the NUL
			 * transition.  We couldn't have
			 * yy_get_previous_state() go ahead and do it
			 * for us because it doesn't know how to deal
			 * with the possibility of jamming (and we don't
			 * want to build jamming into it because then it
			 * will run more slowly).
			 */

			yy_next_state = yy_try_NUL_trans( yy_current_state );

			yy_bp = yytext_ptr + YY_MORE_ADJ;

			if ( yy_next_state )
				{
				/* Consume the NUL. */
				yy_cp = ++yy_c_buf_p;
				yy_current_state = yy_next_state;
				goto yy_match;
				}

			else
				{
				yy_cp = yy_c_buf_p;
				goto yy_find_action;
				}
			}

		else switch ( yy_get_next_buffer() )
			{
			case EOB_ACT_END_OF_FILE:
				{
				yy_did_buffer_switch_on_eof = 0;

				if ( yywrap() )
					{
					/* Note: because we've taken care in
					 * yy_get_next_buffer() to have set up
					 * yytext, we can now set up
					 * yy_c_buf_p so that if some total
					 * hoser (like flex itself) wants to
					 * call the scanner after we return the
					 * YY_NULL, it'll still work - another
					 * YY_NULL will get returned.
					 */
					yy_c_buf_p = yytext_ptr + YY_MORE_ADJ;

					yy_act = YY_STATE_EOF(YY_START);
					goto do_action;
					}

				else
					{
					if ( ! yy_did_buffer_switch_on_eof )
						YY_NEW_FILE;
					}
				break;
				}

			case EOB_ACT_CONTINUE_SCAN:
				yy_c_buf_p =
					yytext_ptr + yy_amount_of_matched_text;

				yy_current_state = yy_get_previous_state();

				yy_cp = yy_c_buf_p;
				yy_bp = yytext_ptr + YY_MORE_ADJ;
				goto yy_match;

			case EOB_ACT_LAST_MATCH:
				yy_c_buf_p =
				&yy_current_buffer->yy_ch_buf[yy_n_chars];

				yy_current_state = yy_get_previous_state();

				yy_cp = yy_c_buf_p;
				yy_bp = yytext_ptr + YY_MORE_ADJ;
				goto yy_find_action;
			}
		break;
		}

	default:
		YY_FATAL_ERROR(
			"fatal flex scanner internal error--no action found" );
	} /* end of action switch */
		} /* end of scanning one token */
	} /* end of yylex */


/* yy_get_next_buffer - try to read in a new buffer
 *
 * Returns a code representing an action:
 *	EOB_ACT_LAST_MATCH -
 *	EOB_ACT_CONTINUE_SCAN - continue scanning from current position
 *	EOB_ACT_END_OF_FILE - end of file
 */

static int yy_get_next_buffer()
	{
	register char *dest = yy_current_buffer->yy_ch_buf;
	register char *source = yytext_ptr;
	register int number_to_move, i;
	int ret_val;

	if ( yy_c_buf_p > &yy_current_buffer->yy_ch_buf[yy_n_chars + 1] )
		YY_FATAL_ERROR(
		"fatal flex scanner internal error--end of buffer missed" );

	if ( yy_current_buffer->yy_fill_buffer == 0 )
		{ /* Don't try to fill the buffer, so this is an EOF. */
		if ( yy_c_buf_p - yytext_ptr - YY_MORE_ADJ == 1 )
			{
			/* We matched a singled characater, the EOB, so
			 * treat this as a final EOF.
			 */
			return EOB_ACT_END_OF_FILE;
			}

		else
			{
			/* We matched some text prior to the EOB, first
			 * process it.
			 */
			return EOB_ACT_LAST_MATCH;
			}
		}

	/* Try to read more data. */

	/* First move last chars to start of buffer. */
	number_to_move = (int) (yy_c_buf_p - yytext_ptr) - 1;

	for ( i = 0; i < number_to_move; ++i )
		*(dest++) = *(source++);

	if ( yy_current_buffer->yy_buffer_status == YY_BUFFER_EOF_PENDING )
		/* don't do the read, it's not guaranteed to return an EOF,
		 * just force an EOF
		 */
		yy_n_chars = 0;

	else
		{
		int num_to_read =
			yy_current_buffer->yy_buf_size - number_to_move - 1;

		while ( num_to_read <= 0 )
			{ /* Not enough room in the buffer - grow it. */
#ifdef YY_USES_REJECT
			YY_FATAL_ERROR(
"input buffer overflow, can't enlarge buffer because scanner uses REJECT" );
#else

			/* just a shorter name for the current buffer */
			YY_BUFFER_STATE b = yy_current_buffer;

			int yy_c_buf_p_offset =
				(int) (yy_c_buf_p - b->yy_ch_buf);

			if ( b->yy_is_our_buffer )
				{
				int new_size = b->yy_buf_size * 2;

				if ( new_size <= 0 )
					b->yy_buf_size += b->yy_buf_size / 8;
				else
					b->yy_buf_size *= 2;

				b->yy_ch_buf = (char *)
					/* Include room in for 2 EOB chars. */
					yy_flex_realloc( (void *) b->yy_ch_buf,
							 b->yy_buf_size + 2 );
				}
			else
				/* Can't grow it, we don't own it. */
				b->yy_ch_buf = 0;

			if ( ! b->yy_ch_buf )
				YY_FATAL_ERROR(
				"fatal error - scanner input buffer overflow" );

			yy_c_buf_p = &b->yy_ch_buf[yy_c_buf_p_offset];

			num_to_read = yy_current_buffer->yy_buf_size -
						number_to_move - 1;
#endif
			}

		if ( num_to_read > YY_READ_BUF_SIZE )
			num_to_read = YY_READ_BUF_SIZE;

		/* Read in more data. */
		YY_INPUT( (&yy_current_buffer->yy_ch_buf[number_to_move]),
			yy_n_chars, num_to_read );
		}

	if ( yy_n_chars == 0 )
		{
		if ( number_to_move == YY_MORE_ADJ )
			{
			ret_val = EOB_ACT_END_OF_FILE;
			yyrestart( yyin );
			}

		else
			{
			ret_val = EOB_ACT_LAST_MATCH;
			yy_current_buffer->yy_buffer_status =
				YY_BUFFER_EOF_PENDING;
			}
		}

	else
		ret_val = EOB_ACT_CONTINUE_SCAN;

	yy_n_chars += number_to_move;
	yy_current_buffer->yy_ch_buf[yy_n_chars] = YY_END_OF_BUFFER_CHAR;
	yy_current_buffer->yy_ch_buf[yy_n_chars + 1] = YY_END_OF_BUFFER_CHAR;

	yytext_ptr = &yy_current_buffer->yy_ch_buf[0];

	return ret_val;
	}


/* yy_get_previous_state - get the state just before the EOB char was reached */

static yy_state_type yy_get_previous_state()
	{
	register yy_state_type yy_current_state;
	register char *yy_cp;

	yy_current_state = yy_start;
	yy_current_state += YY_AT_BOL();
	yy_state_ptr = yy_state_buf;
	*yy_state_ptr++ = yy_current_state;

	for ( yy_cp = yytext_ptr + YY_MORE_ADJ; yy_cp < yy_c_buf_p; ++yy_cp )
		{
		register YY_CHAR yy_c = (*yy_cp ? yy_ec[YY_SC_TO_UI(*yy_cp)] : 1);
		while ( yy_chk[yy_base[yy_current_state] + yy_c] != yy_current_state )
			{
			yy_current_state = (int) yy_def[yy_current_state];
			if ( yy_current_state >= 28 )
				yy_c = yy_meta[(unsigned int) yy_c];
			}
		yy_current_state = yy_nxt[yy_base[yy_current_state] + (unsigned int) yy_c];
		*yy_state_ptr++ = yy_current_state;
		}

	return yy_current_state;
	}


/* yy_try_NUL_trans - try to make a transition on the NUL character
 *
 * synopsis
 *	next_state = yy_try_NUL_trans( current_state );
 */

#ifdef YY_USE_PROTOS
static yy_state_type yy_try_NUL_trans( yy_state_type yy_current_state )
#else
static yy_state_type yy_try_NUL_trans( yy_current_state )
yy_state_type yy_current_state;
#endif
	{
	register int yy_is_jam;

	register YY_CHAR yy_c = 1;
	while ( yy_chk[yy_base[yy_current_state] + yy_c] != yy_current_state )
		{
		yy_current_state = (int) yy_def[yy_current_state];
		if ( yy_current_state >= 28 )
			yy_c = yy_meta[(unsigned int) yy_c];
		}
	yy_current_state = yy_nxt[yy_base[yy_current_state] + (unsigned int) yy_c];
	*yy_state_ptr++ = yy_current_state;
	yy_is_jam = (yy_current_state == 27);

	return yy_is_jam ? 0 : yy_current_state;
	}


#if 0  /* --DS */
#ifdef YY_USE_PROTOS
static void yyunput( int c, register char *yy_bp )
#else
static void yyunput( c, yy_bp )
int c;
register char *yy_bp;
#endif
	{
	register char *yy_cp = yy_c_buf_p;

	/* undo effects of setting up yytext */
	*yy_cp = yy_hold_char;

	if ( yy_cp < yy_current_buffer->yy_ch_buf + 2 )
		{ /* need to shift things up to make room */
		/* +2 for EOB chars. */
		register int number_to_move = yy_n_chars + 2;
		register char *dest = &yy_current_buffer->yy_ch_buf[
					yy_current_buffer->yy_buf_size + 2];
		register char *source =
				&yy_current_buffer->yy_ch_buf[number_to_move];

		while ( source > yy_current_buffer->yy_ch_buf )
			*--dest = *--source;

		yy_cp += (int) (dest - source);
		yy_bp += (int) (dest - source);
		yy_n_chars = yy_current_buffer->yy_buf_size;

		if ( yy_cp < yy_current_buffer->yy_ch_buf + 2 )
			YY_FATAL_ERROR( "flex scanner push-back overflow" );
		}

	*--yy_cp = (char) c;

	if ( c == '\n' )
		--yylineno;

	yytext_ptr = yy_bp;
	yy_hold_char = *yy_cp;
	yy_c_buf_p = yy_cp;
	}
#endif  /* --DS */

#ifdef __cplusplus
static int yyinput()
#else
static int input()
#endif
	{
	int c;

	*yy_c_buf_p = yy_hold_char;

	if ( *yy_c_buf_p == YY_END_OF_BUFFER_CHAR )
		{
		/* yy_c_buf_p now points to the character we want to return.
		 * If this occurs *before* the EOB characters, then it's a
		 * valid NUL; if not, then we've hit the end of the buffer.
		 */
		if ( yy_c_buf_p < &yy_current_buffer->yy_ch_buf[yy_n_chars] )
			/* This was really a NUL. */
			*yy_c_buf_p = '\0';

		else
			{ /* need more input */
			yytext_ptr = yy_c_buf_p;
			++yy_c_buf_p;

			switch ( yy_get_next_buffer() )
				{
				case EOB_ACT_END_OF_FILE:
					{
					if ( yywrap() )
						{
						yy_c_buf_p =
						yytext_ptr + YY_MORE_ADJ;
						return EOF;
						}

					if ( ! yy_did_buffer_switch_on_eof )
						YY_NEW_FILE;
#ifdef __cplusplus
					return yyinput();
#else
					return input();
#endif
					}

				case EOB_ACT_CONTINUE_SCAN:
					yy_c_buf_p = yytext_ptr + YY_MORE_ADJ;
					break;

				case EOB_ACT_LAST_MATCH:
#ifdef __cplusplus
					YY_FATAL_ERROR(
					"unexpected last match in yyinput()" );
#else
					YY_FATAL_ERROR(
					"unexpected last match in input()" );
#endif
				}
			}
		}

	c = *(unsigned char *) yy_c_buf_p;	/* cast for 8-bit char's */
	*yy_c_buf_p = '\0';	/* preserve yytext */
	yy_hold_char = *++yy_c_buf_p;

	yy_current_buffer->yy_at_bol = (c == '\n');
	if ( yy_current_buffer->yy_at_bol )
		++yylineno;

	return c;
	}


#ifdef YY_USE_PROTOS
void yyrestart( FILE *input_file )
#else
void yyrestart( input_file )
FILE *input_file;
#endif
	{
	if ( ! yy_current_buffer )
		yy_current_buffer = yy_create_buffer( yyin, YY_BUF_SIZE );

	yy_init_buffer( yy_current_buffer, input_file );
	yy_load_buffer_state();
	}


#ifdef YY_USE_PROTOS
void yy_switch_to_buffer( YY_BUFFER_STATE new_buffer )
#else
void yy_switch_to_buffer( new_buffer )
YY_BUFFER_STATE new_buffer;
#endif
	{
	if ( yy_current_buffer == new_buffer )
		return;

	if ( yy_current_buffer )
		{
		/* Flush out information for old buffer. */
		*yy_c_buf_p = yy_hold_char;
		yy_current_buffer->yy_buf_pos = yy_c_buf_p;
		yy_current_buffer->yy_n_chars = yy_n_chars;
		}

	yy_current_buffer = new_buffer;
	yy_load_buffer_state();

	/* We don't actually know whether we did this switch during
	 * EOF (yywrap()) processing, but the only time this flag
	 * is looked at is after yywrap() is called, so it's safe
	 * to go ahead and always set it.
	 */
	yy_did_buffer_switch_on_eof = 1;
	}


#ifdef YY_USE_PROTOS
void yy_load_buffer_state( void )
#else
void yy_load_buffer_state()
#endif
	{
	yy_n_chars = yy_current_buffer->yy_n_chars;
	yytext_ptr = yy_c_buf_p = yy_current_buffer->yy_buf_pos;
	yyin = yy_current_buffer->yy_input_file;
	yy_hold_char = *yy_c_buf_p;
	}


#ifdef YY_USE_PROTOS
YY_BUFFER_STATE yy_create_buffer( FILE *file, int size )
#else
YY_BUFFER_STATE yy_create_buffer( file, size )
FILE *file;
int size;
#endif
	{
	YY_BUFFER_STATE b;

	b = (YY_BUFFER_STATE) yy_flex_alloc( sizeof( struct yy_buffer_state ) );
	if ( ! b )
		YY_FATAL_ERROR( "out of dynamic memory in yy_create_buffer()" );

	b->yy_buf_size = size;

	/* yy_ch_buf has to be 2 characters longer than the size given because
	 * we need to put in 2 end-of-buffer characters.
	 */
	b->yy_ch_buf = (char *) yy_flex_alloc( b->yy_buf_size + 2 );
	if ( ! b->yy_ch_buf )
		YY_FATAL_ERROR( "out of dynamic memory in yy_create_buffer()" );

	b->yy_is_our_buffer = 1;

	yy_init_buffer( b, file );

	return b;
	}


#ifdef YY_USE_PROTOS
void yy_delete_buffer( YY_BUFFER_STATE b )
#else
void yy_delete_buffer( b )
YY_BUFFER_STATE b;
#endif
	{
	if ( b == yy_current_buffer )
		yy_current_buffer = (YY_BUFFER_STATE) 0;

	if ( b->yy_is_our_buffer )
		yy_flex_free( (void *) b->yy_ch_buf );

	yy_flex_free( (void *) b );
	}


#ifndef YY_ALWAYS_INTERACTIVE
#ifndef YY_NEVER_INTERACTIVE
extern int isatty YY_PROTO(( int ));
#endif
#endif

#ifdef YY_USE_PROTOS
void yy_init_buffer( YY_BUFFER_STATE b, FILE *file )
#else
void yy_init_buffer( b, file )
YY_BUFFER_STATE b;
FILE *file;
#endif


	{
	yy_flush_buffer( b );

	b->yy_input_file = file;
	b->yy_fill_buffer = 1;

#if YY_ALWAYS_INTERACTIVE
	b->yy_is_interactive = 1;
#else
#if YY_NEVER_INTERACTIVE
	b->yy_is_interactive = 0;
#else
	b->yy_is_interactive = file ? (isatty( fileno(file) ) > 0) : 0;
#endif
#endif
	}


#ifdef YY_USE_PROTOS
void yy_flush_buffer( YY_BUFFER_STATE b )
#else
void yy_flush_buffer( b )
YY_BUFFER_STATE b;
#endif

	{
	b->yy_n_chars = 0;

	/* We always need two end-of-buffer characters.  The first causes
	 * a transition to the end-of-buffer state.  The second causes
	 * a jam in that state.
	 */
	b->yy_ch_buf[0] = YY_END_OF_BUFFER_CHAR;
	b->yy_ch_buf[1] = YY_END_OF_BUFFER_CHAR;

	b->yy_buf_pos = &b->yy_ch_buf[0];

	b->yy_at_bol = 1;
	b->yy_buffer_status = YY_BUFFER_NEW;

	if ( b == yy_current_buffer )
		yy_load_buffer_state();
	}


#ifndef YY_NO_SCAN_BUFFER
#ifdef YY_USE_PROTOS
YY_BUFFER_STATE yy_scan_buffer( char *base, yy_size_t size )
#else
YY_BUFFER_STATE yy_scan_buffer( base, size )
char *base;
yy_size_t size;
#endif
	{
	YY_BUFFER_STATE b;

	if ( size < 2 ||
	     base[size-2] != YY_END_OF_BUFFER_CHAR ||
	     base[size-1] != YY_END_OF_BUFFER_CHAR )
		/* They forgot to leave room for the EOB's. */
		return 0;

	b = (YY_BUFFER_STATE) yy_flex_alloc( sizeof( struct yy_buffer_state ) );
	if ( ! b )
		YY_FATAL_ERROR( "out of dynamic memory in yy_scan_buffer()" );

	b->yy_buf_size = size - 2;	/* "- 2" to take care of EOB's */
	b->yy_buf_pos = b->yy_ch_buf = base;
	b->yy_is_our_buffer = 0;
	b->yy_input_file = 0;
	b->yy_n_chars = b->yy_buf_size;
	b->yy_is_interactive = 0;
	b->yy_at_bol = 1;
	b->yy_fill_buffer = 0;
	b->yy_buffer_status = YY_BUFFER_NEW;

	yy_switch_to_buffer( b );

	return b;
	}
#endif


#ifndef YY_NO_SCAN_STRING
#ifdef YY_USE_PROTOS
YY_BUFFER_STATE yy_scan_string( const char *str )
#else
YY_BUFFER_STATE yy_scan_string( str )
const char *str;
#endif
	{
	int len;
	for ( len = 0; str[len]; ++len )
		;

	return yy_scan_bytes( str, len );
	}
#endif


#ifndef YY_NO_SCAN_BYTES
#ifdef YY_USE_PROTOS
YY_BUFFER_STATE yy_scan_bytes( const char *bytes, int len )
#else
YY_BUFFER_STATE yy_scan_bytes( bytes, len )
const char *bytes;
int len;
#endif
	{
	YY_BUFFER_STATE b;
	char *buf;
	yy_size_t n;
	int i;

	/* Get memory for full buffer, including space for trailing EOB's. */
	n = len + 2;
	buf = (char *) yy_flex_alloc( n );
	if ( ! buf )
		YY_FATAL_ERROR( "out of dynamic memory in yy_scan_bytes()" );

	for ( i = 0; i < len; ++i )
		buf[i] = bytes[i];

	buf[len] = buf[len+1] = YY_END_OF_BUFFER_CHAR;

	b = yy_scan_buffer( buf, n );
	if ( ! b )
		YY_FATAL_ERROR( "bad buffer in yy_scan_bytes()" );

	/* It's okay to grow etc. this buffer, and we should throw it
	 * away when we're done.
	 */
	b->yy_is_our_buffer = 1;

	return b;
	}
#endif


#ifndef YY_NO_PUSH_STATE
#ifdef YY_USE_PROTOS
static void yy_push_state( int new_state )
#else
static void yy_push_state( new_state )
int new_state;
#endif
	{
	if ( yy_start_stack_ptr >= yy_start_stack_depth )
		{
		yy_size_t new_size;

		yy_start_stack_depth += YY_START_STACK_INCR;
		new_size = yy_start_stack_depth * sizeof( int );

		if ( ! yy_start_stack )
			yy_start_stack = (int *) yy_flex_alloc( new_size );

		else
			yy_start_stack = (int *) yy_flex_realloc(
					(void *) yy_start_stack, new_size );

		if ( ! yy_start_stack )
			YY_FATAL_ERROR(
			"out of memory expanding start-condition stack" );
		}

	yy_start_stack[yy_start_stack_ptr++] = YY_START;

	BEGIN(new_state);
	}
#endif


#ifndef YY_NO_POP_STATE
static void yy_pop_state()
	{
	if ( --yy_start_stack_ptr < 0 )
		YY_FATAL_ERROR( "start-condition stack underflow" );

	BEGIN(yy_start_stack[yy_start_stack_ptr]);
	}
#endif


#ifndef YY_NO_TOP_STATE
static int yy_top_state()
	{
	return yy_start_stack[yy_start_stack_ptr - 1];
	}
#endif

#ifndef YY_EXIT_FAILURE
#define YY_EXIT_FAILURE 2
#endif

#ifdef YY_USE_PROTOS
static void yy_fatal_error( const char msg[] )
#else
static void yy_fatal_error( msg )
char msg[];
#endif
	{
	(void) fprintf( stderr, "%s\n", msg );
	exit( YY_EXIT_FAILURE );
	}



/* Redefine yyless() so it works in section 3 code. */

#undef yyless
#define yyless(n) \
	do \
		{ \
		/* Undo effects of setting up yytext. */ \
		yytext[yyleng] = yy_hold_char; \
		yy_c_buf_p = yytext + n - YY_MORE_ADJ; \
		yy_hold_char = *yy_c_buf_p; \
		*yy_c_buf_p = '\0'; \
		yyleng = n; \
		} \
	while ( 0 )


/* Internal utility routines. */

#ifndef yytext_ptr
#ifdef YY_USE_PROTOS
static void yy_flex_strncpy( char *s1, const char *s2, int n )
#else
static void yy_flex_strncpy( s1, s2, n )
char *s1;
const char *s2;
int n;
#endif
	{
	register int i;
	for ( i = 0; i < n; ++i )
		s1[i] = s2[i];
	}
#endif


#ifdef YY_USE_PROTOS
static void *yy_flex_alloc( yy_size_t size )
#else
static void *yy_flex_alloc( size )
yy_size_t size;
#endif
	{
	return (void *) malloc( size );
	}

#if 0  /* --DS */
#ifdef YY_USE_PROTOS
static void *yy_flex_realloc( void *ptr, yy_size_t size )
#else
static void *yy_flex_realloc( ptr, size )
void *ptr;
yy_size_t size;
#endif
	{
	return (void *) realloc( ptr, size );
	}
#endif  /* --DS */

#ifdef YY_USE_PROTOS
static void yy_flex_free( void *ptr )
#else
static void yy_flex_free( ptr )
void *ptr;
#endif
	{
	free( ptr );
	}

#if YY_MAIN
int main()
	{
	yylex();
	return 0;
	}
#endif
/* #line 68 "pp_lexer.fl" --DS */


/************************ exported functions ******************************/

PPLexTable *pp_lexer_open(FILE *f)
{
  PPLexTable *lt;
  if (f==NULL) error("pp_lexer_open: passed a NULL file pointer");
  yyin = f;            /* redirect lex to look at the specified file */
  lt = (PPLexTable*) xalloc (sizeof(PPLexTable));
  setup(lt);
  clt = lt;    /* set lt to be the current table, which yylex will fill in */
  yylex();  
  clt = NULL; 
  lt->idx_of_active_label=-1;
  return lt;
}

void pp_lexer_close(PPLexTable *lt)
{
  int i;
  pp_label_node *node,*next;
  for (i=0; i<PP_LEXER_MAX_LABELS; i++)
    {
      /* free the linked list */
      node = lt->nodes_of_label[i];
      while (node)
	{
	  next = node->next;
	  xfree(node, sizeof(pp_label_node));
	  node=next;
	}
    }
  string_set_delete(lt->string_set);
  xfree(lt, sizeof(PPLexTable));
}

int pp_lexer_set_label(PPLexTable *lt, const char *label) 
{
  /* set lexer state to first node of this label */
  lt->idx_of_active_label = get_index_of_label(lt, label);
  if (lt->idx_of_active_label==-1) return 0;    /* label not found */
  lt->current_node_of_active_label=lt->nodes_of_label[lt->idx_of_active_label];
  return 1;
}

int pp_lexer_count_tokens_of_label(PPLexTable *lt) 
{
  /* counts all tokens, even the commas */
  int n;
  pp_label_node *p;
  if (lt->idx_of_active_label==-1) 
	error("pp_lexer: current label is invalid");
  for (n=0, p=lt->nodes_of_label[lt->idx_of_active_label]; p;p=p->next, n++);
  return n;
}


char *pp_lexer_get_next_token_of_label(PPLexTable *lt) 
{
  /* retrieves next token of set label, or NULL if list exhausted */
  static char *p;
  if (lt->current_node_of_active_label==NULL) return NULL;
  p = lt->current_node_of_active_label->str;
  lt->current_node_of_active_label=lt->current_node_of_active_label->next;
  return p;
}

int pp_lexer_count_commas_of_label(PPLexTable *lt) 
{
  int n;
  pp_label_node *p;
  if (lt->idx_of_active_label==-1) error("pp_lexer: current label is invalid");
  for (n=0,p=lt->nodes_of_label[lt->idx_of_active_label];p!=NULL;p=p->next)
    if (!strcmp(p->str, ",")) n++;
  return n;
}

char **pp_lexer_get_next_group_of_tokens_of_label(PPLexTable *lt,int *n_tokens)
{ 
  /* all tokens until next comma, null-terminated */
  int n;
  pp_label_node *p;
  static char **tokens=0;  
  static int extents=0;

  p=lt->current_node_of_active_label;	   
  for (n=0; p!=NULL && strcmp(p->str,","); n++, p=p->next);
  if (n>extents) {
     extents=n;
     free(tokens);
     tokens = (char **) malloc (extents * sizeof(char*));
  }   

  p=lt->current_node_of_active_label;	   
  for (n=0; p!=NULL && strcmp(p->str,","); n++, p=p->next)
       tokens[n] = string_set_add(p->str, lt->string_set);
  
  /* advance "current node of label" state */
  lt->current_node_of_active_label = p;
  if (p!=NULL) lt->current_node_of_active_label=p->next;
  
  *n_tokens = n;
  return tokens;
}


int yywrap()
{
  /* must return 1 for end of input, 0 otherwise */
  return 1;
}

/********************** non-exported functions ************************/

static void setup(PPLexTable *lt) 
{
  int i;
  for (i=0; i<PP_LEXER_MAX_LABELS; i++)
    {
      lt->nodes_of_label[i]     = NULL;
      lt->last_node_of_label[i] = NULL;
      lt->labels[i]=NULL;
    }
  lt->string_set = string_set_create();
}

static void set_label(PPLexTable *lt, const char *label) 
{
  int i;
  char *c;
  char *label_sans_colon;

  /* check for and then slice off the trailing colon */
  label_sans_colon = strdup(label);
  c=&(label_sans_colon[strlen(label_sans_colon)-1]);
  if (*c != ':') error("Label %s must end with :", label);
  *c = 0;

  /* have we seen this label already? If so, abort */
  for (i=0;lt->labels[i]!=NULL && strcmp(lt->labels[i],label_sans_colon);i++);
  if (lt->labels[i]!=NULL) 
    error("pp_lexer: label %s multiply defined!", label_sans_colon);

  /* new label. Store it */
  if (i == PP_LEXER_MAX_LABELS-1) 
    error("pp_lexer: too many labels. Raise PP_LEXER_MAX_LABELS");
  lt->labels[i] = string_set_add(label_sans_colon, lt->string_set);
  lt->idx_of_active_label = i;

  free(label_sans_colon);
}


static void add_string_to_label(PPLexTable *lt, char *str) 
{
  /* add the single string str to the set of strings associated with label */
  pp_label_node *new_node;

  if (lt->idx_of_active_label==-1)
    error("pp_lexer: invalid syntax (line %i)",yylineno);

  /* make sure string is legal */
  check_string(str);

  /* create a new node in (as yet to be determined) linked list of strings */
  new_node = (pp_label_node *) xalloc (sizeof(pp_label_node));
  new_node->str = string_set_add(str, lt->string_set);
  new_node->next = NULL;
  
  /* stick newly-created node at the *end* of the appropriate linked list */
  if (lt->last_node_of_label[lt->idx_of_active_label]==NULL)
    {
      /* first entry on linked list */
      lt->nodes_of_label[lt->idx_of_active_label]     = new_node;      
      lt->last_node_of_label[lt->idx_of_active_label] = new_node;
    }
  else
    {
      /* non-first entry on linked list */
      lt->last_node_of_label[lt->idx_of_active_label]->next = new_node;
      lt->last_node_of_label[lt->idx_of_active_label]       = new_node;
    }
}

static void add_set_of_strings_to_label(PPLexTable *lt,const char *label_of_set) 
{
  /* add the set of strings, defined earlier by label_of_set, to the set of 
     strings associated with the current label */
  pp_label_node *p;
  int idx_of_label_of_set;
  if (lt->idx_of_active_label==-1)
    error("pp_lexer: invalid syntax (line %i)",yylineno);
  if ((idx_of_label_of_set = get_index_of_label(lt, label_of_set))==-1)
    error("pp_lexer: label %s must be defined before it's referred to (line %i)"
	  ,label_of_set, yylineno);
  for (p=lt->nodes_of_label[idx_of_label_of_set]; p!=NULL; p=p->next)
    add_string_to_label(lt, p->str);
}

#if 0
/* --DS */
static void show_bindings(PPLexTable *lt)
{
  /* Diagnostic. Show contents of knowledge file, as arranged internally */
  int i,j;
  char *la;
  pp_label_node *p;
  printf("The symbol table's contents: \n");
  for (i=0; (la=lt->labels[i])!=NULL; i++)
    {
      printf("\n\n%s\n", la);
      for (j=0; j<strlen(la); j++)
	printf("=");
      printf("\n");
      for (p=lt->nodes_of_label[i]; p!=NULL; p=p->next)
	printf(" %s ", p->str);
    }
  printf("\n");
}
#endif

static int get_index_of_label(PPLexTable *lt, const char *label) 
{
  int i;
  for (i=0; lt->labels[i]!=NULL; i++) 
    if (!strcmp(lt->labels[i], label)) return i;
  return -1;
}
    
static void check_string(const char *str)
{
   if (strlen(str)>1 && strchr(str, ',')!=NULL)
      error("pp_lexer: string %s contains a comma, which is a no-no.",str);
}


/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

/***********************************************************************
pp_linkset.c
maintains sets of pointers to link names 
Similar to string-set, except that the comparison and hashing functions are
tailored for links. More importantly, all we store here is pointers. It's up
to the caller to ensure that the pointers always point to something useful.
**********************************************************************/

#include <memory.h>

#include "link-includes.h"

#define LINKSET_SPARSENESS 2
#define LINKSET_SEED_VALUE 37

/* forward declarations of non-exported functions */
static void initialize(pp_linkset *ls, int size);
static pp_linkset_node *add_internal(pp_linkset *ls, char *str);
static int compute_hash(pp_linkset *ls, const char *str);
 
extern int post_process_match(char *s, char *t);


pp_linkset *pp_linkset_open(int size)
{
  pp_linkset *ls;
  if (size==0) return NULL;
  ls = (pp_linkset *) xalloc (sizeof(pp_linkset));
  initialize(ls, size);
  return ls;
}

void pp_linkset_close(pp_linkset *ls)
{
  if (ls==NULL) return; 
  pp_linkset_clear(ls);      /* free memory taken by linked lists */
  xfree((void*) ls->hash_table, ls->hash_table_size*sizeof(pp_linkset_node*));
  xfree((void*) ls, sizeof(pp_linkset));
}

void pp_linkset_clear(pp_linkset *ls)
{
  /* clear dangling linked lists, but retain hash table itself */
  int i;
  pp_linkset_node *p;
  if (ls==NULL) return; 
  for (i=0; i<ls->hash_table_size; i++) {
    p=ls->hash_table[i];
    while (p) {
      pp_linkset_node *q = p;
      p=p->next;
      xfree((void*) q, sizeof(pp_linkset_node));
    }
  }
  clear_hash_table(ls);
  ls->population=0;
}

int pp_linkset_add(pp_linkset *ls, char *str)
{
  /* returns 0 if already there, 1 if new. Stores only the pointer */
  if (ls==NULL) error("pp_linkset internal error: Trying to add to a null set");
  if (add_internal(ls, str) == NULL) return 0;
  ls->population++;
  return 1;
}

int pp_linkset_match(pp_linkset *ls, char *str) 
{
  /* Set query. Returns 1 if str pp-matches something in the set, 0 otherwise */
  int hashval;
  pp_linkset_node *p;
  if (ls==NULL) return 0; 
  hashval = compute_hash(ls, str);
  p = ls->hash_table[hashval];
  while(p!=0) {
    if (post_process_match(p->str,str)) return 1;
    p=p->next;
  }
  return 0;
}

int pp_linkset_match_bw(pp_linkset *ls, char *str) 
{
  int hashval;
  pp_linkset_node *p;
  if (ls==NULL) return 0; 
  hashval = compute_hash(ls, str);
  p = ls->hash_table[hashval];
  while(p!=0) {
    if (post_process_match(str,p->str)) return 1;
    p=p->next;
  }
  return 0;
}

int pp_linkset_population(pp_linkset *ls) {
  return (ls==NULL)? 0 : ls->population; 
}

/*********************** non-exported functions ************************/

static void clear_hash_table(pp_linkset *ls)
{
  memset(ls->hash_table,0,ls->hash_table_size*sizeof(pp_linkset_node *));
}

static void initialize(pp_linkset *ls, int size) 
{
  ls->hash_table_size = size*LINKSET_SPARSENESS;
  ls->population = 0;
  ls->hash_table = 
    (pp_linkset_node**) xalloc (ls->hash_table_size*sizeof(pp_linkset_node *));
  clear_hash_table(ls);
}

static pp_linkset_node *add_internal(pp_linkset *ls, char *str)
{
  pp_linkset_node *p, *n;
  int hashval;
  
  /* look for str (exactly) in linkset */
  hashval = compute_hash(ls, str);
  for (p=ls->hash_table[hashval]; p!=0; p=p->next)
    if (!strcmp(p->str,str)) return NULL;  /* already present */
  
  /* create a new node for u; stick it at head of linked list */
  n = (pp_linkset_node *) xalloc (sizeof(pp_linkset_node));      
  n->next = ls->hash_table[hashval];
  n->str = str;
  ls->hash_table[hashval] = n;
  return n;
}

static int compute_hash(pp_linkset *ls, const char *str)
 {
   /* hash is computed from capitalized prefix only */
  int i, hashval;
  hashval=LINKSET_SEED_VALUE;
  for (i=0; isupper((int)str[i]); i++)
    hashval = str[i] + 31*hashval;
  hashval = hashval % ls->hash_table_size;
  if (hashval<0) hashval*=-1;
  return hashval;
}

/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

static int null_links;

void print_statistics() {
}

void free_deletable(Sentence sent) {
    int w;
    if (sent->deletable != NULL) {
	for (w = -1; w<sent->length; w++) {
	    xfree((char *)sent->deletable[w],sizeof(char)*(sent->length+1));
	}
	sent->deletable--;
	xfree((char *) sent->deletable, (sent->length+1)*sizeof(char *));
	sent->deletable = NULL;
    }
}

void build_deletable(Sentence sent, int has_conjunction) {
/* Initialize the array deletable[i][j] to indicate if the words           */
/* i+1...j-1 could be non existant in one of the multiple linkages.  This  */
/* array is used in conjunction_prune and power_prune.  Regions of length  */
/* 0 are always deletable.  A region of length two with a conjunction at   */
/* one end is always deletable.  Another observation is that for the       */
/* comma to form the right end of a deletable region, it must be the case  */
/* that there is a conjunction to the right of the comma.  Also, when      */
/* considering deletable regions with a comma on their left sides, there   */
/* must be a conjunction inside the region to be deleted. Finally, the     */
/* words "either", "neither", "both", "not" and "not only" are all         */
/* deletable.                                                              */
   
    int i,j,k;

    free_deletable(sent);
    assert(sent->length < MAX_SENTENCE, "sent->length too big");

    sent->deletable = (char **) xalloc((sent->length+1)*sizeof(char *));
    sent->deletable++;  /* we need to be able to access the [-1] position in this array */

    for (i = -1; i<sent->length; i++) {
	sent->deletable[i] = (char *) xalloc(sent->length+1);  
	/* the +1 is to allow us to have the info for the last word
	   read the comment above */
	for (j=0; j<= sent->length; j++) {
	    if (j == i+1) {
		sent->deletable[i][j] = TRUE;
	    } else if (null_links) {
		sent->deletable[i][j] = TRUE;
	    } else if (!has_conjunction) {
		sent->deletable[i][j] = FALSE;
	    } else if ((j>i+2)&&(sent->is_conjunction[i+1] || 
				 sent->is_conjunction[j-1] ||
                                (strcmp(",",sent->word[i+1].string)==0 && 
				 conj_in_range(sent, i+2,j-1)) ||
 	                        (strcmp(",",sent->word[j-1].string)==0 && 
				 conj_in_range(sent, j,sent->length-1)))){
		sent->deletable[i][j] = TRUE;
	    } else if (j > i) {
		for (k=i+1; k<j; k++) {
		    if ((strcmp("either", sent->word[k].string) == 0) ||
			(strcmp("neither", sent->word[k].string) == 0) ||
			(strcmp("both", sent->word[k].string) == 0) ||
			(strcmp("not", sent->word[k].string) == 0)) continue;
		    if ((strcmp("only", sent->word[k].string)==0) && (k > i+1) &&
                                   (strcmp("not", sent->word[k-1].string)==0)) continue;
		    break;
		}
		sent->deletable[i][j] = (k==j);
	    } else {
		sent->deletable[i][j] = FALSE;
	    }
	}
    }
}

void free_effective_dist(Sentence sent) {
    int w;
    if (sent->effective_dist != NULL) {
	for (w=0; w<sent->length; w++) {
	    xfree((char *)sent->effective_dist[w],sizeof(char)*(sent->length+1));
	}
	xfree((char *) sent->effective_dist, sizeof(char *)*(sent->length));
	sent->effective_dist = NULL;
    }
}

void build_effective_dist(Sentence sent, int has_conjunction) {
  /*
    The "effective distance" between two words is the actual distance minus
    the largest deletable region strictly between the two words.  If the
    effective distance between two words is greater than a connector's max
    link length, then that connector cannot be satisfied by linking these
    two words.

    [Note: The effective distance is not monotically increasing as you move
    away from a word.]

    This function creates effective_dist[][].  It assumes that deleteble[][]
    has already been computed.

    Dynamic programming is used to compute this.  The order used is smallest
    region to largest.

    Just as deletable[i][j] is constructed for j=N_words (which is one
    off the end of the sentence) we do that for effective_dist[][].
  */

    int i, j, diff;

    free_effective_dist(sent);
    sent->effective_dist = (char **) xalloc((sent->length)*sizeof(char *));

    for (i=0; i<sent->length; i++) {
	sent->effective_dist[i] = (char *) xalloc(sent->length+1);
    }
    for (i=0; i<sent->length; i++) {  
	/* Fill in the silly part */
	for (j=0; j<=i; j++) {
	    sent->effective_dist[i][j] = j-i;
	}
    }

    /* what is the rationale for ignoring the effective_dist 
       if null links are allowed? */
    if (null_links) {  
	for (i=0; i<sent->length; i++) {  
	    for (j=0; j<=sent->length; j++) {
		sent->effective_dist[i][j] = j-i;
	    }
	}
    } 
    else {
	for (diff = 1; diff < sent->length; diff++) {
	    for (i=0; i+diff <= sent->length; i++) {
		j = i+diff;
		if (sent->deletable[i][j]) {  /* note that deletable[x][x+1] is TRUE */
		    sent->effective_dist[i][j] = 1;		    
		} else {
		    sent->effective_dist[i][j] = 1 + MIN(sent->effective_dist[i][j-1],sent->effective_dist[i+1][j]);
		}
	    }
	}

	/* now when you link to a conjunction, your effective length is 1 */

	for (i=0; i<sent->length; i++) {  
	    for (j=i+1; j<sent->length; j++) {
		if (sent->is_conjunction[i] || 
		    sent->is_conjunction[j]) sent->effective_dist[i][j] = 1;
	    }
	}
    }

    /* sent->effective_dist[i][i] should be 0 */

    /*      
    for (j=0; j<=sent->length; j++) {
      printf("%4d", j);
    }
    printf("\n");
    for (i=0; i<sent->length; i++) {  
      for (j=0; j<=sent->length; j++) {
	printf("%4d", sent->effective_dist[i][j]);
      }
      printf("\n");
    }
    */
}

void install_fat_connectors(Sentence sent) {
/* Installs all the special fat disjuncts on all of the words of the   */
/* sentence */  
    int i;
    for (i=0; i<sent->length; i++) {
	if (sent->is_conjunction[i]) {
	    sent->word[i].d = catenate_disjuncts(sent->word[i].d,
			       build_AND_disjunct_list(sent, sent->word[i].string));
	} else {
	    sent->word[i].d = catenate_disjuncts(sent->word[i].d,
			       explode_disjunct_list(sent, sent->word[i].d));
	    if (strcmp(sent->word[i].string, ",") == 0) {
		sent->word[i].d = catenate_disjuncts(sent->word[i].d,
					       build_COMMA_disjunct_list(sent));
	    }
	}
    }
}

static void set_connector_list_length_limit(Connector *c, Connector_set *conset, int short_len, Parse_Options opts) {
    for (; c!=NULL; c=c->next) {
        if (parse_options_get_all_short_connectors(opts)) {
	    c->length_limit = short_len;
	}
	else if (conset == NULL || match_in_connector_set(conset, c, '+')) {
	    c->length_limit = UNLIMITED_LEN;	    
	} else {
	    c->length_limit = short_len;
	}
    }
}

static void set_connector_length_limits(Sentence sent, Parse_Options opts) {
    int i;
    int len;
    Disjunct *d;

    len = opts->short_length;
    if (len > UNLIMITED_LEN) len = UNLIMITED_LEN;
    
    for (i=0; i<sent->length; i++) {
	for (d = sent->word[i].d; d != NULL; d = d->next) {
	    set_connector_list_length_limit(d->left, sent->dict->unlimited_connector_set, len, opts);
	    set_connector_list_length_limit(d->right, sent->dict->unlimited_connector_set, len, opts);
	}
    }
}

void free_sentence_expressions(Sentence sent) {
  int i;
  for (i=0; i<sent->length; i++) {
    free_X_nodes(sent->word[i].x);
  }
}

void free_sentence_disjuncts(Sentence sent) {
    int i;

    for (i=0; i<sent->length; ++i) {
	free_disjuncts(sent->word[i].d);
	sent->word[i].d = NULL;
    }
    if (sentence_contains_conjunction(sent)) free_AND_tables(sent);
}


void prepare_to_parse(Sentence sent, Parse_Options opts) {
/* assumes that the sentence expression lists have been generated     */
/* this does all the necessary pruning and building of and            */
/* structures.                                                        */
    int i, has_conjunction;

    build_sentence_disjuncts(sent, opts->disjunct_cost);
    if (verbosity > 2) {
	printf("After expanding expressions into disjuncts:") ;
	print_disjunct_counts(sent);
    }
    print_time(opts, "Built disjuncts");
    
    for (i=0; i<sent->length; i++) {
	sent->word[i].d = eliminate_duplicate_disjuncts(sent->word[i].d);
    }
    print_time(opts, "Eliminated duplicate disjuncts");

    if (verbosity > 2) {
	printf("\nAfter expression pruning and duplicate elimination:\n");
	print_disjunct_counts(sent);
    }

    null_links = (opts->min_null_count > 0);

    has_conjunction = sentence_contains_conjunction(sent);
    set_connector_length_limits(sent, opts);
    build_deletable(sent, has_conjunction);
    build_effective_dist(sent, has_conjunction);  
    /* why do we do these here instead of in
       first_prepare_to_parse() only?  The
       reason is that the deletable region
       depends on if null links are in use.
       with null_links everything is deletable */

    if (!has_conjunction) {
	pp_and_power_prune(sent, RUTHLESS, opts);
    } else {
	pp_and_power_prune(sent, GENTLE, opts);
	/*
	if (verbosity > 2) {
	    printf("\nAfter Gentle power pruning:\n");
	    print_disjunct_counts(sent);
	}
	*/
	/*print_time(opts, "Finished gentle power pruning"); */
	conjunction_prune(sent, opts);
	if (verbosity > 2) {
	    printf("\nAfter conjunction pruning:\n");
	    print_disjunct_counts(sent);
	    print_statistics();
	}
	print_time(opts, "Done conjunction pruning");
	build_conjunction_tables(sent);
	install_fat_connectors(sent);
	install_special_conjunctive_connectors(sent);
	if (verbosity > 2) {
	    printf("After conjunctions, disjuncts counts:\n");
	    print_disjunct_counts(sent);
	}
	set_connector_length_limits(sent, opts);
              /* have to do this again cause of the
	         new fat connectors and disjuncts */

	print_time(opts, "Constructed fat disjuncts");
	
	prune(sent);
	print_time(opts, "Pruned fat disjuncts");
	
	for (i=0; i<sent->length; i++) {
	    sent->word[i].d = eliminate_duplicate_disjuncts(sent->word[i].d);
	}
	if (verbosity > 2) {
	    printf("After pruning and duplicate elimination:\n");
	    print_disjunct_counts(sent);
	}
	print_time(opts, "Eliminated duplicate disjuncts (again)");
	
	if (verbosity > 2) print_AND_statistics(sent);

	power_prune(sent, RUTHLESS, opts);
    }

    /*
    if (verbosity > 2) {
	printf("\nAfter RUTHLESS power-pruning:\n");
	print_disjunct_counts(sent);
    }
    */
    /* print time for power pruning used to be here */
    /* now done in power_prune itself */
    print_time(opts, "Initialized fast matcher and hash table");
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/


#include <stdarg.h>
#include "link-includes.h"

static int center[MAX_SENTENCE];
static int N_words_to_print;  /* version of N_words in this file for printing links */
char * trailer(int mode);
char * header(int mode);

void set_centers(Linkage linkage, int print_word_0) {
    int i, len, tot;
    tot = 0;
    if (print_word_0) i=0; else i=1;
    for (; i<N_words_to_print; i++) {
	len = strlen(linkage->word[i]);
	center[i] = tot + (len/2);
	tot += len+1;
    }
}

/* the following are all for generating postscript */
static int link_heights[MAX_LINKS];
  /* tells the height of the links above the sentence */
static int row_starts[MAX_SENTENCE];
  /* the word beginning each row of the display */
static int N_rows;
  /* the number of rows */

void left_append_string(String * string, char * s, char * t) {
/* prints s then prints the last |t|-|s| characters of t.
   if s is longer than t, it truncates s.
*/
    int i, j, k;
    j = strlen(t);
    k = strlen(s);
    for (i=0; i<j; i++) {
	if (i<k) {
	    append_string(string, "%c", s[i]);
	} else {
	    append_string(string, "%c", t[i]);
	}
    }
}



void print_a_link(String * s, Linkage linkage, int link) {
    Sentence sent = linkage_get_sentence(linkage);
    Dictionary dict = sent->dict;
    int l, r;
    char *label, *llabel, *rlabel;
    
    l      = linkage_get_link_lword(linkage, link);
    r      = linkage_get_link_rword(linkage, link);
    label  = linkage_get_link_label(linkage, link);
    llabel = linkage_get_link_llabel(linkage, link);
    rlabel = linkage_get_link_rlabel(linkage, link);

    if ((l == 0) && dict->left_wall_defined) {
	left_append_string(s, LEFT_WALL_DISPLAY,"               ");
    } else if ((l == (linkage_get_num_words(linkage)-1)) && dict->right_wall_defined) {
	left_append_string(s, RIGHT_WALL_DISPLAY,"               ");	
    } else {
	left_append_string(s, linkage_get_word(linkage, l), "               ");
    }
    left_append_string(s, llabel, "     ");
    append_string(s, "   <---"); 
    left_append_string(s, label, "-----");
    append_string(s, "->  "); 
    left_append_string(s, rlabel, "     ");
    append_string(s, "     %s\n", linkage_get_word(linkage, r));
}


char * linkage_print_links_and_domains(Linkage linkage) {
    /* To the left of each link, print the sequence of domains it is in. */
    /* Printing a domain means printing its type                         */
    /* Takes info from pp_link_array and pp and chosen_words.            */    
    int link, longest, j;
    int N_links = linkage_get_num_links(linkage);
    String * s = String_create();
    char * links_string;
    char ** dname;

    longest = 0;
    for (link=0; link<N_links; link++) {
	if (linkage_get_link_lword(linkage, link) == -1) continue;
	if (linkage_get_link_num_domains(linkage, link) > longest) 
	    longest = linkage_get_link_num_domains(linkage, link);
    }
    for (link=0; link<N_links; link++) {
	if (linkage_get_link_lword(linkage, link) == -1) continue;
	dname = linkage_get_link_domain_names(linkage, link);
	for (j=0; j<linkage_get_link_num_domains(linkage, link); ++j) {
	    append_string(s, " (%s)", dname[j]);
	}
	for (;j<longest; j++) {
	    append_string(s, "    ");
	}
	append_string(s, "   ");
	print_a_link(s, linkage, link);
    }
    append_string(s, "\n");
    if (linkage_get_violation_name(linkage) != NULL) {
	append_string(s, "P.P. violations:\n");
	append_string(s, "        %s\n\n", linkage_get_violation_name(linkage));
    }
    
    links_string = exalloc(strlen(s->p)+1);
    strcpy(links_string, s->p);
    exfree(s->p, sizeof(char)*s->allocated);
    exfree(s, sizeof(String));

    return links_string; 
}

char * build_linkage_postscript_string(Linkage linkage) {
    int link, i,j;
    int d;
    int print_word_0 = 0, print_word_N = 0, N_wall_connectors, suppressor_used;
    Sublinkage *sublinkage=&(linkage->sublinkage[linkage->current]);
    int N_links = sublinkage->num_links;
    Link *ppla = sublinkage->link;
    String  * string;
    char * ps_string;
    Dictionary dict = linkage->sent->dict;
    Parse_Options opts = linkage->opts;

    string = String_create();

    N_wall_connectors = 0;
    if (dict->left_wall_defined) {
	suppressor_used = FALSE;
	if (!opts->display_walls) 
	    for (j=0; j<N_links; j++) {
		if (ppla[j]->l == 0) {
		    if (ppla[j]->r == linkage->num_words-1) continue;
		    N_wall_connectors ++;
		    if (strcmp(ppla[j]->lc->string, LEFT_WALL_SUPPRESS)==0) {
			suppressor_used = TRUE;
		    }
		}
	    }
	print_word_0 = (((!suppressor_used) && (N_wall_connectors != 0)) 
			|| (N_wall_connectors > 1) || opts->display_walls);
    } else {
	print_word_0 = TRUE;
    }

    N_wall_connectors = 0;
    if (dict->right_wall_defined) {
	suppressor_used = FALSE;
	for (j=0; j<N_links; j++) {
	    if (ppla[j]->r == linkage->num_words-1) {
		N_wall_connectors ++;
		if (strcmp(ppla[j]->lc->string, RIGHT_WALL_SUPPRESS)==0){
		    suppressor_used = TRUE;
		}
	    }
	}
	print_word_N = (((!suppressor_used) && (N_wall_connectors != 0)) 
			|| (N_wall_connectors > 1) || opts->display_walls);
    } 
    else {
	print_word_N = TRUE;
    }


    if (print_word_0) d=0; else d=1;

    i = 0;
    N_words_to_print = linkage->num_words;
    if (!print_word_N) N_words_to_print--;

    append_string(string, "[");
    for (j=d; j<N_words_to_print; j++) {
	if ((i%10 == 0) && (i>0)) append_string(string, "\n");
	i++;
	append_string(string, "(%s)", linkage->word[j]);
    }
    append_string(string,"]");
    append_string(string,"\n");

    append_string(string,"[");
    j = 0;
    for (link=0; link<N_links; link++) {
	if (!print_word_0 && (ppla[link]->l == 0)) continue;
	if (!print_word_N && (ppla[link]->r == linkage->num_words-1)) continue;
	if (ppla[link]->l == -1) continue;
	if ((j%7 == 0) && (j>0)) append_string(string,"\n");
	j++;
	append_string(string,"[%d %d %d",
		ppla[link]->l-d, ppla[link]->r-d, 
		link_heights[link]);
	if (ppla[link]->lc->label < 0) {
	    append_string(string," (%s)]", ppla[link]->name);
	} else {
	    append_string(string," ()]");
	}
    }
    append_string(string,"]");
    append_string(string,"\n");
    append_string(string,"[");
    for (j=0; j<N_rows; j++ ){
	if (j>0) append_string(string, " %d", row_starts[j]);
	else append_string(string,"%d", row_starts[j]);
    }
    append_string(string,"]\n");

    ps_string = exalloc(strlen(string->p)+1);
    strcpy(ps_string, string->p);
    exfree(string->p, sizeof(char)*string->allocated);
    exfree(string, sizeof(String));

    return ps_string; 
}

void compute_chosen_words(Sentence sent, Linkage linkage) {
    /*
       This takes the current chosen_disjuncts array and uses it to
       compute the chosen_words array.  "I.xx" suffixes are eliminated.
       */
    int i, l;
    char * s, *t, *u;
    Parse_info * pi = sent->parse_info;
    char * chosen_words[MAX_SENTENCE];
    Parse_Options opts = linkage->opts;

    for (i=0; i<sent->length; i++) {   /* get rid of those ugly ".Ixx" */
	chosen_words[i] = sent->word[i].string;
	if (pi->chosen_disjuncts[i] == NULL) {  
	    /* no disjunct is used on this word because of null-links */
	    t = chosen_words[i];
	    l = strlen(t) + 2;
	    s = (char *) xalloc(l+1);
	    sprintf(s, "[%s]", t);
	    t = string_set_add(s, sent->string_set);
	    xfree(s, l+1);
	    chosen_words[i] = t;
	} else if (opts->display_word_subscripts) {
	    t = pi->chosen_disjuncts[i]->string;	
	    if (is_idiom_word(t)) {
		l = strlen(t);
		s = (char *) xalloc(l+1);
		strcpy(s,t);
		for (u=s; *u != '.'; u++)
		  ;
		*u = '\0';
		t = string_set_add(s, sent->string_set);
		xfree(s, l+1);
		chosen_words[i] = t;
	    } else {
		chosen_words[i] = t;
	    }
	}
    }
    if (sent->dict->left_wall_defined) {
	chosen_words[0] = LEFT_WALL_DISPLAY;
    }
    if (sent->dict->right_wall_defined) {
	chosen_words[sent->length-1] = RIGHT_WALL_DISPLAY;
    }
    for (i=0; i<linkage->num_words; ++i) {
	linkage->word[i] = (char *) exalloc(strlen(chosen_words[i])+1);
	strcpy(linkage->word[i], chosen_words[i]);
    }
}


#define MAX_HEIGHT 30

static char picture[MAX_HEIGHT][MAX_LINE];
static char xpicture[MAX_HEIGHT][MAX_LINE];

/* String allocated with exalloc.  Needs to be freed with exfree */
char * linkage_print_diagram(Linkage linkage) {
    int i, j, k, cl, cr, row, top_row, width, flag;
    char *t, *s;
    int print_word_0 = 0, print_word_N = 0, N_wall_connectors, suppressor_used;
    char connector[MAX_TOKEN_LENGTH];
    int line_len, link_length;
    Sublinkage *sublinkage=&(linkage->sublinkage[linkage->current]);
    int N_links = sublinkage->num_links;
    Link *ppla = sublinkage->link;
    String * string;
    char * gr_string;
    Dictionary dict = linkage->sent->dict;
    Parse_Options opts = linkage->opts;
    int x_screen_width = parse_options_get_screen_width(opts);

    string = String_create();

    N_wall_connectors = 0;
    if (dict->left_wall_defined) {
	suppressor_used = FALSE;
	if (!opts->display_walls) 
	    for (j=0; j<N_links; j++) {
		if ((ppla[j]->l == 0)) {
		    if (ppla[j]->r == linkage->num_words-1) continue;
		    N_wall_connectors ++;
		    if (strcmp(ppla[j]->lc->string, LEFT_WALL_SUPPRESS)==0){
			suppressor_used = TRUE;
		    }
		}
	    }
	print_word_0 = (((!suppressor_used) && (N_wall_connectors != 0)) 
			|| (N_wall_connectors > 1) || opts->display_walls);
    } 
    else {
	print_word_0 = TRUE;
    }

    N_wall_connectors = 0;
    if (dict->right_wall_defined) {
	suppressor_used = FALSE;
	for (j=0; j<N_links; j++) {
	    if (ppla[j]->r == linkage->num_words-1) {
		N_wall_connectors ++;
		if (strcmp(ppla[j]->lc->string, RIGHT_WALL_SUPPRESS)==0){
		    suppressor_used = TRUE;
		}
	    }
	}
	print_word_N = (((!suppressor_used) && (N_wall_connectors != 0)) 
			|| (N_wall_connectors > 1) || opts->display_walls);
    } 
    else {
	print_word_N = TRUE;
    }

    N_words_to_print = linkage->num_words;
    if (!print_word_N) N_words_to_print--;
    
    set_centers(linkage, print_word_0);
    line_len = center[N_words_to_print-1]+1;
    
    for (k=0; k<MAX_HEIGHT; k++) {
	for (j=0; j<line_len; j++) picture[k][j] = ' ';
	picture[k][line_len] = '\0';
    }
    top_row = 0;
    
    for (link_length = 1; link_length < N_words_to_print; link_length++) {
	for (j=0; j<N_links; j++) {
	    if (ppla[j]->l == -1) continue;
	    if ((ppla[j]->r - ppla[j]->l) != link_length)
	      continue;
	    if (!print_word_0 && (ppla[j]->l == 0)) continue;
	    /* gets rid of the irrelevant link to the left wall */
	    if (!print_word_N && (ppla[j]->r == linkage->num_words-1)) continue;	    
	    /* gets rid of the irrelevant link to the right wall */

	    /* put it into the lowest position */
	    cl = center[ppla[j]->l];
	    cr = center[ppla[j]->r];
	    for (row=0; row < MAX_HEIGHT; row++) {
		for (k=cl+1; k<cr; k++) {
		    if (picture[row][k] != ' ') break;
		}
		if (k == cr) break;
	    }
	    /* we know it fits, so put it in this row */

	    link_heights[j] = row;
	    
	    if (2*row+2 > MAX_HEIGHT-1) {
		append_string(string, "The diagram is too high.\n");
		gr_string = exalloc(strlen(string->p)+1);
		strcpy(gr_string, string->p);
		exfree(string->p, sizeof(char)*string->allocated);
		exfree(string, sizeof(String));
		return gr_string; 
	    }
	    if (row > top_row) top_row = row;
	    
	    picture[row][cl] = '+';
	    picture[row][cr] = '+';
	    for (k=cl+1; k<cr; k++) {
		picture[row][k] = '-';
	    }
	    s = ppla[j]->name;
	    
	    if (opts->display_link_subscripts) {
	      if (!isalpha((int)*s))
		s = "";
	    } else {
	      if (!isupper((int)*s)) {
		s = "";   /* Don't print fat link connector name */
	      }
	    }
	    strncpy(connector, s, MAX_TOKEN_LENGTH-1);
	    connector[MAX_TOKEN_LENGTH-1] = '\0';
	    k=0;
	    if (opts->display_link_subscripts)
	      k = strlen(connector);
	    else
	      for (t=connector; isupper((int)*t); t++) k++; /* uppercase len of conn*/
	    if ((cl+cr-k)/2 + 1 <= cl) {
		t = picture[row] + cl + 1;
	    } else {
		t = picture[row] + (cl+cr-k)/2 + 1;
	    }
	    s = connector;
	    if (opts->display_link_subscripts)
	      while((*s != '\0') && (*t == '-')) *t++ = *s++; 
	    else
	      while(isupper((int)*s) && (*t == '-')) *t++ = *s++; 

	    /* now put in the | below this one, where needed */
	    for (k=0; k<row; k++) {
		if (picture[k][cl] == ' ') {
		    picture[k][cl] = '|';
		}
		if (picture[k][cr] == ' ') {
		    picture[k][cr] = '|';
		}
	    }
	}
    }
    
    /* we have the link picture, now put in the words and extra "|"s */
    
    s = xpicture[0];
    if (print_word_0) k = 0; else k = 1;
    for (; k<N_words_to_print; k++) {
	t = linkage->word[k];
	i=0;
	while(*t != '\0') {
	    *s++ = *t++;
	    i++;
	}
	*s++ = ' ';
    }
    *s = '\0';
    
    if (opts->display_short) {
	for (k=0; picture[0][k] != '\0'; k++) {
	    if ((picture[0][k] == '+') || (picture[0][k] == '|')) {
		xpicture[1][k] = '|';
	    } else {
		xpicture[1][k] = ' ';
	    }
	}
	xpicture[1][k] = '\0';
	for (row=0; row <= top_row; row++) {
	    strcpy(xpicture[row+2],picture[row]);
	}
	top_row = top_row+2;
    } else {
	for (row=0; row <= top_row; row++) {
	    strcpy(xpicture[2*row+2],picture[row]);
	    for (k=0; picture[row][k] != '\0'; k++) {
		if ((picture[row][k] == '+') || (picture[row][k] == '|')) {
		    xpicture[2*row+1][k] = '|';
		} else {
		    xpicture[2*row+1][k] = ' ';
		}
	    }
	    xpicture[2*row+1][k] = '\0';
	}
	top_row = 2*top_row + 2;
    }
    
    /* we've built the picture, now print it out */
    
    if (print_word_0) i = 0; else i = 1;
    k = 0;
    N_rows = 0;
    row_starts[N_rows] = 0;
    N_rows++;
    while(i < N_words_to_print) {
	append_string(string, "\n");
	width = 0;
	do {
	    width += strlen(linkage->word[i])+1;
	    i++;
	} while((i<N_words_to_print) &&
	      (width + ((int)strlen(linkage->word[i]))+1 < x_screen_width));
	row_starts[N_rows] = i - (!print_word_0);    /* PS junk */
	if (i<N_words_to_print) N_rows++;     /* same */
	for (row = top_row; row >= 0; row--) {
	    flag = TRUE;
	    for (j=k;flag&&(j<k+width)&&(xpicture[row][j]!='\0'); j++){
		flag = flag && (xpicture[row][j] == ' ');
	    }
	    if (!flag) {
		for (j=k;(j<k+width)&&(xpicture[row][j]!='\0'); j++){
		    append_string(string, "%c", xpicture[row][j]);
		}
		append_string(string, "\n");
	    }
	}
	append_string(string, "\n");
	k += width;
    }
    gr_string = exalloc(strlen(string->p)+1);
    strcpy(gr_string, string->p);
    exfree(string->p, sizeof(char)*string->allocated);
    exfree(string, sizeof(String));
    return gr_string; 
}

char * linkage_print_postscript(Linkage linkage, int mode) {
    char * ps, * qs;
    int size;

    ps = build_linkage_postscript_string(linkage);
    size = strlen(header(mode)) + strlen(ps) + strlen(trailer(mode)) + 1;
    
    qs = exalloc(sizeof(char)*size);
    sprintf(qs, "%s%s%s", header(mode), ps, trailer(mode));
    exfree(ps, strlen(ps)+1);
						      
    return qs;
}

void print_disjunct_counts(Sentence sent) {
    int i;
    int c;
    Disjunct *d;
    for (i=0; i<sent->length; i++) {
	c = 0;
	for (d=sent->word[i].d; d != NULL; d = d->next) {
	    c++;
	}
	printf("%s(%d) ",sent->word[i].string, c);
    }
    printf("\n\n");
}

void print_expression_sizes(Sentence sent) {
    X_node * x;
    int w, size;
    for (w=0; w<sent->length; w++) {
	size = 0;
	for (x=sent->word[w].x; x!=NULL; x = x->next) {
	    size += size_of_expression(x->exp);
	}
	printf("%s[%d] ",sent->word[w].string, size);
    }
    printf("\n\n");
}

void print_sentence(FILE *fp, Sentence sent, int w) {
/* this version just prints it on one line.  */
    int i;
    if (sent->dict->left_wall_defined) i=1; else i=0;
    for (; i<sent->length - sent->dict->right_wall_defined; i++) {
	fprintf(fp, "%s ", sent->word[i].string);
    }
    fprintf(fp, "\n");
}

char * trailer(int mode) {
    static char * trailer_string=
        "diagram\n"
	"\n"
	"%%EndDocument\n"
	;

    if (mode==1) return trailer_string;
    else return "";
}

char * header(int mode) {
    static char * header_string=
        "%!PS-Adobe-2.0 EPSF-1.2\n"
        "%%Pages: 1\n"
        "%%BoundingBox: 0 -20 500 200\n"
        "%%EndComments\n"
        "%%BeginDocument: \n"
        "\n"
        "% compute size of diagram by adding\n"
        "% #rows x 8.5\n"
        "% (#rows -1) x 10\n"
        "% \\sum maxheight x 10\n"
        "/nulllink () def                     % The symbol of a null link\n"
        "/wordfontsize 11 def      % the size of the word font\n"
        "/labelfontsize 9 def      % the size of the connector label font\n"
        "/ex 10 def  % the horizontal radius of all the links\n"
        "/ey 10 def  % the height of the level 0 links\n"
        "/ed 10 def  % amount to add to this height per level\n"
        "/radius 10 def % radius for rounded arcs\n"
        "/row-spacing 10 def % the space between successive rows of the diagram\n"
        "\n"
        "/gap wordfontsize .5 mul def  % the gap between words\n"
        "/top-of-words wordfontsize .85 mul def\n"
        "             % the delta y above where the text is written where\n"
        "             % the major axis of the ellipse is located\n"
        "/label-gap labelfontsize .1 mul def\n"
        "\n"
        "/xwordfontsize 10 def      % the size of the word font\n"
        "/xlabelfontsize 10 def      % the size of the connector label font\n"
        "/xex 10 def  % the horizontal radius of all the links\n"
        "/xey 10 def  % the height of the level 0 links\n"
        "/xed 10 def  % amount to add to this height per level\n"
        "/xradius 10 def % radius for rounded arcs\n"
        "/xrow-spacing 10 def % the space between successive rows of the diagram\n"
        "/xgap wordfontsize .5 mul def  % the gap between words\n"
        "\n"
        "/centerpage 6.5 72 mul 2 div def\n"
        "  % this number of points from the left margin is the center of page\n"
        "\n"
        "/rightpage 6.5 72 mul def\n"
        "  % number of points from the left margin is the the right margin\n"
        "\n"
        "/show-string-centered-dict 5 dict def\n"
        "\n"
        "/show-string-centered {\n"
        "  show-string-centered-dict begin\n"
        "  /string exch def\n"
        "  /ycenter exch def\n"
        "  /xcenter exch def\n"
        "  xcenter string stringwidth pop 2 div sub\n"
        "  ycenter labelfontsize .3 mul sub\n"
        "  moveto\n"
        "  string show\n"
        "  end\n"
        "} def\n"
        "\n"
        "/clear-word-box {\n"
        "  show-string-centered-dict begin\n"
        "  /string exch def\n"
        "  /ycenter exch def\n"
        "  /xcenter exch def\n"
        "  newpath\n"
        "  /urx string stringwidth pop 2 div def\n"
        "  /ury labelfontsize .3 mul def\n"
        "  xcenter urx sub ycenter ury sub moveto\n"
        "  xcenter urx add ycenter ury sub lineto\n"
        "  xcenter urx add ycenter ury add lineto\n"
        "  xcenter urx sub ycenter ury add lineto\n"
        "  closepath\n"
        "  1 setgray fill\n"
        "  0 setgray\n"
        "  end\n"
        "} def\n"
        "\n"
        "/diagram-sentence-dict 20 dict def\n"
        "\n"
        "/diagram-sentence-circle\n"
        "{diagram-sentence-dict begin  \n"
        "   /links exch def\n"
        "   /words exch def\n"
        "   /n words length def\n"
        "   /Times-Roman findfont wordfontsize scalefont setfont\n"
        "   /x 0 def\n"
        "   /y 0 def\n"
        "\n"
        "   /left-ends [x dup words {stringwidth pop add gap add dup}\n"
        "	                     forall pop pop] def\n"
        "   /right-ends [x words {stringwidth pop add dup gap add} forall pop] def\n"
        "   /centers [0 1 n 1 sub {/i exch def\n"
        "		      left-ends i get\n"
        "		      right-ends i get\n"
        "		      add 2 div\n"
        "		    } for ] def\n"
        "\n"
        "   x y moveto\n"
        "   words {show gap 0 rmoveto} forall\n"
        "\n"
        "   .5 setlinewidth \n"
        "\n"
        "   links {dup 0 get /leftword exch def\n"
        "          dup 1 get /rightword exch def\n"
        "          dup 2 get /level exch def\n"
        "          3 get /string exch def\n"
        "          newpath\n"
        "          string nulllink eq {[2] 1 setdash}{[] 0 setdash} ifelse\n"
        "%          string nulllink eq {.8 setgray}{0 setgray} ifelse\n"
        "          centers leftword get\n"
        "	  y top-of-words add\n"
        "          moveto\n"
        "      \n"
        "          centers rightword get\n"
        "          centers leftword get\n"
        "          sub 2  div dup\n"
        "          radius \n"
        "          lt {/radiusx exch def}{pop /radiusx radius def} ifelse\n"
        "  \n"
        "          \n"
        " \n"
        "          centers leftword get\n"
        "	  y top-of-words add ey ed level mul add add\n"
        "          centers rightword get\n"
        "	  y top-of-words add ey ed level mul add add\n"
        "	  radiusx\n"
        "          arcto\n"
        "          4 {pop} repeat\n"
        "	  centers rightword get\n"
        "          y top-of-words add ey ed level mul add add\n"
        "	  centers rightword get\n"
        "	  y top-of-words add\n"
        "	  radiusx\n"
        "	  arcto\n"
        "          4 {pop} repeat\n"
        "	  centers rightword get\n"
        "	  y top-of-words add\n"
        "	  lineto\n"
        "\n"
        "	  stroke\n"
        "\n"
        "          /radius-y    ey ed level mul add	  def\n"
        "\n"
        "	  /center-arc-x\n"
        "	     centers leftword get centers rightword get add 2 div\n"
        "	  def\n"
        "	  \n"
        "          /center-arc-y\n"
        "             y top-of-words radius-y add add\n"
        "	  def\n"
        "\n"
        "          /Courier-Bold findfont labelfontsize scalefont setfont \n"
        "	  center-arc-x center-arc-y string clear-word-box\n"
        "	  center-arc-x center-arc-y string show-string-centered\n"
        "          } forall\n"
        "	  end\n"
        "  } def\n"
        "\n"
        "/diagramdict 20 dict def\n"
        "\n"
        "/diagram\n"
        "{diagramdict begin\n"
        "   /break-words exch def\n"
        "   /links exch def\n"
        "   /words exch def\n"
        "   /n words length def\n"
        "   /n-rows break-words length def\n"
        "   /Times-Roman findfont wordfontsize scalefont setfont\n"
        "\n"
        "   /left-ends [0 dup words {stringwidth pop add gap add dup}\n"
        "	                     forall pop pop] def\n"
        "   /right-ends [0 words {stringwidth pop add dup gap add} forall pop] def\n"
        "\n"
        "   /lwindows [ break-words {left-ends exch get gap 2 div sub } forall ] def\n"
        "   /rwindows [1 1 n-rows 1 sub {/i exch def\n"
        "		      lwindows i get } for\n"
        "	              right-ends n 1 sub get gap 2 div add\n"
        "	      ] def\n"
        "\n"
        "\n"
        "    /max 0 def\n"
        "    0 1 links length 1 sub {\n"
        "	/i exch def\n"
        "	/t links i get 2 get def\n"
        "	t max gt {/max t def} if\n"
        "      } for\n"
        "\n"
        "    /max-height ed max mul ey add top-of-words add row-spacing add def\n"
        "    /total-height n-rows max-height mul row-spacing sub def\n"
        "\n"
        "    /max-width 0 def            % compute the widest window\n"
        "    0 1 n-rows 1 sub {\n"
        "        /i exch def\n"
        "        /t rwindows i get lwindows i get sub def\n"
        "        t max-width gt {/max-width t def} if\n"
        "      } for\n"
        "\n"
        "    centerpage max-width 2 div sub 0 translate  % centers it\n"
        "   % rightpage max-width sub 0 translate      % right justified\n"
        "                        % Delete both of these to make it left justified\n"
        "\n"
        "   n-rows 1 sub -1 0\n"
        "     {/i exch def\n"
        "	gsave\n"
        "	newpath\n"
        "        %/centering centerpage rwindows i get lwindows i get sub 2 div sub def\n"
        "               % this line causes each row to be centered\n"
        "        /centering 0 def\n"
        "               % set centering to 0 to prevent centering of each row \n"
        "\n"
        "	centering -100 moveto  % -100 because some letters go below zero\n"
        "        centering max-height n-rows mul lineto\n"
        "        rwindows i get lwindows i get sub centering add\n"
        "                       max-height n-rows mul lineto\n"
        "        rwindows i get lwindows i get sub centering add\n"
        "                       -100 lineto\n"
        "	closepath\n"
        "        clip\n"
        "	lwindows i get neg n-rows i sub 1 sub max-height mul translate\n"
        "        centerpage centering 0 translate\n"
        "        words links diagram-sentence-circle\n"
        "	grestore\n"
        "     } for\n"
        "     end\n"
        "} def \n"
        "\n"
        "/diagramx\n"
        "{diagramdict begin\n"
        "   /break-words exch def\n"
        "   /links exch def\n"
        "   /words exch def\n"
        "   /n words length def\n"
        "   /n-rows break-words length def\n"
        "   /Times-Roman findfont xwordfontsize scalefont setfont\n"
        "\n"
        "   /left-ends [0 dup words {stringwidth pop add gap add dup}\n"
        "	                     forall pop pop] def\n"
        "   /right-ends [0 words {stringwidth pop add dup gap add} forall pop] def\n"
        "\n"
        "   /lwindows [ break-words {left-ends exch get gap 2 div sub } forall ] def\n"
        "   /rwindows [1 1 n-rows 1 sub {/i exch def\n"
        "		      lwindows i get } for\n"
        "	              right-ends n 1 sub get xgap 2 div add\n"
        "	      ] def\n"
        "\n"
        "\n"
        "    /max 0 def\n"
        "    0 1 links length 1 sub {\n"
        "	/i exch def\n"
        "	/t links i get 2 get def\n"
        "	t max gt {/max t def} if\n"
        "      } for\n"
        "\n"
        "    /max-height xed max mul xey add top-of-words add xrow-spacing add def\n"
        "    /total-height n-rows max-height mul xrow-spacing sub def\n"
        "\n"
        "    /max-width 0 def            % compute the widest window\n"
        "    0 1 n-rows 1 sub {\n"
        "        /i exch def\n"
        "        /t rwindows i get lwindows i get sub def\n"
        "        t max-width gt {/max-width t def} if\n"
        "      } for\n"
        "\n"
        "    centerpage max-width 2 div sub 0 translate  % centers it\n"
        "   % rightpage max-width sub 0 translate      % right justified\n"
        "                        % Delete both of these to make it left justified\n"
        "\n"
        "   n-rows 1 sub -1 0\n"
        "     {/i exch def\n"
        "	gsave\n"
        "	newpath\n"
        "        %/centering centerpage rwindows i get lwindows i get sub 2 div sub def\n"
        "               % this line causes each row to be centered\n"
        "        /centering 0 def\n"
        "               % set centering to 0 to prevent centering of each row \n"
        "\n"
        "	centering -100 moveto  % -100 because some letters go below zero\n"
        "        centering max-height n-rows mul lineto\n"
        "        rwindows i get lwindows i get sub centering add\n"
        "                       max-height n-rows mul lineto\n"
        "        rwindows i get lwindows i get sub centering add\n"
        "                       -100 lineto\n"
        "	closepath\n"
        "        clip\n"
        "	lwindows i get neg n-rows i sub 1 sub max-height mul translate\n"
        "        centerpage centering 0 translate\n"
        "        words links diagram-sentence-circle\n"
        "	grestore\n"
        "     } for\n"
        "     end\n"
        "} def \n"
        "\n"
        "/ldiagram\n"
        "{diagramdict begin\n"
        "   /break-words exch def\n"
        "   /links exch def\n"
        "   /words exch def\n"
        "   /n words length def\n"
        "   /n-rows break-words length def\n"
        "   /Times-Roman findfont wordfontsize scalefont setfont\n"
        "\n"
        "   /left-ends [0 dup words {stringwidth pop add gap add dup}\n"
        "	                     forall pop pop] def\n"
        "   /right-ends [0 words {stringwidth pop add dup gap add} forall pop] def\n"
        "\n"
        "   /lwindows [ break-words {left-ends exch get gap 2 div sub } forall ] def\n"
        "   /rwindows [1 1 n-rows 1 sub {/i exch def\n"
        "		      lwindows i get } for\n"
        "	              right-ends n 1 sub get gap 2 div add\n"
        "	      ] def\n"
        "\n"
        "\n"
        "    /max 0 def\n"
        "    0 1 links length 1 sub {\n"
        "	/i exch def\n"
        "	/t links i get 2 get def\n"
        "	t max gt {/max t def} if\n"
        "      } for\n"
        "\n"
        "    /max-height ed max mul ey add top-of-words add row-spacing add def\n"
        "    /total-height n-rows max-height mul row-spacing sub def\n"
        "\n"
        "    /max-width 0 def            % compute the widest window\n"
        "    0 1 n-rows 1 sub {\n"
        "        /i exch def\n"
        "        /t rwindows i get lwindows i get sub def\n"
        "        t max-width gt {/max-width t def} if\n"
        "      } for\n"
        "\n"
        "   % centerpage max-width 2 div sub 0 translate  % centers it\n"
        "   % rightpage max-width sub 0 translate      % right justified\n"
        "                        % Delete both of these to make it left justified\n"
        "\n"
        "   n-rows 1 sub -1 0\n"
        "     {/i exch def\n"
        "	gsave\n"
        "	newpath\n"
        "        %/centering centerpage rwindows i get lwindows i get sub 2 div sub def\n"
        "               % this line causes each row to be centered\n"
        "        /centering 0 def\n"
        "               % set centering to 0 to prevent centering of each row \n"
        "\n"
        "	centering -100 moveto  % -100 because some letters go below zero\n"
        "        centering max-height n-rows mul lineto\n"
        "        rwindows i get lwindows i get sub centering add\n"
        "                       max-height n-rows mul lineto\n"
        "        rwindows i get lwindows i get sub centering add\n"
        "                       -100 lineto\n"
        "	closepath\n"
        "        clip\n"
        "	lwindows i get neg n-rows i sub 1 sub max-height mul translate\n"
        "        centerpage centering 0 translate\n"
        "        words links diagram-sentence-circle\n"
        "	grestore\n"
        "     } for\n"
        "     end\n"
        "} def \n"
	;
    if (mode==1) return header_string;
    else return "";
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include <stdarg.h>
#include "link-includes.h"

/* This is a "safe" append function, used here to build up a link diagram
   incrementally.  Because the diagram is built up a few characters at
   a time, we keep around a pointer to the end of string to prevent
   the algorithm from being quadratic. */

String * String_create() {
    String * string;
    string = (String *) exalloc(sizeof(String));
    string->allocated = 1;
    string->p = (char *) exalloc(sizeof(char));
    string->p[0] = '\0';
    string->eos = string->p;
    return string;
}

int append_string(String * string, char *fmt, ...) {
    char temp_string[1024];
    char * p;
    int new_size;
    va_list args;

    va_start(args, fmt);
    vsprintf(temp_string, fmt, args); 
    va_end(args);

    if (string->allocated <= strlen(string->p)+strlen(temp_string)) {
	new_size = 2*string->allocated+strlen(temp_string)+1;
	p = exalloc(sizeof(char)*new_size);
	strcpy(p, string->p);
	strcat(p, temp_string);
	exfree(string->p, sizeof(char)*string->allocated);
	string->p = p;
	string->eos = strchr(p,'\0');
	string->allocated = new_size;
    }
    else {
	strcat(string->eos, temp_string);
	string->eos += strlen(temp_string);
    }

    return 0;
}

/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

static char ** deletable;
static char ** effective_dist;
static int     null_links;

/*

  The algorithms in this file prune disjuncts from the disjunct list
  of the sentence that can be elimininated by a simple checks.  The first
  check works as follows:

  A series of passes are made through the sentence, alternating
  left-to-right and right-to-left.  Consier the left-to-right pass (the
  other is symmetric).  A set S of connectors is maintained (initialized
  to be empty).  Now the disjuncts of the current word are processed.
  If a given disjunct's left pointing connectors have the property that
  at least one of them has no connector in S to which it can be matched,
  then that disjunct is deleted. Now the set S is augmented by the right
  connectors of the remaining disjuncts of that word.  This completes
  one word.  The process continues through the words from left to right.
  Alternate passes are made until no disjunct is deleted.

  It worries me a little that if there are some really huge disjuncts lists,
  then this process will probably do nothing.  (This fear turns out to be 
  unfounded.)

  Notes:  Power pruning will not work if applied before generating the
  "and" disjuncts.  This is because certain of it's tricks don't work.
  Think about this, and finish this note later....
  Also, currently I use the standard connector match procedure instead
  of the pruning one, since I know power pruning will not be used before 
  and generation.  Replace this to allow power pruning to work before
  generating and disjuncts.

  Currently it seems that normal pruning, power pruning, and generation,
  pruning, and power pruning (after "and" generation) and parsing take
  about the same amount of time.  This is why doing power pruning before
  "and" generation might be a very good idea.

  New idea:  Suppose all the disjuncts of a word have a connector of type
  c pointing to the right.  And further, suppose that there is exactly one
  word to its right containing that type of connector pointing to the left.
  Then all the other disjuncts on the latter word can be deleted.
  (This situation is created by the processing of "either...or", and by
  the extra disjuncts added to a "," neighboring a conjunction.)

*/

int x_prune_match(Connector *a, Connector *b) {
    return prune_match(a, b, 0, 0);
}

int prune_match(Connector *a, Connector *b, int aw, int bw) {
/* This is almost identical to match().  Its reason for existance
   is the rather subtle fact that with "and" can transform a "Ss"
   connector into "Sp".  This means that in order for pruning to
   work, we must allow a "Ss" connector on word match an "Sp" connector
   on a word to its right.  This is what this version of match allows.
   we assume that a is on a word to the left of b.
*/
    char *s, *t;
    int x, y, dist;
    if (a->label != b->label) return FALSE;
    x = a->priority;
    y = b->priority;

    s = a->string;
    t = b->string;

    while(isupper((int)*s) || isupper((int)*t)) {
	if (*s != *t) return FALSE;
	s++;
	t++;
    }


    if (aw==0 && bw==0) {  /* probably not necessary, as long as effective_dist[0][0]=0 and is defined */
	dist = 0;
    } else {
	assert(aw < bw, "prune_match() did not receive params in the natural order.");
	dist = effective_dist[aw][bw];
    }
    /*    printf("PM: a=%4s b=%4s  ap=%d bp=%d  aw=%d  bw=%d  a->ll=%d b->ll=%d  dist=%d\n", 
	   s, t, x, y, aw, bw, a->length_limit, b->length_limit, dist); */
    if (dist > a->length_limit || dist > b->length_limit) return FALSE;


    if ((x==THIN_priority) && (y==THIN_priority)) {
#if defined(PLURALIZATION)
/*
        if ((*(a->string)=='S') && ((*s=='s') || (*s=='p')) &&  (*t=='p')) {
	    return TRUE;
	}
*/
/*
   The above is a kludge to stop pruning from killing off disjuncts
   which (because of pluralization in and) might become valid later.
   Recall that "and" converts a singular subject into a plural one.
   The (*s=='p') part is so that "he and I are good" doesn't get killed off.
   The above hack is subsumed by the following one:
*/
	if ((*(a->string)=='S') && ((*s=='s') || (*s=='p')) &&
	    ((*t=='p') || (*t=='s')) &&
	    ((s-1 == a->string) || ((s-2 == a->string) && (*(s-1) == 'I')))){
	    return TRUE;
	}
/*
   This change is to accommodate "nor".  In particular we need to
   prevent "neither John nor I likes dogs" from being killed off.
   We want to allow this to apply to "are neither a dog nor a cat here"
   and "is neither a dog nor a cat here".  This uses the "SI" connector.
   The third line above ensures that the connector is either "S" or "SI".
*/
#endif	
	while ((*s!='\0') && (*t!='\0')) {
	    if ((*s == '*') || (*t == '*') ||
		((*s == *t) && (*s != '^'))) {
              /* this last case here is rather obscure.  It prevents
                 '^' from matching '^'.....Is this necessary?
		     ......yes, I think it is.   */
		s++;
		t++;
	    } else return FALSE;
	}
	return TRUE;
    } else if ((x==UP_priority) && (y==DOWN_priority)) {
	while ((*s!='\0') && (*t!='\0')) {
	    if ((*s == *t) || (*s == '*') || (*t == '^')) {
                /* that '^' should match on the DOWN_priority
		   node is subtle, but correct */
		s++;
		t++;
	    } else return FALSE;
	}
	return TRUE;
    } else if ((y==UP_priority) && (x==DOWN_priority)) {
	while ((*s!='\0') && (*t!='\0')) {
	    if ((*s == *t) || (*t == '*') || (*s == '^')) {
		s++;
		t++;
	    } else return FALSE;
	}
	return TRUE;
    } else return FALSE;
}

static int s_table_size;
static Connector ** table2;

void free_S(void) {
/* This function removes all connectors from the set S */
    int i;
    for (i=0; i<s_table_size; i++) {
	if (table2[i] == NULL) continue; /* a prehaps stupid optimization */
	free_connectors(table2[i]);
	table2[i] = NULL;
    }
}

int hash_S(Connector * c) {
/* This hash function only looks at the leading upper case letters of
   the connector string, and the label fields.  This ensures that if two
   strings match (formally), then they must hash to the same place.
*/
    char *s;
    int i;
    i = c->label;
    s = c->string;
    while(isupper((int)*s)) {
	i = i + (i<<1) + randtable[(*s + i) & (RTSIZE-1)];
	s++;
    }
    return (i & (s_table_size-1));
}

void insert_S(Connector * c) {
/* this function puts a copy of c into S if one like it isn't already there */
    int h;
    Connector * e;

    h = hash_S(c);

    for (e = table2[h]; e != NULL; e = e->next) {
	if ((strcmp(c->string, e->string) == 0) &&
	    (c->label == e->label) && (c->priority == e->priority)) {
	    return;
	}
    }
    e = init_connector((Connector *) xalloc(sizeof(Connector)));
    *e = *c;
    e->next = table2[h];
    table2[h] = e;
}


void zero_S(void) {
    int i;
    for (i=0; i<s_table_size; i++) {
	table2[i] = NULL;
    }
}

#if 0
OBS void init_S(Connector * c) {
OBS /* put a copy of the the connector list c into S.  Assumes S is empty */
OBS     int h;
OBS     Connector *c1;
OBS     for (; c!=NULL; c = c->next) {
OBS 	h = hash_S(c);
OBS 	c1 = new_connector();
OBS 	*c1 = *c;
OBS 	c1->next = table2[h];
OBS 	table2[h] = c1;
OBS     }
OBS }
#endif

int matches_S(Connector * c, int dir) {
/* returns TRUE if c can match anything in the set S */
/* because of the horrible kludge, prune match is assymetric, and   */
/* direction is '-' if this is an l->r pass, and '+' if an r->l pass.   */

    int h;
    Connector * e;

    h = hash_S(c);
    if (dir=='-') {
	for (e = table2[h]; e != NULL; e = e->next) {
	    if (x_prune_match(e, c)) return TRUE;
	}
    } else {
	for (e = table2[h]; e != NULL; e = e->next) {
	    if (x_prune_match(c, e)) return TRUE;
	}
    }
    return FALSE;
}

void clean_up(Sentence sent, int w) {
/* This removes the disjuncts that are empty from the list corresponding
   to word w of the sentence.
*/
    Disjunct head_disjunct, *d, *d1;

    d = &head_disjunct;

    d->next = sent->word[w].d;

    while(d->next != NULL) {
	if ((d->next->left == NULL) && (d->next->right == NULL)) {
	    d1 = d->next;
	    d->next = d1->next;
	    xfree((char *)d1, sizeof(Disjunct));
	} else {
	    d = d->next;
	}
    }
    sent->word[w].d = head_disjunct.next;
}

int count_disjuncts(Disjunct * d) {
/* returns the number of disjuncts in the list pointed to by d */
    int count = 0;
    for (; d!=NULL; d=d->next) {
	count++;
    }
    return count;
}

int count_disjuncts_in_sentence(Sentence sent) {
/* returns the total number of disjuncts in the sentence */
    int w, count;
    count = 0;
    for (w=0; w<sent->length; w++) {
	count += count_disjuncts(sent->word[w].d);
    }
    return count;
}

void prune(Sentence sent) {
    int N_deleted;
    Disjunct *d;
    Connector *e;
    int w;

    s_table_size = next_power_of_two_up(count_disjuncts_in_sentence(sent));
    table2 = (Connector **) xalloc(s_table_size * sizeof (Connector *));
/* You know, I don't think this makes much sense.  This is probably much  */
/* too big.  There are many fewer connectors than disjuncts.              */

    zero_S();
    N_deleted = 1;  /* a lie to make it always do at least 2 passes */
    count_set_effective_distance(sent);

    for (;;) {
	/* left-to-right pass */

	for (w = 0; w < sent->length; w++) {
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		for (e = d->left; e != NULL; e = e->next) {
		    if (!matches_S(e, '-')) break;
		}
		if (e != NULL) {
		    /* we know this disjunct is dead */
		    N_deleted ++;
		    free_connectors(d->left);
		    free_connectors(d->right);
		    d->left = d->right = NULL;
		}
	    }
	    clean_up(sent, w);
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		for (e = d->right; e != NULL; e = e->next) {
		    insert_S(e);
		}
	    }
	}

	if (verbosity > 2) {
	    printf("l->r pass removed %d\n",N_deleted);
	    print_disjunct_counts(sent);
	}
	free_S();
	if (N_deleted == 0) break;

	/* right-to-left pass */
	N_deleted = 0;
	for (w = sent->length-1; w >= 0; w--) {
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		for (e = d->right; e != NULL; e = e->next) {
		    if (!matches_S(e,'+')) break;
		}
		if (e != NULL) {
		    /* we know this disjunct is dead */
		    N_deleted ++;
		    free_connectors(d->left);
		    free_connectors(d->right);
		    d->left = d->right = NULL;
		}
	    }
	    clean_up(sent, w);
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		for (e = d->left; e != NULL; e = e->next) {
		    insert_S(e);
		}
	    }
	}
	if (verbosity > 2) {
	    printf("r->l pass removed %d\n",N_deleted);
	    print_disjunct_counts(sent);
	}
	free_S();
	if (N_deleted == 0) break;
	N_deleted = 0;
    }
    xfree((char *)table2, s_table_size * sizeof (Connector *));
}

/*
   The second algorithm eliminates disjuncts that are dominated by 
   another.  It works by hashing them all, and checking for domination.
*/

static int dup_table_size;
static Disjunct ** dup_table;

int string_hash(char * s, int i) {
/* hash function that takes a string and a seed value i */
    for(;*s != '\0';s++) i = i + (i<<1) + randtable[(*s + i) & (RTSIZE-1)];
    return (i & (dup_table_size-1));
}

#if FALSE

/*
  Consider the idea of deleting a disjunct if it is dominated (in terms of
  what it can match) by some other disjunct on the same word.  This has
  been implemented below.  There are three problems with it:

  (1) It is almost never the case that any disjuncts are eliminated.
      (The code below has works correctly with fat links, but because
      all of the fat connectors on a fat disjunct have the same matching
      string, the only time a disjuct will die is if it is the same
      as another one.  This is captured by the simplistic version below.

  (2) connector_matches_alam may not be exactly correct.  I don't
      think it does the fat link matches properly.   (See the comment
      in and.c for more information about matching fat links.)  This is
      irrelevant because of (1).
      
  (3) The linkage that is eliminated by this, might just be the one that
      passes post-processing, as the following example shows.
      This is pretty silly, and should probably be changed.

> telling John how our program works would be stupid 
Accepted (2 linkages, 1 with no P.P. violations)
  Linkage 1, cost vector = (0, 0, 7)

    +------------------G-----------------+          
    +-----R-----+----CL----+             |          
    +---O---+   |   +---D--+---S---+     +--I-+-AI-+
    |       |   |   |      |       |     |    |    |
telling.g John how our program.n works would be stupid 

               /////          CLg     <---CLg--->  CL        telling.g
 (g)           telling.g      G       <---G----->  G         would
 (g) (d)       telling.g      R       <---R----->  R         how
 (g) (d)       telling.g      O       <---O----->  O         John
 (g) (d)       how            CLe     <---CLe--->  CL        program.n
 (g) (d) (e)   our            D       <---Ds---->  Ds        program.n
 (g) (d) (e)   program.n      Ss      <---Ss---->  Ss        works
 (g)           would          I       <---Ix---->  Ix        be
 (g)           be             AI      <---AIi--->  AIi       stupid

(press return for another)
> 
  Linkage 2 (bad), cost vector = (0, 0, 7)

    +------------------G-----------------+          
    +-----R-----+----CL----+             |          
    +---O---+   |   +---D--+---S---+     +--I-+-AI-+
    |       |   |   |      |       |     |    |    |
telling.g John how our program.n works would be stupid 

               /////          CLg     <---CLg--->  CL        telling.g
 (g)           telling.g      G       <---G----->  G         would
 (g) (d)       telling.g      R       <---R----->  R         how
 (g) (d)       telling.g      O       <---O----->  O         John
 (g) (d)       how            CLe     <---CLe--->  CL        program.n
 (g) (d) (e)   our            D       <---Ds---->  Ds        program.n
 (g) (d) (e)   program.n      Ss      <---Ss---->  Ss        works
 (g)           would          I       <---Ix---->  Ix        be
 (g)           be             AI      <---AI---->  AI        stupid

P.P. violations:
        Special subject rule violated
*/

int connector_matches_alam(Connector * a, Connector * b) {
/* This returns true if the connector a matches everything that b
   matches, and possibly more.  (alam=at least as much)

   TRUE for equal connectors.
   remains TRUE if multi-match added to the first.
   remains TRUE if subsrcripts deleted from the first.

*/
    char * s, * t, *u;
    if (((!a->multi) && b->multi) ||
	(a->label != b->label) ||
	(a->priority != b->priority))  return FALSE;
    s = a->string;
    t = b->string;
    while(isupper(*s) || isupper(*t)) {
	if (*s == *t) {
	    s++;
	    t++;
	} else return FALSE;
    }
    if (a->priority == DOWN_priority) {
	u = s;
	s = t;
	t = u;
    }
    while((*s != '\0') && (*t != '\0')) {
	if ((*s == *t) || (*s == '*') || (*t == '^')) {
	    s++;
	    t++;
	} else return FALSE;
    }
    while ((*s != '\0') && (*s == '*')) s++;
    return (*s == '\0');
}

int connector_hash(Connector * c, int i) {
/* This hash function that takes a connector and a seed value i.
   It only looks at the leading upper case letters of
   the string, and the label.  This ensures that if two connectors
   match, then they must hash to the same place. 
*/
    char * s;
    s = c->string;

    i = i + (i<<1) + randtable[(c->label + i) & (RTSIZE-1)];
    while(isupper(*s)) {
	i = i + (i<<1) + randtable[(*s + i) & (RTSIZE-1)];
	s++;
    }
    return (i & (dup_table_size-1));
}

int hash_disjunct(Disjunct * d) {
/* This is a hash function for disjuncts */
    int i;
    Connector *e;
    i = 0;
    for (e = d->left ; e != NULL; e = e->next) {
	i = connector_hash(e, i);
    }
    for (e = d->right ; e != NULL; e = e->next) {
	i = connector_hash(e, i);
    }
    return string_hash(d->string, i);
}

int disjunct_matches_alam(Disjunct * d1, Disjunct * d2) {
/* returns TRUE if disjunct d1 can match anything that d2 can       */
/* if this happens, it constitutes a proof that there is absolutely */
/* no use for d2. */    
    Connector *e1, *e2;
    if (d1->cost > d2->cost) return FALSE;
    e1 = d1->left;
    e2 = d2->left;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connector_matches_alam(e1,e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    e1 = d1->right;
    e2 = d2->right;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connector_matches_alam(e1,e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    return (strcmp(d1->string, d2->string) == 0);
}

Disjunct * eliminate_duplicate_disjuncts(Disjunct * d) {
/* Takes the list of disjuncts pointed to by d, eliminates all
   duplicates, and returns a pointer to a new list.
   It frees the disjuncts that are eliminated.
*/
    int i, h, count;
    Disjunct *dn, *dx, *dxn, *front;
    count = 0;
    dup_table_size = next_power_of_two_up(2 * count_disjuncts(d));
    dup_table = (Disjunct **) xalloc(dup_table_size * sizeof(Disjunct *));
    for (i=0; i<dup_table_size; i++) dup_table[i] = NULL;
    for (;d!=NULL; d = dn) {
	dn = d->next;
	h = hash_disjunct(d);

	front = NULL;
	for (dx = dup_table[h]; dx!=NULL; dx=dxn) {
	    dxn = dx->next;
	    if (disjunct_matches_alam(dx,d)) {
		/* we know that d should be killed */
		d->next = NULL;
		free_disjuncts(d);
		count++;
		front = catenate_disjuncts(front, dx);
		break;
	    } else if (disjunct_matches_alam(d,dx)) {
		/* we know that dx should be killed off */
		dx->next = NULL;
		free_disjuncts(dx);
		count++;
	    } else {
		/* neither should be killed off */
		dx->next = front;
		front = dx;
	    }
	}
	if (dx == NULL) {
	    /* we put d in the table2 */
	    d->next = front;
	    front = d;
	}
	dup_table[h] = front;
    }
    /* d is now NULL */
    for (i=0; i<dup_table_size; i++) {
	for (dx = dup_table[i]; dx != NULL; dx = dxn) {
	    dxn = dx->next;
	    dx->next = d;
	    d = dx;
	}
    }
    xfree((char *) dup_table, dup_table_size * sizeof(Disjunct *));
    if ((verbosity > 2)&&(count != 0)) printf("killed %d duplicates\n",count);
    return d;
}

#endif

/*
   Here is the old version that doesn't check for domination, just
   equality.
*/

int old_hash_disjunct(Disjunct * d) {
/* This is a hash function for disjuncts */
    int i;
    Connector *e;
    i = 0;
    for (e = d->left ; e != NULL; e = e->next) {
	i = string_hash(e->string, i);
    }
    for (e = d->right ; e != NULL; e = e->next) {
	i = string_hash(e->string, i);
    }
    return string_hash(d->string, i);
}

int connectors_equal_prune(Connector *c1, Connector *c2) {
/* The connectors must be exactly equal.  A similar function
   is connectors_equal_AND(), but that ignores priorities,
   this does not.
*/
    return (c1->label == c2->label) &&
	   (c1->multi == c2->multi) &&
	   (c1->priority == c2->priority) &&
           (strcmp(c1->string, c2->string) == 0);
}

int disjuncts_equal(Disjunct * d1, Disjunct * d2) {
/* returns TRUE if the disjuncts are exactly the same */
    Connector *e1, *e2;
    e1 = d1->left;
    e2 = d2->left;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connectors_equal_prune(e1,e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    e1 = d1->right;
    e2 = d2->right;
    while((e1!=NULL) && (e2!=NULL)) {
	if (!connectors_equal_prune(e1,e2)) break;
	e1 = e1->next;
	e2 = e2->next;
    }
    if ((e1!=NULL) || (e2!=NULL)) return FALSE;
    return (strcmp(d1->string, d2->string) == 0);
}

Disjunct * eliminate_duplicate_disjuncts(Disjunct * d) {
/* Takes the list of disjuncts pointed to by d, eliminates all
   duplicates, and returns a pointer to a new list.
   It frees the disjuncts that are eliminated.
*/
    int i, h, count;
    Disjunct *dn, *dx;
    count = 0;
    dup_table_size = next_power_of_two_up(2 * count_disjuncts(d));
    dup_table = (Disjunct **) xalloc(dup_table_size * sizeof(Disjunct *));
    for (i=0; i<dup_table_size; i++) dup_table[i] = NULL;
    while (d!=NULL) {
	dn = d->next;
	h = old_hash_disjunct(d);

	for (dx = dup_table[h]; dx!=NULL; dx=dx->next) {
	    if (disjuncts_equal(dx, d)) break;
	}
	if (dx==NULL) {
	    d->next = dup_table[h];
	    dup_table[h] = d;
	} else {
	    d->next = NULL;  /* to prevent it from freeing the whole list */
	    if (d->cost < dx->cost) dx->cost = d->cost;
	    free_disjuncts(d);
	    count++;
	}
	d = dn;
    }
    /* d is already null */
    for (i=0; i<dup_table_size; i++) {
	for (dn = dup_table[i]; dn != NULL; dn = dx) {
	    dx = dn->next;
	    dn->next = d;
	    d = dn;
	}
    }
    xfree((char *) dup_table, dup_table_size * sizeof(Disjunct *));
    if ((verbosity > 2)&&(count != 0)) printf("killed %d duplicates\n",count);
    return d;
}


/* Here is expression pruning.  This is done even before the expressions
   are turned into lists of disjuncts.

   This uses many of the same data structures and functions that are used
   by prune.
*/

int size_of_sentence_expressions(Sentence sent) {
/* Computes and returns the number of connectors in all of the expressions 
   of the sentence.
*/
    X_node * x;
    int w, size;
    size = 0;
    for (w=0; w<sent->length; w++) {
	for (x=sent->word[w].x; x!=NULL; x = x->next) {
	    size += size_of_expression(x->exp);
	}
    }
    return size;
}

/* The purge operations remove all irrelevant stuff from the expression,    */
/* and free the purged stuff.  A connector is deemed irrelevant if its      */
/* string pointer has been set to NULL.  The passes through the sentence    */
/* have the job of doing this.                                              */

/* If an OR or AND type expression node has one child, we can replace it by */
/* its child.  This, of course, is not really necessary                     */

int and_purge_E_list(E_list * l);
E_list * or_purge_E_list(E_list * l);

Exp* purge_Exp(Exp *e) {
/* Must be called with a non-null expression                                */
/* Return NULL iff the expression has no disjuncts.                         */
/*  Exp * ne; */

    if (e->type == CONNECTOR_type) {
	if (e->u.string == NULL) {
	    xfree((char *)e, sizeof(Exp));
	    return NULL;
	} else {
	    return e;
	}
    }
    if (e->type == AND_type) {
	if (and_purge_E_list(e->u.l) == 0) {
	    xfree((char *)e, sizeof(Exp));
	    return NULL;
	}
    } else {
	e->u.l = or_purge_E_list(e->u.l);
	if (e->u.l == NULL) {
	    xfree((char *)e, sizeof(Exp));
	    return NULL;
	}
    }

/* This code makes it kill off nodes that have just one child
   (1) It's going to give an insignificant speed-up
   (2) Costs have not been handled correctly here.
   The code is excised for these reasons.
*/
/*  
    if ((e->u.l != NULL) && (e->u.l->next == NULL)) { 
	ne = e->u.l->e;
	xfree((char *) e->u.l, sizeof(E_list));
	xfree((char *) e, sizeof(Exp));
	return ne;
    }
*/    
    return e;
}

int and_purge_E_list(E_list * l) {
/* Returns 0 iff the length of the disjunct list is 0.          */
/* If this is the case, it frees the structure rooted at l.     */
    if (l == NULL) return 1;
    if ((l->e = purge_Exp(l->e)) == NULL) {
	free_E_list(l->next);
	xfree((char *)l, sizeof(E_list));
	return 0;
    }
    if (and_purge_E_list(l->next) == 0) {
	free_Exp(l->e);
	xfree((char *)l, sizeof(E_list));
	return 0;
    }
    return 1;
}

E_list * or_purge_E_list(E_list * l) {
/* get rid of the elements with null expressions */
    E_list * el;
    if (l == NULL) return NULL;
    if ((l->e = purge_Exp(l->e)) == NULL) {
	el = or_purge_E_list(l->next);
	xfree((char *)l, sizeof(E_list));
	return el;
    }
    l->next = or_purge_E_list(l->next);
    return l;
}

int mark_dead_connectors(Exp * e, int dir) {
/* Mark as dead all of the dir-pointing connectors
   in e that are not matched by anything in the current set.
   Returns the number of connectors so marked.
*/
    Connector dummy;
    int count;
    E_list *l;
    init_connector(&dummy);
    dummy.label = NORMAL_LABEL;
    dummy.priority = THIN_priority;
    /*    dummy.my_word = NO_WORD; */  /* turn off the length part of the matching */
    count = 0;
    if (e->type == CONNECTOR_type) {
	if (e->dir == dir) {
	    dummy.string = e->u.string;
	    if (!matches_S(&dummy,dir)) {
		e->u.string = NULL;
		count++;
	    }
	}
    } else {
	for (l=e->u.l; l!=NULL; l=l->next) {
	    count += mark_dead_connectors(l->e, dir);
	}
    }
    return count;
}

void insert_connectors(Exp * e, int dir) {
/* Put into the set S all of the dir-pointing connectors still in e.    */
    Connector dummy;
    E_list *l;
    init_connector(&dummy);
    dummy.label = NORMAL_LABEL;
    dummy.priority = THIN_priority;
    /*    dummy.my_word = NO_WORD; */ /* turn off the length part of the matching */    

    if (e->type == CONNECTOR_type) {
	if (e->dir == dir) {
	    dummy.string = e->u.string;
	    insert_S(&dummy);
	}
    } else {
	for (l=e->u.l; l!=NULL; l=l->next) {
	    insert_connectors(l->e, dir);
	}
    }
}

void clean_up_expressions(Sentence sent, int w) {
/* This removes the expressions that are empty from the list corresponding
   to word w of the sentence.
*/
    X_node head_node, *d, *d1;
    d = &head_node;
    d->next = sent->word[w].x;
    while(d->next != NULL) {
	if (d->next->exp == NULL) {
	    d1 = d->next;
	    d->next = d1->next;
	    xfree((char *)d1, sizeof(X_node));
	} else {
	    d = d->next;
	}
    }
    sent->word[w].x = head_node.next;
}

void expression_prune(Sentence sent){
    int N_deleted;
    X_node * x;
    int w;

    s_table_size = next_power_of_two_up(size_of_sentence_expressions(sent));
    table2 = (Connector **) xalloc(s_table_size * sizeof (Connector *));

    zero_S();
    N_deleted = 1;  /* a lie to make it always do at least 2 passes */

    for (;;) {
	/* left-to-right pass */
	for (w = 0; w < sent->length; w++) {
	    for (x = sent->word[w].x; x != NULL; x = x->next) {
/*     printf("before marking: "); print_expression(x->exp); printf("\n"); */
		N_deleted += mark_dead_connectors(x->exp, '-');
/*     printf("after marking marking: "); print_expression(x->exp); printf("\n"); */
	    }
	    for (x = sent->word[w].x; x != NULL; x = x->next) {
/*     printf("before purging: "); print_expression(x->exp); printf("\n"); */
		x->exp = purge_Exp(x->exp);
/*     printf("after purging: "); print_expression(x->exp); printf("\n"); */
	    }
	    clean_up_expressions(sent, w);  /* gets rid of X_nodes with NULL exp */
	    for (x = sent->word[w].x; x != NULL; x = x->next) {
		insert_connectors(x->exp,'+');
	    }
	}

	if (verbosity > 2) {
	    printf("l->r pass removed %d\n",N_deleted);
	    print_expression_sizes(sent);
	}

	free_S();
	if (N_deleted == 0) break;

	/* right-to-left pass */
	N_deleted = 0;
	for (w = sent->length-1; w >= 0; w--) {
	    for (x = sent->word[w].x; x != NULL; x = x->next) {
/*     printf("before marking: "); print_expression(x->exp); printf("\n"); */
		N_deleted += mark_dead_connectors(x->exp, '+');
/*     printf("after marking: "); print_expression(x->exp); printf("\n"); */
	    }
	    for (x = sent->word[w].x; x != NULL; x = x->next) {
/*     printf("before perging: "); print_expression(x->exp); printf("\n"); */
		x->exp = purge_Exp(x->exp);
/*     printf("after perging: "); print_expression(x->exp); printf("\n"); */
	    }
	    clean_up_expressions(sent, w);  /* gets rid of X_nodes with NULL exp */
	    for (x = sent->word[w].x; x != NULL; x = x->next) {
		insert_connectors(x->exp, '-');
	    }
	}

	if (verbosity > 2) {
	    printf("r->l pass removed %d\n",N_deleted);
	    print_expression_sizes(sent);
	}
	free_S();
	if (N_deleted == 0) break;
	N_deleted = 0;
    }
    xfree((char *)table2, s_table_size * sizeof (Connector *));
}



/*
  Here is what you've been waiting for: POWER-PRUNE

  The kinds of constraints it checks for are the following:

    1) successive connectors on the same disjunct have to go to
       nearer and nearer words.

    2) two deep connectors cannot attach to eachother
       (A connectors is deep if it is not the first in its list, it
       is shallow if it is the first in its list, it is deepest if it
       is the last on its list.)

    3) on two adjacent words, a pair of connectors can be used
       only if they're the deepest ones on their disjuncts

    4) on two non-adjacent words, a pair of connectors can be used only
       if not [both of them are the deepest].

   The data structure consists of a pair of hash tables on every word.
   Each bucket of a hash table has a list of pointers to connectors.
   These nodes also store if the chosen connector is shallow.
*/
typedef struct c_list_struct C_list;
struct c_list_struct {
    Connector * c;
    int shallow;
    C_list * next;
};
/*
   As with normal pruning, we make alternate left->right and right->left
   passes.  In the R->L pass, when we're on a word w, we make use of
   all the left-pointing hash tables on the words to the right of w.
   After the pruning on this word, we build the left-pointing hash table
   this word.  This guarantees idempotence of the pass -- after doing an
   L->R, doing another would change nothing.

   Each connector has an integer c_word field.  This refers to the closest
   word that it could be connected to.  These are initially determined by
   how deep the connector is.  For example, a deepest connector can connect
   to the neighboring word, so its c_word field is w+1 (w-1 if this is a left
   pointing connector).  It's neighboring shallow connector has a c_word
   value of w+2, etc.

   The pruning process adjusts these c_word values as it goes along,
   accumulating information about any way of linking this sentence.
   The pruning process stops only after no disjunct is deleted and no
   c_word values change.

   The difference between RUTHLESS and GENTLE power pruning is simply
   that GENTLE uses the deletable region array, and RUTHLESS does not.
   So we can get the effect of these two different methods simply by
   always unsuring that deletable[][] has been defined.  With nothing
   deletable, this is equivalent to RUTHLESS.   --DS, 7/97
*/

static int power_cost;
static int power_prune_mode;  /* either GENTLE or RUTHLESS */
                              /* obviates excessive paramater passing */

int left_connector_count(Disjunct * d) {
/* returns the number of connectors in the left lists of the disjuncts. */
    Connector *c;
    int i=0;
    for (;d!=NULL; d=d->next) {
	for (c = d->left; c!=NULL; c = c->next) i++;
    }
    return i;
}
int right_connector_count(Disjunct * d) {
    Connector *c;
    int i=0;
    for (;d!=NULL; d=d->next) {
      for (c = d->right; c!=NULL; c = c->next) i++;
    }
    return i;
}

#define BAD_WORD (MAX_SENTENCE+1)
   /* the indiction in a word field that this connector cannot
      be used -- is obsolete.
   */

static int l_table2_size[MAX_SENTENCE];  /* the sizes of the hash tables */
static int r_table2_size[MAX_SENTENCE];

static C_list ** l_table2[MAX_SENTENCE];
                 /* the beginnings of the hash tables */
static C_list ** r_table2[MAX_SENTENCE];

void free_C_list(C_list * t) {
    C_list *xt;
    for (; t!=NULL; t=xt) {
	xt = t->next;
	xfree((char *)t, sizeof(C_list));
    }
}

void free_power_tables(Sentence sent) {
/* free all of the hash tables and C_lists */
    int w;
    int i;
    for (w=0; w<sent->length; w++) {
	for (i=0; i<l_table2_size[w]; i++) {
	    free_C_list(l_table2[w][i]);
	}
	xfree((char *)l_table2[w], l_table2_size[w] * sizeof (C_list *));
	for (i=0; i<r_table2_size[w]; i++) {
	    free_C_list(r_table2[w][i]);
	}
	xfree((char *)r_table2[w], r_table2_size[w] * sizeof (C_list *));
    }
}

int power_hash(Connector * c) {
/* This hash function only looks at the leading upper case letters of
   the connector string, and the label fields.  This ensures that if two
   strings match (formally), then they must hash to the same place.
   The answer must be masked to the appropriate table size.

   This is exactly the same hash function used in fast-match.
*/
    char *s;
    int i;
    i = randtable[c->label & (RTSIZE-1)];
    s = c->string;
    while(isupper((int)*s)) {
	i = i + (i<<1) + randtable[((*s) + i) & (RTSIZE-1)];
	s++;
    }
    return i;
}

void put_into_power_table(int size, C_list ** t, Connector * c, int shal ) {
/* The disjunct d (whose left or right pointer points to c) is put
   into the appropriate hash table
*/
    int h;
    C_list * m;
    h = power_hash(c) & (size-1);
    m = (C_list *) xalloc (sizeof(C_list));
    m->next = t[h];
    t[h] = m;
    m->c = c;
    m->shallow = shal;
}

int set_dist_fields(Connector * c, int w, int delta) {
    int i;
    if (c==NULL) return w;
    i = set_dist_fields(c->next, w, delta) + delta;
    c->word = i;
    return i;
}

void init_power(Sentence sent) {
/* allocates and builds the initial power hash tables */
    int w, len, size, i;
    C_list ** t;
    Disjunct * d, * xd, * head;
    Connector * c;

    deletable = sent->deletable;
    effective_dist = sent->effective_dist;

   /* first we initialize the word fields of the connectors, and
      eliminate those disjuncts with illegal connectors */
    for (w=0; w<sent->length; w++) {
      head = NULL;
      for (d=sent->word[w].d; d!=NULL; d=xd) {
          xd = d->next;
          if ((set_dist_fields(d->left, w, -1) < 0) ||
              (set_dist_fields(d->right, w, 1) >= sent->length)) {
              d->next = NULL;
              free_disjuncts(d);
          } else {
              d->next = head;
              head = d;
          }
      }
      sent->word[w].d = head;
    }

    for (w=0; w<sent->length; w++) {
	len = left_connector_count(sent->word[w].d);
	size = next_power_of_two_up(len);
	l_table2_size[w] = size;
	t = l_table2[w] = (C_list **) xalloc(size * sizeof(C_list *));
	for (i=0; i<size; i++) t[i] = NULL;

	for (d=sent->word[w].d; d!=NULL; d=d->next) {
	    c = d->left;
	    if (c != NULL) {
		put_into_power_table(size, t, c, TRUE);
		for (c=c->next; c!=NULL; c=c->next){
		    put_into_power_table(size, t, c, FALSE);
		}
	    }
	}

	len = right_connector_count(sent->word[w].d);
	size = next_power_of_two_up(len);
	r_table2_size[w] = size;
	t = r_table2[w] = (C_list **) xalloc(size * sizeof(C_list *));
	for (i=0; i<size; i++) t[i] = NULL;

	for (d=sent->word[w].d; d!=NULL; d=d->next) {
	    c = d->right;
	    if (c != NULL) {
		put_into_power_table(size, t, c, TRUE);
		for (c=c->next; c!=NULL; c=c->next){
		    put_into_power_table(size, t, c, FALSE);
		}
	    }
	}
    }
}

void clean_table(int size, C_list ** t) {
/* This runs through all the connectors in this table, and eliminates those
   who are obsolete.  The word fields of an obsolete one has been set to
   BAD_WORD.
*/
    int i;
    C_list * m, * xm, * head;
    for (i=0; i<size; i++) {
	head = NULL;
	for (m=t[i]; m!=NULL; m=xm) {
	    xm = m->next;
	    if (m->c->word != BAD_WORD) {
		m->next = head;
		head = m;
	    } else {
		xfree((char *) m, sizeof(C_list));
	    }
	}
	t[i] = head;
    }
}

int possible_connection(Connector *lc, Connector *rc,
                      int lshallow, int rshallow,
                      int lword, int rword) {
/* this takes two connectors (and whether these are shallow or not)
   (and the two words that these came from) and returns TRUE if it is
   possible for these two to match based on local considerations.
*/
    if ((!lshallow) && (!rshallow)) return FALSE;
      /* two deep connectors can't work */
    if ((lc->word > rword) || (rc->word < lword)) return FALSE;
      /* word range constraints */

    /* Now, notice that the only differences between the following two
       cases is that (1) ruthless uses match and gentle uses prune_match.
       and (2) ruthless doesn't use deletable[][].  This latter fact is
       irrelevant, since deletable[][] is now guaranteed to have been
       created. */

    if (power_prune_mode == RUTHLESS) {
	if (lword == rword-1) {
	    if (!((lc->next == NULL) && (rc->next == NULL))) return FALSE;
	} else {
	    if ((!null_links) &&
		(lc->next == NULL) && (rc->next == NULL) && (!lc->multi) && (!rc->multi)) {
		return FALSE;
	    }
	}
	return match(lc, rc, lword, rword);
    } else {
	if (lword == rword-1) {
	    if (!((lc->next == NULL) && (rc->next == NULL))) return FALSE;
	} else {
	    if ((!null_links) &&
		(lc->next == NULL) && (rc->next == NULL) && (!lc->multi) && (!rc->multi) &&
		!deletable[lword][rword]) {
		return FALSE;
	    }
	}
	return prune_match(lc, rc, lword, rword);
    }
}


int right_table_search(int w, Connector *c, int shallow, int word_c) {
/* this returns TRUE if the right table of word w contains
   a connector that can match to c.  shallow tells if c is shallow */
    int size, h;
    C_list *cl;
    size = r_table2_size[w];
    h = power_hash(c) & (size-1);
    for (cl = r_table2[w][h]; cl!=NULL; cl = cl->next) {
        if (possible_connection(cl->c, c, cl->shallow, shallow, w, word_c)) {
            return TRUE;
        }
    }
    return FALSE;
}

int left_table_search(int w, Connector *c, int shallow, int word_c) {
/* this returns TRUE if the right table of word w contains
   a connector that can match to c.  shallows tells if c is shallow
*/
    int size, h;
    C_list *cl;
    size = l_table2_size[w];
    h = power_hash(c) & (size-1);
    for (cl = l_table2[w][h]; cl!=NULL; cl = cl->next) {
      if (possible_connection(c, cl->c, shallow, cl->shallow, word_c, w)) {
          return TRUE;
      }
    }
    return FALSE;
}

static int N_changed;   /* counts the number of changes
			   of c->word fields in a pass */

int ok_cwords(Sentence sent, Connector *c){
    for (; c != NULL; c=c->next) {
	if (c->word == BAD_WORD) return FALSE;
	if (c->word >= sent->length) return FALSE;
    }
    return TRUE;
}

int left_connector_list_update(Connector *c, int word_c, int w, int shallow) {
/* take this connector list, and try to match it with the words
   w-1, w-2, w-3...Returns the word to which the first connector of the
   list could possibly be matched.  If c is NULL, returns w.  If there
   is no way to match this list, it returns a negative number.
   If it does find a way to match it, it updates the c->word fields
   correctly.
*/
    int n;
    int foundmatch;

    if (c==NULL) return w;
    n = left_connector_list_update(c->next, word_c, w, FALSE) - 1;
    if (((int) c->word) < n) n = c->word;

    /* n is now the rightmost word we need to check */
    foundmatch = FALSE;
    for (; (n >= 0) && ((w-n) <= MAX_SENTENCE); n--) {
	power_cost++;
	if (right_table_search(n, c, shallow, word_c)) {
	    foundmatch = TRUE;
	    break;
	}
    }
    if (n < ((int) c->word)) {
	c->word = n;
	N_changed++;
    }
    return (foundmatch ? n : -1);
}

int right_connector_list_update(Sentence sent, Connector *c, int word_c, int w, int shallow) {
/* take this connector list, and try to match it with the words
   w+1, w+2, w+3...Returns the word to which the first connector of the
   list could possibly be matched.  If c is NULL, returns w.  If there
   is no way to match this list, it returns a number greater than N_words-1
   If it does find a way to match it, it updates the c->word fields
   correctly.
*/
    int n;
    int foundmatch;

    if (c==NULL) return w;
    n = right_connector_list_update(sent, c->next, word_c, w, FALSE) + 1;
    if (c->word > n) n = c->word;

    /* n is now the leftmost word we need to check */
    foundmatch = FALSE;
    for (; (n < sent->length) && ((n-w) <= MAX_SENTENCE); n++) {
	power_cost++;
	if (left_table_search(n, c, shallow, word_c)) {
	    foundmatch = TRUE;
	    break;
	}
    }
    if (n > c->word) {
	c->word = n;
	N_changed++;
    }
    return (foundmatch ? n : sent->length);
}

int power_prune(Sentence sent, int mode, Parse_Options opts) {
/* The return value is the number of disjuncts deleted */
    Disjunct *d, *free_later, *dx, *nd;
    Connector *c;
    int w, N_deleted, total_deleted;

    power_prune_mode = mode; /* this global variable avoids lots of
				parameter passing */
    null_links = (opts->min_null_count > 0);
    count_set_effective_distance(sent);

    init_power(sent);
    power_cost = 0;
    free_later = NULL;
    N_changed = 1;  /* forces it always to make at least two passes */
    N_deleted = 0;

    total_deleted = 0;

    while (!parse_options_resources_exhausted(opts)) {
	/* left-to-right pass */
	
	for (w = 0; (w < sent->length) && (!parse_options_resources_exhausted(opts)); w++) {
	    if (parse_options_resources_exhausted(opts)) break;
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		if (d->left == NULL) continue;
		if (left_connector_list_update(d->left, w, w, TRUE) < 0) {
		    for (c=d->left  ;c!=NULL; c = c->next) c->word = BAD_WORD;
		    for (c=d->right ;c!=NULL; c = c->next) c->word = BAD_WORD;
		    N_deleted++;
		    total_deleted++;
		}
	    }
	    
	    clean_table(r_table2_size[w], r_table2[w]);
	    nd = NULL;
	    for (d = sent->word[w].d; d != NULL; d = dx) {
		dx = d->next;
		if ((d->left != NULL) && (d->left->word == BAD_WORD)) {
		    d->next = free_later;
		    free_later = d;
		} else {
		    d->next = nd;
		    nd = d;
		}
	    }
	    sent->word[w].d = nd;
	}
	if (verbosity > 2) {
	   printf("l->r pass changed %d and deleted %d\n",N_changed,N_deleted);
	}

	if (N_changed == 0) break;
	
	N_changed = N_deleted = 0;
	/* right-to-left pass */
	
	for (w = sent->length-1; (w >= 0) && (!parse_options_resources_exhausted(opts)); w--) {
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		if (d->right == NULL) continue;
		if (right_connector_list_update(sent, d->right,w,w,TRUE) >= sent->length){
		    for (c=d->right;c!=NULL; c = c->next) c->word = BAD_WORD;
		    for (c=d->left ;c!=NULL; c = c->next) c->word = BAD_WORD;
		    N_deleted++;
		    total_deleted++;
		}
	    }
	    clean_table(l_table2_size[w], l_table2[w]);
	    nd = NULL;
	    for (d = sent->word[w].d; d != NULL; d = dx) {
		dx = d->next;
		if ((d->right != NULL) && (d->right->word == BAD_WORD)) {
		    d->next = free_later;
		    free_later = d;
		} else {
		    d->next = nd;
		    nd = d;
		}
	    }
	    sent->word[w].d = nd;
	}
	
	if (verbosity > 2) {
	   printf("r->l pass changed %d and deleted %d\n",N_changed,N_deleted);
	}

	if (N_changed == 0) break;
	N_changed = N_deleted = 0;
    }
    free_disjuncts(free_later);
    free_power_tables(sent);
    if (verbosity > 2) printf("%d power prune cost:\n", power_cost);

    if (mode == RUTHLESS) {
	print_time(opts, "power pruned (ruthless)");
    } else {
	print_time(opts, "power pruned (gentle)");
    }

    if (verbosity > 2) {
	if (mode == RUTHLESS) {
	    printf("\nAfter power_pruning (ruthless):\n");
	} else {
	    printf("\nAfter power_pruning (gentle):\n");
	}
	print_disjunct_counts(sent);
    }

    return total_deleted;
}

/*
   PP Pruning

   The "contains one" post-processing constraints give us a new way to
   prune.  Suppose there's a rule that says "a group that contains foo
   must contain a bar or a baz."  Here foo, bar, and baz are connector
   types.  foo is the trigger link, bar and baz are called the criterion
   links.  If, after considering the disjuncts we find that there is is
   a foo, but neither a bar, nor a baz, then we can eliminte the disjuct
   containing bar.

   Things are actually a bit more complex, because of the matching rules
   and subscripts.  The problem is that post-processing deals with link
   names, while at this point all we have to work with is connector
   names.  Consider the foo part.  Consider a connector C.  When does
   foo match C for our purposes?  It matches it if every possible link
   name L (that can result from C being at one end of that link) results
   in post_process_match(foo,L) being true.  Suppose foo contains a "*".
   Then there is no C that has this property.  This is because the *s in
   C may be replaced by some other subscripts in the construction of L.
   And the non-* chars in L will not post_process_match the * in foo.

   So let's assume that foo has no *.  Now the result we want is simply
   given by post_process_match(foo, C).  Proof: L is the same as C but
   with some *s replaced by some other letters.  Since foo contains no *
   the replacement in C of some * by some other letter could change
   post_process_match from FALSE to TRUE, but not vice versa.  Therefore
   it's conservative to use this test.

   For the criterion parts, we need to determine if there is a
   collection of connectors C1, C2,... such that by combining them you
   can get a link name L that post_process_matches bar or baz.  Here's a
   way to do this.  Say bar="Xabc".  Then we see if there are connector
   names that post_process_match "Xa##", "X#b#", and "X##c".  They must
   all be there in order for it to be possible to create a link name
   "Xabc".  A "*" in the criterion part is a little different.  In this
   case we can simply skip the * (treat it like an upper case letter)
   for this purpose.  So if bar="X*ab" then we look for "X*#b" and
   "X*a#".  (The * in this case could be treated the same as another
   subscript without breaking it.)  Note also that it's only necessary
   to find a way to match one of the many criterion links that may be in
   the rule.  If none can match, then we can delete the disjunct
   containing C.

   Here's how we're going to implement this.  We'll maintain a multiset
   of connector names.  We'll represent them in a hash table, where the
   hash function uses only the upper case letters of the connector name.
   We'll insert all the connectors into the multiset.  The multiset will
   support the operation of deletion (probably simplest to just
   decrement the count).  Here's the algorithm.

   Insert all the connectors into M.

   While the previous pass caused a count to go to 0 do:
      For each connector C do
         For each rule R do
            if C is a trigger for R and the criterion links
	    of the rule cannot be satisfied by the connectors in
	    M, Then:
               We delete C's disjunct.  But before we do, 
               we remove all the connectors of this disjunct
	       from the multiset.  Keep tabs on whether or not
	       any of the counts went to 0.



    Efficiency hacks to be added later:
        Note for a given rule can become less and less satisfiable.
        That is, rule_satisfiable(r) for a given rule r can change from
        TRUE to FALSE, but not vice versa.  So once it's FALSE, we can just
	remember that.

        Consider the effect of a pass p on the set of rules that are
	satisfiable.  Suppose this set does not change.  Then pass p+1
	will do nothing.  This is true even if pass p caused some
	disjuncts to be deleted.  (This observation will only obviate
	the need for the last pass.)

  */

typedef struct cms_struct Cms;
struct cms_struct {
    Cms * next;
    char * name;
    int count;      /* the number of times this is in the multiset */
};

#define CMS_SIZE (2<<10)
static Cms * cms_table[CMS_SIZE];

void init_cms_table(void) {
    int i;
    for (i=0; i<CMS_SIZE; i++) {
	cms_table[i] = NULL;
    }
}

void free_cms_table(void) {
    Cms * cms, *xcms;
    int i;
    for (i=0; i<CMS_SIZE; i++) {
	for (cms = cms_table[i]; cms!=NULL; cms=xcms) {
	    xcms = cms->next;
	    xfree(cms, sizeof(Cms));
	}	
    }
}

int cms_hash(char * s) {
    int i=0;
    while(isupper((int)*s)) {
	i = i + (i<<1) + randtable[(*s + i) & (RTSIZE-1)];
	s++;
    }
    return (i & (CMS_SIZE-1));
}

int match_in_cms_table(char * pp_match_name) {
    /* This returns TRUE if there is a connector name C in the table
       such that post_process_match(pp_match_name, C) is TRUE */
    Cms * cms;
    for (cms = cms_table[cms_hash(pp_match_name)]; cms!=NULL; cms=cms->next) {
	if(post_process_match(pp_match_name, cms->name)) return TRUE;
    }
    return FALSE;
}

Cms * lookup_in_cms_table(char * str) {
    Cms * cms;
    for (cms = cms_table[cms_hash(str)]; cms!=NULL; cms=cms->next) {
	if(strcmp(str, cms->name) == 0) return cms;
    }
    return NULL;
}

/*  This is not used currently */
/*
int is_in_cms_table(char * str) {
    Cms * cms = lookup_in_cms_table(str);
    return (cms != NULL && cms->count > 0);
}
*/

void insert_in_cms_table(char * str) {
    Cms * cms;
    int h;
    cms = lookup_in_cms_table(str);
    if (cms != NULL) {
	cms->count++;
    } else {
	cms = (Cms *) xalloc(sizeof(Cms));
	cms->name = str;  /* don't copy the string...just keep a pointer to it.
			     we won't free these later */
	cms->count = 1;
	h = cms_hash(str);
	cms->next = cms_table[h];
	cms_table[h] = cms;
    }
}

int delete_from_cms_table(char * str) {
    /* Delete the given string from the table.  Return TRUE if
       this caused a count to go to 0, return FALSE otherwise */
    Cms * cms;
    cms = lookup_in_cms_table(str);
    if (cms != NULL && cms->count > 0) {
	cms->count--;
	return (cms->count == 0);
    }
    return FALSE;
}

int rule_satisfiable(pp_linkset *ls) {
    int hashval;
    char name[20], *s, *t;
    pp_linkset_node *p;
    int bad, n_subscripts;
    for (hashval = 0; hashval < ls->hash_table_size; hashval++) {
	for (p = ls->hash_table[hashval]; p!=NULL; p=p->next) {

	    /* ok, we've got our hands on one of the criterion links */
	    strncpy(name, p->str, sizeof(name)-1);
	    /* could actually use the string in place because we change it back */
	    name[sizeof(name)-1] = '\0';
	    /* now we want to see if we can satisfy this criterion link */
	    /* with a collection of the links in the cms table */

	    for (s = name; isupper((int)*s); s++);
	    for (;*s != '\0'; s++) if (*s != '*') *s = '#';
	    for (s = name, t = p->str; isupper((int) *s); s++, t++);

	    /* s and t remain in lockstep */
	    bad = 0;
	    n_subscripts = 0;
	    for (;*s != '\0' && bad==0; s++, t++) {
		if (*s == '*') continue;
		n_subscripts++;
		/* after the upper case part, and is not a * so must be a regular subscript */
		*s = *t;
		if (!match_in_cms_table(name)) bad++;
		*s = '#';
	    }

	    if (n_subscripts == 0) {
		/* now we handle the special case which occurs if there
		   were 0 subscripts */
		if (!match_in_cms_table(name)) bad++;		
	    }
	    
	    /* now if bad==0 this criterion link does the job
	       to satisfy the needs of the trigger link */

	    if (bad == 0) return TRUE;
	}
    }
    return FALSE;
}

int pp_prune(Sentence sent, Parse_Options opts) {
    pp_knowledge * knowledge;
    pp_rule rule;
    char * selector;
    pp_linkset * link_set;
    int i, w, dir;
    Disjunct *d;
    Connector *c;
    int change, total_deleted, N_deleted, deleteme;

    if (sent->dict->postprocessor == NULL) return 0;

    knowledge = sent->dict->postprocessor->knowledge;

    init_cms_table();

    for (w = 0; w < sent->length; w++) {
	for (d = sent->word[w].d; d != NULL; d = d->next) {
	    d->marked = TRUE;
	    for (dir=0; dir < 2; dir++) {
		for (c = ( (dir)?(d->left):(d->right) ); c!=NULL; c=c->next) {
		    insert_in_cms_table(c->string);
		}
	    }
	}
    }

    total_deleted = 0;
    change = 1;
    while (change > 0) {
	change = 0;
	N_deleted = 0;
	for (w = 0; w < sent->length; w++) {
	    for (d = sent->word[w].d; d != NULL; d = d->next) {
		if (!d->marked) continue;
		deleteme = FALSE;
		for (dir=0; dir < 2; dir++) {
		    for (c = ( (dir)?(d->left):(d->right) ); c!=NULL; c=c->next) {
			for (i=0; i<knowledge->n_contains_one_rules; i++) {

			    rule = knowledge->contains_one_rules[i]; /* the ith rule */
			    selector = rule.selector;                /* selector string for this rule */
			    link_set = rule.link_set;                /* the set of criterion links */

			    if (strchr(selector, '*') != NULL) continue;  /* If it has a * forget it */

			    if (!post_process_match(selector, c->string)) continue;

			    /*
			    printf("pp_prune: trigger ok.  selector = %s  c->string = %s\n", selector, c->string);
			    */

			    /* We know c matches the trigger link of the rule. */
			    /* Now check the criterion links */

			    if (!rule_satisfiable(link_set)) {
				deleteme = TRUE;
			    }
			    if (deleteme) break;
			}
			if (deleteme) break;
		    }
		    if (deleteme) break;
		}

		if (deleteme) {		    /* now we delete this disjunct */
		    N_deleted++;
		    total_deleted++;
		    d->marked = FALSE; /* mark for deletion later */
		    for (dir=0; dir < 2; dir++) {
			for (c = ( (dir)?(d->left):(d->right) ); c!=NULL; c=c->next) {
			    change += delete_from_cms_table(c->string);
			}
		    }
		}
	    }
	}
	
	if (verbosity > 2) {
	   printf("pp_prune pass deleted %d\n", N_deleted);
	}

    }
    delete_unmarked_disjuncts(sent);
    free_cms_table();

    if (verbosity > 2) {
	printf("\nAfter pp_pruning:\n");
	print_disjunct_counts(sent);
    }

    print_time(opts, "pp pruning");

    return total_deleted;
}


void pp_and_power_prune(Sentence sent, int mode, Parse_Options opts) {
    /* do the following pruning steps until nothing happens:
       power pp power pp power pp....
       Make sure you do them both at least once.
       */
    power_prune(sent, mode, opts);

    for (;;) {
	if (parse_options_resources_exhausted(opts)) break;
	if (pp_prune(sent, opts) == 0) break;
	if (parse_options_resources_exhausted(opts)) break;
	if (power_prune(sent, mode, opts) == 0) break;
    }

}

/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/*
  The dictionary format:

  In what follows:
    Every "%" symbol and everything after it is ignored on every line. 
    Every newline or tab is replaced by a space.

  The dictionary file is a sequence of ENTRIES.  Each ENTRY is one or
  more WORDS (a sequence of upper or lower case letters) separated by
  spaces, followed by a ":", followed by an EXPRESSION followed by a
  ";".  An EXPRESSION is a lisp expression where the functions are "&"
  or "and" or "|" or "or", and there are three types of parentheses:
  "()", "{}", and "[]".  The terminal symbols of this grammar are the
  connectors, which are strings of letters or numbers or *s.  (This
  description applies to the prefix form of the dictionary.  the current
  dictionary is written in infix form.  If the defined constant
  INFIX_NOTATION is defined, then infix is used otherwise prefix is used.)

  The connector begins with an optinal @, which is followed by an upper
  case sequence of letters. Each subsequent *, lower case letter or
  number is a subscript. At the end is a + or - sign.  The "@" allows
  this connector to attach to one or more other connectors.

  Here is a sample dictionary entry (in infix form):

      gone:         T- & {@EV+};

  (See our paper for more about how to interpret the meaning of the
  dictionary expressions.)

  A previously defined word (such as "gone" above) may be used instead
  of a connector to specify the expression it was defined to be.  Of
  course, in this case, it must uniquely specify a word in the
  dictionary, and have been previously defined.

  If a word is of the form "/foo", then the file current-dir/foo
  is a so-called word file, and is read in as a list of words.
  A word file is just a list of words separted by blanks or newlines.

  A word that contains the character "_" defines an idiomatic use of
  the words separated by the "_".  For example "kind of" is an idiomatic
  expression, so a word "kind_of" is defined in the dictionary.
  Idomatic expressions of any number of words can be defined in this way.
  When the word "kind" is encountered, all the idiomatic uses of the word
  are considered.

  An expresion enclosed in "[..]" is give a cost of 1.  This means
  that if any of the connectors inside the square braces are used,
  a cost of 1 is incurred.  (This cost is the first element of the cost
  vector printed when a sentence is parsed.)  Of course if something is
  inside of 10 levels of "[..]" then using it incurs a cost of 10.
  These costs are called "disjunct costs".  The linkages are printed out
  in order of non-increasing disjunct cost.

  The expression "(A+ or ())" means that you can choose either "A+" or
  the empty expression "()", that is, that the connector "A+" is
  optional.  This is more compactly expressed as "{A+}".  In other words,
  curly braces indicate an optional expression.

  The expression "(A+ or [])" is the same as that above, but there is a
  cost of 1 incurred for choosing not to use "A+".  The expression
  "(EXP1 & [EXP2])" is exactly the same as "[EXP1 & EXP2]".  The difference
  between "({[A+]} & B+)" and "([{A+}] & B+)" is that the latter always
  incurrs a cost of 1, while the former only gets a cost of 1 if "A+" is
  used.  

  The dictionary writer is not allowed to use connectors that begin in
  "ID".  This is reserved for the connectors automatically
  generated for idioms.

*/

int advance(Dictionary dict);

static void dict_error(Dictionary dict, char * s) {
    int i;
    char tokens[1024], t[128];

    tokens[0] = '\0';
    for (i=0; i<5 && dict->token[0] != '\0' ; i++) {
	sprintf(t, "\"%s\" ", dict->token);
	strcat(tokens, t);
	(void) advance(dict);
    }
    lperror(DICTPARSE, ". %s\n\t line %d, tokens = %s\n", 
	    s, dict->line_number, tokens);
}

void warning(Dictionary dict, char * s) {
    printf("\nWarning: %s\n",s);
    printf("line %d, current token = \"%s\"\n", dict->line_number, dict->token);
}

Exp * Exp_create(Dictionary dict) {
    /* allocate a new Exp node and link it into the
       exp_list for freeing later */
    Exp * e;
    e = (Exp *) xalloc(sizeof(Exp));
    e->next = dict->exp_list;
    dict->exp_list = e;
    return e;
}

int get_character(Dictionary dict, int quote_mode) {
/* This gets the next character from the input, eliminating comments.
   If we're in quote mode, it does not consider the % character for
   comments */
    
    int c;
    
    c = fgetc(dict->fp);
    if ((c == '%') && (!quote_mode)) {
	while((c != EOF) && (c != '\n')) c = fgetc(dict->fp);
    }
    if (c == '\n') dict->line_number++;
    return c;
}


/*
   This set of 10 characters are the ones defining the syntax of the dictionary.
*/
#define SPECIAL "(){};[]&|:"

int advance(Dictionary dict) {
   /* this reads the next token from the input into token */
    int c, i;
    int quote_mode;

    dict->is_special = FALSE;

    if (dict->already_got_it != '\0') {
	dict->is_special = (strchr(SPECIAL, dict->already_got_it) != NULL);
	if (dict->already_got_it == EOF) {
	    dict->token[0] = '\0';
	} else {
	    dict->token[0] = dict->already_got_it;
	    dict->token[1] = '\0';
	}
	dict->already_got_it = '\0';
	return 1;
    }

    do c=get_character(dict, FALSE); while (isspace(c));

    quote_mode = FALSE;

    i = 0;
    for (;;) {
	if (i > MAX_TOKEN_LENGTH-1) {
	    dict_error(dict, "Token too long");
	    return 0;
	}
	if (quote_mode) {
	    if (c == '\"') {
		quote_mode = FALSE;
		dict->token[i] = '\0';
		return 1;
	    }
	    if (isspace(c)) {
		dict_error(dict, "White space inside of token");
		return 0;
	    }	     
	    dict->token[i] = c;
	    i++;
	} else {
	    if (strchr(SPECIAL, c) != NULL) {
		if (i==0) {
		    dict->token[0] = c;
		    dict->token[1] = '\0';
		    dict->is_special = TRUE;
		    return 1;
		}
		dict->token[i] = '\0';
		dict->already_got_it = c;
		return 1;
	    }
	    if (c==EOF) {
		if (i==0) {
		    dict->token[0] = '\0';
		    return 1;
		}
		dict->token[i] = '\0';
		dict->already_got_it = c;
		return 1;
	    }
	    if (isspace(c)) {
		dict->token[i] = '\0';
		return 1;
	    }
	    if (c == '\"') {
		quote_mode = TRUE;
	    } else {
		dict->token[i] = c;
		i++;
	    }
	}
	c = get_character(dict, quote_mode);
    }
    return 1;
}

int is_equal(Dictionary dict, int c) {
/* returns TRUE if this token is a special token and it is equal to c */
    return (dict->is_special && c==dict->token[0] && dict->token[1] == '\0');
}

int check_connector(Dictionary dict, char * s) {
    /* makes sure the string s is a valid connector */
    int i;
    i = strlen(s);
    if (i < 1) {
	dict_error(dict, "Expecting a connector.");
	return 0;
    }
    i = s[i-1];  /* the last character of the token */
    if ((i!='+') && (i!='-')) {
	dict_error(dict, "A connector must end in a \"+\" or \"-\".");
	return 0;
    }
    if (*s == '@') s++;
    if (!isupper((int)*s)) {
	dict_error(dict, "The first letter of a connector must be in [A--Z].");
	return 0;
    }
    if ((*s=='I') && (*(s+1)=='D')) {
	dict_error(dict, "Connectors beginning with \"ID\" are forbidden");
	return 0;
    }
    while (*(s+1)) {
	if ((!isalnum((int)*s)) && (*s != '*') && (*s != '^')) {
	    dict_error(dict, "All letters of a connector must be alpha-numeric.");
	    return 0;
	}
	s++;
    }
    return 1;
}

Exp * make_unary_node(Dictionary dict, Exp * e);

Exp * connector(Dictionary dict) {
/* the current token is a connector (or a dictionary word)           */
/* make a node for it                                                */   
 
    Exp * n;
    Dict_node * dn;
    int i;

    i = strlen(dict->token)-1;  /* this must be + or - if a connector */
    if ((dict->token[i] != '+') && (dict->token[i] != '-')) {
	dn = abridged_lookup(dict, dict->token);
	while((dn != NULL) && (strcmp(dn->string, dict->token) != 0)) {
	    dn = dn->right;
	}
	if (dn == NULL) {
	    
	    dict_error(dict, "\nPerhaps missing + or - in a connector.\n"
		             "Or perhaps you forgot the suffix on a word.\n"
		             "Or perhaps a word is used before it is defined.\n");
	    return NULL;
	}
	n = make_unary_node(dict, dn->exp);
    } else {
	if (!check_connector(dict, dict->token)) {
	    return NULL;
	}
	n = Exp_create(dict);
	n->dir = dict->token[i];
	dict->token[i] = '\0';                   /* get rid of the + or - */
	if (dict->token[0] == '@') {
	    n->u.string = string_set_add(dict->token+1, dict->string_set);
	    n->multi = TRUE;
	} else {
	    n->u.string = string_set_add(dict->token, dict->string_set);
	    n->multi = FALSE;
	}
	n->type = CONNECTOR_type;		
	n->cost = 0;
    }
    if (!advance(dict)) {
	return NULL;
    }
    return n;
}

Exp * make_unary_node(Dictionary dict, Exp * e) {
/* This creates a node with one child (namely e).  Initializes  */
/* the cost to zero                                      */
    Exp * n;
    n = Exp_create(dict);
    n->type = AND_type;  /* these must be AND types */
    n->cost = 0;
    n->u.l = (E_list *) xalloc(sizeof(E_list));
    n->u.l->next = NULL;
    n->u.l->e = e;
    return n;
}

Exp * make_zeroary_node(Dictionary dict) {
/* This creates a node with zero children.  Initializes  */
/* the cost to zero                                      */
    Exp * n;
    n = Exp_create(dict);
    n->type = AND_type;  /* these must be AND types */
    n->cost = 0;
    n->u.l = NULL;
    return n;
}

Exp * make_optional_node(Dictionary dict, Exp * e) {
/* This creates an OR node with two children, one the given node,
   and the other as zeroary node.  This has the effect of creating
   what used to be called an optional node.
*/
    Exp * n;
    E_list *el, *elx;
    n = Exp_create(dict);
    n->type = OR_type; 
    n->cost = 0;
    n->u.l = el = (E_list *) xalloc(sizeof(E_list));
    el->e = make_zeroary_node(dict);
    el->next = elx = (E_list *) xalloc(sizeof(E_list));
    elx->next = NULL;
    elx->e = e;
    return n;
}

Exp * expression(Dictionary dict);

#if ! defined INFIX_NOTATION

Exp * operator_exp(Dictionary dict, int type) {
/* We're looking at the first of the stuff after an "and" or "or".     */
/* Build a Exp node for this expression.  Set the cost and optional    */
/* fields to the default values.  Set the type field according to type */
    Exp * n;
    E_list first;
    E_list * elist;
    n = Exp_create(dict);
    n->type = type;
    n->cost = 0;
    elist = &first;
    while((!is_equal(dict, ')')) && (!is_equal(dict, ']')) && (!is_equal(dict, '}'))) {
	elist->next = (E_list *) xalloc(sizeof(E_list));
	elist = elist->next;
	elist->next = NULL;
	elist->e = expression(dict);
	if (elist->e == NULL) {
	    return NULL;
	}
    }
    if (elist == &first) {
	dict_error(dict, "An \"or\" or \"and\" of nothing");
	return NULL;
    }
    n->u.l = first.next;
    return n;
}

Exp * in_parens(Dictionary dict) {
/* looks for the stuff that is allowed to be inside of parentheses */
/* either & or | followed by a list, or a terminal symbol          */    
    Exp * e;

    if (is_equal(dict, '&') || (strcmp(token, "and")==0)) {
	if (!advance(dict)) {
	    return NULL;
	}
	return operator_exp(dict, AND_type);
    } else if (is_equal(dict, '|') || (strcmp(dict->token, "or")==0)) {
	if (!advance(dict)) {
	    return NULL;
	}
	return operator_exp(dict, OR_type);
    } else {
	return expression(dict);
    }
}

Exp * expression(Dictionary dict) {
/* Build (and return the root of) the tree for the expression beginning     */
/* with the current token.  At the end, the token is the first one not part */
/* of this expression.                                                      */

    Exp * n;
    if (is_equal(dict, '(')) {
	if (!advance(dict)) {
	    return NULL;
	}
	n = in_parens(dict);
	if (!is_equal(dict, ')')) {
	    dict_error(dict, "Expecting a \")\".");
	    return NULL;
	}
	if (!advance(dict)) {
	    return NULL;
	}
    } else if (is_equal(dict, '{')) {
	if (!advance(dict)) {
	    return NULL;
	}
	n = in_parens(dict);
	if (!is_equal(dict, '}')) {
	    dict_error(dict, "Expecting a \"}\".");
	    return NULL;
	}
	if (!advance(dict)) {
	    return NULL;
	}
	n = make_optional_node(dict, n);
    } else if (is_equal(dict, '[')) {
	if (!advance(dict)) {
	    return NULL;
	}
	n = in_parens(dict);
	if (!is_equal(dict, ']')) {
	    dict_error(dict, "Expecting a \"]\".");
	    return NULL;
	}
	if (!advance(dict)) {
	    return NULL;
	}
	n->cost += 1;
    } else if (!dict->is_special) {
	n = connector(dict);
	if (n == NULL) {
	    return NULL;
	}
    } else if (is_equal(dict, ')') || is_equal(dict, ']')) {    
	/* allows "()" or "[]" */
        n = make_zeroary_node(dict);
    } else {
    	dict_error(dict, "Connector, \"(\", \"[\", or \"{\" expected.");
	return NULL;
    }
    return n;
}

#else

/* This is for infix notation */

Exp * restricted_expression(Dictionary dict, int and_ok, int or_ok);

Exp * expression(Dictionary dict) {
/* Build (and return the root of) the tree for the expression beginning  */
/* with the current token.  At the end, the token is the first one not   */
/* part of this expression.                                              */
    return restricted_expression(dict, TRUE,TRUE);
}

Exp * restricted_expression(Dictionary dict, int and_ok, int or_ok) {
    Exp * nl=NULL, * nr, * n;
    E_list *ell, *elr;
    if (is_equal(dict, '(')) {
	if (!advance(dict)) {
	    return NULL;
	}
	nl = expression(dict);
	if (nl == NULL) {
	    return NULL;
	}
	if (!is_equal(dict, ')')) {
	    dict_error(dict, "Expecting a \")\".");
	    return NULL;
	}
	if (!advance(dict)) {
	    return NULL;
	}
    } else if (is_equal(dict, '{')) {
	if (!advance(dict)) {
	    return NULL;
	}
	nl = expression(dict);
	if (nl == NULL) {
	    return NULL;
	}
	if (!is_equal(dict, '}')) {
	    dict_error(dict, "Expecting a \"}\".");
	    return NULL;
	}
	if (!advance(dict)) {
	    return NULL;
	}
	nl = make_optional_node(dict, nl);
    } else if (is_equal(dict, '[')) {
	if (!advance(dict)) {
	    return NULL;
	}
	nl = expression(dict);
	if (nl == NULL) {
	    return NULL;
	}
	if (!is_equal(dict, ']')) {
	    dict_error(dict, "Expecting a \"]\".");
	    return NULL;
	}
	if (!advance(dict)) {
	    return NULL;
	}
	nl->cost += 1;
    } else if (!dict->is_special) {
	nl = connector(dict);
	if (nl == NULL) {
	    return NULL;
	}
    } else if (is_equal(dict, ')') || is_equal(dict, ']')) {    
	/* allows "()" or "[]" */
        nl = make_zeroary_node(dict);
    } else {
    	dict_error(dict, "Connector, \"(\", \"[\", or \"{\" expected.");
	return NULL;
    }
    
    if (is_equal(dict, '&') || (strcmp(dict->token, "and")==0)) {
	if (!and_ok) {
	    warning(dict, "\"and\" and \"or\" at the same level in an expression");
	}
	if (!advance(dict)) {
	    return NULL;
	}
	nr = restricted_expression(dict, TRUE,FALSE);
	if (nr == NULL) {
	    return NULL;
	}
	n = Exp_create(dict);
	n->u.l = ell = (E_list *) xalloc(sizeof(E_list));
	ell->next = elr = (E_list *) xalloc(sizeof(E_list));
	elr->next = NULL;
	
	ell->e = nl;
	elr->e = nr;
	n->type = AND_type;
	n->cost = 0;
    } else if (is_equal(dict, '|') || (strcmp(dict->token, "or")==0)) {
	if (!or_ok) {
	    warning(dict, "\"and\" and \"or\" at the same level in an expression");
	}
	if (!advance(dict)) {
	    return NULL;
	}
	nr = restricted_expression(dict, FALSE,TRUE);
	if (nr == NULL) {
	    return NULL;
	}
	n = Exp_create(dict);
	n->u.l = ell = (E_list *) xalloc(sizeof(E_list));
	ell->next = elr = (E_list *) xalloc(sizeof(E_list));
	elr->next = NULL;

	ell->e = nl;
	elr->e = nr;
	n->type = OR_type;
	n->cost = 0;
    } else return nl;
    return n;
}

#endif

/* The data structure storing the dictionary is simply a binary tree.  */
/* There is one catch however.  The ordering of the words is not       */
/* exactly the order given by strcmp.  It was necessary to             */
/* modify the order to make it so that "make" < "make.n" < "make-up"   */
/* The problem is that if some other string could  lie between '\0'    */
/* and '.' (such as '-' which strcmp would give) then it makes it much */
/* harder to return all the strings that match a given word.           */
/* For example, if "make-up" was inserted, then "make" was inserted    */
/* the a search was done for "make.n", the obvious algorithm would     */
/* not find the match.                                                 */

/* verbose version */
/*
int dict_compare(char *s, char *t) {
    int ss, tt;
    while (*s != '\0' && *s == *t) {
	s++;
	t++;
    }
    if (*s == '.') {
	ss = 1;
    } else {
	ss = (*s)<<1;
    }
    if (*t == '.') {
	tt = 1;
    } else {
	tt = (*t)<<1;
    }
    return (ss - tt);
}
*/

/* terse version */
int dict_compare(char *s, char *t) {
    while (*s != '\0' && *s == *t) {s++; t++;}
    return (((*s == '.')?(1):((*s)<<1))  -  ((*t == '.')?(1):((*t)<<1)));
}
    
Dict_node * insert_dict(Dictionary dict, Dict_node * n, Dict_node * new) {
/* Insert the new node into the dictionary below node n                  */
/* give error message if the new element's string is already there       */
/* assumes that the "n" field of new is already set, and the left        */
/* and right fields of it are NULL                                       */
    int comp;
    char t[128];
    
    if (n == NULL) return new;
    comp = dict_compare(new->string, n->string);
    if (comp < 0) {
        n->left = insert_dict(dict, n->left, new);
    } else if (comp > 0) {
        n->right = insert_dict(dict, n->right, new);
    } else {
        sprintf(t, "The word \"%s\" has been multiply defined\n", new->string);
	dict_error(dict, t);
	return NULL;
    }
    return n;
}

void insert_list(Dictionary dict, Dict_node * p, int l) {
/* p points to a list of dict_nodes connected by their left pointers   */
/* l is the length of this list (the last ptr may not be NULL)         */
/* It inserts the list into the dictionary.                            */
/* It does the middle one first, then the left half, then the right.   */

    Dict_node * dn, *dnx, *dn_second_half;
    int k, i; /* length of first half */

    if (l == 0) return;

    k = (l-1)/2;
    dn = p;
    for (i=0; i<k; i++) {
	dn = dn->left;
    }
    /* dn now points to the middle element */
    dn_second_half = dn->left;
    dn->left = dn->right = NULL;
    if (contains_underbar(dn->string)) {
	insert_idiom(dict, dn);
    } else if (is_idiom_word(dn->string)) {
	printf("*** Word \"%s\" found near line %d.\n", dn->string, dict->line_number);
	printf("    Words ending \".Ix\" (x a number) are reserved for idioms.\n");
	printf("    This word will be ignored.\n");
	xfree((char *)dn, sizeof(Dict_node));	    
    } else if ((dnx = abridged_lookup(dict, dn->string))!= NULL) {
	printf("*** The word \"%s\"", dn->string);
	printf(" found near line %d matches the following words:\n",
	       dict->line_number);
	for (;dnx != NULL; dnx = dnx->right) {
	    printf(" %s", dnx->string);
	}
	printf("\n    This word will be ignored.\n");
	xfree((char *)dn, sizeof(Dict_node));	    
    } else {
	dict->root = insert_dict(dict, dict->root, dn);
	dict->num_entries++;
    }

    insert_list(dict, p, k);
    insert_list(dict, dn_second_half, l-k-1);
}

int read_entry(Dictionary dict) {
/* Starting with the current token parse one dictionary entry.         */
/* Add these words to the dictionary                                   */
    Exp *n;
    int i;

    Dict_node  *dn_new, *dnx, *dn = NULL;

    for (; !is_equal(dict, ':') ; advance(dict)) {
	  if (dict->is_special) {
	      dict_error(dict, "I expected a word but didn\'t get it.");
	      return 0;
	  }
	  if (dict->token[0] == '/') {
	      /* it's a word-file name */
	      dn = read_word_file(dict, dn, dict->token);
	      if (dn == NULL) {
		  lperror(WORDFILE, " %s\n", dict->token);
		  return 0;
	      }
	  } else {
	      dn_new = (Dict_node *) xalloc(sizeof(Dict_node));
	      dn_new->left = dn;
	      dn = dn_new;
	      dn->file = NULL;
	      dn->string = string_set_add(dict->token, dict->string_set);
	  }
    }
    if (!advance(dict)) {  /* pass the : */
	return 0;
    }
    n = expression(dict);
    if (n == NULL) {
	return 0;
    }
    if (!is_equal(dict, ';')) {
	dict_error(dict, "Expecting \";\" at the end of an entry.");
	return 0;
    }
    if (!advance(dict)) {  /* pass the ; */
	return 0;
    }

    /* at this point, dn points to a list of Dict_nodes connected by      */
    /* their left pointers.  These are to be inserted into the dictionary */
    i = 0;
    for (dnx = dn; dnx != NULL; dnx = dnx->left) {
	dnx->exp = n;
	i++;
    }
    insert_list(dict, dn, i);
    return 1;
}

void print_expression(Exp * n) {
    E_list * el; int i;
    if (n == NULL) {
	printf("NULL expression");
	return;
    }
    if (n->type == CONNECTOR_type) {
	for (i=0; i<n->cost; i++) printf("[");
	printf("%s%c",n->u.string, n->dir);
	for (i=0; i<n->cost; i++) printf("] ");
    } else {
	for (i=0; i<n->cost; i++) printf("[");
	if (n->cost == 0) printf("(");
	if (n->type == AND_type) printf("& ");
	if (n->type == OR_type) printf("or ");
	for (el = n->u.l; el != NULL; el = el->next) {
	    print_expression(el->e);
	}
	for (i=0; i<n->cost; i++) printf("] ");
	if (n->cost == 0) printf(") ");
    }
}

void rprint_dictionary_data(Dict_node * n) {
	if (n==NULL) return;
	rprint_dictionary_data(n->left);
	printf("%s: ", n->string);
	print_expression(n->exp);
	printf("\n");
	rprint_dictionary_data(n->right);
}

void print_dictionary_data(Dictionary dict) {
    rprint_dictionary_data(dict->root);
}


int read_dictionary(Dictionary dict) {
    lperrno = 0;
    if (!advance(dict)) {
	fclose(dict->fp);
	return 0;
    }
    while(dict->token[0] != '\0') {
    	if (!read_entry(dict)) {
	    fclose(dict->fp);
	    return 0;
	}
    }
    fclose(dict->fp);
    return 1;
}

int dict_match(char * s, char * t) {
/* assuming that s is a pointer to a dictionary string, and that
   t is a pointer to a search string, this returns 0 if they
   match, >0 if s>t, and <0 if s<t.
   
   The matching is done as follows.  Walk down the strings until
   you come to the end of one of them, or until you find unequal
   characters.  A "*" matches anything.  Otherwise, replace "."
   by "\0", and take the difference.  This behavior matches that
   of the function dict_compare().
*/
    while((*s != '\0') && (*s == *t)) {s++; t++;}
    if ((*s == '*') || (*t == '*')) return 0;
    return (((*s == '.')?('\0'):(*s))  -  ((*t == '.')?('\0'):(*t)));
}

/* We need to prune out the lists thus generated.               */
/* A sub string will only be considered a subscript if it       */
/* followes the last "." in the word, and it does not begin     */
/* with a digit.                                                */

int true_dict_match(char * s, char * t) {
    char *ds, *dt;
    ds = strrchr(s, '.');
    dt = strrchr(t, '.');

    /* a dot at the end or a dot followed by a number is NOT considered a subscript */
    if ((dt != NULL) && ((*(dt+1) == '\0') || (isdigit((int)*(dt+1))))) dt = NULL;
    if ((ds != NULL) && ((*(ds+1) == '\0') || (isdigit((int)*(ds+1))))) ds = NULL;

    if (dt == NULL && ds != NULL) {
	if (((int)strlen(t)) > (ds-s)) return FALSE;   /* we need to do this to ensure that */
	return (strncmp(s, t, ds-s) == 0);      /*"i.e." does not match "i.e" */
    } else if (dt != NULL && ds == NULL) {
	if (((int)strlen(s)) > (dt-t)) return FALSE;   
	return (strncmp(s, t, dt-t) == 0);
    } else {
	return (strcmp(s, t) == 0);	
    }
}

static Dict_node * lookup_list = NULL;
          /* a pointer to the temporary lookup list */

void prune_lookup_list(char * s) {
    Dict_node *dn, *dnx, *dn_new;
    dn_new = NULL;
    for (dn = lookup_list; dn!=NULL; dn = dnx) {
	dnx = dn->right;
	/* now put dn onto the answer list, or free it */
	if (true_dict_match(dn->string, s)) {
	    dn->right = dn_new;
	    dn_new = dn;
	} else {
	    xfree((char *)dn, sizeof(Dict_node));	    
	}
    }
    /* now reverse the list back */
    lookup_list = NULL;
    for (dn = dn_new; dn!=NULL; dn = dnx) {
	dnx = dn->right;
	dn->right = lookup_list;
	lookup_list = dn;
    }
}

void free_lookup_list(void) {
    Dict_node * n;
    while(lookup_list != NULL) {
	n = lookup_list->right;
	xfree((char *)lookup_list, sizeof(Dict_node));
	lookup_list = n;
    }
}

void rdictionary_lookup(Dict_node * dn, char * s) {
/* see comment in dictionary_lookup below */
    int m;
    Dict_node * dn_new;
    if (dn == NULL) return;
    m = dict_match(s, dn->string);
    if (m >= 0) {
        rdictionary_lookup(dn->right, s);
    }
    if (m == 0) {
        dn_new = (Dict_node*) xalloc(sizeof(Dict_node));
	*dn_new = *dn;
        dn_new->right = lookup_list;
        lookup_list = dn_new;
    }
    if (m <= 0) {
        rdictionary_lookup(dn->left, s);
    }
}

Dict_node * dictionary_lookup(Dictionary dict, char *s) {
/* Returns a pointer to a lookup list of the words in the dictionary.
   This list is made up of Dict_nodes, linked by their right pointers.
   The node, file and string fields are copied from the dictionary.
   
   Freeing this list elsewhere is unnecessary, as long as the rest of 
   the program merely examines the list (doesn't change it) */

   free_lookup_list();
   rdictionary_lookup(dict->root, s);
   prune_lookup_list(s);
   return lookup_list;
}

int boolean_dictionary_lookup(Dictionary dict, char *s) {
    return (dictionary_lookup(dict, s) != NULL);
}


/* the following routines are exactly the same as those above, 
   only they do not consider the idiom words
*/

void rabridged_lookup(Dict_node * dn, char * s) {
    int m;
    Dict_node * dn_new;
    if (dn == NULL) return;
    m = dict_match(s, dn->string);
    if (m >= 0) {
        rabridged_lookup(dn->right, s);
    }
    if ((m == 0) && (!is_idiom_word(dn->string))) {
        dn_new = (Dict_node*) xalloc(sizeof(Dict_node));
	*dn_new = *dn;
        dn_new->right = lookup_list;
        lookup_list = dn_new;
    }
    if (m <= 0) {
        rabridged_lookup(dn->left, s);
    }
}

Dict_node * abridged_lookup(Dictionary dict, char *s) {
   free_lookup_list();
   rabridged_lookup(dict->root, s);
   prune_lookup_list(s);
   return lookup_list;
}

int boolean_abridged_lookup(Dictionary dict, char *s) {
/* returns true if in the dictionary, false otherwise */
    return (abridged_lookup(dict, s) != NULL);
}

/* the following functions are for handling deletion */

static Dict_node * parent;
static Dict_node * to_be_deleted;


int find_one_non_idiom_node(Dict_node * p, Dict_node * dn, char * s) {
/* Returns true if it finds a non-idiom dict_node in a file that matches
   the string s.

   ** note: this now DOES include non-file words in its search.

    Also sets parent and to_be_deleted appropriately.
*/
    int m;
    if (dn == NULL) return FALSE;
    m = dict_match(s, dn->string);
    if (m <= 0) {
	if (find_one_non_idiom_node(dn,dn->left, s)) return TRUE;
    }
/*    if ((m == 0) && (!is_idiom_word(dn->string)) && (dn->file != NULL)) { */
    if ((m == 0) && (!is_idiom_word(dn->string))) {
	to_be_deleted = dn;
	parent = p;
	return TRUE;
    }
    if (m >= 0) {
	if (find_one_non_idiom_node(dn,dn->right, s)) return TRUE;
    }
    return FALSE;
}

void set_parent_of_node(Dictionary dict, 
			Dict_node *p, Dict_node * del, Dict_node * new) {
    if (p == NULL) {
	dict->root = new;
    } else {
	if (p->left == del) {
	    p->left = new;
	} else if (p->right == del) {
	    p->right = new;
	} else {
	    assert(FALSE, "Dictionary broken?");
	}
    }
}

int delete_dictionary_words(Dictionary dict, char * s) {
/* This deletes all the non-idiom words of the dictionary that match
   the given string.  Returns TRUE if some deleted, FALSE otherwise.
*/
    Dict_node *pred, *pred_parent;
    if (!find_one_non_idiom_node(NULL, dict->root, s)) return FALSE;
    for(;;) {
	/* now parent and to_be_deleted are set */
	if (to_be_deleted->file != NULL) {
	    to_be_deleted->file->changed = TRUE;
	}
	if (to_be_deleted->left == NULL) {
	    set_parent_of_node(dict, parent, to_be_deleted, to_be_deleted->right);
	    xfree((char *)to_be_deleted, sizeof(Dict_node));
	} else {
	    pred_parent = to_be_deleted;
	    pred = to_be_deleted->left;
	    while(pred->right != NULL) {
		pred_parent = pred;
		pred = pred->right;
	    }
	    to_be_deleted->string = pred->string;
	    to_be_deleted->file = pred->file;
	    to_be_deleted->exp = pred->exp;
	    set_parent_of_node(dict, pred_parent, pred, pred->left);
	    xfree((char *)pred, sizeof(Dict_node));
	}
	if (!find_one_non_idiom_node(NULL, dict->root, s)) return TRUE;
    }
}

int open_dictionary(char * dict_path_name, Dictionary dict) {
    if ((dict->fp = dictopen(dict_path_name, dict->name, "r")) == NULL) {
	return 0;
    }
    return 1;
}


void free_Word_file(Word_file * wf) {
    Word_file *wf1;

    for (;wf != NULL; wf = wf1) {
	wf1 = wf->next;
	xfree((char *) wf, sizeof(Word_file));
    }
}

/* The following two functions free the Exp s and the 
   E_lists of the dictionary.  Not to be confused with
   free_E_list in utilities.c
 */
static void free_Elist(E_list * l) {
    E_list * l1;

    for (; l != NULL; l = l1) {
        l1 = l->next;
	xfree(l, sizeof(E_list));
    }
}

static void free_Exp_list(Exp * e) {
    Exp * e1;
    for (; e != NULL; e = e1) {
        e1 = e->next;
	if (e->type != CONNECTOR_type) {
	   free_Elist(e->u.l);
	}
	xfree((char *)e, sizeof(Exp));
    }
}

void free_Dict_node(Dict_node * dn) {
    if (dn == NULL) return;
    free_Dict_node(dn->left);
    free_Dict_node(dn->right);
    xfree(dn, sizeof(Dict_node));
}

void free_dictionary(Dictionary dict) {
    free_Dict_node(dict->root);
    free_Word_file(dict->word_file_header);
    free_Exp_list(dict->exp_list);
}

void dict_display_word_info(Dictionary dict, char * s) {
    /* display the information about the given word */
    Dict_node * dn;
    Disjunct * d1, * d2;
    int len;
    if ((dn=dictionary_lookup(dict, s)) == NULL) {
	printf("    \"%s\" matches nothing in the dictionary.\n", s);
	return;
    }
    printf("Matches:\n");
    for (;dn != NULL; dn = dn->right) {
	len=0;
	d1 = build_disjuncts_for_dict_node(dn);
	for(d2 = d1 ; d2!=NULL; d2 = d2->next){
	    len++;
	}
	free_disjuncts(d1);
	printf("          ");
	left_print_string(stdout, dn->string, "                  ");
	printf(" %5d  ", len);
	if (dn->file != NULL) {
	    printf("<%s>", dn->file->file);
	}
	printf("\n");
    }
    free_lookup_list();
    return;
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"
#include <time.h>

#if !defined(WIN32)
   #include <sys/time.h>
   #include <sys/resource.h>
#endif

#if defined(__linux__)
/* based on reading the man page for getrusage on linux, I inferred that
   I needed to include this.  However it doesn't seem to be necessary */
   #include <unistd.h>
#endif

#if defined(__hpux__)
  #include <sys/syscall.h>
  int syscall(int, int, struct rusage *rusage);  /* can't find
						    the prototype for this */
  #define getrusage(a, b)  syscall(SYS_GETRUSAGE, (a), (b))
#endif /* __hpux__ */

#if defined(__sun__)
int getrusage(int who, struct rusage *rusage);
/* Declaration missing from sys/resource.h in sun operating systems (?) */
#endif /* __sun__ */

static double current_usage_time(void) {
/* returns the current usage time clock in seconds */
#if !defined(WIN32)
    struct rusage u;
    getrusage (RUSAGE_SELF, &u);
    return (u.ru_utime.tv_sec + ((double) u.ru_utime.tv_usec) / 1000000.0);
#else
    return ((double) clock())/CLOCKS_PER_SEC;
#endif
}

void print_time(Parse_Options opts, char * s) {
    resources_print_time(opts->verbosity, opts->resources, s);
}

void print_total_time(Parse_Options opts) {
    resources_print_total_time(opts->verbosity, opts->resources);
}

void print_total_space(Parse_Options opts) {
    resources_print_total_space(opts->verbosity, opts->resources);
}

Resources resources_create() {
    Resources r;

    r = (Resources) xalloc(sizeof(struct Resources_s));
    r->max_parse_time = MAX_PARSE_TIME_DEFAULT;
    r->when_created = current_usage_time();
    r->when_last_called = current_usage_time();
    r->time_when_parse_started = current_usage_time();
    r->space_when_parse_started = space_in_use;
    r->max_memory = MAX_MEMORY_DEFAULT;
    r->cumulative_time = 0;
    r->memory_exhausted = FALSE;
    r->timer_expired = FALSE;

    return r;
}

void resources_delete(Resources r) {
    xfree(r, sizeof(struct Resources_s));
}

void resources_reset(Resources r) {
    r->when_last_called = r->time_when_parse_started = current_usage_time();
    r->space_when_parse_started = space_in_use;
    r->timer_expired = FALSE;
    r->memory_exhausted = FALSE;
}

void resources_reset_time(Resources r) {
    r->when_last_called = r->time_when_parse_started = current_usage_time();
}

void resources_reset_space(Resources r) {
    r->space_when_parse_started = space_in_use;
}

int resources_exhausted(Resources r) {
    if (resources_timer_expired(r)) {
	r->timer_expired = TRUE;
    }
    if (resources_memory_exhausted(r)) {
	r->memory_exhausted = TRUE;
    }
    return (r->timer_expired || r->memory_exhausted);
}

int resources_timer_expired(Resources r) {
    if (r->max_parse_time == MAX_PARSE_TIME_DEFAULT) return 0;
    else return (r->timer_expired || (current_usage_time() - r->time_when_parse_started > r->max_parse_time));
}

int resources_memory_exhausted(Resources r) {
    if (r->max_memory == MAX_MEMORY_DEFAULT) return 0;
    else return (r->memory_exhausted || (space_in_use > r->max_memory));
}

void resources_print_time(int verbosity, Resources r, char * s) {
/* print out the cpu ticks since this was last called */
    double new_t;
    new_t = current_usage_time();
    if (verbosity > 1) {
	printf("++++");
	left_print_string(stdout, s, "                                           ");
	printf("%7.2f seconds\n", new_t - r->when_last_called);
    }
    r->when_last_called = new_t;
}

void resources_print_total_time(int verbosity, Resources r) {
/* print out the cpu ticks since this was last called */
    double new_t;
    new_t = current_usage_time();
    r->cumulative_time += (new_t - r->time_when_parse_started) ;
    if (verbosity > 0) {
        printf("++++");
	left_print_string(stdout, "Time",
			  "                                           ");
	printf("%7.2f seconds (%.2f total)\n", 
	       new_t - r->time_when_parse_started, r->cumulative_time);
    }
    r->time_when_parse_started = new_t;
}

void resources_print_total_space(int verbosity, Resources r) {
    if (verbosity > 1) {
       printf("++++");
      left_print_string(stdout, "Total space",
			"                                            ");
      printf("%d bytes (%d max)\n", space_in_use, max_space_in_use);
    }
}

/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

/* Suppose you have a program that generates strings and keeps pointers to them. 
   The program never needs to change these strings once they're generated.
   If it generates the same string again, then it can reuse the one it
   generated before.  This is what this package supports.

   String_set is the object.  The functions are:

   char * string_set_add(char * source_string, String_set * ss);
     This function returns a pointer to a string with the same 
     contents as the source_string.  If that string is already
     in the table, then it uses that copy, otherwise it generates
     and inserts a new one.

   char * string_set_lookup(char * source_string, String_set * ss);
     This function returns a pointer to a string with the same 
     contents as the source_string.  If that string is not already
     in the table, returns NULL;

   String_set * string_set_create(void);
     Create a new empty String_set.
     
   string_set_delete(String_set *ss);
     Free all the space associated with this string set.

   The implementation uses probed hashing (i.e. not bucket).

   */

int hash_string(unsigned char *str, String_set *ss) {
    unsigned int accum = 0;
    for (;*str != '\0'; str++) accum = ((256*accum) + (*str)) % (ss->size);
    return accum;
}

int stride_hash_string(unsigned char *str, String_set *ss) {
  /* This is the stride used, so we have to make sure that its value is not 0 */
    unsigned int accum = 0;
    for (;*str != '\0'; str++) accum = ((17*accum) + (*str)) % (ss->size);
    if (accum == 0) accum = 1;
    return accum;
}

int next_prime_up(int start) {
/* return the next prime up from start */
    int i;
    start = start | 1; /* make it odd */
    for (;;) {
	for (i=3; (i <= (start/i)); i += 2) {
	    if (start % i == 0) break;
	}
	if (start % i == 0) {
	    start += 2;
	} else {
	    return start;
	}
    }
}

String_set * string_set_create(void) {
    String_set *ss;
    int i;
    ss = (String_set *) xalloc(sizeof(String_set));
    ss->size = next_prime_up(100);
    ss->table = (char **) xalloc(ss->size * sizeof(char *));
    ss->count = 0;
    for (i=0; i<ss->size; i++) ss->table[i] = NULL;
    return ss;
}

int find_place(char * str, String_set *ss) {
    /* lookup the given string in the table.  Return a pointer
       to the place it is, or the place where it should be. */
    int h, s, i;
    h = hash_string(str, ss);
    s = stride_hash_string(str, ss);
    for (i=h; 1; i = (i + s)%(ss->size)) {
	if ((ss->table[i] == NULL) || (strcmp(ss->table[i], str) == 0)) return i;
    }
}

void grow_table(String_set *ss) {
    String_set old;
    int i, p;
    
    old = *ss;
    ss->size = next_prime_up(2 * old.size);  /* at least double the size */
    ss->table = (char **) xalloc(ss->size * sizeof(char *));
    ss->count = 0;
    for (i=0; i<ss->size; i++) ss->table[i] = NULL;
    for (i=0; i<old.size; i++) {
	if (old.table[i] != NULL) {
	    p = find_place(old.table[i], ss);
	    ss->table[p] = old.table[i];
	    ss->count++;
	}
    }
    /*printf("growing from %d to %d\n", old.size, ss->size);*/
    fflush(stdout);
    xfree((char *) old.table, old.size * sizeof(char *));
}

char * string_set_add(char * source_string, String_set * ss) {
    char * str;
    int len, p;
    
    assert(source_string != NULL, "STRING_SET: Can't insert a null string");

    p = find_place(source_string, ss);
    if (ss->table[p] != NULL) return ss->table[p];
    
    len = strlen(source_string);
    str = (char *) xalloc(len+1);
    strcpy(str, source_string);
    ss->table[p] = str;
    ss->count++;
    
    /* We just added it to the table.
       If the table got too big, we grow it.
       Too big is defined as being more than 3/4 full */
    if ((4 * ss->count) > (3 * ss->size)) grow_table(ss);
    
    return str;
}

char * string_set_lookup(char * source_string, String_set * ss) {
    int p;
    
    p = find_place(source_string, ss);
    return ss->table[p];
}

void string_set_delete(String_set *ss) {
    int i;
    
    if (ss == NULL) return;
    for (i=0; i<ss->size; i++) {
	if (ss->table[i] != NULL) xfree(ss->table[i], strlen(ss->table[i]) + 1);
    }
    xfree((char *) ss->table, ss->size * sizeof(char *));
    xfree((char *) ss, sizeof(String_set));
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

#define MAX_STRIP 10

int post_quote[MAX_SENTENCE];
/*static char * strip_left[] = {"(", "$", "``", NULL}; */
/*static char * strip_right[] = {")", "%", ",", ".", ":", ";", "?", "!", "''", "'", "'s", NULL};*/

static int is_initials_word(char * word) {
    /* This is rather esoteric and not terribly important.
       It returns TRUE if the word matches the pattern /[A-Z]\.]+/
       */
    int i;
    for (i=0; word[i] != '\0'; i += 2) {
	if (!isupper((int)word[i])) return FALSE;
	if (word[i+1] != '.') return FALSE;
    }
    return TRUE;
}

static int is_number2(char * s) {
    if (!isdigit((int)*s)) return FALSE;
    for (; *s != '\0'; s++) {
	if (isdigit((int)*s) || (*s == '.') || (*s == ',') || (*s == ':')) continue;
	/* The ":" is included here so we allow "10:30" to be a number. */
	return FALSE;
    }
    return TRUE;
}

static int ishyphenated(char * s) {
/* returns TRUE iff it's an appropriately formed hyphenated word.
   This means all letters, numbers, or hyphens, not beginning and
   ending with a hyphen.
*/
    int hyp, nonalpha;
    hyp = nonalpha = 0;
    if (*s == '-') return FALSE;
    while (*s != '\0') {
	if (!isalpha((int)*s) && !isdigit((int)*s) && (*s!='.') && (*s!=',')
	    && (*s!='-')) return FALSE;
	if (*s == '-') hyp++;
	s++;
    }
    return ((*(s-1)!='-') && (hyp>0));
}

static int is_ing_word(char * s) {

    int i=0;
    for (; *s != '\0'; s++) i++;
    if(i<5) return FALSE;
    if(strncmp("ing", s-3, 3)==0) return TRUE; 
    return FALSE;
}

static int is_s_word(char * s) {

    for (; *s != '\0'; s++);
    s--;
    if(*s != 's') return FALSE;
    s--;
    if(*s == 'i' || *s == 'u' || *s == 'o' || *s == 'y' || *s == 's') return FALSE;
    return TRUE;
}

static int is_ed_word(char * s) {

    int i=0;
    for (; *s != '\0'; s++) i++;
    if(i<4) return FALSE;
    if(strncmp("ed", s-2, 2)==0) return TRUE; 
    return FALSE;
}

static int is_ly_word(char * s) {

    int i=0;
    for (; *s != '\0'; s++) i++;
    if(i<4) return FALSE;
    if(strncmp("ly", s-2, 2)==0) return TRUE; 
    return FALSE;
}

static int issue_sentence_word(Sentence sent, char * s) {
    /* the string s is the next word of the sentence do not issue the empty
     * string.  return false if too many words or the word is too long.  */
    if (*s == '\0') return TRUE;
    if (strlen(s) > MAX_WORD) {
	lperror(SEPARATE, 
		". The word \"%s\" is too long.\n"  
		"A word can have a maximum of %d characters.\n", s, MAX_WORD);
	return FALSE;
    }

    if (sent->length == MAX_SENTENCE) {
	lperror(SEPARATE, ". The sentence has too many words.\n");
	return FALSE;
    }
    strcpy(sent->word[sent->length].string, s);
    /* Now we record whether the first character of the word is upper-case. 
       (The first character may be made lower-case
       later, but we may want to get at the original version) */
    if(isupper((int)s[0])) sent->word[sent->length].firstupper=1;
    else sent->word[sent->length].firstupper = 0; 
    sent->length++;
    return TRUE;
}

/*
    Here's a summary of how subscripts are handled:

    Reading the dictionary:

      If the last "." in a string is followed by a non-digit character,
      then the "." and everything after it is considered to be the subscript
      of the word.

      The dictionary reader does not allow you to have two words that
      match according to the criterion below.  (so you can't have
      "dog.n" and "dog")

      Quote marks are used to allow you to define words in the dictionary
      which would otherwise be considered part of the dictionary, as in

       ";": {@Xca-} & Xx- & (W+ or Qd+) & {Xx+};
       "%" : (ND- & {DD-} & <noun-sub-x> & 
	   (<noun-main-x> or B*x+)) or (ND- & (OD- or AN+));

    Rules for chopping words from the input sentence:

       First the prefix chars are stripped off of the word.  These
       characters are "(" and "$" (and now "``")

       Now, repeat the following as long as necessary:

	   Look up the word in the dictionary.
	   If it's there, the process terminates.

	   If it's not there and it ends in one of the right strippable
	   strings (see "right_strip") then remove the strippable string
	   and make it into a separate word.

	   If there is no strippable string, then the process terminates. 

    Rule for defining subscripts in input words:

       The subscript rule is followed just as when reading the dictionary.

    When does a word in the sentence match a word in the dictionary?

       Matching is done as follows: Two words with subscripts must match
       exactly.  If neither has a subscript they must match exactly.  If one
       does and one doesn't then they must match when the subscript is
       removed.  Notice that this is symmetric.

    So, under this system, the dictonary could have the words "Ill" and
    also the word "Ill."  It could also have the word "i.e.", which could be
    used in a sentence.
*/



static int separate_word(Sentence sent, char *w, char *wend, int is_first_word, int quote_found) {
    /* w points to a string, wend points to the char one after the end.  The
     * "word" w contains no blanks.  This function splits up the word if
     * necessary, and calls "issue_sentence_word()" on each of the resulting
     * parts.  The process is described above.  returns TRUE of OK, FALSE if
     * too many punctuation marks */
    int i, j, k, l, len;
    int r_strippable=0, l_strippable=0, s_strippable=0, p_strippable=0, n_r_stripped, s_stripped,
      word_is_in_dict, s_ok;
    int r_stripped[MAX_STRIP];  /* these were stripped from the right */
    char ** strip_left=NULL;
    char ** strip_right=NULL;
    char ** prefix=NULL;
    char ** suffix=NULL;
    char word[MAX_WORD+1];
    char newword[MAX_WORD+1];
    Dict_node * dn, * dn2, * start_dn;
    char * rpunc_con = "RPUNC";
    char * lpunc_con = "LPUNC";
    char * suf_con = "SUF";
    char * pre_con = "PRE";

    if(sent->dict->affix_table!=NULL) {

      start_dn = list_whole_dictionary(sent->dict->affix_table->root, NULL);
      for(dn=start_dn; dn!=NULL; dn=dn->right) {
	if(word_has_connector(dn, rpunc_con, 0)) r_strippable++;
	if(word_has_connector(dn, lpunc_con, 0)) l_strippable++;
	if(word_has_connector(dn, suf_con, 0)) s_strippable++;
	if(word_has_connector(dn, pre_con, 0)) p_strippable++;
      }
      strip_right = (char **) xalloc(r_strippable * sizeof(char *));
      strip_left = (char **) xalloc(l_strippable * sizeof(char *));
      suffix = (char **) xalloc(s_strippable * sizeof(char *));
      prefix = (char **) xalloc(p_strippable * sizeof(char *));
      
      i=0;
      j=0;
      k=0;
      l=0;
      dn=start_dn;
      while(dn!=NULL) {
	if(word_has_connector(dn, rpunc_con, 0)) {
	  strip_right[i] = dn->string;
	  i++;
	}
	if(word_has_connector(dn, lpunc_con, 0)) {
	  strip_left[j] = dn->string;
	  j++;
	}
	if(word_has_connector(dn, suf_con, 0)) {
	  suffix[k] = dn->string;
	  k++;
	}
	if(word_has_connector(dn, pre_con, 0)) {
	  prefix[l] = dn->string;
	  l++;
	}
	dn2=dn->right;
	xfree(dn, sizeof(Dict_node));
	dn=dn2;
      }
    }

    for (;;) {
	for (i=0; i<l_strippable; i++) {
	    if (strncmp(w, strip_left[i], strlen(strip_left[i])) == 0) {
		if (!issue_sentence_word(sent, strip_left[i])) return FALSE;
		w += strlen(strip_left[i]);
		break;
	    }
	}
	if (i==l_strippable) break;
    }

    /* Now w points to the string starting just to the right of any left-stripped characters. */
    /* stripped[] is an array of numbers, indicating the index numbers (in the strip_right array) of any
       strings stripped off; stripped[0] is the number of the first string stripped off, etc. When it
       breaks out of this loop, n_stripped will be the number of strings stripped off. */

    for (n_r_stripped = 0; n_r_stripped < MAX_STRIP; n_r_stripped++) {

	strncpy(word, w, wend-w);
	word[wend-w] = '\0';
	if (wend == w) break;  /* it will work without this */
	
	if (boolean_dictionary_lookup(sent->dict, word) || is_initials_word(word)) break;
	if (is_first_word && isupper((int)word[0])) {
	  /* This should happen if it's a word after a colon, also! */
	    word[0] = tolower(word[0]);
	    if (boolean_dictionary_lookup(sent->dict, word)) {
		word[0] = toupper(word[0]);  /* restore word to what it was */
		break;
	    }
	    word[0] = toupper(word[0]);
	}
	for (i=0; i < r_strippable; i++) {
	    len = strlen(strip_right[i]);
	    if ((wend-w) < len) continue;  /* the remaining w is too short for a possible match */
	    if (strncmp(wend-len, strip_right[i], len) == 0) {
		r_stripped[n_r_stripped] = i;
		wend -= len;
		break;
	    }
	}
	if(i==r_strippable) break;
    }

    /* Now we strip off suffixes...w points to the remaining word, "wend" to the end of the word. */

    s_stripped = -1;
    strncpy(word, w, wend-w);
    word[wend-w] = '\0';
    word_is_in_dict=0;

    if (boolean_dictionary_lookup(sent->dict, word) || is_initials_word(word)) word_is_in_dict=1;
    if (is_first_word && isupper((int)word[0])) {
      word[0] = tolower(word[0]);
      if (boolean_dictionary_lookup(sent->dict, word)) word_is_in_dict=1;
      word[0] = toupper(word[0]);
    }
    if(word_is_in_dict==0) {
      j=0;
      for (i=0; i <= s_strippable; i++) {
	s_ok = 0;
	/* Go through once for each suffix; then go through one final time for the no-suffix case */
	if(i < s_strippable) {
	  len = strlen(suffix[i]);
	  if ((wend-w) < len) continue;  /* the remaining w is too short for a possible match */
	  if (strncmp(wend-len, suffix[i], len) == 0) s_ok=1;
	  	}
	else len=0;

	if(s_ok==1 || i==s_strippable) {
	  
	  strncpy(newword, w, (wend-len)-w);
	  newword[(wend-len)-w] = '\0';

	  /* Check if the remainder is in the dictionary; for the no-suffix case, it won't be */	  
	  if (boolean_dictionary_lookup(sent->dict, newword)) {
	    if(verbosity>1) if(i< s_strippable) printf("Splitting word into two: %s-%s\n", newword, suffix[i]); 
	    s_stripped = i;
	    wend -= len;
	    strncpy(word, w, wend-w);
	    word[wend-w] = '\0';
	    break;
	  }

	  /* If the remainder isn't in the dictionary, try stripping off prefixes */
	  else {
	    for (j=0; j<p_strippable; j++) {
	      if (strncmp(w, prefix[j], strlen(prefix[j])) == 0) {
		strncpy(newword, w+strlen(prefix[j]), (wend-len)-(w+strlen(prefix[j])));
		newword[(wend-len)-(w+strlen(prefix[j]))]='\0';
		if(boolean_dictionary_lookup(sent->dict, newword)) {
		  if(verbosity>1) if(i < s_strippable) printf("Splitting word into three: %s-%s-%s\n", prefix[j], newword, suffix[i]); 
		  if (!issue_sentence_word(sent, prefix[j])) return FALSE;
		  if(i < s_strippable) s_stripped = i;
		  wend -= len;
		  w += strlen(prefix[j]);
		  strncpy(word, w, wend-w);
		word[wend-w] = '\0';
		break;
		}
	      }
	    }
	  }
	  if(j!=p_strippable) break;
	}
      }
    }
    
    /* word is now what remains after all the stripping has been done */

    /*    
    if (n_stripped == MAX_STRIP) {
	lperror(SEPARATE, 
		".\n\"%s\" is followed by too many punctuation marks.\n", word);
	return FALSE;
    } */

    if (quote_found==1) post_quote[sent->length]=1;

    if (!issue_sentence_word(sent, word)) return FALSE;

    if(s_stripped != -1) {
      if (!issue_sentence_word(sent, suffix[s_stripped])) return FALSE;
    }

    for (i=n_r_stripped-1; i>=0; i--) {
	if (!issue_sentence_word(sent, strip_right[r_stripped[i]])) return FALSE;
    }

    if(sent->dict->affix_table!=NULL) {
      xfree(strip_right, r_strippable * sizeof(char *));
      xfree(strip_left, l_strippable * sizeof(char *));
      xfree(suffix, s_strippable * sizeof(char *));
      xfree(prefix, p_strippable * sizeof(char *));
    }
    return TRUE;
}

int separate_sentence(char * s, Sentence sent) {
    /* The string s has just been read in from standard input.
       This function breaks it up into words and stores these words in
       the sent->word[] array.  Returns TRUE if all is well, FALSE otherwise.
       Quote marks are treated just like blanks.
       */
    char *t;
    int i, is_first, quote_found;
    Dictionary dict = sent->dict;

    for(i=0; i<MAX_SENTENCE; i++) post_quote[i]=0;
    sent->length = 0;
    
    if (dict->left_wall_defined) 
	if (!issue_sentence_word(sent, LEFT_WALL_WORD)) return FALSE;
    
    is_first = TRUE;
    for(;;) {
        quote_found = FALSE;
	while(isspace((int)*s) || (*s == '\"')) {
	  s++;
	  if(*s == '\"') quote_found=TRUE;
	}
	if (*s == '\0') break;
	for (t=s; !((isspace((int)*t) || (*t == '\"')) || *t=='\0'); t++);
	if (!separate_word(sent, s, t, is_first, quote_found)) return FALSE;
	is_first = FALSE;
	s = t;
	if (*s == '\0') break;
    }
    
    if (dict->right_wall_defined) 
	if (!issue_sentence_word(sent, RIGHT_WALL_WORD)) return FALSE;

    return (sent->length > dict->left_wall_defined + dict->right_wall_defined);
}

static int special_string(Sentence sent, int i, char * s) {
    X_node * e;
    if (boolean_dictionary_lookup(sent->dict, s)) {
	sent->word[i].x = build_word_expressions(sent, s);
	for (e = sent->word[i].x; e != NULL; e = e->next) {
	    e->string = sent->word[i].string;
	}
	return TRUE;
    } else {
	lperror(BUILDEXPR, ".\n To process this sentence your dictionary "
		"needs the word \"%s\".\n", s);
	return FALSE;
    }
}

static int guessed_string(Sentence sent, int i, char * s, char * type) {
    X_node * e;
    char *t, *u;
    char str[MAX_WORD+1];
    if (boolean_dictionary_lookup(sent->dict, type)) {
      sent->word[i].x = build_word_expressions(sent, type);
      e = sent->word[i].x;
	  if(is_s_word(s)) {

	    for (; e != NULL; e = e->next) {
	      t = strchr(e->string, '.');
	      if (t != NULL) {
		sprintf(str, "%.50s[!].%.5s", s, t+1);
	      } else {
		sprintf(str, "%.50s[!]", s);
	      }
	      t = (char *) xalloc(strlen(str)+1);
	      strcpy(t,str);
	      u = string_set_add(t, sent->string_set);
	      xfree(t, strlen(str)+1);
	      e->string = u;
	    }
	  }

	  else {
	    if(is_ed_word(s)) {
	      sprintf(str, "%.50s[!].v", s);
	    }
	    else if(is_ing_word(s)) {
	      sprintf(str, "%.50s[!].g", s);
	    }
	    else if(is_ly_word(s)) {
	      sprintf(str, "%.50s[!].e", s);
	    }
	    else sprintf(str, "%.50s[!]", s);

	    t = (char *) xalloc(strlen(str)+1);
	    strcpy(t,str);
	    u = string_set_add(t, sent->string_set);
	    xfree(t, strlen(str)+1);
	    e->string = u;
	  }
	  return TRUE;

    } else {
	lperror(BUILDEXPR, ".\n To process this sentence your dictionary "
		"needs the word \"%s\".\n", type);
	return FALSE;
    }
}

static void handle_unknown_word(Sentence sent, int i, char * s) {
  /* puts into word[i].x the expression for the unknown word */
  /* the parameter s is the word that was not in the dictionary */    
  /* it massages the names to have the corresponding subscripts */
  /* to those of the unknown words */
  /* so "grok" becomes "grok[?].v"  */
    char *t,*u;
    X_node *d;
    char str[MAX_WORD+1];
    
    sent->word[i].x = build_word_expressions(sent, UNKNOWN_WORD);
    if (sent->word[i].x == NULL) 
	assert(FALSE, "UNKNOWN_WORD should have been there");
    
    for (d = sent->word[i].x; d != NULL; d = d->next) {
	t = strchr(d->string, '.');
	if (t != NULL) {
	    sprintf(str, "%.50s[?].%.5s", s, t+1);
	} else {
	    sprintf(str, "%.50s[?]", s);
	}
	t = (char *) xalloc(strlen(str)+1);
	strcpy(t,str);
	u = string_set_add(t, sent->string_set);
	xfree(t, strlen(str)+1);
	d->string = u;
    }
}

int build_sentence_expressions(Sentence sent) {
    /* Corrects case of first word, fills in other proper nouns, and
       builds the expression lists for the resulting words.
       
       Algorithm:  
       Apply the following step to all words w:
       if w is in the dictionary, use it.
       else if w is upper case use PROPER_WORD disjuncts for w.
       else if it's hyphenated, use HYPHENATED_WORD
       else if it's a number, use NUMBER_WORD.
       
       Now, we correct the first word, w.
       if w is upper case, let w' be the lower case version of w.
       if both w and w' are in the dict, concatenate these disjncts.
       else if w' is in dict, use disjuncts of w'
       else leave the disjuncts alone
       */
    int i, first_word;  /* the index of the first word after the wall */
    char *s, *u, temp_word[MAX_WORD+1];
    X_node * e;
    Dictionary dict = sent->dict;


    if (dict->left_wall_defined) {
	first_word = 1;
    } else {
	first_word = 0;
    }
    
    /* the following loop treats all words the same 
       (nothing special for 1st word) */
    for (i=0; i<sent->length; i++) {
	s = sent->word[i].string;
	if (boolean_dictionary_lookup(sent->dict, s)) {
	    sent->word[i].x = build_word_expressions(sent, s);
	} else if (isupper((int)s[0]) && is_s_word(s) && dict->pl_capitalized_word_defined) {
	    if (!special_string(sent, i, PL_PROPER_WORD)) return FALSE;
	} else if (isupper((int)s[0]) && dict->capitalized_word_defined) {
	    if (!special_string(sent, i, PROPER_WORD)) return FALSE;
	} else if (is_number2(s) && dict->number_word_defined) {
	    /* we know it's a plural number, or 1 */
	    /* if the string is 1, we'll only be here if 1's not in the dictionary */
	    if (!special_string(sent, i, NUMBER_WORD)) return FALSE;
	} else if (ishyphenated(s) && dict->hyphenated_word_defined) {
	    /* singular hyphenated */
	    if (!special_string(sent, i, HYPHENATED_WORD)) return FALSE;
	} else if (is_ing_word(s) && dict->ing_word_defined) {
	    if (!guessed_string(sent, i, s, ING_WORD)) return FALSE;
	} else if (is_s_word(s) && dict->s_word_defined) {
	    if (!guessed_string(sent, i, s, S_WORD)) return FALSE;
	} else if (is_ed_word(s) && dict->ed_word_defined) {
	    if (!guessed_string(sent, i, s, ED_WORD)) return FALSE;
	} else if (is_ly_word(s) && dict->ly_word_defined) {
	    if (!guessed_string(sent, i, s, LY_WORD)) return FALSE;
	} else if (dict->unknown_word_defined && dict->use_unknown_word) {
	    handle_unknown_word(sent, i, s);
	} else {
	    /* the reason I can assert this is that the word
	       should have been looked up already if we get here. */
	    assert(FALSE, "I should have found that word.");
	}
    }

    /* Under certain cases--if it's the first word of the sentence, or if it follows a colon
       or a quotation mark--a word that's capitalized has to be looked up as an uncapitalized 
       word (as well as a capitalized word). */


    for (i=0; i<sent->length; i++) {
	if (! (i==first_word || (i>0 && strcmp(":", sent->word[i-1].string)==0) || post_quote[i]==1) ) continue;
	s = sent->word[i].string;
	if (isupper((int)s[0])) {
	    safe_strcpy(temp_word, s, sizeof(temp_word));
	    temp_word[0] = tolower(temp_word[0]);
	    u = string_set_add(temp_word, sent->string_set);
	    /* If the lower-case version is in the dictionary... */
	    if (boolean_dictionary_lookup(sent->dict, u)) {
	        /* Then check if the upper-case version is there. If it is, the disjuncts for
		   the upper-case version have been put there already. So add on the disjuncts
		   for the lower-case version. */
		if (boolean_dictionary_lookup(sent->dict, s)) {
		    e = build_word_expressions(sent, u);
		    sent->word[i].x = 
			catenate_X_nodes(sent->word[i].x, e);
		} else {
		  /* If the upper-case version isn't there, replace the u.c. disjuncts with l.c. ones */
		    s[0] = tolower(s[0]);
		    e = build_word_expressions(sent, s);
		    free_X_nodes(sent->word[i].x);
		    sent->word[i].x = e;
		}
	    }
	}
    }
    
    return TRUE;
}


/* This just looks up all the words in the sentence, and builds
   up an appropriate error message in case some are not there.
   It has no side effect on the sentence.  Returns TRUE if all
   went well.
 */
int sentence_in_dictionary(Sentence sent){
    int w, ok_so_far;
    char * s;
    Dictionary dict = sent->dict;
    char temp[1024];

    ok_so_far = TRUE;
    for (w=0; w<sent->length; w++) {
	s = sent->word[w].string;
	if (!boolean_dictionary_lookup(dict, s) &&
	    !(isupper((int)s[0])   && dict->capitalized_word_defined) &&
	    !(isupper((int)s[0]) && is_s_word(s) && dict->pl_capitalized_word_defined) &&
	    !(ishyphenated(s) && dict->hyphenated_word_defined)  &&
	    !(is_number2(s)    && dict->number_word_defined) &&
	    !(is_ing_word(s)  && dict->ing_word_defined)  &&
	    !(is_s_word(s)    && dict->s_word_defined)  &&
	    !(is_ed_word(s)   && dict->ed_word_defined)  &&
	    !(is_ly_word(s)   && dict->ly_word_defined)) {
	    if (ok_so_far) {
		safe_strcpy(temp, "The following words are not in the dictionary:", sizeof(temp));
		ok_so_far = FALSE;
	    }
	    safe_strcat(temp, " \"", sizeof(temp));
	    safe_strcat(temp, sent->word[w].string, sizeof(temp));
	    safe_strcat(temp, "\"", sizeof(temp));
	}
    }
    if (!ok_so_far) {
	lperror(NOTINDICT, "\n%s\n", temp);
    }
    return ok_so_far;
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

#define DEFAULTPATH ".:./data:"  /* Appended to the end of the 
				    environment dictpath */

/* This file contains certain general utilities. */

int   verbosity;

void safe_strcpy(char *u, char * v, int usize) {
/* Copies as much of v into u as it can assuming u is of size usize */
/* guaranteed to terminate u with a '\0'.                           */    
    strncpy(u, v, usize-1);
    u[usize-1] = '\0';
}

void safe_strcat(char *u, char *v, int usize) {
/* catenates as much of v onto u as it can assuming u is of size usize           */
/* guaranteed to terminate u with a '\0'.  Assumes u and v are null terminated.  */
    strncat(u, v, usize-strlen(u)-1);
    u[usize-1] = '\0';
}    

void free_connectors(Connector *e) {
/* free the list of connectors pointed to by e
   (does not free any strings) 
*/
    Connector * n;
    for(;e != NULL; e = n) {
	n = e->next;
	xfree((char *)e, sizeof(Connector));
    }
}

void free_disjuncts(Disjunct *c) {
/* free the list of disjuncts pointed to by c
   (does not free any strings) 
*/
    Disjunct *c1;
    for (;c != NULL; c = c1) {
	c1 = c->next;
	free_connectors(c->left);
	free_connectors(c->right);
	xfree((char *)c, sizeof(Disjunct));
    }
}

Connector * init_connector(Connector *c) {
    c->length_limit = UNLIMITED_LEN;
    /*    c->my_word = NO_WORD;  */       /* mark it unset, to make sure it gets set later */
    return c;
}

void free_X_nodes(X_node * x) {
/* frees the list of X_nodes pointed to by x, and all of the expressions */
    X_node * y;
    for (; x!= NULL; x = y) {
	y = x->next;
	free_Exp(x->exp);
	xfree((char *)x, sizeof(X_node));
    }
}

void free_E_list(E_list *);
void free_Exp(Exp * e) {
    if (e->type != CONNECTOR_type) {
	free_E_list(e->u.l);
    }
    xfree((char *)e, sizeof(Exp));
}

void free_E_list(E_list * l) {
    if (l == NULL) return;
    free_E_list(l->next);
    free_Exp(l->e);
    xfree((char *)l, sizeof(E_list));
}

int size_of_expression(Exp * e) {
/* Returns the number of connectors in the expression e */
    int size;
    E_list * l;
    if (e->type == CONNECTOR_type) return 1;
    size = 0;
    for (l=e->u.l; l!=NULL; l=l->next) {
	size += size_of_expression(l->e);
    }
    return size;
}

unsigned int randtable[RTSIZE];

/* There is a legitimate question of whether having the hash function    */
/* depend on a large array is a good idea.  It might not be fastest on   */
/* a machine that depends on caching for its efficiency.  On the other   */
/* hand, Phong Vo's hash (and probably other linear-congruential) is     */
/* pretty bad.  So, mine is a "competitive" hash function -- you can't   */
/* make it perform horribly.                                             */

void init_randtable(void) {
    int i;
    srand(10);
    for (i=0; i<RTSIZE; i++) {
	randtable[i] = rand();
    }
}

/* Build a copy of the given expression (don't copy strings, of course) */
E_list * copy_E_list(E_list * l);
Exp * copy_Exp(Exp * e) {
    Exp * n;
    if (e == NULL) return NULL;
    n = (Exp *) xalloc(sizeof(Exp));
    *n = *e;
    if (e->type != CONNECTOR_type) {
	n->u.l = copy_E_list(e->u.l);
    }
    return n;
}

E_list * copy_E_list(E_list * l) {
    E_list * nl;
    if (l == NULL) return NULL;
    nl = (E_list *) xalloc(sizeof(E_list));
    *nl = *l;    /* not necessary -- both fields will be built below */
    nl->next = copy_E_list(l->next);
    nl->e = copy_Exp(l->e);
    return nl;
}

Connector * copy_connectors(Connector * c) {
/* This builds a new copy of the connector list pointed to by c.
   Strings, as usual, are not copied.
*/
    Connector *c1;
    if (c == NULL) return NULL;
    c1 = init_connector((Connector *) xalloc(sizeof(Connector)));
    *c1 = *c;
    c1->next = copy_connectors(c->next);
    return c1;
}

Disjunct * copy_disjunct(Disjunct * d) {
/* This builds a new copy of the disjunct pointed to by d (except for the
   next field which is set to NULL).  Strings, as usual,
   are not copied.
*/
    Disjunct * d1;
    if (d == NULL) return NULL;
    d1 = (Disjunct *) xalloc(sizeof(Disjunct));
    *d1 = *d;
    d1->next = NULL;
    d1->left = copy_connectors(d->left);
    d1->right = copy_connectors(d->right);
    return d1;
}

int max_space_in_use;
int space_in_use;
int max_external_space_in_use;
int external_space_in_use;

void * xalloc(int size) {
/* To allow printing of a nice error message, and keep track of the
   space allocated.
*/   
    char * p = (char *) malloc(size);
    space_in_use += size;
    if (space_in_use > max_space_in_use) max_space_in_use = space_in_use;
    if ((p == NULL) && (size != 0)){
        printf("Ran out of space.\n");
	abort();
        exit(1);
    }
    return (void *) p;
}

void xfree(void * p, int size) {
    space_in_use -= size;
    free(p);
}

void * exalloc(int size) {

    char * p = (char *) malloc(size);
    external_space_in_use += size;
    if (external_space_in_use > max_external_space_in_use) {
	max_external_space_in_use = external_space_in_use;
    }
    if ((p == NULL) && (size != 0)){
        printf("Ran out of space.\n");
	abort();
        exit(1);
    }
    return (void *) p;
}

void exfree(void * p, int size) {
    external_space_in_use -= size;
    free(p);
}

/* This is provided as part of the API */
void string_delete(char * p) {
    exfree(p, strlen(p)+1);
}

void exfree_connectors(Connector *e) {
    Connector * n;
    for(;e != NULL; e = n) {
	n = e->next;
	exfree(e->string, sizeof(char)*(strlen(e->string)+1));
	exfree(e, sizeof(Connector));
    }
}

Connector * excopy_connectors(Connector * c) {
    Connector *c1;

    if (c == NULL) return NULL;

    c1 = init_connector((Connector *) exalloc(sizeof(Connector)));
    *c1 = *c;
    c1->string = (char *) exalloc(sizeof(char)*(strlen(c->string)+1));
    strcpy(c1->string, c->string);
    c1->next = excopy_connectors(c->next);

    return c1;
}


Link excopy_link(Link l) {
     Link newl;

     if (l == NULL) return NULL;

     newl = (Link) exalloc(sizeof(struct Link_s));
     newl->name = (char *) exalloc(sizeof(char)*(strlen(l->name)+1));
     strcpy(newl->name, l->name);
     newl->l = l->l;
     newl->r = l->r;
     newl->lc = excopy_connectors(l->lc);
     newl->rc = excopy_connectors(l->rc);

     return newl;
}

void exfree_link(Link l) {
     exfree_connectors(l->rc);
     exfree_connectors(l->lc);
     exfree(l->name, sizeof(char)*(strlen(l->name)+1));
     exfree(l, sizeof(struct Link_s));
}


Disjunct * catenate_disjuncts(Disjunct *d1, Disjunct *d2) {
/* Destructively catenates the two disjunct lists d1 followed by d2. */
/* Doesn't change the contents of the disjuncts */
/* Traverses the first list, but not the second */    
    Disjunct * dis = d1;

    if (d1 == NULL) return d2;
    if (d2 == NULL) return d1;
    while (dis->next != NULL) dis = dis->next;
    dis->next = d2;
    return d1;
}

X_node * catenate_X_nodes(X_node *d1, X_node *d2) {
/* Destructively catenates the two disjunct lists d1 followed by d2. */
/* Doesn't change the contents of the disjuncts */
/* Traverses the first list, but not the second */    
    X_node * dis = d1;

    if (d1 == NULL) return d2;
    if (d2 == NULL) return d1;
    while (dis->next != NULL) dis = dis->next;
    dis->next = d2;
    return d1;
}

int next_power_of_two_up(int i) {
/* Returns the smallest power of two that is at least i and at least 1 */
    int j=1;
    while(j<i) j = j<<1;
    return j;
}

int upper_case_match(char *s, char *t) {
/* returns TRUE if the initial upper case letters of s and t match */
    while(isupper((int)*s) || isupper((int)*t)) {
	if (*s != *t) return FALSE;
	s++;
	t++;
    }
    return (!isupper((int)*s) && !isupper((int)*t));
}

void left_print_string(FILE * fp, char * s, char * t) {
/* prints s then prints the last |t|-|s| characters of t.
   if s is longer than t, it truncates s.
*/
    int i, j, k;
    j = strlen(t);
    k = strlen(s);
    for (i=0; i<j; i++) {
	if (i<k) {
	    fprintf(fp, "%c", s[i]);
	} else {
	    fprintf(fp, "%c", t[i]);
	}
    }
}

int sentence_contains(Sentence sent, char * s) {
/* Returns TRUE if one of the words in the sentence is s */
    int w;
    for (w=0; w<sent->length; w++) {
	if (strcmp(sent->word[w].string, s) == 0) return TRUE;
    }
    return FALSE;
}

void set_is_conjunction(Sentence sent) {
    int w;
    char * s;
    for (w=0; w<sent->length; w++) {
	s = sent->word[w].string;
	sent->is_conjunction[w] = ((strcmp(s, "and")==0) || (strcmp(s, "or" )==0) ||
				   (strcmp(s, "but")==0) || (strcmp(s, "nor")==0));
    }
}

int sentence_contains_conjunction(Sentence sent) {
/* Return true if the sentence contains a conjunction.  Assumes
   is_conjunction[] has been initialized.
*/
    int w;
    for (w=0; w<sent->length; w++) {
	if (sent->is_conjunction[w]) return TRUE;
    }
    return FALSE;
}

int conj_in_range(Sentence sent, int lw, int rw) {
/* Returns true if the range lw...rw inclusive contains a conjunction     */
    for (;lw <= rw; lw++) {
	if (sent->is_conjunction[lw]) return TRUE;
    }
    return FALSE;
}

FILE *old_dictopen(char *dictname, char *filename, char *how) {
    /* This is the old version.  I think it's buggy and inelegant.  I've
       rewritten it below   *DS*  */

    /* This function is used to open a dictionary file or a word file.
       It tries the following sequence of places to look for the file.
       The first one that is successful is used.  (1) Open the given
       file.  (2) if DICTPATH is defined, try the paths defined by that.
       (3) Use the path name of the given dictionary.  */

    char completename[MAX_PATH_NAME+1], fullpath[MAX_PATH_NAME+1], *dictpath;
    char dummy[MAX_PATH_NAME+1];
    char *pos, *oldpos;
    int filenamelen, len;
    FILE *fp;

    sprintf(fullpath, "%s%s", getenv(DICTPATH), DEFAULTPATH);
    dictpath = fullpath;

    if (dictpath == NULL && dictname != NULL) {
	/* look in the dictname part */
	safe_strcpy(dummy, dictname, sizeof(dummy));
	pos = strrchr(dummy, '/');
	if (pos != NULL) {
	    *pos = '\0';
	    dictpath = dummy;
	}
    }

    dictpath = fullpath;

    if (dictpath == NULL && dictname != NULL) {
	/* look in the dictname part */
	safe_strcpy(dummy, dictname, sizeof(dummy));
	pos = strrchr(dummy, '/');
	if (pos != NULL) {
	    *pos = '\0';
	    dictpath = dummy;
	}
    }


    if (dictpath == NULL) {
	printf("   Opening %s\n", filename); 
	return (fopen(filename, how));
    } else if ((fp = fopen(filename, how)) != NULL) {
	printf("   Opening %s\n", filename);
	return fp;
    }
    
    filenamelen = strlen(filename);
    len = strlen(dictpath)+ filenamelen + 1 + 1;
    oldpos = dictpath;
    fp = NULL;
    while ((pos = strchr(oldpos, ':')) != NULL) {
	strncpy(completename, oldpos, (pos-oldpos));
	*(completename+(pos-oldpos)) = '/';
	strcpy(completename+(pos-oldpos)+1,filename);
	if ((fp = fopen(completename, how)) != NULL) {
	    printf("   Opening %s\n", completename); 
	    return fp;
	}
	oldpos = pos+1;
    }
    pos = oldpos+strlen(oldpos);
    strcpy(completename, oldpos);
    *(completename+(pos-oldpos)) = '/';
    strcpy(completename+(pos-oldpos)+1,filename);
    fp = fopen(completename,how);
    printf("   Opening %s\n", completename); 
    return fp;
}




FILE *dictopen(char *dictname, char *filename, char *how) {

    /* This function is used to open a dictionary file or a word file,
       or any associated data file (like a post process knowledge file).

       It works as follows.  If the file name begins with a "/", then
       it's assumed to be an absolute file name and it tries to open
       that exact file.

       If the filename does not begin with a "/", then it uses the
       dictpath mechanism to find the right file to open.  This looks
       for the file in a sequence of directories until it finds it.  The
       sequence of directories is specified in a dictpath string, in
       which each directory is followed by a ":".

       The dictpath that it uses is constructed as follows.  If the
       dictname is non-null, and is an absolute path name (beginning
       with a "/", then the part after the last "/" is removed and this
       is the first directory on the dictpath.  After this comes the
       DICTPATH environment variable, followed by the DEFAULTPATH
    */

    char completename[MAX_PATH_NAME+1];
    char fulldictpath[MAX_PATH_NAME+1];
    char dummy1[MAX_PATH_NAME+1];
    char *dp1, *dp2;
    char *pos, *oldpos;
    int filenamelen, len;
    FILE *fp;

    if (filename[0] == '/') {
	return fopen(filename, how);  /* If the file does not exist NULL is returned */
    }

    dp1 = "";
    /*    if (dictname != NULL && dictname[0] == '/') { */
    if (dictname != NULL) {
	safe_strcpy(dummy1, dictname, sizeof(dummy1));
	pos = strrchr(dummy1, '/');
	if (pos != NULL) {
	    *pos = ':';
	    *(pos+1) = '\0';
	    dp1 = dummy1;
	}
    }
    /* dp1 now points to the part of the dictpath due to dictname */
    
    dp2 = "";
    /*  We're no longer using the dictpath in the environment
    if (strlen(getenv(DICTPATH)) > 0) {
	sprintf(dummy2, "%s:", getenv(DICTPATH));
	dp2 = dummy2;
    }
    */
    /* dp2 now points to the part of the dictpath due to the environment var */

    sprintf(fulldictpath, "%s%s%s", dp1, dp2, DEFAULTPATH);
    /* now fulldictpath is our dictpath, where each entry is followed by a ":"
       including the last one */
    
    filenamelen = strlen(filename);
    len = strlen(fulldictpath)+ filenamelen + 1 + 1;
    oldpos = fulldictpath;
    while ((pos = strchr(oldpos, ':')) != NULL) {
	strncpy(completename, oldpos, (pos-oldpos));
	*(completename+(pos-oldpos)) = '/';
	strcpy(completename+(pos-oldpos)+1,filename);
	if ((fp = fopen(completename, how)) != NULL) {
	    printf("   Opening %s\n", completename); 
	    return fp;
	}
	oldpos = pos+1;
    }
    return NULL;
}

static int random_state[2] = {0,0};
static int random_count = 0;
static int random_inited = FALSE;

int step_generator(int d) {
    /* no overflow should occur, so this is machine independent */
    random_state[0] = ((random_state[0] * 3) + d + 104729) % 179424673;
    random_state[1] = ((random_state[1] * 7) + d + 48611) % 86028121;
    return random_state[0] + random_state[1];;
}

void my_random_initialize(int seed) {
    assert(!random_inited, "Random number generator not finalized.");
    
    seed = (seed < 0) ? -seed : seed;
    seed = seed % (1 << 30);

    random_state[0] = seed % 3;
    random_state[1] = seed % 5;
    random_count = seed;
    random_inited = TRUE;
}

void my_random_finalize() {
    assert(random_inited, "Random number generator not initialized.");
    random_inited = FALSE;
}

int my_random(void) {
    random_count++;
    return (step_generator(random_count));
}


/* Here's the connector set data structure */

#if 0
/* this is defined in parser_api.h */
typedef struct {
    Connector ** hash_table;
    int          table_size;
    int          is_defined;  /* if 0 then there is no such set */
} Connector_set;
#endif

int connector_set_hash(Connector_set *conset, char * s, int d) {
/* This hash function only looks at the leading upper case letters of
   the string, and the direction, '+' or '-'.
*/
    int i;
    for(i=d; isupper((int)*s); s++) i = i + (i<<1) + randtable[(*s + i) & (RTSIZE-1)];
    return (i & (conset->table_size-1));
}

void build_connector_set_from_expression(Connector_set * conset, Exp * e) {
    E_list * l;
    Connector * c;
    int h;
    if (e->type == CONNECTOR_type) {
	c = init_connector((Connector *) xalloc(sizeof(Connector)));
	c->string = e->u.string;
	c->label = NORMAL_LABEL;        /* so we can use match() */
	c->priority = THIN_priority;
	c->word = e->dir;     /* just use the word field to give the dir */
	h = connector_set_hash(conset, c->string, c->word);
	c->next = conset->hash_table[h];
	conset->hash_table[h] = c;
    } else {
	for (l=e->u.l; l!=NULL; l=l->next) {
	    build_connector_set_from_expression(conset, l->e);
	}
    }
}	

Connector_set * connector_set_create(Exp *e) {
    int i;
    Connector_set *conset;

    conset = (Connector_set *) xalloc(sizeof(Connector_set));
    conset->table_size = next_power_of_two_up(size_of_expression(e));
    conset->hash_table =
      (Connector **) xalloc(conset->table_size * sizeof(Connector *));
    for (i=0; i<conset->table_size; i++) conset->hash_table[i] = NULL;
    build_connector_set_from_expression(conset, e);
    return conset;
}

void connector_set_delete(Connector_set * conset) {
    int i;
    if (conset == NULL) return;
    for (i=0; i<conset->table_size; i++) free_connectors(conset->hash_table[i]);
    xfree(conset->hash_table, conset->table_size * sizeof(Connector *));
    xfree(conset, sizeof(Connector_set));
}

int match_in_connector_set(Connector_set *conset, Connector * c, int d) {
/* Returns TRUE the given connector is in this conset.  FALSE otherwise.
   d='+' means this connector is on the right side of the disjunct.
   d='-' means this connector is on the left side of the disjunct.
*/
    int h;
    Connector * c1;
    if (conset == NULL) return FALSE;
    h = connector_set_hash(conset, c->string, d);
    for (c1=conset->hash_table[h]; c1!=NULL; c1 = c1->next) {
	if (x_match(c1, c) && (d == c1->word)) return TRUE;
    }
    return FALSE;
}

Dict_node * list_whole_dictionary(Dict_node *root, Dict_node *dn) {
    Dict_node *c, *d;
    if (root == NULL) return dn;
    c = (Dict_node *) xalloc(sizeof(Dict_node));
    *c = *root;
    d = list_whole_dictionary(root->left, dn);
    c->right = list_whole_dictionary(root->right, d);
    return c;
}

void free_listed_dictionary(Dict_node *dn) {
    Dict_node * dn2;
    while(dn!=NULL) {
      dn2=dn->right;
      xfree(dn, sizeof(Dict_node));
      dn=dn2;
    }
}

int easy_match(char * s, char * t) {

    /* This is like the basic "match" function in count.c - the basic connector-matching 
       function used in parsing - except it ignores "priority" (used to handle fat links) */

    while(isupper((int)*s) || isupper((int)*t)) {
	if (*s != *t) return FALSE;
	s++;
	t++;
    }

    while ((*s!='\0') && (*t!='\0')) {
      if ((*s == '*') || (*t == '*') ||
	  ((*s == *t) && (*s != '^'))) {
	s++;
	t++;
      } else return FALSE;
    }
    return TRUE;

}

int word_has_connector(Dict_node * dn, char * cs, int direction) {

  /* This function takes a dict_node (corresponding to an entry in a given dictionary), a
     string (representing a connector), and a direction (0 = right-pointing, 1 = left-pointing);
     it returns 1 if the dictionary expression for the word includes the connector, 0 otherwise.
     This can be used to see if a word is in a certain category (checking for a category 
     connector in a table), or to see if a word has a connector in a normal dictionary. The
     connector check uses a "smart-match", the same kind used by the parser. */

    Connector * c2=NULL;
    Disjunct * d, *d0;
    if(dn == NULL) return -1;
    d0 = d = build_disjuncts_for_dict_node(dn);
    if(d == NULL) return 0;
    for(; d!=NULL; d=d->next) { 
      if(direction==0) c2 = d->right;
      if(direction==1) c2 = d->left;
      for(; c2!=NULL; c2=c2->next) {
	if(easy_match(c2->string, cs)==1) {
	    free_disjuncts(d0);
	    return 1;
	}
      }
    }
    free_disjuncts(d0);
    return 0;
}
/********************************************************************************/
/* Copyright (c) 2004                                                           */
/* Daniel Sleator, David Temperley, and John Lafferty                           */
/* All rights reserved                                                          */
/*                                                                              */
/* Use of the link grammar parsing system is subject to the terms of the        */
/* license set forth in the LICENSE file included with this software,           */ 
/* and also available at http://www.link.cs.cmu.edu/link/license.html           */
/* This license allows free redistribution and use in source and binary         */
/* forms, with or without modification, subject to certain conditions.          */
/*                                                                              */
/********************************************************************************/

#include "link-includes.h"

char * get_a_word(Dictionary dict, FILE * fp) {
/* Reads in one word from the file, allocates space for it,
   and returns it.
*/
    char word[MAX_WORD+1];
    char * s;
    int c, j;
    do {
	c = getc(fp);
    } while ((c != EOF) && isspace(c));
    if (c == EOF) return NULL;

    for (j=0; (j <= MAX_WORD-1) && (!isspace(c)) && (c != EOF); j++) {
	word[j] = c;
	c = getc(fp);
    }

    if (j == MAX_WORD) {
	error("The dictionary contains a word that is too long.");
    }
    word[j] = '\0';
    s = string_set_add(word, dict->string_set);
    return s;
}

Dict_node * read_word_file(Dictionary dict, Dict_node * dn, char * filename) {
/*

   (1) opens the word file and adds it to the word file list
   (2) reads in the words
   (3) puts each word in a Dict_node
   (4) links these together by their left pointers at the
       front of the list pointed to by dn
   (5) returns a pointer to the first of this list

*/
    Dict_node * dn_new;
    Word_file * wf;
    FILE * fp;
    char * s;
    char file_name_copy[MAX_PATH_NAME+1];

    safe_strcpy(file_name_copy, filename+1, sizeof(file_name_copy)); /* get rid of leading '/' */

    if ((fp = dictopen(dict->name, file_name_copy, "r")) == NULL) {
	lperror(WORDFILE, "%s\n", file_name_copy);
	return NULL;
    }

    /*printf("   Reading \"%s\"\n", file_name_copy);*/
    /*printf("*"); fflush(stdout);*/

    wf = (Word_file *) xalloc(sizeof (Word_file));
    safe_strcpy(wf->file, file_name_copy, sizeof(wf->file));
    wf->changed = FALSE;
    wf->next = dict->word_file_header;
    dict->word_file_header = wf;

    while ((s = get_a_word(dict, fp)) != NULL) {
	dn_new = (Dict_node *) xalloc(sizeof(Dict_node));
	dn_new->left = dn;
	dn = dn_new;
	dn->string = s;
	dn->file = wf;
    }
    fclose(fp);
    return dn;
}

#define LINE_LIMIT 70
int xwhere_in_line;

void routput_dictionary(Dict_node * dn, FILE *fp, Word_file *wf) {
/* scan the entire dictionary rooted at dn and print into the file
   pf all of the words whose origin is the file wf.
*/
    if (dn == NULL) return;
    routput_dictionary(dn->left, fp, wf);
    if (dn->file == wf) {
	if (xwhere_in_line+strlen(dn->string) > LINE_LIMIT) {
	    xwhere_in_line = 0;
	    fprintf(fp,"\n");
	}
	xwhere_in_line += strlen(dn->string) + 1;
	fprintf(fp,"%s ", dn->string);
    }
    routput_dictionary(dn->right, fp, wf);
}

void output_dictionary(Dict_node * dn, FILE *fp, Word_file *wf) {
    xwhere_in_line = 0;
    routput_dictionary(dn, fp, wf);
    fprintf(fp,"\n");
}

void save_files(Dictionary dict) {
    Word_file *wf;
    FILE *fp;
    for (wf = dict->word_file_header; wf != NULL; wf = wf->next) {
	if (wf->changed) {
	    if ((fp = fopen(wf->file, "w")) == NULL) {
	     printf("\nCannot open %s. Gee, this shouldn't happen.\n", wf->file);
             printf("file not saved\n");
	     return;
	    }
	    printf("   saving file \"%s\"\n", wf->file);
	    /*output_dictionary(dict_root, fp, wf);*/
	    fclose(fp);
	    wf->changed = FALSE;
	}
    }
}

int files_need_saving(Dictionary dict) {
    Word_file *wf;
    for (wf = dict->word_file_header; wf != NULL; wf = wf->next) {
	if (wf->changed) return TRUE;
    }
    return FALSE;
}
