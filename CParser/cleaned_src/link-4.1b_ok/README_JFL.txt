
Compilation:

gcc all.c -o link

Execution:

./link < jfl_example.txt

Output attendu:

Found 3 linkages (3 had no P.P. violations) at null count 4
  Linkage 1, cost vector = (UNUSED=4 DIS=3 AND=0 LEN=38)

    +---------------------------------------------------Xp-------------------
    |                      +----------------------Bs---------------------+---
    |           +----Ost---+                    +--------MVi--------+    +--M
    +--Wd--+-SX-+   +--Ds--+-------Rn------+-Spx+---Ost--+          +--I-+   
    |      |    |   |      |               |    |        |          |    |   
LEFT-WALL I.p am.v an engineer.n [.] [Do] you are.v [?] You [have] to work.v 


--------------------------------+
-MVi---+                        |
Va-+   |     +-----Os-----+     |
+ID+   +--If-+     +--Ds--+     |
|  |   |     |     |      |     |
a lot to become.v an engineer.n .
