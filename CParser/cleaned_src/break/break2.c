#include "continue2-break2_macros.h"
#include "break2_vars.h"
void break_template() 
{
  // securing a loop with a break statement
  CHECK_INCR((*cpt), v) 
  stmt1; 
  CHECK_INCR((*cpt), v+1) 
  stmt2;
  CHECK_INCR((*cpt), v+2) 
  DECL_INIT(b, 0);
  CHECK_INCR((*cpt), v+3) 
  DECL_INIT(b_break, 0)
  CHECK_INCR((*cpt), v+4) 
  DECL_INIT(cnt_while, init_val)
  CHECK_INCR((*cpt), v+5) 
  start_while: 
  {
    RESET_CNT_BREAK(cnt_while, init_val, init_val+10)    
    if (!CHECK_INCR_COND(b, cnt_while, init_val, (cond != 0))) 
    {
       goto end_while;
    }
    CHECK_LOOP_INCR(cnt_while, init_val+1, b) 
    cond --; // stmt_while1
    CHECK_INCR(cnt_while, init_val+2) 
    stmt_while2;
    CHECK_INCR(cnt_while, init_val+3) 
    INIT(b_break, 0)
    CHECK_INCR(cnt_while, init_val+4) 
    DECL_INIT(cnt_if_break, init_break)
    CHECK_INCR(cnt_while, init_val+5) 
    if (CHECK_INCR_COND(b_break, cnt_while, init_val+6, break_condition)) 
    {
      CHECK_INCR(cnt_if_break, init_break);    
      stmt_if_break1;
      CHECK_INCR(cnt_if_break, init_break+1); 
    }  
    CHECK_INCR(cnt_while, init_val+7) // +8 -- end value if break
    CHECK_END_IF_BREAK(cnt_if_break, b_break, init_break+2,init_break) 
    CHECK_COND_INCR(cnt_while,init_val+8,!b_break)
    stmt_while3;
    CHECK_INCR(cnt_while, init_val+9) // +10 -- end value of loop
    goto start_while;
  }
  end_while:
  CHECK_INCR((*cpt), v+6) 
  CHECK_END_LOOP_BREAK(cnt_while, b, init_val+1, b_break, init_val+8)
  CHECK_INCR((*cpt), v+7) 
  stmt3;
  CHECK_INCR((*cpt), v+8) 
  return ; 
}
