#include <stdio.h>

// Already introduced macros
#define DECL_INIT(cnt,val) short cnt = val;
#define INIT(var, val) var = val;
#define CHECK_INCR(cnt,val) cnt = (cnt == val ? cnt + 1 : killcard());
#define INCR(cnt) cnt++;
#define CHECK_COND_INCR(cnt,val, b) cnt = ((cnt == val && b)? cnt + 1 : killcard());
#define CHECK_INCR_COND(b, cnt, val, cond) (b = (((cnt)++ != val) ? killcard() : cond))
#define INCR_COND(b, cnt, cond) (cnt++ && (b=cond))
#define CHECK_LOOP_INCR(cnt, val, b) cnt = (b ? (cnt + 1) : 0);
#define CHECK_END_LOOP(cnt_loop, b, val) if ( ! (cnt_loop == val && !b) )killcard();
#define RESET_CNT(cnt_while, val) cnt_while = !(cnt_while == 0 || cnt_while == val) ? killcard() : 0;

//int killcard() { printf("KILLCARD !\n"); }
int cpt_init = 12;
int * cpt = &cpt_init;
int v = 12;
#define break_template main
#define break_condition (cond == 2)

// CFI constants
#define stmt1 printf("stmt1");
#define stmt2 printf("stmt2");
#define stmt3 printf("stmt3");
#define stmt_while1 printf("while_stmt1 - ");
#define stmt_while2 printf("while_stmt2 - ");
#define stmt_while3 printf("while_stmt3");
#define stmt_if_break1 printf("stmt_break");
#define init_val  5
#define init_cont 3
#define init_break 9

// Input values for testing continue template
int cond = 6;

// Execution output: 
/*
stmt1
stmt2
while_stmt1 - while_stmt2 - while_stmt3
while_stmt1 - while_stmt2 - while_stmt3
while_stmt1 - while_stmt2 - stmt_cont
while_stmt1 - while_stmt2 - stmt_cont
while_stmt1 - while_stmt2 - while_stmt3
while_stmt1 - while_stmt2 - while_stmt3
stmt3
*/
