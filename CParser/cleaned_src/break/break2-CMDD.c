#include "continue2-break2_macros.h"
#include "break2_vars.h"

#define COND_INCR(cnt, cond) cnt = cond ? cnt + 1 : 0;
void break_template() 
{
  // securing a loop with a break statement
  INCR((*cpt)) 
  stmt1; 
  INCR((*cpt)) 
  stmt2;
  INCR((*cpt)) 
  DECL_INIT(b, 0);
  INCR((*cpt)) 
  DECL_INIT(b_break, 0)
  INCR((*cpt)) 
  DECL_INIT(cnt_while, init_val)
  INCR((*cpt)) 
  start_while: 
  {
    RESET_CNT_BREAK(cnt_while, init_val, init_val+10)    
    if (!INCR_COND(b, cnt_while, (cond != 0))) 
    {
       goto end_while;
    }
    CHECK_LOOP_INCR(cnt_while, init_val+1, b) 
    cond --; // stmt_while1
    INCR(cnt_while) 
    stmt_while2;
    INCR(cnt_while) 
    INIT(b_break, 0)
    INCR(cnt_while) 
    DECL_INIT(cnt_if_break, init_break)
    INCR(cnt_while) 
    if (INCR_COND(b_break, cnt_while,  break_condition)) 
    {
      INCR(cnt_if_break);    
      stmt_if_break1;
      INCR(cnt_if_break); 
    }  
    INCR(cnt_while) // +8 -- end value if break
    CHECK_END_IF_BREAK(cnt_if_break, b_break, init_break+2,init_break) 
    CHECK_COND_INCR(cnt_while,init_val+8,!b_break) // on doit pouvoir le virer aussi en mettant une valeur pourrie dans cnt_while si la condition n'est pas bonne, voir ci-dessus la macro et à mettre ici COND_INCR(cnt_while, !b_break)
    stmt_while3;
    INCR(cnt_while) // +10 -- end value of loop
    goto start_while;
  }
  end_while:
  INCR((*cpt)) 
  CHECK_END_LOOP_BREAK(cnt_while, b, init_val+1, b_break, init_val+8)
  INCR((*cpt)) 
  stmt3;
  CHECK_INCR((*cpt), v+8) 
  return ; 
}
