#include "continue2-break2_macros.h"
#include "break2_vars.h"
void break_template() {
	// a loop with a break statement
	stmt1 ;
	stmt2 ;
	while ( cond != 0) 
	{
		cond --; // stmt_while1
		stmt_while2 ;
		if ( break_condition )
		{
			stmt_if_break1 ;
			break; }
		stmt_while3 ;
	}
	stmt3 ;
}
