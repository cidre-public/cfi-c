// extra macro for in-loop continue statement
#define RESET_CNT_CONTINUE(cnt, val1, condition, val2, init_value) cnt = !(cnt == init_value || \
     (condition && (cnt == val2)) || cnt == val1)? killcard() : init_value;
#define CHECK_END_IF_CONTINUE(cnt, b, val, initv) if ( (b || cnt != initv) && \
     ((( cnt != val && b) || (!b && cnt != initv)) ? killcard() : 1) ) goto start_while;

// extra macro for in-loop break statement
#define RESET_CNT_BREAK(cnt_while, val1, val2) cnt_while = !(cnt_while == val1 || cnt_while == val2) ? killcard() : val1;
#define CHECK_END_IF_BREAK(cnt, b, val, initv) if ( (b || cnt != initv) && \
     ((( cnt != val && b) || (!b && cnt != initv)) ? killcard() : 1) ) goto end_while;
#define CHECK_END_LOOP_BREAK(cnt_loop, b, val1, b_break, val2) if (! (cnt_loop == val1 && !b || cnt_loop == val2 && b_break)) killcard();
