#include <stdio.h>
//include <stm32f10x.h>

#include "types.h"
#include "interface.h"
#include "commons.h"

// global variables definition
BOOL g_authenticated;
SBYTE g_ptc;
UBYTE g_countermeasure;
UBYTE g_userPin[PIN_SIZE];
UBYTE g_cardPin[PIN_SIZE];


// define the oracle you want to use.
#define AUTH
#ifdef AUTH
#define oracle_auth oracle
#endif

#ifdef PTC
#define oracle_ptc oracle
#endif

int resultat = 2;



void killcard() 
{
  resultat = -1;
  _sys_exit(6);
}

BOOL oracle_auth()
{
    return g_authenticated == 1;
}

BOOL oracle_ptc()
{
    return g_ptc >= 3;
}



void initialize()
{
   // local variables
   int i;
   // global variables initialization
   g_authenticated = BOOL_FALSE;
   g_ptc = 3;
 
   // card PIN = 1 2 3 4 5...
   for (i = 0; i < PIN_SIZE; ++i) 
   {
       g_cardPin[i] = i+1;
   }
   // user PIN = 0 0 0 0 0...
   for (i = 0 ; i < PIN_SIZE; ++i) 
   {
       g_userPin[i] = 0;
   }
   int jfl1;
}

#ifdef INLINE
inline BOOL byteArrayCompare(UBYTE* a1, UBYTE* a2, UBYTE size) __attribute__((always_inline))
#else
BOOL byteArrayCompare(UBYTE* a1, UBYTE* a2, UBYTE size)
#endif
{
  int i;
  for(i = 0; i < size; i++) 
  {
    if(a1[i] != a2[i]) 
    {
      return 0;
    }
  }
  return 1;
}

#if defined INLINE && defined PTC
inline BOOL verifyPIN_0() __attribute__((always_inline))
#else
BOOL verifyPIN_0()
#endif
{
  g_authenticated = 0;

  if(g_ptc > 0) 
  {
    if(byteArrayCompare(g_userPin, g_cardPin, PIN_SIZE) == 1) 
    {
      g_ptc = 3;
      g_authenticated = 1; // Authentication();
      return BOOL_TRUE;
    }
    else 
    {
      g_ptc--;
      return BOOL_FALSE;
    }
  }

  return BOOL_FALSE;
}


int main()
{
   SER_Init();
    initialize();
    verifyPIN_0();
    if (oracle()) 
    {
      resultat = 1;
    }
    else 
    {
      resultat = 0;
    }
    printf("res %d\n", resultat);
    _sys_exit(-1);
}


 /* main */
