#include "switch_vars.h"
#include "switch_macros.h"

void switch_template()
{
  // securing example of switch contruct
  CHECK_INCR(cpt,v)
  stmt_before;
  CHECK_INCR(cpt, v+1)
  DECL_INIT(cpt_sw, val_sw)
  CHECK_INCR(cpt, v+2)  
  DECL_INIT(n_sw, -1)
  CHECK_INCR(cpt, v+3)  
  // continued on the right side
  switch(INCR_SW(n_sw, c, cpt, v+4))
  {
  case a:
    INCR_ASSIGN(cpt, v+5, cpt_sw, 1000)
    CHECK_INCR(cpt_sw, 1100)
    stmt1_1;
    CHECK_INCR(cpt_sw, 1101)
    stmt1_2;
    CHECK_INCR(cpt_sw, 1102)
    break;
  case b:
    INCR_ASSIGN(cpt, v+5, cpt_sw, 2000)
    CHECK_INCR(cpt_sw, 2100)
    stmt2_1;
    CHECK_INCR(cpt_sw, 2101)
    stmt2_2;
    CHECK_INCR(cpt_sw, 2102)
    stmt2_3;
    CHECK_INCR(cpt_sw, 2103)
    break;
  default:
    INCR_ASSIGN(cpt, v+5, cpt_sw, 3000)
    CHECK_INCR(cpt_sw, 3100)
    stmt3_1;
    CHECK_INCR(cpt_sw, 3101)
    break;
  }				
CHECK_INCR(cpt, v+6)
  CHECK_END_SW(cpt_sw, n_sw, a, 1103 /*end case 1*/, b, 2104 /*end case 2*/, 3102 /*default*/)
CHECK_INCR(cpt, v+7)
  stmt_after;
CHECK_INCR(cpt, v+8)
}
