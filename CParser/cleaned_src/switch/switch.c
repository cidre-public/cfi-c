#include "switch_vars.h"
#include "switch_macros.h"

void switch_template()
{
  CHECK_INCR(cpt,v)
  stmt_before;
  CHECK_INCR(cpt, v+1)
  DECL_INIT(cpt_sw, val_sw)
  CHECK_INCR(cpt, v+2)  

  switch(INCR_SW(n_sw, c, cpt, v+3))
  {
    case a:
    INCR_ASSIGN(cpt, v+4, cpt_sw, v1)
    INCR(cpt_sw)
    stmt1_1;
    INCR(cpt_sw)
    stmt1_2;
    INCR(cpt_sw)
    break;
			        
    case b:
    INCR_ASSIGN(cpt, v+4, cpt_sw, v2)
    INCR(cpt_sw)
    stmt2_1;
    INCR(cpt_sw)
    stmt2_2;
    INCR(cpt_sw)
    stmt2_3;
    INCR(cpt_sw)
    break;
			        
    default:
    INCR_ASSIGN(cpt, v+4, cpt_sw, vdefault)
    INCR(cpt_sw)
    stmt3_1;
    INCR(cpt_sw)
    break;
  }				
  INCR(cpt)
  CHECK_END_SW(cpt_sw, n_sw, a, end_val1, b, end_val2, end_default)
  INCR(cpt)
  stmt_after;
  INCR(cpt)
}
