#include <stdio.h>
// Already introduced macros
#define DECL_INIT(cnt,val) short cnt = val;
#define CHECK_INCR(cnt,val) cnt = (cnt == val ? cnt + 1 : killcard());
#define INCR(cnt) cnt++;
//int killcard() { printf("KILLCARD !\n"); }
#define switch_template main

// CFI variables
int cpt = 0; // counter outside the switch
int cpt_sw; // counter inside the switch
int n_sw; // value of the choosen case

// CFI constants
#define v 0 // initial value for cpt
#define val_sw 100 // initial value for cpt_sw
#define v1 1000
#define v2 2000
#define vdefault 3000
#define end_val1 val_sw+v1+3
#define end_val2  val_sw+v2+4
#define end_default val_sw+vdefault+2

#define stmt_before printf("stmt_before") ;
#define stmt1_1 printf("stmt1_1");
#define stmt1_2 printf("stmt1_2");
#define stmt2_1 printf("stmt2_1");
#define stmt2_2 printf("stmt2_2");
#define stmt2_3 printf("stmt2_3");
#define stmt3_1 printf("stmt3_1");
#define stmt_after printf("stmt_after");

// Input values for testing switch_template
int c = 2; // choose the value for the switch
#define a 1 // Case a
#define b 2 // Case b

// Execution output:
/*
stmt_before
stmt2_1
stmt2_2
stmt2_3
stmt_after
*/
