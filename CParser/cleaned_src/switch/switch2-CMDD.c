#include "switch_vars.h"
#include "switch_macros.h"

void switch_template()
{
  // securing example of switch contruct
  INCR(cpt)
  stmt_before;
  INCR(cpt)
  DECL_INIT(cpt_sw, val_sw)
  INCR(cpt)  
  DECL_INIT(n_sw, -1)
  INCR(cpt)  
  // continued on the right side
  switch(INCR_SW(n_sw, c, cpt, v+4))
  {
  case a:
    INCR_ASSIGN(cpt, v+5, cpt_sw, 1000)
    INCR(cpt_sw)
    stmt1_1;
    INCR(cpt_sw)
    stmt1_2;
    CHECK_INCR(cpt_sw, 1102)
    break;
  case b:
    INCR_ASSIGN(cpt, v+5, cpt_sw, 2000)
    INCR(cpt_sw)
    stmt2_1;
    INCR(cpt_sw)
    stmt2_2;
    INCR(cpt_sw)
    stmt2_3;
    CHECK_INCR(cpt_sw, 2103)
    break;
  default:
    INCR_ASSIGN(cpt, v+5, cpt_sw, 3000)
    INCR(cpt_sw)
    stmt3_1;
    CHECK_INCR(cpt_sw, 3101)
    break;
  }				
INCR(cpt)
  CHECK_END_SW(cpt_sw, n_sw, a, 1103 /*end case 1*/, b, 2104 /*end case 2*/, 3102 /*default*/)
INCR(cpt)
  stmt_after;
CHECK_INCR(cpt, v+8)
}
