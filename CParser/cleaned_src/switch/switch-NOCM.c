#include "switch_vars.h"
#include "switch_macros.h"

void switch_template()
{
  stmt_before;
  switch(c)
  {
    case a:
    stmt1_1;
    stmt1_2;
    break;
			        
    case b:
    stmt2_1;
    stmt2_2;
    stmt2_3;
    break;
			        
    default:
    stmt3_1;
    break;
  }				
  stmt_after;
}
