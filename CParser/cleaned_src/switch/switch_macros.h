// Additional macros for switch construct
#define INCR_SW(n_sw, c, cpt, val) n_sw = ((cpt++ == val) ? c : killcard())
#define INCR_ASSIGN(cpt, val, cpt_sw, v1) cpt_sw += (cpt++ == val)? v1 : killcard();
#define CHECK_END_SW(cpt_sw, n_sw, val1, end_val1, val2, end_val2, end_default) \
        if (((n_sw == val1) &&( cpt_sw != end_val1))|| ((n_sw == val2) && (cpt_sw != end_val2)) \
             || (((n_sw != val1) && (n_sw != val2)) && (cpt_sw != end_default))) killcard();


