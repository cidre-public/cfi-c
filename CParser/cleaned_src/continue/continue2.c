#include "continue2-break2_macros.h"
#include "continue2_vars.h"
void continue_template() 
{
  // securing a loop with a continue statement
  CHECK_INCR((*cpt), v) 
  stmt1; 
  CHECK_INCR((*cpt), v+1) 
  stmt2;
  CHECK_INCR((*cpt), v+2) 
  DECL_INIT(b, 0);
  CHECK_INCR((*cpt), v+3) 
  DECL_INIT(b_cont, 0)
  CHECK_INCR((*cpt), v+4) 
  DECL_INIT(cnt_while, init_val)
  CHECK_INCR((*cpt), v+5) 
  start_while: 
  { 
    RESET_CNT_CONTINUE(cnt_while, init_val+10, b_cont, init_val+8, init_val)
    if (!CHECK_INCR_COND(b, cnt_while, init_val, (cond != 0)))
    {
       goto end_while;
    }
    CHECK_LOOP_INCR(cnt_while, init_val+1, b) 
    cond --; // stmt_while1
    CHECK_INCR(cnt_while, init_val+2) 
    stmt_while2;
    CHECK_INCR(cnt_while, init_val+3) 
    INIT(b_cont, 0)
    CHECK_INCR(cnt_while, init_val+4) 
    DECL_INIT(cnt_if_cont, init_cont)
    CHECK_INCR(cnt_while, init_val+5) 
    if (CHECK_INCR_COND(b_cont, cnt_while, init_val+6, continue_condition)) 
    { 
       CHECK_INCR(cnt_if_cont, init_cont);    
       stmt_if_cont1;
       CHECK_INCR(cnt_if_cont, init_cont+1); 
    }  
    CHECK_INCR(cnt_while, init_val+7)    //+8 -- end value if continue
    CHECK_END_IF_CONTINUE(cnt_if_cont, b_cont,init_cont+2, init_cont) 
    CHECK_COND_INCR(cnt_while,init_val+8,!b_cont) 
    stmt_while3;
    CHECK_INCR(cnt_while, init_val+9)  // +10 -- end value of loop
    goto start_while;
  }
  end_while:
  CHECK_INCR((*cpt), v+6) 
  CHECK_END_LOOP(cnt_while, b, init_val+1)
  CHECK_INCR((*cpt), v+7) 
  stmt3;
  CHECK_INCR((*cpt), v+8) 
  return ; 
}
