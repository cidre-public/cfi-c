#include "continue2-break2_macros.h"
#include "continue2_vars.h"
void continue_template() {
  stmt1; 
  stmt2;
  while ( cond != 0) 
  {
	  cond --; // stmt_while1
	  stmt_while2 ;
	  if ( continue_condition )
	  {
		  stmt_if_cont1 ;
		  continue; 
          }
	  stmt_while3 ;
  }
  stmt3 ;
  return ; 
}
