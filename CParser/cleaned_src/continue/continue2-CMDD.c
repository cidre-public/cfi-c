#include "continue2-break2_macros.h"
#include "continue2_vars.h"

#define COND_INCR(cnt, cond) cnt = cond ? cnt + 1 : 0;
void continue_template() 
{
  // securing a loop with a continue statement
  INCR((*cpt)) 
  stmt1; 
  INCR((*cpt)) 
  stmt2;
  INCR((*cpt)) 
  DECL_INIT(b, 0);
  INCR((*cpt)) 
  DECL_INIT(b_cont, 0)
  INCR((*cpt)) 
  DECL_INIT(cnt_while, init_val)
  INCR((*cpt)) 
  start_while: 
  { 
    RESET_CNT_CONTINUE(cnt_while, init_val+10, b_cont, init_val+8, init_val)
    if (!INCR_COND(b, cnt_while, (cond != 0))) 
    {
       goto end_while;
    }
    CHECK_LOOP_INCR(cnt_while, init_val+1, b) 
    cond --; // stmt_while1
    INCR(cnt_while) 
    stmt_while2;
    INCR(cnt_while) 
    INIT(b_cont, 0)
    INCR(cnt_while) 
    DECL_INIT(cnt_if_cont, init_cont)
    INCR(cnt_while) 
    if (INCR_COND(b_cont, cnt_while, continue_condition)) 
    { 
       INCR(cnt_if_cont);    
       stmt_if_cont1;
       INCR(cnt_if_cont); 
    }  
    INCR(cnt_while)    //+8 -- end value if continue
    CHECK_END_IF_CONTINUE(cnt_if_cont, b_cont,init_cont+2, init_cont) 
    CHECK_COND_INCR(cnt_while,init_val+8,!b_cont) // on doit pouvoir le virer aussi en mettant une valeur pourrie dans cnt_while si la condition n'est pas bonne, voir ci-dessus la macro et à mettre ici COND_INCR(cnt_while, !b_cont)
    stmt_while3;
    CHECK_INCR(cnt_while, init_val+9)  // +10 -- end value of loop
    goto start_while;
  }
  end_while:
  INCR((*cpt)) 
  CHECK_END_LOOP(cnt_while, b, init_val+1)
  INCR((*cpt)) 
  stmt3;
  CHECK_INCR((*cpt), v+8) 
  return ; 
}
