	.file	"all.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"   Copyright (C) 1992-1993 Jean-loup Gailly"
	.align 8
.LC1:
	.string	"   This program is free software; you can redistribute it and/or modify"
	.align 8
.LC2:
	.string	"   it under the terms of the GNU General Public License as published by"
	.align 8
.LC3:
	.string	"   the Free Software Foundation; either version 2, or (at your option)"
.LC4:
	.string	"   any later version."
.LC5:
	.string	""
	.align 8
.LC6:
	.string	"   This program is distributed in the hope that it will be useful,"
	.align 8
.LC7:
	.string	"   but WITHOUT ANY WARRANTY; without even the implied warranty of"
	.align 8
.LC8:
	.string	"   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
	.align 8
.LC9:
	.string	"   GNU General Public License for more details."
	.align 8
.LC10:
	.string	"   You should have received a copy of the GNU General Public License"
	.align 8
.LC11:
	.string	"   along with this program; if not, write to the Free Software"
	.align 8
.LC12:
	.string	"   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA."
	.data
	.align 32
	.type	license_msg, @object
	.size	license_msg, 120
license_msg:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC5
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	0
	.comm	inbuf,32832,32
	.comm	outbuf,18432,32
	.comm	d_buf,65536,32
	.comm	window,65536,32
	.comm	prev,131072,32
	.globl	ascii
	.bss
	.align 4
	.type	ascii, @object
	.size	ascii, 4
ascii:
	.zero	4
	.globl	to_stdout
	.align 4
	.type	to_stdout, @object
	.size	to_stdout, 4
to_stdout:
	.zero	4
	.globl	decompress
	.align 4
	.type	decompress, @object
	.size	decompress, 4
decompress:
	.zero	4
	.globl	force
	.align 4
	.type	force, @object
	.size	force, 4
force:
	.zero	4
	.globl	no_name
	.data
	.align 4
	.type	no_name, @object
	.size	no_name, 4
no_name:
	.long	-1
	.globl	no_time
	.align 4
	.type	no_time, @object
	.size	no_time, 4
no_time:
	.long	-1
	.globl	recursive
	.bss
	.align 4
	.type	recursive, @object
	.size	recursive, 4
recursive:
	.zero	4
	.globl	list
	.align 4
	.type	list, @object
	.size	list, 4
list:
	.zero	4
	.globl	verbose
	.align 4
	.type	verbose, @object
	.size	verbose, 4
verbose:
	.zero	4
	.globl	quiet
	.align 4
	.type	quiet, @object
	.size	quiet, 4
quiet:
	.zero	4
	.globl	do_lzw
	.align 4
	.type	do_lzw, @object
	.size	do_lzw, 4
do_lzw:
	.zero	4
	.globl	test
	.align 4
	.type	test, @object
	.size	test, 4
test:
	.zero	4
	.comm	foreground,4,4
	.comm	progname,8,8
	.globl	maxbits
	.data
	.align 4
	.type	maxbits, @object
	.size	maxbits, 4
maxbits:
	.long	16
	.globl	method
	.align 4
	.type	method, @object
	.size	method, 4
method:
	.long	8
	.globl	level
	.align 4
	.type	level, @object
	.size	level, 4
level:
	.long	6
	.globl	exit_code
	.bss
	.align 4
	.type	exit_code, @object
	.size	exit_code, 4
exit_code:
	.zero	4
	.comm	save_orig_name,4,4
	.comm	last_member,4,4
	.comm	part_nb,4,4
	.comm	time_stamp,8,8
	.comm	ifile_size,8,8
	.comm	env,8,8
	.globl	args
	.align 8
	.type	args, @object
	.size	args, 8
args:
	.zero	8
	.comm	z_suffix,31,16
	.comm	z_len,4,4
	.comm	bytes_in,8,8
	.comm	bytes_out,8,8
	.globl	total_in
	.align 8
	.type	total_in, @object
	.size	total_in, 8
total_in:
	.zero	8
	.globl	total_out
	.align 8
	.type	total_out, @object
	.size	total_out, 8
total_out:
	.zero	8
	.comm	ifname,1024,32
	.comm	ofname,1024,32
	.globl	remove_ofname
	.align 4
	.type	remove_ofname, @object
	.size	remove_ofname, 4
remove_ofname:
	.zero	4
	.comm	istat,144,32
	.comm	ifd,4,4
	.comm	ofd,4,4
	.comm	insize,4,4
	.comm	inptr,4,4
	.comm	outcnt,4,4
	.globl	longopts
	.section	.rodata
.LC13:
	.string	"ascii"
.LC14:
	.string	"to-stdout"
.LC15:
	.string	"stdout"
.LC16:
	.string	"decompress"
.LC17:
	.string	"uncompress"
.LC18:
	.string	"force"
.LC19:
	.string	"help"
.LC20:
	.string	"list"
.LC21:
	.string	"license"
.LC22:
	.string	"no-name"
.LC23:
	.string	"name"
.LC24:
	.string	"quiet"
.LC25:
	.string	"silent"
.LC26:
	.string	"recursive"
.LC27:
	.string	"suffix"
.LC28:
	.string	"test"
.LC29:
	.string	"no-time"
.LC30:
	.string	"verbose"
.LC31:
	.string	"version"
.LC32:
	.string	"fast"
.LC33:
	.string	"best"
.LC34:
	.string	"lzw"
.LC35:
	.string	"bits"
	.data
	.align 32
	.type	longopts, @object
	.size	longopts, 768
longopts:
	.quad	.LC13
	.long	0
	.zero	4
	.quad	0
	.long	97
	.zero	4
	.quad	.LC14
	.long	0
	.zero	4
	.quad	0
	.long	99
	.zero	4
	.quad	.LC15
	.long	0
	.zero	4
	.quad	0
	.long	99
	.zero	4
	.quad	.LC16
	.long	0
	.zero	4
	.quad	0
	.long	100
	.zero	4
	.quad	.LC17
	.long	0
	.zero	4
	.quad	0
	.long	100
	.zero	4
	.quad	.LC18
	.long	0
	.zero	4
	.quad	0
	.long	102
	.zero	4
	.quad	.LC19
	.long	0
	.zero	4
	.quad	0
	.long	104
	.zero	4
	.quad	.LC20
	.long	0
	.zero	4
	.quad	0
	.long	108
	.zero	4
	.quad	.LC21
	.long	0
	.zero	4
	.quad	0
	.long	76
	.zero	4
	.quad	.LC22
	.long	0
	.zero	4
	.quad	0
	.long	110
	.zero	4
	.quad	.LC23
	.long	0
	.zero	4
	.quad	0
	.long	78
	.zero	4
	.quad	.LC24
	.long	0
	.zero	4
	.quad	0
	.long	113
	.zero	4
	.quad	.LC25
	.long	0
	.zero	4
	.quad	0
	.long	113
	.zero	4
	.quad	.LC26
	.long	0
	.zero	4
	.quad	0
	.long	114
	.zero	4
	.quad	.LC27
	.long	1
	.zero	4
	.quad	0
	.long	83
	.zero	4
	.quad	.LC28
	.long	0
	.zero	4
	.quad	0
	.long	116
	.zero	4
	.quad	.LC29
	.long	0
	.zero	4
	.quad	0
	.long	84
	.zero	4
	.quad	.LC30
	.long	0
	.zero	4
	.quad	0
	.long	118
	.zero	4
	.quad	.LC31
	.long	0
	.zero	4
	.quad	0
	.long	86
	.zero	4
	.quad	.LC32
	.long	0
	.zero	4
	.quad	0
	.long	49
	.zero	4
	.quad	.LC33
	.long	0
	.zero	4
	.quad	0
	.long	57
	.zero	4
	.quad	.LC34
	.long	0
	.zero	4
	.quad	0
	.long	90
	.zero	4
	.quad	.LC35
	.long	1
	.zero	4
	.quad	0
	.long	98
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.globl	work
	.align 8
	.type	work, @object
	.size	work, 8
work:
	.quad	zip
	.section	.rodata
	.align 8
.LC36:
	.string	"usage: %s [-%scdfhlLnN%stvV19] [-S suffix] [file ...]\n"
	.text
	.type	usage, @function
usage:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progname(%rip), %rdx
	movl	$.LC36, %esi
	movq	stderr(%rip), %rax
	movl	$.LC5, %r8d
	movl	$.LC5, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	usage, .-usage
	.section	.rodata
.LC37:
	.string	"%s %s (%s)\n"
.LC38:
	.string	"18 Aug 93"
.LC39:
	.string	"1.2.4"
.LC40:
	.string	"%s\n"
	.text
	.type	help, @function
help:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	$help_msg.3671, -8(%rbp)
	movq	progname(%rip), %rdx
	movl	$.LC37, %esi
	movq	stderr(%rip), %rax
	movl	$.LC38, %r8d
	movl	$.LC39, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	usage
	jmp	.L3
.L4:
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	addq	$8, -8(%rbp)
	movl	$.LC40, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L3:
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L4
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	help, .-help
	.type	license, @function
license:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	$license_msg, -8(%rbp)
	movq	progname(%rip), %rdx
	movl	$.LC37, %esi
	movq	stderr(%rip), %rax
	movl	$.LC38, %r8d
	movl	$.LC39, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L6
.L7:
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	addq	$8, -8(%rbp)
	movl	$.LC40, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L6:
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L7
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	license, .-license
	.section	.rodata
.LC41:
	.string	"Compilation options:\n%s %s "
.LC42:
	.string	"UTIME"
.LC43:
	.string	"NO_DIR"
	.text
	.type	version, @function
version:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progname(%rip), %rdx
	movl	$.LC37, %esi
	movq	stderr(%rip), %rax
	movl	$.LC38, %r8d
	movl	$.LC39, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$.LC41, %esi
	movq	stderr(%rip), %rax
	movl	$.LC42, %ecx
	movl	$.LC43, %edx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$10, %edi
	call	fputc
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	version, .-version
	.section	.rodata
.LC44:
	.string	".exe"
.LC45:
	.string	"GZIP"
.LC46:
	.string	"un"
.LC47:
	.string	"gun"
.LC48:
	.string	"cat"
.LC49:
	.string	"gzcat"
.LC50:
	.string	".gz"
	.align 8
.LC51:
	.string	"%s: -r not supported on this system\n"
	.align 8
.LC52:
	.string	"%s: -Z not supported in this version\n"
	.align 8
.LC53:
	.string	"ab:cdfhH?lLmMnNqrS:tvVZ123456789"
	.align 8
.LC54:
	.string	"%s: option --ascii ignored on this system\n"
.LC55:
	.string	"%s: incorrect suffix '%s'\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	basename
	movq	%rax, progname(%rip)
	movq	progname(%rip), %rax
	movq	$-1, -40(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-40(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -12(%rbp)
	cmpl	$4, -12(%rbp)
	jle	.L10
	movq	progname(%rip), %rax
	movl	-12(%rbp), %edx
	movslq	%edx, %rdx
	subq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	movl	$.LC44, %eax
	movl	$5, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L10
	movq	progname(%rip), %rax
	movl	-12(%rbp), %edx
	movslq	%edx, %rdx
	subq	$4, %rdx
	addq	%rdx, %rax
	movb	$0, (%rax)
.L10:
	leaq	-32(%rbp), %rcx
	leaq	-20(%rbp), %rax
	movl	$.LC45, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	add_envopt
	movq	%rax, env(%rip)
	movq	env(%rip), %rax
	testq	%rax, %rax
	je	.L11
	movq	-32(%rbp), %rax
	movq	%rax, args(%rip)
.L11:
	movl	$1, %esi
	movl	$2, %edi
	call	signal
	cmpq	$1, %rax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, foreground(%rip)
	movl	foreground(%rip), %eax
	testl	%eax, %eax
	je	.L12
	movl	$abort_gzip, %eax
	movq	%rax, %rsi
	movl	$2, %edi
	call	signal
.L12:
	movl	$1, %esi
	movl	$15, %edi
	call	signal
	cmpq	$1, %rax
	je	.L13
	movl	$abort_gzip, %eax
	movq	%rax, %rsi
	movl	$15, %edi
	call	signal
.L13:
	movl	$1, %esi
	movl	$1, %edi
	call	signal
	cmpq	$1, %rax
	je	.L14
	movl	$abort_gzip, %eax
	movq	%rax, %rsi
	movl	$1, %edi
	call	signal
.L14:
	movq	progname(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC46, %eax
	movl	$2, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L15
	movq	progname(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC47, %eax
	movl	$3, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L16
.L15:
	movl	$1, decompress(%rip)
	jmp	.L17
.L16:
	movq	progname(%rip), %rax
	addq	$1, %rax
	movq	%rax, %rdx
	movl	$.LC48, %eax
	movl	$4, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L18
	movq	progname(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC49, %eax
	movl	$6, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L17
.L18:
	movl	$1, to_stdout(%rip)
	movl	to_stdout(%rip), %eax
	movl	%eax, decompress(%rip)
.L17:
	movl	$.LC50, %eax
	movl	$30, %edx
	movq	%rax, %rsi
	movl	$z_suffix, %edi
	call	strncpy
	movl	$z_suffix, %eax
	movq	$-1, -40(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-40(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, z_len(%rip)
	jmp	.L19
.L42:
	movl	-8(%rbp), %eax
	subl	$49, %eax
	cmpl	$69, %eax
	ja	.L20
	movl	%eax, %eax
	movq	.L41(,%rax,8), %rax
	jmp	*%rax
	.section	.rodata
	.align 8
	.align 4
.L41:
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L21
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L22
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L22
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L23
	.quad	.L24
	.quad	.L25
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L26
	.quad	.L20
	.quad	.L20
	.quad	.L27
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L28
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L29
	.quad	.L30
	.quad	.L31
	.quad	.L32
	.quad	.L20
	.quad	.L33
	.quad	.L20
	.quad	.L22
	.quad	.L20
	.quad	.L20
	.quad	.L20
	.quad	.L34
	.quad	.L35
	.quad	.L36
	.quad	.L20
	.quad	.L20
	.quad	.L37
	.quad	.L38
	.quad	.L20
	.quad	.L39
	.quad	.L20
	.quad	.L40
	.text
.L29:
	movl	$1, ascii(%rip)
	jmp	.L19
.L30:
	movq	optarg(%rip), %rax
	movq	%rax, %rdi
	call	atoi
	movl	%eax, maxbits(%rip)
	jmp	.L19
.L31:
	movl	$1, to_stdout(%rip)
	jmp	.L19
.L32:
	movl	$1, decompress(%rip)
	jmp	.L19
.L33:
	movl	force(%rip), %eax
	addl	$1, %eax
	movl	%eax, force(%rip)
	jmp	.L19
.L22:
	call	help
	movl	$0, %edi
	call	do_exit
	jmp	.L19
.L34:
	movl	$1, to_stdout(%rip)
	movl	to_stdout(%rip), %eax
	movl	%eax, decompress(%rip)
	movl	decompress(%rip), %eax
	movl	%eax, list(%rip)
	jmp	.L19
.L23:
	call	license
	movl	$0, %edi
	call	do_exit
	jmp	.L19
.L35:
	movl	$1, no_time(%rip)
	jmp	.L19
.L24:
	movl	$0, no_time(%rip)
	jmp	.L19
.L36:
	movl	$1, no_time(%rip)
	movl	no_time(%rip), %eax
	movl	%eax, no_name(%rip)
	jmp	.L19
.L25:
	movl	$0, no_time(%rip)
	movl	no_time(%rip), %eax
	movl	%eax, no_name(%rip)
	jmp	.L19
.L37:
	movl	$1, quiet(%rip)
	movl	$0, verbose(%rip)
	jmp	.L19
.L38:
	movq	progname(%rip), %rdx
	movl	$.LC51, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	usage
	movl	$1, %edi
	call	do_exit
	jmp	.L19
.L26:
	movq	optarg(%rip), %rax
	movq	$-1, -40(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-40(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, z_len(%rip)
	movq	optarg(%rip), %rax
	movq	%rax, %rsi
	movl	$z_suffix, %edi
	call	strcpy
	jmp	.L19
.L39:
	movl	$1, to_stdout(%rip)
	movl	to_stdout(%rip), %eax
	movl	%eax, decompress(%rip)
	movl	decompress(%rip), %eax
	movl	%eax, test(%rip)
	jmp	.L19
.L40:
	movl	verbose(%rip), %eax
	addl	$1, %eax
	movl	%eax, verbose(%rip)
	movl	$0, quiet(%rip)
	jmp	.L19
.L27:
	call	version
	movl	$0, %edi
	call	do_exit
	jmp	.L19
.L28:
	movq	progname(%rip), %rdx
	movl	$.LC52, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	usage
	movl	$1, %edi
	call	do_exit
	jmp	.L19
.L21:
	movl	-8(%rbp), %eax
	subl	$48, %eax
	movl	%eax, level(%rip)
	jmp	.L19
.L20:
	call	usage
	movl	$1, %edi
	call	do_exit
.L19:
	movq	-32(%rbp), %rsi
	movl	-20(%rbp), %eax
	movl	$0, %r8d
	movl	$longopts, %ecx
	movl	$.LC53, %edx
	movl	%eax, %edi
	call	getopt_long
	movl	%eax, -8(%rbp)
	cmpl	$-1, -8(%rbp)
	jne	.L42
	movl	no_time(%rip), %eax
	testl	%eax, %eax
	jns	.L43
	movl	decompress(%rip), %eax
	movl	%eax, no_time(%rip)
.L43:
	movl	no_name(%rip), %eax
	testl	%eax, %eax
	jns	.L44
	movl	decompress(%rip), %eax
	movl	%eax, no_name(%rip)
.L44:
	movl	-20(%rbp), %edx
	movl	optind(%rip), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -4(%rbp)
	movl	ascii(%rip), %eax
	testl	%eax, %eax
	je	.L45
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L45
	movq	progname(%rip), %rdx
	movl	$.LC54, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L45:
	movl	z_len(%rip), %eax
	testl	%eax, %eax
	jne	.L46
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L47
.L46:
	movl	z_len(%rip), %eax
	cmpl	$30, %eax
	jle	.L48
.L47:
	movq	optarg(%rip), %rcx
	movq	progname(%rip), %rdx
	movl	$.LC55, %esi
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	do_exit
.L48:
	movl	do_lzw(%rip), %eax
	testl	%eax, %eax
	je	.L49
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	jne	.L49
	movq	$lzw, work(%rip)
.L49:
	cmpl	$0, -4(%rbp)
	je	.L50
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L56
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L56
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L56
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	jmp	.L56
.L53:
	movq	-32(%rbp), %rdx
	movl	optind(%rip), %eax
	movslq	%eax, %rcx
	salq	$3, %rcx
	addq	%rcx, %rdx
	movq	(%rdx), %rdx
	addl	$1, %eax
	movl	%eax, optind(%rip)
	movq	%rdx, %rdi
	call	treat_file
	jmp	.L52
.L56:
	nop
.L52:
	movl	optind(%rip), %edx
	movl	-20(%rbp), %eax
	cmpl	%eax, %edx
	jl	.L53
	jmp	.L54
.L50:
	call	treat_stdin
.L54:
	movl	list(%rip), %eax
	testl	%eax, %eax
	je	.L55
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L55
	cmpl	$1, -4(%rbp)
	jle	.L55
	movl	$-1, %esi
	movl	$-1, %edi
	call	do_list
.L55:
	movl	exit_code(%rip), %eax
	movl	%eax, %edi
	call	do_exit
	movl	exit_code(%rip), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	main, .-main
	.section	.rodata
.LC56:
	.string	"de"
.LC57:
	.string	"read from"
.LC58:
	.string	"written to"
	.align 8
.LC59:
	.string	"%s: compressed data not %s a terminal. Use -f to force %scompression.\n"
.LC60:
	.string	"For help, type: %s -h\n"
.LC61:
	.string	"stdin"
.LC62:
	.string	"fstat(stdin)"
.LC63:
	.string	" OK\n"
	.text
	.type	treat_stdin, @function
treat_stdin:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	movl	force(%rip), %eax
	testl	%eax, %eax
	jne	.L58
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L58
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L59
	movq	stdin(%rip), %rax
	jmp	.L60
.L59:
	movq	stdout(%rip), %rax
.L60:
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %edi
	movl	$0, %eax
	call	isatty
	testl	%eax, %eax
	je	.L58
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L61
	movl	$.LC56, %edx
	jmp	.L62
.L61:
	movl	$.LC5, %edx
.L62:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L63
	movl	$.LC57, %eax
	jmp	.L64
.L63:
	movl	$.LC58, %eax
.L64:
	movq	progname(%rip), %r9
	movl	$.LC59, %esi
	movq	stderr(%rip), %rcx
	movq	%rcx, %rdi
	movq	%rdx, %r8
	movq	%rax, %rcx
	movq	%r9, %rdx
	movl	$0, %eax
	call	fprintf
	movq	progname(%rip), %rdx
	movl	$.LC60, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	do_exit
.L58:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L66
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L66
	movl	decompress(%rip), %eax
	testl	%eax, %eax
.L66:
	movl	$.LC61, %eax
	movl	(%rax), %edx
	movl	%edx, ifname(%rip)
	movzwl	4(%rax), %eax
	movw	%ax, ifname+4(%rip)
	movl	$.LC15, %eax
	movl	(%rax), %edx
	movl	%edx, ofname(%rip)
	movzwl	4(%rax), %edx
	movw	%dx, ofname+4(%rip)
	movzbl	6(%rax), %eax
	movb	%al, ofname+6(%rip)
	movq	$0, time_stamp(%rip)
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L67
	movl	no_time(%rip), %eax
	testl	%eax, %eax
	jne	.L68
.L67:
	movq	stdin(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	$istat, %esi
	movl	%eax, %edi
	call	fstat
	testl	%eax, %eax
	je	.L69
	movl	$.LC62, %edi
	call	error
.L69:
	movq	istat+88(%rip), %rax
	movq	%rax, time_stamp(%rip)
.L68:
	movq	$-1, ifile_size(%rip)
	call	clear_bufs
	movl	$1, to_stdout(%rip)
	movl	$0, part_nb(%rip)
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L70
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	call	get_method
	movl	%eax, method(%rip)
	movl	method(%rip), %eax
	testl	%eax, %eax
	jns	.L70
	movl	exit_code(%rip), %eax
	movl	%eax, %edi
	call	do_exit
.L70:
	movl	list(%rip), %eax
	testl	%eax, %eax
	je	.L71
	movl	method(%rip), %edx
	movl	ifd(%rip), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	do_list
	jmp	.L57
.L71:
	movq	work(%rip), %r12
	movq	stdout(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %ebx
	movq	stdin(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%ebx, %esi
	movl	%eax, %edi
	call	*%r12
	testl	%eax, %eax
	jne	.L77
.L73:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L74
	movl	last_member(%rip), %eax
	testl	%eax, %eax
	jne	.L74
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	je	.L74
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	call	get_method
	movl	%eax, method(%rip)
	movl	method(%rip), %eax
	testl	%eax, %eax
	js	.L78
.L75:
	movq	$0, bytes_out(%rip)
	jmp	.L71
.L74:
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L57
	movl	test(%rip), %eax
	testl	%eax, %eax
	je	.L76
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC63, %eax
	movq	%rdx, %rcx
	movl	$4, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	jmp	.L57
.L76:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	jne	.L57
	movq	stderr(%rip), %rdx
	movq	bytes_in(%rip), %rax
	movq	header_bytes(%rip), %rsi
	movq	bytes_out(%rip), %rcx
	subq	%rcx, %rsi
	movq	bytes_in(%rip), %rcx
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	display_ratio
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$10, %edi
	call	fputc
	jmp	.L57
.L77:
	nop
	jmp	.L57
.L78:
	nop
.L57:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	treat_stdin, .-treat_stdin
	.section	.rodata
.LC64:
	.string	"-"
	.align 8
.LC65:
	.string	"%s: %s is a directory -- ignored\n"
	.align 8
.LC66:
	.string	"%s: %s is not a directory or a regular file - ignored\n"
	.align 8
.LC67:
	.string	"%s: %s has %d other link%c -- unchanged\n"
.LC68:
	.string	"%s: "
.LC69:
	.string	"%s: %s compressed to %s\n"
.LC70:
	.string	"\t"
.LC71:
	.string	"\t\t"
.LC72:
	.string	"%s:\t%s"
.LC73:
	.string	" OK"
.LC74:
	.string	" -- replaced with %s"
	.text
	.type	treat_file, @function
treat_file:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdx
	movl	$.LC64, %eax
	movl	$2, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L80
	movl	to_stdout(%rip), %eax
	movl	%eax, -4(%rbp)
	call	treat_stdin
	movl	-4(%rbp), %eax
	movl	%eax, to_stdout(%rip)
	jmp	.L79
.L80:
	movq	-24(%rbp), %rax
	movl	$istat, %esi
	movq	%rax, %rdi
	call	get_istat
	testl	%eax, %eax
	jne	.L122
.L82:
	movl	istat+24(%rip), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L83
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L84
	movq	progname(%rip), %rdx
	movl	$.LC65, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L84:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L123
	movl	$2, exit_code(%rip)
	jmp	.L123
.L83:
	movl	istat+24(%rip), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L86
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L87
	movq	progname(%rip), %rdx
	movl	$.LC66, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L87:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L124
	movl	$2, exit_code(%rip)
	jmp	.L124
.L86:
	movq	istat+16(%rip), %rax
	cmpq	$1, %rax
	jbe	.L89
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	jne	.L89
	movl	force(%rip), %eax
	testl	%eax, %eax
	jne	.L89
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L90
	movq	istat+16(%rip), %rax
	cmpq	$2, %rax
	jbe	.L91
	movl	$115, %eax
	jmp	.L92
.L91:
	movl	$32, %eax
.L92:
	movq	istat+16(%rip), %rdx
	leal	-1(%rdx), %r8d
	movq	progname(%rip), %rdx
	movl	$.LC67, %esi
	movq	stderr(%rip), %rcx
	movq	%rcx, %rdi
	movl	%eax, %r9d
	movl	$ifname, %ecx
	movl	$0, %eax
	call	fprintf
.L90:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L125
	movl	$2, exit_code(%rip)
	jmp	.L125
.L89:
	movq	istat+48(%rip), %rax
	movq	%rax, ifile_size(%rip)
	movl	no_time(%rip), %eax
	testl	%eax, %eax
	je	.L94
	movl	list(%rip), %eax
	testl	%eax, %eax
	je	.L95
.L94:
	movq	istat+88(%rip), %rax
	jmp	.L96
.L95:
	movl	$0, %eax
.L96:
	movq	%rax, time_stamp(%rip)
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L97
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L97
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L97
	movl	$.LC15, %eax
	movl	(%rax), %edx
	movl	%edx, ofname(%rip)
	movzwl	4(%rax), %edx
	movw	%dx, ofname+4(%rip)
	movzbl	6(%rax), %eax
	movb	%al, ofname+6(%rip)
	jmp	.L98
.L97:
	call	make_ofname
	testl	%eax, %eax
	jne	.L126
.L98:
	movl	$384, %edx
	movl	$0, %esi
	movl	$ifname, %edi
	movl	$0, %eax
	call	open
	movl	%eax, ifd(%rip)
	movl	ifd(%rip), %eax
	cmpl	$-1, %eax
	jne	.L99
	movq	progname(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$ifname, %edi
	call	perror
	movl	$1, exit_code(%rip)
	jmp	.L79
.L99:
	call	clear_bufs
	movl	$0, part_nb(%rip)
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L100
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	call	get_method
	movl	%eax, method(%rip)
	movl	method(%rip), %eax
	testl	%eax, %eax
	jns	.L100
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	jmp	.L79
.L100:
	movl	list(%rip), %eax
	testl	%eax, %eax
	je	.L101
	movl	method(%rip), %edx
	movl	ifd(%rip), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	do_list
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	jmp	.L79
.L101:
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L102
	movq	stdout(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, ofd(%rip)
	jmp	.L103
.L102:
	call	create_outfile
	testl	%eax, %eax
	jne	.L127
.L104:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	jne	.L103
	movl	save_orig_name(%rip), %eax
	testl	%eax, %eax
	je	.L103
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	jne	.L103
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L103
	movq	progname(%rip), %rdx
	movl	$.LC69, %esi
	movq	stderr(%rip), %rax
	movl	$ofname, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L103:
	movl	save_orig_name(%rip), %eax
	testl	%eax, %eax
	jne	.L105
	movl	no_name(%rip), %eax
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	movl	%eax, save_orig_name(%rip)
.L105:
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L106
	movl	$ifname, %eax
	movq	$-1, -32(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-32(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cmpl	$14, %eax
	jg	.L107
	movl	$ifname, %eax
	movq	$-1, -32(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-32(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	cmpl	$6, %eax
	jle	.L108
	movl	$.LC70, %eax
	jmp	.L109
.L108:
	movl	$.LC71, %eax
.L109:
	jmp	.L110
.L107:
	movl	$.LC5, %eax
.L110:
	movl	$.LC72, %esi
	movq	stderr(%rip), %rdx
	movq	%rdx, %rdi
	movq	%rax, %rcx
	movl	$ifname, %edx
	movl	$0, %eax
	call	fprintf
.L106:
	movq	work(%rip), %rcx
	movl	ofd(%rip), %edx
	movl	ifd(%rip), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	*%rcx
	testl	%eax, %eax
	je	.L111
	movl	$-1, method(%rip)
	jmp	.L112
.L111:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L112
	movl	last_member(%rip), %eax
	testl	%eax, %eax
	jne	.L112
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	je	.L112
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	call	get_method
	movl	%eax, method(%rip)
	movl	method(%rip), %eax
	testl	%eax, %eax
	js	.L128
.L113:
	movq	$0, bytes_out(%rip)
	jmp	.L106
.L128:
	nop
.L112:
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	jne	.L114
	movl	ofd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	testl	%eax, %eax
	je	.L114
	call	write_error
.L114:
	movl	method(%rip), %eax
	cmpl	$-1, %eax
	jne	.L115
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	jne	.L129
	movl	$ofname, %edi
	movl	$0, %eax
	call	unlink
	jmp	.L129
.L115:
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L117
	movl	test(%rip), %eax
	testl	%eax, %eax
	je	.L118
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC73, %eax
	movq	%rdx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	jmp	.L119
.L118:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L120
	movq	stderr(%rip), %rdx
	movq	bytes_out(%rip), %rax
	movq	header_bytes(%rip), %rsi
	movq	bytes_in(%rip), %rcx
	subq	%rcx, %rsi
	movq	bytes_out(%rip), %rcx
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	display_ratio
	jmp	.L119
.L120:
	movq	stderr(%rip), %rdx
	movq	bytes_in(%rip), %rax
	movq	header_bytes(%rip), %rsi
	movq	bytes_out(%rip), %rcx
	subq	%rcx, %rsi
	movq	bytes_in(%rip), %rcx
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	display_ratio
.L119:
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L121
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	jne	.L121
	movl	$.LC74, %ecx
	movq	stderr(%rip), %rax
	movl	$ofname, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L121:
	movq	stderr(%rip), %rax
	movq	%rax, %rsi
	movl	$10, %edi
	call	fputc
.L117:
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	jne	.L79
	movl	$istat, %edi
	call	copy_stat
	jmp	.L79
.L122:
	nop
	jmp	.L79
.L123:
	nop
	jmp	.L79
.L124:
	nop
	jmp	.L79
.L125:
	nop
	jmp	.L79
.L126:
	nop
	jmp	.L79
.L127:
	nop
	jmp	.L79
.L129:
	nop
.L79:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	treat_file, .-treat_file
	.section	.rodata
	.align 8
.LC75:
	.string	"%s: %s: warning, name truncated\n"
	.text
	.type	create_outfile, @function
create_outfile:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$160, %rsp
	movl	$193, -4(%rbp)
	movl	ascii(%rip), %eax
	testl	%eax, %eax
	je	.L131
	movl	decompress(%rip), %eax
	testl	%eax, %eax
.L131:
	call	check_ofname
	testl	%eax, %eax
	je	.L132
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$1, %eax
	jmp	.L133
.L132:
	movl	$1, remove_ofname(%rip)
	movl	-4(%rbp), %eax
	movl	$384, %edx
	movl	%eax, %esi
	movl	$ofname, %edi
	movl	$0, %eax
	call	open
	movl	%eax, ofd(%rip)
	movl	ofd(%rip), %eax
	cmpl	$-1, %eax
	jne	.L134
	movl	$ofname, %edi
	call	perror
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L133
.L134:
	movl	ofd(%rip), %eax
	leaq	-160(%rbp), %rdx
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	fstat
	testl	%eax, %eax
	je	.L135
	movq	progname(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$ofname, %edi
	call	perror
	movl	ifd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	ofd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$ofname, %edi
	movl	$0, %eax
	call	unlink
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L133
.L135:
	leaq	-160(%rbp), %rax
	movq	%rax, %rsi
	movl	$ofname, %edi
	call	name_too_long
	testl	%eax, %eax
	jne	.L136
	movl	$0, %eax
	jmp	.L133
.L136:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L137
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L138
	movq	progname(%rip), %rdx
	movl	$.LC75, %esi
	movq	stderr(%rip), %rax
	movl	$ofname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L138:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L139
	movl	$2, exit_code(%rip)
.L139:
	movl	$0, %eax
	jmp	.L133
.L137:
	movl	ofd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$ofname, %edi
	movl	$0, %eax
	call	unlink
	movl	$ofname, %edi
	call	shorten_name
	jmp	.L131
.L133:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	create_outfile, .-create_outfile
	.type	do_stat, @function
do_stat:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	call	__errno_location
	movl	$0, (%rax)
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	jne	.L141
	movl	force(%rip), %eax
	testl	%eax, %eax
	jne	.L141
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	lstat
	jmp	.L142
.L141:
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
.L142:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	do_stat, .-do_stat
	.section	.rodata
.LC76:
	.string	"z"
	.text
	.type	get_suffix, @function
get_suffix:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$known_suffixes.3768, -72(%rbp)
	movl	$z_suffix, %edx
	movl	$.LC76, %eax
	movl	$2, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	seta	%dl
	setb	%al
	movl	%edx, %ecx
	subb	%al, %cl
	movl	%ecx, %eax
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L144
	addq	$8, -72(%rbp)
.L144:
	movq	-88(%rbp), %rax
	movq	$-1, -96(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-96(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -60(%rbp)
	cmpl	$32, -60(%rbp)
	jg	.L145
	movq	-88(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	jmp	.L146
.L145:
	movl	-60(%rbp), %eax
	cltq
	subq	$32, %rax
	addq	-88(%rbp), %rax
	movq	%rax, %rdx
	leaq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
.L146:
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	strlwr
	leaq	-48(%rbp), %rax
	movq	$-1, -96(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-96(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -56(%rbp)
.L149:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	$-1, -96(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-96(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -52(%rbp)
	movl	-56(%rbp), %eax
	cmpl	-52(%rbp), %eax
	jle	.L147
	movl	-52(%rbp), %eax
	movl	-56(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	subl	$1, %eax
	cltq
	movzbl	-48(%rbp,%rax), %eax
	cmpb	$47, %al
	je	.L147
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movl	-56(%rbp), %edx
	movslq	%edx, %rcx
	movl	-52(%rbp), %edx
	movslq	%edx, %rdx
	subq	%rdx, %rcx
	leaq	-48(%rbp), %rdx
	addq	%rcx, %rdx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L147
	movl	-60(%rbp), %eax
	movslq	%eax, %rdx
	movl	-52(%rbp), %eax
	cltq
	movq	%rdx, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	addq	-88(%rbp), %rax
	jmp	.L148
.L147:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L149
	movl	$0, %eax
.L148:
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	je	.L150
	call	__stack_chk_fail
.L150:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	get_suffix, .-get_suffix
	.type	get_istat, @function
get_istat:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	$suffixes.3778, -24(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rsi
	movl	$ifname, %edi
	call	strcpy
	movq	-48(%rbp), %rax
	movq	%rax, %rsi
	movl	$ifname, %edi
	call	do_stat
	testl	%eax, %eax
	jne	.L152
	movl	$0, %eax
	jmp	.L153
.L152:
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L154
	call	__errno_location
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L155
.L154:
	movl	$ifname, %edi
	call	perror
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L153
.L155:
	movl	$ifname, %edi
	call	get_suffix
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L156
	movl	$ifname, %edi
	call	perror
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L153
.L156:
	movl	$ifname, %eax
	movq	$-1, -56(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-56(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -4(%rbp)
	movl	$.LC50, %esi
	movl	$z_suffix, %edi
	call	strcmp
	testl	%eax, %eax
	jne	.L157
	addq	$8, -24(%rbp)
.L157:
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$ifname, %edi
	call	strcat
	movq	-48(%rbp), %rax
	movq	%rax, %rsi
	movl	$ifname, %edi
	call	do_stat
	testl	%eax, %eax
	jne	.L158
	movl	$0, %eax
	jmp	.L153
.L158:
	movl	-4(%rbp), %eax
	cltq
	movb	$0, ifname(%rax)
	addq	$8, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L157
	movl	$z_suffix, %eax
	movq	%rax, %rsi
	movl	$ifname, %edi
	call	strcat
	movl	$ifname, %edi
	call	perror
	movl	$1, exit_code(%rip)
	movl	$1, %eax
.L153:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	get_istat, .-get_istat
	.section	.rodata
	.align 8
.LC77:
	.string	"%s: %s: unknown suffix -- ignored\n"
.LC78:
	.string	".tgz"
.LC79:
	.string	".taz"
.LC80:
	.string	".tar"
	.align 8
.LC81:
	.string	"%s: %s already has %s suffix -- unchanged\n"
	.text
	.type	make_ofname, @function
make_ofname:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$ifname, %eax
	movq	%rax, %rsi
	movl	$ofname, %edi
	call	strcpy
	movl	$ofname, %edi
	call	get_suffix
	movq	%rax, -8(%rbp)
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L160
	cmpq	$0, -8(%rbp)
	jne	.L161
	movl	recursive(%rip), %eax
	testl	%eax, %eax
	jne	.L162
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L163
	movl	test(%rip), %eax
	testl	%eax, %eax
	je	.L162
.L163:
	movl	$0, %eax
	jmp	.L164
.L162:
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	jne	.L165
	movl	recursive(%rip), %eax
	testl	%eax, %eax
	jne	.L166
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L166
.L165:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L167
	movq	progname(%rip), %rdx
	movl	$.LC77, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L167:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L166
	movl	$2, exit_code(%rip)
.L166:
	movl	$2, %eax
	jmp	.L164
.L161:
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	strlwr
	movq	-8(%rbp), %rax
	movl	$.LC78, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L168
	movq	-8(%rbp), %rax
	movl	$.LC79, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L169
.L168:
	movl	$.LC80, %edx
	movq	-8(%rbp), %rax
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movzbl	4(%rdx), %edx
	movb	%dl, 4(%rax)
	jmp	.L171
.L169:
	movq	-8(%rbp), %rax
	movb	$0, (%rax)
	jmp	.L171
.L160:
	cmpq	$0, -8(%rbp)
	je	.L172
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	jne	.L173
	movl	recursive(%rip), %eax
	testl	%eax, %eax
	jne	.L174
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L174
.L173:
	movq	progname(%rip), %rdx
	movl	$.LC81, %esi
	movq	stderr(%rip), %rax
	movq	-8(%rbp), %rcx
	movq	%rcx, %r8
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L174:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L175
	movl	$2, exit_code(%rip)
.L175:
	movl	$2, %eax
	jmp	.L164
.L172:
	movl	$0, save_orig_name(%rip)
	movl	$z_suffix, %eax
	movq	%rax, %rsi
	movl	$ofname, %edi
	call	strcat
.L171:
	movl	$0, %eax
.L164:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	make_ofname, .-make_ofname
	.section	.rodata
.LC82:
	.string	"\037\213"
.LC83:
	.string	"\037\236"
	.align 8
.LC84:
	.string	"%s: %s: unknown method %d -- get newer version of gzip\n"
	.align 8
.LC85:
	.string	"%s: %s is encrypted -- get newer version of gzip\n"
	.align 8
.LC86:
	.string	"%s: %s is a a multi-part gzip file -- get newer version of gzip\n"
	.align 8
.LC87:
	.string	"%s: %s has flags 0x%x -- get newer version of gzip\n"
.LC88:
	.string	"%s: %s: part number %u\n"
	.align 8
.LC89:
	.string	"%s: %s: extra field of %u bytes ignored\n"
	.align 8
.LC90:
	.string	"corrupted input -- file name too large"
.LC91:
	.string	"PK\003\004"
.LC92:
	.string	"\037\036"
.LC93:
	.string	"\037\235"
.LC94:
	.string	"\037\240"
.LC95:
	.string	"\n%s: %s: not in gzip format\n"
	.align 8
.LC96:
	.string	"\n%s: %s: decompression OK, trailing garbage ignored\n"
	.text
	.type	get_method, @function
get_method:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movl	%edi, -52(%rbp)
	movl	force(%rip), %eax
	testl	%eax, %eax
	je	.L177
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L177
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L178
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L179
.L178:
	movl	$1, %edi
	call	fill_inbuf
.L179:
	movb	%al, -16(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L180
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L181
.L180:
	movl	$1, %edi
	call	fill_inbuf
.L181:
	movb	%al, -15(%rbp)
	jmp	.L182
.L177:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L183
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L184
.L183:
	movl	$0, %edi
	call	fill_inbuf
.L184:
	movb	%al, -16(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L185
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L186
.L185:
	movl	$0, %edi
	call	fill_inbuf
.L186:
	movb	%al, -15(%rbp)
.L182:
	movl	$-1, method(%rip)
	movl	part_nb(%rip), %eax
	addl	$1, %eax
	movl	%eax, part_nb(%rip)
	movq	$0, header_bytes(%rip)
	movl	$0, last_member(%rip)
	leaq	-16(%rbp), %rax
	movl	$2, %edx
	movl	$.LC82, %esi
	movq	%rax, %rdi
	call	memcmp
	testl	%eax, %eax
	je	.L187
	leaq	-16(%rbp), %rax
	movl	$2, %edx
	movl	$.LC83, %esi
	movq	%rax, %rdi
	call	memcmp
	testl	%eax, %eax
	jne	.L188
.L187:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L189
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L190
.L189:
	movl	$0, %edi
	call	fill_inbuf
.L190:
	movl	%eax, method(%rip)
	movl	method(%rip), %eax
	cmpl	$8, %eax
	je	.L191
	movl	method(%rip), %ecx
	movq	progname(%rip), %rdx
	movl	$.LC84, %esi
	movq	stderr(%rip), %rax
	movl	%ecx, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$-1, %eax
	jmp	.L192
.L191:
	movq	$unzip, work(%rip)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L193
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L194
.L193:
	movl	$0, %edi
	call	fill_inbuf
.L194:
	movb	%al, -2(%rbp)
	movzbl	-2(%rbp), %eax
	andl	$32, %eax
	testl	%eax, %eax
	je	.L195
	movq	progname(%rip), %rdx
	movl	$.LC85, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$-1, %eax
	jmp	.L192
.L195:
	movzbl	-2(%rbp), %eax
	andl	$2, %eax
	testl	%eax, %eax
	je	.L196
	movq	progname(%rip), %rdx
	movl	$.LC86, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	force(%rip), %eax
	cmpl	$1, %eax
	jg	.L196
	movl	$-1, %eax
	jmp	.L192
.L196:
	movzbl	-2(%rbp), %eax
	andl	$192, %eax
	testl	%eax, %eax
	je	.L197
	movzbl	-2(%rbp), %ecx
	movq	progname(%rip), %rdx
	movl	$.LC87, %esi
	movq	stderr(%rip), %rax
	movl	%ecx, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	force(%rip), %eax
	cmpl	$1, %eax
	jg	.L197
	movl	$-1, %eax
	jmp	.L192
.L197:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L198
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L199
.L198:
	movl	$0, %edi
	call	fill_inbuf
	cltq
.L199:
	movq	%rax, -40(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L200
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	salq	$8, %rax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L201
.L200:
	movl	$0, %edi
	call	fill_inbuf
	cltq
	salq	$8, %rax
.L201:
	orq	%rax, -40(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L202
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	salq	$16, %rax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L203
.L202:
	movl	$0, %edi
	call	fill_inbuf
	cltq
	salq	$16, %rax
.L203:
	orq	%rax, -40(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L204
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	salq	$24, %rax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L205
.L204:
	movl	$0, %edi
	call	fill_inbuf
	cltq
	salq	$24, %rax
.L205:
	orq	%rax, -40(%rbp)
	cmpq	$0, -40(%rbp)
	je	.L206
	movl	no_time(%rip), %eax
	testl	%eax, %eax
	jne	.L206
	movq	-40(%rbp), %rax
	movq	%rax, time_stamp(%rip)
.L206:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L207
	movl	inptr(%rip), %eax
	addl	$1, %eax
	movl	%eax, inptr(%rip)
	jmp	.L208
.L207:
	movl	$0, %edi
	call	fill_inbuf
.L208:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L209
	movl	inptr(%rip), %eax
	addl	$1, %eax
	movl	%eax, inptr(%rip)
	jmp	.L210
.L209:
	movl	$0, %edi
	call	fill_inbuf
.L210:
	movzbl	-2(%rbp), %eax
	andl	$2, %eax
	testl	%eax, %eax
	je	.L211
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L212
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L213
.L212:
	movl	$0, %edi
	call	fill_inbuf
.L213:
	movl	%eax, -20(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L214
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L215
.L214:
	movl	$0, %edi
	call	fill_inbuf
	sall	$8, %eax
.L215:
	orl	%eax, -20(%rbp)
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L211
	movq	progname(%rip), %rdx
	movl	$.LC88, %esi
	movq	stderr(%rip), %rax
	movl	-20(%rbp), %ecx
	movl	%ecx, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L211:
	movzbl	-2(%rbp), %eax
	andl	$4, %eax
	testl	%eax, %eax
	je	.L216
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L217
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L218
.L217:
	movl	$0, %edi
	call	fill_inbuf
.L218:
	movl	%eax, -24(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L219
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	sall	$8, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L220
.L219:
	movl	$0, %edi
	call	fill_inbuf
	sall	$8, %eax
.L220:
	orl	%eax, -24(%rbp)
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L253
	movq	progname(%rip), %rdx
	movl	$.LC89, %esi
	movq	stderr(%rip), %rax
	movl	-24(%rbp), %ecx
	movl	%ecx, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L253
.L224:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L223
	movl	inptr(%rip), %eax
	addl	$1, %eax
	movl	%eax, inptr(%rip)
	jmp	.L222
.L223:
	movl	$0, %edi
	call	fill_inbuf
	jmp	.L222
.L253:
	nop
.L222:
	cmpl	$0, -24(%rbp)
	setne	%al
	subl	$1, -24(%rbp)
	testb	%al, %al
	jne	.L224
.L216:
	movzbl	-2(%rbp), %eax
	andl	$8, %eax
	testl	%eax, %eax
	je	.L225
	movl	no_name(%rip), %eax
	testl	%eax, %eax
	jne	.L226
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L227
	movl	list(%rip), %eax
	testl	%eax, %eax
	je	.L226
.L227:
	movl	part_nb(%rip), %eax
	cmpl	$1, %eax
	jle	.L228
.L226:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L229
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L230
.L229:
	movl	$0, %edi
	call	fill_inbuf
.L230:
	movb	%al, -1(%rbp)
	cmpb	$0, -1(%rbp)
	jne	.L226
	jmp	.L225
.L228:
	movl	$ofname, %edi
	call	basename
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L236
.L255:
	nop
.L236:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L231
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L232
.L231:
	movl	$0, %edi
	call	fill_inbuf
.L232:
	movq	-48(%rbp), %rdx
	movb	%al, (%rdx)
	movq	-48(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	sete	%al
	addq	$1, -48(%rbp)
	testb	%al, %al
	jne	.L254
.L233:
	cmpq	$ofname+1024, -48(%rbp)
	jb	.L255
	movl	$.LC90, %edi
	call	error
	jmp	.L255
.L254:
	nop
.L252:
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L225
	cmpq	$0, -32(%rbp)
	je	.L225
	movl	$0, list(%rip)
.L225:
	movzbl	-2(%rbp), %eax
	andl	$16, %eax
	testl	%eax, %eax
	je	.L237
	nop
.L240:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L238
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	testb	%al, %al
	setne	%al
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L239
.L238:
	movl	$0, %edi
	call	fill_inbuf
	testl	%eax, %eax
	setne	%al
.L239:
	testb	%al, %al
	jne	.L240
.L237:
	movl	part_nb(%rip), %eax
	cmpl	$1, %eax
	jne	.L242
	movl	inptr(%rip), %eax
	movl	%eax, %eax
	addq	$16, %rax
	movq	%rax, header_bytes(%rip)
	jmp	.L242
.L188:
	leaq	-16(%rbp), %rax
	movl	$2, %edx
	movl	$.LC91, %esi
	movq	%rax, %rdi
	call	memcmp
	testl	%eax, %eax
	jne	.L243
	movl	inptr(%rip), %eax
	cmpl	$2, %eax
	jne	.L243
	movl	$4, %edx
	movl	$.LC91, %esi
	movl	$inbuf, %edi
	call	memcmp
	testl	%eax, %eax
	jne	.L243
	movl	$0, inptr(%rip)
	movq	$unzip, work(%rip)
	movl	-52(%rbp), %eax
	movl	%eax, %edi
	call	check_zipfile
	testl	%eax, %eax
	je	.L244
	movl	$-1, %eax
	jmp	.L192
.L244:
	movl	$1, last_member(%rip)
	jmp	.L242
.L243:
	leaq	-16(%rbp), %rax
	movl	$2, %edx
	movl	$.LC92, %esi
	movq	%rax, %rdi
	call	memcmp
	testl	%eax, %eax
	jne	.L245
	movq	$unpack, work(%rip)
	movl	$2, method(%rip)
	jmp	.L242
.L245:
	leaq	-16(%rbp), %rax
	movl	$2, %edx
	movl	$.LC93, %esi
	movq	%rax, %rdi
	call	memcmp
	testl	%eax, %eax
	jne	.L246
	movq	$unlzw, work(%rip)
	movl	$1, method(%rip)
	movl	$1, last_member(%rip)
	jmp	.L242
.L246:
	leaq	-16(%rbp), %rax
	movl	$2, %edx
	movl	$.LC94, %esi
	movq	%rax, %rdi
	call	memcmp
	testl	%eax, %eax
	jne	.L247
	movq	$unlzh, work(%rip)
	movl	$3, method(%rip)
	movl	$1, last_member(%rip)
	jmp	.L242
.L247:
	movl	force(%rip), %eax
	testl	%eax, %eax
	je	.L242
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L242
	movl	list(%rip), %eax
	testl	%eax, %eax
	jne	.L242
	movl	$0, method(%rip)
	movq	$copy, work(%rip)
	movl	$0, inptr(%rip)
	movl	$1, last_member(%rip)
.L242:
	movl	method(%rip), %eax
	testl	%eax, %eax
	js	.L248
	movl	method(%rip), %eax
	jmp	.L192
.L248:
	movl	part_nb(%rip), %eax
	cmpl	$1, %eax
	jne	.L249
	movq	progname(%rip), %rdx
	movl	$.LC95, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$-1, %eax
	jmp	.L192
.L249:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L250
	movq	progname(%rip), %rdx
	movl	$.LC96, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L250:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L251
	movl	$2, exit_code(%rip)
.L251:
	movl	$-2, %eax
.L192:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
	.size	get_method, .-get_method
	.section	.rodata
.LC97:
	.string	"method  crc     date  time  "
	.align 8
.LC98:
	.string	"compressed  uncompr. ratio uncompressed_name"
	.align 8
.LC99:
	.string	"                            %9lu %9lu "
.LC100:
	.string	"%9ld %9ld "
.LC101:
	.string	" (totals)"
.LC102:
	.string	"%5s %08lx %11s "
.LC103:
	.string	" %s\n"
	.text
	.type	do_list, @function
do_list:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%edi, -36(%rbp)
	movl	%esi, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	first_time.3812(%rip), %eax
	testl	%eax, %eax
	je	.L257
	cmpl	$0, -40(%rbp)
	js	.L257
	movl	$0, first_time.3812(%rip)
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L258
	movl	$.LC97, %eax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
.L258:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L260
	movl	$.LC98, %edi
	call	puts
	jmp	.L260
.L257:
	cmpl	$0, -40(%rbp)
	jns	.L260
	movq	total_in(%rip), %rax
	testq	%rax, %rax
	jle	.L274
	movq	total_out(%rip), %rax
	testq	%rax, %rax
	jle	.L274
.L262:
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L264
	movq	total_out(%rip), %rdx
	movq	total_in(%rip), %rcx
	movl	$.LC99, %eax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	jmp	.L265
.L264:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L265
	movq	total_out(%rip), %rdx
	movq	total_in(%rip), %rcx
	movl	$.LC100, %eax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
.L265:
	movq	stdout(%rip), %rdx
	movq	total_out(%rip), %rax
	movq	header_bytes(%rip), %rsi
	movq	total_in(%rip), %rcx
	subq	%rcx, %rsi
	movq	total_out(%rip), %rcx
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	display_ratio
	movl	$.LC101, %edi
	call	puts
	jmp	.L256
.L260:
	movq	$-1, -32(%rbp)
	movq	$-1, bytes_out(%rip)
	movq	ifile_size(%rip), %rax
	movq	%rax, bytes_in(%rip)
	cmpl	$8, -40(%rbp)
	jne	.L266
	movl	last_member(%rip), %eax
	testl	%eax, %eax
	jne	.L266
	movl	-36(%rbp), %eax
	movl	$2, %edx
	movq	$-8, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	lseek
	cltq
	movq	%rax, bytes_in(%rip)
	movq	bytes_in(%rip), %rax
	cmpq	$-1, %rax
	je	.L266
	movq	bytes_in(%rip), %rax
	addq	$8, %rax
	movq	%rax, bytes_in(%rip)
	leaq	-16(%rbp), %rcx
	movl	-36(%rbp), %eax
	movl	$8, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	read
	cmpl	$8, %eax
	je	.L267
	call	read_error
.L267:
	movzbl	-16(%rbp), %eax
	movzbl	%al, %eax
	movzbl	-15(%rbp), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	-14(%rbp), %edx
	movzbl	%dl, %edx
	movzbl	-13(%rbp), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -32(%rbp)
	movzbl	-12(%rbp), %eax
	movzbl	%al, %eax
	movzbl	-11(%rbp), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	-10(%rbp), %edx
	movzbl	%dl, %edx
	movzbl	-9(%rbp), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
.L266:
	movl	$time_stamp, %edi
	call	ctime
	addq	$4, %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movb	$0, (%rax)
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	je	.L268
	movl	-40(%rbp), %eax
	cltq
	movq	methods.3813(,%rax,8), %rsi
	movl	$.LC102, %eax
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rdx
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
.L268:
	movq	bytes_out(%rip), %rdx
	movq	bytes_in(%rip), %rcx
	movl	$.LC100, %eax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	movq	bytes_in(%rip), %rax
	cmpq	$-1, %rax
	jne	.L269
	movq	$-1, total_in(%rip)
	movq	$0, header_bytes(%rip)
	movq	header_bytes(%rip), %rax
	movq	%rax, bytes_out(%rip)
	movq	bytes_out(%rip), %rax
	movq	%rax, bytes_in(%rip)
	jmp	.L270
.L269:
	movq	total_in(%rip), %rax
	testq	%rax, %rax
	js	.L270
	movq	total_in(%rip), %rdx
	movq	bytes_in(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, total_in(%rip)
.L270:
	movq	bytes_out(%rip), %rax
	cmpq	$-1, %rax
	jne	.L271
	movq	$-1, total_out(%rip)
	movq	$0, header_bytes(%rip)
	movq	header_bytes(%rip), %rax
	movq	%rax, bytes_out(%rip)
	movq	bytes_out(%rip), %rax
	movq	%rax, bytes_in(%rip)
	jmp	.L272
.L271:
	movq	total_out(%rip), %rax
	testq	%rax, %rax
	js	.L272
	movq	total_out(%rip), %rdx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, total_out(%rip)
.L272:
	movq	stdout(%rip), %rdx
	movq	bytes_out(%rip), %rax
	movq	header_bytes(%rip), %rsi
	movq	bytes_in(%rip), %rcx
	subq	%rcx, %rsi
	movq	bytes_out(%rip), %rcx
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	display_ratio
	movl	$.LC103, %eax
	movl	$ofname, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	jmp	.L256
.L274:
	nop
.L256:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L273
	call	__stack_chk_fail
.L273:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	do_list, .-do_list
	.type	same_file, @function
same_file:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	8(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L276
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L276
	movl	$1, %eax
	jmp	.L277
.L276:
	movl	$0, %eax
.L277:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	same_file, .-same_file
	.type	name_too_long, @function
name_too_long:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movq	%rdi, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-168(%rbp), %rax
	movq	$-1, -184(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-184(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-168(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1(%rbp)
	movq	-176(%rbp), %rax
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	movl	$18, %eax
	movq	%rdx, %rdi
	movq	%rax, %rcx
	rep movsq
	movl	-12(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-168(%rbp), %rax
	movb	$0, (%rax)
	movq	-168(%rbp), %rax
	leaq	-160(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	testl	%eax, %eax
	jne	.L279
	leaq	-160(%rbp), %rdx
	movq	-176(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	same_file
	testl	%eax, %eax
	je	.L279
	movl	$1, %eax
	jmp	.L280
.L279:
	movl	$0, %eax
.L280:
	movl	%eax, -8(%rbp)
	movl	-12(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-168(%rbp), %rax
	movzbl	-1(%rbp), %edx
	movb	%dl, (%rax)
	movl	-8(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	name_too_long, .-name_too_long
	.section	.rodata
.LC104:
	.string	"name too short"
.LC105:
	.string	"can't recover suffix\n"
.LC106:
	.string	"."
	.align 8
.LC107:
	.string	"internal error in shorten_name"
	.text
	.type	shorten_name, @function
shorten_name:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$3, -12(%rbp)
	movq	-40(%rbp), %rax
	movq	$-1, -48(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-48(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	movl	%eax, -8(%rbp)
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L282
	cmpl	$1, -8(%rbp)
	jg	.L283
	movl	$.LC104, %edi
	call	error
.L283:
	movl	-8(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	-40(%rbp), %rax
	movb	$0, (%rax)
	jmp	.L281
.L282:
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	get_suffix
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L285
	movl	$.LC105, %edi
	call	error
.L285:
	movq	-24(%rbp), %rax
	movb	$0, (%rax)
	movl	$1, save_orig_name(%rip)
	cmpl	$4, -8(%rbp)
	jle	.L286
	movq	-24(%rbp), %rax
	subq	$4, %rax
	movl	$.LC80, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L286
	movl	$.LC78, %eax
	movq	-24(%rbp), %rdx
	subq	$4, %rdx
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	movzbl	4(%rax), %eax
	movb	%al, 4(%rdx)
	jmp	.L281
.L286:
	movq	-40(%rbp), %rax
	movl	$47, %esi
	movq	%rax, %rdi
	call	strrchr
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	je	.L287
	movq	-24(%rbp), %rax
	addq	$1, %rax
	jmp	.L288
.L287:
	movq	-40(%rbp), %rax
.L288:
	movq	%rax, -24(%rbp)
	jmp	.L289
.L291:
	movq	-24(%rbp), %rax
	movl	$.LC106, %esi
	movq	%rax, %rdi
	call	strcspn
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	cltq
	addq	%rax, -24(%rbp)
	movl	-4(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jle	.L290
	movq	-24(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -32(%rbp)
.L290:
	movq	-24(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L289
	addq	$1, -24(%rbp)
.L289:
	movq	-24(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L291
	cmpq	$0, -32(%rbp)
	jne	.L292
	subl	$1, -12(%rbp)
	cmpl	$0, -12(%rbp)
	jne	.L286
.L292:
	cmpq	$0, -32(%rbp)
	je	.L293
.L294:
	movq	-32(%rbp), %rax
	movzbl	1(%rax), %edx
	movq	-32(%rbp), %rax
	movb	%dl, (%rax)
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	setne	%al
	addq	$1, -32(%rbp)
	testb	%al, %al
	jne	.L294
	subq	$1, -32(%rbp)
	jmp	.L295
.L293:
	movl	$46, %eax
	movsbl	%al, %edx
	movq	-40(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	strrchr
	movq	%rax, -32(%rbp)
	cmpq	$0, -32(%rbp)
	jne	.L296
	movl	$.LC107, %edi
	call	error
.L296:
	movq	-32(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L295
	subq	$1, -32(%rbp)
.L295:
	movl	$z_suffix, %edx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
.L281:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	shorten_name, .-shorten_name
	.section	.rodata
	.align 8
.LC108:
	.string	"%s: %s: cannot %scompress onto itself\n"
	.align 8
.LC109:
	.string	"%s: %s and %s are the same file\n"
.LC110:
	.string	"n"
.LC111:
	.string	"%s: %s already exists;"
	.align 8
.LC112:
	.string	" do you wish to overwrite (y or n)? "
.LC113:
	.string	"\tnot overwritten\n"
	.text
	.type	check_ofname, @function
check_ofname:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	__errno_location
	movl	$0, (%rax)
	jmp	.L298
.L301:
	call	__errno_location
	movl	(%rax), %eax
	cmpl	$36, %eax
	je	.L299
	movl	$0, %eax
	jmp	.L300
.L299:
	movl	$ofname, %edi
	call	shorten_name
.L298:
	movl	$ofname, %eax
	leaq	-240(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	testl	%eax, %eax
	jne	.L301
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	jne	.L302
	leaq	-240(%rbp), %rax
	movq	%rax, %rsi
	movl	$ofname, %edi
	call	name_too_long
	testl	%eax, %eax
	je	.L302
	movl	$ofname, %edi
	call	shorten_name
	movl	$ofname, %eax
	leaq	-240(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	stat
	testl	%eax, %eax
	je	.L302
	movl	$0, %eax
	jmp	.L300
.L302:
	leaq	-240(%rbp), %rax
	movq	%rax, %rsi
	movl	$istat, %edi
	call	same_file
	testl	%eax, %eax
	je	.L303
	movl	$ofname, %esi
	movl	$ifname, %edi
	call	strcmp
	testl	%eax, %eax
	jne	.L304
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L305
	movl	$.LC56, %eax
	jmp	.L306
.L305:
	movl	$.LC5, %eax
.L306:
	movq	progname(%rip), %rdx
	movl	$.LC108, %esi
	movq	stderr(%rip), %rcx
	movq	%rcx, %rdi
	movq	%rax, %r8
	movl	$ifname, %ecx
	movl	$0, %eax
	call	fprintf
	jmp	.L307
.L304:
	movq	progname(%rip), %rdx
	movl	$.LC109, %esi
	movq	stderr(%rip), %rax
	movl	$ofname, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L307:
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L300
.L303:
	movl	force(%rip), %eax
	testl	%eax, %eax
	jne	.L308
	movl	$.LC110, %edx
	leaq	-96(%rbp), %rax
	movzwl	(%rdx), %edx
	movw	%dx, (%rax)
	movq	progname(%rip), %rdx
	movl	$.LC111, %esi
	movq	stderr(%rip), %rax
	movl	$ofname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	foreground(%rip), %eax
	testl	%eax, %eax
	je	.L309
	movq	stdin(%rip), %rax
	movq	%rax, %rdi
	call	fileno
	movl	%eax, %edi
	movl	$0, %eax
	call	isatty
	testl	%eax, %eax
	je	.L309
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC112, %eax
	movq	%rdx, %rcx
	movl	$36, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movq	stderr(%rip), %rax
	movq	%rax, %rdi
	call	fflush
	movq	stdin(%rip), %rax
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movl	$79, %esi
	movq	%rax, %rdi
	call	fgets
.L309:
	call	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	-96(%rbp), %edx
	movsbq	%dl, %rdx
	addq	%rdx, %rdx
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	andl	$256, %eax
	testl	%eax, %eax
	je	.L310
	movzbl	-96(%rbp), %eax
	cmpb	$89, %al
	setne	%al
	jmp	.L311
.L310:
	movzbl	-96(%rbp), %eax
	cmpb	$121, %al
	setne	%al
.L311:
	testb	%al, %al
	je	.L308
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC113, %eax
	movq	%rdx, %rcx
	movl	$17, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L312
	movl	$2, exit_code(%rip)
.L312:
	movl	$1, %eax
	jmp	.L300
.L308:
	movl	$511, %esi
	movl	$ofname, %edi
	call	chmod
	movl	$ofname, %edi
	movl	$0, %eax
	call	unlink
	testl	%eax, %eax
	je	.L313
	movq	progname(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$ofname, %edi
	call	perror
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L300
.L313:
	movl	$0, %eax
.L300:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L314
	call	__stack_chk_fail
.L314:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	check_ofname, .-check_ofname
	.type	reset_times, @function
reset_times:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	72(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-32(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -8(%rbp)
	leaq	-16(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	utime
	testl	%eax, %eax
	je	.L315
	movq	-32(%rbp), %rax
	movl	24(%rax), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L315
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L317
	movq	progname(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L317:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L318
	movl	$2, exit_code(%rip)
.L318:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L315
	movl	$ofname, %edi
	call	perror
.L315:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	reset_times, .-reset_times
	.section	.rodata
.LC114:
	.string	"%s: time stamp restored\n"
	.text
	.type	copy_stat, @function
copy_stat:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	decompress(%rip), %eax
	testl	%eax, %eax
	je	.L320
	movq	time_stamp(%rip), %rax
	testq	%rax, %rax
	je	.L320
	movq	-8(%rbp), %rax
	movq	88(%rax), %rdx
	movq	time_stamp(%rip), %rax
	cmpq	%rax, %rdx
	je	.L320
	movq	time_stamp(%rip), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 88(%rax)
	movl	verbose(%rip), %eax
	cmpl	$1, %eax
	jle	.L320
	movl	$.LC114, %ecx
	movq	stderr(%rip), %rax
	movl	$ofname, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L320:
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	movl	$ofname, %edi
	call	reset_times
	movq	-8(%rbp), %rax
	movl	24(%rax), %eax
	andl	$4095, %eax
	movl	%eax, %esi
	movl	$ofname, %edi
	call	chmod
	testl	%eax, %eax
	je	.L321
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L322
	movq	progname(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L322:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L323
	movl	$2, exit_code(%rip)
.L323:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L321
	movl	$ofname, %edi
	call	perror
.L321:
	movq	-8(%rbp), %rax
	movl	32(%rax), %edx
	movq	-8(%rbp), %rax
	movl	28(%rax), %eax
	movl	%eax, %esi
	movl	$ofname, %edi
	movl	$0, %eax
	call	chown
	movl	$0, remove_ofname(%rip)
	movl	$511, %esi
	movl	$ifname, %edi
	call	chmod
	movl	$ifname, %edi
	movl	$0, %eax
	call	unlink
	testl	%eax, %eax
	je	.L319
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L325
	movq	progname(%rip), %rdx
	movl	$.LC68, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L325:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L326
	movl	$2, exit_code(%rip)
.L326:
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L319
	movl	$ifname, %edi
	call	perror
.L319:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
	.size	copy_stat, .-copy_stat
	.type	do_exit, @function
do_exit:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	in_exit.3870(%rip), %eax
	testl	%eax, %eax
	je	.L328
	movl	-4(%rbp), %eax
	movl	%eax, %edi
	call	exit
.L328:
	movl	$1, in_exit.3870(%rip)
	movq	env(%rip), %rax
	testq	%rax, %rax
	je	.L329
	movq	env(%rip), %rax
	movq	%rax, %rdi
	call	free
	movq	$0, env(%rip)
.L329:
	movq	args(%rip), %rax
	testq	%rax, %rax
	je	.L330
	movq	args(%rip), %rax
	movq	%rax, %rdi
	call	free
	movq	$0, args(%rip)
.L330:
	movl	-4(%rbp), %eax
	movl	%eax, %edi
	call	exit
	.cfi_endproc
.LFE20:
	.size	do_exit, .-do_exit
	.globl	abort_gzip
	.type	abort_gzip, @function
abort_gzip:
.LFB21:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	remove_ofname(%rip), %eax
	testl	%eax, %eax
	je	.L332
	movl	ofd(%rip), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$ofname, %edi
	movl	$0, %eax
	call	unlink
.L332:
	movl	$1, %edi
	call	do_exit
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21:
	.size	abort_gzip, .-abort_gzip
	.local	crc
	.comm	crc,8,8
	.comm	header_bytes,8,8
	.globl	zip
	.type	zip, @function
zip:
.LFB22:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movb	$0, -1(%rbp)
	movw	$0, -6(%rbp)
	movw	$0, -4(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, ifd(%rip)
	movl	-24(%rbp), %eax
	movl	%eax, ofd(%rip)
	movl	$0, outcnt(%rip)
	movl	$8, method(%rip)
	movl	outcnt(%rip), %eax
	movl	$31, %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L334
	call	flush_outbuf
.L334:
	movl	outcnt(%rip), %eax
	movl	$-117, %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L335
	call	flush_outbuf
.L335:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$8, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L336
	call	flush_outbuf
.L336:
	movl	save_orig_name(%rip), %eax
	testl	%eax, %eax
	je	.L337
	orb	$8, -1(%rbp)
.L337:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movzbl	-1(%rbp), %ecx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L338
	call	flush_outbuf
.L338:
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L339
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L340
.L339:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L341
	call	flush_outbuf
.L341:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L340
	call	flush_outbuf
.L340:
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L342
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L343
.L342:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L344
	call	flush_outbuf
.L344:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$0, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L343
	call	flush_outbuf
.L343:
	movl	$0, %esi
	movl	$0, %edi
	call	updcrc
	movq	%rax, crc(%rip)
	movl	-24(%rbp), %eax
	movl	%eax, %edi
	call	bi_init
	leaq	-6(%rbp), %rax
	movl	$method, %esi
	movq	%rax, %rdi
	call	ct_init
	movl	level(%rip), %eax
	leaq	-4(%rbp), %rdx
	movq	%rdx, %rsi
	movl	%eax, %edi
	call	lm_init
	movl	outcnt(%rip), %eax
	movzwl	-4(%rbp), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L345
	call	flush_outbuf
.L345:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movb	$3, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L346
	call	flush_outbuf
.L346:
	movl	save_orig_name(%rip), %eax
	testl	%eax, %eax
	je	.L347
	movl	$ifname, %edi
	call	basename
	movq	%rax, -16(%rbp)
.L349:
	movl	outcnt(%rip), %eax
	movq	-16(%rbp), %rdx
	movzbl	(%rdx), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L348
	call	flush_outbuf
.L348:
	movq	-16(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	setne	%al
	addq	$1, -16(%rbp)
	testb	%al, %al
	jne	.L349
.L347:
	movl	outcnt(%rip), %eax
	movl	%eax, %eax
	movq	%rax, header_bytes(%rip)
	call	deflate
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L350
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L351
.L350:
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L352
	call	flush_outbuf
.L352:
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L351
	call	flush_outbuf
.L351:
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L353
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	shrq	$16, %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	shrq	$16, %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L354
.L353:
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	shrq	$16, %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L355
	call	flush_outbuf
.L355:
	movl	outcnt(%rip), %eax
	movq	crc(%rip), %rdx
	shrq	$16, %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L354
	call	flush_outbuf
.L354:
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L356
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L357
.L356:
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L358
	call	flush_outbuf
.L358:
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L357
	call	flush_outbuf
.L357:
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L359
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	shrq	$16, %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	shrq	$16, %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L360
.L359:
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	shrq	$16, %rdx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L361
	call	flush_outbuf
.L361:
	movl	outcnt(%rip), %eax
	movq	bytes_in(%rip), %rdx
	shrq	$16, %rdx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L360
	call	flush_outbuf
.L360:
	movq	header_bytes(%rip), %rax
	addq	$16, %rax
	movq	%rax, header_bytes(%rip)
	call	flush_outbuf
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	zip, .-zip
	.globl	file_read
	.type	file_read, @function
file_read:
.LFB23:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	ifd(%rip), %eax
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rcx
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	read
	movl	%eax, -4(%rbp)
	cmpl	$-1, -4(%rbp)
	je	.L363
	cmpl	$0, -4(%rbp)
	jne	.L364
.L363:
	movl	-4(%rbp), %eax
	jmp	.L365
.L364:
	movl	-4(%rbp), %edx
	movq	-24(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	updcrc
	movq	%rax, crc(%rip)
	movl	-4(%rbp), %edx
	movq	bytes_in(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_in(%rip)
	movl	-4(%rbp), %eax
.L365:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
	.size	file_read, .-file_read
	.globl	window_size
	.data
	.align 8
	.type	window_size, @object
	.size	window_size, 8
window_size:
	.quad	65536
	.comm	block_start,8,8
	.local	ins_h
	.comm	ins_h,4,4
	.comm	prev_length,4,4
	.comm	strstart,4,4
	.comm	match_start,4,4
	.local	eofile
	.comm	eofile,4,4
	.local	lookahead
	.comm	lookahead,4,4
	.comm	max_chain_length,4,4
	.local	max_lazy_match
	.comm	max_lazy_match,4,4
	.local	compr_level
	.comm	compr_level,4,4
	.comm	good_match,4,4
	.comm	nice_match,4,4
	.align 32
	.type	configuration_table, @object
	.size	configuration_table, 80
configuration_table:
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	8
	.value	4
	.value	4
	.value	5
	.value	16
	.value	8
	.value	4
	.value	6
	.value	32
	.value	32
	.value	4
	.value	4
	.value	16
	.value	16
	.value	8
	.value	16
	.value	32
	.value	32
	.value	8
	.value	16
	.value	128
	.value	128
	.value	8
	.value	32
	.value	128
	.value	256
	.value	32
	.value	128
	.value	258
	.value	1024
	.value	32
	.value	258
	.value	258
	.value	4096
	.section	.rodata
.LC115:
	.string	"bad pack level"
	.text
	.globl	lm_init
	.type	lm_init, @function
lm_init:
.LFB24:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	cmpl	$0, -20(%rbp)
	jle	.L367
	.cfi_offset 3, -24
	cmpl	$9, -20(%rbp)
	jle	.L368
.L367:
	movl	$.LC115, %edi
	call	error
.L368:
	movl	-20(%rbp), %eax
	movl	%eax, compr_level(%rip)
	movl	$prev+65536, %eax
	movl	$65536, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movl	-20(%rbp), %eax
	cltq
	movzwl	configuration_table+2(,%rax,8), %eax
	movzwl	%ax, %eax
	movl	%eax, max_lazy_match(%rip)
	movl	-20(%rbp), %eax
	cltq
	movzwl	configuration_table(,%rax,8), %eax
	movzwl	%ax, %eax
	movl	%eax, good_match(%rip)
	movl	-20(%rbp), %eax
	cltq
	movzwl	configuration_table+4(,%rax,8), %eax
	movzwl	%ax, %eax
	movl	%eax, nice_match(%rip)
	movl	-20(%rbp), %eax
	cltq
	movzwl	configuration_table+6(,%rax,8), %eax
	movzwl	%ax, %eax
	movl	%eax, max_chain_length(%rip)
	cmpl	$1, -20(%rbp)
	jne	.L369
	movq	-32(%rbp), %rax
	movzwl	(%rax), %eax
	movl	%eax, %edx
	orl	$4, %edx
	movq	-32(%rbp), %rax
	movw	%dx, (%rax)
	jmp	.L370
.L369:
	cmpl	$9, -20(%rbp)
	jne	.L370
	movq	-32(%rbp), %rax
	movzwl	(%rax), %eax
	movl	%eax, %edx
	orl	$2, %edx
	movq	-32(%rbp), %rax
	movw	%dx, (%rax)
.L370:
	movl	$0, strstart(%rip)
	movq	$0, block_start(%rip)
	movq	read_buf(%rip), %rax
	movl	$65536, %esi
	movl	$window, %edi
	call	*%rax
	movl	%eax, lookahead(%rip)
	movl	lookahead(%rip), %eax
	testl	%eax, %eax
	je	.L371
	movl	lookahead(%rip), %eax
	cmpl	$-1, %eax
	jne	.L372
.L371:
	movl	$1, eofile(%rip)
	movl	$0, lookahead(%rip)
	jmp	.L366
.L372:
	movl	$0, eofile(%rip)
	jmp	.L374
.L376:
	call	fill_window
.L374:
	movl	lookahead(%rip), %eax
	cmpl	$261, %eax
	ja	.L375
	movl	eofile(%rip), %eax
	testl	%eax, %eax
	je	.L376
.L375:
	movl	$0, ins_h(%rip)
	movl	$0, %ebx
	jmp	.L377
.L378:
	movl	ins_h(%rip), %eax
	movl	%eax, %edx
	sall	$5, %edx
	movl	%ebx, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$32767, %eax
	movl	%eax, ins_h(%rip)
	addl	$1, %ebx
.L377:
	cmpl	$1, %ebx
	jbe	.L378
.L366:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24:
	.size	lm_init, .-lm_init
	.globl	longest_match
	.type	longest_match, @function
longest_match:
.LFB25:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movl	%edi, -60(%rbp)
	movl	max_chain_length(%rip), %eax
	movl	%eax, -52(%rbp)
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	leaq	window(%rax), %rbx
	.cfi_offset 3, -56
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	prev_length(%rip), %eax
	movl	%eax, -48(%rbp)
	movl	strstart(%rip), %eax
	cmpl	$32506, %eax
	jbe	.L380
	movl	strstart(%rip), %eax
	subl	$32506, %eax
	jmp	.L381
.L380:
	movl	$0, %eax
.L381:
	movl	%eax, -44(%rbp)
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	addq	$258, %rax
	leaq	window(%rax), %r13
	movl	-48(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	%rbx, %rax
	movzbl	(%rax), %r14d
	movl	-48(%rbp), %eax
	cltq
	addq	%rbx, %rax
	movzbl	(%rax), %r15d
	movl	prev_length(%rip), %edx
	movl	good_match(%rip), %eax
	cmpl	%eax, %edx
	jb	.L382
	shrl	$2, -52(%rbp)
.L382:
	movl	-60(%rbp), %eax
	leaq	window(%rax), %r12
	movl	-48(%rbp), %eax
	cltq
	addq	%r12, %rax
	movzbl	(%rax), %eax
	cmpb	%r15b, %al
	jne	.L390
	movl	-48(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	%r12, %rax
	movzbl	(%rax), %eax
	cmpb	%r14b, %al
	jne	.L390
	movzbl	(%r12), %edx
	movzbl	(%rbx), %eax
	cmpb	%al, %dl
	jne	.L390
	addq	$1, %r12
	movzbl	(%r12), %edx
	leaq	1(%rbx), %rax
	movzbl	(%rax), %eax
	cmpb	%al, %dl
	jne	.L390
.L384:
	addq	$2, %rbx
	addq	$1, %r12
.L387:
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	addq	$1, %rbx
	movzbl	(%rbx), %edx
	addq	$1, %r12
	movzbl	(%r12), %eax
	cmpb	%al, %dl
	jne	.L386
	cmpq	%r13, %rbx
	jb	.L387
.L386:
	movq	%r13, %rdx
	movq	%rbx, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	$258, %edx
	movl	%edx, %r12d
	subl	%eax, %r12d
	leaq	-258(%r13), %rbx
	cmpl	-48(%rbp), %r12d
	jle	.L385
	movl	-60(%rbp), %eax
	movl	%eax, match_start(%rip)
	movl	%r12d, -48(%rbp)
	movl	nice_match(%rip), %eax
	cmpl	%eax, %r12d
	jge	.L391
.L388:
	movl	-48(%rbp), %eax
	cltq
	subq	$1, %rax
	addq	%rbx, %rax
	movzbl	(%rax), %r14d
	movl	-48(%rbp), %eax
	cltq
	addq	%rbx, %rax
	movzbl	(%rax), %r15d
	jmp	.L385
.L390:
	nop
.L385:
	movl	-60(%rbp), %eax
	andl	$32767, %eax
	movl	%eax, %eax
	movzwl	prev(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -60(%rbp)
	movl	-60(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jbe	.L389
	subl	$1, -52(%rbp)
	cmpl	$0, -52(%rbp)
	jne	.L382
	jmp	.L389
.L391:
	nop
.L389:
	movl	-48(%rbp), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25:
	.size	longest_match, .-longest_match
	.type	fill_window, @function
fill_window:
.LFB26:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	movq	window_size(%rip), %rax
	movl	%eax, %edx
	movl	lookahead(%rip), %eax
	subl	%eax, %edx
	movl	strstart(%rip), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -20(%rbp)
	cmpl	$-1, -20(%rbp)
	jne	.L393
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	subl	$1, -20(%rbp)
	jmp	.L394
.L393:
	movl	strstart(%rip), %eax
	cmpl	$65273, %eax
	jbe	.L394
	movl	$window+32768, %edx
	movl	$window, %eax
	movq	%rdx, %rcx
	movl	$32768, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	memcpy
	movl	match_start(%rip), %eax
	subl	$32768, %eax
	movl	%eax, match_start(%rip)
	movl	strstart(%rip), %eax
	subl	$32768, %eax
	movl	%eax, strstart(%rip)
	movq	block_start(%rip), %rax
	subq	$32768, %rax
	movq	%rax, block_start(%rip)
	movl	$0, %ebx
	jmp	.L395
.L398:
	movl	%ebx, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %r12d
	movl	%ebx, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	leaq	prev(%rax), %rdx
	cmpl	$32767, %r12d
	jbe	.L396
	leal	-32768(%r12), %eax
	jmp	.L397
.L396:
	movl	$0, %eax
.L397:
	movw	%ax, (%rdx)
	addl	$1, %ebx
.L395:
	cmpl	$32767, %ebx
	jbe	.L398
	movl	$0, %ebx
	jmp	.L399
.L402:
	movl	%ebx, %eax
	movzwl	prev(%rax,%rax), %eax
	movzwl	%ax, %r12d
	cmpl	$32767, %r12d
	jbe	.L400
	leal	-32768(%r12), %eax
	jmp	.L401
.L400:
	movl	$0, %eax
.L401:
	movl	%ebx, %edx
	movw	%ax, prev(%rdx,%rdx)
	addl	$1, %ebx
.L399:
	cmpl	$32767, %ebx
	jbe	.L402
	addl	$32768, -20(%rbp)
.L394:
	movl	eofile(%rip), %eax
	testl	%eax, %eax
	jne	.L392
	movq	read_buf(%rip), %rdx
	movl	strstart(%rip), %eax
	movl	%eax, %ecx
	movl	lookahead(%rip), %eax
	movl	%eax, %eax
	addq	%rcx, %rax
	leaq	window(%rax), %rcx
	movl	-20(%rbp), %eax
	movl	%eax, %esi
	movq	%rcx, %rdi
	call	*%rdx
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.L404
	cmpl	$-1, %ebx
	jne	.L405
.L404:
	movl	$1, eofile(%rip)
	jmp	.L392
.L405:
	movl	lookahead(%rip), %eax
	addl	%ebx, %eax
	movl	%eax, lookahead(%rip)
.L392:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
	.size	fill_window, .-fill_window
	.type	deflate_fast, @function
deflate_fast:
.LFB27:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movl	$0, -24(%rbp)
	movl	$2, prev_length(%rip)
	jmp	.L407
	.cfi_offset 3, -24
.L418:
	movl	ins_h(%rip), %eax
	movl	%eax, %edx
	sall	$5, %edx
	movl	strstart(%rip), %eax
	addl	$2, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$32767, %eax
	movl	%eax, ins_h(%rip)
	movl	strstart(%rip), %eax
	movl	%eax, %ecx
	andl	$32767, %ecx
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, %edx
	movl	%ecx, %eax
	movw	%dx, prev(%rax,%rax)
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	leaq	prev(%rax), %rdx
	movl	strstart(%rip), %eax
	movw	%ax, (%rdx)
	cmpl	$0, -20(%rbp)
	je	.L408
	movl	strstart(%rip), %eax
	subl	-20(%rbp), %eax
	cmpl	$32506, %eax
	ja	.L408
	movl	-20(%rbp), %eax
	movl	%eax, %edi
	call	longest_match
	movl	%eax, -24(%rbp)
	movl	lookahead(%rip), %eax
	cmpl	%eax, -24(%rbp)
	jbe	.L408
	movl	lookahead(%rip), %eax
	movl	%eax, -24(%rbp)
.L408:
	cmpl	$2, -24(%rbp)
	jbe	.L409
	movl	-24(%rbp), %eax
	subl	$3, %eax
	movl	%eax, %edx
	movl	strstart(%rip), %ecx
	movl	match_start(%rip), %eax
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	ct_tally
	movl	%eax, -28(%rbp)
	movl	lookahead(%rip), %eax
	subl	-24(%rbp), %eax
	movl	%eax, lookahead(%rip)
	movl	max_lazy_match(%rip), %eax
	cmpl	%eax, -24(%rbp)
	ja	.L410
	subl	$1, -24(%rbp)
.L411:
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
	movl	ins_h(%rip), %eax
	movl	%eax, %edx
	sall	$5, %edx
	movl	strstart(%rip), %eax
	addl	$2, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$32767, %eax
	movl	%eax, ins_h(%rip)
	movl	strstart(%rip), %eax
	movl	%eax, %ecx
	andl	$32767, %ecx
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, %edx
	movl	%ecx, %eax
	movw	%dx, prev(%rax,%rax)
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	leaq	prev(%rax), %rdx
	movl	strstart(%rip), %eax
	movw	%ax, (%rdx)
	subl	$1, -24(%rbp)
	cmpl	$0, -24(%rbp)
	jne	.L411
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
	jmp	.L412
.L410:
	movl	strstart(%rip), %eax
	addl	-24(%rbp), %eax
	movl	%eax, strstart(%rip)
	movl	$0, -24(%rbp)
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, ins_h(%rip)
	movl	ins_h(%rip), %eax
	movl	%eax, %edx
	sall	$5, %edx
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$32767, %eax
	movl	%eax, ins_h(%rip)
	jmp	.L412
.L409:
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$0, %edi
	call	ct_tally
	movl	%eax, -28(%rbp)
	movl	lookahead(%rip), %eax
	subl	$1, %eax
	movl	%eax, lookahead(%rip)
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
.L412:
	cmpl	$0, -28(%rbp)
	je	.L421
	movl	strstart(%rip), %eax
	movl	%eax, %edx
	movq	block_start(%rip), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, %rcx
	movq	block_start(%rip), %rax
	testq	%rax, %rax
	js	.L414
	movq	block_start(%rip), %rax
	movl	%eax, %eax
	addq	$window, %rax
	jmp	.L415
.L414:
	movl	$0, %eax
.L415:
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	flush_block
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	movq	%rax, block_start(%rip)
	jmp	.L421
.L417:
	call	fill_window
	jmp	.L416
.L421:
	nop
.L416:
	movl	lookahead(%rip), %eax
	cmpl	$261, %eax
	ja	.L407
	movl	eofile(%rip), %eax
	testl	%eax, %eax
	je	.L417
.L407:
	movl	lookahead(%rip), %eax
	testl	%eax, %eax
	jne	.L418
	movl	strstart(%rip), %eax
	movl	%eax, %edx
	movq	block_start(%rip), %rax
	movq	%rdx, %rbx
	subq	%rax, %rbx
	movq	%rbx, %rax
	movq	%rax, %rcx
	movq	block_start(%rip), %rax
	testq	%rax, %rax
	js	.L419
	movq	block_start(%rip), %rax
	movl	%eax, %eax
	addq	$window, %rax
	jmp	.L420
.L419:
	movl	$0, %eax
.L420:
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	flush_block
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27:
	.size	deflate_fast, .-deflate_fast
	.globl	deflate
	.type	deflate, @function
deflate:
.LFB28:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movl	$0, -32(%rbp)
	movl	$2, %ebx
	.cfi_offset 3, -24
	movl	compr_level(%rip), %eax
	cmpl	$3, %eax
	jg	.L444
	call	deflate_fast
	jmp	.L424
.L440:
	movl	ins_h(%rip), %eax
	movl	%eax, %edx
	sall	$5, %edx
	movl	strstart(%rip), %eax
	addl	$2, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$32767, %eax
	movl	%eax, ins_h(%rip)
	movl	strstart(%rip), %eax
	movl	%eax, %ecx
	andl	$32767, %ecx
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -28(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, %edx
	movl	%ecx, %eax
	movw	%dx, prev(%rax,%rax)
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	leaq	prev(%rax), %rdx
	movl	strstart(%rip), %eax
	movw	%ax, (%rdx)
	movl	%ebx, prev_length(%rip)
	movl	match_start(%rip), %eax
	movl	%eax, -24(%rbp)
	movl	$2, %ebx
	cmpl	$0, -28(%rbp)
	je	.L426
	movl	prev_length(%rip), %edx
	movl	max_lazy_match(%rip), %eax
	cmpl	%eax, %edx
	jae	.L426
	movl	strstart(%rip), %eax
	subl	-28(%rbp), %eax
	cmpl	$32506, %eax
	ja	.L426
	movl	-28(%rbp), %eax
	movl	%eax, %edi
	call	longest_match
	movl	%eax, %ebx
	movl	lookahead(%rip), %eax
	cmpl	%eax, %ebx
	jbe	.L427
	movl	lookahead(%rip), %ebx
.L427:
	cmpl	$3, %ebx
	jne	.L426
	movl	strstart(%rip), %edx
	movl	match_start(%rip), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	$4096, %eax
	jbe	.L426
	subl	$1, %ebx
.L426:
	movl	prev_length(%rip), %eax
	cmpl	$2, %eax
	jbe	.L428
	movl	prev_length(%rip), %eax
	cmpl	%eax, %ebx
	ja	.L428
	movl	prev_length(%rip), %eax
	subl	$3, %eax
	movl	%eax, %edx
	movl	strstart(%rip), %eax
	subl	-24(%rbp), %eax
	subl	$1, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	ct_tally
	movl	%eax, -20(%rbp)
	movl	lookahead(%rip), %edx
	movl	prev_length(%rip), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, lookahead(%rip)
	movl	prev_length(%rip), %eax
	subl	$2, %eax
	movl	%eax, prev_length(%rip)
.L429:
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
	movl	ins_h(%rip), %eax
	movl	%eax, %edx
	sall	$5, %edx
	movl	strstart(%rip), %eax
	addl	$2, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$32767, %eax
	movl	%eax, ins_h(%rip)
	movl	strstart(%rip), %eax
	movl	%eax, %ecx
	andl	$32767, %ecx
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -28(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, %edx
	movl	%ecx, %eax
	movw	%dx, prev(%rax,%rax)
	movl	ins_h(%rip), %eax
	movl	%eax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	leaq	prev(%rax), %rdx
	movl	strstart(%rip), %eax
	movw	%ax, (%rdx)
	movl	prev_length(%rip), %eax
	subl	$1, %eax
	movl	%eax, prev_length(%rip)
	movl	prev_length(%rip), %eax
	testl	%eax, %eax
	jne	.L429
	movl	$0, -32(%rbp)
	movl	$2, %ebx
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
	cmpl	$0, -20(%rbp)
	je	.L433
	movl	strstart(%rip), %eax
	movl	%eax, %edx
	movq	block_start(%rip), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, %rcx
	movq	block_start(%rip), %rax
	testq	%rax, %rax
	js	.L431
	movq	block_start(%rip), %rax
	movl	%eax, %eax
	addq	$window, %rax
	jmp	.L432
.L431:
	movl	$0, %eax
.L432:
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	flush_block
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	movq	%rax, block_start(%rip)
	jmp	.L433
.L428:
	cmpl	$0, -32(%rbp)
	je	.L434
	movl	strstart(%rip), %eax
	subl	$1, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$0, %edi
	call	ct_tally
	testl	%eax, %eax
	je	.L435
	movl	strstart(%rip), %eax
	movl	%eax, %edx
	movq	block_start(%rip), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, %rcx
	movq	block_start(%rip), %rax
	testq	%rax, %rax
	js	.L436
	movq	block_start(%rip), %rax
	movl	%eax, %eax
	addq	$window, %rax
	jmp	.L437
.L436:
	movl	$0, %eax
.L437:
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	flush_block
	movl	strstart(%rip), %eax
	movl	%eax, %eax
	movq	%rax, block_start(%rip)
.L435:
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
	movl	lookahead(%rip), %eax
	subl	$1, %eax
	movl	%eax, lookahead(%rip)
	jmp	.L438
.L434:
	movl	$1, -32(%rbp)
	movl	strstart(%rip), %eax
	addl	$1, %eax
	movl	%eax, strstart(%rip)
	movl	lookahead(%rip), %eax
	subl	$1, %eax
	movl	%eax, lookahead(%rip)
	jmp	.L438
.L433:
	jmp	.L438
.L439:
	call	fill_window
.L438:
	movl	lookahead(%rip), %eax
	cmpl	$261, %eax
	ja	.L425
	movl	eofile(%rip), %eax
	testl	%eax, %eax
	je	.L439
	jmp	.L425
.L444:
	nop
.L425:
	movl	lookahead(%rip), %eax
	testl	%eax, %eax
	jne	.L440
	cmpl	$0, -32(%rbp)
	je	.L441
	movl	strstart(%rip), %eax
	subl	$1, %eax
	movl	%eax, %eax
	movzbl	window(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$0, %edi
	call	ct_tally
.L441:
	movl	strstart(%rip), %eax
	movl	%eax, %edx
	movq	block_start(%rip), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, %rcx
	movq	block_start(%rip), %rax
	testq	%rax, %rax
	js	.L442
	movq	block_start(%rip), %rax
	movl	%eax, %eax
	addq	$window, %rax
	jmp	.L443
.L442:
	movl	$0, %eax
.L443:
	movl	$1, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	flush_block
.L424:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28:
	.size	deflate, .-deflate
	.data
	.align 32
	.type	extra_lbits, @object
	.size	extra_lbits, 116
extra_lbits:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	5
	.long	5
	.long	5
	.long	5
	.long	0
	.align 32
	.type	extra_dbits, @object
	.size	extra_dbits, 120
extra_dbits:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	6
	.long	7
	.long	7
	.long	8
	.long	8
	.long	9
	.long	9
	.long	10
	.long	10
	.long	11
	.long	11
	.long	12
	.long	12
	.long	13
	.long	13
	.align 32
	.type	extra_blbits, @object
	.size	extra_blbits, 76
extra_blbits:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	2
	.long	3
	.long	7
	.local	dyn_ltree
	.comm	dyn_ltree,2292,32
	.local	dyn_dtree
	.comm	dyn_dtree,244,32
	.local	static_ltree
	.comm	static_ltree,1152,32
	.local	static_dtree
	.comm	static_dtree,120,32
	.local	bl_tree
	.comm	bl_tree,156,32
	.align 32
	.type	l_desc, @object
	.size	l_desc, 40
l_desc:
	.quad	dyn_ltree
	.quad	static_ltree
	.quad	extra_lbits
	.long	257
	.long	286
	.long	15
	.long	0
	.align 32
	.type	d_desc, @object
	.size	d_desc, 40
d_desc:
	.quad	dyn_dtree
	.quad	static_dtree
	.quad	extra_dbits
	.long	0
	.long	30
	.long	15
	.long	0
	.align 32
	.type	bl_desc, @object
	.size	bl_desc, 40
bl_desc:
	.quad	bl_tree
	.quad	0
	.quad	extra_blbits
	.long	0
	.long	19
	.long	7
	.long	0
	.local	bl_count
	.comm	bl_count,32,32
	.align 16
	.type	bl_order, @object
	.size	bl_order, 19
bl_order:
	.byte	16
	.byte	17
	.byte	18
	.byte	0
	.byte	8
	.byte	7
	.byte	9
	.byte	6
	.byte	10
	.byte	5
	.byte	11
	.byte	4
	.byte	12
	.byte	3
	.byte	13
	.byte	2
	.byte	14
	.byte	1
	.byte	15
	.local	heap
	.comm	heap,2292,32
	.local	heap_len
	.comm	heap_len,4,4
	.local	heap_max
	.comm	heap_max,4,4
	.local	depth
	.comm	depth,573,32
	.local	length_code
	.comm	length_code,256,32
	.local	dist_code
	.comm	dist_code,512,32
	.local	base_length
	.comm	base_length,116,32
	.local	base_dist
	.comm	base_dist,120,32
	.local	flag_buf
	.comm	flag_buf,4096,32
	.local	last_lit
	.comm	last_lit,4,4
	.local	last_dist
	.comm	last_dist,4,4
	.local	last_flags
	.comm	last_flags,4,4
	.local	flags
	.comm	flags,1,1
	.local	flag_bit
	.comm	flag_bit,1,1
	.local	opt_len
	.comm	opt_len,8,8
	.local	static_len
	.comm	static_len,8,8
	.local	compressed_len
	.comm	compressed_len,8,8
	.local	input_len
	.comm	input_len,8,8
	.comm	file_type,8,8
	.comm	file_method,8,8
	.text
	.globl	ct_init
	.type	ct_init, @function
ct_init:
.LFB29:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, file_type(%rip)
	movq	-64(%rbp), %rax
	movq	%rax, file_method(%rip)
	movq	$0, input_len(%rip)
	movq	input_len(%rip), %rax
	movq	%rax, compressed_len(%rip)
	movzwl	static_dtree+2(%rip), %eax
	testw	%ax, %ax
	jne	.L472
	.cfi_offset 3, -24
.L446:
	movl	$0, -28(%rbp)
	movl	$0, -24(%rbp)
	jmp	.L448
.L451:
	movl	-24(%rbp), %eax
	cltq
	movl	-28(%rbp), %edx
	movl	%edx, base_length(,%rax,4)
	movl	$0, -36(%rbp)
	jmp	.L449
.L450:
	movl	-24(%rbp), %eax
	movl	%eax, %edx
	movl	-28(%rbp), %eax
	cltq
	movb	%dl, length_code(%rax)
	addl	$1, -28(%rbp)
	addl	$1, -36(%rbp)
.L449:
	movl	-24(%rbp), %eax
	cltq
	movl	extra_lbits(,%rax,4), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	cmpl	-36(%rbp), %eax
	jg	.L450
	addl	$1, -24(%rbp)
.L448:
	cmpl	$27, -24(%rbp)
	jle	.L451
	movl	-28(%rbp), %eax
	leal	-1(%rax), %ecx
	movl	-24(%rbp), %eax
	movl	%eax, %edx
	movslq	%ecx, %rax
	movb	%dl, length_code(%rax)
	movl	$0, -20(%rbp)
	movl	$0, -24(%rbp)
	jmp	.L452
.L455:
	movl	-24(%rbp), %eax
	cltq
	movl	-20(%rbp), %edx
	movl	%edx, base_dist(,%rax,4)
	movl	$0, -36(%rbp)
	jmp	.L453
.L454:
	movl	-24(%rbp), %eax
	movl	%eax, %edx
	movl	-20(%rbp), %eax
	cltq
	movb	%dl, dist_code(%rax)
	addl	$1, -20(%rbp)
	addl	$1, -36(%rbp)
.L453:
	movl	-24(%rbp), %eax
	cltq
	movl	extra_dbits(,%rax,4), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	cmpl	-36(%rbp), %eax
	jg	.L454
	addl	$1, -24(%rbp)
.L452:
	cmpl	$15, -24(%rbp)
	jle	.L455
	sarl	$7, -20(%rbp)
	jmp	.L456
.L459:
	movl	-20(%rbp), %eax
	movl	%eax, %edx
	sall	$7, %edx
	movl	-24(%rbp), %eax
	cltq
	movl	%edx, base_dist(,%rax,4)
	movl	$0, -36(%rbp)
	jmp	.L457
.L458:
	movl	-20(%rbp), %eax
	leal	256(%rax), %ecx
	movl	-24(%rbp), %eax
	movl	%eax, %edx
	movslq	%ecx, %rax
	movb	%dl, dist_code(%rax)
	addl	$1, -20(%rbp)
	addl	$1, -36(%rbp)
.L457:
	movl	-24(%rbp), %eax
	cltq
	movl	extra_dbits(,%rax,4), %eax
	subl	$7, %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	cmpl	-36(%rbp), %eax
	jg	.L458
	addl	$1, -24(%rbp)
.L456:
	cmpl	$29, -24(%rbp)
	jle	.L459
	movl	$0, -32(%rbp)
	jmp	.L460
.L461:
	movl	-32(%rbp), %eax
	cltq
	movw	$0, bl_count(%rax,%rax)
	addl	$1, -32(%rbp)
.L460:
	cmpl	$15, -32(%rbp)
	jle	.L461
	movl	$0, -36(%rbp)
	jmp	.L462
.L463:
	movl	-36(%rbp), %eax
	cltq
	movw	$8, static_ltree+2(,%rax,4)
	addl	$1, -36(%rbp)
	movzwl	bl_count+16(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_count+16(%rip)
.L462:
	cmpl	$143, -36(%rbp)
	jle	.L463
	jmp	.L464
.L465:
	movl	-36(%rbp), %eax
	cltq
	movw	$9, static_ltree+2(,%rax,4)
	addl	$1, -36(%rbp)
	movzwl	bl_count+18(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_count+18(%rip)
.L464:
	cmpl	$255, -36(%rbp)
	jle	.L465
	jmp	.L466
.L467:
	movl	-36(%rbp), %eax
	cltq
	movw	$7, static_ltree+2(,%rax,4)
	addl	$1, -36(%rbp)
	movzwl	bl_count+14(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_count+14(%rip)
.L466:
	cmpl	$279, -36(%rbp)
	jle	.L467
	jmp	.L468
.L469:
	movl	-36(%rbp), %eax
	cltq
	movw	$8, static_ltree+2(,%rax,4)
	addl	$1, -36(%rbp)
	movzwl	bl_count+16(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_count+16(%rip)
.L468:
	cmpl	$287, -36(%rbp)
	jle	.L469
	movl	$287, %esi
	movl	$static_ltree, %edi
	call	gen_codes
	movl	$0, -36(%rbp)
	jmp	.L470
.L471:
	movl	-36(%rbp), %eax
	cltq
	movw	$5, static_dtree+2(,%rax,4)
	movl	-36(%rbp), %eax
	movl	$5, %esi
	movl	%eax, %edi
	call	bi_reverse
	movl	%eax, %edx
	movl	-36(%rbp), %eax
	cltq
	movw	%dx, static_dtree(,%rax,4)
	addl	$1, -36(%rbp)
.L470:
	cmpl	$29, -36(%rbp)
	jle	.L471
	call	init_block
	jmp	.L445
.L472:
	nop
.L445:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29:
	.size	ct_init, .-ct_init
	.type	init_block, @function
init_block:
.LFB30:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, -4(%rbp)
	jmp	.L474
.L475:
	movl	-4(%rbp), %eax
	cltq
	movw	$0, dyn_ltree(,%rax,4)
	addl	$1, -4(%rbp)
.L474:
	cmpl	$285, -4(%rbp)
	jle	.L475
	movl	$0, -4(%rbp)
	jmp	.L476
.L477:
	movl	-4(%rbp), %eax
	cltq
	movw	$0, dyn_dtree(,%rax,4)
	addl	$1, -4(%rbp)
.L476:
	cmpl	$29, -4(%rbp)
	jle	.L477
	movl	$0, -4(%rbp)
	jmp	.L478
.L479:
	movl	-4(%rbp), %eax
	cltq
	movw	$0, bl_tree(,%rax,4)
	addl	$1, -4(%rbp)
.L478:
	cmpl	$18, -4(%rbp)
	jle	.L479
	movw	$1, dyn_ltree+1024(%rip)
	movq	$0, static_len(%rip)
	movq	static_len(%rip), %rax
	movq	%rax, opt_len(%rip)
	movl	$0, last_flags(%rip)
	movl	last_flags(%rip), %eax
	movl	%eax, last_dist(%rip)
	movl	last_dist(%rip), %eax
	movl	%eax, last_lit(%rip)
	movb	$0, flags(%rip)
	movb	$1, flag_bit(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30:
	.size	init_block, .-init_block
	.type	pqdownheap, @function
pqdownheap:
.LFB31:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	-28(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	movl	%eax, -4(%rbp)
	movl	-28(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, -8(%rbp)
	jmp	.L481
.L486:
	movl	heap_len(%rip), %eax
	cmpl	%eax, -8(%rbp)
	jge	.L482
	movl	-8(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, %dx
	jb	.L483
	movl	-8(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, %dx
	jne	.L482
	movl	-8(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	movzbl	depth(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	movzbl	depth(%rax), %eax
	cmpb	%al, %dl
	ja	.L482
.L483:
	addl	$1, -8(%rbp)
.L482:
	movl	-4(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, %dx
	jb	.L484
	movl	-4(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, %dx
	jne	.L485
	movl	-4(%rbp), %eax
	cltq
	movzbl	depth(%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	movzbl	depth(%rax), %eax
	cmpb	%al, %dl
	jbe	.L484
.L485:
	movl	-8(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %edx
	movl	-28(%rbp), %eax
	cltq
	movl	%edx, heap(,%rax,4)
	movl	-8(%rbp), %eax
	movl	%eax, -28(%rbp)
	sall	-8(%rbp)
.L481:
	movl	heap_len(%rip), %eax
	cmpl	%eax, -8(%rbp)
	jle	.L486
.L484:
	movl	-28(%rbp), %eax
	cltq
	movl	-4(%rbp), %edx
	movl	%edx, heap(,%rax,4)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31:
	.size	pqdownheap, .-pqdownheap
	.type	gen_bitlen, @function
gen_bitlen:
.LFB32:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rax
	movl	24(%rax), %eax
	movl	%eax, -20(%rbp)
	movq	-72(%rbp), %rax
	movl	36(%rax), %eax
	movl	%eax, -16(%rbp)
	movq	-72(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, -12(%rbp)
	movq	-72(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	movl	$0, -24(%rbp)
	movl	$0, -32(%rbp)
	jmp	.L488
.L489:
	movl	-32(%rbp), %eax
	cltq
	movw	$0, bl_count(%rax,%rax)
	addl	$1, -32(%rbp)
.L488:
	cmpl	$15, -32(%rbp)
	jle	.L489
	movl	heap_max(%rip), %eax
	cltq
	movl	heap(,%rax,4), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movw	$0, 2(%rax)
	movl	heap_max(%rip), %eax
	addl	$1, %eax
	movl	%eax, -40(%rbp)
	jmp	.L490
.L495:
	movl	-40(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	movl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	addl	$1, %eax
	movl	%eax, -32(%rbp)
	movl	-32(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jle	.L491
	movl	-12(%rbp), %eax
	movl	%eax, -32(%rbp)
	addl	$1, -24(%rbp)
.L491:
	movl	-36(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	-32(%rbp), %edx
	movw	%dx, 2(%rax)
	movl	-36(%rbp), %eax
	cmpl	-16(%rbp), %eax
	jg	.L506
.L492:
	movl	-32(%rbp), %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	leal	1(%rax), %edx
	movl	-32(%rbp), %eax
	cltq
	movw	%dx, bl_count(%rax,%rax)
	movl	$0, -28(%rbp)
	movl	-36(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jl	.L494
	movl	-20(%rbp), %eax
	movl	-36(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cltq
	salq	$2, %rax
	addq	-56(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -28(%rbp)
.L494:
	movl	-36(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -2(%rbp)
	movzwl	-2(%rbp), %edx
	movl	-28(%rbp), %eax
	movl	-32(%rbp), %ecx
	addl	%ecx, %eax
	cltq
	imulq	%rax, %rdx
	movq	opt_len(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, opt_len(%rip)
	cmpq	$0, -48(%rbp)
	je	.L493
	movzwl	-2(%rbp), %edx
	movl	-36(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	addl	-28(%rbp), %eax
	cltq
	imulq	%rax, %rdx
	movq	static_len(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, static_len(%rip)
	jmp	.L493
.L506:
	nop
.L493:
	addl	$1, -40(%rbp)
.L490:
	cmpl	$572, -40(%rbp)
	jle	.L495
	cmpl	$0, -24(%rbp)
	je	.L507
.L496:
	movl	-12(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -32(%rbp)
	jmp	.L498
.L499:
	subl	$1, -32(%rbp)
.L498:
	movl	-32(%rbp), %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	testw	%ax, %ax
	je	.L499
	movl	-32(%rbp), %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	leal	-1(%rax), %edx
	movl	-32(%rbp), %eax
	cltq
	movw	%dx, bl_count(%rax,%rax)
	movl	-32(%rbp), %eax
	leal	1(%rax), %ecx
	movl	-32(%rbp), %eax
	addl	$1, %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	leal	2(%rax), %edx
	movslq	%ecx, %rax
	movw	%dx, bl_count(%rax,%rax)
	movl	-12(%rbp), %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	leal	-1(%rax), %edx
	movl	-12(%rbp), %eax
	cltq
	movw	%dx, bl_count(%rax,%rax)
	subl	$2, -24(%rbp)
	cmpl	$0, -24(%rbp)
	jg	.L496
	movl	-12(%rbp), %eax
	movl	%eax, -32(%rbp)
	jmp	.L500
.L505:
	movl	-32(%rbp), %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -36(%rbp)
	jmp	.L501
.L504:
	subl	$1, -40(%rbp)
	movl	-40(%rbp), %eax
	cltq
	movl	heap(,%rax,4), %eax
	movl	%eax, -8(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-16(%rbp), %eax
	jg	.L508
.L502:
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %edx
	movl	-32(%rbp), %eax
	cmpl	%eax, %edx
	je	.L503
	movl	-32(%rbp), %eax
	movslq	%eax, %rdx
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	subq	%rax, %rdx
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	imulq	%rdx, %rax
	movq	%rax, %rdx
	movq	opt_len(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, opt_len(%rip)
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-64(%rbp), %rax
	movl	-32(%rbp), %edx
	movw	%dx, 2(%rax)
.L503:
	subl	$1, -36(%rbp)
	jmp	.L501
.L508:
	nop
.L501:
	cmpl	$0, -36(%rbp)
	jne	.L504
	subl	$1, -32(%rbp)
.L500:
	cmpl	$0, -32(%rbp)
	jne	.L505
	jmp	.L487
.L507:
	nop
.L487:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32:
	.size	gen_bitlen, .-gen_bitlen
	.type	gen_codes, @function
gen_codes:
.LFB33:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	movq	%rdi, -72(%rbp)
	movl	%esi, -76(%rbp)
	movw	$0, -18(%rbp)
	movl	$1, -32(%rbp)
	jmp	.L510
	.cfi_offset 3, -24
.L511:
	movl	-32(%rbp), %eax
	subl	$1, %eax
	cltq
	movzwl	bl_count(%rax,%rax), %eax
	addw	-18(%rbp), %ax
	addl	%eax, %eax
	movw	%ax, -18(%rbp)
	movl	-32(%rbp), %eax
	cltq
	movzwl	-18(%rbp), %edx
	movw	%dx, -64(%rbp,%rax,2)
	addl	$1, -32(%rbp)
.L510:
	cmpl	$15, -32(%rbp)
	jle	.L511
	movl	$0, -28(%rbp)
	jmp	.L512
.L515:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-72(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -24(%rbp)
	cmpl	$0, -24(%rbp)
	je	.L516
.L513:
	movl	-28(%rbp), %eax
	cltq
	salq	$2, %rax
	movq	%rax, %rbx
	addq	-72(%rbp), %rbx
	leaq	-64(%rbp), %rax
	movl	-24(%rbp), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	leaq	(%rax,%rdx), %rcx
	movl	-24(%rbp), %eax
	cltq
	movzwl	-64(%rbp,%rax,2), %eax
	movzwl	%ax, %edx
	addl	$1, %eax
	movw	%ax, (%rcx)
	movl	-24(%rbp), %eax
	movl	%eax, %esi
	movl	%edx, %edi
	call	bi_reverse
	movw	%ax, (%rbx)
	jmp	.L514
.L516:
	nop
.L514:
	addl	$1, -28(%rbp)
.L512:
	movl	-28(%rbp), %eax
	cmpl	-76(%rbp), %eax
	jle	.L515
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
	.size	gen_codes, .-gen_codes
	.type	build_tree, @function
build_tree:
.LFB34:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -40(%rbp)
	movq	-56(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-56(%rbp), %rax
	movl	28(%rax), %eax
	movl	%eax, -12(%rbp)
	movl	$-1, -20(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, -16(%rbp)
	movl	$0, heap_len(%rip)
	movl	$573, heap_max(%rip)
	movl	$0, -24(%rbp)
	jmp	.L518
.L521:
	movl	-24(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	je	.L519
	movl	heap_len(%rip), %eax
	addl	$1, %eax
	movl	%eax, heap_len(%rip)
	movl	heap_len(%rip), %edx
	movl	-24(%rbp), %eax
	movl	%eax, -20(%rbp)
	movslq	%edx, %rax
	movl	-20(%rbp), %edx
	movl	%edx, heap(,%rax,4)
	movl	-24(%rbp), %eax
	cltq
	movb	$0, depth(%rax)
	jmp	.L520
.L519:
	movl	-24(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movw	$0, 2(%rax)
.L520:
	addl	$1, -24(%rbp)
.L518:
	movl	-24(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jl	.L521
	jmp	.L522
.L525:
	movl	heap_len(%rip), %eax
	addl	$1, %eax
	movl	%eax, heap_len(%rip)
	movl	heap_len(%rip), %edx
	cmpl	$1, -20(%rbp)
	jg	.L523
	addl	$1, -20(%rbp)
	movl	-20(%rbp), %eax
	jmp	.L524
.L523:
	movl	$0, %eax
.L524:
	movslq	%edx, %rcx
	movl	%eax, heap(,%rcx,4)
	movslq	%edx, %rax
	movl	heap(,%rax,4), %eax
	movl	%eax, -8(%rbp)
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movw	$1, (%rax)
	movl	-8(%rbp), %eax
	cltq
	movb	$0, depth(%rax)
	movq	opt_len(%rip), %rax
	subq	$1, %rax
	movq	%rax, opt_len(%rip)
	cmpq	$0, -32(%rbp)
	je	.L522
	movq	static_len(%rip), %rdx
	movl	-8(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-32(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, static_len(%rip)
.L522:
	movl	heap_len(%rip), %eax
	cmpl	$1, %eax
	jle	.L525
	movq	-56(%rbp), %rax
	movl	-20(%rbp), %edx
	movl	%edx, 36(%rax)
	movl	heap_len(%rip), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -24(%rbp)
	jmp	.L526
.L527:
	movl	-24(%rbp), %edx
	movq	-40(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	pqdownheap
	subl	$1, -24(%rbp)
.L526:
	cmpl	$0, -24(%rbp)
	jg	.L527
.L530:
	movl	heap+4(%rip), %eax
	movl	%eax, -24(%rbp)
	movl	heap_len(%rip), %eax
	movslq	%eax, %rdx
	movl	heap(,%rdx,4), %edx
	movl	%edx, heap+4(%rip)
	subl	$1, %eax
	movl	%eax, heap_len(%rip)
	movq	-40(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	pqdownheap
	movl	heap+4(%rip), %eax
	movl	%eax, -4(%rbp)
	movl	heap_max(%rip), %eax
	subl	$1, %eax
	movl	%eax, heap_max(%rip)
	movl	heap_max(%rip), %eax
	cltq
	movl	-24(%rbp), %edx
	movl	%edx, heap(,%rax,4)
	movl	heap_max(%rip), %eax
	subl	$1, %eax
	movl	%eax, heap_max(%rip)
	movl	heap_max(%rip), %eax
	cltq
	movl	-4(%rbp), %edx
	movl	%edx, heap(,%rax,4)
	movl	-16(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	-24(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-40(%rbp), %rdx
	movzwl	(%rdx), %ecx
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	-40(%rbp), %rdx
	movzwl	(%rdx), %edx
	addl	%ecx, %edx
	movw	%dx, (%rax)
	movl	-24(%rbp), %eax
	cltq
	movzbl	depth(%rax), %edx
	movl	-4(%rbp), %eax
	cltq
	movzbl	depth(%rax), %eax
	cmpb	%al, %dl
	jb	.L528
	movl	-24(%rbp), %eax
	cltq
	movzbl	depth(%rax), %eax
	addl	$1, %eax
	jmp	.L529
.L528:
	movl	-4(%rbp), %eax
	cltq
	movzbl	depth(%rax), %eax
	addl	$1, %eax
.L529:
	movl	-16(%rbp), %edx
	movslq	%edx, %rdx
	movb	%al, depth(%rdx)
	movl	-24(%rbp), %eax
	cltq
	salq	$2, %rax
	movq	%rax, %rdx
	addq	-40(%rbp), %rdx
	movl	-4(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movl	-16(%rbp), %ecx
	movw	%cx, 2(%rax)
	movzwl	2(%rax), %eax
	movw	%ax, 2(%rdx)
	movl	-16(%rbp), %eax
	movl	%eax, heap+4(%rip)
	addl	$1, -16(%rbp)
	movq	-40(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	pqdownheap
	movl	heap_len(%rip), %eax
	cmpl	$1, %eax
	jg	.L530
	movl	heap_max(%rip), %eax
	subl	$1, %eax
	movl	%eax, heap_max(%rip)
	movl	heap_max(%rip), %eax
	movl	heap+4(%rip), %edx
	cltq
	movl	%edx, heap(,%rax,4)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	gen_bitlen
	movl	-20(%rbp), %edx
	movq	-40(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	gen_codes
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34:
	.size	build_tree, .-build_tree
	.type	scan_tree, @function
scan_tree:
.LFB35:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -40(%rbp)
	movl	%esi, -44(%rbp)
	movl	$-1, -24(%rbp)
	movq	-40(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	movl	$0, -16(%rbp)
	movl	$7, -12(%rbp)
	movl	$4, -8(%rbp)
	cmpl	$0, -20(%rbp)
	jne	.L532
	movl	$138, -12(%rbp)
	movl	$3, -8(%rbp)
.L532:
	movl	-44(%rbp), %eax
	cltq
	addq	$1, %rax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movw	$-1, 2(%rax)
	movl	$0, -28(%rbp)
	jmp	.L533
.L543:
	movl	-20(%rbp), %eax
	movl	%eax, -4(%rbp)
	movl	-28(%rbp), %eax
	cltq
	addq	$1, %rax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	addl	$1, -16(%rbp)
	movl	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	.L534
	movl	-4(%rbp), %eax
	cmpl	-20(%rbp), %eax
	je	.L544
.L534:
	movl	-16(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.L536
	movl	-4(%rbp), %eax
	cltq
	movzwl	bl_tree(,%rax,4), %edx
	movl	-16(%rbp), %eax
	addl	%eax, %edx
	movl	-4(%rbp), %eax
	cltq
	movw	%dx, bl_tree(,%rax,4)
	jmp	.L537
.L536:
	cmpl	$0, -4(%rbp)
	je	.L538
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	je	.L539
	movl	-4(%rbp), %eax
	cltq
	movzwl	bl_tree(,%rax,4), %eax
	leal	1(%rax), %edx
	movl	-4(%rbp), %eax
	cltq
	movw	%dx, bl_tree(,%rax,4)
.L539:
	movzwl	bl_tree+64(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_tree+64(%rip)
	jmp	.L537
.L538:
	cmpl	$10, -16(%rbp)
	jg	.L540
	movzwl	bl_tree+68(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_tree+68(%rip)
	jmp	.L537
.L540:
	movzwl	bl_tree+72(%rip), %eax
	addl	$1, %eax
	movw	%ax, bl_tree+72(%rip)
.L537:
	movl	$0, -16(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, -24(%rbp)
	cmpl	$0, -20(%rbp)
	jne	.L541
	movl	$138, -12(%rbp)
	movl	$3, -8(%rbp)
	jmp	.L535
.L541:
	movl	-4(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L542
	movl	$6, -12(%rbp)
	movl	$3, -8(%rbp)
	jmp	.L535
.L542:
	movl	$7, -12(%rbp)
	movl	$4, -8(%rbp)
	jmp	.L535
.L544:
	nop
.L535:
	addl	$1, -28(%rbp)
.L533:
	movl	-28(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jle	.L543
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE35:
	.size	scan_tree, .-scan_tree
	.type	send_tree, @function
send_tree:
.LFB36:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movl	%esi, -44(%rbp)
	movl	$-1, -24(%rbp)
	movq	-40(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	movl	$0, -16(%rbp)
	movl	$7, -12(%rbp)
	movl	$4, -8(%rbp)
	cmpl	$0, -20(%rbp)
	jne	.L546
	movl	$138, -12(%rbp)
	movl	$3, -8(%rbp)
.L546:
	movl	$0, -28(%rbp)
	jmp	.L547
.L558:
	movl	-20(%rbp), %eax
	movl	%eax, -4(%rbp)
	movl	-28(%rbp), %eax
	cltq
	addq	$1, %rax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	addl	$1, -16(%rbp)
	movl	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	.L548
	movl	-4(%rbp), %eax
	cmpl	-20(%rbp), %eax
	je	.L559
.L548:
	movl	-16(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.L550
.L551:
	movl	-4(%rbp), %eax
	cltq
	movzwl	bl_tree+2(,%rax,4), %eax
	movzwl	%ax, %edx
	movl	-4(%rbp), %eax
	cltq
	movzwl	bl_tree(,%rax,4), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	subl	$1, -16(%rbp)
	cmpl	$0, -16(%rbp)
	jne	.L551
	jmp	.L552
.L550:
	cmpl	$0, -4(%rbp)
	je	.L553
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	je	.L554
	movl	-4(%rbp), %eax
	cltq
	movzwl	bl_tree+2(,%rax,4), %eax
	movzwl	%ax, %edx
	movl	-4(%rbp), %eax
	cltq
	movzwl	bl_tree(,%rax,4), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	subl	$1, -16(%rbp)
.L554:
	movzwl	bl_tree+66(%rip), %eax
	movzwl	%ax, %edx
	movzwl	bl_tree+64(%rip), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-16(%rbp), %eax
	subl	$3, %eax
	movl	$2, %esi
	movl	%eax, %edi
	call	send_bits
	jmp	.L552
.L553:
	cmpl	$10, -16(%rbp)
	jg	.L555
	movzwl	bl_tree+70(%rip), %eax
	movzwl	%ax, %edx
	movzwl	bl_tree+68(%rip), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-16(%rbp), %eax
	subl	$3, %eax
	movl	$3, %esi
	movl	%eax, %edi
	call	send_bits
	jmp	.L552
.L555:
	movzwl	bl_tree+74(%rip), %eax
	movzwl	%ax, %edx
	movzwl	bl_tree+72(%rip), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-16(%rbp), %eax
	subl	$11, %eax
	movl	$7, %esi
	movl	%eax, %edi
	call	send_bits
.L552:
	movl	$0, -16(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, -24(%rbp)
	cmpl	$0, -20(%rbp)
	jne	.L556
	movl	$138, -12(%rbp)
	movl	$3, -8(%rbp)
	jmp	.L549
.L556:
	movl	-4(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L557
	movl	$6, -12(%rbp)
	movl	$3, -8(%rbp)
	jmp	.L549
.L557:
	movl	$7, -12(%rbp)
	movl	$4, -8(%rbp)
	jmp	.L549
.L559:
	nop
.L549:
	addl	$1, -28(%rbp)
.L547:
	movl	-28(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jle	.L558
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE36:
	.size	send_tree, .-send_tree
	.type	build_bl_tree, @function
build_bl_tree:
.LFB37:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	l_desc+36(%rip), %eax
	movl	%eax, %esi
	movl	$dyn_ltree, %edi
	call	scan_tree
	movl	d_desc+36(%rip), %eax
	movl	%eax, %esi
	movl	$dyn_dtree, %edi
	call	scan_tree
	movl	$bl_desc, %edi
	call	build_tree
	movl	$18, -4(%rbp)
	jmp	.L561
.L564:
	movl	-4(%rbp), %eax
	cltq
	movzbl	bl_order(%rax), %eax
	movzbl	%al, %eax
	cltq
	movzwl	bl_tree+2(,%rax,4), %eax
	testw	%ax, %ax
	jne	.L565
.L562:
	subl	$1, -4(%rbp)
.L561:
	cmpl	$2, -4(%rbp)
	jg	.L564
	jmp	.L563
.L565:
	nop
.L563:
	movl	-4(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	$14, %eax
	movslq	%eax, %rdx
	movq	opt_len(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, opt_len(%rip)
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE37:
	.size	build_bl_tree, .-build_bl_tree
	.type	send_all_trees, @function
send_all_trees:
.LFB38:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movl	-20(%rbp), %eax
	subl	$257, %eax
	movl	$5, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-24(%rbp), %eax
	subl	$1, %eax
	movl	$5, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-28(%rbp), %eax
	subl	$4, %eax
	movl	$4, %esi
	movl	%eax, %edi
	call	send_bits
	movl	$0, -4(%rbp)
	jmp	.L567
.L568:
	movl	-4(%rbp), %eax
	cltq
	movzbl	bl_order(%rax), %eax
	movzbl	%al, %eax
	cltq
	movzwl	bl_tree+2(,%rax,4), %eax
	movzwl	%ax, %eax
	movl	$3, %esi
	movl	%eax, %edi
	call	send_bits
	addl	$1, -4(%rbp)
.L567:
	movl	-4(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jl	.L568
	movl	-20(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %esi
	movl	$dyn_ltree, %edi
	call	send_tree
	movl	-24(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %esi
	movl	$dyn_dtree, %edi
	call	send_tree
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE38:
	.size	send_all_trees, .-send_all_trees
	.globl	flush_block
	.type	flush_block, @function
flush_block:
.LFB39:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movl	%edx, -52(%rbp)
	movl	last_flags(%rip), %eax
	movzbl	flags(%rip), %edx
	movl	%eax, %eax
	movb	%dl, flag_buf(%rax)
	movq	file_type(%rip), %rax
	movzwl	(%rax), %eax
	cmpw	$-1, %ax
	jne	.L570
	call	set_file_type
.L570:
	movl	$l_desc, %edi
	call	build_tree
	movl	$d_desc, %edi
	call	build_tree
	call	build_bl_tree
	movl	%eax, -4(%rbp)
	movq	opt_len(%rip), %rax
	addq	$10, %rax
	shrq	$3, %rax
	movq	%rax, -24(%rbp)
	movq	static_len(%rip), %rax
	addq	$10, %rax
	shrq	$3, %rax
	movq	%rax, -16(%rbp)
	movq	input_len(%rip), %rax
	addq	-48(%rbp), %rax
	movq	%rax, input_len(%rip)
	movq	-16(%rbp), %rax
	cmpq	-24(%rbp), %rax
	ja	.L571
	movq	-16(%rbp), %rax
	movq	%rax, -24(%rbp)
.L571:
	movq	-48(%rbp), %rax
	addq	$4, %rax
	cmpq	-24(%rbp), %rax
	ja	.L572
	cmpq	$0, -40(%rbp)
	je	.L572
	movl	-52(%rbp), %eax
	movl	$3, %esi
	movl	%eax, %edi
	call	send_bits
	movq	compressed_len(%rip), %rax
	addq	$10, %rax
	andq	$-8, %rax
	movq	%rax, compressed_len(%rip)
	movq	-48(%rbp), %rax
	addq	$4, %rax
	leaq	0(,%rax,8), %rdx
	movq	compressed_len(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, compressed_len(%rip)
	movq	-48(%rbp), %rax
	movl	%eax, %ecx
	movq	-40(%rbp), %rax
	movl	$1, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	copy_block
	jmp	.L573
.L572:
	movq	-16(%rbp), %rax
	cmpq	-24(%rbp), %rax
	jne	.L574
	movl	-52(%rbp), %eax
	addl	$2, %eax
	movl	$3, %esi
	movl	%eax, %edi
	call	send_bits
	movl	$static_dtree, %esi
	movl	$static_ltree, %edi
	call	compress_block
	movq	static_len(%rip), %rdx
	movq	compressed_len(%rip), %rax
	addq	%rdx, %rax
	addq	$3, %rax
	movq	%rax, compressed_len(%rip)
	jmp	.L573
.L574:
	movl	-52(%rbp), %eax
	addl	$4, %eax
	movl	$3, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-4(%rbp), %eax
	leal	1(%rax), %edx
	movl	d_desc+36(%rip), %eax
	leal	1(%rax), %ecx
	movl	l_desc+36(%rip), %eax
	addl	$1, %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	send_all_trees
	movl	$dyn_dtree, %esi
	movl	$dyn_ltree, %edi
	call	compress_block
	movq	opt_len(%rip), %rdx
	movq	compressed_len(%rip), %rax
	addq	%rdx, %rax
	addq	$3, %rax
	movq	%rax, compressed_len(%rip)
.L573:
	call	init_block
	cmpl	$0, -52(%rbp)
	je	.L575
	call	bi_windup
	movq	compressed_len(%rip), %rax
	addq	$7, %rax
	movq	%rax, compressed_len(%rip)
.L575:
	movq	compressed_len(%rip), %rax
	shrq	$3, %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE39:
	.size	flush_block, .-flush_block
	.globl	ct_tally
	.type	ct_tally, @function
ct_tally:
.LFB40:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -36(%rbp)
	movl	%esi, -40(%rbp)
	movl	last_lit(%rip), %eax
	movl	-40(%rbp), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, inbuf(%rdx)
	addl	$1, %eax
	movl	%eax, last_lit(%rip)
	cmpl	$0, -36(%rbp)
	jne	.L577
	movl	-40(%rbp), %eax
	cltq
	movzwl	dyn_ltree(,%rax,4), %eax
	leal	1(%rax), %edx
	movl	-40(%rbp), %eax
	cltq
	movw	%dx, dyn_ltree(,%rax,4)
	jmp	.L578
.L577:
	subl	$1, -36(%rbp)
	movl	-40(%rbp), %eax
	cltq
	movzbl	length_code(%rax), %eax
	movzbl	%al, %eax
	addl	$257, %eax
	movslq	%eax, %rdx
	movzwl	dyn_ltree(,%rdx,4), %edx
	addl	$1, %edx
	cltq
	movw	%dx, dyn_ltree(,%rax,4)
	cmpl	$255, -36(%rbp)
	jg	.L579
	movl	-36(%rbp), %eax
	cltq
	movzbl	dist_code(%rax), %eax
	movzbl	%al, %eax
	jmp	.L580
.L579:
	movl	-36(%rbp), %eax
	sarl	$7, %eax
	addl	$256, %eax
	cltq
	movzbl	dist_code(%rax), %eax
	movzbl	%al, %eax
.L580:
	movslq	%eax, %rdx
	movzwl	dyn_dtree(,%rdx,4), %edx
	addl	$1, %edx
	cltq
	movw	%dx, dyn_dtree(,%rax,4)
	movl	last_dist(%rip), %eax
	movl	-36(%rbp), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movw	%cx, d_buf(%rdx,%rdx)
	addl	$1, %eax
	movl	%eax, last_dist(%rip)
	movzbl	flags(%rip), %edx
	movzbl	flag_bit(%rip), %eax
	orl	%edx, %eax
	movb	%al, flags(%rip)
.L578:
	movzbl	flag_bit(%rip), %eax
	addl	%eax, %eax
	movb	%al, flag_bit(%rip)
	movl	last_lit(%rip), %eax
	andl	$7, %eax
	testl	%eax, %eax
	jne	.L581
	movl	last_flags(%rip), %eax
	movzbl	flags(%rip), %ecx
	movl	%eax, %edx
	movb	%cl, flag_buf(%rdx)
	addl	$1, %eax
	movl	%eax, last_flags(%rip)
	movb	$0, flags(%rip)
	movb	$1, flag_bit(%rip)
.L581:
	movl	level(%rip), %eax
	cmpl	$2, %eax
	jle	.L582
	movl	last_lit(%rip), %eax
	andl	$4095, %eax
	testl	%eax, %eax
	jne	.L582
	movl	last_lit(%rip), %eax
	movl	%eax, %eax
	salq	$3, %rax
	movq	%rax, -24(%rbp)
	movl	strstart(%rip), %eax
	movl	%eax, %edx
	movq	block_start(%rip), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rax, -16(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L583
.L584:
	movl	-4(%rbp), %eax
	cltq
	movzwl	dyn_dtree(,%rax,4), %eax
	movzwl	%ax, %edx
	movl	-4(%rbp), %eax
	cltq
	movl	extra_dbits(,%rax,4), %eax
	cltq
	addq	$5, %rax
	imulq	%rdx, %rax
	addq	%rax, -24(%rbp)
	addl	$1, -4(%rbp)
.L583:
	cmpl	$29, -4(%rbp)
	jle	.L584
	shrq	$3, -24(%rbp)
	movl	last_lit(%rip), %eax
	movl	%eax, %edx
	shrl	%edx
	movl	last_dist(%rip), %eax
	cmpl	%eax, %edx
	jbe	.L582
	movq	-16(%rbp), %rax
	shrq	%rax
	cmpq	-24(%rbp), %rax
	jbe	.L582
	movl	$1, %eax
	jmp	.L585
.L582:
	movl	last_lit(%rip), %eax
	cmpl	$32767, %eax
	je	.L586
	movl	last_dist(%rip), %eax
	cmpl	$32768, %eax
	jne	.L587
.L586:
	movl	$1, %eax
	jmp	.L588
.L587:
	movl	$0, %eax
.L588:
.L585:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE40:
	.size	ct_tally, .-ct_tally
	.type	compress_block, @function
compress_block:
.LFB41:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movl	$0, -32(%rbp)
	movl	$0, -28(%rbp)
	movl	$0, -24(%rbp)
	movb	$0, -1(%rbp)
	movl	last_lit(%rip), %eax
	testl	%eax, %eax
	je	.L590
.L597:
	movl	-32(%rbp), %eax
	andl	$7, %eax
	testl	%eax, %eax
	jne	.L591
	movl	-24(%rbp), %eax
	movzbl	flag_buf(%rax), %eax
	movb	%al, -1(%rbp)
	addl	$1, -24(%rbp)
.L591:
	movl	-32(%rbp), %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -20(%rbp)
	addl	$1, -32(%rbp)
	movzbl	-1(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	jne	.L592
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %edx
	movl	-20(%rbp), %eax
	cltq
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	jmp	.L593
.L592:
	movl	-20(%rbp), %eax
	cltq
	movzbl	length_code(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -16(%rbp)
	movl	-16(%rbp), %eax
	addl	$257, %eax
	movl	%eax, %eax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %edx
	movl	-16(%rbp), %eax
	addl	$257, %eax
	movl	%eax, %eax
	salq	$2, %rax
	addq	-40(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-16(%rbp), %eax
	movl	extra_lbits(,%rax,4), %eax
	movl	%eax, -12(%rbp)
	cmpl	$0, -12(%rbp)
	je	.L594
	movl	-16(%rbp), %eax
	movl	base_length(,%rax,4), %eax
	subl	%eax, -20(%rbp)
	movl	-12(%rbp), %edx
	movl	-20(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
.L594:
	movl	-28(%rbp), %eax
	movzwl	d_buf(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -8(%rbp)
	addl	$1, -28(%rbp)
	cmpl	$255, -8(%rbp)
	ja	.L595
	movl	-8(%rbp), %eax
	movzbl	dist_code(%rax), %eax
	movzbl	%al, %eax
	jmp	.L596
.L595:
	movl	-8(%rbp), %eax
	shrl	$7, %eax
	addl	$256, %eax
	movl	%eax, %eax
	movzbl	dist_code(%rax), %eax
	movzbl	%al, %eax
.L596:
	movl	%eax, -16(%rbp)
	movl	-16(%rbp), %eax
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %edx
	movl	-16(%rbp), %eax
	salq	$2, %rax
	addq	-48(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	movl	-16(%rbp), %eax
	movl	extra_dbits(,%rax,4), %eax
	movl	%eax, -12(%rbp)
	cmpl	$0, -12(%rbp)
	je	.L593
	movl	-16(%rbp), %eax
	movl	base_dist(,%rax,4), %eax
	subl	%eax, -8(%rbp)
	movl	-8(%rbp), %eax
	movl	-12(%rbp), %edx
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
.L593:
	shrb	-1(%rbp)
	movl	last_lit(%rip), %eax
	cmpl	%eax, -32(%rbp)
	jb	.L597
.L590:
	movq	-40(%rbp), %rax
	addq	$1024, %rax
	movzwl	2(%rax), %eax
	movzwl	%ax, %edx
	movq	-40(%rbp), %rax
	addq	$1024, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	send_bits
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE41:
	.size	compress_block, .-compress_block
	.type	set_file_type, @function
set_file_type:
.LFB42:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, -12(%rbp)
	movl	$0, -8(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L599
.L600:
	movl	-12(%rbp), %eax
	cltq
	movzwl	dyn_ltree(,%rax,4), %eax
	movzwl	%ax, %eax
	addl	%eax, -4(%rbp)
	addl	$1, -12(%rbp)
.L599:
	cmpl	$6, -12(%rbp)
	jle	.L600
	jmp	.L601
.L602:
	movl	-12(%rbp), %eax
	cltq
	movzwl	dyn_ltree(,%rax,4), %eax
	movzwl	%ax, %eax
	addl	%eax, -8(%rbp)
	addl	$1, -12(%rbp)
.L601:
	cmpl	$127, -12(%rbp)
	jle	.L602
	jmp	.L603
.L604:
	movl	-12(%rbp), %eax
	cltq
	movzwl	dyn_ltree(,%rax,4), %eax
	movzwl	%ax, %eax
	addl	%eax, -4(%rbp)
	addl	$1, -12(%rbp)
.L603:
	cmpl	$255, -12(%rbp)
	jle	.L604
	movq	file_type(%rip), %rax
	movl	-8(%rbp), %edx
	shrl	$2, %edx
	cmpl	-4(%rbp), %edx
	setae	%dl
	movzbl	%dl, %edx
	movw	%dx, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE42:
	.size	set_file_type, .-set_file_type
	.local	zfile
	.comm	zfile,4,4
	.local	bi_buf
	.comm	bi_buf,2,2
	.local	bi_valid
	.comm	bi_valid,4,4
	.comm	read_buf,8,8
	.globl	bi_init
	.type	bi_init, @function
bi_init:
.LFB43:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, zfile(%rip)
	movw	$0, bi_buf(%rip)
	movl	$0, bi_valid(%rip)
	movl	zfile(%rip), %eax
	cmpl	$-1, %eax
	je	.L605
	movq	$file_read, read_buf(%rip)
.L605:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE43:
	.size	bi_init, .-bi_init
	.globl	send_bits
	.type	send_bits, @function
send_bits:
.LFB44:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	$16, %eax
	movl	%eax, %edx
	subl	-24(%rbp), %edx
	movl	bi_valid(%rip), %eax
	cmpl	%eax, %edx
	jge	.L608
	.cfi_offset 3, -24
	movl	bi_valid(%rip), %eax
	movl	-20(%rbp), %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movzwl	bi_buf(%rip), %eax
	orl	%edx, %eax
	movw	%ax, bi_buf(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L609
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L610
.L609:
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L611
	call	flush_outbuf
.L611:
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L610
	call	flush_outbuf
.L610:
	movl	-20(%rbp), %eax
	movzwl	%ax, %edx
	movl	bi_valid(%rip), %eax
	movl	$16, %ecx
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	movw	%ax, bi_buf(%rip)
	movl	bi_valid(%rip), %eax
	movl	%eax, %edx
	movl	-24(%rbp), %eax
	addl	%edx, %eax
	subl	$16, %eax
	movl	%eax, bi_valid(%rip)
	jmp	.L607
.L608:
	movl	bi_valid(%rip), %eax
	movl	-20(%rbp), %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movzwl	bi_buf(%rip), %eax
	orl	%edx, %eax
	movw	%ax, bi_buf(%rip)
	movl	bi_valid(%rip), %eax
	addl	-24(%rbp), %eax
	movl	%eax, bi_valid(%rip)
.L607:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE44:
	.size	send_bits, .-send_bits
	.globl	bi_reverse
	.type	bi_reverse, @function
bi_reverse:
.LFB45:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	movl	%edi, -12(%rbp)
	movl	%esi, -16(%rbp)
	movl	$0, %ebx
	.cfi_offset 3, -24
.L614:
	movl	-12(%rbp), %eax
	andl	$1, %eax
	orl	%eax, %ebx
	shrl	-12(%rbp)
	addl	%ebx, %ebx
	subl	$1, -16(%rbp)
	cmpl	$0, -16(%rbp)
	jg	.L614
	movl	%ebx, %eax
	shrl	%eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE45:
	.size	bi_reverse, .-bi_reverse
	.globl	bi_windup
	.type	bi_windup, @function
bi_windup:
.LFB46:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	bi_valid(%rip), %eax
	cmpl	$8, %eax
	jle	.L616
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L617
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L618
.L617:
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L619
	call	flush_outbuf
.L619:
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L618
	call	flush_outbuf
	jmp	.L618
.L616:
	movl	bi_valid(%rip), %eax
	testl	%eax, %eax
	jle	.L618
	movl	outcnt(%rip), %eax
	movzwl	bi_buf(%rip), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L618
	call	flush_outbuf
.L618:
	movw	$0, bi_buf(%rip)
	movl	$0, bi_valid(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE46:
	.size	bi_windup, .-bi_windup
	.globl	copy_block
	.type	copy_block, @function
copy_block:
.LFB47:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	%edx, -16(%rbp)
	call	bi_windup
	cmpl	$0, -16(%rbp)
	je	.L629
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L622
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L623
.L622:
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L624
	call	flush_outbuf
.L624:
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L623
	call	flush_outbuf
.L623:
	movl	outcnt(%rip), %eax
	cmpl	$16381, %eax
	ja	.L625
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	movl	%edx, %ecx
	notl	%ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	notl	%edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	jmp	.L629
.L625:
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	movl	%edx, %ecx
	notl	%ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L626
	call	flush_outbuf
.L626:
	movl	outcnt(%rip), %eax
	movl	-12(%rbp), %edx
	notl	%edx
	shrw	$8, %dx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L629
	call	flush_outbuf
	jmp	.L629
.L628:
	movl	outcnt(%rip), %eax
	movq	-8(%rbp), %rdx
	movzbl	(%rdx), %edx
	movl	%edx, %ecx
	movl	%eax, %edx
	movb	%cl, outbuf(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	addq	$1, -8(%rbp)
	movl	outcnt(%rip), %eax
	cmpl	$16384, %eax
	jne	.L627
	call	flush_outbuf
	jmp	.L627
.L629:
	nop
.L627:
	cmpl	$0, -12(%rbp)
	setne	%al
	subl	$1, -12(%rbp)
	testb	%al, %al
	jne	.L628
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE47:
	.size	copy_block, .-copy_block
	.comm	decrypt,4,4
	.comm	key,8,8
	.globl	pkzip
	.bss
	.align 4
	.type	pkzip, @object
	.size	pkzip, 4
pkzip:
	.zero	4
	.globl	ext_header
	.align 4
	.type	ext_header, @object
	.size	ext_header, 4
ext_header:
	.zero	4
	.section	.rodata
	.align 8
.LC116:
	.string	"\n%s: %s: not a valid zip file\n"
	.align 8
.LC117:
	.string	"\n%s: %s: first entry not deflated or stored -- use unzip\n"
	.align 8
.LC118:
	.string	"\n%s: %s: encrypted file -- use unzip\n"
	.text
	.globl	check_zipfile
	.type	check_zipfile, @function
check_zipfile:
.LFB48:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	inptr(%rip), %eax
	movl	%eax, %eax
	addq	$inbuf, %rax
	movq	%rax, -8(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, ifd(%rip)
	movq	-8(%rbp), %rax
	addq	$26, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movq	-8(%rbp), %rdx
	addq	$27, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	leal	30(%rax), %ecx
	movq	-8(%rbp), %rax
	addq	$28, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movq	-8(%rbp), %rdx
	addq	$29, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, %edx
	movl	inptr(%rip), %eax
	addl	%edx, %eax
	movl	%eax, inptr(%rip)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	ja	.L631
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movq	-8(%rbp), %rdx
	addq	$1, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movq	-8(%rbp), %rdx
	addq	$2, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	movq	-8(%rbp), %rcx
	addq	$3, %rcx
	movzbl	(%rcx), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpq	$67324752, %rax
	je	.L632
.L631:
	movq	progname(%rip), %rdx
	movl	$.LC116, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L633
.L632:
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, method(%rip)
	movl	method(%rip), %eax
	testl	%eax, %eax
	je	.L634
	movl	method(%rip), %eax
	cmpl	$8, %eax
	je	.L634
	movq	progname(%rip), %rdx
	movl	$.LC117, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L633
.L634:
	movq	-8(%rbp), %rax
	addq	$6, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	andl	$1, %eax
	movl	%eax, decrypt(%rip)
	movl	decrypt(%rip), %eax
	testl	%eax, %eax
	je	.L635
	movq	progname(%rip), %rdx
	movl	$.LC118, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L633
.L635:
	movq	-8(%rbp), %rax
	addq	$6, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	andl	$8, %eax
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, ext_header(%rip)
	movl	$1, pkzip(%rip)
	movl	$0, %eax
.L633:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE48:
	.size	check_zipfile, .-check_zipfile
	.section	.rodata
.LC119:
	.string	"out of memory"
	.align 8
.LC120:
	.string	"invalid compressed data--format violated"
.LC121:
	.string	"len %ld, siz %ld\n"
	.align 8
.LC122:
	.string	"invalid compressed data--length mismatch"
	.align 8
.LC123:
	.string	"internal error, invalid method"
	.align 8
.LC124:
	.string	"invalid compressed data--crc error"
	.align 8
.LC125:
	.string	"invalid compressed data--length error"
	.align 8
.LC126:
	.string	"%s: %s has more than one entry--rest ignored\n"
	.align 8
.LC127:
	.string	"%s: %s has more than one entry -- unchanged\n"
	.text
	.globl	unzip
	.type	unzip, @function
unzip:
.LFB49:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$88, %rsp
	movl	%edi, -84(%rbp)
	movl	%esi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	-84(%rbp), %eax
	movl	%eax, ifd(%rip)
	movl	-88(%rbp), %eax
	movl	%eax, ofd(%rip)
	movl	$0, %esi
	movl	$0, %edi
	.cfi_offset 3, -24
	call	updcrc
	movl	pkzip(%rip), %eax
	testl	%eax, %eax
	je	.L637
	movl	ext_header(%rip), %eax
	testl	%eax, %eax
	jne	.L637
	movzbl	inbuf+14(%rip), %eax
	movzbl	%al, %eax
	movzbl	inbuf+15(%rip), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	inbuf+16(%rip), %edx
	movzbl	%dl, %edx
	movzbl	inbuf+17(%rip), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movzbl	inbuf+22(%rip), %eax
	movzbl	%al, %eax
	movzbl	inbuf+23(%rip), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	inbuf+24(%rip), %edx
	movzbl	%dl, %edx
	movzbl	inbuf+25(%rip), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -72(%rbp)
.L637:
	movl	method(%rip), %eax
	cmpl	$8, %eax
	jne	.L638
	call	inflate
	movl	%eax, -56(%rbp)
	cmpl	$3, -56(%rbp)
	jne	.L639
	movl	$.LC119, %edi
	call	error
	jmp	.L640
.L639:
	cmpl	$0, -56(%rbp)
	je	.L640
	movl	$.LC120, %edi
	call	error
	jmp	.L640
.L638:
	movl	pkzip(%rip), %eax
	testl	%eax, %eax
	je	.L641
	movl	method(%rip), %eax
	testl	%eax, %eax
	jne	.L641
	movzbl	inbuf+22(%rip), %eax
	movzbl	%al, %eax
	movzbl	inbuf+23(%rip), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	inbuf+24(%rip), %edx
	movzbl	%dl, %edx
	movzbl	inbuf+25(%rip), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	movq	%rax, %rbx
	orq	%rdx, %rbx
	movzbl	inbuf+18(%rip), %eax
	movzbl	%al, %eax
	movzbl	inbuf+19(%rip), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	inbuf+20(%rip), %edx
	movzbl	%dl, %edx
	movzbl	inbuf+21(%rip), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rax, %rdx
	movl	decrypt(%rip), %eax
	testl	%eax, %eax
	je	.L642
	movl	$12, %eax
	jmp	.L643
.L642:
	movl	$0, %eax
.L643:
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	cmpq	%rbx, %rax
	je	.L666
	movzbl	inbuf+18(%rip), %eax
	movzbl	%al, %eax
	movzbl	inbuf+19(%rip), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	inbuf+20(%rip), %edx
	movzbl	%dl, %edx
	movzbl	inbuf+21(%rip), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rax, %rdx
	movl	$.LC121, %esi
	movq	stderr(%rip), %rax
	movq	%rdx, %rcx
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$.LC122, %edi
	call	error
	jmp	.L666
.L648:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L646
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L647
.L646:
	movl	$0, %edi
	call	fill_inbuf
.L647:
	movb	%al, -49(%rbp)
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movzbl	-49(%rbp), %ecx
	movb	%cl, window(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$32768, %eax
	jne	.L645
	call	flush_window
	jmp	.L645
.L666:
	nop
.L645:
	testq	%rbx, %rbx
	setne	%al
	subq	$1, %rbx
	testb	%al, %al
	jne	.L648
	call	flush_window
	jmp	.L640
.L641:
	movl	$.LC123, %edi
	call	error
.L640:
	movl	pkzip(%rip), %eax
	testl	%eax, %eax
	jne	.L649
	movl	$0, -60(%rbp)
	jmp	.L650
.L653:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L651
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L652
.L651:
	movl	$0, %edi
	call	fill_inbuf
.L652:
	movl	-60(%rbp), %edx
	movslq	%edx, %rdx
	movb	%al, -48(%rbp,%rdx)
	addl	$1, -60(%rbp)
.L650:
	cmpl	$7, -60(%rbp)
	jle	.L653
	movzbl	-48(%rbp), %eax
	movzbl	%al, %eax
	movzbl	-47(%rbp), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	-46(%rbp), %edx
	movzbl	%dl, %edx
	movzbl	-45(%rbp), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movzbl	-44(%rbp), %eax
	movzbl	%al, %eax
	movzbl	-43(%rbp), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	-42(%rbp), %edx
	movzbl	%dl, %edx
	movzbl	-41(%rbp), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L654
.L649:
	movl	ext_header(%rip), %eax
	testl	%eax, %eax
	je	.L654
	movl	$0, -60(%rbp)
	jmp	.L655
.L658:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L656
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L657
.L656:
	movl	$0, %edi
	call	fill_inbuf
.L657:
	movl	-60(%rbp), %edx
	movslq	%edx, %rdx
	movb	%al, -48(%rbp,%rdx)
	addl	$1, -60(%rbp)
.L655:
	cmpl	$15, -60(%rbp)
	jle	.L658
	movzbl	-44(%rbp), %eax
	movzbl	%al, %eax
	movzbl	-43(%rbp), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	-42(%rbp), %edx
	movzbl	%dl, %edx
	movzbl	-41(%rbp), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movzbl	-36(%rbp), %eax
	movzbl	%al, %eax
	movzbl	-35(%rbp), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movzbl	-34(%rbp), %edx
	movzbl	%dl, %edx
	movzbl	-33(%rbp), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	movq	%rax, -72(%rbp)
.L654:
	movl	$0, %esi
	movl	$outbuf, %edi
	call	updcrc
	cmpq	-80(%rbp), %rax
	je	.L659
	movl	$.LC124, %edi
	call	error
.L659:
	movq	bytes_out(%rip), %rax
	cmpq	-72(%rbp), %rax
	je	.L660
	movl	$.LC125, %edi
	call	error
.L660:
	movl	pkzip(%rip), %eax
	testl	%eax, %eax
	je	.L661
	movl	inptr(%rip), %eax
	leal	4(%rax), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L661
	movl	inptr(%rip), %eax
	movl	%eax, %eax
	addq	$inbuf, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	inptr(%rip), %edx
	movl	%edx, %edx
	addq	$1, %rdx
	addq	$inbuf, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%edx, %eax
	cltq
	movl	inptr(%rip), %edx
	movl	%edx, %edx
	addq	$2, %rdx
	addq	$inbuf, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	movl	inptr(%rip), %ecx
	movl	%ecx, %ecx
	addq	$3, %rcx
	addq	$inbuf, %rcx
	movzbl	(%rcx), %ecx
	movzbl	%cl, %ecx
	sall	$8, %ecx
	orl	%ecx, %edx
	movslq	%edx, %rdx
	salq	$16, %rdx
	orq	%rdx, %rax
	cmpq	$67324752, %rax
	jne	.L661
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L662
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L663
	movq	progname(%rip), %rdx
	movl	$.LC126, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L663:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L661
	movl	$2, exit_code(%rip)
	jmp	.L661
.L662:
	movq	progname(%rip), %rdx
	movl	$.LC127, %esi
	movq	stderr(%rip), %rax
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$0, pkzip(%rip)
	movl	pkzip(%rip), %eax
	movl	%eax, ext_header(%rip)
	movl	$1, %eax
	jmp	.L664
.L661:
	movl	$0, pkzip(%rip)
	movl	pkzip(%rip), %eax
	movl	%eax, ext_header(%rip)
	movl	$0, %eax
.L664:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L665
	call	__stack_chk_fail
.L665:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE49:
	.size	unzip, .-unzip
	.data
	.align 32
	.type	border, @object
	.size	border, 76
border:
	.long	16
	.long	17
	.long	18
	.long	0
	.long	8
	.long	7
	.long	9
	.long	6
	.long	10
	.long	5
	.long	11
	.long	4
	.long	12
	.long	3
	.long	13
	.long	2
	.long	14
	.long	1
	.long	15
	.align 32
	.type	cplens, @object
	.size	cplens, 62
cplens:
	.value	3
	.value	4
	.value	5
	.value	6
	.value	7
	.value	8
	.value	9
	.value	10
	.value	11
	.value	13
	.value	15
	.value	17
	.value	19
	.value	23
	.value	27
	.value	31
	.value	35
	.value	43
	.value	51
	.value	59
	.value	67
	.value	83
	.value	99
	.value	115
	.value	131
	.value	163
	.value	195
	.value	227
	.value	258
	.value	0
	.value	0
	.align 32
	.type	cplext, @object
	.size	cplext, 62
cplext:
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	3
	.value	3
	.value	3
	.value	3
	.value	4
	.value	4
	.value	4
	.value	4
	.value	5
	.value	5
	.value	5
	.value	5
	.value	0
	.value	99
	.value	99
	.align 32
	.type	cpdist, @object
	.size	cpdist, 60
cpdist:
	.value	1
	.value	2
	.value	3
	.value	4
	.value	5
	.value	7
	.value	9
	.value	13
	.value	17
	.value	25
	.value	33
	.value	49
	.value	65
	.value	97
	.value	129
	.value	193
	.value	257
	.value	385
	.value	513
	.value	769
	.value	1025
	.value	1537
	.value	2049
	.value	3073
	.value	4097
	.value	6145
	.value	8193
	.value	12289
	.value	16385
	.value	24577
	.align 32
	.type	cpdext, @object
	.size	cpdext, 60
cpdext:
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1
	.value	1
	.value	2
	.value	2
	.value	3
	.value	3
	.value	4
	.value	4
	.value	5
	.value	5
	.value	6
	.value	6
	.value	7
	.value	7
	.value	8
	.value	8
	.value	9
	.value	9
	.value	10
	.value	10
	.value	11
	.value	11
	.value	12
	.value	12
	.value	13
	.value	13
	.comm	bb,8,8
	.comm	bk,4,4
	.globl	mask_bits
	.align 32
	.type	mask_bits, @object
	.size	mask_bits, 34
mask_bits:
	.value	0
	.value	1
	.value	3
	.value	7
	.value	15
	.value	31
	.value	63
	.value	127
	.value	255
	.value	511
	.value	1023
	.value	2047
	.value	4095
	.value	8191
	.value	16383
	.value	32767
	.value	-1
	.globl	lbits
	.align 4
	.type	lbits, @object
	.size	lbits, 4
lbits:
	.long	9
	.globl	dbits
	.align 4
	.type	dbits, @object
	.size	dbits, 4
dbits:
	.long	6
	.comm	hufts,4,4
	.text
	.globl	huft_build
	.type	huft_build, @function
huft_build:
.LFB50:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1560, %rsp
	movq	%rdi, -1560(%rbp)
	movl	%esi, -1564(%rbp)
	movl	%edx, -1568(%rbp)
	movq	%rcx, -1576(%rbp)
	movq	%r8, -1584(%rbp)
	movq	%r9, -1592(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, %rdx
	movl	$0, %eax
	movl	$8, %ecx
	movq	%rdx, %rdi
	rep stosq
	movq	%rdi, %rdx
	movl	%eax, (%rdx)
	addq	$4, %rdx
	movq	-1560(%rbp), %r13
	.cfi_offset 3, -56
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	-1564(%rbp), %r12d
.L668:
	movl	0(%r13), %eax
	movl	%eax, %edx
	movl	-272(%rbp,%rdx,4), %edx
	addl	$1, %edx
	movl	%eax, %eax
	movl	%edx, -272(%rbp,%rax,4)
	addq	$4, %r13
	subl	$1, %r12d
	testl	%r12d, %r12d
	jne	.L668
	movl	-272(%rbp), %eax
	cmpl	-1564(%rbp), %eax
	jne	.L669
	movq	-1592(%rbp), %rax
	movq	$0, (%rax)
	movq	16(%rbp), %rax
	movl	$0, (%rax)
	movl	$0, %eax
	jmp	.L670
.L669:
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -64(%rbp)
	movl	$1, %ebx
	jmp	.L671
.L674:
	movl	%ebx, %eax
	movl	-272(%rbp,%rax,4), %eax
	testl	%eax, %eax
	jne	.L716
.L672:
	addl	$1, %ebx
.L671:
	cmpl	$16, %ebx
	jbe	.L674
	jmp	.L673
.L716:
	nop
.L673:
	movl	%ebx, -1596(%rbp)
	movl	-64(%rbp), %eax
	cmpl	%ebx, %eax
	jae	.L675
	movl	%ebx, -64(%rbp)
.L675:
	movl	$16, %r12d
	jmp	.L676
.L679:
	movl	%r12d, %eax
	movl	-272(%rbp,%rax,4), %eax
	testl	%eax, %eax
	jne	.L717
.L677:
	subl	$1, %r12d
.L676:
	testl	%r12d, %r12d
	jne	.L679
	jmp	.L678
.L717:
	nop
.L678:
	movl	%r12d, -52(%rbp)
	movl	-64(%rbp), %eax
	cmpl	%r12d, %eax
	jbe	.L680
	movl	%r12d, -64(%rbp)
.L680:
	movq	16(%rbp), %rax
	movl	-64(%rbp), %edx
	movl	%edx, (%rax)
	movl	%ebx, %eax
	movl	$1, %edx
	movl	%edx, %esi
	movl	%eax, %ecx
	sall	%cl, %esi
	movl	%esi, %eax
	movl	%eax, -60(%rbp)
	jmp	.L681
.L683:
	movl	-60(%rbp), %edx
	movl	%ebx, %eax
	movl	-272(%rbp,%rax,4), %eax
	movl	%edx, %edi
	subl	%eax, %edi
	movl	%edi, %eax
	movl	%eax, -60(%rbp)
	cmpl	$0, -60(%rbp)
	jns	.L682
	movl	$2, %eax
	jmp	.L670
.L682:
	addl	$1, %ebx
	sall	-60(%rbp)
.L681:
	cmpl	%r12d, %ebx
	jb	.L683
	movl	-60(%rbp), %edx
	movl	%r12d, %eax
	movl	-272(%rbp,%rax,4), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -60(%rbp)
	cmpl	$0, -60(%rbp)
	jns	.L684
	movl	$2, %eax
	jmp	.L670
.L684:
	movl	%r12d, %eax
	movl	-272(%rbp,%rax,4), %edx
	movl	-60(%rbp), %eax
	addl	%eax, %edx
	movl	%r12d, %eax
	movl	%edx, -272(%rbp,%rax,4)
	movl	$0, %ebx
	movl	%ebx, -188(%rbp)
	leaq	-272(%rbp), %rax
	leaq	4(%rax), %r13
	leaq	-192(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -88(%rbp)
	jmp	.L685
.L686:
	movl	0(%r13), %eax
	addl	%eax, %ebx
	movq	-88(%rbp), %rax
	movl	%ebx, (%rax)
	addq	$4, -88(%rbp)
	addq	$4, %r13
.L685:
	subl	$1, %r12d
	testl	%r12d, %r12d
	jne	.L686
	movq	-1560(%rbp), %r13
	movl	$0, %r12d
.L688:
	movl	0(%r13), %ebx
	testl	%ebx, %ebx
	setne	%al
	addq	$4, %r13
	testb	%al, %al
	je	.L687
	leaq	-192(%rbp), %rax
	movl	%ebx, %edx
	salq	$2, %rdx
	leaq	(%rax,%rdx), %rcx
	movl	%ebx, %eax
	movl	-192(%rbp,%rax,4), %eax
	movl	%eax, %edx
	movl	%r12d, -1552(%rbp,%rdx,4)
	addl	$1, %eax
	movl	%eax, (%rcx)
.L687:
	addl	$1, %r12d
	cmpl	-1564(%rbp), %r12d
	jb	.L688
	movl	$0, %r12d
	movl	%r12d, -192(%rbp)
	leaq	-1552(%rbp), %r13
	movl	$-1, -68(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, %r14d
	negl	%r14d
	movq	$0, -400(%rbp)
	movl	$0, %r15d
	movl	$0, -56(%rbp)
	jmp	.L689
.L713:
	movslq	-1596(%rbp), %rax
	movl	-272(%rbp,%rax,4), %eax
	movl	%eax, -76(%rbp)
	jmp	.L690
.L700:
	addl	$1, -68(%rbp)
	addl	-64(%rbp), %r14d
	movl	-52(%rbp), %eax
	subl	%r14d, %eax
	movl	%eax, -56(%rbp)
	movl	-64(%rbp), %eax
	cmpl	%eax, -56(%rbp)
	jbe	.L692
	movl	-64(%rbp), %eax
	jmp	.L693
.L692:
	movl	-56(%rbp), %eax
.L693:
	movl	%eax, -56(%rbp)
	movl	-1596(%rbp), %eax
	subl	%r14d, %eax
	movl	%eax, %ebx
	movl	%ebx, %eax
	movl	$1, %edx
	movl	%edx, %esi
	movl	%eax, %ecx
	sall	%cl, %esi
	movl	%esi, %eax
	movl	%eax, -72(%rbp)
	movl	-76(%rbp), %eax
	addl	$1, %eax
	cmpl	%eax, -72(%rbp)
	jbe	.L694
	movl	-76(%rbp), %eax
	notl	%eax
	addl	%eax, -72(%rbp)
	movslq	-1596(%rbp), %rax
	leaq	0(,%rax,4), %rdx
	leaq	-272(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	jmp	.L695
.L697:
	sall	-72(%rbp)
	addq	$4, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	cmpl	%eax, -72(%rbp)
	jbe	.L718
.L696:
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	subl	%eax, -72(%rbp)
.L695:
	addl	$1, %ebx
	cmpl	-56(%rbp), %ebx
	jb	.L697
	jmp	.L694
.L718:
	nop
.L694:
	movl	%ebx, %eax
	movl	$1, %edx
	movl	%edx, %esi
	movl	%eax, %ecx
	sall	%cl, %esi
	movl	%esi, %eax
	movl	%eax, -56(%rbp)
	movl	-56(%rbp), %eax
	addl	$1, %eax
	movl	%eax, %eax
	salq	$4, %rax
	movq	%rax, %rdi
	call	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.L698
	cmpl	$0, -68(%rbp)
	je	.L699
	movq	-400(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
.L699:
	movl	$3, %eax
	jmp	.L670
.L698:
	movl	hufts(%rip), %eax
	addl	-56(%rbp), %eax
	addl	$1, %eax
	movl	%eax, hufts(%rip)
	leaq	16(%r15), %rdx
	movq	-1592(%rbp), %rax
	movq	%rdx, (%rax)
	leaq	8(%r15), %rax
	movq	%rax, -1592(%rbp)
	movq	-1592(%rbp), %rax
	movq	$0, (%rax)
	addq	$16, %r15
	movl	-68(%rbp), %eax
	cltq
	movq	%r15, -400(%rbp,%rax,8)
	cmpl	$0, -68(%rbp)
	je	.L691
	movl	-68(%rbp), %eax
	cltq
	movl	%r12d, -192(%rbp,%rax,4)
	movl	-64(%rbp), %eax
	movb	%al, -111(%rbp)
	movl	%ebx, %eax
	addl	$16, %eax
	movb	%al, -112(%rbp)
	movq	%r15, -104(%rbp)
	movl	%r14d, %eax
	subl	-64(%rbp), %eax
	movl	%r12d, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	-68(%rbp), %eax
	subl	$1, %eax
	cltq
	movq	-400(%rbp,%rax,8), %rax
	movl	%ebx, %edx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	-112(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-104(%rbp), %rdx
	movq	%rdx, 8(%rax)
	jmp	.L691
.L719:
	nop
.L691:
	movl	%r14d, %eax
	addl	-64(%rbp), %eax
	cmpl	-1596(%rbp), %eax
	jl	.L700
	movzbl	-1596(%rbp), %edx
	movl	%r14d, %eax
	movl	%edx, %ebx
	subb	%al, %bl
	movl	%ebx, %eax
	movb	%al, -111(%rbp)
	movl	-1564(%rbp), %eax
	leaq	0(,%rax,4), %rdx
	leaq	-1552(%rbp), %rax
	addq	%rdx, %rax
	cmpq	%r13, %rax
	ja	.L701
	movb	$99, -112(%rbp)
	jmp	.L702
.L701:
	movl	0(%r13), %eax
	cmpl	-1568(%rbp), %eax
	jae	.L703
	movl	0(%r13), %eax
	cmpl	$255, %eax
	ja	.L704
	movl	$16, %eax
	jmp	.L705
.L704:
	movl	$15, %eax
.L705:
	movb	%al, -112(%rbp)
	movl	0(%r13), %eax
	movw	%ax, -104(%rbp)
	addq	$4, %r13
	jmp	.L702
.L703:
	movl	0(%r13), %eax
	subl	-1568(%rbp), %eax
	movl	%eax, %eax
	addq	%rax, %rax
	addq	-1584(%rbp), %rax
	movzwl	(%rax), %eax
	movb	%al, -112(%rbp)
	movl	0(%r13), %eax
	subl	-1568(%rbp), %eax
	movl	%eax, %eax
	addq	%rax, %rax
	addq	-1576(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -104(%rbp)
	addq	$4, %r13
.L702:
	movl	-1596(%rbp), %eax
	subl	%r14d, %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, -72(%rbp)
	movl	%r12d, %ebx
	movl	%r14d, %ecx
	shrl	%cl, %ebx
	jmp	.L706
.L707:
	movl	%ebx, %eax
	salq	$4, %rax
	addq	%r15, %rax
	movq	-112(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-104(%rbp), %rdx
	movq	%rdx, 8(%rax)
	addl	-72(%rbp), %ebx
.L706:
	cmpl	-56(%rbp), %ebx
	jb	.L707
	movl	-1596(%rbp), %eax
	subl	$1, %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	jmp	.L708
.L709:
	xorl	%ebx, %r12d
	shrl	%ebx
.L708:
	movl	%r12d, %eax
	andl	%ebx, %eax
	testl	%eax, %eax
	jne	.L709
	xorl	%ebx, %r12d
	jmp	.L710
.L711:
	subl	$1, -68(%rbp)
	subl	-64(%rbp), %r14d
.L710:
	movl	$1, %eax
	movl	%r14d, %ecx
	sall	%cl, %eax
	subl	$1, %eax
	movl	%eax, %edx
	andl	%r12d, %edx
	movl	-68(%rbp), %eax
	cltq
	movl	-192(%rbp,%rax,4), %eax
	cmpl	%eax, %edx
	jne	.L711
.L690:
	cmpl	$0, -76(%rbp)
	setne	%al
	subl	$1, -76(%rbp)
	testb	%al, %al
	jne	.L719
	addl	$1, -1596(%rbp)
.L689:
	movl	-1596(%rbp), %ebx
	cmpl	-52(%rbp), %ebx
	jle	.L713
	cmpl	$0, -60(%rbp)
	je	.L714
	cmpl	$1, -52(%rbp)
	je	.L714
	movl	$1, %eax
	jmp	.L715
.L714:
	movl	$0, %eax
.L715:
.L670:
	addq	$1560, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE50:
	.size	huft_build, .-huft_build
	.globl	huft_free
	.type	huft_free, @function
huft_free:
.LFB51:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rbx
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	jmp	.L721
.L722:
	subq	$16, %rbx
	movq	8(%rbx), %r12
	movq	%rbx, %rdi
	call	free
	movq	%r12, %rbx
.L721:
	testq	%rbx, %rbx
	jne	.L722
	movl	$0, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE51:
	.size	huft_free, .-huft_free
	.globl	inflate_codes
	.type	inflate_codes, @function
inflate_codes:
.LFB52:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movl	%edx, -84(%rbp)
	movl	%ecx, -88(%rbp)
	movq	bb(%rip), %r13
	.cfi_offset 3, -40
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	bk(%rip), %r12d
	movl	outcnt(%rip), %eax
	movl	%eax, -44(%rbp)
	movl	-84(%rbp), %eax
	cltq
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -40(%rbp)
	movl	-88(%rbp), %eax
	cltq
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -36(%rbp)
	jmp	.L724
.L769:
	nop
.L767:
	jmp	.L724
.L727:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L725
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L726
.L725:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L726:
	movl	%r12d, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	addl	$8, %r12d
.L724:
	movl	-84(%rbp), %eax
	cmpl	%r12d, %eax
	ja	.L727
	movl	%r13d, %eax
	andl	-40(%rbp), %eax
	movl	%eax, %eax
	salq	$4, %rax
	addq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ebx
	cmpl	$16, %ebx
	jbe	.L728
.L735:
	cmpl	$99, %ebx
	jne	.L729
	movl	$1, %eax
	jmp	.L730
.L729:
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	shrq	%cl, %r13
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	subl	%eax, %r12d
	subl	$16, %ebx
	jmp	.L731
.L734:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L732
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L733
.L732:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L733:
	movl	%r12d, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	addl	$8, %r12d
.L731:
	cmpl	%ebx, %r12d
	jb	.L734
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdx
	movl	%r13d, %ecx
	movl	%ebx, %eax
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	andl	%ecx, %eax
	movl	%eax, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ebx
	cmpl	$16, %ebx
	ja	.L735
.L728:
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	shrq	%cl, %r13
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	subl	%eax, %r12d
	cmpl	$16, %ebx
	jne	.L736
	movq	-64(%rbp), %rax
	movzwl	8(%rax), %eax
	movl	%eax, %edx
	movl	-44(%rbp), %eax
	movb	%dl, window(%rax)
	addl	$1, -44(%rbp)
	cmpl	$32768, -44(%rbp)
	jne	.L769
	movl	-44(%rbp), %eax
	movl	%eax, outcnt(%rip)
	call	flush_window
	movl	$0, -44(%rbp)
	jmp	.L769
.L736:
	cmpl	$15, %ebx
	jne	.L740
	jmp	.L768
.L743:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L741
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L742
.L741:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L742:
	movl	%r12d, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	addl	$8, %r12d
.L740:
	cmpl	%ebx, %r12d
	jb	.L743
	movq	-64(%rbp), %rax
	movzwl	8(%rax), %eax
	movzwl	%ax, %edx
	movl	%r13d, %ecx
	movl	%ebx, %eax
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	andl	%ecx, %eax
	addl	%edx, %eax
	movl	%eax, -52(%rbp)
	movl	%ebx, %eax
	movl	%eax, %ecx
	shrq	%cl, %r13
	subl	%ebx, %r12d
	jmp	.L744
.L747:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L745
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L746
.L745:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L746:
	movl	%r12d, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	addl	$8, %r12d
.L744:
	movl	-88(%rbp), %eax
	cmpl	%r12d, %eax
	ja	.L747
	movl	%r13d, %eax
	andl	-36(%rbp), %eax
	movl	%eax, %eax
	salq	$4, %rax
	addq	-80(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ebx
	cmpl	$16, %ebx
	jbe	.L748
.L754:
	cmpl	$99, %ebx
	jne	.L749
	movl	$1, %eax
	jmp	.L730
.L749:
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	shrq	%cl, %r13
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	subl	%eax, %r12d
	subl	$16, %ebx
	jmp	.L750
.L753:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L751
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L752
.L751:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L752:
	movl	%r12d, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	addl	$8, %r12d
.L750:
	cmpl	%ebx, %r12d
	jb	.L753
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdx
	movl	%r13d, %ecx
	movl	%ebx, %eax
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	andl	%ecx, %eax
	movl	%eax, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %ebx
	cmpl	$16, %ebx
	ja	.L754
.L748:
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %ecx
	shrq	%cl, %r13
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	subl	%eax, %r12d
	jmp	.L755
.L758:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L756
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L757
.L756:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L757:
	movl	%r12d, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	addl	$8, %r12d
.L755:
	cmpl	%ebx, %r12d
	jb	.L758
	movq	-64(%rbp), %rax
	movzwl	8(%rax), %eax
	movzwl	%ax, %eax
	movl	-44(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%r13d, %edx
	movl	%ebx, %eax
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	andl	%edx, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -48(%rbp)
	movl	%ebx, %eax
	movl	%eax, %ecx
	shrq	%cl, %r13
	subl	%ebx, %r12d
.L766:
	andl	$32767, -48(%rbp)
	movl	-48(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jbe	.L759
	movl	$32768, %eax
	subl	-48(%rbp), %eax
	jmp	.L760
.L759:
	movl	$32768, %eax
	subl	-44(%rbp), %eax
.L760:
	movl	%eax, %ebx
	cmpl	-52(%rbp), %ebx
	jbe	.L761
	movl	-52(%rbp), %eax
	jmp	.L762
.L761:
	movl	%ebx, %eax
.L762:
	movl	%eax, %ebx
	subl	%ebx, -52(%rbp)
	movl	-48(%rbp), %eax
	movl	-44(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	%ebx, %eax
	jb	.L763
	movl	%ebx, %edx
	movl	$window, %ecx
	movl	-48(%rbp), %eax
	leaq	(%rcx,%rax), %rsi
	movl	$window, %ecx
	movl	-44(%rbp), %eax
	addq	%rcx, %rax
	movq	%rsi, %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	memcpy
	addl	%ebx, -44(%rbp)
	addl	%ebx, -48(%rbp)
	jmp	.L764
.L763:
	movl	-48(%rbp), %eax
	movzbl	window(%rax), %edx
	movl	-44(%rbp), %eax
	movb	%dl, window(%rax)
	addl	$1, -44(%rbp)
	addl	$1, -48(%rbp)
	subl	$1, %ebx
	testl	%ebx, %ebx
	jne	.L763
.L764:
	cmpl	$32768, -44(%rbp)
	jne	.L765
	movl	-44(%rbp), %eax
	movl	%eax, outcnt(%rip)
	call	flush_window
	movl	$0, -44(%rbp)
.L765:
	cmpl	$0, -52(%rbp)
	jne	.L766
	jmp	.L769
.L768:
	movl	-44(%rbp), %eax
	movl	%eax, outcnt(%rip)
	movq	%r13, bb(%rip)
	movl	%r12d, bk(%rip)
	movl	$0, %eax
.L730:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE52:
	.size	inflate_codes, .-inflate_codes
	.globl	inflate_stored
	.type	inflate_stored, @function
inflate_stored:
.LFB53:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	movq	bb(%rip), %r12
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	movl	bk(%rip), %ebx
	movl	outcnt(%rip), %eax
	movl	%eax, -20(%rbp)
	movl	%ebx, %eax
	andl	$7, %eax
	movl	%eax, -24(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, %ecx
	shrq	%cl, %r12
	subl	-24(%rbp), %ebx
	jmp	.L771
.L774:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L772
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L773
.L772:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L773:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L771:
	cmpl	$15, %ebx
	jbe	.L774
	movl	%r12d, %eax
	andl	$65535, %eax
	movl	%eax, -24(%rbp)
	shrq	$16, %r12
	subl	$16, %ebx
	jmp	.L775
.L778:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L776
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L777
.L776:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L777:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L775:
	cmpl	$15, %ebx
	jbe	.L778
	movl	%r12d, %eax
	notl	%eax
	andl	$65535, %eax
	cmpl	-24(%rbp), %eax
	je	.L779
	movl	$1, %eax
	jmp	.L780
.L779:
	shrq	$16, %r12
	subl	$16, %ebx
	jmp	.L781
.L785:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L783
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L784
.L783:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L784:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L782:
	cmpl	$7, %ebx
	jbe	.L785
	movl	%r12d, %edx
	movl	-20(%rbp), %eax
	movb	%dl, window(%rax)
	addl	$1, -20(%rbp)
	cmpl	$32768, -20(%rbp)
	jne	.L786
	movl	-20(%rbp), %eax
	movl	%eax, outcnt(%rip)
	call	flush_window
	movl	$0, -20(%rbp)
.L786:
	shrq	$8, %r12
	subl	$8, %ebx
.L781:
	cmpl	$0, -24(%rbp)
	setne	%al
	subl	$1, -24(%rbp)
	testb	%al, %al
	jne	.L782
	movl	-20(%rbp), %eax
	movl	%eax, outcnt(%rip)
	movq	%r12, bb(%rip)
	movl	%ebx, bk(%rip)
	movl	$0, %eax
.L780:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE53:
	.size	inflate_stored, .-inflate_stored
	.globl	inflate_fixed
	.type	inflate_fixed, @function
inflate_fixed:
.LFB54:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1200, %rsp
	movl	$0, -4(%rbp)
	jmp	.L789
.L790:
	movl	-4(%rbp), %eax
	cltq
	movl	$8, -1184(%rbp,%rax,4)
	addl	$1, -4(%rbp)
.L789:
	cmpl	$143, -4(%rbp)
	jle	.L790
	jmp	.L791
.L792:
	movl	-4(%rbp), %eax
	cltq
	movl	$9, -1184(%rbp,%rax,4)
	addl	$1, -4(%rbp)
.L791:
	cmpl	$255, -4(%rbp)
	jle	.L792
	jmp	.L793
.L794:
	movl	-4(%rbp), %eax
	cltq
	movl	$7, -1184(%rbp,%rax,4)
	addl	$1, -4(%rbp)
.L793:
	cmpl	$279, -4(%rbp)
	jle	.L794
	jmp	.L795
.L796:
	movl	-4(%rbp), %eax
	cltq
	movl	$8, -1184(%rbp,%rax,4)
	addl	$1, -4(%rbp)
.L795:
	cmpl	$287, -4(%rbp)
	jle	.L796
	movl	$7, -12(%rbp)
	leaq	-32(%rbp), %rcx
	leaq	-1184(%rbp), %rax
	leaq	-12(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	%rcx, %r9
	movl	$cplext, %r8d
	movl	$cplens, %ecx
	movl	$257, %edx
	movl	$288, %esi
	movq	%rax, %rdi
	call	huft_build
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L797
	movl	-4(%rbp), %eax
	jmp	.L798
.L797:
	movl	$0, -4(%rbp)
	jmp	.L799
.L800:
	movl	-4(%rbp), %eax
	cltq
	movl	$5, -1184(%rbp,%rax,4)
	addl	$1, -4(%rbp)
.L799:
	cmpl	$29, -4(%rbp)
	jle	.L800
	movl	$5, -8(%rbp)
	leaq	-24(%rbp), %rcx
	leaq	-1184(%rbp), %rax
	leaq	-8(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	%rcx, %r9
	movl	$cpdext, %r8d
	movl	$cpdist, %ecx
	movl	$0, %edx
	movl	$30, %esi
	movq	%rax, %rdi
	call	huft_build
	movl	%eax, -4(%rbp)
	cmpl	$1, -4(%rbp)
	jle	.L801
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movl	-4(%rbp), %eax
	jmp	.L798
.L801:
	movl	-8(%rbp), %ecx
	movl	-12(%rbp), %edx
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	inflate_codes
	testl	%eax, %eax
	je	.L802
	movl	$1, %eax
	jmp	.L798
.L802:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movl	$0, %eax
.L798:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE54:
	.size	inflate_fixed, .-inflate_fixed
	.section	.rodata
.LC128:
	.string	" incomplete literal tree\n"
.LC129:
	.string	" incomplete distance tree\n"
	.text
	.globl	inflate_dynamic
	.type	inflate_dynamic, @function
inflate_dynamic:
.LFB55:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$1344, %rsp
	movq	bb(%rip), %r12
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	movl	bk(%rip), %ebx
	jmp	.L804
.L807:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L805
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L806
.L805:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L806:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L804:
	cmpl	$4, %ebx
	jbe	.L807
	movl	%r12d, %eax
	andl	$31, %eax
	addl	$257, %eax
	movl	%eax, -36(%rbp)
	shrq	$5, %r12
	subl	$5, %ebx
	jmp	.L808
.L811:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L809
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L810
.L809:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L810:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L808:
	cmpl	$4, %ebx
	jbe	.L811
	movl	%r12d, %eax
	andl	$31, %eax
	addl	$1, %eax
	movl	%eax, -32(%rbp)
	shrq	$5, %r12
	subl	$5, %ebx
	jmp	.L812
.L815:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L813
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L814
.L813:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L814:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L812:
	cmpl	$3, %ebx
	jbe	.L815
	movl	%r12d, %eax
	andl	$15, %eax
	addl	$4, %eax
	movl	%eax, -28(%rbp)
	shrq	$4, %r12
	subl	$4, %ebx
	cmpl	$286, -36(%rbp)
	ja	.L816
	cmpl	$30, -32(%rbp)
	jbe	.L817
.L816:
	movl	$1, %eax
	jmp	.L818
.L817:
	movl	$0, -44(%rbp)
	jmp	.L819
.L823:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L821
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L822
.L821:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L822:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L820:
	cmpl	$2, %ebx
	jbe	.L823
	movl	-44(%rbp), %eax
	movl	border(,%rax,4), %eax
	movl	%r12d, %edx
	andl	$7, %edx
	movl	%eax, %eax
	movl	%edx, -1344(%rbp,%rax,4)
	shrq	$3, %r12
	subl	$3, %ebx
	addl	$1, -44(%rbp)
.L819:
	movl	-44(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jb	.L820
	jmp	.L825
.L826:
	movl	-44(%rbp), %eax
	movl	border(,%rax,4), %eax
	movl	%eax, %eax
	movl	$0, -1344(%rbp,%rax,4)
	addl	$1, -44(%rbp)
.L825:
	cmpl	$18, -44(%rbp)
	jbe	.L826
	movl	$7, -56(%rbp)
	leaq	-72(%rbp), %rcx
	leaq	-1344(%rbp), %rax
	leaq	-56(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	%rcx, %r9
	movl	$0, %r8d
	movl	$0, %ecx
	movl	$19, %edx
	movl	$19, %esi
	movq	%rax, %rdi
	call	huft_build
	movl	%eax, -48(%rbp)
	cmpl	$0, -48(%rbp)
	je	.L827
	cmpl	$1, -48(%rbp)
	jne	.L828
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
.L828:
	movl	-48(%rbp), %eax
	jmp	.L818
.L827:
	movl	-32(%rbp), %eax
	movl	-36(%rbp), %edx
	addl	%edx, %eax
	movl	%eax, -24(%rbp)
	movl	-56(%rbp), %eax
	cltq
	movzwl	mask_bits(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -20(%rbp)
	movl	$0, -40(%rbp)
	movl	$0, -48(%rbp)
	jmp	.L829
.L833:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L831
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L832
.L831:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L832:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L830:
	movl	-56(%rbp), %eax
	cmpl	%ebx, %eax
	ja	.L833
	movq	-72(%rbp), %rdx
	movl	%r12d, %eax
	andl	-20(%rbp), %eax
	movl	%eax, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movzbl	1(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -44(%rbp)
	movl	-44(%rbp), %eax
	movl	%eax, %ecx
	shrq	%cl, %r12
	subl	-44(%rbp), %ebx
	movq	-64(%rbp), %rax
	movzwl	8(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -44(%rbp)
	cmpl	$15, -44(%rbp)
	ja	.L834
	movl	-44(%rbp), %eax
	movl	%eax, -40(%rbp)
	movl	-48(%rbp), %eax
	cltq
	movl	-40(%rbp), %edx
	movl	%edx, -1344(%rbp,%rax,4)
	addl	$1, -48(%rbp)
	jmp	.L829
.L834:
	cmpl	$16, -44(%rbp)
	jne	.L835
	jmp	.L836
.L839:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L837
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L838
.L837:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L838:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L836:
	cmpl	$1, %ebx
	jbe	.L839
	movl	%r12d, %eax
	andl	$3, %eax
	addl	$3, %eax
	movl	%eax, -44(%rbp)
	shrq	$2, %r12
	subl	$2, %ebx
	movl	-48(%rbp), %eax
	addl	-44(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jbe	.L864
	movl	$1, %eax
	jmp	.L818
.L842:
	movl	-48(%rbp), %eax
	cltq
	movl	-40(%rbp), %edx
	movl	%edx, -1344(%rbp,%rax,4)
	addl	$1, -48(%rbp)
	jmp	.L841
.L864:
	nop
.L841:
	cmpl	$0, -44(%rbp)
	setne	%al
	subl	$1, -44(%rbp)
	testb	%al, %al
	jne	.L842
	jmp	.L829
.L835:
	cmpl	$17, -44(%rbp)
	jne	.L851
	jmp	.L844
.L847:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L845
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L846
.L845:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L846:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L844:
	cmpl	$2, %ebx
	jbe	.L847
	movl	%r12d, %eax
	andl	$7, %eax
	addl	$3, %eax
	movl	%eax, -44(%rbp)
	shrq	$3, %r12
	subl	$3, %ebx
	movl	-48(%rbp), %eax
	addl	-44(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jbe	.L865
	movl	$1, %eax
	jmp	.L818
.L850:
	movl	-48(%rbp), %eax
	cltq
	movl	$0, -1344(%rbp,%rax,4)
	addl	$1, -48(%rbp)
	jmp	.L849
.L865:
	nop
.L849:
	cmpl	$0, -44(%rbp)
	setne	%al
	subl	$1, -44(%rbp)
	testb	%al, %al
	jne	.L850
	movl	$0, -40(%rbp)
	jmp	.L829
.L854:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L852
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L853
.L852:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L853:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L851:
	cmpl	$6, %ebx
	jbe	.L854
	movl	%r12d, %eax
	andl	$127, %eax
	addl	$11, %eax
	movl	%eax, -44(%rbp)
	shrq	$7, %r12
	subl	$7, %ebx
	movl	-48(%rbp), %eax
	addl	-44(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jbe	.L866
	movl	$1, %eax
	jmp	.L818
.L857:
	movl	-48(%rbp), %eax
	cltq
	movl	$0, -1344(%rbp,%rax,4)
	addl	$1, -48(%rbp)
	jmp	.L856
.L866:
	nop
.L856:
	cmpl	$0, -44(%rbp)
	setne	%al
	subl	$1, -44(%rbp)
	testb	%al, %al
	jne	.L857
	movl	$0, -40(%rbp)
.L829:
	movl	-48(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jb	.L830
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movq	%r12, bb(%rip)
	movl	%ebx, bk(%rip)
	movl	lbits(%rip), %eax
	movl	%eax, -56(%rbp)
	leaq	-72(%rbp), %rcx
	movl	-36(%rbp), %esi
	leaq	-1344(%rbp), %rax
	leaq	-56(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	%rcx, %r9
	movl	$cplext, %r8d
	movl	$cplens, %ecx
	movl	$257, %edx
	movq	%rax, %rdi
	call	huft_build
	movl	%eax, -48(%rbp)
	cmpl	$0, -48(%rbp)
	je	.L859
	cmpl	$1, -48(%rbp)
	jne	.L860
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC128, %eax
	movq	%rdx, %rcx
	movl	$25, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
.L860:
	movl	-48(%rbp), %eax
	jmp	.L818
.L859:
	movl	dbits(%rip), %eax
	movl	%eax, -52(%rbp)
	movl	-36(%rbp), %eax
	leaq	0(,%rax,4), %rdx
	leaq	-1344(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	leaq	-64(%rbp), %rcx
	movl	-32(%rbp), %eax
	leaq	-52(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	%rcx, %r9
	movl	$cpdext, %r8d
	movl	$cpdist, %ecx
	movl	$0, %edx
	movl	%eax, %esi
	call	huft_build
	movl	%eax, -48(%rbp)
	cmpl	$0, -48(%rbp)
	je	.L861
	cmpl	$1, -48(%rbp)
	jne	.L862
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC129, %eax
	movq	%rdx, %rcx
	movl	$26, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
.L862:
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movl	-48(%rbp), %eax
	jmp	.L818
.L861:
	movl	-52(%rbp), %ecx
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	inflate_codes
	testl	%eax, %eax
	je	.L863
	movl	$1, %eax
	jmp	.L818
.L863:
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	huft_free
	movl	$0, %eax
.L818:
	addq	$1344, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE55:
	.size	inflate_dynamic, .-inflate_dynamic
	.globl	inflate_block
	.type	inflate_block, @function
inflate_block:
.LFB56:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	movq	%rdi, -40(%rbp)
	movq	bb(%rip), %r12
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	movl	bk(%rip), %ebx
	jmp	.L868
.L871:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L869
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L870
.L869:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L870:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L868:
	testl	%ebx, %ebx
	je	.L871
	movl	%r12d, %eax
	movl	%eax, %edx
	andl	$1, %edx
	movq	-40(%rbp), %rax
	movl	%edx, (%rax)
	shrq	%r12
	subl	$1, %ebx
	jmp	.L872
.L875:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L873
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L874
.L873:
	movl	$0, %edi
	call	fill_inbuf
	movzbl	%al, %eax
.L874:
	movl	%ebx, %edx
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %r12
	addl	$8, %ebx
.L872:
	cmpl	$1, %ebx
	jbe	.L875
	movl	%r12d, %eax
	andl	$3, %eax
	movl	%eax, -20(%rbp)
	shrq	$2, %r12
	subl	$2, %ebx
	movq	%r12, bb(%rip)
	movl	%ebx, bk(%rip)
	cmpl	$2, -20(%rbp)
	jne	.L876
	call	inflate_dynamic
	jmp	.L877
.L876:
	cmpl	$0, -20(%rbp)
	jne	.L878
	call	inflate_stored
	jmp	.L877
.L878:
	cmpl	$1, -20(%rbp)
	jne	.L879
	call	inflate_fixed
	jmp	.L877
.L879:
	movl	$2, %eax
.L877:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE56:
	.size	inflate_block, .-inflate_block
	.globl	inflate
	.type	inflate, @function
inflate:
.LFB57:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$0, outcnt(%rip)
	movl	$0, bk(%rip)
	movq	$0, bb(%rip)
	movl	$0, -8(%rbp)
.L884:
	movl	$0, hufts(%rip)
	leaq	-12(%rbp), %rax
	movq	%rax, %rdi
	call	inflate_block
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L881
	movl	-4(%rbp), %eax
	jmp	.L882
.L881:
	movl	hufts(%rip), %eax
	cmpl	-8(%rbp), %eax
	jbe	.L883
	movl	hufts(%rip), %eax
	movl	%eax, -8(%rbp)
.L883:
	movl	-12(%rbp), %eax
	testl	%eax, %eax
	je	.L884
	jmp	.L885
.L886:
	movl	bk(%rip), %eax
	subl	$8, %eax
	movl	%eax, bk(%rip)
	movl	inptr(%rip), %eax
	subl	$1, %eax
	movl	%eax, inptr(%rip)
.L885:
	movl	bk(%rip), %eax
	cmpl	$7, %eax
	ja	.L886
	movl	outcnt(%rip), %eax
	movl	%eax, outcnt(%rip)
	call	flush_window
	movl	$0, %eax
.L882:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE57:
	.size	inflate, .-inflate
	.globl	copy
	.type	copy, @function
copy:
.LFB58:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	call	__errno_location
	movl	$0, (%rax)
	jmp	.L888
.L890:
	movl	insize(%rip), %edx
	movl	-8(%rbp), %eax
	movl	$inbuf, %esi
	movl	%eax, %edi
	call	write_buf
	movl	insize(%rip), %eax
	movl	%eax, %edx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
	movl	-4(%rbp), %eax
	movl	$32768, %edx
	movl	$inbuf, %esi
	movl	%eax, %edi
	movl	$0, %eax
	call	read
	movl	%eax, insize(%rip)
.L888:
	movl	insize(%rip), %eax
	testl	%eax, %eax
	je	.L889
	movl	insize(%rip), %eax
	cmpl	$-1, %eax
	jne	.L890
.L889:
	movl	insize(%rip), %eax
	cmpl	$-1, %eax
	jne	.L891
	call	__errno_location
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L891
	call	read_error
.L891:
	movq	bytes_out(%rip), %rax
	movq	%rax, bytes_in(%rip)
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE58:
	.size	copy, .-copy
	.globl	updcrc
	.type	updcrc, @function
updcrc:
.LFB59:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L893
	.cfi_offset 3, -24
	movl	$4294967295, %ebx
	jmp	.L894
.L893:
	movq	crc.4663(%rip), %rbx
	cmpl	$0, -20(%rbp)
	je	.L894
.L895:
	movl	%ebx, %edx
	movq	-16(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	xorl	%edx, %eax
	andl	$255, %eax
	cltq
	movq	crc_32_tab(,%rax,8), %rax
	movq	%rbx, %rdx
	shrq	$8, %rdx
	movq	%rax, %rbx
	xorq	%rdx, %rbx
	addq	$1, -16(%rbp)
	subl	$1, -20(%rbp)
	cmpl	$0, -20(%rbp)
	jne	.L895
.L894:
	movq	%rbx, crc.4663(%rip)
	movl	$4294967295, %eax
	xorq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE59:
	.size	updcrc, .-updcrc
	.globl	clear_bufs
	.type	clear_bufs, @function
clear_bufs:
.LFB60:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, outcnt(%rip)
	movl	$0, inptr(%rip)
	movl	inptr(%rip), %eax
	movl	%eax, insize(%rip)
	movq	$0, bytes_out(%rip)
	movq	bytes_out(%rip), %rax
	movq	%rax, bytes_in(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE60:
	.size	clear_bufs, .-clear_bufs
	.globl	fill_inbuf
	.type	fill_inbuf, @function
fill_inbuf:
.LFB61:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	$0, insize(%rip)
	call	__errno_location
	movl	$0, (%rax)
.L899:
	movl	insize(%rip), %eax
	movl	$32768, %edx
	subl	%eax, %edx
	movl	insize(%rip), %eax
	movl	%eax, %eax
	leaq	inbuf(%rax), %rcx
	movl	ifd(%rip), %eax
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	read
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L898
	cmpl	$-1, -4(%rbp)
	je	.L898
	movl	insize(%rip), %edx
	movl	-4(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, insize(%rip)
	movl	insize(%rip), %eax
	cmpl	$32767, %eax
	jbe	.L899
.L898:
	movl	insize(%rip), %eax
	testl	%eax, %eax
	jne	.L900
	cmpl	$0, -20(%rbp)
	je	.L901
	movl	$-1, %eax
	jmp	.L902
.L901:
	call	read_error
.L900:
	movl	insize(%rip), %eax
	movl	%eax, %edx
	movq	bytes_in(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_in(%rip)
	movl	$1, inptr(%rip)
	movzbl	inbuf(%rip), %eax
	movzbl	%al, %eax
.L902:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE61:
	.size	fill_inbuf, .-fill_inbuf
	.globl	flush_outbuf
	.type	flush_outbuf, @function
flush_outbuf:
.LFB62:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	outcnt(%rip), %eax
	testl	%eax, %eax
	je	.L906
.L904:
	movl	outcnt(%rip), %edx
	movl	ofd(%rip), %eax
	movl	$outbuf, %esi
	movl	%eax, %edi
	call	write_buf
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
	movl	$0, outcnt(%rip)
	jmp	.L903
.L906:
	nop
.L903:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE62:
	.size	flush_outbuf, .-flush_outbuf
	.globl	flush_window
	.type	flush_window, @function
flush_window:
.LFB63:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	outcnt(%rip), %eax
	testl	%eax, %eax
	je	.L911
.L908:
	movl	outcnt(%rip), %eax
	movl	%eax, %esi
	movl	$window, %edi
	call	updcrc
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L910
	movl	outcnt(%rip), %edx
	movl	ofd(%rip), %eax
	movl	$window, %esi
	movl	%eax, %edi
	call	write_buf
.L910:
	movl	outcnt(%rip), %eax
	movl	%eax, %edx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
	movl	$0, outcnt(%rip)
	jmp	.L907
.L911:
	nop
.L907:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE63:
	.size	flush_window, .-flush_window
	.globl	write_buf
	.type	write_buf, @function
write_buf:
.LFB64:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -24(%rbp)
	jmp	.L913
.L915:
	cmpl	$-1, -4(%rbp)
	jne	.L914
	call	write_error
.L914:
	movl	-4(%rbp), %eax
	subl	%eax, -24(%rbp)
	movl	-4(%rbp), %eax
	addq	%rax, -32(%rbp)
.L913:
	movl	-24(%rbp), %edx
	movq	-32(%rbp), %rcx
	movl	-20(%rbp), %eax
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	write
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jne	.L915
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE64:
	.size	write_buf, .-write_buf
	.globl	strlwr
	.type	strlwr, @function
strlwr:
.LFB65:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	jmp	.L917
.L920:
	call	__ctype_b_loc
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
	movsbq	%al, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	andl	$256, %eax
	testl	%eax, %eax
	je	.L918
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
	addl	$32, %eax
	jmp	.L919
.L918:
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
.L919:
	movq	-8(%rbp), %rdx
	movb	%al, (%rdx)
	addq	$1, -8(%rbp)
.L917:
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L920
	movq	-24(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE65:
	.size	strlwr, .-strlwr
	.globl	basename
	.type	basename, @function
basename:
.LFB66:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	$47, %esi
	movq	%rax, %rdi
	call	strrchr
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	je	.L922
	movq	-8(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -24(%rbp)
.L922:
	movq	-24(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE66:
	.size	basename, .-basename
	.globl	make_simple_name
	.type	make_simple_name, @function
make_simple_name:
.LFB67:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movl	$46, %esi
	movq	%rax, %rdi
	call	strrchr
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	je	.L928
.L924:
	movq	-8(%rbp), %rax
	cmpq	-24(%rbp), %rax
	jne	.L926
	addq	$1, -8(%rbp)
.L926:
	subq	$1, -8(%rbp)
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	$46, %al
	jne	.L927
	movq	-8(%rbp), %rax
	movb	$95, (%rax)
.L927:
	movq	-8(%rbp), %rax
	cmpq	-24(%rbp), %rax
	jne	.L926
	jmp	.L923
.L928:
	nop
.L923:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE67:
	.size	make_simple_name, .-make_simple_name
	.section	.rodata
.LC130:
	.string	" \t"
.LC131:
	.string	"argc<=0"
	.text
	.globl	add_envopt
	.type	add_envopt, @function
add_envopt:
.LFB68:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	-40(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -8(%rbp)
	movl	$0, -4(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	getenv
	movq	%rax, -56(%rbp)
	cmpq	$0, -56(%rbp)
	jne	.L930
	movl	$0, %eax
	jmp	.L931
.L930:
	movq	-56(%rbp), %rax
	movq	$-1, -64(%rbp)
	movq	%rax, %rdx
	movl	$0, %eax
	movq	-64(%rbp), %rcx
	movq	%rdx, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	subq	$1, %rax
	addl	$1, %eax
	movl	%eax, %edi
	call	xmalloc
	movq	%rax, -32(%rbp)
	movq	-56(%rbp), %rdx
	movq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L932
.L936:
	movq	-32(%rbp), %rax
	movl	$.LC130, %esi
	movq	%rax, %rdi
	call	strspn
	addq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L945
.L933:
	movq	-32(%rbp), %rax
	movl	$.LC130, %esi
	movq	%rax, %rdi
	call	strcspn
	addq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L935
	movq	-32(%rbp), %rax
	movb	$0, (%rax)
	addq	$1, -32(%rbp)
.L935:
	addl	$1, -4(%rbp)
.L932:
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L936
	jmp	.L934
.L945:
	nop
.L934:
	cmpl	$0, -4(%rbp)
	jne	.L937
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	free
	movl	$0, %eax
	jmp	.L931
.L937:
	movq	-40(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	addl	-4(%rbp), %edx
	movq	-40(%rbp), %rax
	movl	%edx, (%rax)
	movq	-40(%rbp), %rax
	movl	(%rax), %eax
	addl	$1, %eax
	cltq
	movl	$8, %esi
	movq	%rax, %rdi
	call	calloc
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne	.L938
	movl	$.LC119, %edi
	call	error
.L938:
	movq	-48(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -24(%rbp)
	movq	-48(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	-8(%rbp), %eax
	shrl	$31, %eax
	subl	$1, -8(%rbp)
	testb	%al, %al
	je	.L939
	movl	$.LC131, %edi
	call	error
.L939:
	movq	-24(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, (%rax)
	addq	$8, -16(%rbp)
	addq	$8, -24(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -32(%rbp)
	jmp	.L940
.L942:
	movq	-32(%rbp), %rax
	movl	$.LC130, %esi
	movq	%rax, %rdi
	call	strspn
	addq	%rax, -32(%rbp)
	movq	-16(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, (%rax)
	addq	$8, -16(%rbp)
	nop
.L941:
	movq	-32(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	setne	%al
	addq	$1, -32(%rbp)
	testb	%al, %al
	jne	.L941
	subl	$1, -4(%rbp)
.L940:
	cmpl	$0, -4(%rbp)
	jg	.L942
	jmp	.L943
.L944:
	movq	-24(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, (%rax)
	addq	$8, -16(%rbp)
	addq	$8, -24(%rbp)
.L943:
	cmpl	$0, -8(%rbp)
	setne	%al
	subl	$1, -8(%rbp)
	testb	%al, %al
	jne	.L944
	movq	-16(%rbp), %rax
	movq	$0, (%rax)
	movq	-56(%rbp), %rax
.L931:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE68:
	.size	add_envopt, .-add_envopt
	.section	.rodata
.LC132:
	.string	"\n%s: %s: %s\n"
	.text
	.globl	error
	.type	error, @function
error:
.LFB69:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	progname(%rip), %rdx
	movl	$.LC132, %esi
	movq	stderr(%rip), %rax
	movq	-8(%rbp), %rcx
	movq	%rcx, %r8
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	abort_gzip
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE69:
	.size	error, .-error
	.section	.rodata
.LC133:
	.string	"%s: %s: warning: %s%s\n"
	.text
	.globl	warn
	.type	warn, @function
warn:
.LFB70:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L948
	movq	progname(%rip), %rdx
	movl	$.LC133, %esi
	movq	stderr(%rip), %rax
	movq	-16(%rbp), %rdi
	movq	-8(%rbp), %rcx
	movq	%rdi, %r9
	movq	%rcx, %r8
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L948:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L947
	movl	$2, exit_code(%rip)
.L947:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE70:
	.size	warn, .-warn
	.section	.rodata
.LC134:
	.string	"\n%s: "
.LC135:
	.string	"%s: unexpected end of file\n"
	.text
	.globl	read_error
	.type	read_error, @function
read_error:
.LFB71:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progname(%rip), %rdx
	movl	$.LC134, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	call	__errno_location
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L951
	movl	$ifname, %edi
	call	perror
	jmp	.L952
.L951:
	movl	$.LC135, %ecx
	movq	stderr(%rip), %rax
	movl	$ifname, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L952:
	call	abort_gzip
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE71:
	.size	read_error, .-read_error
	.globl	write_error
	.type	write_error, @function
write_error:
.LFB72:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	progname(%rip), %rdx
	movl	$.LC134, %ecx
	movq	stderr(%rip), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$ofname, %edi
	call	perror
	call	abort_gzip
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE72:
	.size	write_error, .-write_error
	.section	.rodata
.LC136:
	.string	"%2ld.%1ld%%"
	.text
	.globl	display_ratio
	.type	display_ratio, @function
display_ratio:
.LFB73:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	cmpq	$0, -32(%rbp)
	jne	.L955
	movq	$0, -8(%rbp)
	jmp	.L956
.L955:
	cmpq	$2147482, -32(%rbp)
	jg	.L957
	movq	-24(%rbp), %rax
	imulq	$1000, %rax, %rax
	movq	%rax, %rdx
	sarq	$63, %rdx
	idivq	-32(%rbp)
	movq	%rax, -8(%rbp)
	jmp	.L956
.L957:
	movq	-32(%rbp), %rcx
	movabsq	$2361183241434822607, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	sarq	$7, %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, -48(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdx
	sarq	$63, %rdx
	idivq	-48(%rbp)
	movq	%rax, -8(%rbp)
.L956:
	cmpq	$0, -8(%rbp)
	jns	.L958
	movq	-40(%rbp), %rax
	movq	%rax, %rsi
	movl	$45, %edi
	call	_IO_putc
	negq	-8(%rbp)
	jmp	.L959
.L958:
	movq	-40(%rbp), %rax
	movq	%rax, %rsi
	movl	$32, %edi
	call	_IO_putc
.L959:
	movq	-8(%rbp), %rsi
	movabsq	$7378697629483820647, %rdx
	movq	%rsi, %rax
	imulq	%rdx
	sarq	$2, %rdx
	movq	%rsi, %rax
	sarq	$63, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	salq	$2, %rax
	addq	%rcx, %rax
	addq	%rax, %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	-8(%rbp), %rsi
	movabsq	$7378697629483820647, %rdx
	movq	%rsi, %rax
	imulq	%rdx
	sarq	$2, %rdx
	movq	%rsi, %rax
	sarq	$63, %rax
	subq	%rax, %rdx
	movl	$.LC136, %esi
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE73:
	.size	display_ratio, .-display_ratio
	.globl	xmalloc
	.type	xmalloc, @function
xmalloc:
.LFB74:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	-20(%rbp), %eax
	movq	%rax, %rdi
	call	malloc
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L961
	movl	$.LC119, %edi
	call	error
.L961:
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE74:
	.size	xmalloc, .-xmalloc
	.globl	crc_32_tab
	.data
	.align 32
	.type	crc_32_tab, @object
	.size	crc_32_tab, 2048
crc_32_tab:
	.quad	0
	.quad	1996959894
	.quad	3993919788
	.quad	2567524794
	.quad	124634137
	.quad	1886057615
	.quad	3915621685
	.quad	2657392035
	.quad	249268274
	.quad	2044508324
	.quad	3772115230
	.quad	2547177864
	.quad	162941995
	.quad	2125561021
	.quad	3887607047
	.quad	2428444049
	.quad	498536548
	.quad	1789927666
	.quad	4089016648
	.quad	2227061214
	.quad	450548861
	.quad	1843258603
	.quad	4107580753
	.quad	2211677639
	.quad	325883990
	.quad	1684777152
	.quad	4251122042
	.quad	2321926636
	.quad	335633487
	.quad	1661365465
	.quad	4195302755
	.quad	2366115317
	.quad	997073096
	.quad	1281953886
	.quad	3579855332
	.quad	2724688242
	.quad	1006888145
	.quad	1258607687
	.quad	3524101629
	.quad	2768942443
	.quad	901097722
	.quad	1119000684
	.quad	3686517206
	.quad	2898065728
	.quad	853044451
	.quad	1172266101
	.quad	3705015759
	.quad	2882616665
	.quad	651767980
	.quad	1373503546
	.quad	3369554304
	.quad	3218104598
	.quad	565507253
	.quad	1454621731
	.quad	3485111705
	.quad	3099436303
	.quad	671266974
	.quad	1594198024
	.quad	3322730930
	.quad	2970347812
	.quad	795835527
	.quad	1483230225
	.quad	3244367275
	.quad	3060149565
	.quad	1994146192
	.quad	31158534
	.quad	2563907772
	.quad	4023717930
	.quad	1907459465
	.quad	112637215
	.quad	2680153253
	.quad	3904427059
	.quad	2013776290
	.quad	251722036
	.quad	2517215374
	.quad	3775830040
	.quad	2137656763
	.quad	141376813
	.quad	2439277719
	.quad	3865271297
	.quad	1802195444
	.quad	476864866
	.quad	2238001368
	.quad	4066508878
	.quad	1812370925
	.quad	453092731
	.quad	2181625025
	.quad	4111451223
	.quad	1706088902
	.quad	314042704
	.quad	2344532202
	.quad	4240017532
	.quad	1658658271
	.quad	366619977
	.quad	2362670323
	.quad	4224994405
	.quad	1303535960
	.quad	984961486
	.quad	2747007092
	.quad	3569037538
	.quad	1256170817
	.quad	1037604311
	.quad	2765210733
	.quad	3554079995
	.quad	1131014506
	.quad	879679996
	.quad	2909243462
	.quad	3663771856
	.quad	1141124467
	.quad	855842277
	.quad	2852801631
	.quad	3708648649
	.quad	1342533948
	.quad	654459306
	.quad	3188396048
	.quad	3373015174
	.quad	1466479909
	.quad	544179635
	.quad	3110523913
	.quad	3462522015
	.quad	1591671054
	.quad	702138776
	.quad	2966460450
	.quad	3352799412
	.quad	1504918807
	.quad	783551873
	.quad	3082640443
	.quad	3233442989
	.quad	3988292384
	.quad	2596254646
	.quad	62317068
	.quad	1957810842
	.quad	3939845945
	.quad	2647816111
	.quad	81470997
	.quad	1943803523
	.quad	3814918930
	.quad	2489596804
	.quad	225274430
	.quad	2053790376
	.quad	3826175755
	.quad	2466906013
	.quad	167816743
	.quad	2097651377
	.quad	4027552580
	.quad	2265490386
	.quad	503444072
	.quad	1762050814
	.quad	4150417245
	.quad	2154129355
	.quad	426522225
	.quad	1852507879
	.quad	4275313526
	.quad	2312317920
	.quad	282753626
	.quad	1742555852
	.quad	4189708143
	.quad	2394877945
	.quad	397917763
	.quad	1622183637
	.quad	3604390888
	.quad	2714866558
	.quad	953729732
	.quad	1340076626
	.quad	3518719985
	.quad	2797360999
	.quad	1068828381
	.quad	1219638859
	.quad	3624741850
	.quad	2936675148
	.quad	906185462
	.quad	1090812512
	.quad	3747672003
	.quad	2825379669
	.quad	829329135
	.quad	1181335161
	.quad	3412177804
	.quad	3160834842
	.quad	628085408
	.quad	1382605366
	.quad	3423369109
	.quad	3138078467
	.quad	570562233
	.quad	1426400815
	.quad	3317316542
	.quad	2998733608
	.quad	733239954
	.quad	1555261956
	.quad	3268935591
	.quad	3050360625
	.quad	752459403
	.quad	1541320221
	.quad	2607071920
	.quad	3965973030
	.quad	1969922972
	.quad	40735498
	.quad	2617837225
	.quad	3943577151
	.quad	1913087877
	.quad	83908371
	.quad	2512341634
	.quad	3803740692
	.quad	2075208622
	.quad	213261112
	.quad	2463272603
	.quad	3855990285
	.quad	2094854071
	.quad	198958881
	.quad	2262029012
	.quad	4057260610
	.quad	1759359992
	.quad	534414190
	.quad	2176718541
	.quad	4139329115
	.quad	1873836001
	.quad	414664567
	.quad	2282248934
	.quad	4279200368
	.quad	1711684554
	.quad	285281116
	.quad	2405801727
	.quad	4167216745
	.quad	1634467795
	.quad	376229701
	.quad	2685067896
	.quad	3608007406
	.quad	1308918612
	.quad	956543938
	.quad	2808555105
	.quad	3495958263
	.quad	1231636301
	.quad	1047427035
	.quad	2932959818
	.quad	3654703836
	.quad	1088359270
	.quad	936918000
	.quad	2847714899
	.quad	3736837829
	.quad	1202900863
	.quad	817233897
	.quad	3183342108
	.quad	3401237130
	.quad	1404277552
	.quad	615818150
	.quad	3134207493
	.quad	3453421203
	.quad	1423857449
	.quad	601450431
	.quad	3009837614
	.quad	3294710456
	.quad	1567103746
	.quad	711928724
	.quad	3020668471
	.quad	3272380065
	.quad	1510334235
	.quad	755167117
	.local	msg_done
	.comm	msg_done,4,4
	.section	.rodata
	.align 8
.LC137:
	.string	"output in compress .Z format not supported\n"
	.text
	.globl	lzw
	.type	lzw, @function
lzw:
.LFB75:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	msg_done(%rip), %eax
	testl	%eax, %eax
	je	.L963
	movl	$1, %eax
	jmp	.L964
.L963:
	movl	$1, msg_done(%rip)
	movq	stderr(%rip), %rax
	movq	%rax, %rdx
	movl	$.LC137, %eax
	movq	%rdx, %rcx
	movl	$43, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	je	.L965
	movl	$1, exit_code(%rip)
.L965:
	movl	$1, %eax
.L964:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE75:
	.size	lzw, .-lzw
	.globl	block_mode
	.data
	.align 4
	.type	block_mode, @object
	.size	block_mode, 4
block_mode:
	.long	128
	.section	.rodata
	.align 8
.LC138:
	.string	"\n%s: %s: warning, unknown flags 0x%x\n"
	.align 8
.LC139:
	.string	"\n%s: %s: compressed with %d bits, can only handle %d bits\n"
.LC140:
	.string	"corrupt input."
	.align 8
.LC141:
	.string	"corrupt input. Use zcat to recover some data."
	.text
	.globl	unlzw
	.type	unlzw, @function
unlzw:
.LFB76:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	movl	%edi, -116(%rbp)
	movl	%esi, -120(%rbp)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L967
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L968
.L967:
	movl	$0, %edi
	call	fill_inbuf
.L968:
	movl	%eax, maxbits(%rip)
	movl	maxbits(%rip), %eax
	andl	$128, %eax
	movl	%eax, block_mode(%rip)
	movl	maxbits(%rip), %eax
	andl	$96, %eax
	testl	%eax, %eax
	je	.L969
	movl	quiet(%rip), %eax
	testl	%eax, %eax
	jne	.L970
	movl	maxbits(%rip), %eax
	movl	%eax, %ecx
	andl	$96, %ecx
	movq	progname(%rip), %rdx
	movl	$.LC138, %esi
	movq	stderr(%rip), %rax
	movl	%ecx, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L970:
	movl	exit_code(%rip), %eax
	testl	%eax, %eax
	jne	.L969
	movl	$2, exit_code(%rip)
.L969:
	movl	maxbits(%rip), %eax
	andl	$31, %eax
	movl	%eax, maxbits(%rip)
	movl	maxbits(%rip), %eax
	movl	$1, %edx
	movq	%rdx, %rbx
	movl	%eax, %ecx
	salq	%cl, %rbx
	movq	%rbx, %rax
	movq	%rax, -72(%rbp)
	movl	maxbits(%rip), %eax
	cmpl	$16, %eax
	jle	.L971
	movl	maxbits(%rip), %ecx
	movq	progname(%rip), %rdx
	movl	$.LC139, %esi
	movq	stderr(%rip), %rax
	movl	$16, %r9d
	movl	%ecx, %r8d
	movl	$ifname, %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, exit_code(%rip)
	movl	$1, %eax
	jmp	.L972
.L971:
	movl	insize(%rip), %eax
	movl	%eax, -28(%rbp)
	movl	$9, -32(%rbp)
	movl	-32(%rbp), %eax
	movl	$1, %edx
	movq	%rdx, %rbx
	movl	%eax, %ecx
	salq	%cl, %rbx
	movq	%rbx, %rax
	subq	$1, %rax
	movq	%rax, -80(%rbp)
	movl	-32(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	movl	%eax, -36(%rbp)
	movq	$-1, -104(%rbp)
	movl	$0, -44(%rbp)
	movl	$0, -40(%rbp)
	movl	inptr(%rip), %eax
	sall	$3, %eax
	movl	%eax, %eax
	movq	%rax, -96(%rbp)
	movl	block_mode(%rip), %eax
	testl	%eax, %eax
	je	.L973
	movl	$257, %eax
	jmp	.L974
.L973:
	movl	$256, %eax
.L974:
	movq	%rax, -88(%rbp)
	movl	$prev, %esi
	movl	$0, %eax
	movl	$32, %edx
	movq	%rsi, %rdi
	movq	%rdx, %rcx
	rep stosq
	movq	$255, -112(%rbp)
	jmp	.L975
.L976:
	movq	-112(%rbp), %rax
	movq	-112(%rbp), %rdx
	addq	$window, %rdx
	movb	%al, (%rdx)
	subq	$1, -112(%rbp)
.L975:
	cmpq	$0, -112(%rbp)
	jns	.L976
.L977:
	movl	insize(%rip), %edx
	movq	-96(%rbp), %rax
	sarq	$3, %rax
	movl	%eax, -24(%rbp)
	movl	-24(%rbp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -20(%rbp)
	movl	$0, %ebx
	jmp	.L978
.L979:
	movl	%ebx, %eax
	addl	-24(%rbp), %eax
	cltq
	movzbl	inbuf(%rax), %edx
	movslq	%ebx, %rax
	movb	%dl, inbuf(%rax)
	addl	$1, %ebx
.L978:
	cmpl	-20(%rbp), %ebx
	jl	.L979
	movl	-20(%rbp), %eax
	movl	%eax, insize(%rip)
	movq	$0, -96(%rbp)
	movl	insize(%rip), %eax
	cmpl	$63, %eax
	ja	.L980
	movl	insize(%rip), %eax
	movl	%eax, %eax
	leaq	inbuf(%rax), %rcx
	movl	-116(%rbp), %eax
	movl	$32768, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	read
	movl	%eax, -28(%rbp)
	cmpl	$-1, -28(%rbp)
	jne	.L981
	call	read_error
.L981:
	movl	insize(%rip), %edx
	movl	-28(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, insize(%rip)
	movl	-28(%rbp), %eax
	movslq	%eax, %rdx
	movq	bytes_in(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_in(%rip)
.L980:
	cmpl	$0, -28(%rbp)
	je	.L982
	movl	insize(%rip), %eax
	movl	%eax, %ecx
	movl	insize(%rip), %eax
	movl	-32(%rbp), %esi
	movl	$0, %edx
	divl	%esi
	movl	%edx, %eax
	movl	%eax, %eax
	movq	%rcx, %rbx
	subq	%rax, %rbx
	movq	%rbx, %rax
	salq	$3, %rax
	jmp	.L983
.L982:
	movl	insize(%rip), %eax
	movl	%eax, %eax
	leaq	0(,%rax,8), %rdx
	movl	-32(%rbp), %eax
	subl	$1, %eax
	cltq
	movq	%rdx, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
.L983:
	movq	%rax, -64(%rbp)
	jmp	.L984
.L1006:
	movq	-88(%rbp), %rax
	cmpq	-80(%rbp), %rax
	jle	.L985
	movq	-96(%rbp), %rax
	leaq	-1(%rax), %rdi
	movl	-32(%rbp), %eax
	sall	$3, %eax
	movslq	%eax, %rcx
	movq	-96(%rbp), %rax
	leaq	-1(%rax), %rdx
	movl	-32(%rbp), %eax
	sall	$3, %eax
	cltq
	addq	%rdx, %rax
	movl	-32(%rbp), %edx
	sall	$3, %edx
	movslq	%edx, %rsi
	movq	%rax, %rdx
	sarq	$63, %rdx
	idivq	%rsi
	movq	%rdx, %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%rdi, %rax
	movq	%rax, -96(%rbp)
	addl	$1, -32(%rbp)
	movl	maxbits(%rip), %eax
	cmpl	%eax, -32(%rbp)
	jne	.L986
	movq	-72(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L987
.L986:
	movl	-32(%rbp), %eax
	movl	$1, %edx
	movq	%rdx, %rbx
	movl	%eax, %ecx
	salq	%cl, %rbx
	movq	%rbx, %rax
	subq	$1, %rax
	movq	%rax, -80(%rbp)
.L987:
	movl	-32(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	.L977
.L985:
	movq	-96(%rbp), %rax
	sarq	$3, %rax
	leaq	inbuf(%rax), %rbx
	movzbl	(%rbx), %eax
	movzbl	%al, %edx
	leaq	1(%rbx), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	salq	$8, %rax
	orq	%rax, %rdx
	leaq	2(%rbx), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	salq	$16, %rax
	orq	%rax, %rdx
	movq	-96(%rbp), %rax
	andl	$7, %eax
	movl	%eax, %ecx
	sarq	%cl, %rdx
	movl	-36(%rbp), %eax
	andq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movl	-32(%rbp), %eax
	cltq
	addq	%rax, -96(%rbp)
	cmpq	$-1, -104(%rbp)
	jne	.L988
	cmpq	$255, -112(%rbp)
	jle	.L989
	movl	$.LC140, %edi
	call	error
.L989:
	movq	-112(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	movl	%eax, -44(%rbp)
	movl	-44(%rbp), %eax
	movl	%eax, %edx
	movl	-40(%rbp), %eax
	cltq
	movb	%dl, outbuf(%rax)
	addl	$1, -40(%rbp)
	jmp	.L984
.L988:
	cmpq	$256, -112(%rbp)
	jne	.L990
	movl	block_mode(%rip), %eax
	testl	%eax, %eax
	je	.L990
	movl	$prev, %esi
	movl	$0, %eax
	movl	$32, %edx
	movq	%rsi, %rdi
	movq	%rdx, %rcx
	rep stosq
	movq	$256, -88(%rbp)
	movq	-96(%rbp), %rax
	leaq	-1(%rax), %rdi
	movl	-32(%rbp), %eax
	sall	$3, %eax
	movslq	%eax, %rcx
	movq	-96(%rbp), %rax
	leaq	-1(%rax), %rdx
	movl	-32(%rbp), %eax
	sall	$3, %eax
	cltq
	addq	%rdx, %rax
	movl	-32(%rbp), %edx
	sall	$3, %edx
	movslq	%edx, %rsi
	movq	%rax, %rdx
	sarq	$63, %rdx
	idivq	%rsi
	movq	%rdx, %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%rdi, %rax
	movq	%rax, -96(%rbp)
	movl	$9, -32(%rbp)
	movl	-32(%rbp), %eax
	movl	$1, %edx
	movq	%rdx, %rbx
	movl	%eax, %ecx
	salq	%cl, %rbx
	movq	%rbx, %rax
	subq	$1, %rax
	movq	%rax, -80(%rbp)
	movl	-32(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	.L977
.L990:
	movq	-112(%rbp), %rax
	movq	%rax, -56(%rbp)
	movl	$d_buf+65534, %ebx
	movq	-112(%rbp), %rax
	cmpq	-88(%rbp), %rax
	jl	.L1008
	movq	-112(%rbp), %rax
	cmpq	-88(%rbp), %rax
	jle	.L992
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L993
	cmpl	$0, -40(%rbp)
	jle	.L993
	movl	-40(%rbp), %edx
	movl	-120(%rbp), %eax
	movl	$outbuf, %esi
	movl	%eax, %edi
	call	write_buf
	movl	-40(%rbp), %eax
	movslq	%eax, %rdx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
.L993:
	movl	to_stdout(%rip), %eax
	testl	%eax, %eax
	je	.L994
	movl	$.LC140, %eax
	jmp	.L995
.L994:
	movl	$.LC141, %eax
.L995:
	movq	%rax, %rdi
	call	error
.L992:
	subq	$1, %rbx
	movl	-44(%rbp), %eax
	movb	%al, (%rbx)
	movq	-104(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L1008
.L997:
	subq	$1, %rbx
	movq	-112(%rbp), %rax
	addq	$window, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rbx)
	movq	-112(%rbp), %rax
	movzwl	prev(%rax,%rax), %eax
	movzwl	%ax, %eax
	movq	%rax, -112(%rbp)
	jmp	.L996
.L1008:
	nop
.L996:
	movq	-112(%rbp), %rax
	cmpq	$255, %rax
	ja	.L997
	subq	$1, %rbx
	movq	-112(%rbp), %rax
	addq	$window, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -44(%rbp)
	movl	-44(%rbp), %eax
	movb	%al, (%rbx)
	movl	$d_buf+65534, %edx
	movq	%rbx, %rax
	movq	%rdx, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
	movl	%eax, %r12d
	movl	%r12d, %eax
	addl	-40(%rbp), %eax
	cmpl	$16383, %eax
	jle	.L998
.L1003:
	movl	$16384, %eax
	subl	-40(%rbp), %eax
	cmpl	%r12d, %eax
	jge	.L999
	movl	$16384, %eax
	movl	%eax, %r12d
	subl	-40(%rbp), %r12d
.L999:
	testl	%r12d, %r12d
	jle	.L1000
	movslq	%r12d, %rdx
	movq	%rbx, %rcx
	movl	$outbuf, %esi
	movl	-40(%rbp), %eax
	cltq
	addq	%rsi, %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	memcpy
	addl	%r12d, -40(%rbp)
.L1000:
	cmpl	$16383, -40(%rbp)
	jle	.L1001
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L1002
	movl	-40(%rbp), %edx
	movl	-120(%rbp), %eax
	movl	$outbuf, %esi
	movl	%eax, %edi
	call	write_buf
	movl	-40(%rbp), %eax
	movslq	%eax, %rdx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
.L1002:
	movl	$0, -40(%rbp)
.L1001:
	movslq	%r12d, %rax
	addq	%rax, %rbx
	movl	$d_buf+65534, %edx
	movq	%rbx, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jg	.L1003
	jmp	.L1004
.L998:
	movslq	%r12d, %rdx
	movq	%rbx, %rcx
	movl	$outbuf, %esi
	movl	-40(%rbp), %eax
	cltq
	addq	%rsi, %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	memcpy
	addl	%r12d, -40(%rbp)
.L1004:
	movq	-88(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	-72(%rbp), %rax
	jge	.L1005
	movq	-104(%rbp), %rax
	movl	%eax, %edx
	movq	-112(%rbp), %rax
	movw	%dx, prev(%rax,%rax)
	movl	-44(%rbp), %eax
	movq	-112(%rbp), %rdx
	addq	$window, %rdx
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -88(%rbp)
.L1005:
	movq	-56(%rbp), %rax
	movq	%rax, -104(%rbp)
.L984:
	movq	-64(%rbp), %rax
	cmpq	-96(%rbp), %rax
	jg	.L1006
	cmpl	$0, -28(%rbp)
	jne	.L977
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L1007
	cmpl	$0, -40(%rbp)
	jle	.L1007
	movl	-40(%rbp), %edx
	movl	-120(%rbp), %eax
	movl	$outbuf, %esi
	movl	%eax, %edi
	call	write_buf
	movl	-40(%rbp), %eax
	movslq	%eax, %rdx
	movq	bytes_out(%rip), %rax
	addq	%rdx, %rax
	movq	%rax, bytes_out(%rip)
.L1007:
	movl	$0, %eax
.L972:
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE76:
	.size	unlzw, .-unlzw
	.local	orig_len
	.comm	orig_len,8,8
	.local	max_len
	.comm	max_len,4,4
	.local	literal
	.comm	literal,256,32
	.local	lit_base
	.comm	lit_base,104,32
	.local	leaves
	.comm	leaves,104,32
	.local	parents
	.comm	parents,104,32
	.local	peek_bits
	.comm	peek_bits,4,4
	.local	bitbuf
	.comm	bitbuf,8,8
	.local	valid
	.comm	valid,4,4
	.section	.rodata
	.align 8
.LC142:
	.string	"invalid compressed data -- Huffman code > 32 bits"
	.align 8
.LC143:
	.string	"too many leaves in Huffman tree"
	.text
	.type	read_tree, @function
read_tree:
.LFB77:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movq	$0, orig_len(%rip)
	movl	$1, -20(%rbp)
	jmp	.L1010
	.cfi_offset 3, -24
.L1013:
	movq	orig_len(%rip), %rax
	movq	%rax, %rbx
	salq	$8, %rbx
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1011
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1012
.L1011:
	movl	$0, %edi
	call	fill_inbuf
	cltq
.L1012:
	orq	%rbx, %rax
	movq	%rax, orig_len(%rip)
	addl	$1, -20(%rbp)
.L1010:
	cmpl	$4, -20(%rbp)
	jle	.L1013
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1014
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1015
.L1014:
	movl	$0, %edi
	call	fill_inbuf
.L1015:
	movl	%eax, max_len(%rip)
	movl	max_len(%rip), %eax
	cmpl	$25, %eax
	jle	.L1016
	movl	$.LC142, %edi
	call	error
.L1016:
	movl	$0, -20(%rbp)
	movl	$1, -28(%rbp)
	jmp	.L1017
.L1020:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1018
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1019
.L1018:
	movl	$0, %edi
	call	fill_inbuf
.L1019:
	movl	-28(%rbp), %edx
	movslq	%edx, %rdx
	movl	%eax, leaves(,%rdx,4)
	movl	-28(%rbp), %eax
	cltq
	movl	leaves(,%rax,4), %eax
	addl	%eax, -20(%rbp)
	addl	$1, -28(%rbp)
.L1017:
	movl	max_len(%rip), %eax
	cmpl	%eax, -28(%rbp)
	jle	.L1020
	cmpl	$256, -20(%rbp)
	jle	.L1021
	movl	$.LC143, %edi
	call	error
.L1021:
	movl	max_len(%rip), %eax
	movslq	%eax, %rdx
	movl	leaves(,%rdx,4), %edx
	addl	$1, %edx
	cltq
	movl	%edx, leaves(,%rax,4)
	movl	$0, -24(%rbp)
	movl	$1, -28(%rbp)
	jmp	.L1022
.L1027:
	movl	-28(%rbp), %eax
	cltq
	movl	-24(%rbp), %edx
	movl	%edx, lit_base(,%rax,4)
	movl	-28(%rbp), %eax
	cltq
	movl	leaves(,%rax,4), %eax
	movl	%eax, -20(%rbp)
	jmp	.L1023
.L1026:
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1024
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1025
.L1024:
	movl	$0, %edi
	call	fill_inbuf
.L1025:
	movl	-24(%rbp), %edx
	movslq	%edx, %rdx
	movb	%al, literal(%rdx)
	addl	$1, -24(%rbp)
	subl	$1, -20(%rbp)
.L1023:
	cmpl	$0, -20(%rbp)
	jg	.L1026
	addl	$1, -28(%rbp)
.L1022:
	movl	max_len(%rip), %eax
	cmpl	%eax, -28(%rbp)
	jle	.L1027
	movl	max_len(%rip), %eax
	movslq	%eax, %rdx
	movl	leaves(,%rdx,4), %edx
	addl	$1, %edx
	cltq
	movl	%edx, leaves(,%rax,4)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE77:
	.size	read_tree, .-read_tree
	.type	build_tree2, @function
build_tree2:
.LFB78:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	movl	$0, -20(%rbp)
	movl	max_len(%rip), %eax
	movl	%eax, -16(%rbp)
	jmp	.L1029
	.cfi_offset 3, -24
.L1030:
	sarl	-20(%rbp)
	movl	-16(%rbp), %eax
	cltq
	movl	-20(%rbp), %edx
	movl	%edx, parents(,%rax,4)
	movl	-16(%rbp), %eax
	cltq
	movl	lit_base(,%rax,4), %eax
	movl	%eax, %edx
	subl	-20(%rbp), %edx
	movl	-16(%rbp), %eax
	cltq
	movl	%edx, lit_base(,%rax,4)
	movl	-16(%rbp), %eax
	cltq
	movl	leaves(,%rax,4), %eax
	addl	%eax, -20(%rbp)
	subl	$1, -16(%rbp)
.L1029:
	cmpl	$0, -16(%rbp)
	jg	.L1030
	movl	max_len(%rip), %eax
	movl	$12, %edx
	cmpl	$12, %eax
	cmovg	%edx, %eax
	movl	%eax, peek_bits(%rip)
	movl	peek_bits(%rip), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	cltq
	addq	$outbuf, %rax
	movq	%rax, -32(%rbp)
	movl	$1, -16(%rbp)
	jmp	.L1031
.L1034:
	movl	-16(%rbp), %eax
	cltq
	movl	leaves(,%rax,4), %edx
	movl	peek_bits(%rip), %eax
	subl	-16(%rbp), %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, -12(%rbp)
	jmp	.L1032
.L1033:
	subq	$1, -32(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, %edx
	movq	-32(%rbp), %rax
	movb	%dl, (%rax)
.L1032:
	cmpl	$0, -12(%rbp)
	setne	%al
	subl	$1, -12(%rbp)
	testb	%al, %al
	jne	.L1033
	addl	$1, -16(%rbp)
.L1031:
	movl	peek_bits(%rip), %eax
	cmpl	%eax, -16(%rbp)
	jle	.L1034
	jmp	.L1035
.L1036:
	subq	$1, -32(%rbp)
	movq	-32(%rbp), %rax
	movb	$0, (%rax)
.L1035:
	cmpq	$outbuf, -32(%rbp)
	ja	.L1036
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE78:
	.size	build_tree2, .-build_tree2
	.globl	unpack
	.type	unpack, @function
unpack:
.LFB79:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	movl	%edi, -52(%rbp)
	movl	%esi, -56(%rbp)
	movl	-52(%rbp), %eax
	movl	%eax, ifd(%rip)
	movl	-56(%rbp), %eax
	movl	%eax, ofd(%rip)
	.cfi_offset 3, -24
	call	read_tree
	call	build_tree2
	movl	$0, valid(%rip)
	movq	$0, bitbuf(%rip)
	movl	peek_bits(%rip), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	subl	$1, %eax
	movl	%eax, -24(%rbp)
	movl	max_len(%rip), %eax
	cltq
	movl	leaves(,%rax,4), %eax
	subl	$1, %eax
	movl	%eax, -20(%rbp)
	jmp	.L1038
.L1041:
	movq	bitbuf(%rip), %rax
	movq	%rax, %rbx
	salq	$8, %rbx
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1039
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1040
.L1039:
	movl	$0, %edi
	call	fill_inbuf
	cltq
.L1040:
	orq	%rbx, %rax
	movq	%rax, bitbuf(%rip)
	movl	valid(%rip), %eax
	addl	$8, %eax
	movl	%eax, valid(%rip)
.L1038:
	movl	valid(%rip), %edx
	movl	peek_bits(%rip), %eax
	cmpl	%eax, %edx
	jl	.L1041
	movq	bitbuf(%rip), %rdx
	movl	valid(%rip), %ecx
	movl	peek_bits(%rip), %eax
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	movq	%rdx, %rbx
	movl	%eax, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rax
	movl	%eax, %ebx
	andl	-24(%rbp), %ebx
	movl	%ebx, %eax
	movzbl	outbuf(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -28(%rbp)
	cmpl	$0, -28(%rbp)
	jle	.L1042
	movl	peek_bits(%rip), %eax
	subl	-28(%rbp), %eax
	movl	%eax, %ecx
	shrl	%cl, %ebx
	jmp	.L1043
.L1042:
	movl	-24(%rbp), %ebx
	movq	%rbx, -40(%rbp)
	movl	peek_bits(%rip), %eax
	movl	%eax, -28(%rbp)
.L1048:
	addl	$1, -28(%rbp)
	movq	-40(%rbp), %rax
	addq	%rax, %rax
	addq	$1, %rax
	movq	%rax, -40(%rbp)
	jmp	.L1044
.L1047:
	movq	bitbuf(%rip), %rax
	movq	%rax, %rbx
	salq	$8, %rbx
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1045
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1046
.L1045:
	movl	$0, %edi
	call	fill_inbuf
	cltq
.L1046:
	orq	%rbx, %rax
	movq	%rax, bitbuf(%rip)
	movl	valid(%rip), %eax
	addl	$8, %eax
	movl	%eax, valid(%rip)
.L1044:
	movl	valid(%rip), %eax
	cmpl	-28(%rbp), %eax
	jl	.L1047
	movq	bitbuf(%rip), %rdx
	movl	valid(%rip), %eax
	subl	-28(%rbp), %eax
	movq	%rdx, %rbx
	movl	%eax, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rax
	movl	%eax, %edx
	movq	-40(%rbp), %rax
	movl	%edx, %ebx
	andl	%eax, %ebx
	movl	-28(%rbp), %eax
	cltq
	movl	parents(,%rax,4), %eax
	cmpl	%ebx, %eax
	ja	.L1048
.L1043:
	cmpl	-20(%rbp), %ebx
	jne	.L1049
	movl	max_len(%rip), %eax
	cmpl	%eax, -28(%rbp)
	je	.L1055
.L1049:
	movl	outcnt(%rip), %eax
	movl	-28(%rbp), %edx
	movslq	%edx, %rdx
	movl	lit_base(,%rdx,4), %edx
	addl	%ebx, %edx
	movl	%edx, %edx
	movzbl	literal(%rdx), %ecx
	movl	%eax, %edx
	movb	%cl, window(%rdx)
	addl	$1, %eax
	movl	%eax, outcnt(%rip)
	movl	outcnt(%rip), %eax
	cmpl	$32768, %eax
	jne	.L1051
	call	flush_window
.L1051:
	movl	valid(%rip), %eax
	subl	-28(%rbp), %eax
	movl	%eax, valid(%rip)
	nop
	jmp	.L1038
.L1055:
	nop
.L1054:
	call	flush_window
	movq	bytes_out(%rip), %rax
	movq	%rax, %rdx
	movq	orig_len(%rip), %rax
	cmpq	%rax, %rdx
	je	.L1053
	movl	$.LC125, %edi
	call	error
.L1053:
	movl	$0, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE79:
	.size	unpack, .-unpack
	.local	pt_len
	.comm	pt_len,19,16
	.local	blocksize
	.comm	blocksize,4,4
	.local	pt_table
	.comm	pt_table,512,32
	.local	subbitbuf
	.comm	subbitbuf,4,4
	.local	bitcount
	.comm	bitcount,4,4
	.type	fillbuf, @function
fillbuf:
.LFB80:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movl	%edi, -20(%rbp)
	movq	bitbuf(%rip), %rdx
	movl	-20(%rbp), %eax
	movq	%rdx, %rbx
	.cfi_offset 3, -24
	movl	%eax, %ecx
	salq	%cl, %rbx
	movq	%rbx, %rax
	movq	%rax, bitbuf(%rip)
	jmp	.L1057
.L1061:
	movl	subbitbuf(%rip), %edx
	movl	bitcount(%rip), %eax
	subl	%eax, -20(%rbp)
	movl	-20(%rbp), %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movq	bitbuf(%rip), %rax
	orq	%rdx, %rax
	movq	%rax, bitbuf(%rip)
	movl	inptr(%rip), %edx
	movl	insize(%rip), %eax
	cmpl	%eax, %edx
	jae	.L1058
	movl	inptr(%rip), %edx
	movl	%edx, %eax
	movzbl	inbuf(%rax), %eax
	movzbl	%al, %eax
	addl	$1, %edx
	movl	%edx, inptr(%rip)
	jmp	.L1059
.L1058:
	movl	$1, %edi
	call	fill_inbuf
.L1059:
	movl	%eax, subbitbuf(%rip)
	movl	subbitbuf(%rip), %eax
	cmpl	$-1, %eax
	jne	.L1060
	movl	$0, subbitbuf(%rip)
.L1060:
	movl	$8, bitcount(%rip)
.L1057:
	movl	bitcount(%rip), %eax
	cmpl	%eax, -20(%rbp)
	jg	.L1061
	movl	subbitbuf(%rip), %edx
	movl	bitcount(%rip), %eax
	subl	-20(%rbp), %eax
	movl	%eax, bitcount(%rip)
	movl	bitcount(%rip), %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movq	bitbuf(%rip), %rax
	orq	%rdx, %rax
	movq	%rax, bitbuf(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE80:
	.size	fillbuf, .-fillbuf
	.type	getbits, @function
getbits:
.LFB81:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	movl	%edi, -36(%rbp)
	movq	bitbuf(%rip), %rdx
	movl	-36(%rbp), %eax
	movl	$16, %ecx
	movl	%ecx, %ebx
	.cfi_offset 3, -24
	subl	%eax, %ebx
	movl	%ebx, %eax
	movq	%rdx, %rbx
	movl	%eax, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rax
	movl	%eax, -20(%rbp)
	movl	-36(%rbp), %eax
	movl	%eax, %edi
	call	fillbuf
	movl	-20(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE81:
	.size	getbits, .-getbits
	.type	init_getbits, @function
init_getbits:
.LFB82:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	$0, bitbuf(%rip)
	movl	$0, subbitbuf(%rip)
	movl	$0, bitcount(%rip)
	movl	$16, %edi
	call	fillbuf
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE82:
	.size	init_getbits, .-init_getbits
	.section	.rodata
.LC144:
	.string	"Bad table\n"
	.text
	.type	make_table, @function
make_table:
.LFB83:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$216, %rsp
	movl	%edi, -196(%rbp)
	movq	%rsi, -208(%rbp)
	movl	%edx, -200(%rbp)
	movq	%rcx, -216(%rbp)
	movl	$1, -48(%rbp)
	jmp	.L1065
	.cfi_offset 3, -24
.L1066:
	movl	-48(%rbp), %eax
	movw	$0, -144(%rbp,%rax,2)
	addl	$1, -48(%rbp)
.L1065:
	cmpl	$16, -48(%rbp)
	jbe	.L1066
	movl	$0, -48(%rbp)
	jmp	.L1067
.L1068:
	movl	-48(%rbp), %eax
	addq	-208(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movslq	%eax, %rdx
	movzwl	-144(%rbp,%rdx,2), %edx
	addl	$1, %edx
	cltq
	movw	%dx, -144(%rbp,%rax,2)
	addl	$1, -48(%rbp)
.L1067:
	movl	-196(%rbp), %eax
	cmpl	-48(%rbp), %eax
	ja	.L1068
	movw	$0, -190(%rbp)
	movl	$1, -48(%rbp)
	jmp	.L1069
.L1070:
	movl	-48(%rbp), %eax
	leal	1(%rax), %edi
	movl	-48(%rbp), %eax
	movzwl	-192(%rbp,%rax,2), %edx
	movl	-48(%rbp), %eax
	movzwl	-144(%rbp,%rax,2), %eax
	movzwl	%ax, %esi
	movl	$16, %eax
	subl	-48(%rbp), %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	addl	%eax, %edx
	movl	%edi, %eax
	movw	%dx, -192(%rbp,%rax,2)
	addl	$1, -48(%rbp)
.L1069:
	cmpl	$16, -48(%rbp)
	jbe	.L1070
	movzwl	-158(%rbp), %eax
	testw	%ax, %ax
	je	.L1071
	movl	$.LC144, %edi
	call	error
.L1071:
	movl	$16, %eax
	subl	-200(%rbp), %eax
	movl	%eax, -32(%rbp)
	movl	$1, -48(%rbp)
	jmp	.L1072
.L1073:
	movl	-48(%rbp), %eax
	movzwl	-192(%rbp,%rax,2), %eax
	movzwl	%ax, %edx
	movl	-32(%rbp), %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movl	-48(%rbp), %eax
	movw	%dx, -192(%rbp,%rax,2)
	movl	-200(%rbp), %eax
	subl	-48(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movl	-48(%rbp), %eax
	movw	%dx, -96(%rbp,%rax,2)
	addl	$1, -48(%rbp)
.L1072:
	movl	-200(%rbp), %eax
	cmpl	-48(%rbp), %eax
	jae	.L1073
	jmp	.L1074
.L1075:
	movl	$16, %eax
	subl	-48(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movl	-48(%rbp), %eax
	movw	%dx, -96(%rbp,%rax,2)
	addl	$1, -48(%rbp)
.L1074:
	cmpl	$16, -48(%rbp)
	jbe	.L1075
	movl	-200(%rbp), %eax
	addl	$1, %eax
	cltq
	movzwl	-192(%rbp,%rax,2), %eax
	movzwl	%ax, %edx
	movl	-32(%rbp), %eax
	movl	%edx, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, -48(%rbp)
	cmpl	$0, -48(%rbp)
	je	.L1076
	movl	-200(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, -44(%rbp)
	jmp	.L1077
.L1078:
	movl	-48(%rbp), %eax
	addq	%rax, %rax
	addq	-216(%rbp), %rax
	movw	$0, (%rax)
	addl	$1, -48(%rbp)
.L1077:
	movl	-48(%rbp), %eax
	cmpl	-44(%rbp), %eax
	jne	.L1078
.L1076:
	movl	-196(%rbp), %eax
	movl	%eax, -36(%rbp)
	movl	$15, %eax
	subl	-200(%rbp), %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, -28(%rbp)
	movl	$0, -40(%rbp)
	jmp	.L1079
.L1091:
	movl	-40(%rbp), %eax
	addq	-208(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, -24(%rbp)
	cmpl	$0, -24(%rbp)
	je	.L1092
.L1080:
	movl	-24(%rbp), %eax
	movzwl	-192(%rbp,%rax,2), %eax
	movzwl	%ax, %edx
	movl	-24(%rbp), %eax
	movzwl	-96(%rbp,%rax,2), %eax
	movzwl	%ax, %eax
	addl	%edx, %eax
	movl	%eax, -20(%rbp)
	movl	-200(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jb	.L1082
	movl	-24(%rbp), %eax
	movzwl	-192(%rbp,%rax,2), %eax
	movzwl	%ax, %eax
	movl	%eax, -48(%rbp)
	jmp	.L1083
.L1084:
	movl	-48(%rbp), %eax
	addq	%rax, %rax
	addq	-216(%rbp), %rax
	movl	-40(%rbp), %edx
	movw	%dx, (%rax)
	addl	$1, -48(%rbp)
.L1083:
	movl	-48(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jb	.L1084
	jmp	.L1085
.L1082:
	movl	-24(%rbp), %eax
	movzwl	-192(%rbp,%rax,2), %eax
	movzwl	%ax, %eax
	movl	%eax, -44(%rbp)
	movl	-32(%rbp), %eax
	movl	-44(%rbp), %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	shrl	%cl, %ebx
	movl	%ebx, %eax
	movl	%eax, %eax
	addq	%rax, %rax
	addq	-216(%rbp), %rax
	movq	%rax, -56(%rbp)
	movl	-200(%rbp), %eax
	movl	-24(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -48(%rbp)
	jmp	.L1086
.L1090:
	movq	-56(%rbp), %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L1087
	movl	-36(%rbp), %eax
	addq	$32768, %rax
	addq	%rax, %rax
	leaq	prev(%rax), %rdx
	movl	-36(%rbp), %eax
	movw	$0, prev(%rax,%rax)
	movl	-36(%rbp), %eax
	movzwl	prev(%rax,%rax), %eax
	movw	%ax, (%rdx)
	movl	-36(%rbp), %eax
	movl	%eax, %edx
	movq	-56(%rbp), %rax
	movw	%dx, (%rax)
	addl	$1, -36(%rbp)
.L1087:
	movl	-28(%rbp), %eax
	movl	-44(%rbp), %edx
	andl	%edx, %eax
	testl	%eax, %eax
	je	.L1088
	movq	-56(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movq	%rax, -56(%rbp)
	jmp	.L1089
.L1088:
	movq	-56(%rbp), %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	addq	%rax, %rax
	addq	$prev, %rax
	movq	%rax, -56(%rbp)
.L1089:
	sall	-44(%rbp)
	subl	$1, -48(%rbp)
.L1086:
	cmpl	$0, -48(%rbp)
	jne	.L1090
	movl	-40(%rbp), %eax
	movl	%eax, %edx
	movq	-56(%rbp), %rax
	movw	%dx, (%rax)
.L1085:
	movl	-20(%rbp), %eax
	movl	%eax, %edx
	movl	-24(%rbp), %eax
	movw	%dx, -192(%rbp,%rax,2)
	jmp	.L1081
.L1092:
	nop
.L1081:
	addl	$1, -40(%rbp)
.L1079:
	movl	-196(%rbp), %eax
	cmpl	-40(%rbp), %eax
	ja	.L1091
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE83:
	.size	make_table, .-make_table
	.type	read_pt_len, @function
read_pt_len:
.LFB84:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, %edi
	call	getbits
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	jne	.L1094
	movl	-24(%rbp), %eax
	movl	%eax, %edi
	call	getbits
	movl	%eax, -12(%rbp)
	movl	$0, -16(%rbp)
	jmp	.L1095
.L1096:
	movl	-16(%rbp), %eax
	cltq
	movb	$0, pt_len(%rax)
	addl	$1, -16(%rbp)
.L1095:
	movl	-16(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jl	.L1096
	movl	$0, -16(%rbp)
	jmp	.L1097
.L1098:
	movl	-12(%rbp), %eax
	movl	%eax, %edx
	movl	-16(%rbp), %eax
	cltq
	movw	%dx, pt_table(%rax,%rax)
	addl	$1, -16(%rbp)
.L1097:
	cmpl	$255, -16(%rbp)
	jle	.L1098
	jmp	.L1093
.L1094:
	movl	$0, -16(%rbp)
	jmp	.L1100
.L1108:
	movq	bitbuf(%rip), %rax
	shrq	$13, %rax
	movl	%eax, -12(%rbp)
	cmpl	$7, -12(%rbp)
	jne	.L1101
	movl	$4096, -8(%rbp)
	jmp	.L1102
.L1103:
	shrl	-8(%rbp)
	addl	$1, -12(%rbp)
.L1102:
	movl	-8(%rbp), %edx
	movq	bitbuf(%rip), %rax
	andq	%rdx, %rax
	testq	%rax, %rax
	jne	.L1103
.L1101:
	cmpl	$6, -12(%rbp)
	jle	.L1104
	movl	-12(%rbp), %eax
	subl	$3, %eax
	jmp	.L1105
.L1104:
	movl	$3, %eax
.L1105:
	movl	%eax, %edi
	call	fillbuf
	movl	-12(%rbp), %eax
	movl	%eax, %edx
	movl	-16(%rbp), %eax
	cltq
	movb	%dl, pt_len(%rax)
	addl	$1, -16(%rbp)
	movl	-16(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jne	.L1100
	movl	$2, %edi
	call	getbits
	movl	%eax, -12(%rbp)
	jmp	.L1106
.L1107:
	movl	-16(%rbp), %eax
	cltq
	movb	$0, pt_len(%rax)
	addl	$1, -16(%rbp)
.L1106:
	subl	$1, -12(%rbp)
	cmpl	$0, -12(%rbp)
	jns	.L1107
.L1100:
	movl	-16(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jl	.L1108
	jmp	.L1109
.L1110:
	movl	-16(%rbp), %eax
	cltq
	movb	$0, pt_len(%rax)
	addl	$1, -16(%rbp)
.L1109:
	movl	-16(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jl	.L1110
	movl	-20(%rbp), %eax
	movl	$pt_table, %ecx
	movl	$8, %edx
	movl	$pt_len, %esi
	movl	%eax, %edi
	call	make_table
.L1093:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE84:
	.size	read_pt_len, .-read_pt_len
	.type	read_c_len, @function
read_c_len:
.LFB85:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$9, %edi
	call	getbits
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	jne	.L1112
	movl	$9, %edi
	call	getbits
	movl	%eax, -12(%rbp)
	movl	$0, -16(%rbp)
	jmp	.L1113
.L1114:
	movl	-16(%rbp), %eax
	cltq
	movb	$0, outbuf(%rax)
	addl	$1, -16(%rbp)
.L1113:
	cmpl	$509, -16(%rbp)
	jle	.L1114
	movl	$0, -16(%rbp)
	jmp	.L1115
.L1116:
	movl	-12(%rbp), %eax
	movl	%eax, %edx
	movl	-16(%rbp), %eax
	cltq
	movw	%dx, d_buf(%rax,%rax)
	addl	$1, -16(%rbp)
.L1115:
	cmpl	$4095, -16(%rbp)
	jle	.L1116
	jmp	.L1111
.L1112:
	movl	$0, -16(%rbp)
	jmp	.L1118
.L1129:
	movq	bitbuf(%rip), %rax
	shrq	$8, %rax
	movzwl	pt_table(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -12(%rbp)
	cmpl	$18, -12(%rbp)
	jle	.L1119
	movl	$128, -8(%rbp)
.L1122:
	movl	-8(%rbp), %edx
	movq	bitbuf(%rip), %rax
	andq	%rdx, %rax
	testq	%rax, %rax
	je	.L1120
	movl	-12(%rbp), %eax
	cltq
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -12(%rbp)
	jmp	.L1121
.L1120:
	movl	-12(%rbp), %eax
	cltq
	movzwl	prev(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -12(%rbp)
.L1121:
	shrl	-8(%rbp)
	cmpl	$18, -12(%rbp)
	jg	.L1122
.L1119:
	movl	-12(%rbp), %eax
	cltq
	movzbl	pt_len(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	fillbuf
	cmpl	$2, -12(%rbp)
	jg	.L1123
	cmpl	$0, -12(%rbp)
	jne	.L1124
	movl	$1, -12(%rbp)
	jmp	.L1127
.L1124:
	cmpl	$1, -12(%rbp)
	jne	.L1126
	movl	$4, %edi
	call	getbits
	addl	$3, %eax
	movl	%eax, -12(%rbp)
	jmp	.L1127
.L1126:
	movl	$9, %edi
	call	getbits
	addl	$20, %eax
	movl	%eax, -12(%rbp)
	jmp	.L1127
.L1128:
	movl	-16(%rbp), %eax
	cltq
	movb	$0, outbuf(%rax)
	addl	$1, -16(%rbp)
.L1127:
	subl	$1, -12(%rbp)
	cmpl	$0, -12(%rbp)
	jns	.L1128
	jmp	.L1118
.L1123:
	movl	-12(%rbp), %eax
	leal	-2(%rax), %edx
	movl	-16(%rbp), %eax
	cltq
	movb	%dl, outbuf(%rax)
	addl	$1, -16(%rbp)
.L1118:
	movl	-16(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jl	.L1129
	jmp	.L1130
.L1131:
	movl	-16(%rbp), %eax
	cltq
	movb	$0, outbuf(%rax)
	addl	$1, -16(%rbp)
.L1130:
	cmpl	$509, -16(%rbp)
	jle	.L1131
	movl	$d_buf, %ecx
	movl	$12, %edx
	movl	$outbuf, %esi
	movl	$510, %edi
	call	make_table
.L1111:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE85:
	.size	read_c_len, .-read_c_len
	.type	decode_c, @function
decode_c:
.LFB86:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	blocksize(%rip), %eax
	testl	%eax, %eax
	jne	.L1133
	movl	$16, %edi
	call	getbits
	movl	%eax, blocksize(%rip)
	movl	blocksize(%rip), %eax
	testl	%eax, %eax
	jne	.L1134
	movl	$510, %eax
	jmp	.L1135
.L1134:
	movl	$3, %edx
	movl	$5, %esi
	movl	$19, %edi
	call	read_pt_len
	call	read_c_len
	movl	$-1, %edx
	movl	$4, %esi
	movl	$14, %edi
	call	read_pt_len
.L1133:
	movl	blocksize(%rip), %eax
	subl	$1, %eax
	movl	%eax, blocksize(%rip)
	movq	bitbuf(%rip), %rax
	shrq	$4, %rax
	movzwl	d_buf(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -8(%rbp)
	cmpl	$509, -8(%rbp)
	jbe	.L1136
	movl	$8, -4(%rbp)
.L1139:
	movl	-4(%rbp), %edx
	movq	bitbuf(%rip), %rax
	andq	%rdx, %rax
	testq	%rax, %rax
	je	.L1137
	movl	-8(%rbp), %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -8(%rbp)
	jmp	.L1138
.L1137:
	movl	-8(%rbp), %eax
	movzwl	prev(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -8(%rbp)
.L1138:
	shrl	-4(%rbp)
	cmpl	$509, -8(%rbp)
	ja	.L1139
.L1136:
	movl	-8(%rbp), %eax
	movzbl	outbuf(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	fillbuf
	movl	-8(%rbp), %eax
.L1135:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE86:
	.size	decode_c, .-decode_c
	.type	decode_p, @function
decode_p:
.LFB87:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movq	bitbuf(%rip), %rax
	shrq	$8, %rax
	movzwl	pt_table(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -24(%rbp)
	cmpl	$13, -24(%rbp)
	jbe	.L1141
	.cfi_offset 3, -24
	movl	$128, -20(%rbp)
.L1144:
	movl	-20(%rbp), %edx
	movq	bitbuf(%rip), %rax
	andq	%rdx, %rax
	testq	%rax, %rax
	je	.L1142
	movl	-24(%rbp), %eax
	addq	$32768, %rax
	addq	%rax, %rax
	addq	$prev, %rax
	movzwl	(%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -24(%rbp)
	jmp	.L1143
.L1142:
	movl	-24(%rbp), %eax
	movzwl	prev(%rax,%rax), %eax
	movzwl	%ax, %eax
	movl	%eax, -24(%rbp)
.L1143:
	shrl	-20(%rbp)
	cmpl	$13, -24(%rbp)
	ja	.L1144
.L1141:
	movl	-24(%rbp), %eax
	movzbl	pt_len(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %edi
	call	fillbuf
	cmpl	$0, -24(%rbp)
	je	.L1145
	movl	-24(%rbp), %eax
	subl	$1, %eax
	movl	$1, %edx
	movl	%edx, %ebx
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	-24(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edi
	call	getbits
	addl	%ebx, %eax
	movl	%eax, -24(%rbp)
.L1145:
	movl	-24(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE87:
	.size	decode_p, .-decode_p
	.type	huf_decode_start, @function
huf_decode_start:
.LFB88:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	init_getbits
	movl	$0, blocksize(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE88:
	.size	huf_decode_start, .-huf_decode_start
	.local	j
	.comm	j,4,4
	.local	done
	.comm	done,4,4
	.type	decode_start, @function
decode_start:
.LFB89:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	huf_decode_start
	movl	$0, j(%rip)
	movl	$0, done(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE89:
	.size	decode_start, .-decode_start
	.type	decode, @function
decode:
.LFB90:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L1149
.L1151:
	movl	-8(%rbp), %eax
	addq	-32(%rbp), %rax
	movl	i.5069(%rip), %edx
	movl	%edx, %edx
	addq	-32(%rbp), %rdx
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movl	i.5069(%rip), %eax
	addl	$1, %eax
	andl	$8191, %eax
	movl	%eax, i.5069(%rip)
	addl	$1, -8(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L1149
	movl	-8(%rbp), %eax
	jmp	.L1150
.L1149:
	movl	j(%rip), %eax
	subl	$1, %eax
	movl	%eax, j(%rip)
	movl	j(%rip), %eax
	testl	%eax, %eax
	jns	.L1151
	jmp	.L1157
.L1158:
	nop
.L1157:
	call	decode_c
	movl	%eax, -4(%rbp)
	cmpl	$510, -4(%rbp)
	jne	.L1152
	movl	$1, done(%rip)
	movl	-8(%rbp), %eax
	jmp	.L1150
.L1152:
	cmpl	$255, -4(%rbp)
	ja	.L1153
	movl	-8(%rbp), %eax
	addq	-32(%rbp), %rax
	movl	-4(%rbp), %edx
	movb	%dl, (%rax)
	addl	$1, -8(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L1158
	movl	-8(%rbp), %eax
	jmp	.L1150
.L1153:
	movl	-4(%rbp), %eax
	subl	$253, %eax
	movl	%eax, j(%rip)
	call	decode_p
	movl	-8(%rbp), %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	subl	$1, %eax
	andl	$8191, %eax
	movl	%eax, i.5069(%rip)
	jmp	.L1155
.L1156:
	movl	-8(%rbp), %eax
	addq	-32(%rbp), %rax
	movl	i.5069(%rip), %edx
	movl	%edx, %edx
	addq	-32(%rbp), %rdx
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movl	i.5069(%rip), %eax
	addl	$1, %eax
	andl	$8191, %eax
	movl	%eax, i.5069(%rip)
	addl	$1, -8(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L1155
	movl	-8(%rbp), %eax
	jmp	.L1150
.L1155:
	movl	j(%rip), %eax
	subl	$1, %eax
	movl	%eax, j(%rip)
	movl	j(%rip), %eax
	testl	%eax, %eax
	jns	.L1156
	jmp	.L1158
.L1150:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE90:
	.size	decode, .-decode
	.globl	unlzh
	.type	unlzh, @function
unlzh:
.LFB91:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, ifd(%rip)
	movl	-24(%rbp), %eax
	movl	%eax, ofd(%rip)
	call	decode_start
	jmp	.L1160
.L1161:
	movl	$window, %esi
	movl	$8192, %edi
	call	decode
	movl	%eax, -4(%rbp)
	movl	test(%rip), %eax
	testl	%eax, %eax
	jne	.L1160
	cmpl	$0, -4(%rbp)
	je	.L1160
	movl	-4(%rbp), %edx
	movl	-24(%rbp), %eax
	movl	$window, %esi
	movl	%eax, %edi
	call	write_buf
.L1160:
	movl	done(%rip), %eax
	testl	%eax, %eax
	je	.L1161
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE91:
	.size	unlzh, .-unlzh
	.local	i.5069
	.comm	i.5069,4,4
	.data
	.align 8
	.type	crc.4663, @object
	.size	crc.4663, 8
crc.4663:
	.quad	4294967295
	.local	in_exit.3870
	.comm	in_exit.3870,4,4
	.align 4
	.type	first_time.3812, @object
	.size	first_time.3812, 4
first_time.3812:
	.long	1
	.section	.rodata
.LC145:
	.string	"store"
.LC146:
	.string	"compr"
.LC147:
	.string	"pack "
.LC148:
	.string	"lzh  "
.LC149:
	.string	"defla"
	.data
	.align 32
	.type	methods.3813, @object
	.size	methods.3813, 72
methods.3813:
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC5
	.quad	.LC5
	.quad	.LC5
	.quad	.LC5
	.quad	.LC149
	.section	.rodata
.LC150:
	.string	".z"
.LC151:
	.string	"-z"
.LC152:
	.string	".Z"
	.data
	.align 32
	.type	suffixes.3778, @object
	.size	suffixes.3778, 48
suffixes.3778:
	.quad	z_suffix
	.quad	.LC50
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	0
	.section	.rodata
.LC153:
	.string	"-gz"
.LC154:
	.string	"_z"
	.data
	.align 32
	.type	known_suffixes.3768, @object
	.size	known_suffixes.3768, 72
known_suffixes.3768:
	.quad	z_suffix
	.quad	.LC50
	.quad	.LC150
	.quad	.LC79
	.quad	.LC78
	.quad	.LC153
	.quad	.LC151
	.quad	.LC154
	.quad	0
	.section	.rodata
	.align 8
.LC155:
	.string	" -c --stdout      write on standard output, keep original files unchanged"
.LC156:
	.string	" -d --decompress  decompress"
	.align 8
.LC157:
	.string	" -f --force       force overwrite of output file and compress links"
	.align 8
.LC158:
	.string	" -h --help        give this help"
	.align 8
.LC159:
	.string	" -l --list        list compressed file contents"
	.align 8
.LC160:
	.string	" -L --license     display software license"
	.align 8
.LC161:
	.string	" -n --no-name     do not save or restore the original name and time stamp"
	.align 8
.LC162:
	.string	" -N --name        save or restore the original name and time stamp"
	.align 8
.LC163:
	.string	" -q --quiet       suppress all warnings"
	.align 8
.LC164:
	.string	" -S .suf  --suffix .suf     use suffix .suf on compressed files"
	.align 8
.LC165:
	.string	" -t --test        test compressed file integrity"
	.align 8
.LC166:
	.string	" -v --verbose     verbose mode"
	.align 8
.LC167:
	.string	" -V --version     display version number"
	.align 8
.LC168:
	.string	" -1 --fast        compress faster"
	.align 8
.LC169:
	.string	" -9 --best        compress better"
	.align 8
.LC170:
	.string	" file...          files to (de)compress. If none given, use standard input."
	.data
	.align 32
	.type	help_msg.3671, @object
	.size	help_msg.3671, 136
help_msg.3671:
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.quad	0
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
