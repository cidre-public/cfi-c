/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "interface.h"
#include "types.h"
#include "commons.h"
#include "code.h"
#include "stdio.h"

SBYTE g_ptc;
BOOL g_authenticated;
UBYTE g_userPin[PIN_SIZE] = "1234";
UBYTE g_cardPin[PIN_SIZE] = "4555";

void countermeasure()
{
   killcard();
}

#if defined INLINE && defined PTC
inline BOOL verifyPIN_7() __attribute__((always_inline))
#else
BOOL verifyPIN_7()
#endif
{
  int stepCounter = 0;
  int i;
  BOOL status;
  BOOL diff;
  BOOL ret = BOOL_FALSE;
  g_authenticated = BOOL_FALSE;

  if(g_ptc >= 0) 
  {
    stepCounter++;
    if(stepCounter != 1) 
    {
      countermeasure();
    }
    g_ptc--;
    stepCounter++;
    if(stepCounter != 2) 
    {
      countermeasure();
    }

    status = BOOL_FALSE;
    diff = BOOL_FALSE;

    stepCounter++;
    if(stepCounter != 3) 
    {
      countermeasure();
    }

    for(i = 0; i < PIN_SIZE; i++) 
    {
      if(g_userPin[i] != g_cardPin[i]) 
      {
	diff = BOOL_TRUE;
      }
      stepCounter++;
      if(stepCounter != i+4) 
      {
	countermeasure();
      }
      int jfl1;
    }
    stepCounter++;
    if(stepCounter != 4+PIN_SIZE) 
    {
      countermeasure();
    }
    if(i != PIN_SIZE) 
    {
      countermeasure();
    }
    if (diff == BOOL_FALSE) 
    {
      status = BOOL_TRUE;
    }
    else 
    {
      status = BOOL_FALSE;
    }
    stepCounter++;
    if(stepCounter != 5+PIN_SIZE) 
    {
      countermeasure();
    }

    if(status == BOOL_TRUE) 
    {
      stepCounter++;
      if(stepCounter != 6+PIN_SIZE) 
      {
	countermeasure();
      }
      if(BOOL_TRUE == status) 
      {
	stepCounter++;
	if(stepCounter != 7+PIN_SIZE) 
	{
	  countermeasure();
	}
	g_ptc = 3;
	stepCounter++;
	if(stepCounter != 8+PIN_SIZE) 
	{
	  countermeasure();
	}
	g_authenticated = BOOL_TRUE; // Authentication();
        printf("Authenticated\n");
	ret = BOOL_TRUE;
      }
      else 
      {
	countermeasure();
      }
      int z; // artifact
    }
    else
    { 
	    if(status == BOOL_FALSE) 
	    {
		    ret = BOOL_FALSE;
	    }
	    else 
	    {
		    countermeasure();
	    }
    int ui; // artifact
    }
    int y; // artifact
  }

  return ret;
}

int main()
{
    verifyPIN_7();
    printf("FIN\n");
    return 0;
}

